﻿using FiverrIntelligense.Data.Models;
using FiverrIntelligense.Repository.Common;
using FiverrIntelligense.Repository.IRepository;
using FiverrIntelligense.Services.Common;
using FiverrIntelligense.Services.IServices;
using System;
using System.Collections.Generic;
using System.Text;

namespace FiverrIntelligense.Services.Services
{
    public class UserTypeService : BaseService<TblUserType>, IUserTypeService
    {
        IUserTypeRepo _userTypeRepo;
        public UserTypeService(IUserTypeRepo userRepo) :base(userRepo)
        {
            _userTypeRepo = userRepo;
        }
    }
}
