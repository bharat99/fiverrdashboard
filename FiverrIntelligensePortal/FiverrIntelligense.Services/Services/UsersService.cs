﻿using FiverrIntelligense.Data.Models;
using FiverrIntelligense.Repository.Common;
using FiverrIntelligense.Repository.IRepository;
using FiverrIntelligense.Services.Common;
using FiverrIntelligense.Services.IServices;
using System;
using System.Collections.Generic;
using System.Text;

namespace FiverrIntelligense.Services.Services
{
    public class UsersService: BaseService<TblUsers>,IUsersService
    {
        IUsersRepo _userRepo;
        public UsersService(IUsersRepo userRepo) :base(userRepo)
        {
            _userRepo = userRepo;
        }
    }
}
