﻿using FiverrIntelligense.Data.Models;
using FiverrIntelligense.Repository.Common;
using FiverrIntelligense.Repository.IRepository;
using FiverrIntelligense.Services.Common;
using FiverrIntelligense.Services.IServices;
using System;
using System.Collections.Generic;
using System.Text;

namespace FiverrIntelligense.Services.Services
{
    public class UserStatusService : BaseService<TblUserStatus>, IUserStatusService
    {
        IUserStatusRepo _userStatusRepo;
        public UserStatusService(IUserStatusRepo userRepo) :base(userRepo)
        {
            _userStatusRepo = userRepo;
        }
    }
}
