﻿using FiverrIntelligense.Data.Models;
using FiverrIntelligense.Repository.Common;
using FiverrIntelligense.Repository.IRepository;
using FiverrIntelligense.Services.Common;
using FiverrIntelligense.Services.IServices;
using System;
using System.Collections.Generic;
using System.Text;

namespace FiverrIntelligense.Services.Services
{
    public class UserGroupService: BaseService<TblUserGroupMap>,IUserGroupService
    {
        IUsersGroupRepo _groupRepo;
        public UserGroupService(IUsersGroupRepo groupRepo) :base(groupRepo)
        {
            _groupRepo = groupRepo;
        }
    }
}
