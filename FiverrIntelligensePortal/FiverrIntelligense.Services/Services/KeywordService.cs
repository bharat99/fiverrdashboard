﻿using FiverrIntelligense.Data.Common;
using FiverrIntelligense.Data.Models;
using FiverrIntelligense.Repository.Common;
using FiverrIntelligense.Repository.IRepository;
using FiverrIntelligense.Services.Common;
using FiverrIntelligense.Services.IServices;
using System;
using System.Collections.Generic;
using System.Text;

namespace FiverrIntelligense.Services.Services
{
    public class KeywordService: BaseService<TblFiverrKeyword>,IKeywordService
    {
        IKeywordRepo _keywordRepo;
        public KeywordService(IKeywordRepo keywordRepo) :base(keywordRepo)
        {
            _keywordRepo = keywordRepo;
        }

        public IEnumerable<TblFiverrKeyword> GetAllKeywordOfUserGroup(Guid userId, string groupIds)
        {
            return _keywordRepo.GetAllKeywordOfUserGroup(userId, groupIds);
        }

        public List<KeywordRunningStatus> GetKeywodsRunningStatus(Guid userId, string groupId)
        {
            return _keywordRepo.GetKeywodsRunningStatus(userId, groupId);
        }

        public List<KeywordProgressCount> GetKeywordProgressCount(Guid userId, string groupId)
        {
            return _keywordRepo.GetKeywordProgressCount(userId, groupId);
        }

        #region Rolling Chart Shilpa
        public List<Keyword> GetKeywordByGroup(Guid userId, int groupId)
        {
            return _keywordRepo.GetKeywordByGroup(userId, groupId);
        }
        public RollingData GetRollingData(Guid userId, int keywordId, int secDifference)
        {
            return _keywordRepo.GetRollingData(userId, keywordId, secDifference);
        }
        #endregion
    }
}
