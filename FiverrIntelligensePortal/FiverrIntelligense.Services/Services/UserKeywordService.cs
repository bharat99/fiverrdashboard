﻿using FiverrIntelligense.Data.Models;
using FiverrIntelligense.Repository.Common;
using FiverrIntelligense.Repository.IRepository;
using FiverrIntelligense.Services.Common;
using FiverrIntelligense.Services.IServices;
using System;
using System.Collections.Generic;
using System.Text;

namespace FiverrIntelligense.Services.Services
{
    public class UserKeywordService: BaseService<TblUserKeywordMap>,IUserKeywordService
    {
        IUserKeywordRepo _keywordRepo;
        public UserKeywordService(IUserKeywordRepo keywordRepo) :base(keywordRepo)
        {
            _keywordRepo = keywordRepo;
        }
    }
}
