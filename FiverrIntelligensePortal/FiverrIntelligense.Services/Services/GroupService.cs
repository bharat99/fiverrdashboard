﻿using FiverrIntelligense.Data.Common;
using FiverrIntelligense.Data.Models;
using FiverrIntelligense.Repository.Common;
using FiverrIntelligense.Repository.IRepository;
using FiverrIntelligense.Services.Common;
using FiverrIntelligense.Services.IServices;
using System;
using System.Collections.Generic;
using System.Text;

namespace FiverrIntelligense.Services.Services
{
    public class GroupService: BaseService<TblFiverrGroup>,IGroupService
    {
        IGroupRepo _groupRepo;
        public GroupService(IGroupRepo groupRepo) :base(groupRepo)
        {
            _groupRepo = groupRepo;
        }

        public List<TblFiverrGroup> GetAllGroupsOfUsr(Guid userId)
        {
            return _groupRepo.GetAllGroupsOfUsr(userId);
        }
        public List<TblFiverrKeyword> GetAllKeywordsByGroups(Guid userId, string Groups)
        {
            return _groupRepo.GetAllKeywordsByGroups(userId, Groups);
        }

        public List<HistoryViewModel> GetAllHistory(Guid userId, string groupId, string startDate, string endDate)
        {
            return _groupRepo.GetAllHistory(userId, groupId, startDate, endDate);
        }

        public List<ChangeDetected> GetChangesDetected(Guid userId, string SelectedKeywordIds, string startDate, string endDate)
        {
            return _groupRepo.GetChangesDetected(userId, SelectedKeywordIds, startDate, endDate);
        }

        public List<SellerAndCustomerCount> GetCountryWiseCustomerCount(Guid userId, string SelectedKeywordIds, string startDate,string endDate)
        {
            return _groupRepo.GetCountryWiseCustomerCount(userId, SelectedKeywordIds, startDate, endDate);
        }

        public List<SellerAndCustomerCount> GetCountryWiseSellerCount(Guid userId, string SelectedKeywordIds, string startDate, string endDate)
        {
            return _groupRepo.GetCountryWiseSellerCount(userId, SelectedKeywordIds, startDate, endDate);
        }

        public string GetLastUpdatedDate(Guid userId, string groupId)
        {
            return _groupRepo.GetLastUpdatedDate(userId, groupId);
        }

        public List<PlotData> GetPlotData(Guid userId, string SelectedKeywordIds, string startDate, string endDate, string type)
        {
            return _groupRepo.GetPlotData(userId, SelectedKeywordIds, startDate, endDate, type);
        }

        public List<ChangeDetectedDetails> GetChangesDetectedDetailsByLabel(Guid userId, string selectedKeywordIds, string startDate, string endDate, string label)
        {
            return _groupRepo.GetChangesDetectedDetailsByLabel(userId, selectedKeywordIds, startDate, endDate, label);
        }

       
        #region Feedback Changes - Add View button for dahsboard chart and Display records on Popup
        public List<Seller> GetSellerByUserAssignedGroup(Guid userId, string KeywordIds, string startDate, string endDate)
        {
            return _groupRepo.GetSellerByUserAssignedGroup(userId, KeywordIds, startDate, endDate);
        }
        public List<Gig> GetGigByUserAssignedGroup(Guid userId, string KeywordIds, string startDate, string endDate)
        {
            return _groupRepo.GetGigByUserAssignedGroup(userId, KeywordIds, startDate, endDate);
        }
        public List<CustomerReview> GetNewOrderByUserAssignedGroup(Guid userId, string KeywordIds, string startDate, string endDate)
        {
            return _groupRepo.GetNewOrderByUserAssignedGroup(userId, KeywordIds, startDate, endDate);
        }
        public List<Seller> GetRemovedSellerByUserAssignedGroup(Guid userId, string KeywordIds, string startDate, string endDate)
        {
            return _groupRepo.GetRemovedSellerByUserAssignedGroup(userId, KeywordIds, startDate, endDate);
        }

        public List<Gig> GetRemovedGigByUserAssignedGroup(Guid userId, string KeywordIds, string startDate, string endDate)
        {
            return _groupRepo.GetRemovedGigByUserAssignedGroup(userId, KeywordIds, startDate, endDate);
        }
        #endregion
    }
}
