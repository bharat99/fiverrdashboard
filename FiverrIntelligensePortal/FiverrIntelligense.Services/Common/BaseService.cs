﻿using FiverrIntelligense.Repository.Common;
using System;
using System.Collections.Generic;
using System.Text;

namespace FiverrIntelligense.Services.Common
{
    public class BaseService<T> : IBaseService<T> where T : class
    {
        IBaseRepo<T> _baseRepo;
        public BaseService(IBaseRepo<T> baseRepo)
        {
            _baseRepo = baseRepo;
        }
        public void Delete(int id)
        {
            _baseRepo.Delete(id);
        }

        public IEnumerable<T> GetAll()
        {
            return _baseRepo.GetAll();
        }

        public T GetById(int id)
        {
            return _baseRepo.GetById(id);
        }

        public void Insert(T obj)
        {
            _baseRepo.Insert(obj);
        }

        public void Save()
        {
            _baseRepo.Save();
        }

        public void Update(T obj)
        {
            _baseRepo.Update(obj);
        }
    }
}
