﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FiverrIntelligense.Services.Common
{
    public interface IBaseService<T>
    {
        IEnumerable<T> GetAll();
        T GetById(int id);
        void Insert(T obj);
        void Update(T obj);
        void Delete(int id);
        void Save();
    }
}
