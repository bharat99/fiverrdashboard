﻿using FiverrIntelligense.Data.Common;
using FiverrIntelligense.Data.Models;
using FiverrIntelligense.Repository.Common;
using System;
using System.Collections.Generic;
using System.Text;

namespace FiverrIntelligense.Services.IServices
{
    public interface IKeywordService : IBaseRepo<TblFiverrKeyword>
    {
        IEnumerable<TblFiverrKeyword> GetAllKeywordOfUserGroup(Guid userId, string groupIds);
        List<KeywordRunningStatus> GetKeywodsRunningStatus(Guid userId, string groupId);
        List<KeywordProgressCount> GetKeywordProgressCount(Guid userId, string groupId);
        #region Rolling CHART Shilpa
        List<Keyword> GetKeywordByGroup(Guid userId, int groupId);
        RollingData GetRollingData(Guid userId, int keywordId, int secDifference);
        #endregion
    }
}
