﻿using FiverrIntelligense.Data.Common;
using FiverrIntelligense.Data.Models;
using FiverrIntelligense.Repository.Common;
using System;
using System.Collections.Generic;
using System.Text;

namespace FiverrIntelligense.Services.IServices
{
    public interface IGroupService : IBaseRepo<TblFiverrGroup>
    {
        List<TblFiverrGroup> GetAllGroupsOfUsr(Guid userId);
        List<TblFiverrKeyword> GetAllKeywordsByGroups(Guid userId, string Groups);
        List<SellerAndCustomerCount> GetCountryWiseSellerCount(Guid userId, string SelectedKeywordIds, string startDate, string endDate);
        List<SellerAndCustomerCount> GetCountryWiseCustomerCount(Guid userId, string SelectedKeywordIds, string startDate,string endDate);
        string GetLastUpdatedDate(Guid userId, string SelectedKeywordIds);
        List<PlotData> GetPlotData(Guid userId, string SelectedKeywordIds, string startDate, string endDate, string type);
        List<ChangeDetected> GetChangesDetected(Guid userId, string SelectedKeywordIds, string startDate, string endDate);
        List<HistoryViewModel> GetAllHistory(Guid userId, string groupId, string startDate, string endDate);
        List<ChangeDetectedDetails> GetChangesDetectedDetailsByLabel(Guid userId, string selectedKeywordIds, string startDate, string endDate, string label);

        
        #region Feedback Changes - Add View button for dahsboard chart and Display records on Popup
        List<Seller> GetSellerByUserAssignedGroup(Guid userId, string KeywordIds, string startDate, string endDate);
        List<Gig> GetGigByUserAssignedGroup(Guid userId, string KeywordIds, string startDate, string endDate);

        List<CustomerReview> GetNewOrderByUserAssignedGroup(Guid userId, string KeywordIds, string startDate, string endDate);
        List<Seller> GetRemovedSellerByUserAssignedGroup(Guid userId, string KeywordIds, string startDate, string endDate);

        List<Gig> GetRemovedGigByUserAssignedGroup(Guid userId, string KeywordIds, string startDate, string endDate);
        #endregion

    }
}
