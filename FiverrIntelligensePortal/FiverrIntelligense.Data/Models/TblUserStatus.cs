﻿using System;
using System.Collections.Generic;

namespace FiverrIntelligense.Data.Models
{
    public partial class TblUserStatus
    {
        public int Id { get; set; }
        public Guid StatusId { get; set; }
        public string StatusName { get; set; }
    }
}
