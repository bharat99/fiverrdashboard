﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace FiverrIntelligense.Data.Models
{
    public partial class FiverrDevContext : DbContext
    {
        public FiverrDevContext()
        {
        }

        public FiverrDevContext(DbContextOptions<FiverrDevContext> options)
            : base(options)
        {
        }

        public virtual DbSet<FiverrData> FiverrData { get; set; }
        public virtual DbSet<TblBreadcrumbHistory> TblBreadcrumbHistory { get; set; }
        public virtual DbSet<TblFaqhistory> TblFaqhistory { get; set; }
        public virtual DbSet<TblFiverrCustomerReview> TblFiverrCustomerReview { get; set; }
        public virtual DbSet<TblFiverrDataDetails> TblFiverrDataDetails { get; set; }
        public virtual DbSet<TblFiverrDataDetailsBreadcrumb> TblFiverrDataDetailsBreadcrumb { get; set; }
        public virtual DbSet<TblFiverrDataDetailsHistory> TblFiverrDataDetailsHistory { get; set; }
        public virtual DbSet<TblFiverrDataDetailsLog> TblFiverrDataDetailsLog { get; set; }
        public virtual DbSet<TblFiverrDataDetailsQamapping> TblFiverrDataDetailsQamapping { get; set; }
        public virtual DbSet<TblFiverrDataDetailsQuestion> TblFiverrDataDetailsQuestion { get; set; }
        public virtual DbSet<TblFiverrDataRawHistory> TblFiverrDataRawHistory { get; set; }
        public virtual DbSet<TblFiverrDatadetailsImages> TblFiverrDatadetailsImages { get; set; }
        public virtual DbSet<TblFiverrDetailsPayment> TblFiverrDetailsPayment { get; set; }
        public virtual DbSet<TblFiverrGroup> TblFiverrGroup { get; set; }
        public virtual DbSet<TblFiverrKeyword> TblFiverrKeyword { get; set; }
        public virtual DbSet<TblFiverrRawData> TblFiverrRawData { get; set; }
        public virtual DbSet<TblImagesHistory> TblImagesHistory { get; set; }
        public virtual DbSet<TblKeywordHistory> TblKeywordHistory { get; set; }
        public virtual DbSet<TblPaymentHistory> TblPaymentHistory { get; set; }
        public virtual DbSet<TblReviewHistory> TblReviewHistory { get; set; }
        public virtual DbSet<TblSeller> TblSeller { get; set; }
        public virtual DbSet<TblSellerHistory> TblSellerHistory { get; set; }
        public virtual DbSet<TblUserStatus> TblUserStatus { get; set; }
        public virtual DbSet<TblUserType> TblUserType { get; set; }
        public virtual DbSet<TblUsers> TblUsers { get; set; }
        public virtual DbSet<TblUserKeywordMap> TblUserKeywordMap { get; set; }
        public virtual DbSet<TblUserGroupMap> TblUserGroupMap { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<FiverrData>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("FiverrData");

                entity.Property(e => e.AverageResponseTime).HasMaxLength(100);

                entity.Property(e => e.Date).HasColumnType("date");

                entity.Property(e => e.GigId)
                    .HasColumnName("GigID")
                    .HasMaxLength(500);

                entity.Property(e => e.KeywordID).HasColumnName("KeywordID");

                entity.Property(e => e.KeywordName).HasMaxLength(500);

                entity.Property(e => e.OrderInQueue).HasMaxLength(1000);

                entity.Property(e => e.ProfileUrl).HasColumnName("ProfileURL");

                entity.Property(e => e.Rating).HasMaxLength(100);

                entity.Property(e => e.RecentDeliveryTime).HasMaxLength(1000);

                entity.Property(e => e.SellerFrom).HasMaxLength(300);

                entity.Property(e => e.SellerMemberSince).HasMaxLength(500);

                entity.Property(e => e.SellerName).HasMaxLength(500);

                entity.Property(e => e.SellerTitle).HasMaxLength(1000);

                entity.Property(e => e.Time)
                    .HasMaxLength(15)
                    .IsUnicode(false);

                entity.Property(e => e.Tool).HasMaxLength(1000);

                entity.Property(e => e.TotalRating).HasMaxLength(100);

                entity.Property(e => e.TotalReview).HasMaxLength(500);
            });

            modelBuilder.Entity<TblBreadcrumbHistory>(entity =>
            {
                entity.ToTable("tbl_BreadcrumbHistory");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.BreadcrumbId).HasColumnName("BreadcrumbID");

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.FiverrDataDetailsId).HasColumnName("FiverrDataDetailsID");

                entity.Property(e => e.GigId)
                    .HasColumnName("GigID")
                    .HasMaxLength(500);

                entity.Property(e => e.KeywordID).HasColumnName("KeywordID");
            });

            modelBuilder.Entity<TblFaqhistory>(entity =>
            {
                entity.ToTable("tbl_FAQHistory");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.FiverrDataDetailsId).HasColumnName("FiverrDataDetailsID");

                entity.Property(e => e.GigId)
                    .HasColumnName("GigID")
                    .HasMaxLength(500);

                entity.Property(e => e.KeywordID).HasColumnName("KeywordID");

                entity.Property(e => e.QuestionId).HasColumnName("QuestionID");

                entity.HasOne(d => d.Question)
                    .WithMany(p => p.TblFaqhistory)
                    .HasForeignKey(d => d.QuestionId)
                    .HasConstraintName("FK_tbl_FAQHistory_tbl_FiverrDataDetailsQuestion");
            });

            modelBuilder.Entity<TblFiverrCustomerReview>(entity =>
            {
                entity.HasKey(e => e.ReviewId);

                entity.ToTable("tbl_FiverrCustomerReview");

                entity.Property(e => e.ReviewId).HasColumnName("ReviewID");

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.CustomerFrom).HasMaxLength(500);

                entity.Property(e => e.CustomerName).HasMaxLength(500);

                entity.Property(e => e.FiverrDataDetailsId).HasColumnName("FiverrDataDetailsID");

                entity.Property(e => e.GigId)
                    .HasColumnName("GigID")
                    .HasMaxLength(500);

                entity.Property(e => e.KeywordID).HasColumnName("KeywordID");

                entity.Property(e => e.ReviewDate).HasColumnType("datetime");

                entity.Property(e => e.ReviewRating).HasMaxLength(100);

                entity.Property(e => e.UpdatedOn).HasColumnType("datetime");

                entity.HasOne(d => d.FiverrDataDetails)
                    .WithMany(p => p.TblFiverrCustomerReview)
                    .HasForeignKey(d => d.FiverrDataDetailsId)
                    .HasConstraintName("FK_tbl_FiverrCustomerReview_tbl_FiverrDataDetails");
            });

            modelBuilder.Entity<TblFiverrDataDetails>(entity =>
            {
                entity.HasKey(e => e.FiverrDataDetailsId);

                entity.ToTable("tbl_FiverrDataDetails");

                entity.Property(e => e.FiverrDataDetailsId).HasColumnName("FiverrDataDetailsID");

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.FiverrDataId).HasColumnName("FiverrDataID");

                entity.Property(e => e.GigId)
                    .HasColumnName("GigID")
                    .HasMaxLength(500);

                entity.Property(e => e.KeywordID).HasColumnName("KeywordID");

                entity.Property(e => e.OrderInQueue).HasMaxLength(1000);

                entity.Property(e => e.Tool).HasMaxLength(1000);

                entity.Property(e => e.UpdatedOn).HasColumnType("datetime");

                entity.HasOne(d => d.FiverrData)
                    .WithMany(p => p.TblFiverrDataDetails)
                    .HasForeignKey(d => d.FiverrDataId)
                    .HasConstraintName("FK_tbl_FiverrDataDetails_tbl_FiverrRawData");

            });

            modelBuilder.Entity<TblFiverrDataDetailsBreadcrumb>(entity =>
            {
                entity.ToTable("tbl_FiverrDataDetailsBreadcrumb");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.BreadcrumbName).HasMaxLength(1000);

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.FiverrDataDetailsId).HasColumnName("FiverrDataDetailsID");

                entity.Property(e => e.GigId)
                    .HasColumnName("GigID")
                    .HasMaxLength(500);

                entity.Property(e => e.KeywordID).HasColumnName("KeywordID");

                entity.Property(e => e.Label).HasMaxLength(100);

                entity.Property(e => e.UpdatedOn).HasColumnType("datetime");

                entity.HasOne(d => d.FiverrDataDetails)
                    .WithMany(p => p.TblFiverrDataDetailsBreadcrumb)
                    .HasForeignKey(d => d.FiverrDataDetailsId)
                    .HasConstraintName("FK_tbl_FiverrDataDetailsBreadcrumb_tbl_FiverrDataDetails");
            });

            modelBuilder.Entity<TblFiverrDataDetailsHistory>(entity =>
            {
                entity.ToTable("tbl_FiverrDataDetailsHistory");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.ColumnName).HasMaxLength(1000);

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.FiverrDataDetailsId).HasColumnName("FiverrDataDetailsID");

                entity.Property(e => e.GigId)
                    .HasColumnName("GigID")
                    .HasMaxLength(500);

                entity.Property(e => e.KeywordID).HasColumnName("KeywordID");
            });

            modelBuilder.Entity<TblFiverrDataDetailsLog>(entity =>
            {
                entity.ToTable("tbl_FiverrDataDetailsLog");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.FiverrDataId).HasColumnName("FiverrDataID");

                entity.Property(e => e.GigId)
                    .HasColumnName("GigID")
                    .HasMaxLength(500);

                entity.Property(e => e.KeywordID).HasColumnName("KeywordID");

                entity.Property(e => e.UpdatedOn).HasColumnType("datetime");
            });

            modelBuilder.Entity<TblFiverrDataDetailsQamapping>(entity =>
            {
                entity.HasKey(e => e.AnswerId);

                entity.ToTable("tbl_FiverrDataDetailsQAMapping");

                entity.Property(e => e.AnswerId).HasColumnName("AnswerID");

                entity.Property(e => e.CreatedOn).IsUnicode(false);

                entity.Property(e => e.QuestionId).HasColumnName("QuestionID");

                entity.Property(e => e.UpdatedOn).IsUnicode(false);

                entity.HasOne(d => d.Question)
                    .WithMany(p => p.TblFiverrDataDetailsQamapping)
                    .HasForeignKey(d => d.QuestionId)
                    .HasConstraintName("FK_tbl_FiverrDataDetailsQAMapping_tbl_FiverrDataDetailsQuestion");
            });

            modelBuilder.Entity<TblFiverrDataDetailsQuestion>(entity =>
            {
                entity.HasKey(e => e.QuestionId);

                entity.ToTable("tbl_FiverrDataDetailsQuestion");

                entity.Property(e => e.QuestionId).HasColumnName("QuestionID");

                entity.Property(e => e.CreatedOn).IsUnicode(false);

                entity.Property(e => e.FiverrDataDetailsId).HasColumnName("FiverrDataDetailsID");

                entity.Property(e => e.GigId)
                    .HasColumnName("GigID")
                    .HasMaxLength(500);

                entity.Property(e => e.KeywordID).HasColumnName("KeywordID");

                entity.HasOne(d => d.FiverrDataDetails)
                    .WithMany(p => p.TblFiverrDataDetailsQuestion)
                    .HasForeignKey(d => d.FiverrDataDetailsId)
                    .HasConstraintName("FK_tbl_FiverrDataDetailsQuestion_tbl_FiverrDataDetails");
            });

            modelBuilder.Entity<TblFiverrDataRawHistory>(entity =>
            {
                entity.ToTable("tbl_FiverrDataRawHistory");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.FiverrDataId).HasColumnName("FiverrDataID");

                entity.Property(e => e.GigId)
                    .HasColumnName("GigID")
                    .HasMaxLength(500);

                entity.Property(e => e.KeywordID).HasColumnName("KeywordID");
            });

            modelBuilder.Entity<TblFiverrDatadetailsImages>(entity =>
            {
                entity.ToTable("tbl_fiverrDatadetailsImages");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.FiverrDataDetailsId).HasColumnName("FiverrDataDetailsID");

                entity.Property(e => e.GigId).HasColumnName("GigID");

                entity.Property(e => e.ImageUrlName).HasMaxLength(500);

                entity.Property(e => e.KeywordID).HasColumnName("KeywordID");

                entity.Property(e => e.UpdatedOn).HasColumnType("datetime");

                entity.Property(e => e.VideoUrlName).HasMaxLength(500);
            });

            modelBuilder.Entity<TblFiverrDetailsPayment>(entity =>
            {
                entity.ToTable("tbl_FiverrDetailsPayment");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.DeliveryTime).HasMaxLength(250);

                entity.Property(e => e.DeliveryTimeInHours).HasMaxLength(1000);

                entity.Property(e => e.FiverrDataDetailsId).HasColumnName("FiverrDataDetailsID");

                entity.Property(e => e.GigId)
                    .HasColumnName("GigID")
                    .HasMaxLength(500);

                entity.Property(e => e.KeywordID).HasColumnName("KeywordID");

                entity.Property(e => e.PaymentOption).HasMaxLength(100);

                entity.Property(e => e.Price).HasMaxLength(1000);

                entity.Property(e => e.Revisions).HasMaxLength(300);

                entity.Property(e => e.Title).HasMaxLength(1000);

                entity.Property(e => e.UpdatedOn).HasColumnType("datetime");

                entity.HasOne(d => d.FiverrDataDetails)
                    .WithMany(p => p.TblFiverrDetailsPayment)
                    .HasForeignKey(d => d.FiverrDataDetailsId)
                    .HasConstraintName("FK_tbl_FiverrDetailsPayment_tbl_FiverrDataDetails");
            });

            modelBuilder.Entity<TblFiverrGroup>(entity =>
            {
                entity.HasKey(e => e.GroupId);

                entity.ToTable("tbl_FiverrGroup");

                entity.Property(e => e.GroupId).HasColumnName("GroupID");

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.GroupName).HasMaxLength(1000);

                entity.Property(e => e.UpdatedOn).HasColumnType("datetime");
            });

            modelBuilder.Entity<TblFiverrKeyword>(entity =>
            {
                entity.HasKey(e => e.KeywordID);

                entity.ToTable("tbl_FiverrKeyword");

                entity.Property(e => e.KeywordID).HasColumnName("KeywordID");

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.EndOn).HasColumnType("datetime");

                entity.Property(e => e.GroupId).HasColumnName("GroupID");

                entity.Property(e => e.KeywordName).HasMaxLength(500);

                entity.Property(e => e.ScheduleOn).IsUnicode(false);

                entity.Property(e => e.StartOn).HasColumnType("datetime");

                entity.Property(e => e.Status)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.UpdatedOn).IsUnicode(false);

            });

            modelBuilder.Entity<TblFiverrRawData>(entity =>
            {
                entity.HasKey(e => e.FiverrDataId);

                entity.ToTable("tbl_FiverrRawData");

                entity.Property(e => e.FiverrDataId).HasColumnName("FiverrDataID");

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.GigId)
                    .HasColumnName("GigID")
                    .HasMaxLength(500);

                entity.Property(e => e.KeywordID).HasColumnName("KeywordID");

                entity.Property(e => e.RemovedOn).HasColumnType("datetime");

                entity.Property(e => e.StartingPrice).HasMaxLength(1000);

                entity.Property(e => e.UpdatedOn).HasColumnType("datetime");

            });

            modelBuilder.Entity<TblImagesHistory>(entity =>
            {
                entity.ToTable("tbl_ImagesHistory");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.FiverrDataDetailsId).HasColumnName("FiverrDataDetailsID");

                entity.Property(e => e.GigId)
                    .HasColumnName("GigID")
                    .HasMaxLength(500);

                entity.Property(e => e.ImageId).HasColumnName("ImageID");

                entity.Property(e => e.KeywordID).HasColumnName("KeywordID");
            });

            modelBuilder.Entity<TblKeywordHistory>(entity =>
            {
                entity.ToTable("tbl_KeywordHistory");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.KeywordID).HasColumnName("KeywordID");
            });

            modelBuilder.Entity<TblPaymentHistory>(entity =>
            {
                entity.ToTable("tbl_PaymentHistory");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.FiverrDataDetailsId).HasColumnName("FiverrDataDetailsID");

                entity.Property(e => e.GigId)
                    .HasColumnName("GigID")
                    .HasMaxLength(500);

                entity.Property(e => e.KeywordID).HasColumnName("KeywordID");

                entity.Property(e => e.PaymentId).HasColumnName("PaymentID");
            });

            modelBuilder.Entity<TblReviewHistory>(entity =>
            {
                entity.ToTable("tbl_ReviewHistory");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.FiverrDataDetailsId).HasColumnName("FiverrDataDetailsID");

                entity.Property(e => e.GigId)
                    .HasColumnName("GigID")
                    .HasMaxLength(500);

                entity.Property(e => e.KeywordID).HasColumnName("KeywordID");

                entity.Property(e => e.ReviewId).HasColumnName("ReviewID");
            });

            modelBuilder.Entity<TblSeller>(entity =>
            {
                entity.HasKey(e => e.SellerId);

                entity.ToTable("tbl_seller");

                entity.Property(e => e.SellerId).HasColumnName("SellerID");

                entity.Property(e => e.AverageResponseTime).HasMaxLength(500);

                entity.Property(e => e.AverageResponseTimeInHours).HasMaxLength(100);

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.FiverrDataDetailsId).HasColumnName("FiverrDataDetailsID");

                entity.Property(e => e.FiverrDataId).HasColumnName("FiverrDataID");

                entity.Property(e => e.GigId)
                    .HasColumnName("GigID")
                    .HasMaxLength(500);

                entity.Property(e => e.KeywordID).HasColumnName("KeywordID");

                entity.Property(e => e.ProfileImageUrl).HasColumnName("ProfileImageURL");

                entity.Property(e => e.ProfileUrl).HasColumnName("ProfileURL");

                entity.Property(e => e.RatingOutOfFive).HasMaxLength(100);

                entity.Property(e => e.SellerFrom).HasMaxLength(300);

                entity.Property(e => e.SellerLevel).HasMaxLength(300);

                entity.Property(e => e.SellerMemberSince).HasMaxLength(500);

                entity.Property(e => e.SellerName).HasMaxLength(500);

                entity.Property(e => e.Title).HasMaxLength(1000);

                entity.Property(e => e.TotalReview).HasMaxLength(500);

                entity.Property(e => e.TotatRatingAboutSeller).HasMaxLength(100);

                entity.Property(e => e.UpdatedOn).HasColumnType("datetime");

                entity.HasOne(d => d.FiverrDataDetails)
                    .WithMany(p => p.TblSeller)
                    .HasForeignKey(d => d.FiverrDataDetailsId)
                    .HasConstraintName("FK_tbl_seller_tbl_FiverrDataDetails");

                entity.HasOne(d => d.FiverrData)
                    .WithMany(p => p.TblSeller)
                    .HasForeignKey(d => d.FiverrDataId)
                    .HasConstraintName("FK_tbl_seller_tbl_FiverrRawData");
            });

            modelBuilder.Entity<TblSellerHistory>(entity =>
            {
                entity.ToTable("tbl_SellerHistory");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.FiverrDataDetailsId).HasColumnName("FiverrDataDetailsID");

                entity.Property(e => e.GigId)
                    .HasColumnName("GigID")
                    .HasMaxLength(500);

                entity.Property(e => e.KeywordID).HasColumnName("KeywordID");

                entity.Property(e => e.SellerId).HasColumnName("SellerID");
            });
            modelBuilder.Entity<TblUserKeywordMap>(entity =>
            {
                entity.ToTable("tbl_UserKeywordMap");

                entity.Property(e => e.Id).HasColumnName("Id");
                entity.Property(e => e.KeywordID).HasColumnName("KeywordID");

                entity.Property(e => e.UserId).HasColumnName("UserId");
            });

            modelBuilder.Entity<TblUserGroupMap>(entity =>
            {
                entity.ToTable("tbl_UserGroupMap");

                entity.Property(e => e.Id).HasColumnName("Id");
                entity.Property(e => e.GroupId).HasColumnName("GroupId");

                entity.Property(e => e.UserId).HasColumnName("UserId");
            });

            modelBuilder.Entity<TblUserStatus>(entity =>
            {
                entity.HasKey(e => e.StatusId);

                entity.ToTable("tbl_UserStatus");

                entity.Property(e => e.StatusId).ValueGeneratedNever();

                entity.Property(e => e.Id).ValueGeneratedOnAdd();

                entity.Property(e => e.StatusName)
                    .IsRequired()
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<TblUserType>(entity =>
            {
                entity.HasKey(e => e.TypeId);

                entity.ToTable("tbl_UserType");

                entity.Property(e => e.TypeId).ValueGeneratedNever();

                entity.Property(e => e.Id).ValueGeneratedOnAdd();

                entity.Property(e => e.TypeName)
                    .IsRequired()
                    .HasMaxLength(100);
            });

            modelBuilder.Entity<TblUsers>(entity =>
            {
                entity.HasKey(e => e.UserId)
                    .HasName("PK_Users");

                entity.ToTable("tbl_Users");

                entity.Property(e => e.UserId).HasDefaultValueSql("(newid())");

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.Email)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(e => e.Fname).HasMaxLength(100);

                entity.Property(e => e.Id).ValueGeneratedOnAdd()
            .Metadata.SetAfterSaveBehavior(PropertySaveBehavior.Ignore); ;

                entity.Property(e => e.Lname).HasMaxLength(150);
                entity.Property(e => e.VerificationCode).HasMaxLength(10);

                entity.Property(e => e.Password)
                    .IsRequired()
                    .HasMaxLength(10);

                entity.Property(e => e.RowStatus).HasDefaultValueSql("((4))");

                entity.Property(e => e.UserName)
                    .IsRequired()
                    .HasMaxLength(50);
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
