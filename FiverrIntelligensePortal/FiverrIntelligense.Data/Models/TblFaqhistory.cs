﻿using System;
using System.Collections.Generic;

namespace FiverrIntelligense.Data.Models
{
    public partial class TblFaqhistory
    {
        public int Id { get; set; }
        public int? QuestionId { get; set; }
        public int? FiverrDataDetailsId { get; set; }
        public int? KeywordID { get; set; }
        public string GigId { get; set; }
        public string ColumnName { get; set; }
        public string ColumnValue { get; set; }
        public DateTime? CreatedOn { get; set; }

        public virtual TblFiverrDataDetailsQuestion Question { get; set; }
    }
}
