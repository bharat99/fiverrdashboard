﻿using System;
using System.Collections.Generic;

namespace FiverrIntelligense.Data.Models
{
    public partial class TblUserGroupMap
    {
        public int Id { get; set; }
        public Guid UserId { get; set; }
        public int GroupId { get; set; }
        public int IsActive { get; set; } = 1;
    }
}
