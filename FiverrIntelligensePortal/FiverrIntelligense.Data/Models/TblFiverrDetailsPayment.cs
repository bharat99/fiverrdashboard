﻿using System;
using System.Collections.Generic;

namespace FiverrIntelligense.Data.Models
{
    public partial class TblFiverrDetailsPayment
    {
        public int Id { get; set; }
        public int? FiverrDataDetailsId { get; set; }
        public int? KeywordID { get; set; }
        public string GigId { get; set; }
        public string PaymentOption { get; set; }
        public string Title { get; set; }
        public string Price { get; set; }
        public string Description { get; set; }
        public string DeliveryTime { get; set; }
        public string DeliveryTimeInHours { get; set; }
        public string Revisions { get; set; }
        public string KeyFeatures { get; set; }
        public DateTime? CreatedOn { get; set; }
        public DateTime? UpdatedOn { get; set; }

        public virtual TblFiverrDataDetails FiverrDataDetails { get; set; }
    }
}
