﻿using System;
using System.Collections.Generic;

namespace FiverrIntelligense.Data.Models
{
    public partial class TblFiverrDataDetailsQuestion
    {
        public TblFiverrDataDetailsQuestion()
        {
            TblFaqhistory = new HashSet<TblFaqhistory>();
            TblFiverrDataDetailsQamapping = new HashSet<TblFiverrDataDetailsQamapping>();
        }

        public int QuestionId { get; set; }
        public int? FiverrDataDetailsId { get; set; }
        public int? KeywordID { get; set; }
        public string GigId { get; set; }
        public string Question { get; set; }
        public string CreatedOn { get; set; }
        public string UpdatedOn { get; set; }

        public virtual TblFiverrDataDetails FiverrDataDetails { get; set; }
        public virtual ICollection<TblFaqhistory> TblFaqhistory { get; set; }
        public virtual ICollection<TblFiverrDataDetailsQamapping> TblFiverrDataDetailsQamapping { get; set; }
    }
}
