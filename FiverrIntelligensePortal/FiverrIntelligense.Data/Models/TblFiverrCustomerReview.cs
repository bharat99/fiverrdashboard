﻿using System;
using System.Collections.Generic;

namespace FiverrIntelligense.Data.Models
{
    public partial class TblFiverrCustomerReview
    {
        public int ReviewId { get; set; }
        public int? FiverrDataDetailsId { get; set; }
        public int? KeywordID { get; set; }
        public string GigId { get; set; }
        public string CustomerImageUrl { get; set; }
        public string CustomerName { get; set; }
        public string ReviewRating { get; set; }
        public string CustomerFrom { get; set; }
        public string Description { get; set; }
        public int? IsHelpful { get; set; }
        public int? IsNotHelpful { get; set; }
        public DateTime? ReviewDate { get; set; }
        public DateTime? CreatedOn { get; set; }
        public DateTime? UpdatedOn { get; set; }

        public virtual TblFiverrDataDetails FiverrDataDetails { get; set; }
    }
}
