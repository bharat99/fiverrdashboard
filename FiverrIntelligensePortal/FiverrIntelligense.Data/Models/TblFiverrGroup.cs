﻿using System;
using System.Collections.Generic;

namespace FiverrIntelligense.Data.Models
{
    public partial class TblFiverrGroup
    {

        public int GroupId { get; set; }
        public string GroupName { get; set; }
        public int? IsActive { get; set; }
        public DateTime? CreatedOn { get; set; }
        public DateTime? UpdatedOn { get; set; }

    }
}
