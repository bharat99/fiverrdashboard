﻿using System;
using System.Collections.Generic;

namespace FiverrIntelligense.Data.Models
{
    public partial class TblFiverrDataDetailsQamapping
    {
        public int AnswerId { get; set; }
        public int? QuestionId { get; set; }
        public string Answer { get; set; }
        public string CreatedOn { get; set; }
        public string UpdatedOn { get; set; }

        public virtual TblFiverrDataDetailsQuestion Question { get; set; }
    }
}
