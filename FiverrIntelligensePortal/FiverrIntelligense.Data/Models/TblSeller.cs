﻿using System;
using System.Collections.Generic;

namespace FiverrIntelligense.Data.Models
{
    public partial class TblSeller
    {
        public int SellerId { get; set; }
        public int? FiverrDataId { get; set; }
        public int? FiverrDataDetailsId { get; set; }
        public int? KeywordID { get; set; }
        public string GigId { get; set; }
        public string SellerName { get; set; }
        public string ProfileUrl { get; set; }
        public string ProfileImageUrl { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string SellerFrom { get; set; }
        public string SellerMemberSince { get; set; }
        public string AverageResponseTime { get; set; }
        public string RatingOutOfFive { get; set; }
        public int? TotalRating { get; set; }
        public string SellerLevel { get; set; }
        public int? SellerLevelInNumber { get; set; }
        public string TotalReview { get; set; }
        public string TotatRatingAboutSeller { get; set; }
        public string AverageResponseTimeInHours { get; set; }
        public DateTime? CreatedOn { get; set; }
        public DateTime? UpdatedOn { get; set; }

        public virtual TblFiverrRawData FiverrData { get; set; }
        public virtual TblFiverrDataDetails FiverrDataDetails { get; set; }
    }
}
