﻿using System;
using System.Collections.Generic;

namespace FiverrIntelligense.Data.Models
{
    public partial class TblUserType
    {
        public int Id { get; set; }
        public Guid TypeId { get; set; }
        public string TypeName { get; set; }
    }
}
