﻿using System;
using System.Collections.Generic;

namespace FiverrIntelligense.Data.Models
{
    public partial class TblFiverrDatadetailsImages
    {
        public int Id { get; set; }
        public int? FiverrDataDetailsId { get; set; }
        public int? KeywordID { get; set; }
        public string GigId { get; set; }
        public string ImageUrlName { get; set; }
        public string ImageUrl { get; set; }
        public string VideoUrlName { get; set; }
        public string VideoUrl { get; set; }
        public DateTime? CreatedOn { get; set; }
        public DateTime? UpdatedOn { get; set; }
    }
}
