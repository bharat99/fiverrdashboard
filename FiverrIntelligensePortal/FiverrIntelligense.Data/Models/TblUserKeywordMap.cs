﻿using System;
using System.Collections.Generic;

namespace FiverrIntelligense.Data.Models
{
    public partial class TblUserKeywordMap
    {
        public int Id { get; set; }
        public Guid UserId { get; set; }
        public int KeywordID { get; set; }
        public int IsActive { get; set; } = 1;
    }
}
