﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace FiverrIntelligense.Data.Models
{
    public partial class TblFiverrKeyword
    {

        public int KeywordID { get; set; }
        public string KeywordName { get; set; }
        public int? GroupId { get; set; }
        [NotMapped]
        public string GroupName { get; set; }
        public int? DataExtractCount { get; set; }
        public string ServicesAvailable { get; set; }
        public DateTime? CreatedOn { get; set; }
        public string UpdatedOn { get; set; }
        public string ScheduleOn { get; set; }
        public DateTime? StartOn { get; set; }
        public DateTime? EndOn { get; set; }
        public string Status { get; set; }
        public string Remark { get; set; }

    }

}
