﻿using System;
using System.Collections.Generic;

namespace FiverrIntelligense.Data.Models
{
    public partial class TblFiverrDataDetailsLog
    {
        public int Id { get; set; }
        public int? FiverrDataId { get; set; }
        public int? KeywordID { get; set; }
        public string GigId { get; set; }
        public string NextPageUrl { get; set; }
        public string PageSource { get; set; }
        public int? IsProcessed { get; set; }
        public DateTime? CreatedOn { get; set; }
        public DateTime? UpdatedOn { get; set; }
    }
}
