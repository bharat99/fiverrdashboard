﻿using System;
using System.Collections.Generic;

namespace FiverrIntelligense.Data.Models
{
    public partial class TblFiverrDataDetails
    {
        public TblFiverrDataDetails()
        {
            TblFiverrCustomerReview = new HashSet<TblFiverrCustomerReview>();
            TblFiverrDataDetailsBreadcrumb = new HashSet<TblFiverrDataDetailsBreadcrumb>();
            TblFiverrDataDetailsQuestion = new HashSet<TblFiverrDataDetailsQuestion>();
            TblFiverrDetailsPayment = new HashSet<TblFiverrDetailsPayment>();
            TblSeller = new HashSet<TblSeller>();
        }

        public int FiverrDataDetailsId { get; set; }
        public int? FiverrDataId { get; set; }
        public int? KeywordID { get; set; }
        public string GigId { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public int? YouSave { get; set; }
        public string OrderInQueue { get; set; }
        public string Tool { get; set; }
        public DateTime? CreatedOn { get; set; }
        public DateTime? UpdatedOn { get; set; }

        public virtual TblFiverrRawData FiverrData { get; set; }
        public virtual TblFiverrKeyword Keyword { get; set; }
        public virtual ICollection<TblFiverrCustomerReview> TblFiverrCustomerReview { get; set; }
        public virtual ICollection<TblFiverrDataDetailsBreadcrumb> TblFiverrDataDetailsBreadcrumb { get; set; }
        public virtual ICollection<TblFiverrDataDetailsQuestion> TblFiverrDataDetailsQuestion { get; set; }
        public virtual ICollection<TblFiverrDetailsPayment> TblFiverrDetailsPayment { get; set; }
        public virtual ICollection<TblSeller> TblSeller { get; set; }
    }
}
