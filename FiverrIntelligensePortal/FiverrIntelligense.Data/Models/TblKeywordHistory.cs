﻿using System;
using System.Collections.Generic;

namespace FiverrIntelligense.Data.Models
{
    public partial class TblKeywordHistory
    {
        public int Id { get; set; }
        public int? KeywordID { get; set; }
        public string ColumnName { get; set; }
        public string ColumnValue { get; set; }
        public DateTime? CreatedOn { get; set; }
    }
}
