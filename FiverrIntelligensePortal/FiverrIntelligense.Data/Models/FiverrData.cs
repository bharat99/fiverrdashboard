﻿using System;
using System.Collections.Generic;

namespace FiverrIntelligense.Data.Models
{
    public partial class FiverrData
    {
        public int KeywordID { get; set; }
        public string KeywordName { get; set; }
        public string GigId { get; set; }
        public int? Page { get; set; }
        public int? GigPosition { get; set; }
        public string MainTitle { get; set; }
        public string AboutGig { get; set; }
        public long? LengthOfMainDescription { get; set; }
        public int? KeywordCountInDescription { get; set; }
        public string OrderInQueue { get; set; }
        public string SellerTitle { get; set; }
        public string SellerName { get; set; }
        public string ProfileUrl { get; set; }
        public int? SellerLevel { get; set; }
        public string SellerFrom { get; set; }
        public string SellerMemberSince { get; set; }
        public string AverageResponseTime { get; set; }
        public string Rating { get; set; }
        public int? ReviewCount { get; set; }
        public string TotalReview { get; set; }
        public string TotalRating { get; set; }
        public string SellerDescription { get; set; }
        public long? LengthOfSellerDescription { get; set; }
        public int? KeywordCountInSellerDescription { get; set; }
        public string RecentDeliveryTime { get; set; }
        public string Tool { get; set; }
        public DateTime? Date { get; set; }
        public string Time { get; set; }
    }
}
