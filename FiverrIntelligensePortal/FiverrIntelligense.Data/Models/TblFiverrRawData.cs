﻿using System;
using System.Collections.Generic;

namespace FiverrIntelligense.Data.Models
{
    public partial class TblFiverrRawData
    {
        public TblFiverrRawData()
        {
            TblFiverrDataDetails = new HashSet<TblFiverrDataDetails>();
            TblSeller = new HashSet<TblSeller>();
        }

        public int FiverrDataId { get; set; }
        public int? KeywordID { get; set; }
        public string GigId { get; set; }
        public int? GigPosition { get; set; }
        public int? GigIndex { get; set; }
        public int? Page { get; set; }
        public string NextPageUrl { get; set; }
        public string ImageUrl { get; set; }
        public string ImageUrlSet { get; set; }
        public string StartingPrice { get; set; }
        public DateTime? CreatedOn { get; set; }
        public DateTime? UpdatedOn { get; set; }
        public int? IsChecked { get; set; }
        public int? IsRemoved { get; set; }
        public DateTime? RemovedOn { get; set; }

        public virtual TblFiverrKeyword Keyword { get; set; }
        public virtual ICollection<TblFiverrDataDetails> TblFiverrDataDetails { get; set; }
        public virtual ICollection<TblSeller> TblSeller { get; set; }
    }
}
