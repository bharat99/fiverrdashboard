﻿using System;
using System.Collections.Generic;

namespace FiverrIntelligense.Data.Models
{
    public partial class TblFiverrDataDetailsBreadcrumb
    {
        public int Id { get; set; }
        public int? FiverrDataDetailsId { get; set; }
        public int? KeywordID { get; set; }
        public string GigId { get; set; }
        public string BreadcrumbName { get; set; }
        public string BreadcrumbUrl { get; set; }
        public string Label { get; set; }
        public DateTime? CreatedOn { get; set; }
        public DateTime? UpdatedOn { get; set; }

        public virtual TblFiverrDataDetails FiverrDataDetails { get; set; }
    }
}
