﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace FiverrIntelligense.Data.Common
{
    [NotMapped]
    public class RollingData
    {
        public string KeywordName { get; set; }

        public int TotalServices { get; set; }
        public int ServicesAvailable { get; set; }
        public string Status { get; set; }
    }
    public class Keyword
    {
        public int KeywordID { get; set; }
        public string KeywordName { get; set; }

        public int GroupID { get; set; }
    }
}
