﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace FiverrIntelligense.Data.Common
{
    [NotMapped]
    public class SellerAndCustomerCount
    {
        public int GroupId { get; set; }
        public string GroupName { get; set; }
        public int KeywordId { get; set; }
        public string KeywordName { get; set; }
        public string Country { get; set; }
        public int Count { get; set; }
    }
}
