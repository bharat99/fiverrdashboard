﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FiverrIntelligense.Data.Common
{
   public class Gig
    {
        public string Date { get; set; }
       public string KeywordName { get; set; }
        public string GigID { get; set; }
        public string NextPageUrl { get; set; }
        public string Title { get; set; }
        public string SellerName { get; set; }
        //public string Description { get; set; }
        public string OrderInQueue { get;set; }
        public string StartingPrice { get; set; }
        public string Tool { get; set; }
        public int? YouSave { get; set; }
    }
}
