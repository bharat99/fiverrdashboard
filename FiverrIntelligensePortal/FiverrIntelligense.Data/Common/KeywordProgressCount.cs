﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FiverrIntelligense.Data.Common
{
    public class KeywordProgressCount
    {
        public string GroupID { get; set; } 
        public int TotalGigs { get; set; }
        public int CompletedGigs { get; set; }
    }
}
