﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FiverrIntelligense.Data.Common
{
    public class CustomerReview
    {
        public string Date { get; set; }
        public string KeywordName { get; set; }
        public string GigID { get; set; }
        public string NextPageUrl { get; set; }
        public string CustomerName { get; set; }
        public string Description { get; set; }
        public string CustomerFrom { get; set; }
        public string ReviewRating { get; set; }
    }
}
