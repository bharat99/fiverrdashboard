﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace FiverrIntelligense.Data.Common
{
    [NotMapped]
    public class PlotData
    {
        public string Date { get; set; }
        public int Count { get; set; }
    }

    [NotMapped]
    public class ChangeDetected
    {
        public string ColumnName { get; set; }
        public int Count { get; set; }
    }
}
