﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace FiverrIntelligense.Data.Common
{
    [NotMapped]
    public class HistoryViewModel
    {
        public string Date { get; set; }
        public string BussinesGroup { get; set; }
        public int TotalKeywords { get; set; }
        public int NewOrdersFound { get; set; }
        public int NewSellersFound { get; set; }
        public int NewGigsFound { get; set; }
        public int NoOfChangesInGigs { get; set; }
        public int NoOfReviewsFound { get; set; }

    }
}
