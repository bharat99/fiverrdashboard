﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FiverrIntelligense.Data.Common
{
    public partial class Seller
    {
        public string Date { get; set; }
        public string KeywordName { get; set; }
        public string GigID { get; set; }
        public string NextPageUrl { get; set; }
        public string SellerName { get; set; }
        public string ProfileURL { get; set; }
        public string Title { get; set; }
        //public string Description { get; set; }
        public string SellerFrom { get; set; }
        public string SellerMemberSince { get; set; }
        public string AverageResponseTime { get; set; }
        public string RatingOutOfFive { get; set; }
        public int? TotalRating { get; set; }
        public string SellerLevel { get; set; }
        public string TotalReview { get; set; }
        public string TotatRatingAboutSeller { get; set; }
        
    }
    
}
