﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace FiverrIntelligense.Data.Common
{
    [NotMapped]
    public class KeywordRunningStatus
    {
        public int GroupID { get; set; } 
        public int Total { get; set; } 
        public int Running { get; set; } 
        public int Completed { get; set; } 
        public int Stoped { get; set; } 
        //Total   Running Completed   Stoped
    }
}
