﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FiverrIntelligense.Data.Common
{
    public class ChangeDetectedDetails
    {
        public string Value { get; set; }
        public string KeywordName { get; set; }
        public string NextPageUrl { get; set; }
        
        public string GigId { get; set; }
        public string Date { get; set; }
    }
}
