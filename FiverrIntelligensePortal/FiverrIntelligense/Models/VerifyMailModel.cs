﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace FiverrIntelligense.Models
{
    public class VerifyMailViewModel
    {
        public string UserEmail { get; set; }
        [Required(ErrorMessage = "Code is required")]
        public string VerifyCode { get; set; }
    }
}
