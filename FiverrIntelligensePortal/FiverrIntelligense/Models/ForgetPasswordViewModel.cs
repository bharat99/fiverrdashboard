﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace FiverrIntelligense.Models
{
    public class ForgetPasswordViewModel
    {
        public string EmailForget { get; set; }
        [Required(ErrorMessage = "Password is required")]
        [StringLength(10, ErrorMessage = "Must be between 5 and 10 characters", MinimumLength = 5)]
        [RegularExpression("(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)(?=.*[^\\da-zA-Z]).{5,10}$", ErrorMessage = "Password must contain letters, numbers and at least one special character")]
        [DataType(DataType.Password)]
        public string ForgotPassword { get; set; }
        [Required(ErrorMessage = "Confirm Password is required")]
        [StringLength(10, ErrorMessage = "Must be between 5 and 10 characters", MinimumLength = 5)]
        [DataType(DataType.Password)]
        [Compare("ForgotPassword",ErrorMessage ="Passwords does not match.")]
        public string ComfirmForgotPassword
        {
            get; set;
        }
    }
}
