﻿using FiverrIntelligense.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FiverrIntelligense.Models
{
    public class GroupWithAllKeywords
    {
        public int GroupId { get; set; }
        public string GroupName { get; set; }
        public List<TblFiverrKeyword> lstKeyword { get; set; }
    }
}
