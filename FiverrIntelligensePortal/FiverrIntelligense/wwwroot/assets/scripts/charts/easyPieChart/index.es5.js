﻿'use strict';

Object.defineProperty(exports, '__esModule', {
  value: true
});

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj['default'] = obj; return newObj; } }

var _jquery = require('jquery');

var $ = _interopRequireWildcard(_jquery);

require('easy-pie-chart/dist/jquery.easypiechart.min.js');

exports['default'] = (function () {
  if ($('.easy-pie-chart').length > 0) {
    $('.easy-pie-chart').easyPieChart({
      onStep: function onStep(from, to, percent) {
        this.el.children[0].innerHTML = Math.round(percent) + ' %';
      }
    });
  }
})();

module.exports = exports['default'];

