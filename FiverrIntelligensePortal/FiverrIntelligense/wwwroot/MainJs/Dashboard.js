﻿
var ChangeDetected = {
    'Title': 'title', 'Description': 'description', 'Cost': 'price', 'Delivery Time': 'deliverytime', 'Offering': 'YouSave',
    'title': 'Title', 'description': 'Description', 'price': 'Cost', 'deliverytime': 'Delivery Time', 'YouSave': 'Offering'
};

var GlobalChartObjects = {
    'NewSellerChart': null,
    'NewGigsChart': null,
    'NewOrderChart': null,
    'RemovedSellerChart': null,
    'RemovedGigsChart': null,
    'ChangesDetectedChart': null
};
var CountriesList = [];
var lstLatLagData = [];
var isMapPloted = false;
var SelectedGroupIds = '', SelectedKeywordIds = '';
var isChart = false;
$(document).ready(function () {
    $('#ddlBussinessGroup').multiselect({
        buttonWidth: '200px',
        includeSelectAllOption: true,
        multiple: true
    });
    $('#ddlBussinessGroup').multiselect('selectAll', false);
    $('#ddlBussinessGroup').multiselect('updateButtonText');
    $('#ddlGroupKeywords').multiselect({
        buttonWidth: '200px',
        includeSelectAllOption: true,
        multiple: true
    });

    $('#ddlBussinessGroup').on('change', function () {
        SelectedGroupIds = $('#ddlBussinessGroup').val().join(',');
        $.get("/Dashboard/GetAllKeywords", { groupId: SelectedGroupIds }, function (data) {
            $('#ddlGroupKeywords').multiselect('destroy');
            $('#ddlGroupKeywords').html('');
            if (data === '') {
                toastr.error('Keyword not found');
                return false;
            }

            data = JSON.parse(data);
            for (var i = 0; i < data.length; i++) {
                $('#ddlGroupKeywords').append('<option value="' + data[i].KeywordID + '">' + data[i].KeywordName + '</option>');
            }

            $('#ddlGroupKeywords').multiselect({
                buttonWidth: '200px',
                includeSelectAllOption: true,
                multiple: true
            });
            $('#ddlGroupKeywords').multiselect('selectAll', false);
            $('#ddlGroupKeywords').multiselect('updateButtonText');
            $('#ddlGroupKeywords').trigger('change');
        });
    });

    $('#ddlGroupKeywords').on('change', function () {
        loader.classList.remove('fadeOut');
        //setTimeout(function () {
        //    loader.classList.add('fadeOut');
        //}, 10000);
        if (isMapPloted) {
            var mapObj = $("#divWorldMap").vectorMap("get", "mapObject");
            mapObj.remove();
            isMapPloted = false;
        }
        var startDate = $('#reportrange').data('daterangepicker').startDate.format('YYYY-MM-DD');
        var endDate = $('#reportrange').data('daterangepicker').endDate.format('YYYY-MM-DD');

        SelectedKeywordIds = $('#ddlGroupKeywords').val().join(',');
        GetLastUpdatedDate(SelectedKeywordIds);
        PlotSellersAndCustomersInMap(SelectedKeywordIds, startDate, endDate);

        PlotNewSellerChart();
        PlotNewGigsChart();
        PlotNewOrderChart();
        PlotRemovedSellerChart();
        PlotRemovedGigsChart();

        PlotChangesDetectedChart();

        loader.classList.add('fadeOut');
    });
    $('#ddlBussinessGroup').trigger('change');

    $('#btnViewNewSeller').click(function () {
        GetSellerByUserAssignedGroup('');
    });
    $('#btnViewNewGigs').click(function () {
        GetGigByUserAssignedGroup('');
    });
    $('#btnViewNewOrder').click(function () {
        GetNewOrderByUserAssignedGroup('');
    });
    $('#btnViewRemovedSeller').click(function () {
        GetRemovedSellerByUserAssignedGroup('');
    });
    $('#btnViewRemovedGig').click(function () {
        GetRemovedGigByUserAssignedGroup('');
    });

    CheckUserKeywordStatus();
    $('#btnStartInProgress').click(function () {
        if (SelectedGroupIds != '' && SelectedKeywordIds) {
            debugger;
            $("#hdnKeywordIds").val('');
            $("#btnStartInProgress").prop('disabled', true);
            $("#btnStartInProgress").prop("value", "In-Progrss");
            RunFiverrExeForKeywords(SelectedKeywordIds);
        }
    });
});
function GetLastUpdatedDate(SelectedKeywordIds) {
    $.get("/Dashboard/GetLastUpdatedDate", { SelectedKeywordIds: SelectedKeywordIds }, function (data) {
        data = data === "" ? " NA" : moment(new Date(data.split(' ')[0])).format('MMMM DD, YYYY');
        $('#txtDwnldDate').html(data);
    });
}
function PlotSellersAndCustomersInMap(SelectedKeywordIds, startDate, endDate) {

    $.get("/Dashboard/GetAllSellerCustomerCount", { SelectedKeywordIds: SelectedKeywordIds, startDate: startDate, endDate: endDate }, function (data) {
        data = JSON.parse(data);

        CountriesList = $.map(data, function (item) {
            return item.Country;
        });
        lstLatLagData = GetLogAndLat(CountriesList);

        $('#divWorldMap').vectorMap({
            map: 'world_mill',
            scaleColors: ['#C8EEFF', '#0071A4'],
            backgroundColor: '#fff',
            borderColor: '#fff',
            borderOpacity: 0.25,
            borderWidth: 0,
            color: '#e6e6e6',
            hoverOpacity: 0.5,
            normalizeFunction: 'polynomial',
            zoomOnScroll: false,
            selectedColor: '#c9dfaf',
            enableZoom: false,
            hoverColor: false,

            selectedRegions: [],
            regionStyle: {
                initial: {
                    fill: '#e4ecef',
                },
                hover: {
                    "fill-opacity": 0.8,
                    cursor: 'pointer'
                },
                selected: {
                    fill: '#0b97d6'
                },
                selectedHover: {
                }
            },
            markerStyle: {
                initial: {
                    r: 4,
                    'fill': '#fff',
                    'fill-opacity': 1,
                    'stroke': '#000',
                    'stroke-width': 2,
                    'stroke-opacity': 0.4,
                },
            },
            markers: lstLatLagData,
            series: {
                regions: [{
                    values: {},
                    scale: ['#03a9f3', '#02a7f1'],
                    normalizeFunction: 'polynomial',
                }],
            },
            onMarkerTipShow: function (event, label, code) {
                var t = label.text();
                //label.html("<label>" + t + "</label><br><label>Sellers: " + data[parseInt(code)].SellerCount + "</label><br><label>Reviews : " + data[parseInt(code)].CustomerCount + "</label>");
                label.html("<div style='color:black; box-shadow: 0px 0px 5px 0px #0a0a0a; padding: 4px; font-size: 13px; background: white; border-radius: 3px;'> <div><b>" + t + "</b></div><hr style='margin: 0; '><div>Sellers: " + data[parseInt(code)].SellerCount + "</div><div>Reviews : " + data[parseInt(code)].CustomerCount + "</div> </div>");
            },
            onRegionTipShow: function (event, label, code) {
                var t = label.text();
                label.html("<div style='color:black;box-shadow: 0px 0px 5px 0px #0a0a0a; padding: 4px; font-size: 13px; background: white; border-radius: 3px;'>" + t + "</div>");
            },
        });

        SetSelectedRegions(CountriesList);
        isMapPloted = true;
    });

    function findRegion(robj, rname) {
        var code = '';
        $.each(robj, function (key) {
            if (unescape(encodeURIComponent(robj[key].config.name)) === unescape(encodeURIComponent(rname))) {
                code = key;
            }
        });
        return code;
    }
    function GetLogAndLat(lstCountries) {
        var a = [];
        $.each(jvmCountries, function (key) {
            if (lstCountries.indexOf(jvmCountries[key].name) > -1) {
                a.push({ latLng: jvmCountries[key].coords, name: jvmCountries[key].name });
            }
        });
        return a;
    }
    function SetSelectedRegions(lstCountries) {
        var mapObj = $("#divWorldMap").vectorMap("get", "mapObject");
        mapObj.clearSelectedRegions();
        var a = [];
        $.each(lstCountries, function (key) {
            a.push(findRegion(mapObj.regions, lstCountries[key]));
        });

        a = a.filter(function (item) { return item != "" });
        mapObj.setSelectedRegions(a);
    }
}

function PlotNewSellerChart() {
    GetPlotData('New Seller', 'NewSellerChart', 'Seller');
}
function PlotNewGigsChart() {
    GetPlotData('New Gigs', 'NewGigsChart', 'Gigs');
}
function PlotNewOrderChart() {
    GetPlotData('New Order', 'NewOrderChart', 'Order');
}
function PlotRemovedSellerChart() {
    GetPlotData('Removed Seller', 'RemovedSellerChart', 'Seller');
}
function PlotRemovedGigsChart() {
    GetPlotData('Removed Gigs', 'RemovedGigsChart', 'Gigs');
}

function GetPlotData(type, chartId, lebelText) {
    var startDate = $('#reportrange').data('daterangepicker').startDate.format('YYYY-MM-DD');
    var endDate = $('#reportrange').data('daterangepicker').endDate.format('YYYY-MM-DD');
    //var groupId = $('#ddlBussinessGroup').val().join(',');
    var a = $.get("/Dashboard/GetPlotData", { SelectedKeywordIds: SelectedKeywordIds, startDate: startDate, endDate: endDate, type: type }, function (data) {
        data = data === "" ? "" : JSON.parse(data);
        //return data;
        var dataToMap = [];
        if (!GlobalChartObjects[chartId]) {
            var lineChartBox = document.getElementById(chartId);

            if (lineChartBox) {
                var lineCtx = lineChartBox.getContext('2d');
                lineChartBox.height = 150;

                dataToMap = $.map(data, function (item) { return parseInt(item.Count); });
                $('#' + chartId).prev().prev().find('label').text('Total: ' + dataToMap.reduce(function (a, b) { return a + b; }, 0));
                GlobalChartObjects[chartId] = new Chart(lineCtx, {
                    type: 'line',
                    data: {
                        //labels: ['2020-04-18', '2020-04-19'],
                        labels: $.map(data, function (item) {
                            return moment(item.Date).format('MMM DD, YYYY');;
                        }),
                        datasets: [{
                            label: lebelText,
                            backgroundColor: 'rgba(237, 231, 246, 0.5)',
                            borderColor: COLORS['green-500'],
                            pointBackgroundColor: COLORS['green-500'],
                            borderWidth: 2,
                            //data: [76, 23]
                            data: dataToMap
                        }]
                    },

                    options: {
                        elements: {
                            //line: {
                            //    tension: 0
                            //}
                        },
                        legend: {
                            display: false
                        }, scales: {
                            xAxes: [{
                                gridLines: {
                                    color: "rgba(0, 0, 0, 0)",
                                }
                            }],
                            yAxes: [{
                                gridLines: {
                                    color: "rgba(0, 0, 0, 0)",
                                }
                            }]
                        },
                        onClick: chartClickEvent
                    }

                });
            }
        }
        else {
            var chartObj = GlobalChartObjects[chartId];
            dataToMap = $.map(data, function (item) { return parseInt(item.Count); });
            $('#' + chartId).prev().prev().find('label').text('Total: ' + dataToMap.reduce(function (a, b) { return a + b; }, 0));
            chartObj.data.labels = $.map(data, function (item) {
                return moment(item.Date).format('MMM DD, YYYY');;
            });
            chartObj.data.datasets[0].data = dataToMap;
            chartObj.update();
        }
    });
}

function PlotChangesDetectedChart() {

    var startDate = $('#reportrange').data('daterangepicker').startDate.format('YYYY-MM-DD');
    var endDate = $('#reportrange').data('daterangepicker').endDate.format('YYYY-MM-DD');
    //var groupId = $('#ddlBussinessGroup').val().join(',');
    var a = $.get("/Dashboard/GetChangesDetected", { SelectedKeywordIds: SelectedKeywordIds, startDate: startDate, endDate: endDate }, function (data) {
        data = data === "" ? "" : JSON.parse(data);

        if (!GlobalChartObjects['ChangesDetectedChart']) {
            var barChartBoxHorizontal = document.getElementById('ChangesDetectedChart');

            if (barChartBoxHorizontal) {
                var barCtxHorizontal = barChartBoxHorizontal.getContext('2d');
                GlobalChartObjects['ChangesDetectedChart'] = new Chart(barCtxHorizontal, {
                    type: 'bar',
                    data: {
                        //labels: ['Product1', 'Product2', 'Product3', 'Product4', 'Product5', 'Product6', 'Product7', 'Product8', 'Product9', 'Product10'],
                        labels: ['Title', 'Description', 'Cost', 'Delivery Time', 'Offering'],
                        datasets: [{
                            label: 'Changes in data set',
                            backgroundColor: '#0b97d6',//COLORS['deep-purple-500'],
                            borderColor: COLORS['deep-purple-800'],
                            borderWidth: 1,
                            barPercentage: 0.35,
                            data: [data.find(function (item) {
                                return item.ColumnName.toLowerCase() === 'title';
                            }).Count,
                            data.find(function (item) {
                                return item.ColumnName.toLowerCase() === 'description';
                            }).Count,
                            data.find(function (item) {
                                return item.ColumnName.toLowerCase() === 'price';
                            }).Count,
                            data.find(function (item) {
                                return item.ColumnName.toLowerCase() === 'deliverytime';
                            }).Count,
                            data.find(function (item) {
                                return item.ColumnName.toLowerCase() === 'yousave';
                            }).Count]
                        }]
                    },
                    options: {
                        responsive: true,
                        legend: {
                            position: 'top',
                        },
                        onClick: chartClickEvent
                    },
                    onAnimationComplete: function () {

                        var ctx = this.chart.ctx;
                        ctx.font = this.scale.font;
                        ctx.fillStyle = this.scale.textColor
                        ctx.textAlign = "center";
                        ctx.textBaseline = "bottom";

                        this.datasets.forEach(function (dataset) {
                            dataset.bars.forEach(function (bar) {
                                ctx.fillText(bar.value, bar.x, bar.y - 5);
                            });
                        })
                    }
                });
            }
        }
        else {
            var chartObj = GlobalChartObjects['ChangesDetectedChart'];
            dataToMap = $.map(data, function (item) { return parseInt(item.Count); });
            $('#ChangesDetectedChart').prev().prev().find('label').text('Total: ' + dataToMap.reduce(function (a, b) { return a + b; }, 0));
            chartObj.data.labels = $.map(data, function (item) {
                return item.ColumnName;
            });
            chartObj.data.datasets[0].data = dataToMap;
            chartObj.update();
        }
    });
}

function chartClickEvent(event, array) {
    var myLiveChart = GlobalChartObjects[event.currentTarget.id];
    if (myLiveChart === undefined || myLiveChart === null) {
        return;
    }
    if (event === undefined || event === null) {
        return;
    }
    if (array === undefined || array === null) {
        return;
    }
    if (array.length <= 0) {
        return;
    }
    var active = myLiveChart.getElementAtEvent(event);
    if (active === undefined || active === null) {
        return;
    }
    var elementIndex = active[0]._datasetIndex;
    console.log("elementIndex: " + elementIndex + "; array length: " + array.length);
    if (array[elementIndex] === undefined || array[elementIndex] === null) {
        return;
    }

    var chartData = array[elementIndex]['_chart'].config.data;
    var idx = array[elementIndex]['_index'];

    var label = chartData.labels[idx];
    var value = chartData.datasets[elementIndex].data[idx];
    var series = chartData.datasets[elementIndex].label;

    //alert(series + ':' + label + ':' + value);

    // getting details of chart
    if (event.currentTarget.id === 'ChangesDetectedChart') {

        PlotChangeDetectedBarDetails(label);
    }
    else if (event.currentTarget.id === 'NewSellerChart') {
        isChart = true;
        GetSellerByUserAssignedGroup(formatDate(label));
    }
    else if (event.currentTarget.id === 'NewGigsChart') {
        isChart = true;
        GetGigByUserAssignedGroup(formatDate(label));
    }
    else if (event.currentTarget.id === 'NewOrderChart') {
        isChart = true;
        GetNewOrderByUserAssignedGroup(formatDate(label));
    }
    else if (event.currentTarget.id === 'RemovedSellerChart') {
        isChart = true;
        GetRemovedSellerByUserAssignedGroup(formatDate(label));
    }
    else if (event.currentTarget.id === 'RemovedGigsChart') {
        isChart = true;
        GetRemovedGigByUserAssignedGroup(formatDate(label));
    }
}

function PlotChangeDetectedBarDetails(label) {
    loader.classList.remove('fadeOut');
    var startDate = $('#reportrange').data('daterangepicker').startDate.format('YYYY-MM-DD');
    var endDate = $('#reportrange').data('daterangepicker').endDate.format('YYYY-MM-DD');
    label = ChangeDetected[label];
    $('#changeDetectedDetailsLabel b').html(ChangeDetected[label]);
    //var groupId = $('#ddlBussinessGroup').val().join(',');
    var a = $.get("/Dashboard/GetChangesDetectedChartDetail", { SelectedKeywordIds: SelectedKeywordIds, startDate: startDate, endDate: endDate, label: label }, function (data) {
        data = data === "" ? "" : JSON.parse(data);
        if (data === "") {
            toastr.error('Something went wrong.');
        }
        else {
            if ($.fn.DataTable.isDataTable('#tblChangeDetails')) {
                $('#tblChangeDetails').DataTable().clear();
                $('#tblChangeDetails').DataTable().destroy();
            }
            // plot details in popup
            $('#tblChangeDetails tbody').html('');
            if (data.length > 0) {
                for (var i = 0; i < data.length; i++) {
                    $('#tblChangeDetails tbody').append($('<tr><td>' + (i + 1) + '</td><td>' + data[i].KeywordName + '</td><td><a href="' + data[i].NextPageUrl + '" target="_blank">' + data[i].GigId + '</a></td><td>' + data[i].Date + '</td><td>' + data[i].Value + '</td></tr>')[0]);
                }
            }
            else {
                $('#tblChangeDetails tbody').html('<tr><td colspan="5">No Data Found.</td></tr>');
            }
            $('#tblChangeDetails').DataTable({
                "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]]
            });
            $('#changeDetectedDetailsPopup').modal('show');

            loader.classList.add('fadeOut');
        }
    });
}

function GetSellerByUserAssignedGroup(label) {
    var startDate = '';
    var endDate = '';
    if (isChart && label != '') {
        startDate = label;
        endDate = label;
    }
    else {
        startDate = $('#reportrange').data('daterangepicker').startDate.format('YYYY-MM-DD');
        endDate = $('#reportrange').data('daterangepicker').endDate.format('YYYY-MM-DD');
    }

    var SelectedKeywordIds = $('#ddlGroupKeywords').val().join(',');
    loader.classList.remove('fadeOut');
    $.get("/Dashboard/GetSellerByUserAssignedGroup", { KeywordIds: SelectedKeywordIds, startDate: startDate, endDate: endDate }, function (data) {
        data = data === "" ? "" : JSON.parse(data);
        if (data === "") {
            toastr.error('Something went wrong.');
        }
        else {

            //PlotNewSellerChart();
            if ($.fn.DataTable.isDataTable('#tblNewSeller')) {
                $('#tblNewSeller').DataTable().clear();
                $('#tblNewSeller').DataTable().destroy();
            }
            // plot details in popup 
            $('#tblNewSeller tbody').html('');
            if (data.length > 0) {
                for (var i = 0; i < data.length; i++) {
                    $('#tblNewSeller tbody').append($('<tr><td>' + data[i].Date.split(' ')[0] + '</td><td>' + data[i].KeywordName + '</td><td><a href="' + data[i].NextPageUrl + '" target="_blank">' + data[i].GigID + '</a></td><td><a href="' + data[i].ProfileURL + '" target="_blank">' + data[i].SellerName + '</a></td><td>' + data[i].Title + '</td><td>' + data[i].SellerFrom + '</td><td>' + data[i].SellerMemberSince + '</td><td>' + data[i].AverageResponseTime + '</td><td>' + data[i].SellerLevel + '</td></tr>')[0]);
                }
            }
            else {
                $('#tblNewSeller tbody').html('<tr><td colspan="9">No Data Found.</td></tr>');
            }
            // $('#tblNewSeller').destroy();
            //$('#tblNewSeller').DataTable({ scrollX: true });
            $('#tblNewSeller').DataTable();
            $('#viewNewSellerPopup').modal('show');
            loader.classList.add('fadeOut');
        }
    });
}

function GetGigByUserAssignedGroup(label) {
    var startDate = '';
    var endDate = '';
    if (isChart && label != '') {
        startDate = label;
        endDate = label;
    }
    else {
        startDate = $('#reportrange').data('daterangepicker').startDate.format('YYYY-MM-DD');
        endDate = $('#reportrange').data('daterangepicker').endDate.format('YYYY-MM-DD');
    }
    var SelectedKeywordIds = $('#ddlGroupKeywords').val().join(',');
    loader.classList.remove('fadeOut');
    $.get("/Dashboard/GetGigByUserAssignedGroup", { KeywordIds: SelectedKeywordIds, startDate: startDate, endDate: endDate }, function (data) {
        data = data === "" ? "" : JSON.parse(data);
        if (data === "") {
            toastr.error('Something went wrong.');
        }
        else {
            if ($.fn.DataTable.isDataTable('#tblNewGig')) {
                $('#tblNewGig').DataTable().clear();
                $('#tblNewGig').DataTable().destroy();
            }
            // plot details in popup 
            $('#tblNewGig tbody').html('');
            if (data.length > 0) {
                for (var i = 0; i < data.length; i++) {
                    $('#tblNewGig tbody').append($('<tr><td>' + data[i].Date.split(' ')[0] + '</td><td>' + data[i].KeywordName + '</td><td><a href="' + data[i].NextPageUrl + '" target="_blank">' + data[i].GigID + '</a></td><td>' + data[i].Title + '</td><td>' + data[i].SellerName + '</td><td>' + data[i].StartingPrice + '</td><td>' + data[i].Tool + '</td></tr>')[0]);
                }
            }
            else {
                $('#tblNewGig tbody').html('<tr><td colspan="7">No Data Found.</td></tr>');
            }
            //$('#tblNewGig').DataTable({ scrollX: true });
            $('#tblNewGig').DataTable();
            $('#viewNewGigPopup').modal('show');
            loader.classList.add('fadeOut');
        }
    });
}

function GetNewOrderByUserAssignedGroup(label) {
    loader.classList.remove('fadeOut');
    var startDate = '';
    var endDate = '';
    if (isChart && label != '') {
        startDate = label;
        endDate = label;
    }
    else {
        startDate = $('#reportrange').data('daterangepicker').startDate.format('YYYY-MM-DD');
        endDate = $('#reportrange').data('daterangepicker').endDate.format('YYYY-MM-DD');
    }
    var SelectedKeywordIds = $('#ddlGroupKeywords').val().join(',');
    $.get("/Dashboard/GetNewOrderByUserAssignedGroup", { KeywordIds: SelectedKeywordIds, startDate: startDate, endDate: endDate }, function (data) {
        data = data === "" ? "" : JSON.parse(data);
        if (data === "") {
            toastr.error('Something went wrong.');
        }
        else {
            if ($.fn.DataTable.isDataTable('#tblNewOrder')) {
                $('#tblNewOrder').DataTable().clear();
                $('#tblNewOrder').DataTable().destroy();
            }
            // plot details in popup 
            $('#tblNewOrder tbody').html('');
            if (data.length > 0) {
                for (var i = 0; i < data.length; i++) {
                    $('#tblNewOrder tbody').append($('<tr><td>' + data[i].Date.split(' ')[0] + '</td><td>' + data[i].KeywordName + '</td><td><a href="' + data[i].NextPageUrl + '" target="_blank">' + data[i].GigID + '</a></td><td>' + data[i].CustomerName + '</td><td>' + data[i].CustomerFrom + '</td><td>' + data[i].ReviewRating + '</td></tr>')[0]);
                }
            }
            else {
                $('#tblNewOrder tbody').html('<tr><td colspan="6">No Data Found.</td></tr>');
            }
            //$('#tblNewOrder').DataTable({ scrollX: true });
            $('#tblNewOrder').DataTable();
            $('#viewNewOrderPopup').modal('show');
            loader.classList.add('fadeOut');
        }
    });
}

function GetRemovedSellerByUserAssignedGroup(label) {

    var startDate = '';
    var endDate = '';
    if (isChart && label != '') {
        startDate = label;
        endDate = label;
    }
    else {
        startDate = $('#reportrange').data('daterangepicker').startDate.format('YYYY-MM-DD');
        endDate = $('#reportrange').data('daterangepicker').endDate.format('YYYY-MM-DD');
    }
    var SelectedKeywordIds = $('#ddlGroupKeywords').val().join(',');
    loader.classList.remove('fadeOut');
    $.get("/Dashboard/GetRemovedSellerByUserAssignedGroup", { KeywordIds: SelectedKeywordIds, startDate: startDate, endDate: endDate }, function (data) {
        data = data === "" ? "" : JSON.parse(data);
        if (data === "") {
            toastr.error('Something went wrong.');
        }
        else {
            if ($.fn.DataTable.isDataTable('#tblRemovedSeller')) {
                $('#tblRemovedSeller').DataTable().clear();
                $('#tblRemovedSeller').DataTable().destroy();
            }
            // plot details in popup 
            $('#tblRemovedSeller tbody').html('');
            if (data.length > 0) {
                for (var i = 0; i < data.length; i++) {
                    $('#tblRemovedSeller tbody').append($('<tr><td>' + data[i].Date.split(' ')[0] + '</td><td>' + data[i].KeywordName + '</td><td>' + data[i].GigID + '</td><td>' + data[i].SellerName + '</td><td>' + data[i].Title + '</td><td>' + data[i].SellerFrom + '</td></tr>')[0]);
                }
            }
            else {
                $('#tblRemovedSeller tbody').html('<tr><td colspan="6">No Data Found.</td></tr>');
            }
            $('#tblRemovedSeller').DataTable();
            $('#viewRemovedSellerPopup').modal('show');
            loader.classList.add('fadeOut');
        }
    });
}

function GetRemovedGigByUserAssignedGroup(label) {

    var startDate = '';
    var endDate = '';
    if (isChart && label != '') {
        startDate = label;
        endDate = label;
    }
    else {
        startDate = $('#reportrange').data('daterangepicker').startDate.format('YYYY-MM-DD');
        endDate = $('#reportrange').data('daterangepicker').endDate.format('YYYY-MM-DD');
    }
    var SelectedKeywordIds = $('#ddlGroupKeywords').val().join(',');
    loader.classList.remove('fadeOut');
    $.get("/Dashboard/GetRemovedGigByUserAssignedGroup", { KeywordIds: SelectedKeywordIds, startDate: startDate, endDate: endDate }, function (data) {
        data = data === "" ? "" : JSON.parse(data);
        if (data === "") {
            toastr.error('Something went wrong.');
        }
        else {
            if ($.fn.DataTable.isDataTable('#tblRemovedGig')) {
                $('#tblRemovedGig').DataTable().clear();
                $('#tblRemovedGig').DataTable().destroy();
            }
            // plot details in popup 
            $('#tblRemovedGig tbody').html('');
            if (data.length > 0) {
                for (var i = 0; i < data.length; i++) {
                    $('#tblRemovedGig tbody').append($('<tr><td>' + data[i].Date.split(' ')[0] + '</td><td>' + data[i].KeywordName + '</td><td><a href="' + data[i].NextPageUrl + '" target="_blank">' + data[i].GigID + '</a></td></tr>')[0]);
                }
            }
            else {
                $('#tblRemovedGig tbody').html('<tr><td colspan="3">No Data Found.</td></tr>');
            }

            $('#tblRemovedGig').DataTable();
            $('#viewRemovedGigPopup').modal('show');
            loader.classList.add('fadeOut');
        }
    });
}

function formatDate(date) {
    var d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2)
        month = '0' + month;
    if (day.length < 2)
        day = '0' + day;

    return [year, month, day].join('-');
}

function CheckUserKeywordStatus() {
    var isKeywordStatus = $('#hdnIsKeywordRunning').val();
    if (isKeywordStatus === "true") {
        $("#btnStartInProgress").prop('disabled', true);
        $("#btnStartInProgress").prop("value", "In-Progrss");
        if (keywordID === "") {
            keywordID = $("#hdnKeywordIds").val();
        }
        if (keywordID !== "") {

            GetRollingData(keywordID);
        }
    }
}
function RunFiverrExeForKeywords(keywordID) {
    var b = $.get("/Status/RunFiverrExeForKeywords", { keywordIds: keywordID }, function () {
        isKeywordRunning = true;
        //GetRollingData(keywordID);
    });
}