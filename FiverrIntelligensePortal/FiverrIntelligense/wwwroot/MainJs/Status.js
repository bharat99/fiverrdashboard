﻿var GlobalChartRolling = null;
var CountriesList = [];
var lstLatLagData = [];
var isMapPloted = false;
var keywordID = '';
var isKeywordCompleted = false;
$(document).ready(function () {

    GetLastUpdatedDate('0');
    GetKeywordProgressCount();
    PlotKeywordStatusChart();
    //PlotProgressChart();

    CheckUserKeywordStatus();
    $('#btnStartInProgress').on('click', function () {
        $("#hdnKeywordIds").val('');
        keywordID = '';
        $("#btnStartInProgress").prop('disabled', true);
        $("#btnStartInProgress").prop("value", "In-Progrss");
        GetKeywordByGroup();
    });
});
function GetLastUpdatedDate(groupId) {
    $.get("/Dashboard/GetLastUpdatedDate", { groupId: groupId }, function (data) {
        data = data === "" ? " NA" : moment(new Date(data.split(' ')[0])).format('MMMM DD, YYYY');
        $('#txtDwnldDate').html(data);
    });
}
function PlotKeywordStatusChart() {

    $.get("/Status/GetKeywodsRunningStatus", { keywords: keywordID }, function (data) {
        if (data === "" || data == '[]') {
            data = [{ GroupID: 0, Total: 0, Running: 0, Completed: 0, Stoped: 0 }];
        }
        else {
            data = JSON.parse(data);
            $('#keywordStatus span:eq(1)').html(data.map(function (a) { return a.Completed }).reduce(function (a, b) { return a + b }, 0) + '/' + data.map(function (a) { return a.Total }).reduce(function (a, b) { return a + b }, 0) + ' completed');
            var chart = new Chart('keywordBarChart', {
                type: 'pie',
                data: {
                    labels: ['Progress', 'Completed', 'Stoped'],
                    datasets: [{
                        data: [data[0].Running, data[0].Completed, data[0].Stoped],
                        backgroundColor: ['#0c7cd5', '#D9B44A', 'red'],
                        borderColor: 'white',
                        borderWidth: 2,
                    }]
                },
                options: {
                    pieceLabel: {
                        render: 'value' //show values
                    }
                }
            });
        }
    });

    setTimeout(PlotKeywordStatusChart, 10000);
}

function GetKeywordProgressCount() {

    $.get("/Status/GetKeywordProgressCount", { keywords: keywordID }, function (data) {
        if (data === "" || data === '[]') {
            data = [{ GroupID: 0, TotalGigs: 0, CompletedGigs: 0 }];
        }
        else {
            data = JSON.parse(data);
            $('#spnTotalGigs').html(data[0].TotalGigs);
            //$('#animationProgress')[0].value = parseInt(data[0].CompletedGigs) / parseInt(data[0].TotalGigs);
            var current_progress = parseFloat(parseFloat(parseInt(data[0].CompletedGigs) / parseInt(data[0].TotalGigs)) * 100).toFixed(2);
            $("#animationProgress")
                .css("width", current_progress + "%")
                .attr("aria-valuenow", current_progress)
                .text(current_progress + "% Complete");
            $('#spnProgressCount').html(data[0].CompletedGigs + '/' + data[0].TotalGigs);
        }
    });

    setTimeout(GetKeywordProgressCount, 10000);
}

function CheckUserKeywordStatus() {
    var isKeywordStatus = $('#hdnIsKeywordRunning').val();
    if (isKeywordStatus === "true") {
        $("#btnStartInProgress").prop('disabled', true);
        $("#btnStartInProgress").prop("value", "In-Progrss");
        if (keywordID === "") {
            keywordID = $("#hdnKeywordIds").val();
        }
        if (keywordID !== "") {
            GetRollingData(keywordID);
        }
    }
}
function GetKeywordByGroup() {
    var a = $.get("/Status/GetKeywordByGroupOfUser", function (data) {
        data = data === "" ? "" : JSON.parse(data);
        $.each(data, function (index, item) {
            keywordID += item.KeywordID + ",";
        });
        RunFiverrExeForKeywords(keywordID);
    });
}
function RunFiverrExeForKeywords(keywordID) {
    var b = $.get("/Status/RunFiverrExeForKeywords", { keywordIds: keywordID }, function () {
        //isKeywordRunning = true;
        GetRollingData(keywordID);
    });
}
function GetRollingData(keywordID) {
    if (keywordID != '') {
        var c = $.get("/Status/GetRollingData", { keywordIds: keywordID }, function (data) {
            data = data === "" ? "" : JSON.parse(data);
            let obj = data.find(o => o.Status === 'Running');
            if (!obj) {
                $("#btnStartInProgress").prop('disabled', false);
                $("#btnStartInProgress").prop("value", "Start");
                $("#hdnKeywordIds").val('');
                isKeywordCompleted = true;
                //PlotProgressChart(data);
                ProgressChartPlotNew(data);
            }
            else {
                //PlotProgressChart(data);
                ProgressChartPlotNew(data);
            }
        });
    }
}

function ProgressChartPlotNew(data) {
    if (data.length > 0) {
        $('#KeywordsProgressBars').html('');
        $.each(data, function (index, item) {
            var progressPercentage = parseFloat(parseFloat(parseInt(item.TotalServices) / parseInt(item.ServicesAvailable)) * 100).toFixed(2);
            $('#KeywordsProgressBars').append($('<tr> <th>' + item.KeywordName + '</th> <td> <div class="progress"> <div id="" class="progress-bar progress-bar-success progress-bar-striped active" role="progressbar" aria-valuenow="' + progressPercentage + '" aria-valuemin="0" aria-valuemax="100" style="width: ' + progressPercentage + '%"> <span id="current-progress">' + item.TotalServices + '</span> </div> </div> </td> <td>' + item.ServicesAvailable + '</td> </tr>')[0]);
        });
    }
    else {
        $('#KeywordsProgressBars').html('<tr><td colspan="3">No Data Found.</td></tr>');
    }
    if (!isKeywordCompleted) {
        setTimeout(GetRollingData, 10000, keywordID);
    }
}

function PlotProgressChart(dataRolling) {
    var keywordNames = [];
    var totalServices = [];
    var servicesAvailable = [];
    $.each(dataRolling, function (index, item) {
        keywordNames.push(item.KeywordName);
        totalServices.push(item.TotalServices);
        servicesAvailable.push(item.ServicesAvailable);
    });
    if (keywordNames.length > 0 && totalServices.length > 0) {

        var chartData = {
            labels: keywordNames,// ["January", "February", "March", "April", "May", "June"],
            datasets: [
                {
                    label: 'CompletedServices',
                    backgroundColor: "#3399ff",
                    borderColor: "#3399ff",
                    borderWidth: 1,
                    data: totalServices//[60, 80, 81, 56, 55, 40]

                },
                {
                    label: 'ServicesAvailable',
                    backgroundColor: "#0000ff",
                    borderColor: "#0000ff",
                    borderWidth: 1,
                    data: servicesAvailable//[60, 80, 81, 56, 55, 40]  
                }
            ]
        };
        if (!GlobalChartRolling) {
            var ctx = document.getElementById("progressChart").getContext("2d");
            GlobalChartRolling = new Chart(ctx, {
                type: 'horizontalBar',
                data: chartData,
                options: {
                    "hover": {
                        "animationDuration": 0
                    },
                    "animation": {
                        "duration": 1,
                        "onComplete": function () {
                            var chartInstance = this.chart,
                                ctx = chartInstance.ctx;

                            ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontSize, Chart.defaults.global.defaultFontStyle, Chart.defaults.global.defaultFontFamily);
                            ctx.textAlign = 'center';
                            ctx.textBaseline = 'bottom';

                            this.data.datasets.forEach(function (dataset, i) {
                                var meta = chartInstance.controller.getDatasetMeta(i);
                                meta.data.forEach(function (bar, index) {
                                    var data = dataset.data[index];
                                    ctx.fillText(data, bar._model.x, bar._model.y - 5);
                                });
                            });
                        }
                    },
                    legend: {
                        "display": true
                    },
                    tooltips: {
                        "enabled": true
                    }
                }
            });
        }
        else {
            var chartObj = GlobalChartRolling;
            chartObj.data.labels = keywordNames;
            chartObj.data.datasets[0].data = totalServices;
            chartObj.data.datasets[1].data = servicesAvailable;
            chartObj.update();
        }
    }
    if (!isKeywordCompleted) {
        setTimeout(GetRollingData, 10000, keywordID);
    }
}

