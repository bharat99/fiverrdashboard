﻿
var IDLE_TIMEOUT = 7200; //seconds
var _idleSecondsTimer = null;
var _idleSecondsCounter = 0;

document.onclick = function () {

    _idleSecondsCounter = 0;
};

document.onmousemove = function () {

    _idleSecondsCounter = 0;
};

document.onkeypress = function () {

    _idleSecondsCounter = 0;
};
_idleSecondsCounter = window.setInterval(CheckIdleTime, 1000);

function CheckIdleTime() {
    _idleSecondsCounter++;
    if (_idleSecondsCounter >= IDLE_TIMEOUT) {
        window.clearInterval(_idleSecondsCounter);
        alert("Your Session Has Expired Please Login Again!");
        document.location.href = "/login.aspx";
        var Url = window.location.href;
        if (Url.indexOf('/HRM/') > -1)
        {
            Url = Url.replace('/HRM','');
            document.location.href = Url;
        }
        else
        {
            document.location.href = "/login.aspx";
        }
        
    }
    return;
}

function ResetSession() {
    _idleSecondsCounter = 0;


}

function AddInviteUserfront(cuid, usertypeid) {

    AddInviteUser(cuid, usertypeid);
}