﻿
function OnLogin(TID, MID, BID) {

    var checksutolog = "nitin";
    setCookieforauto("forauto", checksutolog, 15);
    if (document.getElementById("TxtUserName").value == "") {
        alert("Please enter your email address");
        return false;
    }
    var email = "";
    if (document.getElementById("TxtUserName").value != "") {
        var validEmail = validateMultipleEmails($('#TxtUserName')[0].value);
        if (validEmail == false) {
            alert("Please enter a valid email address");
            return false;
        }
        else {
            email = $('#TxtUserName')[0].value;
        }
    }
    if (document.getElementById("TxtPassword").value == "" || document.getElementById("TxtPassword").value == "******") {
        alert("Please enter the password");
        return false;
    }
    var pas = document.getElementById("TxtPassword").value;
    $.ajax({
        type: "GET",
        contentType: "application/json; charset=utf-8",
        url: "/DataTracker.svc/GetUser?Email=" + escape(email) + "&pass=" + escape(pas) + "",
        // dataType: "json",
        success: function (data) {
            if (data.d != null) {
                var res = data.d;
                // var newres = res.split("~");
                // if (newres[0] == "1") {
                if (res != 0) {
                    document.getElementById("TxtUserName").value = "";
                    document.getElementById("TxtUserName").value = "";

                    if (TID != null && TID != "") {
                        //window.location.href = "/Board.aspx";
                        window.location.href = "/Dashbord.aspx?tid=" + TID;
                    }
                    if (MID != null && MID != "") {
                        window.location.href = "/MaricoPriceReport.aspx?mid=" + MID;
                    }
                    if (BID != null && BID != "") {

                        window.location.href = "/board.aspx?" + BID;
                    }
                    else {
                      
                        if (res != 16) {  // user type id 16 is used for client.
                             window.location.href = "/AdminDashboard.aspx";
                            //window.location.href = "/board.aspx?380";
                            //window.location.href = "board.aspx?396#";
                           // window.location.href = "/Dashbord.aspx?2";
                         }
                        else {
                            window.location.href = "/Dashbord.aspx?1";
                         
                        }
                    }

                }
                else {
                    $('#DivLoginstatus')[0].style.display = "block";
                    $('#DivLoginstatus')[0].style.color = "red";
                    $('#DivLoginstatus')[0].innerHTML = "User name or password invalid!";
                    document.getElementById("TxtPassword").value = "";
                    $('#DivLoginstatus').fadeOut(12000);
                }

            }

            else {

                $('#DivLoginstatus')[0].style.display = "block";
                $('#DivLoginstatus')[0].style.color = "red";
                $('#DivLoginstatus')[0].innerHTML = "User name or password invalid!"
                document.getElementById("TxtPassword").value = "";
                $('#DivLoginstatus').fadeOut(12000);
                return false;
            }
        }
    });
}


function validateMultipleEmails(value) {
    var valiemail;
    var result = value.split(",");
    for (var i = 0; i < result.length; i++) {
        var email = result[i];
        var EmailID = "";
        if (result[i].match("<")) {
            var index = result[i].indexOf("<") + 1;
            var index2 = result[i].indexOf(">");
            EmailID = result[i].substring(index, index2);
            EmailID = EmailID.trim();
        }
        else {
            EmailID = email.trim();
        }
        if (!validateEmail(EmailID)) {
            valiemail = false;
            return false;
        }
        else {
            valiemail = true;
        }
    }
    return (valiemail)
}
function validateEmail(field) {
    var regex = /^.+@.+\..{2,3}$/;
    return (regex.test(field)) ? true : false;
}


function deleteAllCookiesAtLogOut() {
    var uuname = getCookieforauto("forauto");
    if (uuname != null && uuname != "") {
        document.cookie = 'forauto' + '=; Path=/; Expires=Thu, 01 Jan 1970 00:00:01 GMT;';
    }

}

function deleteAllCookiesAtUncheck() {

    if ($("#remember_me").is(':checked')) { }
    else {
        var uuname = getCookieforauto("forauto");
        if (uuname != null && uuname != "") {
            document.cookie = 'forauto' + '=; Path=/; Expires=Thu, 01 Jan 1970 00:00:01 GMT;';
        }
        var autoname = getCookie("username");
        if (autoname != null && autoname != "") {
            document.cookie = 'username' + '=; Path=/; Expires=Thu, 01 Jan 1970 00:00:01 GMT;';
        }
    }
}
function setCookieforauto(c_name, value, exdays) {

    var exdate = new Date();
    exdate.setDate(exdate.getDate() + exdays);
    var c_value = escape(value) + ((exdays == null) ? "" : "; expires=" + exdate.toUTCString());
    document.cookie = c_name + "=" + c_value;
}

function getCookieforauto(c_name) {

    var c_value = document.cookie;
    var c_start = c_value.indexOf(" " + c_name + "=");
    if (c_start == -1) {
        c_start = c_value.indexOf(c_name + "=");
    }
    if (c_start == -1) {
        c_value = null;
    }
    else {
        c_start = c_value.indexOf("=", c_start) + 1;
        var c_end = c_value.indexOf(";", c_start);
        if (c_end == -1) {
            c_end = c_value.length;
        }
        c_value = unescape(c_value.substring(c_start, c_end));
    }
    return c_value;
}

function setCookie(c_name, value, exdays) {

    var exdate = new Date();
    exdate.setDate(exdate.getDate() + exdays);
    var c_value = escape(value) + ((exdays == null) ? "" : "; expires=" + exdate.toUTCString());
    document.cookie = c_name + "=" + c_value;
}

function getCookie(c_name) {

    var c_value = document.cookie;
    var c_start = c_value.indexOf(" " + c_name + "=");
    if (c_start == -1) {
        c_start = c_value.indexOf(c_name + "=");
    }
    if (c_start == -1) {
        c_value = null;
    }
    else {
        c_start = c_value.indexOf("=", c_start) + 1;
        var c_end = c_value.indexOf(";", c_start);
        if (c_end == -1) {
            c_end = c_value.length;
        }
        c_value = unescape(c_value.substring(c_start, c_end));
    }
    return c_value;
}
function checkCookie() {

    if ($("#remember_me").is(':checked')) {

        var unameval = $("#TxtUserName").val();
        var passval = $("#TxtPassword").val();
        var bothunamepassword = unameval + "~" + passval;
        if (bothunamepassword != null && bothunamepassword != "") {
            setCookie("username", bothunamepassword, 15);
        }
    }
}

function deleteAllCookiesAtLogOut() {
    var uuname = getCookieforauto("forauto");
    if (uuname != null && uuname != "") {
        document.cookie = 'forauto' + '=; Path=/; Expires=Thu, 01 Jan 1970 00:00:01 GMT;';
    }
    ClearHistory();
}
function ClearHistory() {
    var backlen = history.length;
    history.go(-backlen);
    window.location.href = "/Login.aspx"
}
function deleteAllCookiesAtUncheck() {

    if ($("#remember_me").is(':checked')) { }
    else {
        var uuname = getCookieforauto("forauto");
        if (uuname != null && uuname != "") {
            document.cookie = 'forauto' + '=; Path=/; Expires=Thu, 01 Jan 1970 00:00:01 GMT;';
        }
        var autoname = getCookie("username");
        if (autoname != null && autoname != "") {
            document.cookie = 'username' + '=; Path=/; Expires=Thu, 01 Jan 1970 00:00:01 GMT;';
        }
    }
}
function setCookieforauto(c_name, value, exdays) {

    var exdate = new Date();
    exdate.setDate(exdate.getDate() + exdays);
    var c_value = escape(value) + ((exdays == null) ? "" : "; expires=" + exdate.toUTCString());
    document.cookie = c_name + "=" + c_value;
}

function getCookieforauto(c_name) {

    var c_value = document.cookie;
    var c_start = c_value.indexOf(" " + c_name + "=");
    if (c_start == -1) {
        c_start = c_value.indexOf(c_name + "=");
    }
    if (c_start == -1) {
        c_value = null;
    }
    else {
        c_start = c_value.indexOf("=", c_start) + 1;
        var c_end = c_value.indexOf(";", c_start);
        if (c_end == -1) {
            c_end = c_value.length;
        }
        c_value = unescape(c_value.substring(c_start, c_end));
    }
    return c_value;
}

function setCookie(c_name, value, exdays) {

    var exdate = new Date();
    exdate.setDate(exdate.getDate() + exdays);
    var c_value = escape(value) + ((exdays == null) ? "" : "; expires=" + exdate.toUTCString());
    document.cookie = c_name + "=" + c_value;
}

function getCookie(c_name) {

    var c_value = document.cookie;
    var c_start = c_value.indexOf(" " + c_name + "=");
    if (c_start == -1) {
        c_start = c_value.indexOf(c_name + "=");
    }
    if (c_start == -1) {
        c_value = null;
    }
    else {
        c_start = c_value.indexOf("=", c_start) + 1;
        var c_end = c_value.indexOf(";", c_start);
        if (c_end == -1) {
            c_end = c_value.length;
        }
        c_value = unescape(c_value.substring(c_start, c_end));
    }
    return c_value;
}
function checkCookie() {

    if ($("#remember_me").is(':checked')) {

        var unameval = $("#TxtUserName").val();
        var passval = $("#TxtPassword").val();
        var bothunamepassword = unameval + "~" + passval;
        if (bothunamepassword != null && bothunamepassword != "") {
            setCookie("username", bothunamepassword, 15);
        }
    }
}

