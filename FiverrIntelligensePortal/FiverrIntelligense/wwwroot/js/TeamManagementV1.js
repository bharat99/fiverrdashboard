﻿var allus = new Array();
function BindBootTeamList() {
    $("#AllTeamDropDown")[0].innerHTML = string.empty;
    if ($("#AllTeamDropDown").data("kendoWindow") != null) {
        $("#AllTeamDropDown").data().kendoGrid.destroy();
        $("#AllTeamDropDown").empty();
    }
    var dataSourceDetail = string.empty;
    dataSourceDetail = new kendo.data.DataSource({
        // pageSize: 25,
        // serverPaging: true,
        schema: {
            data: "d",
            total: function (d) {
                if (d.d.length > 0)
                    return d.d[0].TotalCount
            },
            model: {
                id: "ID",
                fields: {
                    ID: { editable: false, type: "number" },
                    Teamid: { editable: false, type: "string" },
                    TeamName: { editable: false, type: "string" }
                }
            }
        },

        transport: {
            read: function (options) {
                $.ajax({
                    type: "GET",
                    url: "/DataTracker.svc/BindTeamListdropdownG",
                    dataType: "json",
                    data: {
                        Page: options.data.page,
                        PageSize: options.data.pageSize,
                        skip: options.data.skip,
                        take: options.data.take
                    },

                    success: function (result) {
                        options.success(result);

                    }
                });
            }
        }

    });
    if ($("#AllTeamDropDown").length > 0)
    { $("#AllTeamDropDown")[0].innerHTML = ""; }
    var element = $("#AllTeamDropDown").kendoGrid({
        dataSource: dataSourceDetail,
        dataBound: function () {
        },
        columns: [
                            {
                                field: "TeamName",
                                title: "Team Name"
                            }
        ],
        editable: false,
        sortable: true,
        selectable: true

    });
    $("#AllTeamDropDown .k-grid-content").css("max-height", $(window).height() - 205);

}

function BindBootTeamListforusersteam() {
    $.ajax({
        type: "GET",
        contentType: "application/json; charset=utf-8",
        url: "/DataTracker.svc/BindTeamListdropdown",
        dataType: "json",
        success: function (data) {
            $.each(data.d, function (index, item) {
                $("#SelForTeamsofUser").append($("<option></option>").val(item.Teamid).html(item.TeamName));
            });
        },
        error: function (result) {

        }
    });
    setTimeout(function () { GetAllMemberforTeam(); }, 300);
}

function SaveNewTeam() {
    var TeamName = $("#TeamNametxt").val();
    if (!TeamName.equal(string.empty)) {
        $.ajax({
            type: "GET",
            contentType: "application/json; charset=utf-8",
            url: "/DataTracker.svc/InsertNewTeam?Teamname=" + TeamName + "",
            dataType: "json",
            success: function (data) {
                if (data.d.equal(0))
                {
                    $("#Result2")[0].style.display = "block";
                    setTimeout(function () { closesavemsg("Result2"); }, 3000);
                    $("#TeamNametxt").val("");
                    removeOptions(document.getElementById("AllTeamDropDown"));
                    setTimeout(function () { BindBootTeamList(); }, 300);
                }
            },
            error: function (result) {

            }
        });
    }
    else
        alert("Please Enter Team Name");
}

function DeleteSelectedTeam() {

    var selectedTId = $("#AllTeamDropDown").val();
    if (!selectedTId.equal(string.empty)) {
        $.ajax({
            type: "GET",
            contentType: "application/json; charset=utf-8",
            url: "/DataTracker.svc/DeleteSelectedTeam?Tid=" + selectedTId + "",
            dataType: "json",
            success: function (data) {
                if (data.d.equal(0)) {

                    $("#Result2")[0].style.display = "block";
                    setTimeout(function () { closesavemsg("Result2"); }, 3000);
                    removeOptions(document.getElementById("AllTeamDropDown"));
                    BindBootTeamList();
                }
            },
            error: function (result) {
            }
        });
    }
}

function rememberTabSelection(tabPaneSelector, useHash) {
    var key = "selectedTabFor" + tabPaneSelector;
    if (get(key))
        $(tabPaneSelector).find("a[href=" + get(key) + "]").tab("show");

    $(tabPaneSelector).on("click", "a[data-toggle]", function (event) {
        set(key, this.getAttribute("href"));
    });

    function get(key) {
        return useHash ? location.hash : localStorage.getItem(key);
    }

    function set(key, value) {
        if (useHash)
            location.hash = value;
        else
            localStorage.setItem(key, value);
    }
}

function createDropdown(id, coname) {
    if (coname.equal("SelectAllNames")) {
        $(id).dropdownchecklist({
            icon: {},
            height: 250,
            firstItemChecksAll: true,
            emptyText: "filter by status",
            textFormatFunction: function (data) {
                var selectedOptions = data.filter(":selected");
                var countOfSelected = selectedOptions.size();
                switch (countOfSelected) {
                    case 0:
                        return "filter by status...";
                    case 1:
                        return selectedOptions.text();
                    case data.size():
                        return "All Selected";
                    default:
                        return countOfSelected + " Item Checked";
                }
            }
                , onItemClick: function (checkbox, selector) {
                    var justChecked = checkbox.prop("checked");
                    if (justChecked && checkbox[0].value == "Select All") {
                        options.length = 0;
                        for (i = 1; i < selector.options.length; i++) {
                            options.push(selector.options[i].value.trim());
                        }
                    }
                    else if (!justChecked && checkbox[0].value.equal("Select All")) {
                        options.length = 0;
                    }
                    else if (justChecked) {
                        options.push(checkbox[0].value.trim());
                    }
                    else {
                        options.splice($.inArray(checkbox[0].value.trim(), options), 1);
                    }

                }
        });
    }
}

var options = new Array();
function BindAlluserwithEmail() {
    $.ajax({
        type: "GET",
        contentType: "application/json; charset=utf-8",
        url: "/DataTracker.svc/getallUserNameWithteamId?fromwhere=" + "t" + "",
        dataType: "json",
        success: function (data) {
            if (data.d != null)
            {
                $.each(data.d, function (index, item) {
                    $("#SelectAllNames").append($("<option></option>").val(item.TIdWitdUserID).html(item.UserName));
                });
            }
        },
        error: function (result) {
        }
    });
}

function SelectteamOnNameChange() {
    var tmid = $("#SelectAllNames").val();
    var otid = tmid.split("~");
    $("#SelForTeamsofUser").val(otid[0]);
}

function SelectManagerOnNameChange() {
    var tlmid = $("#SelectNameforManager").val();
    $("#SelectForAllManagers").val(tlmid);
}

function BindAllManagers() {
    $.ajax({
        type: "GET",
        contentType: "application/json; charset=utf-8",
        url: "/DataTracker.svc/getallUserNameWithteamId?fromwhere=" + "allm" + "",
        dataType: "json",
        success: function (data) {
            $.each(data.d, function (index, item) {
                $("#SelectForAllManagers").append($("<option></option>").val(item.Teamid).html(item.UserName));
            });
        },
        error: function (result) {
        }
    });

    setTimeout(function () { GetAllMemberforTLS(); }, 300);
}


function closesavemsg(id) {
    $("#" + id + "")[0].style.display = "none";
}

function removeOptions(selectbox) {
    var i;
    for (i = selectbox.options.length - 1; i >= 0; i--) {
        if (i.equal(0)) { }
        else
            selectbox.remove(i);
    }
}

function AutoAllUserListT() {
    var dictionary = new Array();
    var msg = "Select or enter user name...";
    $.ajax({
        type: "GET",
        url: "/DataTracker.svc/getallUserNameWithteamId?fromwhere=" + "t" + "",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            var res = data.d;
            if (res != null) {
                for (var n = 0; n < data.d.length; n++) {
                    var value = data.d[n].UserName;
                    dictionary.push(value);
                }
                if ($("#SelectAllNames").length > 0) {
                    $("#SelectAllNames").tokenfield({
                        autocomplete: {
                            source: dictionary,
                            delay: 10
                        },
                        showAutocompleteOnFocus: true
                    });
                }
                if ($("#SelectNameforManager").length > 0) {
                    $("#SelectNameforManager").tokenfield({
                        autocomplete: {
                            source: dictionary,
                            delay: 10
                        },
                        showAutocompleteOnFocus: true
                    });
                }
            }
            else {}
        }
    });
}

function GetAllMemberforTeam() {
    var TmId = $("#SelForTeamsofUser").val();
    var allmm = new Array();
    $.ajax({
        type: "GET",
        contentType: "application/json; charset=utf-8",
        url: "/DataTracker.svc/GetAllTeamMembers?Team_ID=" + TmId + "",
        dataType: "json",
        success: function (data) {
            $.each(data.d, function (index, item) {
                allmm.push(item.UserName);
            });
            if ($("#SelectAllNames").length > 0)
                $("#SelectAllNames")[0].value = allmm;

            $("#SelectAllNames").tokenfield("setTokens", allmm);
        },
        error: function (result) {
        }
    });
}

function SeprateEmailWitoutName(emails) {
    var finalemail = string.empty;
    var emailss = emails.split(",");
    for (var i = 0; i <= emailss.length - 1; i++) {
        var semail = emailss[i].trim().split(" ");
        var femail = semail[0];
        if (i.equal(0))
            finalemail = femail;
        else
            finalemail = finalemail + "," + femail;
    }
    return finalemail;
}

function ChangeteamOfPerson() {
    var emailss = $("#SelectAllNames")[0].value;
    var emails = SeprateEmailWitoutName(emailss);
    var NewTeamId = $("#SelForTeamsofUser").val();
    if (emails != "") {
        $.ajax({
            type: "GET",
            contentType: "application/json; charset=utf-8",
            url: "/DataTracker.svc/ChangeUsersTeam?Tid=" + NewTeamId + "&UEmailId=" + emails + "",
            dataType: "json",
            success: function (data) {
                if (data.d.equal(0))
                {
                    $("#Result1")[0].style.display = "block";
                    setTimeout(function () { closesavemsg("Result1"); }, 3000);
                }
            },
            error: function (result) {
            }
        });
    }
}

function GetAllMemberforTLS() {
    var TlId = $("#SelectForAllManagers").val();
    var allmmt = new Array();
    $.ajax({
        type: "GET",
        contentType: "application/json; charset=utf-8",
        url: "/DataTracker.svc/GetAllTeamMembersforManager?Tl_ID=" + TlId + "",
        dataType: "json",
        success: function (data) {
            $.each(data.d, function (index, item) {
                allmmt.push(item.UserName);
            });
            if ($("#SelectNameforManager").length > 0)
                $("#SelectNameforManager")[0].value = allmmt;

            $("#SelectNameforManager").tokenfield("setTokens", allmmt);
        },
        error: function (result) {
        }
    });
}

function ChamgeTLOfSelectedUsers() {
    var emai = $("#SelectNameforManager")[0].value;
    var emailst = SeprateEmailWitoutName(emai);
    var NewTeamIdm = $("#SelectForAllManagers").val();
    if (!emailst.equal(string.empty)) {
        $.ajax({
            type: "GET",
            contentType: "application/json; charset=utf-8",
            url: "/DataTracker.svc/ChangeUsersManagers?TLid=" + NewTeamIdm + "&UEmailId=" + emailst + "",
            dataType: "json",
            success: function (data) {
                if (data.d.equal(0)) {
                    $("#result")[0].style.display = "block";
                    setTimeout(function () { closesavemsg("result"); }, 3000);
                }
            },
            error: function (result) {
            }
        });
    }
}

function _saveNewProject() {
    var ProName = $("#TxtNewProject").val();
    if (!ProName.equal(string.empty)) {
        $.ajax({
            type: "GET",
            contentType: "application/json; charset=utf-8",
            url: "/DataTracker.svc/InsertNewProject?Projectname=" + ProName + "",
            dataType: "json",
            success: function (data) {
                if (data.d.equal(0))
                {
                    $("#result3")[0].style.display = "block";
                    setTimeout(function () { closesavemsg("result3"); }, 3000);
                    $("#TxtNewProject").val("");
                    removeOptions(document.getElementById("AllTeamDropDown"));
                    setTimeout(function () { _BindBootProjectList(); }, 300);
                }
            },
            error: function (result){}
        });
    }
    else
        alert("Please Enter Project Name");
}

function _BindBootProjectList(){
    $("#AllprojectsList")[0].innerHTML = string.empty;
    if ($("#AllprojectsList").data("kendoWindow") != null) {
        $("#AllprojectsList").data().kendoGrid.destroy();
        $("#AllprojectsList").empty();
    }
    var dataSourceDetail = string.empty;
    dataSourceDetail = new kendo.data.DataSource({
        //  pageSize: 15,
        //  serverPaging: true,
        schema: {
            data: "d",
            total: function (d) {
                if (d.d.length > 0)
                    return d.d[0].TotalCount
            },
            model: {
                id: "ID",
                fields: {
                    ID: { editable: false, type: "number" },
                    ProjectId: { editable: false, type: "string" },
                    ProjectName: { editable: false, type: "string" }
                }
            }
        },

        transport: {
            read: function (options) {
                $.ajax({
                    type: "GET",
                    url: "/DataTracker.svc/BindProjectsListdropdown",
                    dataType: "json",
                    data: {
                        Page: options.data.page,
                        PageSize: options.data.pageSize,
                        skip: options.data.skip,
                        take: options.data.take
                    },

                    success: function (result) {
                        options.success(result);

                    }
                });
            }
        }
    });
    if ($("#AllprojectsList").length > 0)
    { $("#AllprojectsList")[0].innerHTML = string.empty; }
    var element = $("#AllprojectsList").kendoGrid({
        dataSource: dataSourceDetail,
        dataBound: function () {
        },
        columns: [
                            {
                                field: "ProjectName",
                                title: "Project Name"
                            }
        ],
        editable: false,
        sortable: true,
        selectable: true
    });

    $("#AllprojectsList .k-grid-content").css("max-height", $(window).height() - 205);
}
function _BindBootuserproject() {
    $.ajax({
        type: "GET",
        contentType: "application/json; charset=utf-8",
        url: "/DataTracker.svc/GetAllEmp_Project",
        dataType: "json",
        success: function (data) {
            $.each(data.d, function (index, item) {
                $("#AllEmpNamePro").append($("<option></option>").val(item.Usersid).html(item.UserName));
            });
        },
        error: function (result) {}
    });
    setTimeout(function () { _getAllProjectsforusr(); }, 300);
}

function _getAllProjectsforusr() {
    var UsId = $("#AllEmpNamePro").val();
    var allus = new Array();
    $.ajax({
        type: "GET",
        contentType: "application/json; charset=utf-8",
        url: "/DataTracker.svc/GetAllProjectusers?Uid=" + UsId + "",
        dataType: "json",
        success: function (data) {
            $.each(data.d, function (index, item) {
                allus.push(item.UserName);
            });
            if ($("#TextAllprooo").length > 0)
                $("#TextAllprooo")[0].value = allus;

            $("#TextAllprooo").tokenfield("setTokens", allus);
        },
        error: function (result) {}
    });
}

function AutoAllProjectsListT() {
    var dictionary1 = new Array();
    var msg = "Select or enter project...";
    $.ajax({
        type: "GET",
        url: "/DataTracker.svc/GetAllProjectsList",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            var res = data.d;
            if (res != null) {
                for (var n = 0; n < data.d.length; n++) {
                    var value = data.d[n].UserName;
                    dictionary1.push(value);
                }

                if ($("#TextAllprooo").length > 0) {
                    $("#TextAllprooo").tokenfield({
                        autocomplete: {
                            source: dictionary1,
                            delay: 10
                        },
                        showAutocompleteOnFocus: true
                    });
                }
            }
            else {}
        }
    });
}

function _InsertNewProject() {
    var pros = $("#TextAllprooo")[0].value;
    var Ui_Id = $("#AllEmpNamePro").val();
    if (!pros.equal(string.empty)) {
        $.ajax({
            type: "GET",
            contentType: "application/json; charset=utf-8",
            url: "/DataTracker.svc/InsertProForUsers?U_id=" + Ui_Id + "&ProList=" + pros + "",
            dataType: "json",
            success: function (data) {
                if (data.d.equal(0)) {
                    $("#result4")[0].style.display = "block";
                    setTimeout(function () { closesavemsg("result4"); }, 3000);
                }
            },
            error: function (result) {}
        });
    }
}

function BindAllPeojectforname() {
    $.ajax({
        type: "GET",
        contentType: "application/json; charset=utf-8",
        url: "/DataTracker.svc/GetAllProjectsName",
        dataType: "json",
        success: function (data) {
            $.each(data.d, function (index, item) {
                $("#Selectprojectname").append($('<option style="width:420px;"></option>').val(item.Usersid).html(item.UserName));
            });
        },
        error: function (result) {}
    });
    setTimeout(function () { _BindgridforProject(); }, 300);
}


function _BindgridforProject() {
    var pid = $("#Selectprojectname").val();
    $("#DivGridforallNames")[0].innerHTML = string.empty;
    if ($("#DivGridforallNames").data("kendoWindow") != null) {
        $("#DivGridforallNames").data().kendoGrid.destroy();
        $("#DivGridforallNames").empty();
    }
    var dataSourceDetail = string.empty;
    dataSourceDetail = new kendo.data.DataSource({
        //  pageSize: 15,
        //  serverPaging: true,
        schema: {
            data: "d",
            total: function (d) {
                if (d.d.length > 0)
                    return d.d[0].TotalCount
            },
            model: {
                id: "ID",
                fields: {
                    ID: { editable: false, type: "number" },
                    Usersid: { editable: false, type: "string" },
                    UserName: { editable: false, type: "string" }
                }
            }
        },

        transport: {
            read: function (options) {
                $.ajax({
                    type: "GET",
                    url: "/DataTracker.svc/GetAllUsersforproject",
                    dataType: "json",
                    data: {
                        pid: pid
                    },

                    success: function (result) {
                        options.success(result);

                    }
                });
            }
        }
    });

    if ($("#DivGridforallNames").length > 0)
    { $("#DivGridforallNames")[0].innerHTML = string.empty; }
    var element = $("#DivGridforallNames").kendoGrid({
        dataSource: dataSourceDetail,
        dataBound: function () {
            string.empty;
        },

        columns: [
                            {
                                field: "UserName",
                                title: "Employees",
                                template: "#=getRowforGrid(UserName,Usersid)#"

                            }
        ],
        editable: false,
        sortable: true,
        selectable: true
    });

    $("#DivGridforallNames .k-grid-content").css("max-height", $(window).height() - 205);
}

function getRowforGrid(un, id) {
    var pid = $("#Selectprojectname").val();
    return "<div><span>" + un + "</span><span title='delete' style='float:right;' onclick='DeleteUserfromproject(" + id + "," + pid + ");'><img src='/Images/close.png' style='cursor: pointer;height:18px;width:18px;' /></span></div>";
}

function DeleteUserfromproject(uid, pid) {
    var a = confirm("Are you sure to delete ?");
    if (a.equal(true)) {
        $.ajax({
            type: "GET",
            contentType: "application/json; charset=utf-8",
            url: "/DataTracker.svc/DeleteUserfromProject?userid=" + uid + "&pid=" + pid + "",
            dataType: "json",
            success: function (data) {
                if (data.d.equal(0)) {
                    _BindgridforProject();
                    $("#result5")[0].style.display = "block";
                    setTimeout(function () { closesavemsg("result5"); }, 2000);
                }
            },
            error: function (result) {
            }
        });
    }
    else
        return false;
}

function BindAllTeam() {
    $.ajax({
        type: "GET",
        contentType: "application/json; charset=utf-8",
        url: "/DataTracker.svc/GetAllTeamName",
        dataType: "json",
        success: function (data) {
            $.each(data.d, function (index, item) {
                $("#ddlTeamName").append($("<option></option>").val(item.Teamid).html(item.TeamNameGeneric));
            });;
        },
        error: function (result) {}
    });
}

function AutoAllMember() {
    var allus1 = new Array();
    $.ajax({
        type: "GET",
        url: "/DataTracker.svc/GetAutoAllMember",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            $.each(data.d, function (index, item) {
                allus1.push(item.Email);
                allus.push({ Email: item.Email, userid: item.userid });
            });

            if ($("#txtAllMember").length > 0) {
                $("#txtAllMember").tokenfield({
                    autocomplete: {
                        source: allus1,
                        delay: 10
                    },
                    showAutocompleteOnFocus: true
                });
            }
        },
        error: function (result) {}
    });
}

function Changeteammember() {
    var output = new Array();
    var emailss = $("#txtAllMember")[0].value;
    var emails = SeprateEmailWitoutName(emailss);
    var emailidSplit = emails.split(",");

    if (!emailidSplit.equal(string.empty)) {
        for (i = 0; i < emailidSplit.length; i++) {
            var memberemailsingle = emailidSplit[i];

            for (var j = 0; j < allus.length; j++) {
                var memberindex = allus[j];
                var memberemail = memberindex.Email;
                if (memberemail.equal(memberemailsingle)) {
                    var userid = allus[j].userid;
                    output.push(userid);
                }
            }
        }
    }

    if (output.length > 0)
    {
        var arr = JSON.stringify({ userId: output });
    }

    var NewTeammemberid = $("#ddlTeamName").val();
    if (!arr.equal(string.empty) && !arr.equal(undefined) && !NewTeammemberid.equal(string.empty)) {
        $.ajax({
            type: "POST",
            url: "/DataTracker.svc/Changeteammember",
            data: "{\"Tid\": \"" + NewTeammemberid + "\"," + arr + "}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data){  
                if (data.d.equal(0)) {
                    $("#Result6")[0].style.display = "block";
                    setTimeout(function () { closesavemsg("Result6"); }, 3000);
                }
            },
            error: function (result) {}
        });
    }
}