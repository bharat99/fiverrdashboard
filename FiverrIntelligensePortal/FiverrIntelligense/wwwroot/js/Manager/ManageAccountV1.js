﻿$(document).ready(function () {
    GetAccount();
    $("#txtAccountPhone").keydown(function (e) {
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
            // Allow: Ctrl+A, Command+A
            (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) ||
            // Allow: home, end, left, right, down, up
            (e.keyCode >= 35 && e.keyCode <= 40)) {
            // let it happen, don't do anything
            return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });

    $("#txtAccountName").keypress(function (event) {
        var inputValue = event.which;
        // allow letters and whitespaces only.
        if (!(inputValue >= 65 && inputValue <= 122) && (inputValue != 32 && inputValue != 0)) {
            event.preventDefault();
        }
    });
});

function GetAccount() {
    $.ajax({
        type: "GET",
        contentType: "application/json; charset=utf-8",
        url: "/DataTracker.svc/GetAccount",
        data: {},
        dataType: "json",
        success: function (data) {
            AccountGrid(data.d);
        },
        error: function (error) {
        }
    })

}


function AddAccount() {
    $("#AddAccountFlag").css('display', 'none');
    $("#lblNameValidation").css('display', 'none');
    $("#lblEmailValidation").css('display', 'none');
    $("#lblphoneValidation").css('display', 'none');

    var Accountname = $("#txtAccountName").val();
    var Accountaddress = $("#txtAccountAddress").val();
    var Accounemail = $("#txtAccountEmail").val();
    var Accounphone = $("#txtAccountPhone").val();
    // Required Filed.
    if (Accountname.length > 0) {
        var patternCaps = new RegExp('[A-Z]');
        var patternSmall = new RegExp('[a-z]');
        if (Accountname.match(patternCaps) || Accountname.match(patternSmall))
        { $("#txtAccountName").addClass("AccountDetail"); }
        else
        {
            $("#lblNameValidation").css('display', 'block');
            return false;
        }
    }
    else {
        $("#lblNameValidation").css('display', 'block');
        return false;
    }
    //Reguler Expression Email Validation.
    if (Accounemail.length > 0) {
        var regex = /^([a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|"((([ \t]*\r\n)?[ \t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([ \t]*\r\n)?[ \t]+)?")@(([a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i;
        if (!regex.test(Accounemail)) {
            $("#lblEmailValidation").css('display', 'block');
            return false;
        } else {
            $("#txtAccountEmail").addClass("AccountDetail");
        }
    }
    //Address
    if (Accountaddress.length > 0) {
        var patternCaps = new RegExp('[A-Z]');
        var patternSmall = new RegExp('[a-z]');
        if (Accountaddress.match(patternCaps) || Accountaddress.match(patternSmall))
        { $("#txtAccountAddress").addClass("AccountDetail"); }
        else { Accountaddress =''; }
    }
    //Reguler Expression Phone Number Validation.
    if (Accounphone.length > 0) {
        var intRegex = /^\d+$/;
        if (!intRegex.test(Accounphone)) {
            $("#lblphoneValidation").css('display', 'block');
            return false;
        }
        else {
            $("#txtAccountPhone").addClass("AccountDetail");
        }
    }
    $.ajax({
        type: "POST",
        url: "/DataTracker.svc/AddAccount",
        data: "{\"Accountname\":\"" + Accountname + "\",\"Accountaddress\":\"" + Accountaddress + "\",\"Accounemail\":\"" + Accounemail + "\",\"Accounphone\":\"" + Accounphone + "\"}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            $("#lblNameValidation").css('display', 'none');
            $("#lblEmailValidation").css('display', 'none');
            $("#lblphoneValidation").css('display', 'none');

            $("#AddAccountFlag").css('display', 'block');
            $("#txtAccountName").val('');
            $("#txtAccountAddress").val('');
            $("#txtAccountEmail").val('');
            $("#txtAccountPhone").val('');

            $("#txtAccountName").addClass("AccountDetail");
            $("#txtAccountAddress").addClass("AccountDetail");
            $("#txtAccountEmail").addClass("AccountDetail");
            $("#txtAccountPhone").addClass("AccountDetail");
            GetAccount();
        }
    });
}

function AccountGrid(abc) {
    $("#AccountGrid").kendoGrid({
        dataSource: {
            data: abc
        },
        height: 550,
        //groupable: true,
        //sortable: true,
        //editable: true,
        //filterable: true,
        selectable: "row",
        pageable: {
            refresh: false,
            pageSizes: false,
            //buttonCount: 5,
            pageSize: 100
        },
        columns: [{
            field: "Name",
            title: "Name"
        }, {
            field: "Email",
            title: "Email"
        }, {
            field: "Phone",
            title: "Phone"
        }, {
            field: "Address",
            title: "Address"
        }, {
            //field: "Edit",
           // template: '<input type="button" class="Edit  k-button k-button-icontext" name="Edit" value="Edit" align="center" onclick="javascript:EditAccountPopUp();" style="height: 75%; margin: 0px 2px; min-width: 45%;" /><input type="button" class="Delete  k-button k-button-icontext" name="Delete" value="Delete" align="center" onclick="javascript:DeleteAccount();" style="height: 75%; margin: 0px 2px; min-width: 45%;" />',
            template: '<i class="fa fa-pencil-square-o" aria-hidden="true" onclick="javascript:EditAccountPopUp();" ></i><i class="fa fa-trash-o" aria-hidden="true" onclick="javascript:DeleteAccount();"></i>',
            title: "Actions",
            headerAttributes: { "class": "table-header-cell", "style": "text-align: center;" }
        }]
    });
}

function CanelAddPopup() {
    $("#AddAccountFlag").css('display', 'none');
    $("#lblNameValidation").css('display', 'none');
    $("#lblEmailValidation").css('display', 'none');
    $("#lblphoneValidation").css('display', 'none');

    $("#txtAccountName").val('');
    $("#txtAccountAddress").val('');
    $("#txtAccountEmail").val('');
    $("#txtAccountPhone").val('');

    $("#txtAccountName").addClass("AccountDetail");
    $("#txtAccountAddress").addClass("AccountDetail");
    $("#txtAccountEmail").addClass("AccountDetail");
    $("#txtAccountPhone").addClass("AccountDetail");
    var dialog = $("#AddAccount").data("kendoWindow");
    dialog.close();
}
function CancleEditPopup() {
    $("#Flag").css('display', 'none');
    $("#lblNameValidation").css('display', 'none');
    $("#lblEmailValidation").css('display', 'none');
    $("#lblphoneValidation").css('display', 'none');

    var dialog = $("#EditAccount").data("kendoWindow");
    dialog.close();
}

function EditAccountPopUp() {
    var windowElement = $("#EditAccount").kendoWindow({
        actions: {},
        content: "/EditAccount.aspx",
        modal: true,
        width: "500px",
        height: "400px",
        title: "Edit Account Details",
        position: {
            top: 10,
            left: 2
        },
        resizable: false,
        //actions: ["Close"],
        draggable: false

    });
    var dialog = $("#EditAccount").data("kendoWindow");
    dialog.center();
    dialog.open();
}

function DeleteAccount() {
    var grid = $("#AccountGrid").data("kendoGrid");
    var selected = grid.dataItem(grid.select());
    var id = selected.id;
    if (confirm('Are you sure you want to delete?')) {
        $.ajax({
            type: "POST",
            url: "/DataTracker.svc/DeleteAccount",
            data: "{\"Accountid\":\"" + id + "\"}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                //alert("Are you sure.");
                GetAccount();
                CancleEditPopup();
            }
        });
    }
    else {

        return false;
    }
}

function EditAccount() {
    $("#Flag").css('display', 'none');
    $("#lblNameValidation").css('display', 'none');
    $("#lblEmailValidation").css('display', 'none');
    $("#lblphoneValidation").css('display', 'none');

    var id = $("#Accountid").val();
    var Name = $("#txtAccountName").val();
    var Address = $("#txtAccountAddress").val();
    var Email = $("#txtAccountEmail").val();
    var Phone = $("#txtAccountPhone").val();

    if (Name.length > 0) {
        var patternCaps = new RegExp('[A-Z]');
        var patternSmall = new RegExp('[a-z]');
        if (Name.match(patternCaps) || Name.match(patternSmall))
        { $("#txtAccountName").addClass("AccountDetail"); }
        else
        {
            $("#lblNameValidation").css('display', 'block');
            return false;
        }
    }
    else {
        $("#lblNameValidation").css('display', 'block');
        return false;
    }
    //Reguler Expression Email Validation.
    if (Email.length > 0) {
        var regex = /^([a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|"((([ \t]*\r\n)?[ \t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([ \t]*\r\n)?[ \t]+)?")@(([a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i;
        if (!regex.test(Email)) {
            $("#lblEmailValidation").css('display', 'block');
            return false;
        } else {
            $("#txtAccountEmail").addClass("AccountDetail");
        }
    }
    //Address
    if (Address.length > 0) {
        var patternCaps = new RegExp('[A-Z]');
        var patternSmall = new RegExp('[a-z]');
        if (Address.match(patternCaps) || Address.match(patternSmall))
        { $("#txtAccountAddress").addClass("AccountDetail"); }
        else { Address = ""; }
    }
    //Reguler Expression Phone Number Validation.
    if (Phone.length > 0) {
        var intRegex = /^\d+$/;
        if (!intRegex.test(Phone)) {
            $("#lblphoneValidation").css('display', 'block');
            return false;
        }
        else {
            $("#txtAccountPhone").addClass("AccountDetail");
        }
    }

    $.ajax({
        type: "POST",
        url: "/DataTracker.svc/EditAccount",
        data: "{\"id\":\"" + id + "\",\"Name\":\"" + Name + "\",\"Address\":\"" + Address + "\",\"Email\":\"" + Email + "\",\"Phone\":\"" + Phone + "\"}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            $("#lblNameValidation").css('display', 'none');
            $("#lblEmailValidation").css('display', 'none');
            $("#lblphoneValidation").css('display', 'none');

            $("#Flag").css('display', 'block');
            $("#txtAccountName").val('');
            $("#txtAccountAddress").val('');
            $("#txtAccountEmail").val('');
            $("#txtAccountPhone").val('');

            $("#lblNameValidation").css('display', 'none');
            $("#lblEmailValidation").css('display', 'none');
            $("#lblphoneValidation").css('display', 'none');

            $("#txtAccountName").addClass("AccountDetail");
            $("#txtAccountAddress").addClass("AccountDetail");
            $("#txtAccountEmail").addClass("AccountDetail");
            $("#txtAccountPhone").addClass("AccountDetail");
            GetAccount();
            CancleEditPopup();
        }
    });
}

function AddAccountPopUp() {
    var windowElement = $("#AddAccount").kendoWindow({
        actions: {},
        content: "/AddAccount.aspx",
        modal: true,
        width: "500px",
        height: "400px",
        title: "Add Account Manager",
        position: {
            top: 10,
            left: 2
        },
        resizable: false,
       // actions: ["Close"],
        draggable: false

    });
    var dialog = $("#AddAccount").data("kendoWindow");
    dialog.center();
    dialog.open();
    // return false;
}