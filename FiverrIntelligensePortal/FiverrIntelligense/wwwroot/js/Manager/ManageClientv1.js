﻿$(document).ready(function () {
    GetClient();
    $("#txtClientPhone").keydown(function (e) {
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
            // Allow: Ctrl+A, Command+A
            (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) ||
            // Allow: home, end, left, right, down, up
            (e.keyCode >= 35 && e.keyCode <= 40)) {
            // let it happen, don't do anything
            return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });

    $("#txtClientName").keypress(function (event) {
        var inputValue = event.which;
        // allow letters and whitespaces only.
        if (!(inputValue >= 65 && inputValue <= 122) && (inputValue != 32 && inputValue != 0)) {
            event.preventDefault();
        }
    });
});
function GetClient() {
    $.ajax({
        type: "GET",
        contentType: "application/json; charset=utf-8",
        url: "/DataTracker.svc/GetClient",
        data: {},
        dataType: "json",
        success: function (data) {
            ClientGrid(data.d);
        },
        error: function (error) {
        }
    })

}

function GetAccount() {
    $.ajax({
        type: "GET",
        contentType: "application/json; charset=utf-8",
        url: "/DataTracker.svc/GetAccount",
        data: {},
        dataType: "json",
        success: function (data) {
            AccountGrid(data.d);
        },
        error: function (error) {
        }
    })

}

function AddClient() {
    $("#ClientAddedFlag").css('display', 'none');
    $("#lblCNameValidation").css('display', 'none');
    $("#lblCEmailValidation").css('display', 'none');
    $("#lblCphoneValidation").css('display', 'none');

    var ClientName = $("#txtClientName").val();
    var ClientOrg = $("#txtClinetOrg").val();
    var ClientEmail = $("#txtClientEmail").val();
    var ClientPhone = $("#txtClientPhone").val();
    // Required Filed.
    if (ClientName.length > 0) {
        var patternCaps = new RegExp('[A-Z]');
        var patternSmall = new RegExp('[a-z]');
        if (ClientName.match(patternCaps) || ClientName.match(patternSmall))
        { $("#txtClientName").addClass("ClientDetail"); }
        else
        {
            $("#lblCNameValidation").css('display', 'block');
            return false;
        }
    }
    else {
        $("#lblCNameValidation").css('display', 'block');
        return false;
    }
    //Reguler Expression Email Validation.
    if (ClientEmail.length > 0) {
        var regex = /^([a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|"((([ \t]*\r\n)?[ \t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([ \t]*\r\n)?[ \t]+)?")@(([a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i;
        if (!regex.test(ClientEmail)) {
            $("#lblCEmailValidation").css('display', 'block');
            return false;
        } else {
            $("#txtClientEmail").addClass("ClientDetail");
        }
    }
    //Organization
    if (ClientOrg.length > 0) {
        var patternCaps = new RegExp('[A-Z]');
        var patternSmall = new RegExp('[a-z]');
        if (ClientOrg.match(patternCaps) || ClientOrg.match(patternSmall))
        { $("#txtClinetOrg").addClass("ClientDetail"); }
        else { ClientOrg = ""; }
    }
    //Reguler Expression Phone Number Validation.
    if (ClientPhone.length > 0) {
        var intRegex = /^\d+$/;
        if (!intRegex.test(ClientPhone)) {
            $("#lblCphoneValidation").css('display', 'block');
            return false;
        }
        else {
            $("#txtClientPhone").addClass("ClientDetail");
        }
    }
    $.ajax({
        type: "GET",
        url: "/DataTracker.svc/AddClient?Clientname=" + ClientName + "&Clientorg=" + ClientOrg + "&Clientemail=" + ClientEmail + "&ClientPhone=" + ClientPhone + "",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {

            $("#lblCNameValidation").css('display', 'none');
            $("#lblCEmailValidation").css('display', 'none');
            $("#lblCphoneValidation").css('display', 'none');

            $("#ClientAddedFlag").css('display', 'block');
            $("#txtClientName").val('');
            $("#txtClinetOrg").val('');
            $("#txtClientEmail").val('');
            $("#txtClientPhone").val('');

            $("#txtClientName").addClass("ClientDetail");
            $("#txtClinetOrg").addClass("ClientDetail");
            $("#txtClientEmail").addClass("ClientDetail");
            $("#txtClientPhone").addClass("ClientDetail");
            GetClient();
        }
    });
}


function DeleteClient() {
    var grid = $("#ClientGrid").data("kendoGrid");
    var selected = grid.dataItem(grid.select());
    var id = selected.ClientId;
    if (confirm('Are you sure you want to delete?')) {
        $.ajax({
            type: "POST",
            url: "/DataTracker.svc/DeleteClient",
            data: "{\"ClientId\":\"" + id + "\"}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                GetClient();
            }
        });
    }
    else {

        return false;
    }
}

function EditClient() {
    $("#Flag").css('display', 'none');
    $("#lblCNameValidation").css('display', 'none');
    $("#lblCEmailValidation").css('display', 'none');
    $("#lblCphoneValidation").css('display', 'none');

    var ClientId = $("#clientid").val();
    var Name = $("#txtClientName").val();
    var Organization = $("#txtClinetOrg").val();
    var Email = $("#txtClientEmail").val();
    var Phone = $("#txtClientPhone").val();

    if (Name.length > 0) {
        var patternCaps = new RegExp('[A-Z]');
        var patternSmall = new RegExp('[a-z]');
        if (Name.match(patternCaps) || Name.match(patternSmall))
        { $("#txtClientName").addClass("ClientDetail"); }
        else
        {
            $("#lblCNameValidation").css('display', 'block');
            return false;
        }
        
    }
    else {
        $("#lblCNameValidation").css('display', 'block');
        return false;
    }
    //Reguler Expression Email Validation.
    if (Email.length > 0) {
        var regex = /^([a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|"((([ \t]*\r\n)?[ \t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([ \t]*\r\n)?[ \t]+)?")@(([a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i;
        if (!regex.test(Email)) {
            $("#lblCEmailValidation").css('display', 'block');
            return false;
        } else {
            $("#txtClientEmail").addClass("ClientDetail");
        }
    }
    //Organization
    if (Organization.length > 0) {
        var patternCaps = new RegExp('[A-Z]');
        var patternSmall = new RegExp('[a-z]');
        if (Organization.match(patternCaps) || Organization.match(patternSmall))
        { $("#txtClinetOrg").addClass("ClientDetail"); }
        else { ClientOrg = ""; }
    }
    //Reguler Expression Phone Number Validation.
    if (Phone.length > 0) {
        var intRegex = /^\d+$/;
        if (!intRegex.test(Phone)) {
            $("#lblCphoneValidation").css('display', 'block');
            return false;
        }
        else {
            $("#txtClientPhone").addClass("ClientDetail");
        }
    }

    $.ajax({
        type: "POST",
        url: "/DataTracker.svc/EditClient",
        data: "{\"ClientId\":\"" + ClientId + "\",\"Name\":\"" + Name + "\",\"Organization\":\"" + Organization + "\",\"Email\":\"" + Email + "\",\"Phone\":\"" + Phone + "\"}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            $("#lblCNameValidation").css('display', 'none');
            $("#lblCEmailValidation").css('display', 'none');
            $("#lblCphoneValidation").css('display', 'none');

            $("#Flag").css('display', 'block');
            $("#txtClientName").val('');
            $("#txtClinetOrg").val('');
            $("#txtClientEmail").val('');
            $("#txtClientPhone").val('');

            $("#lblCNameValidation").css('display', 'none');
            $("#lblCEmailValidation").css('display', 'none');
            $("#lblCphoneValidation").css('display', 'none');

            $("#txtClientName").addClass("ClientDetail");
            $("#txtClinetOrg").addClass("ClientDetail");
            $("#txtClientEmail").addClass("ClientDetail");
            $("#txtClientPhone").addClass("ClientDetail");

            GetClient();
            CanelEditPopup();
        }
    });
}


function EditClientPopUp() {
    var windowElement = $("#EditClient").kendoWindow({
        actions: {},
        content: "/Editclient.aspx",
        modal: true,
        width: "500px",
        height: "400px",
        title: "Edit Client Details",
        position: {
            top: 10,
            left: 2
        },
        resizable: false,
        //actions: ["Close"],
        draggable: false

    });
    var dialog = $("#EditClient").data("kendoWindow");
    dialog.center();
    dialog.open();
    // return false;
}


function CanelCPopup() {
    $("#ClientAddedFlag").css('display', 'none');
    $("#lblCNameValidation").css('display', 'none');
    $("#lblCEmailValidation").css('display', 'none');
    $("#lblCphoneValidation").css('display', 'none');

        $("#txtClientName").val('');
        $("#txtClinetOrg").val('');
        $("#txtClientEmail").val('');
        $("#txtClientPhone").val('');

        $("#txtClientName").addClass("ClientDetail");
        $("#txtClinetOrg").addClass("ClientDetail");
        $("#txtClientEmail").addClass("ClientDetail");
        $("#txtClientPhone").addClass("ClientDetail");
    var dialog = $("#AddClient").data("kendoWindow");
    dialog.close();
}

function CanelEditPopup() {
    $("#Flag").css('display', 'none');
    $("#lblCNameValidation").css('display', 'none');
    $("#lblCEmailValidation").css('display', 'none');
    $("#lblCphoneValidation").css('display', 'none');

    var dialog = $("#EditClient").data("kendoWindow");
    dialog.close();
}


function ClientGrid(abc) {
    $("#ClientGrid").kendoGrid({
        dataSource: {
            data: abc
        },
        height: 550,
        //groupable: true,
        //sortable: true,
        //editable: true,
        //filterable: true,
        selectable: "row",
        pageable: {
            refresh: false,
            pageSizes: false,
            //buttonCount: 5,
            pageSize: 100
        },
        columns: [{
            field: "Name",
            title: "Name"
        }, {
            field: "Organigation",
            title: "Organization"
        }, {
            field: "Email",
            title: "Email"
        }, {
            field: "Phone",
            title: "Phone"
        }, {
            //field: "Edit",
          //  template: '<input type="button" id="Edit" class="Edit  k-button k-button-icontext" name="Edit" value="Edit" align="center" onclick="javascript:EditClientPopUp();" style="height: 75%; margin: 0px 2px; min-width: 45%;" /><input type="button" id="Delete" class="Delete  k-button k-button-icontext" name="Delete" value="Delete" align="center" onclick="javascript:DeleteClient();" style="height: 75%; margin: 0px 2px; min-width: 45%;" />',
            template: '<i class="fa fa-pencil-square-o" aria-hidden="true" onclick="javascript:EditClientPopUp();" ></i><i class="fa fa-trash-o" aria-hidden="true" onclick="javascript:DeleteClient();"></i>',
            title: "Actions",
            headerAttributes: { "class": "table-header-cell", "style": "text-align: center;" }
        }]
    });

}


function AddClientPopUp() {
    var windowElement = $("#AddClient").kendoWindow({
        actions: {},
        content: "/AddClient.aspx",
        modal: true,
        width: "500px",
        height: "400px",
        title: "Add Client",
        position: {
            top: 10,
            left: 2
        },
        resizable: false,
        //actions: ["Close"],
        draggable: false,

    });
    var dialog = $("#AddClient").data("kendoWindow");
    dialog.center();
    dialog.open();
    // return false;
}


