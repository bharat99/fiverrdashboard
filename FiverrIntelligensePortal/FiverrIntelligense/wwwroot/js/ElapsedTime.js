﻿
function Select1_onchange() {
    var Flag = $("#Dropoption").val();
    var gridMonth = $("#HtmComcoProject").val();
    GetGridData(gridMonth, Flag);
}

function MonthLoad(cmon, Flag) {
    $.ajax({
        type: "GET",
        contentType: "application/json; charset=utf-8",
        url: "/DataTracker.svc/BindMonths",
        dataType: "json",
        success: function (data) {
            // $("#HtmComcoProject").empty().append($("<option></option>").val("-1").html("--- Select Month ---"));
            $.each(data.d, function (index, item) {
                $("#HtmComcoProject").append('<option value = "' + item.MonthID + '">' + item.MonthName + '</option>');
                var today = new Date();
                var cmon = today.getMonth();
                $('#HtmComcoProject').prop('selectedIndex', cmon);

            });
            GetGridData(cmon, Flag);
        },
        error: function (result) {

        }
    });
}

function GetGridData(month, value) {

    if (value == "2") {
        $("#SlipagetimeGrid")[0].innerHTML = "";
        var dataSourceStore = "";
        dataSourceStore = new kendo.data.DataSource({
            transport: {
                read: {
                    url: "/DataTracker.svc/Manageelapsedtime",
                    data: {
                        CDate: month,
                        Flag: value
                    }
                }
            },
            serverPaging: true,
            sortable: {
                mode: "single",
                allowUnsort: false
            },
            pageSize: 8,
            schema: {
                data: "d",
                total: function (d) {
                    if (d.d.length > 0)
                        return d.d[0].totalcount
                },
                model: {
                    id: "ID",
                    fields: {
                        Username: { editable: false, type: "string" },
                        Slipagetime: { editable: false, type: "string" },
                        reopen: { editable: false, type: "string" },
                        Qaretrun: { editable: false, type: "string" },
                        IssueNotFound: { editable: false, type: "string" }
                    }
                }
            },
            pageSize: 8,
            serverPaging: true

        });
        var element = $("#SlipagetimeGrid").kendoGrid({
            dataSource: dataSourceStore,
            excel: {
                fileName: "TeamsSlipageReport.xlsx",
                filterable: true,
                allPages: true
            },
            detailInit: GetTeamsData,
            dataBound: function () {
            },
            pageable: {
                pageSize: 8,
                refresh: true,
                pageSizes: true,
                input: true
            },

            columns: [{
                field: "Username",
                title: "Name",
                width: 100
            },
                        {
                            field: "Slipagetime",
                            title: "Avg Slipage",
                            width: 70
                        },
                        {
                            field: "reopen",
                            title: "Reopen",
                            width: 50
                        },
                        {
                            field: "Qaretrun",
                            title: "QA Retrun",
                            width: 70
                        },
                        {
                            field: "IssueNotFound",
                            title: "Issue Not Found",
                            width: 80

                        }
                         ],
            editable: false,
            sortable: true,
            selectable: true,
            pageable: true
        });
    }
    else {
        $("#SlipagetimeGrid")[0].innerHTML = "";
        var dataSourceStore = "";
        dataSourceStore = new kendo.data.DataSource({
            transport: {
                read: {
                    url: "/DataTracker.svc/Manageelapsedtime",
                    data: {
                        CDate: month,
                        Flag: value
                    }
                }
            },
            serverPaging: true,
            sortable: {
                mode: "single",
                allowUnsort: false
            },
            pageSize: 8,
            schema: {
                data: "d",
                total: function (d) {
                    if (d.d.length > 0)
                        return d.d[0].totalcount
                },
                model: {
                    id: "ID",
                    fields: {
                        Username: { editable: false, type: "string" },
                        Slipagetime: { editable: false, type: "string" },
                        reopen: { editable: false, type: "string" },
                        Qaretrun: { editable: false, type: "string" },
                        IssueNotFound: { editable: false, type: "string" }
                    }
                }
            },
            pageSize: 8,
            serverPaging: true

        });
        var element = $("#SlipagetimeGrid").kendoGrid({
            dataSource: dataSourceStore,
            excel: {
                fileName: "SlipageReport.xlsx",
                filterable: true,
                allPages: true
            },
            dataBound: function () {
            },
            pageable: {
                pageSize: 8,
                refresh: true,
                pageSizes: true,
                input: true
            },

            columns: [{
                field: "Username",
                title: "Name",
                width: 100
            },
                        {
                            field: "Slipagetime",
                            title: "Avg Slipage",
                            width: 70
                        },
                        {
                            field: "reopen",
                            title: "Reopen",
                            width: 50
                        },
                        {
                            field: "Qaretrun",
                            title: "QA Retrun",
                            width: 70
                        },
                        {
                            field: "IssueNotFound",
                            title: "Issue Not Found",
                            width: 80

                        }
                         ],
            editable: false,
            sortable: true,
            selectable: true,
            pageable: true
        });
    }
}
function GetTeamsData(e) {
    var month = $("#HtmComcoProject").val();
    var value = $("#Dropoption").val();
    $("<div/>").appendTo(e.detailCell).kendoGrid({
        dataSource: {
            transport: {
                read: {
                    url: "/DataTracker.svc/GetIndividualData",
                    data: {
                        CDate: month,
                        Flag: value,
                        tid: e.data.Teamid
                    }
                }
            },
            serverPaging: true,
            serverSorting: true,
            serverFiltering: true,
            pageSize: 100,
            schema: {
                data: "d",
                total: function (d) {
                    if (d.d.length > 0)
                        return d.d[0].TotalCount
                },
                model: {
                    id: "ID",
                    fields: {
                        Username: { editable: false, type: "string" },
                        Slipagetime: { editable: false, type: "string" },
                        reopen: { editable: false, type: "string" },
                        Qaretrun: { editable: false, type: "string" },
                        IssueNotFound: { editable: false, type: "string" }
                    }
                }
            }
        },
        pageable: {
            pageSize: 8,
            refresh: true,
            pageSizes: true,
            input: true
        },
        pageable: false,
        dataBound: function () {
        },
        columns: [
                         {
                             field: "Username",
                             title: "Name",
                             width: 100
                         },
                        {
                            field: "Slipagetime",
                            title: "Avg Slipage",
                            width: 70
                        },
                        {
                            field: "reopen",
                            title: "Reopen",
                            width: 50
                        },
                        {
                            field: "Qaretrun",
                            title: "QA Retrun",
                            width: 70
                        },
                        {
                            field: "IssueNotFound",
                            title: "Issue Not Found",
                            width: 80

                        }
                        ],
        editable: false,
        sortable: true,
        selectable: true,
        pageable: false

    });
}

function forOnLoadCall() {
    var today = new Date();
    var cmon = today.getMonth() + 1;
    var Flag = $('#Individual').val();
    MonthLoad(cmon, Flag);
    $("#Individual,#TeamMember,#TeamWise,#LoginWorks").hide();
    var geturl = window.location.href;
    if (geturl != null) {
        var arrgetsid = new Array();
        arrgetsid = geturl.split("&");
        var AllParam = new Array();
        AllParam = arrgetsid[0].split("=");
        var AllParam2 = new Array();
        AllParam2 = arrgetsid[1].split("=");
        var userid = AllParam[1];
        var typeid = AllParam2[1];
    }
    if (typeid == "12") {
        $("#Individual,#TeamMember,#TeamWise,#LoginWorks").show();
    }
    else if (typeid == "14") {
        $("#Individual,#TeamMember").show();
    }
    else {
        $("#Individual").show();
    }

    $('#Dropoption').change(function () {
        if ($("#Dropoption").val() == 3) {
            var gridMonth = $("#HtmComcoProject").val();
            var Flag = $('#Dropoption').val();
            GetGridData(gridMonth, Flag);
        }
    });

    $('#Dropoption').change(function () {
        if ($("#Dropoption").val() == 1) {
            var gridMonth = $("#HtmComcoProject").val();
            var Flag = $('#Dropoption').val();
            GetGridData(gridMonth, Flag);
        }
    });

    $('#Dropoption').change(function () {
        if ($("#Dropoption").val() == 2) {
            var gridMonth = $("#HtmComcoProject").val();
            var Flag = $('#Dropoption').val();
            GetGridData(gridMonth, Flag);
        }
    });


    $('#Dropoption').change(function () {
        if ($("#Dropoption").val() == 4) {
            var gridMonth = $("#HtmComcoProject").val();
            var Flag = $('#Dropoption').val();
            GetGridData(gridMonth, Flag);
        }
    });

}