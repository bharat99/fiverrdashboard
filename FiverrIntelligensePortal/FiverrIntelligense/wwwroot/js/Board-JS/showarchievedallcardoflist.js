﻿function TestController() {
    var allCardRemovedArchiveName1 = '';
    var privateMethods = {
        get_archive_card_from_qstring_onload: function () {
            var url = window.location.href;
            var listid = url.substring(url.lastIndexOf('=') + 1);
            var qs = url.split('?')[1];
            var parts = qs.split('&');
            var arr = [];
            $.each(parts, function () {
                var val = this.split('=')[1];
                arr.push(val);
            });
            var boardid = arr[0];
            $("#boardID").val(boardid);
            var result = listid + "~" + boardid;
            return result;
        },
        show_list: function (listid, boardid) {
            var s = 's';
            $("#dropdownlistid").empty();
            $.ajax({
                type: "POST",
                url: "/DataTracker.svc/showarchiverecord",
                data: "{\"OperationStatus\": \"" + s + "\",\"boardid\": \"" + boardid + "\"}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    $.each(data.d, function (index, item) {
                        if (index == 0) {
                            $("#boarddd").empty();
                            $("#boardnmsec").append("<a id='txtTeamName1' href='board.aspx?" + boardid + "' class='upperpart1' >" + item.BoardName + "</a>");
                        }
                        $("#dropdownlistid").append($("<option></option>").val(item.ProjectID).html(item.ProjectType));
                    });
                    $("#dropdownlistid").val(listid);
                    var selectedText = $("#dropdownlistid").find("option:selected").text();
                    privateMethods.showcardaccordingtolist(listid);
                }
            });
        },
        changofdropdown: function () {
            var selectedtext = $("#dropdownlistid :selected").text();
            var selectedvalue = $(this).val();
            privateMethods.showcardaccordingtolist(selectedvalue);
        },
        showcardaccordingtolist: function (listid) {
            $('#checkedAll1').attr('checked', false);
            $("#showotherchecks").empty();
            $('#hiddenteamid').val(listid);
            var s = "card";
            $.ajax({
                type: "POST",
                url: "/DataTracker.svc/showarchiverecord",
                data: "{\"OperationStatus\": \"" + s + "\",\"listid\": \"" + listid + "\"}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    if (data.d.length > 0) {
                        $.each(data.d, function (index, item) {
                            $("#showotherchecks").append(" <div id=" + item.TaskID + "  class='member-list-item-detail'><div class='member member-no-menu'><span title='Project13' class='member-initials'>" + item.Title.charAt(0).toUpperCase() + "</span></div><div class='details' ><p  class='name-line' ><span   style='margin-top: 10px; line-height: 28px;' id=" + item.ProjectId + "  class='full-name' >" + item.Title + "</span></p></div><div class='options' ><a href='#' ><span class='FromTeamRemoveMember' ><input type='checkbox' class='checkSingle' id='ssssd' name='checkbox' value=" + item.TaskID + "   /></span></a></div><div class='clr'></div></div>");
                            $("#btnsubmit").show();
                            $(".showww").show();
                        });
                    } else {
                        $("#btnsubmit").hide();
                        $(".showww").hide();
                        $("#showotherchecks").append("No record found");
                    }
                }
            });
        },
        searcharcivetasks: function () {
            var s = "search";
            $("#showotherchecks").empty();
            var list = $("#hiddenteamid").val();
            var txtsearch = $("#searchtasks").val();
            if ($("#searchtasks").val() != "") {
                if (txtsearch.length > 0) {
                    $.ajax({
                        type: "POST",
                        url: "/DataTracker.svc/showarchiverecord",
                        data: "{\"OperationStatus\": \"" + s + "\",\"listid\": \"" + list + "\",\"txtsearch\": \"" + txtsearch + "\"}",
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (data) {
                            if (data.d.length > 0) {
                                $.each(data.d, function (index, item) {
                                    $("#showotherchecks").append(" <div id=" + item.TaskID + "  class='member-list-item-detail'><div class='details' ><p class='name-line' ><span class='full-name' >" + item.Title + "</span></p></div><div class='options' ><a href='#' ><span class='FromTeamRemoveMember' ><input type='checkbox' class='checkSingle' id='ssssd' name='checkbox' value=" + item.TaskID + "   onclick='insertshowtask()' /></span></a></div><div class='clr'></div></div>");
                                });

                            } else {
                                $("#btnsubmit").hide();
                                $(".showww").hide();
                                $("#showotherchecks").append("No record found");
                            }
                        }

                    });
                }
            } else {
                privateMethods.showcardaccordingtolist(list)
                $("#btnsubmit").hide();
                $(".showww").hide();
            }


        },
        insertshowtask: function () {
           
            var k = $('#ssssd:checked').map(function () { return this.value; }).get().join(',')
            var m = {};
            m = k.split(',');
            var allCardRemovedArchiveName = "";
            var allCardRemovedArchiveName2 = "";
            for (var i = 0; i < m.length; i++) {

                p = $('#' + m[i].toString() + '').text();
                var n = p.substring(1)

                allCardRemovedArchiveName = allCardRemovedArchiveName + n + ",";
                allCardRemovedArchiveName1 = allCardRemovedArchiveName.split(',');
                allCardRemovedArchiveName2 = allCardRemovedArchiveName1.pop();
                allCardRemovedArchiveName1 = allCardRemovedArchiveName1.join(',');

            }

            return k;
        },
        insertshowarchivetaskindatabase: function () {
           
            var taskid = privateMethods.insertshowtask();
            if (taskid != "") {
                var s = "checked";
                $.ajax({
                    type: "POST",
                    url: "/DataTracker.svc/undoarchivetasks",
                    data: "{\"OperationStatus\": \"" + s + "\",\"taskid\": \"" + taskid + "\"}",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {
                        $("#showotherchecks").empty();
                        var list = $("#hiddenteamid").val();
                        if (data.d == 1) {
                            privateMethods.showcardaccordingtolist(list);
                        }

                        UpdateActivityListItem(allCardRemovedArchiveName1, 30);
                    }
                });
            } else {
                alert("Please select atleast single Card");
                return false;
            }
        },
        checkall: function () {
           //  ;
            if ($("#checkedAll1").is(":checked")) {
                $('.checkSingle').each(function () { //loop through each checkbox
                    this.checked = true;  //select all checkboxes with class "checkbox1"               
                });
            } else {
                $('.checkSingle').each(function () { //loop through each checkbox
                    this.checked = false;  //uncheck all checkboxes with class "checkbox1"               
                });
            }

        },
        setInitialState: function () {
            var result = privateMethods.get_archive_card_from_qstring_onload();
            var arr = result.split('~');
            var listid = arr[0];
            var boardid = arr[1];
            privateMethods.show_list(listid, boardid);
            $('#hiddenteamid').val(listid);
        },
        attachEvents: function () {
            $("#dropdownlistid").bind("change", privateMethods.changofdropdown);
            $("#searchtasks").bind("keyup", privateMethods.searcharcivetasks);
            $("#btnsubmit").bind("click", privateMethods.insertshowarchivetaskindatabase);
            $("#ssssd").bind("click", privateMethods.insertshowtask);
            $("#checkedAll1").bind("click",privateMethods.checkall);
        }
    };
    var constructor = {
        index: function () {
            privateMethods.setInitialState();
            privateMethods.attachEvents();

        }
    }
    constructor.index();
}
$(document).ready(function () {
    var objTest = new TestController();
});


















