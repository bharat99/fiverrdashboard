$(function () {
    var date1=new Date();
    $('.single').pickmeup({
        flat: true
    });

    $('.input-clnder').pickmeup({
        position: 'bottom',
        hide_on_select: true,
        min: date1
    });

      $('.single1').pickmeup({
        flat: true
    });

    $('.input-clnder1').pickmeup({
        position: 'bottom',
        hide_on_select: true,
        max: date1,
        change: function (formatted) {
            $('#txtdate').val(formatted);
        }
    });
    $('.input-clnder2').pickmeup({
        position: 'bottom',
        hide_on_select: true,
        change: function (formatted) {
            FilterByEndDate(formatted);

        }
    });

    $('.StartDate').pickmeup({
        position: 'bottom',
        hide_on_select: true,
        change: function (formatted) {
            $('#hStartDate').val(formatted);
            $('#SpStartDate').text(formatted);
            min: date1
        }
    });
    $('.ClEndDate').pickmeup({
        position: 'bottom',
        hide_on_select: true,
        change: function (formatted) {
            $('#hEndDate').val(formatted);
            $('#SpEndDate').text(formatted);
            min: date1
        }
    });

    $('.JoingDate').pickmeup({
        position: 'bottom',
        hide_on_select: true,
        change: function (formatted) {
            $('#txtJoingDate').val(formatted);
            $('#txtJoingDate').text(formatted);
            min: date1
        }
    });

    $('.ReleavingDate').pickmeup({
        position: 'bottom',
        hide_on_select: true,
        change: function (formatted) {
            $('#txtLeavingDate').val(formatted);
            $('#txtLeavingDate').text(formatted);
            min: date1
        }
    });

    $('#txtDOB').pickmeup({
        position: 'bottom',
        hide_on_select: true,
        change: function (formatted) {
            $('#txtDOB').val(formatted);
            min: date1
        }
    });
    
    $('#dob').pickmeup({
        position: 'bottom',
        hide_on_select: true,
        change: function (formatted) {
            $('#dob').val(formatted);
            $('#dob').text(formatted);
            min: date1
        }
    });
    $('#txtDateofJoining').pickmeup({
        position: 'bottom',
        hide_on_select: true,
        change: function (formatted) {
            $('#txtDateofJoining').val(formatted);
            $('#txtDateofJoining').text(formatted);
            min: date1
        }
    });

    $('#txtDateofLeaving').pickmeup({
        position: 'bottom',
        hide_on_select: true,
        change: function (formatted) {
            $('#txtDateofLeaving').val(formatted);
            $('#txtDateofLeaving').text(formatted);
            min: date1
        }
    });
    $('#txtRetirementDate').pickmeup({
        position: 'bottom',
        hide_on_select: true,
        change: function (formatted) {
            $('#txtRetirementDate').val(formatted);
            $('#txtRetirementDate').text(formatted);
            min: date1
        }
    });

    $('#txtfrom_Add').pickmeup({
        position: 'bottom',
        hide_on_select: true,
        change: function (formatted) {
            $('#txtfrom_Add').val(formatted);
            $('#txtfrom_Add').text(formatted);
            min: date1
        }
    });
    $('#txtTo_Add').pickmeup({
        position: 'bottom',
        hide_on_select: true,
        change: function (formatted) {
            $('#txtTo_Add').val(formatted);
            $('#txtTo_Add').text(formatted);
            min: date1
        }
    });


    $('#txtfrom_Edit').pickmeup({
        position: 'bottom',
        hide_on_select: true,
        change: function (formatted) {
            $('#txtfrom_Edit').val(formatted);
            $('#txtfrom_Edit').text(formatted);
            min: date1
        }
    });
    $('#txtTo_Edit').pickmeup({
        position: 'bottom',
        hide_on_select: true,
        change: function (formatted) {
            $('#txtTo_Edit').val(formatted);
            $('#txtTo_Edit').text(formatted);
            min: date1
        }
    });
    $('#dob1').pickmeup({
        position: 'bottom',
        hide_on_select: true,
        change: function (formatted) {
            $('#dob1').val(formatted);
            $('#dob1').text(formatted);
            min: date1
        }
    });

    $('#txtDateofJoining_Edit').pickmeup({
        position: 'bottom',
        hide_on_select: true,
        change: function (formatted) {
            $('#txtDateofJoining_Edit').val(formatted);
            $('#txtDateofJoining_Edit').text(formatted);
            min: date1
        }
    });
    $('#txtDateofLeaving_Edit').pickmeup({
        position: 'bottom',
        hide_on_select: true,
        change: function (formatted) {
            $('#txtDateofLeaving_Edit').val(formatted);
            $('#txtDateofLeaving_Edit').text(formatted);
            min: date1
        }
    });

    
});
