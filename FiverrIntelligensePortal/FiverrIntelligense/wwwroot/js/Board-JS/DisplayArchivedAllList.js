﻿//show All Archived list

function showArchivelist() {

    $('#searchlist').val('');
    $("#searchlist").show();
    $("#alllistretriew").show();
    $("#archivelist").empty();
    $(".selectall_check").show();
    var Boardid = get_Projectid_from_qstring_onload('bid');
    $("#boardID").val(Boardid);
    var operation = "show";
    $.ajax({
        type: "POST",
        url: "/DataTracker.svc/bindboard",
        data: "{\"Boardid\": \"" + Boardid + "\"}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            $("#txtboardname").text(data.d);
            $("#txtboardname").attr('href', 'board.aspx?' + Boardid);
        }
    });
    $.ajax({
        type: "POST",
        url: "/DataTracker.svc/ShowAllArchivedlist",
        data: "{\"Boardid\": \"" + Boardid + "\",\"OperationStatus\": \"" + operation + "\"}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            if (data.d != 0) {
                ;
                $.each(data.d, function (index, item) {
                    var str = item.Projectname;
                    var firstletter = str.charAt(0);
                    $("#archivelist").append("<div class='member-list-item-detail'><div class='member member-no-menu' ><span title='" + item.Projectname + "' class='member-initials'>" + firstletter + "</span></div><div class='details' ><p class='name-line' ><input type='hidden' id='projectId' value=" + item.ProjectId + " /><span class='full-name' id=" + item.ProjectId + " >" + item.Projectname + "</span></p></div><div class='options' ><input type='checkbox' name='chkforselect' id='chkproject' ></div><div class='clr'></div></div>");
                });

            } else {
                $("#searchlist").hide();
                $("#alllistretriew").hide();
                $(".selectall_check").hide();
                $("#archivelistnorecod").show();
                $("#archivelistnorecod").append("No Record Found In Archived List");


            }
        }
    });

}

// bind list after search
function searchproject_from_grid() {
    $("#archivelist").empty();
    if ($("#searchlist").val() != "") {
        var projectname = $('#searchlist').val();
        var OperationStatus = "search";
        var Boardid = get_Projectid_from_qstring_onload('bid');
        var projid = get_Projectid_from_qstring_onload('Pid');
        if (projectname.length > 0) {
            $.ajax({
                type: "POST",
                url: "/DataTracker.svc/ShowAllArchivedlist",
                data: "{\"txtsearch\": \"" + projectname + "\",\"OperationStatus\": \"" + OperationStatus + "\",\"Boardid\": \"" + Boardid + "\",\"projectlist\": \"" + projid + "\"}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    $.each(data.d, function (index, item) {
                        var str = item.Projectname;
                        var firstletter = str.charAt(0);
                        $("#archivelist").append("<div class='member-list-item-detail'><div class='member member-no-menu' ><span title='" + item.Projectname + "' class='member-initials'>" + firstletter + "</span></div><div class='details' ><p class='name-line' ><input type='hidden' id='projectId' value=" + item.ProjectId + " /><span class='full-name' >" + item.Projectname + "</span></p></div><div class='options' ><input type='checkbox' name='chkforselect' id='chkproject' ></div><div class='clr'></div></div>");
                    });

                }
            });
        }
    } else {
        showArchivelist();
    }
}
// function for move archivelist to board
function retrieve() {
    var Projectid = " ";
    var Projectid1 = "";
    var ProjectidForName = " ";
    var allListRemovedArchiveName = " ";
    var allListRemovedArchiveName1 = " ";
    $('#archivelist input:checkbox').each(function () {
        Projectid = Projectid + (this.checked ? $(this).parent().siblings().first().next().find('input[type="hidden"]').val() : "0") + ",";
        Projectid2 = Projectid1 + (this.checked ? $(this).parent().siblings().first().next().find('input[type="hidden"]').val() : "0");
        if (Projectid2 != 0) {
            allListRemovedArchiveName = allListRemovedArchiveName + $('#' + Projectid2 + '').text() + ",";
            allListRemovedArchiveName1 = allListRemovedArchiveName.split(',');
            var allListRemovedArchiveName2 = allListRemovedArchiveName1.pop();
            allListRemovedArchiveName1 = allListRemovedArchiveName1.join(',');
        }
    });
    $.ajax({
        type: "POST",
        url: "/DataTracker.svc/UpdateArchivestatus",
        data: "{\"Projectid\": \"" + Projectid + "\"}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            showArchivelist()
            UpdateActivityListItem(allListRemovedArchiveName1, 29);

        }
    });

}

//function for select all checkbox
function selectallarchive() {
    if ($("#selectall").is(":checked")) {
        $("#archivelist :checkbox").attr('checked', true);
        return;
    }
    $("#archivelist :checkbox").attr('checked', false);
    return;
}
//To get projectid from q string onload 
function get_Projectid_from_qstring_onload(param) {
    ;
    var url = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
    for (var i = 0; i < url.length; i++) {
        var urlparam = url[i].split('=');
        if (urlparam[0] == param) {
            return urlparam[1];
        }
    }
}

//show archive list in case of no list 
function ShowlistofArchive() {
    var bid = $('#boardID').val();
    window.location.href = "displayArchivedAllList.aspx?bid=" + bid;
    return false;
}

//// Show Archived Swimlanes

function showArchivedswimlanes() {

    $('#searchlist').val('');
    $("#searchlist").show();
    $("#alllistretriew").show();
    $("#archivedSwimlanes").empty();
    $(".selectall_check").show();
    var Boardid = get_Projectid_from_qstring_onload('bid');
    $("#boardID").val(Boardid);
    var operation = "show";
    $.ajax({
        type: "POST",
        url: "/DataTracker.svc/bindboard",
        data: "{\"Boardid\": \"" + Boardid + "\"}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            $("#txtboardname").text(data.d);
            $("#txtboardname").attr('href', 'board.aspx?' + Boardid);
        }
    });
    $.ajax({
        type: "POST",
        url: "/DataTracker.svc/showArchivedswimlanes",
        data: "{\"Boardid\": \"" + Boardid + "\",\"OperationStatus\": \"" + operation + "\"}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            if (data.d != 0) {
                var res = JSON.parse(data.d);
                $.each(res, function (index, item) {
                    var str = item.sprint_name;
                    var firstletter = str.charAt(0);
                    $("#archivedSwimlanes").append("<div class='member-list-item-detail'><div class='member member-no-menu' ><span title='" + item.sprint_name + "' class='member-initials'>" + firstletter + "</span></div><div class='details' ><p class='name-line' ><input type='hidden' id='projectId' value=" + item.SprintBoardRelationid + " /><span class='full-name' id=" + item.SprintBoardRelationid + " >" + item.sprint_name + "</span></p></div><div class='options' ><input type='checkbox' name='chkforselect' id='chkproject' ></div><div class='clr'></div></div>");
                });

            } else {
                $("#searchlist").hide();
                $("#alllistretriew").hide();
                $(".selectall_check").hide();
                $("#archivelistnorecod").show();
                $("#archivelistnorecod").append("No Record Found In Archived List");


            }
        }
    });

}

function searchswimlanes_from_grid() {
    $("#archivedSwimlanes").empty();
    if ($("#searchlist").val() != "") {
        var Swimlanesname = $('#searchlist').val();
        var OperationStatus = "search";
        var Boardid = get_Projectid_from_qstring_onload('bid');
        var projid = get_Projectid_from_qstring_onload('Pid');
        if (Swimlanesname.length > 0) {
            $.ajax({
                type: "POST",
                url: "/DataTracker.svc/showArchivedswimlanes",
                data: "{\"txtsearch\": \"" + Swimlanesname + "\",\"OperationStatus\": \"" + OperationStatus + "\",\"Boardid\": \"" + Boardid + "\"}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    var res = JSON.parse(data.d);
                    $.each(res, function (index, item) {
                        var str = item.sprint_name;
                        var firstletter = str.charAt(0);
                        $("#archivedSwimlanes").append("<div class='member-list-item-detail'><div class='member member-no-menu' ><span title='" + item.sprint_name + "' class='member-initials'>" + firstletter + "</span></div><div class='details' ><p class='name-line' ><input type='hidden' id='projectId' value=" + item.SprintBoardRelationid + " /><span class='full-name' id=" + item.SprintBoardRelationid + " >" + item.sprint_name + "</span></p></div><div class='options' ><input type='checkbox' name='chkforselect' id='chkproject' ></div><div class='clr'></div></div>");
                    });

                }
            });
        }
    } else {
        showArchivedswimlanes();
    }
}

//function for select all checkbox
function selectallarchiveSwimlanes() {
    if ($("#selectall").is(":checked")) {
        $("#archivedSwimlanes :checkbox").attr('checked', true);
        return;
    }
    $("#archivedSwimlanes :checkbox").attr('checked', false);
    return;
}

function RollbackArchivedSwimlanes() {
    var Swimlanesid = " ";
    var Swimlanesid1 = "";
    var Swimlanesid2 = "";
    var ProjectidForName = " ";
    var allListRemovedArchiveName = " ";
    var allListRemovedArchiveName1 = " ";
    $('#archivedSwimlanes input:checkbox').each(function () {
        Swimlanesid = Swimlanesid + (this.checked ? $(this).parent().siblings().first().next().find('input[type="hidden"]').val() : "0") + ",";
        Swimlanesid2 = Swimlanesid1 + (this.checked ? $(this).parent().siblings().first().next().find('input[type="hidden"]').val() : "0");
        if (Swimlanesid2 != 0) {
            allListRemovedArchiveName = allListRemovedArchiveName + $('#' + Swimlanesid2 + '').text() + ",";
            allListRemovedArchiveName1 = allListRemovedArchiveName.split(',');
            var allListRemovedArchiveName2 = allListRemovedArchiveName1.pop();
            allListRemovedArchiveName1 = allListRemovedArchiveName1.join(',');
        }
    });
    $.ajax({
        type: "POST",
        url: "/DataTracker.svc/UpdateArchiveSwimlanes",
        data: "{\"SwimlanesID\": \"" + Swimlanesid + "\"}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            showArchivedswimlanes()
            UpdateActivityListItem(allListRemovedArchiveName1, 29);

        }
    });

}