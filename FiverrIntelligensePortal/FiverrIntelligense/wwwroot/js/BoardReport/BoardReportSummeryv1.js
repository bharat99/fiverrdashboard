﻿$(document).ready(function () {
    var d = new Date();
    var month = d.getMonth() + 1;
    var day = d.getDate();
    var CurrentDate = d.getFullYear() + '-' +
        (('' + month).length < 2 ? '0' : '') + month + '-' +
        (('' + day).length < 2 ? '0' : '') + day;
    var today = new Date();
    var priorDate = new Date();
    priorDate.setDate(priorDate.getDate() - 7);
    var lastSevendayMonth = priorDate.getMonth() + 1;
    var lastSevanday = priorDate.getDate();
    var lastSevanDayDate = priorDate.getFullYear() + '-' +
      (('' + lastSevendayMonth).length < 2 ? '0' : '') + lastSevendayMonth + '-' +
      (('' + lastSevanday).length < 2 ? '0' : '') + lastSevanday;
    $('#SpStartDate').text(lastSevanDayDate);
    $('#SpEndDate').text(CurrentDate);
    SelectBoard();
    TimeSheetDetail();
    TotalEffort();
    $('#timesheet-report').click(function () {

        $('.list-view').toggle();


    })

    $('.list-view-up').click(function () {

        $('.list-view-up').toggleClass('list-view-down')

    })
});

function OnSelectChng() {
    var days = $("#SelectDays").children(":selected").attr("id");
    var d = new Date();
    var addmonth = d.getMonth() + 1;
    var adddays = d.getDate();
    var CurrentDate = d.getFullYear() + "-" + addmonth + "-" + adddays;
    StartDate = "";
    EndDate = "";

    if (days == 6) {
        addmonth = d.getMonth() + 1;
        adddays = d.getDate() - 6;
        if (adddays < 0) {
            addmonth = d.getMonth();
            adddays = 30 + adddays;
            StartDate = d.getFullYear() + "-" + addmonth + "-" + adddays;
            EndDate = CurrentDate;
        }
        else {
            StartDate = d.getFullYear() + "-" + addmonth + "-" + adddays;
            EndDate = CurrentDate;
        }

    }
    else if (days == 14) {
        addmonth = d.getMonth() + 1;
        adddays = d.getDate() - 14;
        if (adddays < 0) {
            addmonth = d.getMonth();
            adddays = 30 + adddays;
            StartDate = d.getFullYear() + "-" + addmonth + "-" + adddays;
            EndDate = CurrentDate;
        }
        else {
            StartDate = d.getFullYear() + "-" + addmonth + "-" + adddays;
            EndDate = CurrentDate;
        }

    }
    else if (days == 29) {

        addmonth = d.getMonth() + 1;
        adddays = d.getDate() - 29;
        if (adddays < 0) {
            addmonth = d.getMonth();
            adddays = 30 + adddays;
            StartDate = d.getFullYear() + "-" + addmonth + "-" + adddays;
            EndDate = CurrentDate;
        }
        else {
            StartDate = d.getFullYear() + "-" + addmonth + "-" + adddays;
            EndDate = CurrentDate;
        }
    }
    $('#hStartDate').val("");
    $('#hEndDate').val("");
    $('#SpStartDate').text(StartDate);
    $('#SpEndDate').text(EndDate);
}
function TotalEffort() {
    //var BoardId = "396";
    var BoardId = $("#SelectBoard").children(":selected").attr("id");
    var StartDate = $('#SpStartDate').text(); //2017-03-29
    var EndDate = $('#SpEndDate').text();
    var days = $("#SelectDays").children(":selected").attr("id");
    var Tafforts = "";

    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "/DataTracker.svc/TotalEffortByBoard",
        data: "{\"BoardId\":\"" + BoardId + "\",\"StartDate\":\"" + StartDate + "\",\"EndDate\":\"" + EndDate + "\",\"days\":\"" + days + "\"}",
        dataType: "json",
        success: function (data) {
            $("#TottalEffort").empty();
            $("#TottalEffort").append("<tr class=\"list-view-color\"><th>Person Name</th><th>Efforts</th></tr>");
            $.each(data.d, function (data, t) {
                $("#TottalEffort").append("<tr><td>" + t.PersonName + "</td><td>" + t.Effort + "</td></tr>");
                Tafforts = t.Taffort;
            });
            if (Tafforts == "")
            {
                Tafforts = "0 hr 0 min";
                $("#TottalEffort").append("<tr><td align=\"Center\">No data available</td></tr>");
            }
            $("#TottalEffort").append("<tr class=\"belt-color\"><td>Total</td><td>" + Tafforts + "</td></tr>");
            $("#lblTotalEfforst").text("Total Efforts( " + Tafforts + " )");
        },
        error: function (error) {
            alert("Error");
        }
    })

}


//function TimeSheetDetail() {
//    //var BoardId = "396";
//    var BoardId = $("#SelectBoard").children(":selected").attr("id");
//    var StartDate = ""; //$('#hStartDate').val();
//    var EndDate = ""; //$('#hEndDate').val();
//    var days = $("#SelectDays").children(":selected").attr("id");

//    $.ajax({
//        type: "POST",
//        contentType: "application/json; charset=utf-8",
//        url: "/DataTracker.svc/TimeSheetDetailByBoard",
//        data: "{\"BoardId\":\"" + BoardId + "\",\"StartDate\":\"" + StartDate + "\",\"EndDate\":\"" + EndDate + "\",\"days\":\"" + days + "\"}",
//        dataType: "json",
//        success: function (data) {
//            $("#TimesheetDetailTable").empty();
//            $("#TimesheetDetailTable").append("<thead><tr class=\"blue\"><th>Person Name</th><th>Card Name</th><th>date</th><th>Efforts</th></tr></thead><tbody>");
//            $.each(data.d, function (data, t) {
//                $("#TimesheetDetailTable tbody").append("<tr><td style=\"width:15%\">" + t.PersonName + "</td><td>" + t.TaskName + "</td><td style=\"width:10%\">" + t.Date + "</td><td style=\"width:10%\">" + t.Effort + "</td></tr>");
//            });
//            $("#TimesheetDetailTable").append("</tbody>");

//            $("#TimesheetDetailTable").dataTable({
//                "bPaginate": true,
//                "bInfo": true,
//                "bFilter": false,
//                "bLengthChange": false,
//                "iDisplayLength": 50,
//                "pagingType": "full_numbers"
//            });
//        },
//        error: function (error) {
//        }
//    })

//}

function TimeSheetDetail() { //TimeSheetDetailonSlect
    //var BoardId = "396";
    var BoardId = $("#SelectBoard").children(":selected").attr("id");
    var StartDate = $('#SpStartDate').text(); //2017-03-29
    var EndDate = $('#SpEndDate').text();
    //EndDate = "2017-04-20";
    var days = $("#SelectDays").children(":selected").attr("id");
    var d = new Date();
    var addmonth = d.getMonth() + 1;
    var adddays = d.getDate() + 1;
    var CurrentDate = d.getFullYear() + "-" + addmonth + "-" + adddays;
    if (new Date(EndDate) > new Date(CurrentDate))
    { alert("End date can not be future date."); return false; }

    if (new Date(StartDate) > new Date(EndDate))
    { alert("Start date must be less then End date."); return false; }

    if (StartDate == "" && EndDate != "")
    { alert("Please select start date."); return false; }
    else if (EndDate == "" && StartDate != "")
    { alert("Please select End date."); return false; }

    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "/DataTracker.svc/TimeSheetDetailByBoard",
        data: "{\"BoardId\":\"" + BoardId + "\",\"StartDate\":\"" + StartDate + "\",\"EndDate\":\"" + EndDate + "\",\"days\":\"" + days + "\"}",
        dataType: "json",
        success: function (data) {
            TotalEffort();
            $("#TimesheetDetailTable").empty();
            $("#TimesheetDetailTable").append("<thead><tr class=\"blue\"><th>Person Name</th><th>Card Name</th><th>Date</th><th>Efforts</th></tr></thead><tbody>");
            $.each(data.d, function (data, t) {
                $("#TimesheetDetailTable tbody").append("<tr><td style=\"width:15%\">" + t.PersonName + "</td><td>" + t.TaskName + "</td><td style=\"width:10%\">" + t.Date + "</td><td style=\"width:10%\">" + t.Effort + "</td></tr>");
            });
            $("#TimesheetDetailTable").append("</tbody>");

            $("#TimesheetDetailTable").dataTable({
                "aoColumns": [
               { "bSortable": true },
               { "bSortable": true },
               { "bSortable": false },
               { "bSortable": false }],
                "destroy": true,
                "bPaginate": true,
                "bInfo": true,
                "bFilter": false,
                "bLengthChange": false,
                "iDisplayLength": 50,
                "pagingType": "full_numbers"
            });    
        },
        error: function (error) {
        }
    })
}

function SelectBoard() {
    var url = window.location.href.slice(window.location.href.indexOf('?') + 1).split('=');
    var Bid = url[1];
    var SelctedBoardName = "";
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "/DataTracker.svc/SelectBoardByUser",
        data: "{}",
        dataType: "json",
        async: false,
        success: function (data) {
            $.each(data.d, function (data, t) {
                $("#SelectBoard").append("<option id=" + t.BoardId + " value=" + t.BoardName + ">" + t.BoardName + "</option>");
            });
            $("#SelectBoard > [id=" + Bid + "]").attr("selected", "true");
        },
        error: function (error) {
        }
    })
}

