﻿
function OpenScreenshotWindow(sdate, uname) {
    if ($("#DivforSlidScreenshot").data("kendoWindow") != null) {
        var dialogtest = $("#DivforSlidScreenshot").data("kendoWindow");
        dialogtest.close();
    }

    var Ptitle = "ScreenShots (" + uname + ")";
    var windowElement = $("#DivforSlidScreenshot").kendoWindow({
        iframe: true,
        content: "ScreenShotSlider.htm?Sdate=" + sdate + "",
        modal: true,
        width: "970px",
        title: "ScreenShots",
        height: "500px",
        position: {
            top: 5,
            left: 2,
            bottom: 5
        },
        resizable: false,
        actions: ["Close"],
        draggable: false

    });
    var dialog = $("#DivforSlidScreenshot").data("kendoWindow");
    dialog.setOptions({
        title: Ptitle
    });
    // $('#DivforSlidScreenshot').data('kendoWindow').center();


    //     $('#DivforSlidScreenshot').closest(".k-window").css({
    //         position: 'fixed',
    //       // margin: 'auto',
    //        top: 5,
    //        left: 2,
    //        bottom: 5,
    //        minWidth: '970px'

    //    });
    dialog.open();
    dialog.center();
    return false;
    // }
}
////manoranjan
function OpenScreenshotWindow(sdate, uname) {
    if ($("#DivforSlidScreenshot").data("kendoWindow") != null) {
        var dialogtest = $("#DivforSlidScreenshot").data("kendoWindow");
        dialogtest.close();
    }
    if (uname != "" && uname != null)
        var Ptitle = "ScreenShots (" + uname + ")";
    else
        var Ptitle = "ScreenShots";
    var windowElement = $("#DivforSlidScreenshot").kendoWindow({
        iframe: true,
        content: "ScreenShotSlider.htm?Sdate=" + sdate + "",
        modal: true,
        width: "920px",
        title: "ScreenShots",
        height: "500px",
        position: {
            top: 5,
            left: 2,
            bottom: 5
        },
        resizable: false,
        actions: ["Close"],
        draggable: false

    });
    var dialog = $("#DivforSlidScreenshot").data("kendoWindow");
    dialog.setOptions({
        title: Ptitle
    });
    //    $('#DivforSlidScreenshot').closest(".k-window").css({
    //        position: 'fixed',
    //       // margin: 'auto',
    //        top: 5,
    //        left: 2,
    //        bottom: 5,
    //        minWidth: '970px'

    //    });
    dialog.open();
    dialog.center();
    return false;
    // }
}

function Getallurlofscreenshot(sdate, ldate, ID, typecall) {
    $('#stDate').val(sdate);
    $('#userid').val(ID);
    $.ajax({

        type: "POST",
        url: "/DataTracker.svc/GetAllScreenShot",
        data: "{\"Userid\": \"" + ID + "\",\"Sdate\": \"" + encodeURIComponent(escape(sdate)) + "\",\"Ldate\": \"" + encodeURIComponent(escape(ldate)) + "\",\"tobecall\": \"" + encodeURIComponent(escape(typecall)) + "\"",
        contentType: "application/json; charset=utf-16",
        dataType: "json",
        success: function (data) {
            var res = data.d;
            if (res != null) {
                $.each(data.d, function (index, item) {
                    var imggg = "<img src='" + item.ScreenshotUrl + "' width=720 height=420 title='" + item.ScreenshotTime + "'>";
                    images.push(imggg);

                });
                addimagess();
                // return "ok";
            }
            else {
                noImagess();
                return false;
            }
        },
        failure: function (response) {
            noImagess();
            //alert(response);
        },
        error: function (xhr, status, error) {
            noImagess();
            // alert(error);
        }
    });
}
function noImagess() {

    $("#outside")[0].style.display = "none";
    $("#Divfornoumage")[0].style.display = "block";

}

var images = [];
function addimagess() {
    for (var i = 0; i < images.length; i++) {
        $('.cycle-slideshow').cycle('add', images[i]);
    }
}



function countallstrng() {
    var cc = $("#custom-pager strong ").length;
    alert(cc);
}


var name = "";
function OpenScreenshotWindow(id) {
   
    var boardID = $('#boardID').val();
    var returnsingleval = "";
    var ZeroTime = $('#settimecolor' + id).text();

    if (ZeroTime != "00:00") {
        $.ajax({
            type: "GET",
            url: "/DataTracker.svc/CheckOwnerId?boardid=" + boardID + "",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: false,
            success: function (data) {

                returnsingleval = data.d;

            },
            error: function (error) {

            }
        });

        var userid = $('#cuserid').val();
        var uid = $('#usertypeid').val();

        if (uid == 12 || uid == 14) {

            var someVarName = id;
            localStorage.setItem("someVarName", someVarName);

            var sdate = "";
            var str = "";
            name = "";
            $.ajax({
                type: "GET",
                url: "/DataTracker.svc/GetActiveUserName?task_id=" + someVarName + "",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                async: false,
                success: function (data) {
                    if (data.d.length > 0) {
                        $.each(data.d, function (index, item) {
                           
                            name += item.UserName + "+";
                            str = name.substring(0, name.length - 1);
                        });


                    }


                },
                error: function (error) {

                }
            });

            var uname = str;
            if ($("#DivforSlidScreenshotInCard").data("kendoWindow") != null) {
                var dialogtest = $("#DivforSlidScreenshotInCard").data("kendoWindow");
                dialogtest.close();
            }

            var Ptitle = "ScreenShots (" + uname + ")";
            var windowElement = $("#DivforSlidScreenshotInCard").kendoWindow({
                iframe: true,
                content: "ScreenShotSliderCard.html?Sdate=" + sdate + "",
                modal: true,
                width: "970px",
                title: "ScreenShots",
                height: "500px",
                position: {
                    top: 5,
                    left: 2,
                    bottom: 5
                },
                resizable: false,
                actions: ["Close"],
                draggable: false

            });
            var dialog = $("#DivforSlidScreenshotInCard").data("kendoWindow");
            dialog.setOptions({
                title: Ptitle

            });
            dialog.center();
            dialog.open();


            return false;
        }
        else if (returnsingleval == userid) {
            var someVarName = id;
            localStorage.setItem("someVarName", someVarName);

            var sdate = "";
            var str = "";
            name = "";
            $.ajax({
                type: "GET",
                url: "/DataTracker.svc/GetActiveUserName?task_id=" + someVarName + "",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                async: false,
                success: function (data) {
                    if (data.d.length > 0) {
                        $.each(data.d, function (index, item) {
                            name += item.UserName + "+";
                            str = name.substring(0, name.length - 1);
                        });


                    }


                },
                error: function (error) {

                }
            });




            var uname = str;
            if ($("#DivforSlidScreenshotInCard").data("kendoWindow") != null) {
                var dialogtest = $("#DivforSlidScreenshotInCard").data("kendoWindow");
                dialogtest.close();
            }

            var Ptitle = "ScreenShots (" + uname + ")";
            var windowElement = $("#DivforSlidScreenshotInCard").kendoWindow({
                iframe: true,
                content: "ScreenShotSliderCard.html?Sdate=" + sdate + "",
                modal: true,
                width: "970px",
                title: "ScreenShots",
                height: "500px",
                position: {
                    top: 5,
                    left: 2,
                    bottom: 5
                },
                resizable: false,
                actions: ["Close"],
                draggable: false

            });
            var dialog = $("#DivforSlidScreenshotInCard").data("kendoWindow");
            dialog.setOptions({
                title: Ptitle

            });
            dialog.center();
            dialog.open();


            return false;
        }
    }
}

function GetScreenShotInCard() {
  
    var card_id = localStorage.getItem("someVarName");
    // var card_id = $('#cardid').val();

    if (card_id > 0) {
        $('#DivforSlidScreenshotInCard').empty();
        $.ajax({
            type: "GET",
            url: "/DataTracker.svc/GetAllScreenShotInCard?task_id=" + card_id + "",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: false,
            success: function (data) {
                if (data.d.length > 0) {
                    $.each(data.d, function (index, item) {

                        var imggg = "<img src='" + item.FileName + "' width=720 height=420 title='" + item.CreatedDate + "'>";
                        images1.push(imggg);

                    });

                    addimagess1();
                }
                else {
                    noImagess();
                    return false;
                }


            },
            error: function (error) {
                noImagess();
            }
        });
    }

}

var images1 = [];
function addimagess1() {

    for (var i = 0; i < images1.length; i++) {
        
        $('.cycle-slideshow').cycle('add', images1[i]);
    }
}

var storedate = "";
function Getallscreenshot(sdate, ID) {
    $("#NextPrivButton")[0].style.display = "block";
    $('#stDate').val(sdate);
    $('#userid').val(ID);
    var stDate = $('#stDate').val();
    var uid = $('#userid').val();
    $.ajax({
        type: "POST",
        url: "/DataTracker.svc/GetAllScreenShotUserWise",
        data: "{\"startdate\":\"" + stDate + "\",\"userid\":\"" + uid + "\"}",
        contentType: "application/json; charset=utf-16",
        dataType: "json",
        success: function (data) {
          if (data.d.length > 0) {
                images1.length = 0;
                $.each(data.d, function (index, item) {
                    var imggg = "<img src='" + item.FileName + "' width=720 height=420 title='" + item.CreatedDate + "'>";
                    images1.push(imggg);
                });
                addimagess1();
            }
            else {
                noImagess();
                return false;
            }
        },
        error: function (error) {
            noImagess();
        }
    });
}

function ScreenShotDateWise() {
    var stDate = $('#stDate').val();
    var uid = $('#userid').val();
    $.ajax({
        type: "POST",
        url: "/DataTracker.svc/GetScreenShotInNextDateWise",
        data: "{\"startdate\":\"" + stDate + "\",\"userid\":\"" + uid + "\"}",
        contentType: "application/json; charset=utf-16",
        dataType: "json",
        success: function (data) {
            if (data.d.length > 0) {
                images1.length = 0;
                var arr = $('div.cycle-slideshow img');

                for (i = 0; i < arr.length; i++) {
                    $(arr[i]).remove();

                }
                $('#custom-pager').empty();
                $('.cycle-slideshow').cycle('reinit');
                $("#outside")[0].style.display = "block";
                $("#Divfornoumage")[0].style.display = "none";


                //$('#custom-pager >strong').empty();
                //$('.cycle-slideshow').empty();
                //$('.cycle-slideshow > img').empty();

                $.each(data.d, function (index, item) {

                    var imggg = "<img src='" + item.FileName + "' width=720 height=420 title='" + item.CreatedDate + "'>";
                  
                    images1.push(imggg);

                });

                addimagess1();
            }
            else {
                noImagess();
                return false;
            }


        },
        error: function (error) {
            noImagess();
        }
    });
    
}



function ScreenShotDateWiseprivious() {
    var uid = $('#userid').val();
    var stDate = $('#stDate').val();
    $.ajax({
        type: "POST",
        url: "/DataTracker.svc/GetScreenShotInPriviousDateWise",
        data: "{\"startdate\":\"" + stDate + "\",\"userid\":\"" + uid + "\"}",
        contentType: "application/json; charset=utf-16",
        dataType: "json",
        success: function (data) {
            if (data.d.length > 0) {
                images1.length = 0;
                var arr = $('div.cycle-slideshow img');

                for (i = 0; i < arr.length; i++) {
                    $(arr[i]).remove();

                }
                $('#custom-pager').empty();
                $('.cycle-slideshow').cycle('reinit');
                $("#outside")[0].style.display = "block";
                $("#Divfornoumage")[0].style.display = "none";


                //$('#custom-pager >strong').empty();
                //$('.cycle-slideshow').empty();
                //$('.cycle-slideshow > img').empty();

                $.each(data.d, function (index, item) {
                   
                    var imggg = "<img src='" + item.FileName + "' width=720 height=420 title='" + item.CreatedDate + "'>";
                    images1.push(imggg);

                });
                addimagess1();
            }
            else {
                noImagess();
                return false;
            }


        },
        error: function (error) {
            noImagess();
        }
    });
}

