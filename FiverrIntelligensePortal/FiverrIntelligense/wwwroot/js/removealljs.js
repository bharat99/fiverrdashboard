﻿////////////Datareport js _start
function exportjobdata() {
    document.getElementById("btnexportads").innerHTML = "Please Wait...";
    var selecteddateexjob = $("#datepicker").val()
    $.ajax({
        type: "POST",
        url: "/DataTracker.svc/Exportmissingjobs",
        data: "{\"jobdate\": \"" + selecteddateexjob + "\"",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            if (data.d != null) {
                window.location.href = 'Downloadfiles.aspx?Missingads=' + data.d;
                document.getElementById("btnexportads").innerHTML = "Export Missing Ads";
            }
            else {
                document.getElementById("btnexportads").innerHTML = "Export Missing Ads";
                alert("No Records Found");
            }
        },
        Error: function (data) {
            alert("Data Cannot Be Exported")
        }
    });

}

function getjobdata() {
    if ($("#datepicker").val() != "") {
        $("#btnexportads").show();
        $("#btnexporturls").hide();
        var selecteddate = $("#datepicker").val()
        var dataSourceStore = "";
        dataSourceStore = new kendo.data.DataSource({
            transport: {
                read: {

                    url: "/DataTracker.svc/BindJobNumber",
                    data: {

                        date: selecteddate

                    }
                }
            },
            schema: {
                data: "d",
                total: function (d) {
                    if (d.d.length > 0)
                        return d.d[0].TotalCount

                },
                model: {
                    id: "ID",
                    fields: {

                        Website: { editable: false, type: "string" },
                        Job_Number: { editable: false, type: "string" },
                        ZipCode: { editable: true, type: "string" },
                        StoreAddress: { editable: false, type: "string" }

                    }
                }
            },
            pageSize: 20,
            serverPaging: true
        });

        var element = $("#jobDategrid").kendoGrid({
            dataSource: dataSourceStore,
            detailInit: ListwebsiteInit,
            dataBound: function () {

            },
            pageable: {
                batch: true,
                pageSize: 20,
                refresh: true,
                pageSizes: true,
                input: true
            },
            columns: [
                            {
                                field: "Website",
                                title: "Website",
                                width: "65%"

                            },
                            {
                                field: "counter",
                                title: "Total JobNumber",
                                width: "35%"
                            }
                            ],
            editable: false,
            sortable: true,
            selectable: true

        });
        $("#btnexportads").show();
        $("#btnexporturls").hide();
    }
    else
    { alert("PLease Select Date"); }
}
function ListwebsiteInit(e) {
    var sdate = $("#datepicker").val()
    $("<div/>").appendTo(e.detailCell).kendoGrid({
        dataSource: {
            transport: {
                read: {
                    //using jsfiddle echo service to simulate JSON endpoint
                    url: "/DataTracker.svc/Getwebsitedetail",
                    data: {

                        website: e.data.Website,
                        date: sdate
                    }
                }
            },
            serverPaging: true,
            serverSorting: true,
            serverFiltering: true,
            pageSize: 5,
            schema: {
                data: "d",
                total: function (d) {
                    if (d.d.length > 0)
                        return d.d[0].TotalCount
                },
                model: {
                    id: "ID",
                    fields: {

                        Website: { editable: false, type: "string" },
                        Job_Number: { editable: false, type: "string" },
                        ZipCode: { editable: false, type: "string" },
                        StoreAddress: { editable: false, type: "string" },
                        Type: { editable: false, type: "string" }
                    }
                }
            }
        },
        pageSize: 5,
        pageable: true,
        dataBound: function () {
        },
        columns: [
                            {
                                field: "Website",
                                title: "Website",
                                width: "25%"
                            },
                            {
                                field: "Job_Number",
                                title: "Job Number",
                                width: "20%"
                            },
                            {
                                field: "ZipCode",
                                title: "ZipCode",
                                width: "15%"
                            },
                            {
                                field: "StoreAddress",
                                title: "Store Address",
                                width: "25%"
                            },
                            {
                                field: "Type",
                                title: "Ads Type",
                                width: "15%"
                            },

                            ],
        editable: !IsClient,
        selectable: true

    });
}
/////////WorkitemStatusComment page js and StatusComment
function checkallspecialchar(srt) {
    var cnt = 0;
    var arrr = ['!', '@', '#', '$', '%', '^', '&', '*', '~', '>', '<', '.', '+', ',', '-', '{', '}', '(', ')', '_'];

    for (i = 0; i <= srt.length; i++) {
        var srtvalu = srt.charAt(i);
        if (arrr.indexOf(srtvalu) != -1)
            cnt++;
        else {

        }

    }
    return cnt;
}
