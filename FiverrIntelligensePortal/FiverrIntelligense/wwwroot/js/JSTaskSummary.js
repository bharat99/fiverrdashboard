﻿function displayallcount() {
    $("#maindiv")[0].innerHTML = "";
    if ($("#maindiv").data("kendoGrid") != null) {
        $("#maindiv").data().kendoGrid.destroy();
        $('#maindiv').empty();

    }
    var datasourceDetail = "";
    datasourceDetail = new kendo.data.DataSource({

        transport: {
            read: {
                url: "/DataTracker.svc/GetTaskCOuntByStatus",
                data: {

            }
        }
    },
    schema: {

        data: "d",
        total: function (d) {
            if (d.d.length > 0)
                return d.d[0].TotalCount
        },
        model: {
            id: "ID",
            fields: {
                newtaskcount: { editable: false, type: "string" },
                analysis: { editable: false, type: "string" },
                inprogresstask: { editable: false, type: "string" },
                reopentask: { editable: false, type: "string" }
            }
        }
    }

});
if ($("#maindiv").length > 0)
{ $("#maindiv")[0].innerHTML = ""; }
var element = $("#maindiv").kendoGrid({
    dataSource: datasourceDetail,
    dataBound: function () {
    },
    pageable: {
        batch: true,
        refresh: true,
        input: true
    },
    columns: [
                            {
                                field: "newtaskcount",
                                title: "NewTask",
                                width: "20%"
                            },
                            {
                                field: "analysis",
                                title: "Anaiysis",
                                width: "25%"

                            },
                                {
                                    field: "inprogresstask",
                                    title: "INProgress",
                                    width: "15%"

                                },
                                {
                                    field: "reopentask",
                                    title: "ReOpen",
                                    width: "40%"
                                }




                        ],
    editable: true,
    sortable: true,
    selectable: true

});

}