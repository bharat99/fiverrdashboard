﻿$(document).ready(function () {
    //Current date
    var d = new Date();
    var month = d.getMonth() + 1;
    var day = d.getDate();
    var CurrentDate = d.getFullYear() + '-' + (('' + month).length < 2 ? '0' : '') + month + '-' + (('' + day).length < 2 ? '0' : '') + day;
    var today = new Date();

    //Last 30 days
    var priorDate1 = new Date();
    priorDate1.setDate(priorDate1.getDate() - 30);
    var lastThirtyDayMonth = priorDate1.getMonth() + 1;
    var lastThirtyDay = priorDate1.getDate();
    var lastThirtyDayDate = priorDate1.getFullYear() + '-' + (('' + lastThirtyDayMonth).length < 2 ? '0' : '') + lastThirtyDayMonth + '-' + (('' + lastThirtyDay).length < 2 ? '0' : '') + lastThirtyDay;
    $('#startdate').val(lastThirtyDayDate);
    $('#enddate').val(CurrentDate);

    $('#startdate, #s_date').pickmeup({
        position: 'bottom',
        hide_on_select: true,
        change: function (formatted) {
            $('#startdate').val(formatted);
            min: date1
        }
    });
    $('#enddate, #e_date').pickmeup({
        position: 'bottom',
        hide_on_select: true,
        change: function (formatted) {
            $('#enddate').val(formatted);
            min: date1
        }
    });

    GetCardsCounts();
});

function GetCardsCounts() {
    var sdate = $('#startdate').val();
    var edate = $('#enddate').val();
    $.ajax({
        type: "POST",
        url: "/DataTracker.svc/GetCardsCounts",
        data: JSON.stringify({ Startdate: sdate, EndDate: edate }),
        dataType: 'json',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            var d = JSON.parse(data.d);
            $('#tblCardscount').empty();
            $('#tblCardscount').append("<thead><tr><th>Board Name</th><th>Total Cards</th><th>Owner Name</th><th>Team Name</th></tr></thead><tbody>");
            $.each(d, function (index, item) {
                $('#tblCardscount').append("<tr><td>" + item.Boardname + "</td><td>" + item.Totalcards + "</td><td>" + item.Owner + "</td><td>" + item.Team + "</td></tr>");
            });
            $('#tblCardscount').append("</tbody>");
        },
        error: function (data) {
        }
    });
}