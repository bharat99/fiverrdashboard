﻿function ddl_List_Bind(Result) {
    var item = Result.split('~');
    var s = 's';
    $("#dropdownlistid").empty();
    $.ajax({
        type: "POST",
        url: "/DataTracker.svc/showarchiverecord",
        data: "{\"OperationStatus\": \"" + s + "\",\"boardid\": \"" + item[1] + "\"}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            $.each(data.d, function (index, item) {
                //if (index == 0) {
                //    $("#boarddd").empty();
                //    $("#boardnmsec").append("<a id='txtTeamName1' href='board.aspx?" + boardid + "' class='upperpart1' >" + item.BoardName + "</a>");
                //}
                $("#dropdownlistid").append($("<option></option>").val(item.ProjectID).html(item.ProjectType));
            });
            $("#dropdownlistid").val(item[0]);
            var selectedText = $("#dropdownlistid").find("option:selected").text();
            showmembersonloadaccordingtolist(item[0]);
        }
    });
}

function get_teamid_from_qstring_onload() {
    ;
    var url = window.location.href;
    var teamid = url.substring(url.lastIndexOf('=') + 1);
    AllBoardsInTeam(teamid);
    teamid = teamid.replace("#", "");
    return teamid;
}

function get_id_from_qstring_onload() {
     
    var url = window.location.href;
    var listid = url.substring(url.lastIndexOf('=') + 1);
    var qs = url.split('?')[1];
    var parts = qs.split('&');
    var arr = [];
    $.each(parts, function () {
        var val = this.split('=')[1];
        arr.push(val);
    });
    var boardid = arr[0];
    $("#boardID").val(boardid);
    var result = listid + "~" + boardid;
    return result;
}

//To change event of team dropdown 
function changeforfunction(selectedtext, selectedvalue) {
    $("#showuser").empty();
    $('#hiddenteamid').val('');
    $('#txtTeamName1').text(selectedtext);
    $('#hiddenteamid').val(selectedvalue);
    var teamid = selectedvalue;
    showmembersonloadaccordingtoteam(teamid);
}

function showuserlistforsearch(sender, arg) {
    ;
    $("#appendmembers").empty();
    var userlist = $("#txteditmembers").val();
    $(".org-members-page-layout-list").empty();
    if (userlist.length > 0) {
        $.ajax({
            type: "GET",
            url: "/DataTracker.svc/GetmembernameonTeam?membersname=" + userlist + "",
            dataType: "json",
            async: false,
            success: function (data) {
                if (data.d.length > 0) {
                    $.each(data.d, function (index, item) {
                        var str = item.Email;
                        var firstletter = str.charAt(0);
                        var username = item.username;
                        $("#appendmembers").append("<span onclick='assignusertolistonselection(" + item.userid + ");'> <a href='#'><h2>" + firstletter + "</h2><div class='member_id'><p title='" + item.Email + "'>" + item.Email + "</p></div><div class='clr'></div></a></span>");
                        $("#appendmembers").show();
                    });
                }
            },
            error: function (error) { alert('Error has occurred!'); alert(JSON.stringify(error)) }
        });
    }
}

function assignusertolistonselection(_userid) {
    
    var _listid = $('#dropdownlistid').val();
    var i = "i";
    $.ajax({
        type: "POST",
        url: "/DataTracker.svc/AddEditDeleteassignedmemberToList",
        data: JSON.stringify({ OperationStatus: i, userid: _userid, listid: _listid, txtsearch: '' }),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            if (data.d != 0) {
                $("#showuser").empty();
                showmembersonloadaccordingtolist(data.d);
            } else {
                alert("This User Is Already Assigned To This List");
            }
        }
    });
}

function showmembersonloadaccordingtolist(listid) {
    $("#showuser").empty();
    var s = "s";
    var txtsearch = "";
    var userid = 0;
    $.ajax({
        type: "POST",
        url: "/DataTracker.svc/ShowAssignUsersTolist",
        data: "{\"OperationStatus\": \"" + s + "\",\"userid\": \"" + userid + "\",\"listid\": \"" + listid + "\",\"txtsearch\": \"" + txtsearch + "\"}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
             
            if (data.d != 0) {
                $.each(data.d, function (index, item) {
                    var str = item.Email;
                    var firstletter = str.charAt(0).toUpperCase();
                    $("#showuser").append(" <div id=" + index + " class='member-list-item-detail' data-reactid='.8.0.1.0.0'><div class='member member-no-menu' data-reactid='.8.0.1.0.0.0'><span title='" + item.UserName + " (" + item.Email + ")' class='member-initials' data-reactid='.8.0.1.0.0.0.0'>" + firstletter + "</span><span title='This member is an admin of this team.' class='member-type admin' data-reactid='.8.0.1.0.0.0.1'></span><span title='This member has Trello Gold.' class='member-gold-badge' data-reactid='.8.0.1.0.0.0.2'></span></div><div class='details' data-reactid='.8.0.1.0.0.1'><p class='name-line' data-reactid='.8.0.1.0.0.1.0'><span class='full-name' data-reactid='.8.0.1.0.0.1.0.0'>" + item.UserName + "</span></p><p class='u-bottom quiet' data-reactid='.8.0.1.0.0.1.1'><span class='quiet u-inline-block' data-reactid='.8.0.1.0.0.1.1.0'><span data-reactid='.8.0.1.0.0.1.1.0.0'>" + item.Email + "</span></span></p></div><div class='options' data-reactid='.8.0.1.0.0.2'><a data-reactid='.8.0.1.0.0.2.0' href='#' class='option quiet-button'><span id=" + index + " class='FromTeamRemoveMember'  onclick='removememberfromlist(" + item.userid + "," + listid + "," + index + ")' data-reactid='.8.0.1.0.0.2.0.0'>Remove</span></a></div><div class='clr'></div></div>");
                });
            } else {
                $("#showuser").append("No Member Assign To This List");
            }
            $('#searchmember').val("");
        }
    });
}

function removememberfromlist(UserID, listid, spanid) {
    var p = confirm("Are you sure you want to delete this item ?");
    var x = "#" + spanid;
    var d = "d";
    if (p) {
        $.ajax({
            type: "POST",
            url: "/DataTracker.svc/ShowAssignUsersTolist",
            data: "{\"OperationStatus\": \"" + d + "\",\"userid\": \"" + UserID + "\",\"listid\": \"" + listid + "\"}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                $(x).remove();
            }
        });
    }
}

function getUrlVars() {
    var a = window.location.href;
    if (a.indexOf("?") != -1) {
        var id = a.substring(a.indexOf("?") + 1);
        return id;
    }
}

function bindddlforchangeteam(teamid) {
    teamIdForOwner = teamid;
    $("#ddlteamtoboard").empty();
    $("#ddlteamtoboard1").empty();

    $("#ddlteamtoboard2").empty();
    $("#ddlteamtoboard3").empty();
    var s = 's';
    $.ajax({
        type: "Post",
        url: "/DataTracker.svc/bindteamforchange",
        data: "{\"OperationStatus\": \"" + s + "\"}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {

            $("#ddlteamtoboard").empty();
            $("#ddlteamtoboard1").empty();
            $("#ddlteamtoboard2").empty();
            $("#ddlteamtoboard3").empty();
            $('#ddlteamtoboard').append("<option value='0'>None</option>");
            $('#ddlteamtoboard1').append("<option value='0'>None</option>");
            $('#ddlteamtoboard2').append("<option value='0'>None</option>");
            $('#ddlteamtoboard3').append("<option value='0'>None</option>");

            $.each(data.d, function (index, item) {
                $("#ddlteamtoboard").append($("<option></option>").val(item.Id).html(item.TeamName));
                $("#ddlteamtoboard1").append($("<option></option>").val(item.Id).html(item.TeamName));
                $("#ddlteamtoboard2").append($("<option></option>").val(item.Id).html(item.TeamName));
                $("#ddlteamtoboard3").append($("<option></option>").val(item.Id).html(item.TeamName));
            });
            $('#ddlteamtoboard').val(teamid);
            $('#ddlteamtoboard1').val(teamid);
            $('#ddlteamtoboard2').val(teamid);
            $('#ddlteamtoboard3').val(teamid);
        }
    });
}

function searchmember_from_grid(sender, arg) {
    var _listid = $('#dropdownlistid').val();
    $("#showuser").empty();
    var teamid = $('#hiddenteamid').val();
    if ($("#searchmember").val() != "") {
        var membersname = $('#searchmember').val();
        var OperationStatus = "search";
        if (membersname.length > 0) {
            $.ajax({
                type: "POST",
                url: "/DataTracker.svc/ShowAssignUsersTolist",
                data: "{\"txtsearch\": \"" + membersname + "\",\"OperationStatus\": \"" + OperationStatus + "\",\"listid\": \"" + _listid + "\"}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                async: false,
                success: function (data) {
                    $.each(data.d, function (index, item) {
                        var str = item.Email;
                        var firstletter = str.charAt(0);
                        $("#showuser").append("<div class='member-list-item-detail' ><div class='member member-no-menu' ><span title='" + item.UserName + " (" + item.Email + ")' class='member-initials' >" + firstletter + "</span><span title='This member is an admin of this team.' class='member-type admin' ></span><span title='This member has Trello Gold.' class='member-gold-badge' ></span></div><div class='details' ><p class='name-line' ><span class='full-name' >" + item.UserName + "</span></p><p class='u-bottom quiet' ><span class='quiet u-inline-block' ><span data-reactid='.8.0.1.0.0.1.1.0.0'>" + item.Email + "</span></span></p></div><div class='options' ><a  href='#' class='option quiet-button'><span id=" + index + " class='FromTeamRemoveMember'  onclick='removememberfromteam(" + item.userid + "," + _listid + "," + index + ")' >Remove</span></a></div><div class='clr'></div></div>");
                    });
                }
            });
        }
    } else {
        showmembersonloadaccordingtolist(_listid);
    }
}


$(document).ready(function () {

    $("#dropdownlistid").change(function () {
        var listid = $("#dropdownlistid").val();
        showmembersonloadaccordingtolist(listid);
    });
    $("#Backtoboard").click(function () {
        var item = $("#boardID").val();
        window.location.href = "board.aspx?" + item;
    });
    $("#accordion").accordion();
    (function () {
        var settings = {
            trigger: 'click',
            multi: false,
            closeable: true,
            style: '',
            delay: 300,
            padding: true,
            backdrop: false
        };

        function initPopover() {
            $('a.show-pop').webuiPopover('destroy').webuiPopover(settings);

            var tableContent = $('#tableContent').html(),
            tableSettings = {
                content: tableContent,
                width: 500
            };
            $('a.show-pop-table').webuiPopover('destroy').webuiPopover($.extend({}, settings, tableSettings));

            var listContent = $('#listContent').html(),
            listSettings = {
                content: listContent,
                title: '',
                padding: false
            };
            $('a.show-pop-list').webuiPopover('destroy').webuiPopover($.extend({}, settings, listSettings));

            var largeContent = $('#largeContent').html(),
            largeSettings = {
                content: largeContent,
                width: 400,
                height: 350,
                delay: { show: 300, hide: 1000 },
                closeable: true
            };

            $('body').on('click', '.pop-click', function (e) {
                e.preventDefault();
                if (console) {
                    console.log('clicked');
                }
            });
        }
        initPopover();
    })();

    $("#emptxt").mouseover(function () {
        $(".cd-nav-gallery").removeClass("is-hidden");
    });
    $(document).mouseup(function (e) {
         
        var subject = $("#cd-nav-gallerysec");

        if (e.target.id != subject.attr('id') && !subject.has(e.target).length) {
            subject.addClass("is-hidden");
        }
    });
    $("#ActionsMenu, #ReportMenu, #SettingMenu ,#imgInviteUser, #AADMenu ,#WebscrapesMenu, #AdsMenu").mouseover(function () {
        $(".cd-nav-gallery").addClass("is-hidden");
    });
});
