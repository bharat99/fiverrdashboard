﻿function ShowTeamLeadList(oUserID, TypeID) {
    // abortRequests();   /
    $("#TeamTeadList")[0].innerHTML = "";
    if ($("#TeamTeadList").data("kendoWindow") != null) {
        $('#TeamTeadList').data().kendoGrid.destroy();
        $('#TeamTeadList').empty();
    }
    var dataSourceDetail = "";
    dataSourceDetail = new kendo.data.DataSource({
        pageSize: 20,
        serverPaging: true,
        schema: {
            data: "d",
            total: function (d) {
                if (d.d.length > 0)
                    return d.d[0].TotalCount
            },
            model: {
                id: "ID",
                fields: {
                    ID: { editable: false, type: "number" },
                    Name: { editable: false, type: "string" },
                    Newtask: { editable: false, type: "string" },
                    Inprogresstask: { editable: false, type: "string" },
                    OverDue: { editable: false, type: "string" },
                    Reopentask: { editable: false, type: "string" },
                    UserID: { editable: false, type: "number" }


                }
            }

        },

        transport: {
            read: function (options) {
                $.ajax({
                    type: "GET",
                    url: "/DataTracker.svc/GetTeamTaskDetails",
                    dataType: "json",
                    data: {

                        Page: options.data.page,
                        PageSize: options.data.pageSize,
                        skip: options.data.skip,
                        take: options.data.take,
                        UserID: oUserID,
                        UserTypeID: TypeID,
                        IsLevel: "0"
                    },

                    success: function (result) {
                        options.success(result);

                    }
                });
            }
        }

    });

    if ($("#TeamTeadList").length > 0)
    { $("#TeamTeadList")[0].innerHTML = ""; }
    if (TypeID == "12") {
        var element = $("#TeamTeadList").kendoGrid({
            dataSource: dataSourceDetail,
            //            excel: {
            //                fileName: "TeamTasksReport.xlsx",
            //                filterable: true,
            //                allPages: true
            //            },
            detailInit: TeamMemberListBy,
            dataBound: function () {

            },
            pageable: {
                batch: true,
                pageSize: 20,
                refresh: true,
                pageSizes: true,
                input: true
            },
            columns: [
                            {
                                field: "Name",
                                title: "Name",
                                width: "55%"
                            },
                            {
                                field: "UserID",
                                title: "UserID",
                                width: "0%",
                                hidden: true
                            },
                            {
                                field: "Newtask",
                                title: "New",
                                width: "10%"


                            },
                            {
                                field: "Analysis",
                                title: "Analysis",
                                width: "7%"
                            },
                            {
                                field: "Inprogresstask",
                                title: "In progress",
                                width: "8%"
                            },
                            {
                                field: "OverDue",
                                title: "Over Due",
                                width: "10%"
                            },
                             {
                                 field: "Reopentask",
                                 title: "Reopen",
                                 width: "10%"
                             },
            //                            { command: { text: "Show tasks list", click: ShowTaskDetails }, title: "", width: "15%" }
                        ],
            editable: false,
            sortable: true,
            selectable: true

        });
    }
    else {

        var element = $("#TeamTeadList").kendoGrid({
            dataSource: dataSourceDetail,
            dataBound: function () {

            },
            pageable: {
                // batch: true,
                pageSize: 20,
                refresh: true,
                pageSizes: true,
                input: true
            },
            columns: [
                            {
                                field: "Name",
                                title: "Name",
                                width: "40%"
                            },
//                            {
//                                field: "UserID",
//                                title: "UserID",
//                                width: "0%"
//                            },
                            {
                                field: "Newtask",
                                title: "New",
                                width: "10%"


                            },
                            {
                                field: "Analysis",
                                title: "Analysis",
                                width: "7%"
                            },
                            {
                                field: "Inprogresstask",
                                title: "In progress",
                                width: "8%"
                            },
                            {
                                field: "OverDue",
                                title: "Over Due",
                                width: "10%"
                            },
                             {
                                 field: "Reopentask",
                                 title: "Reopen",
                                 width: "10%"
                             },
                            { command: { text: "Show tasks list", click: ShowTaskDetails }, title: "", width: "15%" }
                        ],
            editable: false,
            sortable: true,
            selectable: true

        });


    }

}

function ShowTaskDetails(e) {
    e.preventDefault();
    if ($("#ShowTeamTaskDetail").data("kendoWindow") != null) {
        var StatusReportwindow = $("#ShowTeamTaskDetail").data("kendoWindow");
        StatusReportwindow.close();
    }
    var dataItem = this.dataItem($(e.currentTarget).closest("tr"));
    if (dataItem != null) {
        var UserID = dataItem.UserID;
        var name = dataItem.Name;
        OpenTeamPopup(UserID, name);
    }
}

function OpenTeamPopup(UserID, Name) {
    if ($("#ShowTeamTaskDetail").data("kendoWindow") != null) {
        var StatusReport = $("#ShowTeamTaskDetail").data("kendoWindow");
        StatusReport.close();
    }
    var Title = "Task List (" + Name + ")";
    var windowElement = $("#ShowTeamTaskDetail").kendoWindow({
        iframe: true,
        content: "ShowTaskList.htm?id=" + UserID + "",
        modal: true,
        width: "800px",
        height: "445px",
        position: {
            top: 10,
            left: 2
        },
        resizable: false,
        actions: ["Close"],
        draggable: false

    });
    var dialog = $("#ShowTeamTaskDetail").data("kendoWindow");
    dialog.setOptions({
        title: Title
    });
    dialog.open();
    dialog.center();
}

function TeamMemberListBy(e) {
    $("<div/>").appendTo(e.detailCell).kendoGrid({
        dataSource: {
            transport: {
                read: {
                    url: "/DataTracker.svc/GetTeamTaskDetails",
                    data: {
                        UserID: e.data.UserID,
                        IsLevel: "1"
                    }

                }
            },
            serverPaging: true,
            serverSorting: true,
            serverFiltering: true,
            pageSize: 100,
            schema: {
                data: "d",
                total: function (d) {
                    if (d != null) {
                        if (d.d.length > 0)
                            return d.d[0].TotalCount
                    }
                },
                model: {
                    id: "ID",
                    fields: {
                        ID: { editable: false, type: "number" },
                        Name: { editable: false, type: "string" },
                        Newtask: { editable: false, type: "string" },
                        Inprogresstask: { editable: false, type: "string" },
                        OverDue: { editable: false, type: "string" },
                        Reopentask: { editable: false, type: "string" },
                        Analysis: { editable: false, type: "string" },
                        UserID: { editable: false, type: "number" }

                    }
                }

            }

        },
        columns: [
                            {
                                field: "Name",
                                title: "Name",
                                width: "25%"
                            },
                            {
                                field: "Newtask",
                                title: "New",
                                width: "8%"


                            },
                            {
                                field: "Analysis",
                                title: "Analysis",
                                width: "8%"
                            },
                            {
                                field: "Inprogresstask",
                                title: "In progress",
                                width: "7%"
                            },
                            {
                                field: "OverDue",
                                title: "Over Due",
                                width: "10%"
                            },
                             {
                                 field: "Reopentask",
                                 title: "Reopen",
                                 width: "10%"
                             },
                            { command: { text: "Show Detail", click: ShowTaskDetails }, title: "", width: "10%" }
                        ],
        editable: false,
        sortable: true,
        selectable: true

    });

}
function ShowIndiviualTaskList(ID) {
    var dataSourceStore = "";
    dataSourceStore = new kendo.data.DataSource({
        serverPaging: true,
        pageSize: 10,
        sortable: {
            mode: "single",
            allowUnsort: false
        },
        schema: {
            data: "d",
            total: function (d) {
                if (d.d > 0)
                    return d.d[0].TotalCount
            },
            model: {
                id: "ID",
                fields: {
                    TaskID: { editable: false, type: "number" },
                    Title: { editable: false, type: "string" },
                    StatusName: { editable: true, type: "string" },
                    TargetDate: { editable: false, type: "string" },
                    CreatedDate: { editable: false, type: "string" },
                    Priority: { editable: false, type: "string" },
                    AssinedTo: { editable: false, type: "string" },
                    AssinedBy: { editable: false, type: "string" }
                }
            }
        },
        transport: {
            read: function (options) {
                $.ajax({
                    type: "GET",
                    url: "/DataTracker.svc/GetIndiviualTasklist",
                    dataType: "json",
                    data: {
                        Page: options.data.page,
                        PageSize: options.data.pageSize,
                        skip: options.data.skip,
                        take: options.data.take,
                        UserID: ID
                    },
                    success: function (result) {
                        options.success(result);

                    }

                });

            }
        }



    });

    var element = $("#DivMemberTaskListGrid").kendoGrid({
        dataSource: dataSourceStore,
        dataBound: function (e) {
        },
        pageable: {

            pageSize: 10,
            refresh: true,
            resizable: true,
            pageSizes: true,
            input: true
        },

        columns: [


                            {
                                field: "TaskID",
                                title: "Sno",
                                width: 0,
                                hidden: true
                            },
                             {
                                 field: "StatusID",
                                 title: "",
                                 width: 0,
                                 hidden: true
                             },

                              {
                                  field: "Title",
                                  title: "Title",
                                  resizable: 200


                              },
                               {
                                   field: "Priority",
                                   title: "Priority",
                                   width: 90,
                                   template: "#=SetPriorityColor(Priority,enddate,StatusID,isresponsible)#",
                                   resizable: false
                               },
                               {
                                   field: "CreatedDate",
                                   title: "Logged Date",
                                   width: 110

                               },

                               {
                                   field: "TargetDate",
                                   title: "Completed Date",
                                   width: 90,
                                   hidden: true
                               },
                               {
                                   field: "AssinedBy",
                                   title: "Created By",
                                   width: 150,
                                   template: "<span>#=AssinedBy#</span>"
                               },
                               {
                                   field: "StatusName",
                                   title: "Status Name",
                                   width: 100

                               }



],

        editable: false,
        sortable: true,
        selectable: true

    });

}

function SetPriorityColor(prio, edate, stid, isress) {
    //  ; 
    var today = new Date();
    //    var dd = today.getDate();
    //    var mm = today.getMonth() + 1;
    //    var yyyy = today.getFullYear();

    //    var split = edate.split('/');
    //    var edatemm = split[0];
    //    var edatedd = split[1];
    //    var edateyy = split[2];
    //    if (edate != "") {
    //        var yearrr = edateyy.split(' ');
    //        var yyyyyy = yearrr[0];
    //        var endate = edatemm + '/' + edatedd + '/' + yyyyyy;

    //        var today = mm + '/' + dd + '/' + yyyy + '';
    if (edate != "") {
        curentdate = new Date(today);
        endddate = new Date(edate);
        if (curentdate.getTime() == endddate.getTime()) {

            return "<span style='color:blue'>" + prio + "</span>";
        }
        else if ((stid == 12) || (stid == 13) || (stid == 14)) {

            return prio;
        }
        else if (curentdate > endddate) {

            return "<span style='color:red'>" + prio + "</span>";
        }
        else {
            return prio;
        }
    }

    else {
        return prio;
    }

}
