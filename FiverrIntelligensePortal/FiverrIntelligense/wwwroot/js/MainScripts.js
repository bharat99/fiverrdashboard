﻿var taskpermanotice = [];
taskpermanotice[0] = null;
taskpermanotice[1] = null;
taskpermanotice.length = 2;
var ScheduleID = 0;
function inputFocus(i) {
    if (i.value == i.defaultValue) { i.value = ""; i.style.color = "#7D7D7D"; i.style.fontStyle = "normal" }
}
function inputBlur(i) {
    if (i.value == "") { i.value = i.defaultValue; i.style.color = "#7D7D7D"; i.style.fontStyle = "italic" }
}


function getClientHeight() {
    return document.compatMode == 'CSS1Compat' && !window.opera ? document.documentElement.clientHeight : document.body.clientHeight;
}
function getClientWidth() {
    return document.compatMode == 'CSS1Compat' && !window.opera ? document.documentElement.clientWidth : document.body.clientWidth;
}

function OnClientDropDownClosed(sender, args) {
    if (args.get_domEvent().stopPropagation)
        args.get_domEvent().stopPropagation();
}
function MethodFailed() {
}

function Radgrid_OnrowDataBound(sender, args) {
    args.get_item().set_visible(true);
    //args.get_dataItem()["StoreID"]               
    var teststid = args.get_dataItem()["StatusID"];
    var combo = args.get_item().findElement("DllStatus");
    //combo.value = teststid;
    // console.log(combo.value);
    return false;
}
function RadGrid1_RowMouseOver(sender, eventArgs) {
    var currentDataItem = null;
    // clear all currently selected items before selecting new
    sender.get_masterTableView().clearSelectedItems();
    if (eventArgs.get_itemIndexHierarchical().indexOf(':') > 0) {
        var detailTableIndex = eventArgs.get_itemIndexHierarchical().split(':')[0];
        var rowIndex = eventArgs.get_itemIndexHierarchical().split(':')[1].split('_')[1];
        currentDataItem = sender.get_detailTables()[detailTableIndex].get_dataItems()[rowIndex];
    }
    else {
        currentDataItem = sender.get_masterTableView().get_dataItems()[eventArgs.get_itemIndexHierarchical()];
    }
    if (currentDataItem != null) {
        currentDataItem.set_selected(true);
    }
}

function GetRadWindow() {
    var oWindow = null;
    if (window.radWindow) oWindow = window.radWindow;
    else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow;
    return oWindow;
}

function Save(arg) {
    window.setTimeout(function () {
        GetRadWindow().close(arg);
    }, 0);

}
function CancelWin() {
    //  GetRadWindow().close();
    window.parent.$("#Bulkstatusdialog").data("kendoWindow").close();
}

function SaveScheduleValue(sender, StartDate, TargetDate, DueDate, CcEmail) {
    Cid = "0";
    sid = "8";
    var Jobnumber = document.getElementById('TxtJobNumber').value;
    var txtStoreId = document.getElementById('TxtStoreID').value;
    var txtZip = document.getElementById('TxtZip').value;
    var TxtStoreName = document.getElementById('TxtStoreName').value;
    var TxtSAddress = document.getElementById('TxtSAddress').value;
    var TxtCity = document.getElementById('TxtCity').value;
    var TxtState = document.getElementById('TxtState').value;
    var TxtDistrict = document.getElementById('TxtDistrict').value;


    var ds = new Date(StartDate);
    var curr_day1 = ds.getDate();
    if (curr_day1 < 10) {
        curr_day1 = '0' + curr_day1;
    }
    var curr_month1 = ('0' + (ds.getMonth() + 1)).slice(-2)
    var curr_year1 = ds.getFullYear();
    var StartDate_date = curr_year1 + "-" + curr_month1 + "-" + curr_day1;


    var dt = new Date(TargetDate);
    var curr_day2 = dt.getDate();
    if (curr_day2 < 10) {
        curr_day2 = '0' + curr_day2;
    }
    var curr_month2 = ('0' + (dt.getMonth() + 1)).slice(-2)
    var curr_year2 = dt.getFullYear();
    var TargetDate_date = curr_year2 + "-" + curr_month2 + "-" + curr_day2;


    var dd = new Date(DueDate);
    var curr_day3 = dd.getDate();
    if (curr_day3 < 10) {
        curr_day3 = '0' + curr_day3;
    }
    var curr_month3 = ('0' + (dd.getMonth() + 1)).slice(-2)
    var curr_year3 = dd.getFullYear();
    var DueDate_date = curr_year3 + "-" + curr_month3 + "-" + curr_day3;


    if (TxtStoreName != null) {
        if (TxtStoreName != "") {

        }
        else {
            alert("Please enter store name");
            $("#btnSaveSce").attr('value', 'Save');
            //                sender.set_text("Save");
            //                sender.set_enabled(true);
            return false;
        }

    }
    else {
        alert("Please enter store name");
        $("#btnSaveSce").attr('value', 'Save');
        return false;
    }

    if (StartDate != null) {
        if (StartDate != "") {

        }
        else {
            alert("Please enter Start date");
            $("#btnSaveSce").attr('value', 'Save');
            //                sender.set_text("Save");
            //                sender.set_enabled(true);
            return false;
        }

    }
    else {
        alert("Please enter Start date");
        $("#btnSaveSce").attr('value', 'Save');
        return false;
    }

    if (TargetDate != null) {
        if (TargetDate != "") {

        }
        else {
            alert("Please enter target date");
            //                sender.set_text("Save");
            //                sender.set_enabled(true);
            return false;
        }

    }
    else {

    }
    if (DueDate != null) {
        if (DueDate != "") {

        }
        else {
            alert("Please enter Due date");
            $("#btnSaveSce").attr('value', 'Save');
            //                sender.set_text("Save");
            //                sender.set_enabled(true);
            return false;
        }

    }
    else {
        alert("Please enter Due date");
        $("#btnSaveSce").attr('value', 'Save');
        return false;
    }

    //Prit[0].value
    var WebScrapeType = $("#Dropwebtype")[0].value;
    $.ajax({
        type: "POST",
        url: "/DataTracker.svc/AddNewEditSchedule",
        data: "{\"ScheduleID\": \"" + ScheduleID + "\",\"JobNo\": \"" + Jobnumber + "\",\"StoreID\": \"" + txtStoreId + "\",\"StoreName\": \"" + escape(TxtStoreName) + "\",\"Address\":\"" + escape(TxtSAddress) + "\",\"City\": \"" + escape(TxtCity) + "\",\"ST\": \"" + escape(TxtState) + "\",\"Zip\": \"" + txtZip + "\",\"District\": \"" + escape(TxtDistrict) + "\",\"StartDate\": \"" + StartDate_date + "\",\"TargetDate\": \"" + TargetDate_date + "\",\"DueDate\": \"" + DueDate_date + "\",\"StatusID\": \"" + sid + "\",\"CID\": \"" + Cid + "\",\"Tasktype\": \"" + 1 + "\",\"WebType\": \"" + WebScrapeType + "\",\"ToEmail\": \"" + escape(removeLastComma($("#AllUserNameTo")[0].value)) + "\",\"CcEmal\": \"" + escape(removeLastComma(CcEmail)) + "\"",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            var res = data.d;
            if (res != null) {
                //GetRadGridData(true, 0, "0");
                // window.parent.location.href = window.parent.location.href;
                //  parent.$("#RegularTaskGrid").data().kendoGrid.dataSource.read();
                //  var grid = $("#DueDategrid").data("kendoGrid");
                //  if (grid != null) {
                //      grid.refresh();
                //  }
                //                    sender.set_text("Save");
                //                    sender.set_enabled(true);
                CancelSchedule();

            }
            else {
                return false;
            }
        }
    });

}

function ClearAllTextBoxValue(Jobnumber, txtStoreId, TxtStoreName, TxtSAddress, TxtCity, TxtState, txtZip, TxtDistrict, StartDate, TargetDate, DueDate) {

    if (Jobnumber != null)
        Jobnumber.value = "";
    if (txtStoreId != null)
        txtStoreId.value = "";
    if (TxtStoreName != null)
        TxtStoreName.value = "";
    if (TxtSAddress != null)
        TxtSAddress.value = "";
    if (TxtCity != null)
        TxtCity.value = "";
    if (TxtState != null)
        TxtState.value = "";
    if (txtZip != null)
        txtZip.value = "";
    if (TxtDistrict != null)
        TxtDistrict.value = "";
    //        if (TxtEstItems != null)
    //            TxtEstItems.value = "";     
    //        if (TxtEstHours != null)
    //            TxtEstHours.value = "";

    var currentTime = new Date()
    if (StartDate != null)
        StartDate.set_selectedDate(currentTime);
    if (TargetDate != null)
        TargetDate.set_selectedDate(currentTime);

    if (DueDate != null)
        DueDate.set_selectedDate(currentTime);
    //        if (DeliverDate != null)
    //            DeliverDate.set_selectedDate(currentTime);

}

function setWindowDatam(data, SchID) {
    ScheduleID = SchID;
    document.getElementById('TxtJobNumber').value = data.d.GetSchData.Job_number;
    document.getElementById('TxtStoreID').value = data.d.GetSchData.StoreID;
    document.getElementById('TxtZip').value = data.d.GetSchData.Zip;
    document.getElementById('TxtStoreName').value = data.d.GetSchData.StoreName;
    document.getElementById('TxtSAddress').value = data.d.GetSchData.Address;
    document.getElementById('TxtCity').value = data.d.GetSchData.City;
    document.getElementById('TxtState').value = data.d.GetSchData.ST;
    TxtDistrict = document.getElementById('TxtDistrict').value = data.d.GetSchData.District;

    var ds = new Date(data.d.GetSchData.StartDate);
    var curr_day1 = ds.getDate();
    if (curr_day1 < 10) {
        curr_day1 = '0' + curr_day1;
    }
    var curr_month1 = ('0' + (ds.getMonth() + 1)).slice(-2)
    var curr_year1 = ds.getFullYear();
    var StartDate_date = curr_year1 + "-" + curr_month1 + "-" + curr_day1;
    //var date1 = new Date(StartDate_date)
    // StartDate.set_selectedDate(date1);$("#dtpStartDate").val(), $("#TargetDate").val(), $("#DueDate").val(), $("#DeliverDate").val()
    $("#dtpStartDate").val(StartDate_date);

    // target date
    var dt = new Date(data.d.GetSchData.TargetDate);
    var curr_day2 = dt.getDate();
    if (curr_day2 < 10) {
        curr_day2 = '0' + curr_day2;
    }
    var curr_month2 = ('0' + (dt.getMonth() + 1)).slice(-2)
    var curr_year2 = dt.getFullYear();
    var TargetDate_date = curr_year2 + "-" + curr_month2 + "-" + curr_day2;
    // var date2 = new Date(TargetDate_date)
    //TargetDate.set_selectedDate(date2);
    $("#TargetDate").val(TargetDate_date);

    //due date
    var dd = new Date(data.d.GetSchData.DueDate);
    var curr_day3 = dd.getDate();
    if (curr_day3 < 10) {
        curr_day3 = '0' + curr_day3;
    }
    var curr_month3 = ('0' + (dd.getMonth() + 1)).slice(-2)
    var curr_year3 = dd.getFullYear();
    var DueDate_date = curr_year3 + "-" + curr_month3 + "-" + curr_day3;
    //var date3 = new Date(DueDate_date)
    // DueDate.set_selectedDate(date3);
    $("#DueDate").val(DueDate_date);
    // deliver date
    //        var dds = new Date(data.d.GetSchData.DeliverDate);
    //        var curr_day4 = dds.getDate();
    //        if (curr_day4 < 10) {
    //            curr_day4 = '0' + curr_day4;
    //        }
    //        var curr_month4 = ('0' + (dds.getMonth() + 1)).slice(-2)
    //        var curr_year4 = dds.getFullYear();
    //        var DeliverDate_date = curr_year4 + "-" + curr_month4 + "-" + curr_day4;
    //        //var date4 = new Date(DeliverDate_date)
    //        //        DeliverDate.set_selectedDate(date4);
    //        $("#DeliverDate").val(DeliverDate_date);

}

function CancelSchedule() {
    window.parent.$("#DashManageSche").data("kendoWindow").close();
    
    // GetRadWindow().close();        
}

function SelectBulkStratDateCombo(ComboStratDate) {
    if (ComboStratDate != null) {
        var ds = new Date();
        var curr_day1 = ds.getDate();
        if (curr_day1 < 10) {
            curr_day1 = '0' + curr_day1;
        }
        var curr_month1 = ('0' + (ds.getMonth() + 1)).slice(-2)
        var curr_year1 = ds.getFullYear();
        var Start_date = curr_day1 + "-" + curr_month1 + "-" + curr_year1;
        for (var i = 0; i < ComboStratDate.get_items().get_count(); i++) {
            if (ComboStratDate.get_items().getItem(i).get_text() == Start_date) {
                // ComboStratDate.get_items().getItem(i).set_checked(true);
                var itm = ComboStratDate.findItemByValue(ComboStratDate.get_items().getItem(i).get_value());
                itm.select();
                break;
            }
        }

    }
}

function LoadStoreWithCount(storecombo, item) {

    $.ajax({
        type: "GET",
        url: "/DataTracker.svc/GetAllStoreByStartDate?SDate=" + item.get_value() + "",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            var res = data.d;
            if (res != null) {

                storecombo.clearItems();
                storecombo.trackChanges();
                for (var i = 0; i < data.d.length; i++) {
                    var comboItem = new Telerik.Web.UI.RadComboBoxItem();
                    comboItem.set_text(data.d[i].StoreName);
                    comboItem.set_value(data.d[i].StoreName);
                    storecombo.get_items().add(comboItem);
                }
                storecombo.commitChanges();
                storecombo.set_emptyMessage("---Please select store---");
            }
            else {

            }
        }
    });

}

function removeLastComma(str) {
    return str.replace(/,(?=[^,]*$)/, '');
}
function CancelcomPopup() {
    //console.log("hello");
    //if ($("#DivPartialdialog").data("kendoWindow") != null) {
    window.parent.$("#DivPartialdialog").data("kendoWindow").close();
    //  }
}
function NonMatchingStatus(CurrentStoreName) {

    var NonmatchingAdds = document.getElementById("TxtAddressjobno");
    var Nonmatchingads = document.getElementById("TextareaAdsjob");
    var BtnnonMatch = document.getElementById("btnnonMatchSave");
    if (NonmatchingAdds != null && Nonmatchingads != null) {
        if (NonmatchingAdds.value != "" || Nonmatchingads.value != "") {
            BtnnonMatch.value = "Saving ..."
        }
        else if (NonmatchingAdds.value == "" && Nonmatchingads.value == "") {
            alert("Please enter store name");
            BtnnonMatch.value = "Save";
            sender.set_enabled(true);
            return false;
        }
    }
    var parameters = NonmatchingAdds.value.split(','); ;

    var dictionary = new Array();
    for (var i in parameters) {

        var key = "Jobno";
        var value = parameters[i];
        dictionary.push({ "Key": key, "Value": value });
    }

    var listOfObjects = new Array();
    //creating list of objects
    for (var i in parameters) {
        var JobListList = new Object();
        JobListList.job_Number = parameters[i];
        listOfObjects.push(JobListList);
    }

    $.ajax({
        type: "POST",
        async: false,
        url: "/DataTracker.svc/AddressStatus",
        data: { data: JSON.stringify(listOfObjects) },
        //  data: "{\"AddressNonMatchinList\": \"" + parameters + "\",\"AdnonMatchingList\": \"" + removeLastComma(Nonmatchingads.value) + "\",\"StoreName\": \"" + CurrentStoreName + "\"",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            var res = data.d;
            if (res != null) {
                //GetRadGridData(true, 0, "0");
                window.parent.location.href = window.parent.location.href;
                BtnnonMatch.value = "Save";
                // sender.set_enabled(true);
                //  CancelSchedule();
                CancelcomPopup();

            }
            else {
                return false;
            }
        }
    });

    function JobListList() {
        this.job_Number = "";
    }

    function LoadStoreOrGroupName(storecombo) {
        $.ajax({
            type: "GET",
            url: "/DataTracker.svc/GetAllStoreLists",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                var res = data.d;
                if (res != null) {
                    storecombo.clearItems();
                    storecombo.trackChanges();
                    for (var i = 0; i < data.d.length; i++) {
                        var comboItem = new Telerik.Web.UI.RadComboBoxItem();
                        comboItem.set_text(data.d[i].StoreName);
                        comboItem.set_value(data.d[i].StoreName);
                        storecombo.get_items().add(comboItem);
                    }
                    storecombo.commitChanges();
                    storecombo.set_emptyMessage("---Please select store---");
                }
                else {

                }
            }
        });

    }
}
function ChangePassword() {
    
    var oldpassword = $("#txtCupass").val();
    var Newpass = $("#TxtNpass").val().trim();
    var confirmPass = $("#TxtCrpass").val().trim();

    if (oldpassword.length > 0) {

        $("#txtCupass").addClass("ChangePassBox");
    }
    else {

        $("#txtCupass").addClass("ChangePassBoxborder");
        alert("Please enter password");
        return false;
    }

    if (Newpass.length > 0) {
        
        $("#TxtNpass").addClass("ChangePassBox");
    }
    else {

        $("#TxtNpass").addClass("ChangePassBoxborder");
        alert("Please enter new password");
        return false;
    }


    if (confirmPass.length > 0) {

        $("#TxtCrpass").addClass("ChangePassBox");
    }
    else {

        $("#TxtCrpass").addClass("ChangePassBoxborder");
        alert("Please enter confirm password");
        return false;
    }



    if (Newpass != confirmPass) {
        $("#TxtNpass").addClass("ChangePassBoxborder");
        $("#TxtCrpass").addClass("ChangePassBoxborder");
        alert("New password and confirm password not match.");
        return false;
    }


    $.ajax({
        type: "POST",
        url: "/DataTracker.svc/ChangePass",
        data: "{\"Opass\": \"" + escape(oldpassword) + "\",\"Npass\": \"" + escape(Newpass) + "\"",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            var res = data.d;
            if (res != null) {
                if (res == "2") {
                    $("#ChangeMsg")[0].style.color = "red";
                    $("#ChangeMsg")[0].innerHTML = "You cannot add old password";
                }
                else if (res == "1") {
                    $("#ChangeMsg")[0].style.color = "green";
                    $("#ChangeMsg")[0].innerHTML = "Your Password has been changed.";
                    closewithtime();
                }
                else if (res == "0") {
                    $("#ChangeMsg")[0].style.color = "red";
                    $("#ChangeMsg")[0].innerHTML = "Your current password does not exist";

                }
                else if (res == "-1") {
                    $("#ChangeMsg")[0].style.color = "red";
                    $("#ChangeMsg")[0].innerHTML = "Some technical problem occur, please retry!";

                }

            }
            else {
                $("#ChangeMsg")[0].style.color = "red";
                $("#ChangeMsg")[0].innerHTML = "Some technical problem occur, please retry!";
                return false;
            }
        }
    });

}


function checkpss(sender, args) {
    if (args.keyCode == 32) {
        args.preventDefault();
    }
}

function forgetPass() {
    var Email = "";
    var Txtemail = $("#TxtEmail");
    if (Txtemail.length > 0) {
        if ($("#TxtEmail")[0].value != "") {
            $("#TxtEmail").addClass("forgetBox");
        }
        else {
            $("#TxtEmail").addClass("forgetBoxWithborder");
            alert("Please enter email");
            return false;
        }

    }

    if ($("#TxtEmail")[0].value != "") {
        var validEmail = validateMultipleEmails($('#TxtEmail')[0].value);
        if (validEmail == false) {
            $("#TxtEmail").addClass("forgetBoxWithborder");
            alert("Please enter a valid email address");
            return false;
        }
        else {
            $("#TxtEmail").addClass("forgetBox");
            Email = $('#TxtEmail')[0].value;
        }
    }

    $.ajax({
        type: "Get",
        url: "/DataTracker.svc/forgetPass?email=" + Email,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            var res = data.d;
            if (res != null) {
                if (res == "1") {
                    $("#forgetMsg")[0].style.color = "green";
                    $("#forgetMsg")[0].innerHTML = "Your login detail has been sent to your email address.";
                    closewithtime();
                }
                else if (res == "0") {
                    $("#forgetMsg")[0].style.color = "red";
                    $("#forgetMsg")[0].innerHTML = "Your email address does not exist";

                }
                else if (res == "-1") {
                    $("#forgetMsg")[0].style.color = "red";
                    $("#forgetMsg")[0].innerHTML = "Some technical problem occur, please retry!";

                }

            }
            else {
                $("#forgetMsg")[0].style.color = "red";
                $("#forgetMsg")[0].innerHTML = "Some technical problem occur, please retry!";
                return false;
            }
        }
    });

}


function ChangePassPopUp() {
    var windowElement = $("#WinChangePass").kendoWindow({
        content: "/ChangePassword.aspx",
        modal: true,
        width: "500px",
        height: "300px",
        title: "Change password",
        position: {
            top: 10,
            left: 2
        },
        resizable: false,
        actions: ["Close"],
        draggable: false

    });
    var dialog = $("#WinChangePass").data("kendoWindow");
    dialog.open();
    dialog.center();
    // return false;

}

function CanelCPopup() {
    var dialog = $("#WinChangePass").data("kendoWindow");
    dialog.close();
}

function closewithtime() {
    var dialog = $("#WinChangePass").data("kendoWindow");
    if (dialog != null) {
        setTimeout(function () {
            dialog.close();
        }, 5000);
    }
}
function OnKeyChangepass(sender, event) {
    var confirmPass = $("#TxtCrpass");
    if (event.keyCode == 13) {
        if (confirmPass.length > 0) {
            if (confirmPass[0].value != "") {
                ChangePassword();
                return false;
            }

        }
    }
}
function ExportFilteredData(ComboStoreName, ComboStatus, ComboStratDate, ComboDueDate, ChkAllCombo) {

    ListStoreName = "";
    ListStartDate = "";
    ListDueDate = "";
    StatusIds = "";
    text = "";

    if (ListStoreName != "") {
        var allvalstore = ListStoreName.split(',');
        {
            if (allvalstore.length > 75)
                ListStoreName = "";
            else { }
        }
    }
    if (ComboStatus != "")
        StatusIds = ComboStatus;

    GetDetailData(ListStoreName, StatusIds, ComboStratDate, "");
}
function GetDetailData(ListStoreName, StatusIds, ListStartDate, ListDueDate) {
    document.getElementById("Filterscheduledata").innerHTML = "Downloading...";
    $.ajax({
        type: "POST",
        url: "/DataTracker.svc/Filterschedule",
        data: "{\"StatusIDs\": \"" + StatusIds + "\",\"StoreName\": \"" + ListStoreName + "\",\"StartDate\": \"" + ListStartDate + "\",\"DueDate\": \"" + ListDueDate + "\"",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            if (data.d != null) {

                window.location.href = 'Downloadfiles.aspx?filterexpoert=' + data.d;
                document.getElementById("Filterscheduledata").innerHTML = "Download Schedule";
            }
            else {
                document.getElementById("Filterscheduledata").innerHTML = "Download Schedule";
                alert("No Records Found");
            }
        },
        Error: function (data) {
            alert("Data Cannot Be Exported")

        }
    });
}

function SetRegularBandwidth(wi) {
    if (wi <= 480) {
        $("#DivRegularCombo")[0].style.marginLeft = "2%";
        //  $("#divforexpoandprint")[0].style.width = "2%"; 
        // $("#divforexpoandprint")[0].style.width = "90%"; 
    }
    else if (wi <= 767) {
        $("#DivRegularCombo")[0].style.marginLeft = "3%";
        //$("#divforexpoandprint")[0].style.marginRight = "3%"; 

    }
    else if (wi <= 980) {
        $("#DivRegularCombo")[0].style.marginLeft = "5%";
        //$("#divforexpoandprint")[0].style.marginRight = "5%"; 

    }
    else if (wi <= 1200) {
        $("#DivRegularCombo")[0].style.marginLeft = "9%";
        // $("#divforexpoandprint")[0].style.marginRight = "9%";     

    }
    else if (wi <= 1320) {
        $("#DivRegularCombo")[0].style.marginLeft = "17%";
        // $("#divforexpoandprint")[0].style.marginRight = "6%"; 

    }
    else {

        $("#DivRegularCombo")[0].style.marginLeft = "17%";
        // $("#divforexpoandprint")[0].style.marginRight = "2%";

    }
}


function SetComboBandwidth(wi) {
    if (wi <= 480) {
        $("#DivDetailCombo")[0].style.marginLeft = "2%";
    }
    else if (wi <= 767) {
        $("#DivDetailCombo")[0].style.marginLeft = "3%";

    }
    else if (wi <= 980) {
        $("#DivDetailCombo")[0].style.marginLeft = "5%";

    }
    else if (wi <= 1200) {
        $("#DivDetailCombo")[0].style.marginLeft = "7%";

    }
    else if (wi <= 1320) {
        $("#DivDetailCombo")[0].style.marginLeft = "9%";
    }
    else {
        $("#DivDetailCombo")[0].style.marginLeft = "17%";

    }
}
function inputFocus(e) {
    if (e.value == e.defaultValue) {
        e.value = "";
        e.style.color = "#000";
        e.style.fontStyle = "normal"
    }
}

function inputBlur(e) {
    if (e.value == "") {
        e.value = e.defaultValue;
        e.style.color = "#999";
        e.style.fontStyle = "italic"
    }
}

function DownloaAdsReports(sDate, edate, ReportType) {
    document.getElementById("DivDownloadAllReports").innerHTML = "Please Wait...";
    $.ajax({
        type: "POST",
        url: "/DataTracker.svc/DownLoadAllTypeAdsReports",
        data: "{\"SDate\": \"" + sDate + "\",\"EDate\": \"" + edate + "\",\"Type\": \"" + ReportType + "\"",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            if (data.d != null) {
                window.location.href = 'Downloadfiles.aspx?OffFileName=' + data.d;
                document.getElementById("DivDownloadAllReports").innerHTML = "Download reports";
            }
            else {
                document.getElementById("DivDownloadAllReports").innerHTML = "Download reports";
                alert("No Records Found");
            }
        },
        Error: function (data) {
            alert("Data Cannot Be Exported")

        }
    });
}

function AutoAllStoreList() {
    //  $("#Droppriority").kendoDropDownList();
    var dictionary = new Array();
    $.ajax({
        type: "GET",
        url: "/datatracker.svc/GetAllStoreLists",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            var res = data.d;
            if (res != null) {
                for (var n = 0; n < data.d.length; n++) {
                    var value = data.d[n].StoreName;
                    dictionary.push(value);
                }
                $("#StoreName").kendoAutoComplete({
                    dataSource: dictionary,
                    filter: "startswith",
                    placeholder: "Select or insert storename...",
                    separator: ","
                });

            }
            else {

            }
        }

    });

}

function AutoAllUserList(IsCc, fortaskorsdata) {

    var dictionary = new Array();
    var msg = "Select or enter user name...";
    if (IsCc == "1") {
        msg = "Select or enter email address...";
    }
    $.ajax({
        type: "GET",
        url: "/datatracker.svc/getallUserName?Funforwhat=" + fortaskorsdata + "",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            var res = data.d;
            if (res != null) {
                for (var n = 0; n < data.d.length; n++) {
                    var value = data.d[n].UserName;
                    dictionary.push(value);
                }

                if ($("#AllUserName").length > 0) {
                    $("#AllUserName").kendoAutoComplete({
                        dataSource: dictionary,
                        filter: "startswith",
                        placeholder: msg,
                        separator: ","
                    });


                    $("#AllUserNameTo").kendoAutoComplete({
                        dataSource: dictionary,
                        filter: "startswith",
                        placeholder: "Select or enter email address...",
                        separator: ","
                    });
                }

                if ($("#targetInput").length > 0) {
                    $('#targetInput').tokenfield({
                        autocomplete: {
                            source: dictionary,
                            delay: 10
                        },
                        showAutocompleteOnFocus: true
                    });
                }
                if ($("#targetInputsubs").length > 0) {
                    $('#targetInputsubs').tokenfield({
                        autocomplete: {
                            source: dictionary,
                            delay: 10
                        },
                        showAutocompleteOnFocus: true
                    });
                }


            }
            else {

            }




        }

    });



}



function TaskNotification(text, icon) {

    //[0]-->Sync Process. [1]-->sending email
    var stack_bottomrightDown = { "dir1": "up", "dir2": "left", "firstpos1": 15, "firstpos2": 15 };
    if (icon == "no") {
        // CloseNotificationForSync();
        if (taskpermanotice[1] == null) {
            taskpermanotice[0] = $.pnotify({ pnotify_addclass: "stack-bottomright",
                pnotify_stack: stack_bottomrightDown, pnotify_text: text,
                pnotify_nonblock: true, pnotify_hide: false,
                pnotify_closer: false, pnotify_history: false, pnotify_mouse_reset: false, pnotify_opacity: .8, pnotify_notice_icon: ""
            });
        }
    }
    else {
        // alert(text+"df "+icon);
        TaskCloseNotification(); //Specifically to remove sending email notification
        // CloseNotificationForSync();
        taskpermanotice[1] = $.pnotify({ pnotify_addclass: "stack-bottomright", pnotify_stack: stack_bottomrightDown, pnotify_text: text, pnotify_nonblock: true, pnotify_hide: false, pnotify_closer: false, pnotify_history: false, pnotify_mouse_reset: false, pnotify_opacity: .8 });
    }
}
function TaskCloseNotification() {
    if (taskpermanotice[1] != null) {
        taskpermanotice[1].pnotify_remove();
        taskpermanotice[1] = null;
    }
}
function SavedTaskPopUp(Saved) {
    if (Saved == "1")
        TaskOpenAndCloseNotify("Saved successfully!")
    else if (Saved == "-3") {
        TaskOpenAndCloseNotify("Title already exist !")
    }
    else

        TaskOpenAndCloseNotify("Change fail")

}
function SavedTaskPopUpforinviteuser(Saved) {
    // alert(Saved);
    if (Saved == "1")
        TaskOpenAndCloseNotify("Add successfully!")
    else if (Saved == "-3") {
        TaskOpenAndCloseNotify("User already exist !")
    }
    else

        TaskOpenAndCloseNotify("Fail to add user")

}
function TaskOpenAndCloseNotify(Msg) {
    TaskNotification(Msg, "yes");
    setTimeout("TaskCloseNotification()", 5000);
}

function inserTaskWithFile(sender, TaskID, Txttitle, Detail, TargetDate_date, sid, Cid, UserList, GroupName, Prit, feab, commts, ViToclient, pid, fname, SubUserList, Subtasklist) {
  
    $.ajax({
        type: "POST",
        url: "/DataTracker.svc/AddNewEditTask",
        data: "{\"TaskID\": \"" + TaskID + "\",\"TTitle\": \"" + escape(encodeURIComponent(Txttitle)) + "\",\"Desp\": \"" + escape(encodeURIComponent(Detail)) + "\",\"TDate\": \"" + TargetDate_date + "\",\"StatusID\": \"" + sid + "\",\"CID\": \"" + Cid + "\",\"Owner\": \"" + UserList + "\",\"GroupName\": \"" + escape(GroupName) + "\",\"pri\": \"" + Prit + "\",\"feab\": \"" + escape(feab) + "\",\"commts\": \"" + escape(commts) + "\",\"ViToclient\": \"" + ViToclient + "\",\"pid\": \"" + pid + "\",\"Filelist\": \"" + escape(fname) + "\",\"SubOwner\": \"" + SubUserList + "\",\"Subtasklist\": \"" + Subtasklist + "\"",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            var res = data.d;
            if (res != null) {

                //sender.set_text("Save");
                // sender.set_enabled(true); 
                $("#btnSaveSce").attr('value', 'Save');
                // CancelTask();
                if (res == "1") {


                    CancelTask();
                    parent.$("#DivOtherTask").data().kendoGrid.dataSource.read();
                    SavedTaskPopUp("1");

                }
                else if (res == "0") {
                    SavedTaskPopUp("0");
                    $("#btnSaveSce").attr('value', 'Save');
                }
                else if (res == "-3") {
                    // SavedMessagePopUp("-3");

                    $("#btnSaveSce").attr('value', 'Save');
                    alert("Task already exist");
                    return false;
                }


            }
            else {
                // alert("ff");
                $("#btnSaveSce").attr('value', 'Save');
                return false;
            }
        },
        failure: function (response) {
            $("#btnSaveSce").attr('value', 'Save');
            //alert("error");
        },
        error: function (xhr, status, error) {
            $("#btnSaveSce").attr('value', 'Save');
            // alert("error1");
        }
    });




}
function redirectWorkItem(tid) {
    CancelTask();
    $("#btnSaveSce").attr('value', 'Save');
    parent.window.location.href = "/Dashbord.aspx?tid=" + tid;
}


function SetWorkItemCommentBox(wi) {
    //alert(wi);
    if (wi <= 480) {
        $("#DivLComents")[0].style.width = "30%";
        $("#DivMainDe")[0].style.width = "63%";
        // $("#DivDeBox")[0].style.height = "710px";

    }
    else if (wi <= 767) {
        $("#DivLComents")[0].style.width = "28%";
        $("#DivMainDe")[0].style.width = "69%";
        //$("#DivDeBox")[0].style.height = "760px";

    }
    else if (wi <= 980) {
        $("#DivLComents")[0].style.width = "28%";
        $("#DivMainDe")[0].style.width = "69%";
        // $("#DivDeBox")[0].style.height = "710px";

    }
    else if (wi <= 1200) {
        $("#DivLComents")[0].style.width = "28%";
        $("#DivMainDe")[0].style.width = "69%";
        //  $("#DivDeBox")[0].style.height = "710px";

    }
    else if (wi <= 1250) {
        $("#DivLComents")[0].style.width = "25%";
        $("#DivMainDe")[0].style.width = "70%";
        //$("#DivDeBox")[0].style.height = "710px";

    }
    else if (wi <= 1320) {
        $("#DivLComents")[0].style.width = "25%";
        $("#DivMainDe")[0].style.width = "68%";
        // $("#DivDeBox")[0].style.height = "650px";
    }
    else {
        $("#DivLComents")[0].style.width = "25%";
        $("#DivMainDe")[0].style.width = "72.5%";
        //$("#DivDeBox")[0].style.height = "667px";

    }
}


function validateEmail(field) {
    var regex = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,5}$/;
    return (regex.test(field)) ? true : false;
}
function validateMultipleEmailsCommaSeparated(txtvalue) {
    var text = txtvalue;
    if (text != '') {
        var result = text.split(",");
        for (var i = 0; i < result.length; i++) {
            var trimmed = result[i].trim();
            if (trimmed != '') {
                if (!validateEmail(trimmed)) {
                    alert('' + trimmed + ' email addresses not valid!');
                    return false;
                }
            }
        }
    }
    return true;
}


function InserInviteUser(email, name, timezone, teamid, ads, webscrap) {
    $.ajax({
        type: "POST",
        url: "/DataTracker.svc/AddNewInviteUser",
        data: "{\"email\": \"" + email + "\",\"name\": \"" + name + "\",\"timezone\": \"" + timezone + "\",\"teamid\": \"" + teamid + "\",\"ads\": \"" + ads + "\",\"webscrap\": \"" + webscrap + "\"",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            var res = data.d;
            if (res != null) {
                $("#btnSavePer").attr('value', 'Save');
                //Cancelpermission();
                if (res == "1") {
                    window.parent.$("#DivInviteUser").data("kendoWindow").close();
                    SavedTaskPopUpforinviteuser("1");
                }
                else if (res == "0") {
                    SavedTaskPopUpforinviteuser("0");
                    $("#btnSave").attr('value', 'Save');
                }
                else if (res == "2") {
                    $("#diverrormsg")[0].style.display = "block";
                }


            }
            else {
                // alert("ff");
                $("#btnSave").attr('value', 'Save');
                return false;
            }
        },
        failure: function (response) {
            $("#btnSave").attr('value', 'Save');
            //alert("error");
        },
        error: function (xhr, status, error) {
            $("#btnSave").attr('value', 'Save');
            // alert("error1");
        }
    });




}
function bindhtmdropdown() {
    $.ajax({
        type: "GET",
        contentType: "application/json; charset=utf-8",
        url: "/DataTracker.svc/BindTimeZoneDropdown",
        dataType: "json",
        success: function (data) {
            $("#HtmComcoTimezone").empty().append($("<option></option>").val("+05:30").html("Time Zone:"));
            $.each(data.d, function (index, item) {
                $("#HtmComcoTimezone").append('<option value = "' + item.GMTOffset + '">' + item.CountryName + '</option>');

            });
        },
        error: function (result) {

        }
    });
}


function bindhtmteamlist() {
    $.ajax({
        type: "GET",
        contentType: "application/json; charset=utf-8",
        url: "/DataTracker.svc/BindTeamListdropdown",
        dataType: "json",
        success: function (data) {
            $("#HtmComcoteamlist").empty().append($("<option></option>").val("1").html("Team:"));
            $.each(data.d, function (index, item) {
                $('#HtmComcoteamlist').append($('<option></option>').val(item.Teamid).html(item.TeamName));
            });
        },
        error: function (result) {

        }
    });
}

function bindhtmteamlistwithoutdropdown(uid) {
    $.ajax({
        type: "GET",
        contentType: "application/json; charset=utf-8",
        url: "/DataTracker.svc/BindTeamListwithoutdropdown?userid=" + uid + "",
        dataType: "json",
        success: function (data) {
            $.each(data.d, function (index, item) {
                $('#HtmComcoteamlist').append($('<option></option>').val(item.Teamid));
            });
        },
        error: function (result) {

        }
    });
}

function Checkpermissiononaddinvitepage() {
    $.ajax({
        type: "GET",
        contentType: "application/json; charset=utf-8",
        url: "/DataTracker.svc/CheckPermissionForInviteUser",
        dataType: "json",
        success: function (data) {
            $.each(data.d, function (index, item) {
                var strdata = item.Permissionstring;
                var myyaaaray = strdata.split("~");
                if (myyaaaray[0] == 0 && myyaaaray[1] == 1) {

                    $("#divsecondpermission")[0].style.display = "block";
                    $("#divlabelpermission")[0].style.display = "block";
                    $("#divthirdpermission")[0].style.display = "none";
                    $("#divonlyworkitem")[0].style.display = "block";
                    //$("#divlabelpermission").css({ "margin-top": "-32px" });
                    //$("#divsecondpermission").css({ "margin-top": "4px" });
                }
                else if (myyaaaray[0] == 0 && myyaaaray[1] == 0) {

                    $("#divonlyworkitem")[0].style.display = "block";
                    $("#divlabelpermission")[0].style.display = "block";
                    $("#divsecondpermission")[0].style.display = "none";
                    $("#divthirdpermission")[0].style.display = "none";
                    //$("#divlabelpermission").css({ "margin-top": "-18px" });
                    // $("#divonlyworkitem").css({ "margin-top": "-15px" });
                }
                else {
                    $("#divonlyworkitem")[0].style.display = "block";
                    $("#divsecondpermission")[0].style.display = "block";
                    $("#divthirdpermission")[0].style.display = "block";
                    $("#divlabelpermission")[0].style.display = "block";
                    // $("#divsecondpermission").css({ "margin-top": "3px" });
                    // $("#divlabelpermission").css({ "margin-top": "-35px" });
                }
            });

        },
        error: function (result) {

        }
    });
}

function SeprateEmailWitoutName(emails) {
    var finalemail = "";
    var emailss = emails.split(',');
    for (var i = 0; i <= emailss.length - 1; i++) {
        var semail = emailss[i].trim().split(' ');
        var femail = semail[0];
        if (i == 0)
            finalemail = femail
        else
            finalemail = finalemail + ',' + femail;
    }
    return finalemail;
}

function EmailPreferencePopUp() {
    var windowElement = $("#WinChangePass").kendoWindow({
        content: "/EmailPreference.aspx",
        modal: true,
        width: "500px",
        height: "300px",
        title: "Change password",
        position: {
            top: 10,
            left: 2
        },
        resizable: false,
        actions: ["Close"],
        draggable: false

    });
    var dialog = $("#WinChangePass").data("kendoWindow");
    dialog.open();
    dialog.center();
}