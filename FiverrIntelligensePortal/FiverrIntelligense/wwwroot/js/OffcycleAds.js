﻿var monthdate = "";
$(document).ready(function () {
    var months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December']; ;
    var date = new Date();
    if ($("#DivReportPreOff").length > 0) {
        var CMonth = months[date.getMonth()];
        var PreMonth = months[date.getMonth() - 1];
        var NMonth = months[date.getMonth() + 1];
        getOffCycledata("2");
        $("#DivReportPreOff")[0].innerHTML = String.format("<div style='width:100%' ><div  ID='DivMthPre' class='divMonthPre' onclick='javascript:getOffCycledata({1});'><a class='PrecurrNextFont' title='{0}' style='width:35%'  id='ReportAnPre' >{2}</a></div><div class='DivMonthNext'><div  class='DivMonthLast1' align='center' onclick='javascript:getOffCycledata({4});'><a class='SelectedNextFont' title='{3}' style='width:100%'   id='ReportAnCurr'>{5}</a></div><div align='center' onclick='javascript:getOffCycledata({7});'  class='DivMonthLast2'><a id='ReportAnNext' class='PrecurrNextFont' title='{6}'  style='width:100%'  >{8}</a></div></div>", PreMonth, "1", PreMonth, CMonth, "2", CMonth, NMonth, "3", NMonth);
    }
});

function getOffCycledata(month) {
    monthdate = "";
    var months = ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12'];
    var date = new Date();
    if ($("#DivReportPreOff").length > 0) {
        var CMonth = months[date.getMonth()];
        var PreMonth = months[date.getMonth() - 1];
        var NMonth = months[date.getMonth() + 1];
    }
    if (month == "1") {
        monthdate = PreMonth;
        $("#ReportAnCurr").removeClass("SelectedNextFont");
        $("#ReportAnCurr").addClass("PrecurrNextFont");
        $("#ReportAnPre").removeClass("PrecurrNextFont");
        $("#ReportAnPre").addClass("SelectedNextFont");
        $("#ReportAnNext").removeClass("SelectedNextFont");
        $("#ReportAnNext").addClass("PrecurrNextFont");
        if (PreMonth == "7") {
            $("#DivMthPre").attr('disabled', 'disabled');
            alert("Reports data is not available for this month");
            $("#ReportAnCurr").removeClass("PrecurrNextFont");
            $("#ReportAnCurr").addClass("SelectedNextFont");
            $("#ReportAnPre").removeClass("SelectedNextFont");
            $("#ReportAnPre").addClass("PrecurrNextFont");
            $("#ReportAnNext").removeClass("SelectedNextFont");
            $("#ReportAnNext").addClass("PrecurrNextFont");
            return false;
        }
    }
    else if (month == "2") {
        monthdate = CMonth;
        $("#ReportAnCurr").removeClass("PrecurrNextFont");
        $("#ReportAnCurr").addClass("SelectedNextFont");
        $("#ReportAnPre").removeClass("SelectedNextFont");
        $("#ReportAnPre").addClass("PrecurrNextFont");
        $("#ReportAnNext").removeClass("SelectedNextFont");
        $("#ReportAnNext").addClass("PrecurrNextFont");
    }
    else {
        monthdate = NMonth;
        $("#ReportAnCurr").removeClass("SelectedNextFont");
        $("#ReportAnCurr").addClass("PrecurrNextFont");
        $("#ReportAnPre").removeClass("SelectedNextFont");
        $("#ReportAnPre").addClass("PrecurrNextFont");
        $("#ReportAnNext").removeClass("PrecurrNextFont");
        $("#ReportAnNext").addClass("SelectedNextFont");
    }
    //var selecteddate = $("#txtdatepicker").val();

    var dataSourceStore = "";
    dataSourceStore = new kendo.data.DataSource({
        transport: {
            read: {

                url: "/DataTracker.svc/GetOffCycleAds",
                data: {

                    Month: monthdate
                }
            }
        },
        schema: {
            data: "d",
            total: function (d) {
                if (d.d.length > 0)
                    return d.d[0].TotalCount
            },
            model: {

                fields: {
                    Week: { editable: false, type: "string" },
                    TotalStore: { editable: false, type: "int" },
                    JobNumbers: { editable: false, type: "int" },
                    week: { editable: false, type: "int" }    
                }
            }
        },
        pageSize: 5,
        serverPaging: true

    });
    var element = $("#OffCycleGrid").kendoGrid({
        dataSource: dataSourceStore,
        detailInit: GetJobsAdTypeWeek,
        dataBound: function () {
        },
        pageable: {
            batch: true,
            pageSize: 5,
            refresh: true,
            pageSizes: true,
            input: true
        },
        columns: [
                            {
                                field: "Week",
                                title: "Week",
                                width: "50%"

                            },
                             {
                                 field: "TotalStore",
                                 title: "Total Stores",
                                 width: "50%"

                             },
                             {
                                 field: "TotalJobs",
                                 title: "Total Jobs",
                                 width: "50%"

                             }

                            ],

        editable: false,
        sortable: true,
        selectable: true

    });
    

}
// 2nd grid        

function GetJobsAdTypeWeek(e) {
    $("<div/>").appendTo(e.detailCell).kendoGrid({
        dataSource: {
            transport: {
                read: {

                    url: "/DataTracker.svc/GetAdTypeByWeek",
                    data: {
                        Week: e.data.Week,
                        Month: monthdate

                    }
                }
            },
            serverPaging: true,
            serverSorting: true,
            serverFiltering: true,
            pageSize: 5,
            schema: {
                data: "d",
                total: function (d) {
                    if (d.d.length > 0)
                        return d.d[0].TotalCount
                },
                model: {
                    id: "ID",
                    fields: {

                        GroupName: { editable: false, type: "string" },
                        TotalJobs: { editable: false, type: "string" },
                        Week: { editable: false, type: "string" }
                       
                    }
                }
            }
        },
        pageable: true,
        detailInit: GetJobsByGroupNameInit,
        dataBound: function () {
            
        },
        columns: [
                            {
                                field: "GroupName",
                                title: "Group Name",
                                width: "50%"
                            },
                            {
                                field: "TotalJobs",
                                title: "Total Jobs",

                                width: "50%"
                            },
                            
                            ],
        editable: false,
        selectable: true

    });
}

function GetJobsByGroupNameInit(e) {
    $("<div/>").appendTo(e.detailCell).kendoGrid({
        dataSource: {
            transport: {
                read: {                   
                    url: "/DataTracker.svc/GetJobsByAdType",
                    data: {
                        Week: e.data.Week,
                        Month: monthdate,
                        GroupName: e.data.GroupName
                                                
                    }
                }
            },
            serverPaging: true,
            serverSorting: true,
            serverFiltering: true,
            pageSize: 5,
            schema: {
                data: "d",
                total: function (d) {
                    if (d.d.length > 0)
                        return d.d[0].TotalCount
                },
                model: {
                    id: "ID",
                    fields: {

                        StoreId: { editable: false, type: "string" },
                        Zip: { editable: false, type: "string" },
                        Address: { editable: false, type: "string" } ,
                        AdTitle:  { editable: false, type: "string" },
                        AdDate:  { editable: false, type: "string" }                
                      
                    }
                }
            }
        },
        pageable: true,
        dataBound: function () {
           
        },
        columns: [
                            
//                            {
//                                field: "JobNumber",
//                                title: "Job Number",
//                                width: "25%"
//                            },
                            
                            {
                                field: "StoreId",
                                title: "Store ID",
                                width: "25%"
                            },
                            {
                                field: "Zip",
                                title: "Zip Code",
                                width: "10%"
                            },
                            {
                                field: "Address",
                                title: "Address",
                                width: "25%"

                            },
                             {
                                 field: "AdTitle",
                                 title: "Ad Title",
                                 width: "25%"

                             },
                              {
                                  field: "AdDate",
                                  title: "Ad Date",
                                  width: "25%"

                              }                              
                            
                            ],
        editable: false,
        selectable: true

    });
}

function AddNewAddress() {
     
}
//mapping grid

function GetMappingAddress(SearchValue) {
    var dataSourceStore = "";
    if ($("#MappingAddGrid").length > 0) {
        $("#MappingAddGrid")[0].innerHTML = "";
    }
    
    dataSourceStore = new kendo.data.DataSource({
        transport: {
            read: {

                url: "/DataTracker.svc/GetMappingAddress",
                data: {
                        SearchValue: SearchValue                   
                    }
                             
            }
        },
        schema: {
            data: "d",
            total: function (d) {
                if (d.d != null)                
                    return d.d[0].TotalCount
            },
            model: {

                fields: {
                    BannerName: { editable: false, type: "string" },
                    TotalLocations: { editable: false, type: "int" }                    
                }
            }
        },
        pageSize: 15,
        serverPaging: true

    });
    var element = $("#MappingAddGrid").kendoGrid({
        dataSource: dataSourceStore,
        detailInit: GetAllAddressByGroupInit,
        dataBound: function () {
        },
        pageable: {
            batch: true,
            pageSize: 5,
            refresh: true,
            pageSizes: true,
            input: true
        },
        columns: [
                            {
                                field: "GroupName",
                                title: "Group Name",
                                width: "50%"

                            },
                             {
                                 field: "TotalStoreID",
                                 title: "Count (StoreIDs)",
                                 width: "50%"

                             }

                            ],

        editable: false,
        sortable: true,
        selectable: true

    });
  
}

function GetAllAddressByGroupInit(e) {
    var SearchValue = "";
    if ($("#SearchTxt").val() !== "" && $("#SearchTxt").val() != "Search by group name or store id or address") {
        SearchValue=$("#SearchTxt").val();    }
    $("<div/>").appendTo(e.detailCell).kendoGrid({
        dataSource: {
            transport: {
                read: {

                    url: "/DataTracker.svc/GetMappingDetail",
                    data: {
                        GroupName: escape(e.data.GroupName),
                        SearchTxt: SearchValue
                    }
                }
               
            },
            batch: true,
            serverPaging: true,
            serverSorting: true,
            serverFiltering: true,
            pageSize: 5,
            schema: {
                data: "d",
                total: function (d) {
                    if (d.d.length > 0)
                        return d.d[0].TotalCount
                },
                model: {
                    id: "ID",
                    fields: {

                        StoreID: { editable: false, type: "string" },
                        CreateDate: { editable: false, type: "string" },
                        Address: { editable: true, type: "string" },
                        CreatedBy: { editable: false, type: "string" },
                        CreatedDate: { editable: false, type: "string" }
                    }
                }
            }
        },
        pageable: true,
        dataBound: function () {

        },
        columns: [

                            {
                                field: "StoreID",
                                title: "Store ID",
                                width: "15%"
                            },

                            {
                                field: "Address",
                                title: "Address",
                                width: "30%"
                            },
                            {
                                field: "CreatedDate",
                                title: "Last Updated date",
                                width: "20%"
                            },
                            {
                                field: "CreatedBy",
                                title: "Last UpdatedBy",
                                width: "25%"
                              
                            },
                             { command: { text: "Edit", click: EditAddress }, title: "", width: "10%"}],

        editable: false,
        selectable: true

    });
}


function CanelAddPopup() {
    var dialog = $("#Addresswindow").data("kendoWindow");
    dialog.close();
}

function AddMappingAddress() {
    var GroupName = "";
    var TxtGroup = $("#RadComboGroupName");
    if (TxtGroup.length > 0) {
        if ($("#RadComboGroupName")[0].value != "" && $("#RadComboGroupName")[0].value != "---Please select group name---") {
            GroupName = $("#RadComboGroupName")[0].value;
            $("#RadComboGroupName").addClass("forgetBox");
        }
        else {
            $("#RadComboGroupName").addClass("forgetBoxWithborder");
            alert("Please select group name");
            return false;
        }

    }

    if ($("#TxtStoreID")[0].value == "") {
        $("#TxtStoreID").addClass("forgetBoxWithborder");
        alert("Please Store ID");
        return false;
    }
    else {
        $("#TxtStoreID").addClass("forgetBox");
        // Email = $('#TxtStoreID')[0].value;
    }

    if ($("#TxtAddress")[0].value == "") {
        $("#TxtAddress").addClass("forgetBoxWithborder");
        alert("Please Address");
        return false;
    }
    else {
        $("#TxtAddress").addClass("forgetBox");
        //  Email = $('#TxtAddress')[0].value;
    }


    $.ajax({
        type: "POST",
        url: "/DataTracker.svc/AddMatchingAddress",
        data: "{\"GroupName\": \"" + escape(GroupName) + "\",\"StoreID\": \"" + $('#TxtStoreID')[0].value + "\",\"Address\": \"" + escape($('#TxtAddress')[0].value) + "\",\"IsEdit\": \""+"0"+"\"",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            var res = data.d;
            if (res != null) {
                if (res == "1") {
                    $("#AddressSaveMsg")[0].style.color = "green";
                    $("#AddressSaveMsg")[0].innerHTML = "Saved successfully";
                    CloseAndReloadMaapingGrid("1");
                }
                else if (res == "0") {
                    $("#AddressSaveMsg")[0].style.color = "red";
                    $("#AddressSaveMsg")[0].innerHTML = "Store id already exist";

                }
                else if (res == "-1") {
                    $("#AddressSaveMsg")[0].style.color = "red";
                    $("#AddressSaveMsg")[0].innerHTML = "Some technical problem occur, please retry!";

                }

            }
            else {
                $("#AddressSaveMsg")[0].style.color = "red";
                $("#AddressSaveMsg")[0].innerHTML = "Some technical problem occur, please retry!";
               
                return false;
            }
        }
    });

}

function EditPopupAddress() {
    var GroupName = "";   
    if ($("#TxtAddress")[0].value == "") {
        $("#TxtAddress").addClass("forgetBoxWithborder");
        alert("Please Address");
        return false;
    }
    else {
        $("#TxtAddress").addClass("forgetBox");
        //  Email = $('#TxtAddress')[0].value;
    }



    $.ajax({
        type: "POST",
        url: "/DataTracker.svc/AddMatchingAddress",
        data: "{\"GroupName\": \"" + GroupName + "\",\"StoreID\": \"" + $('#TxtStoreID')[0].value + "\",\"Address\": \"" + escape($('#TxtAddress')[0].value) + "\",\"IsEdit\": \""+"1"+"\"",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            var res = data.d;
            if (res != null) {
                if (res == "1") {
                    $("#AddressSaveMsg")[0].style.color = "green";
                    $("#AddressSaveMsg")[0].innerHTML = "Saved successfully";
                    CloseAndReloadMaapingGrid("1");
                }
                else if (res == "0") {
                    $("#AddressSaveMsg")[0].style.color = "red";
                    $("#AddressSaveMsg")[0].innerHTML = "Store id already exist";

                }
                else if (res == "-1") {
                    $("#AddressSaveMsg")[0].style.color = "red";
                    $("#AddressSaveMsg")[0].innerHTML = "Some technical problem occur, please retry!";

                }

            }
            else {
                $("#AddressSaveMsg")[0].style.color = "red";
                $("#AddressSaveMsg")[0].innerHTML = "Some technical problem occur, please retry!";

                return false;
            }
        }
    });

}

 function CloseAndReloadMaapingGrid(IsAdd) {
    window.parent.$("#Addresswindow").data("kendoWindow").close();
    if (IsAdd == "1") {
        window.parent.location.href = window.parent.location.href;
    }
   // setTimeout("GetMappingAddress();", 1000);
}
function EditAddress(e) {
    e.preventDefault();
    if ($("#Addresswindow").data("kendoWindow") != null) {
        var Addresswindow = $("#Addresswindow").data("kendoWindow");
        Addresswindow.close();
    }
    var dataItem = this.dataItem($(e.currentTarget).closest("tr"));
    if (dataItem != null) {
        var stID = dataItem.StoreID;
        var stAddress = dataItem.Address;
        OpenAddsPopup(stID, stAddress, "1");

    }

}
//Edit Address
function OpenAddsPopup(StoreID, stAddress, IsEdit) {
    if ($("#Addresswindow").data("kendoWindow") != null) {
        var StatusReport = $("#Addresswindow").data("kendoWindow");
        StatusReport.close();
    }
    var Url = "";
    if (IsEdit == "1")
        Url = "NewMappingAddress.aspx?StID=" + StoreID + "&Ads=" + escape(stAddress) + "";
        else
            Url = "NewMappingAddress.aspx?StID=" + " " + "&Ads=" + " " + "";
    var windowElement = $("#Addresswindow").kendoWindow({
        iframe: true,
        content: Url,
        modal: true,
        width: "550px",
        title: "Mapping Address",
        height: "300px",
        position: {
            top: 4,
            left: 2
        },
        resizable: false,
        actions: ["Close"],
        draggable: false

    });
    var dialog = $("#Addresswindow").data("kendoWindow");
    dialog.open();
    dialog.center();
    return false;
    // return false;
}

function GetMappAds() {
    if ($("#SearchTxt").val() !== "" && $("#SearchTxt").val() != "Search by group name or store id or address") {
        GetMappingAddress($("#SearchTxt").val());
    }
    else
    { GetMappingAddress(""); }
}
function Clearfilter() {
    if ($("#SearchTxt").val() !== "" && $("#SearchTxt").val() != "Search by group name or store id or address") {
        $("#SearchTxt")[0].value = "Search by group name or store id or address";
        $("#SearchTxt")[0].style.color = "#7D7D7D";
        $("#SearchTxt")[0].style.fontStyle = "italic"

    }
    GetMappingAddress("");
}
function OpenNewAddsPopup() {
    OpenAddsPopup("", "", "");

 }

