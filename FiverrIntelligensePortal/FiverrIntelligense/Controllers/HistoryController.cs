﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FiverrIntelligense.Data.Common;
using FiverrIntelligense.Data.Models;
using FiverrIntelligense.Services.IServices;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace FiverrIntelligense.Controllers
{
    [Authorize]
    public class HistoryController : Controller
    {
        IGroupService _groupService;
        IKeywordService _keywordService;
        IUserKeywordService _userKeywordService;
        public HistoryController(IGroupService groupService, IKeywordService keywordService, IUserKeywordService UserKeywordService)
        {
            _groupService = groupService;
            _keywordService = keywordService;
            _userKeywordService = UserKeywordService;
        }
        public IActionResult Index()
        {
            Guid userId = Guid.Parse(HttpContext.Session.GetString("UserId"));
            List<TblFiverrGroup> lstGfoup = _groupService.GetAllGroupsOfUsr(userId).OrderBy(x => x.GroupName).ToList();
            List<SelectListItem> myGroups = new List<SelectListItem>();
           
            foreach (var item in lstGfoup)
            {
                myGroups.Add(new SelectListItem
                {
                    Text = item.GroupName,
                    Value = item.GroupId.ToString()
                });
            }
            ViewBag.AllGroups = myGroups;
            ViewBag.UserName = HttpContext.Session.GetString("UserName");
            return View();
        }

        public ActionResult GetAllDataForGrid(string groupId, string startDate, string endDate)
        {
            List<HistoryViewModel> historyViewModels = new List<HistoryViewModel>();
            try
            {
                // code to get data from database
                Guid userId = Guid.Parse(HttpContext.Session.GetString("UserId"));
                historyViewModels = _groupService.GetAllHistory(userId, groupId,startDate,endDate).OrderBy(x => x.BussinesGroup).ToList();
            }
            catch (Exception ex)
            {
                throw;
            }
            return PartialView("_historyPartial", historyViewModels);
        }
    }
}