﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FiverrIntelligense.Data.Models;
using FiverrIntelligense.Services.IServices;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace FiverrIntelligense.Controllers
{
    [Authorize]
    public class GroupController : Controller
    {
        IGroupService _groupService;
        IUserGroupService _userGroupService;
        
        public GroupController(IGroupService groupService, IUserGroupService UserGroupService)
        {
            _groupService = groupService;
            _userGroupService = UserGroupService;
        }
        public IActionResult Index()
        {
            
            return View();
        }
        [HttpPost]
        public string AddGroup(string GroupName)
        {
            try
            {
                Guid userId = Guid.Parse(HttpContext.Session.GetString("UserId"));
                var group = _groupService.GetAll().Where(x => x.GroupName.ToLower() == GroupName.ToLower()).FirstOrDefault();
                if (group == null)
                {
                    _groupService.Insert(new TblFiverrGroup()
                    {
                        GroupName = GroupName,
                        IsActive = 1
                    });
                    _groupService.Save();
                    group = _groupService.GetAll().Where(x => x.GroupName.ToLower() == GroupName.ToLower()).FirstOrDefault();
                }
                var groupMap = _userGroupService.GetAll().Where(x => x.GroupId == group.GroupId && x.UserId == userId).FirstOrDefault();
                if (groupMap == null)
                {
                    _userGroupService.Insert(new TblUserGroupMap()
                    {
                        UserId = userId,
                        GroupId = group.GroupId
                    });
                    _userGroupService.Save();
                }
                else
                {
                    return "Already Exist.";
                }
                
                return "success";
            }
            catch (Exception)
            {
            }
            return "";
        }
        public IActionResult GetDetailsOfGroup(string groupId)
        {
            return View();
        }
    }
}