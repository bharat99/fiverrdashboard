﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FiverrIntelligense.Data.Common;
using FiverrIntelligense.Data.Models;
using FiverrIntelligense.Services.IServices;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;

namespace FiverrIntelligense.Controllers
{
    public class StatusController : Controller
    {
        IGroupService _groupService;
        IKeywordService _keywordService;
        IUserKeywordService _userKeywordService;
        private readonly IConfiguration _configuration;
        private readonly IWebHostEnvironment _env;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly ISession _session;
        public StatusController(IGroupService groupService, IKeywordService keywordService, IUserKeywordService UserKeywordService, IConfiguration configuration, IWebHostEnvironment env, IHttpContextAccessor httpContextAccessor)
        {
            _groupService = groupService;
            _keywordService = keywordService;
            _userKeywordService = UserKeywordService;
            _configuration = configuration;
            _env = env;
            _httpContextAccessor = httpContextAccessor;
            _session = _httpContextAccessor.HttpContext.Session;
        }
        public IActionResult Index()
        {
            return View();
        }

        public string GetKeywodsRunningStatus(string keywords)
        {
            string result = "";
            try
            {
                Guid userId = Guid.Parse(HttpContext.Session.GetString("UserId"));
                List<KeywordRunningStatus> lstResult = new List<KeywordRunningStatus>();
                lstResult = _keywordService.GetKeywodsRunningStatus(userId, keywords);
                result = JsonConvert.SerializeObject(lstResult);
            }
            catch (Exception ex)
            {
                throw;
            }
            return result;
        }

        public string GetKeywordProgressCount(string keywords)
        {
            string result = "";
            try
            {
                Guid userId = Guid.Parse(HttpContext.Session.GetString("UserId"));
                List<KeywordProgressCount> lstResult = new List<KeywordProgressCount>();
                lstResult = _keywordService.GetKeywordProgressCount(userId, keywords);
                result = JsonConvert.SerializeObject(lstResult);
            }
            catch (Exception ex)
            {
                throw;
            }
            return result;
        }

        #region Rolling Chart
        public string GetKeywordByGroupOfUser()
        {
            string result = "";
            try
            {
                Guid userId = Guid.Parse(HttpContext.Session.GetString("UserId"));
                List<TblFiverrGroup> lstGroup = _groupService.GetAllGroupsOfUsr(userId);
                List<Keyword> lstKeyword;
                foreach (var group in lstGroup)
                {
                    lstKeyword = _keywordService.GetKeywordByGroup(userId, group.GroupId);
                    result = JsonConvert.SerializeObject(lstKeyword);
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return result;
        }

        public void RunFiverrExeForKeywords(string keywordIds)
        {
            try
            {
                if (!string.IsNullOrEmpty(keywordIds))
                {
                    HttpContext.Session.SetString("KeywordIds", keywordIds.TrimEnd(','));
                    string[] keywordIdds = keywordIds.TrimEnd(',').Split(",");
                    if (keywordIdds.Length > 0)
                    {
                        foreach (var keywordId in keywordIdds)
                        {
                            if (!string.IsNullOrEmpty(keywordId))
                            {
                                string keyword = string.Format("KeywordID:{0}", Convert.ToInt32(keywordId));
                                try
                                {
                                    System.Diagnostics.Process.Start(_configuration.GetValue<string>("FiverrScrapper"), keyword);
                                    HttpContext.Session.SetString("isKeywordRunning", "true");
                                }
                                catch (Exception ex)
                                {
                                    throw;
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
        public string GetRollingData(string keywordIds)
        {
            var result = "";
            try
            {
                int IsKeywordCompleted = 0;
                RollingData objRollingData = new RollingData();
                List<RollingData> lstRollingData = new List<RollingData>();
                Guid userId = Guid.Parse(HttpContext.Session.GetString("UserId"));
                int secDifference = _configuration.GetValue<int>("SecondDifference");
                if (!string.IsNullOrEmpty(keywordIds))
                {
                    string[] keywordIdds = keywordIds.TrimEnd(',').Split(",");
                    if (keywordIdds.Length > 0)
                    {
                        foreach (var keywordID in keywordIdds)
                        {
                            if (!string.IsNullOrEmpty(keywordID))
                            {
                                objRollingData = _keywordService.GetRollingData(userId, Convert.ToInt32(keywordID), secDifference);
                                if (objRollingData != null)
                                {
                                    lstRollingData.Add(objRollingData);
                                    if (objRollingData.Status == "Completed")
                                    {
                                        IsKeywordCompleted++;
                                    }
                                    if (keywordIdds.Length == IsKeywordCompleted)
                                    {
                                        HttpContext.Session.SetString("isKeywordCompleted", "false");
                                    }
                                }
                            }
                        }
                        result = JsonConvert.SerializeObject(lstRollingData);
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return result;
        }
        #endregion
    }
}