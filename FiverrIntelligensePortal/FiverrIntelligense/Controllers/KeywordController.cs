﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FiverrIntelligense.Data.Models;
using FiverrIntelligense.Models;
using FiverrIntelligense.Services.IServices;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Newtonsoft.Json;

namespace FiverrIntelligense.Controllers
{
    //[Authorize]
    public class KeywordController : Controller
    {
        IGroupService _groupService;
        IKeywordService _keywordService;
        IUserKeywordService _userKeywordService;
        public KeywordController(IGroupService groupService, IKeywordService keywordService, IUserKeywordService UserKeywordService)
        {
            _groupService = groupService;
            _keywordService = keywordService;
            _userKeywordService = UserKeywordService;
        }
        public IActionResult Index()
        {
            Guid userId = Guid.Parse(HttpContext.Session.GetString("UserId"));
            List<TblFiverrGroup> lstGfoup = _groupService.GetAllGroupsOfUsr(userId).ToList();
            List<SelectListItem> myGroups = new List<SelectListItem>();
            if (lstGfoup.Count > 0)
            {
                foreach (var item in lstGfoup)
                {
                    myGroups.Add(new SelectListItem
                    {
                        Text = item.GroupName,
                        Value = item.GroupId.ToString()
                    });
                }
            }
            ViewBag.AllGroups = myGroups;
            ViewBag.UserName = HttpContext.Session.GetString("UserName");
            return View();
        }

        public string GetAllKeywordsDetails(string GroupId)
        {
            if (!string.IsNullOrEmpty(GroupId))
            {
                Guid userId = Guid.Parse(HttpContext.Session.GetString("UserId"));

                var lstKeyword = _keywordService.GetAllKeywordOfUserGroup(userId, GroupId).OrderBy(x => x.KeywordName).ToList();
                if (lstKeyword != null)
                {
                    return JsonConvert.SerializeObject(lstKeyword);
                }
            }
            return "";
        }
        public string GetAllKeywordsDetailsOfAllGroups()
        {
            Guid userId = Guid.Parse(HttpContext.Session.GetString("UserId"));
            List<TblFiverrGroup> lstGroup = _groupService.GetAllGroupsOfUsr(userId).OrderBy(x => x.GroupName).ToList();
            List<GroupWithAllKeywords> lstAllGroupWithKeeywords = new List<GroupWithAllKeywords>();
            foreach (var grp in lstGroup)
            {
                GroupWithAllKeywords tempObj = new GroupWithAllKeywords()
                {
                    GroupId = grp.GroupId,
                    GroupName = grp.GroupName,
                    lstKeyword = _keywordService.GetAllKeywordOfUserGroup(userId, grp.GroupId.ToString()).OrderBy(x => x.KeywordName).ToList()
                };
                lstAllGroupWithKeeywords.Add(tempObj);
            }
            return JsonConvert.SerializeObject(lstAllGroupWithKeeywords);
        }
        public string GetAllKeywordsOfGroup(string GroupId)
        {
            var lstKeyword = _keywordService.GetAll().Where(x => x.GroupId == Convert.ToInt32(GroupId)).ToList();
            if (lstKeyword != null)
            {
                return JsonConvert.SerializeObject(lstKeyword);
            }
            return "";
        }
        public string GetAllGroupsAndKeywords()
        {
            return JsonConvert.SerializeObject(new { Groups = _groupService.GetAll().ToList(), Keywords = _keywordService.GetAll().ToList() });
        }
        [HttpPost]
        public string AddKeyword(string keywordName, string groupId)
        {
            try
            {
                Guid userId = Guid.Parse(HttpContext.Session.GetString("UserId"));
                var keyword = _keywordService.GetAll().Where(x => x.KeywordName.ToLower() == keywordName.ToLower()).FirstOrDefault();
                if (keyword == null)
                {
                    _keywordService.Insert(new TblFiverrKeyword()
                    {
                        GroupId = Convert.ToInt32(groupId),
                        KeywordName = keywordName,
                        CreatedOn = DateTime.Now,
                        UpdatedOn = DateTime.Now.ToString()
                    });
                    _keywordService.Save();
                    keyword = _keywordService.GetAll().Where(x => x.KeywordName.ToLower() == keywordName.ToLower()).FirstOrDefault();
                }
                var keyMap = _userKeywordService.GetAll().Where(x => x.UserId == userId && x.KeywordID == keyword.KeywordID).FirstOrDefault();
                if (keyMap == null)
                {
                    _userKeywordService.Insert(new TblUserKeywordMap()
                    {
                        UserId = userId,
                        KeywordID = keyword.KeywordID
                    });
                    _userKeywordService.Save();
                }
                else
                {
                    keyMap.IsActive = 1;
                    _userKeywordService.Update(keyMap);
                    _userKeywordService.Save();
                }
                return "success";
            }
            catch (Exception)
            {
            }
            return "";
        }
        [HttpPost]
        public string RemoveKeywordFromGroup(string keywordId)
        {
            try
            {
                Guid userId = Guid.Parse(HttpContext.Session.GetString("UserId"));
                var existingKey = _userKeywordService.GetAll().Where(x => x.KeywordID == Convert.ToInt32(keywordId) && x.UserId == userId && x.IsActive == 1).FirstOrDefault();
                if (existingKey != null)
                {
                    //_userKeywordService.Delete(existingKey.Id);
                    existingKey.IsActive = 0;
                    _userKeywordService.Update(existingKey);
                    _userKeywordService.Save();
                    return "success";
                }
                return "Key word not found.";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }

        }

        [HttpPost]
        public string EditKeyword(string KeywordID, string keywordName)
        {
            try
            {
                Guid userId = Guid.Parse(HttpContext.Session.GetString("UserId"));
                var existingKey = _keywordService.GetAll().Where(x => x.KeywordID == Convert.ToInt32(KeywordID)).FirstOrDefault();
                if (existingKey != null)
                {
                    existingKey.KeywordName = keywordName;
                    _keywordService.Update(existingKey);
                    _keywordService.Save();
                    return "success";
                }
                return "Key word not found.";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }

        }
    }
}