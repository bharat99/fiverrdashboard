﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using FiverrIntelligense.Data.Common;
using FiverrIntelligense.Data.Models;
using FiverrIntelligense.Services.IServices;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;

namespace FiverrIntelligense.Controllers
{
    [Authorize]
    public class DashboardController : Controller
    {
        IGroupService _groupService;
        IKeywordService _keywordService;
        IUserKeywordService _userKeywordService;
        
        public DashboardController(IGroupService groupService, IKeywordService keywordService, IUserKeywordService UserKeywordService)
        {
            _groupService = groupService;
            _keywordService = keywordService;
            _userKeywordService = UserKeywordService;
        }
        public IActionResult Index()
        {
            Guid userId = Guid.Parse(HttpContext.Session.GetString("UserId"));
            List<TblFiverrGroup> lstGfoup = _groupService.GetAllGroupsOfUsr(userId).OrderBy(x => x.GroupName).ToList();
            List<SelectListItem> myGroups = new List<SelectListItem>();
            //myGroups.Add(new SelectListItem
            //{
            //    Text = "All",
            //    Value = "0"
            //});
            foreach (var item in lstGfoup)
            {
                myGroups.Add(new SelectListItem
                {
                    Text = item.GroupName,
                    Value = item.GroupId.ToString()
                });
            }
            ViewBag.AllGroups = myGroups;
            ViewBag.UserName = HttpContext.Session.GetString("UserName");
            return View();
        }
        public IActionResult Dashboard()
        {
            ViewBag.UserName = HttpContext.Session.GetString("UserName");
            return View();
        }
        public string GetAllKeywords(string groupId)
        {
            string result = "";
            try
            {
                Guid userId = Guid.Parse(HttpContext.Session.GetString("UserId"));
                var t = _groupService.GetAllKeywordsByGroups(userId, groupId).OrderBy(x=>x.KeywordName).ToList();
                result = JsonConvert.SerializeObject(t);
            }
            catch (Exception ex)
            {
                throw;
            }
            return result;
        }
        public string GetLastUpdatedDate(string SelectedKeywordIds)
        {
            string result = "";
            try
            {
                Guid userId = Guid.Parse(HttpContext.Session.GetString("UserId"));
                result = _groupService.GetLastUpdatedDate(userId, SelectedKeywordIds);
            }
            catch (Exception ex)
            {
                throw;
            }
            return result;
        }
        public string GetPlotData(string SelectedKeywordIds, string startDate, string endDate, string type)
        {
            string result = "";
            try
            {
                Guid userId = Guid.Parse(HttpContext.Session.GetString("UserId"));
                List<PlotData> lstPlotData =  _groupService.GetPlotData(userId, SelectedKeywordIds, startDate, endDate, type);
                //DateTime.Parse(result).ToString("yyyy-MM-dd")
                lstPlotData.ForEach(x => x.Date = DateTime.Parse(x.Date).ToString("yyyy-MM-dd"));
                result = JsonConvert.SerializeObject(lstPlotData);
            }
            catch (Exception ex)
            {

                throw;
            }
            return result;
        }
        public string GetChangesDetected(string SelectedKeywordIds, string startDate, string endDate)
        {
            string result = "";
            try
            {
                Guid userId = Guid.Parse(HttpContext.Session.GetString("UserId"));
                List<ChangeDetected> lstPlotData = _groupService.GetChangesDetected(userId, SelectedKeywordIds, startDate, endDate);
                lstPlotData = lstPlotData.OrderBy(x => x.ColumnName).ToList();
                result = JsonConvert.SerializeObject(lstPlotData);
            }
            catch (Exception ex)
            {

                throw;
            }
            return result;
        }
        public string GetAllSellerCustomerCount(string SelectedKeywordIds, string startDate, string endDate)
        {
            string result = "";
            try
            {
                Guid userId = Guid.Parse(HttpContext.Session.GetString("UserId"));
                var lstCustomer = _groupService.GetCountryWiseCustomerCount(userId, SelectedKeywordIds, startDate, endDate);
                var lstSeller = _groupService.GetCountryWiseSellerCount(userId, SelectedKeywordIds, startDate, endDate);
                List<object> lstResult = new List<object>();
                if (lstCustomer.Count > lstSeller.Count)
                {
                    foreach (var item in lstCustomer)
                    {
                        int cnt = 0;
                        var t = lstSeller.Find(x => x.Country == item.Country);
                        if (t != null)
                        {
                            cnt = t.Count;
                        }
                        lstResult.Add(new { KeywordId = item.KeywordId, KeyowrdName = item.KeywordName, Country = item.Country, CustomerCount = item.Count, SellerCount = cnt });
                    }
                }
                else
                {
                    foreach (var item in lstSeller)
                    {
                        int cnt = 0;
                        var t = lstCustomer.Find(x => x.Country == item.Country);
                        if (t != null)
                        {
                            cnt = t.Count;
                        }
                        lstResult.Add(new { KeywordId = item.KeywordId, KeywordName = item.KeywordName, Country = item.Country, CustomerCount = cnt, SellerCount = item.Count });
                    }
                }
                result = JsonConvert.SerializeObject(lstResult);
            }
            catch (Exception ex)
            {
                throw;
            }
            return result;
        }

        public string GetChangesDetectedChartDetail(string SelectedKeywordIds, string startDate, string endDate,string label)
        {
            string result = "";
            try
            {
                Guid userId = Guid.Parse(HttpContext.Session.GetString("UserId"));
                List<ChangeDetectedDetails> lstChange = _groupService.GetChangesDetectedDetailsByLabel(userId, SelectedKeywordIds, startDate, endDate, label);

                result = JsonConvert.SerializeObject(lstChange);
            }
            catch (Exception ex)
            {
                throw;
            }
            return result;
        }

        #region Feedback Changes - Add View button for dahsboard chart and Display records on Popup
        public string GetSellerByUserAssignedGroup(string KeywordIds, string startDate, string endDate)
        {
            string result = "";
            try
            {
                Guid userId = Guid.Parse(HttpContext.Session.GetString("UserId"));
                List<Seller> lstSellerData = _groupService.GetSellerByUserAssignedGroup(userId, KeywordIds, startDate, endDate);
                result = JsonConvert.SerializeObject(lstSellerData);
            }
            catch (Exception)
            {

                throw;
            }
            return result;
        }

        public string GetGigByUserAssignedGroup(string KeywordIds, string startDate, string endDate)
        {
            string result = "";
            try
            {
                Guid userId = Guid.Parse(HttpContext.Session.GetString("UserId"));
                List<Gig> lstGigData = _groupService.GetGigByUserAssignedGroup(userId, KeywordIds, startDate, endDate);
                result = JsonConvert.SerializeObject(lstGigData);
            }
            catch (Exception)
            {

                throw;
            }
            return result;
        }

        public string GetNewOrderByUserAssignedGroup(string KeywordIds, string startDate, string endDate)
        {
            string result = "";
            try
            {
                Guid userId = Guid.Parse(HttpContext.Session.GetString("UserId"));
                List<CustomerReview> lstCustomerData = _groupService.GetNewOrderByUserAssignedGroup(userId, KeywordIds, startDate, endDate);
                result = JsonConvert.SerializeObject(lstCustomerData);
            }
            catch (Exception)
            {

                throw;
            }
            return result;
        }

        public string GetRemovedSellerByUserAssignedGroup(string KeywordIds, string startDate, string endDate)
        {
            string result = "";
            try
            {
                Guid userId = Guid.Parse(HttpContext.Session.GetString("UserId"));
                List<Seller> lstSellerData = _groupService.GetRemovedSellerByUserAssignedGroup(userId, KeywordIds, startDate, endDate);
                result = JsonConvert.SerializeObject(lstSellerData);
            }
            catch (Exception)
            {

                throw;
            }
            return result;
        }

        public string GetRemovedGigByUserAssignedGroup(string KeywordIds, string startDate, string endDate)
        {
            string result = "";
            try
            {
                Guid userId = Guid.Parse(HttpContext.Session.GetString("UserId"));
                List<Gig> lstGigData = _groupService.GetRemovedGigByUserAssignedGroup(userId, KeywordIds, startDate, endDate);
                result = JsonConvert.SerializeObject(lstGigData);
            }
            catch (Exception)
            {

                throw;
            }
            return result;
        }
        #endregion
    }
}