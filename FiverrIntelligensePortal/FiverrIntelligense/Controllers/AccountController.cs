﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using FiverrIntelligense.Common;
using FiverrIntelligense.Data.Models;
using FiverrIntelligense.Models;
using FiverrIntelligense.Services.IServices;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;

namespace FiverrIntelligense.Controllers
{
    public class AccountController : Controller
    {
        IUsersService _usersRepo;
        IUserTypeService _userTypeRepo;
        private static List<TblUserType> lstRoles = new List<TblUserType>();
        public AccountController(IUsersService usersRepo, IUserTypeService UserTypeRepo)
        {
            _usersRepo = usersRepo;
            _userTypeRepo = UserTypeRepo;
            lstRoles = _userTypeRepo.GetAll().ToList();
        }
        // GET: Account
        public ActionResult Index()
        {
            ViewBag.Page = "Login";
            return View();
        }

        [HttpPost]
        public ActionResult Login(LoginViewModel obj)
        {
            List<TblUsers> lstAllUsers = _usersRepo.GetAll().ToList();
            var existingUser = lstAllUsers.Where(x => x.Email.ToLower().Equals(obj.LoginEmail.ToLower()) && x.Password == obj.LoginPassword).FirstOrDefault();
            if (existingUser != null)
            {
                if (existingUser.RowStatus == 4)
                {
                    ViewBag.Page = "Verify";
                    var verifyObj = new VerifyMailViewModel();
                    verifyObj.UserEmail = obj.LoginEmail;
                    ViewBag.UserId = obj.LoginEmail;
                    ModelState.AddModelError("", "Email is not verified. Please verify.");
                    return View("Index", verifyObj);
                }
                else if (existingUser.RowStatus == 1)
                {

                    var userClaims = new List<Claim>()
                    {
                    new Claim(ClaimTypes.Name, existingUser.UserName),
                    new Claim(ClaimTypes.Email, existingUser.Email),
                     };

                    var grandmaIdentity = new ClaimsIdentity(userClaims, "User Identity");

                    var userPrincipal = new ClaimsPrincipal(new[] { grandmaIdentity });
                    HttpContext.SignInAsync(userPrincipal);

                    #region session management
                    string UserName = existingUser.Fname + (existingUser.Lname != " " ? " " + existingUser.Lname : "");
                    UserName = string.IsNullOrEmpty(UserName.Trim()) ? existingUser.Email : UserName;
                    HttpContext.Session.SetString("UserName", UserName);
                    HttpContext.Session.SetString("UserId", existingUser.UserId.ToString());
                    HttpContext.Session.SetString("EmailId", existingUser.Email);
                    HttpContext.Session.SetString("Role", lstRoles.Where(x => x.TypeId == existingUser.TypeId).First().TypeName);
                    #endregion session management
                    return RedirectToAction("Index", "Dashboard");
                }
            }
            ViewBag.Page = "Login";
            ModelState.AddModelError("", "Invalid Credential.");
            return View("Index", obj);
        }
        public IActionResult Logout()
        {
            HttpContext.SignOutAsync().GetAwaiter().GetResult();
            return RedirectToAction("Index", "account");
        }
        public ActionResult Register()
        {
            ViewBag.Page = "Register";
            return View("Index");
        }
        [HttpPost]
        public ActionResult Register(RegistrationViewModel obj)
        {
            ViewBag.Page = "Register";
            try
            {
                if (obj != null)
                {
                    List<TblUsers> lstAllUsers = _usersRepo.GetAll().ToList();
                    var existingUser = lstAllUsers.Where(x => x.Email.ToLower().Equals(obj.Email.ToLower())).FirstOrDefault();
                    if (existingUser != null)
                    {
                        ModelState.AddModelError("", "User Already Exist with " + obj.Email + ".");
                        return View("Index");
                    }
                    // insert in data base
                    TblUsers newUser = new TblUsers();
                    newUser.Fname = obj.FName;
                    newUser.Lname = obj.LName;
                    newUser.Email = obj.Email;
                    newUser.UserName = obj.Email;
                    newUser.Password = obj.Password;
                    newUser.CreatedDate = DateTime.Now;
                    var allUserType = lstRoles;
                    if (lstAllUsers.Count > 0)
                    {
                        var type = allUserType.Where(x => x.TypeName.ToLower().Equals("user")).FirstOrDefault();
                        newUser.TypeId = type.TypeId;
                    }
                    else
                    {
                        var type = allUserType.Where(x => x.TypeName.ToLower().Equals("admin")).FirstOrDefault();
                        newUser.TypeId = type.TypeId;
                    }

                    //// email sending to verify email
                    string VerificationCode = CodeHelper.GetRandomCode();
                    newUser.VerificationCode = VerificationCode;
                    _usersRepo.Insert(newUser);
                    _usersRepo.Save();

                    string url = "https://localhost:44348/Account/EmailVerification?Uts=" + newUser.UserId;
                    //EmailHelper.sendMail("Dherajbhatt889@gmail.com", "A99887766s@", "sonalimishar889@gmail.com", "Email Verification for Fiverr", "<!DOCTYPE html> <html> <head> <meta charset=\"utf-8\" /> <title></title> </head> <body>Hi, <br/><br/>Your verification code is <b>" + VerificationCode + "</b>. Input this at current page or use <b><a href=\"" + url + "\">This</a></b> link to verify your email.<br/><br/>Thanks. Fiverr Administration <b style=\"color: red\"></b>Note: This is auto generated mail. Do not reply. </body> </html>", true);
                    EmailHelper.sendMail("Dherajbhatt889@gmail.com", "A99887766s@", newUser.Email, "Email Verification for Fiverr", "<!DOCTYPE html> <html> <head> <meta charset=\"utf-8\" /> <title></title> </head> <body>Hi, <br/><br/>Your verification code is <b>" + VerificationCode + "</b>. Input this at current page or use <b><a href=\"" + url + "\">This</a></b> link to verify your email.<br/><br/>Thanks. Fiverr Administration <b style=\"color: red\"></b>Note: This is auto generated mail. Do not reply. </body> </html>", true);
                    ViewBag.Page = "Verify";
                    ViewBag.UserId = newUser.UserId;
                    ViewBag.Email = newUser.Email;
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return View("Index");
        }
        public string SendOtp(string emailId)
        {
            try
            {
                var user = _usersRepo.GetAll().Where(x => x.Email == emailId).FirstOrDefault();
                if (user == null)
                {
                    return "Email not exist. Enter registered email.";
                }
                string VerificationCode = CodeHelper.GetRandomCode();
                user.VerificationCode = VerificationCode;
                _usersRepo.Update(user);
                _usersRepo.Save();
                EmailHelper.sendMail("Dherajbhatt889@gmail.com", "A99887766s@", emailId, "Email Verification for Fiverr", "<!DOCTYPE html> <html> <head> <meta charset=\"utf-8\" /> <title></title> </head> <body>Hi, <br/><br/>Your verification code is <b>" + VerificationCode + "</b>. <br/><br/> Thanks. Fiverr Administration <b style=\"color: red\"></b>Note: This is auto generated mail. Do not reply. </body> </html>", true);
            }
            catch (Exception exs)
            {
                return "Email not exist. Enter registered email.";
            }
            return "Otp sent.";
        }

        public string VerifyOtp(string emailId, string otp)
        {
            try
            {
                var user = _usersRepo.GetAll().Where(x => x.Email == emailId).FirstOrDefault();
                if (user == null)
                {
                    return "Email not exist. Enter registered email.";
                }
                if (user.VerificationCode != otp)
                {
                    return "Wrong otp.";
                }
                return "Success";
            }
            catch (Exception exs)
            {
                return "Email not exist. Enter registered email.";
            }
        }
        public ActionResult ForgetPassword()
        {
            ViewBag.Page = "ForgetPassword";
            return View("Index");
        }
        [HttpPost]
        public ActionResult ForgetPassword(ForgetPasswordViewModel obj)
        {
            var user = _usersRepo.GetAll().Where(x => x.Email == obj.EmailForget).FirstOrDefault();
            if (user == null)
            {
                return View("Index");
            }
            user.Password = obj.ForgotPassword;
            _usersRepo.Update(user);
            _usersRepo.Save();
            ViewBag.Page = "Login";
            return View("Index");
        }
        public ActionResult EmailVerification(Guid Uts)
        {
            var user = _usersRepo.GetAll().Where(x => x.UserId == Uts).FirstOrDefault();
            if (user != null)
            {
                user.VerificationCode = "";
                user.RowStatus = 1;

                _usersRepo.Update(user);
                _usersRepo.Save();
            }
            return View();
        }
        [HttpPost]
        public ActionResult EmailVerification(VerifyMailViewModel obj)
        {
            var user = _usersRepo.GetAll().Where(x => x.Email == obj.UserEmail).FirstOrDefault();
            if (user != null)
            {
                if (obj.VerifyCode != user.VerificationCode)
                {
                    ViewBag.Page = "Verify";
                    ModelState.AddModelError("", "Wrong Otp. Try Again.");
                    ViewBag.UserId = obj.VerifyCode;
                    return View("Index");
                }
                user.VerificationCode = "";
                user.RowStatus = 1;
                _usersRepo.Update(user);
                _usersRepo.Save();
                return View();
            }
            ModelState.AddModelError("", "Invalid Email Id.");
            ViewBag.Page = "login";
            return View("Index");
        }
    }
}