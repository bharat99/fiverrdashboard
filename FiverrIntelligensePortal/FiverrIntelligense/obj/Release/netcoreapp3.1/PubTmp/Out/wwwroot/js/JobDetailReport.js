﻿var year = "";
function GetAllDetail(CType, Week, month, type, GroupName, StoreName, IsGType, yr) {
    var dataSourceStore = "";
    year = yr;
    if (type.indexOf("^")!=-1) {
        var at = type.split('^');

        if (at[0] == 11) {
            if (at[1] == 0) {
                alert("No Data");
                return false;
             }
            else {
                $("#btnexport")[0].style.display = "none";
                dataSourceStore = new kendo.data.DataSource({
                    transport: {
                        read: {

                            url: "/DataTracker.svc/GetJobforSecondary",
                            data: {

                                date: month,
                                week: at[1],
                                TypeID: at[0],
                                yyr: year
                            }
                        }
                    },
                    schema: {
                        data: "d",
                        total: function (d) {
                            if (d.d.length > 0)
                                return d.d[0].TotalCount
                        },
                        model: {

                            fields: {
                                Date: { editable: false, type: "string" },
                                TotalStore: { editable: false, type: "string" },
                                TotalJobs: { editable: false, type: "int" }
                            }
                        }
                    },

                    pageSize: 10,
                    serverPaging: true

                });
                $("#reportjDetailDategrid")[0].innerHTML = "";
                var element = $("#reportjDetailDategrid").kendoGrid({
                    dataSource: dataSourceStore,
                    detailInit: GetAllSecondaryByGroup,
                    dataBound: function () {
                    },
                    pageable: {
                        batch: true,
                        pageSize: 10,
                        refresh: true,
                        pageSizes: true,
                        input: true
                    },


                    columns: [
                            {
                                field: "Date",
                                title: "Date",
                                width: "60%"
                            },
                                                        {
                                                            field: "TotalStore",
                                                            title: "Total Stores",
                                                            width: "60%"
                                                        },
                            {
                                field: "TotalJobs",
                                title: "Total Jobs",
                                width: "40%"
                            }
                            ],




                    editable: false,
                    sortable: true,
                    selectable: true

                });
            }
        }
    }
    else if (CType == "1") {
        dataSourceStore = new kendo.data.DataSource({
            transport: {
                read: {

                    url: "/DataTracker.svc/GetOffcyleAdstitleGroup",
                    data: {

                        date: month,
                        Week: Week,
                        type: type,
                        GroupName: GroupName,
                        yyr: year
                    }
                }
            },
            schema: {
                data: "d",
                total: function (d) {
                    if (d.d.length > 0)
                        return d.d[0].TotalCount
                },
                model: {

                    fields: {
                        AdTitle: { editable: false, type: "string" },
                        AdDate: { editable: false, type: "string" },
                        StoreCount: { editable: false, type: "int" }
                    }
                }
            },

            pageSize: 10,
            serverPaging: true

        });
        var element = $("#reportjDetailDategrid").kendoGrid({
            dataSource: dataSourceStore,
            detailInit: GetAdtitleByDate,
            dataBound: function () {
            },
            pageable: {
                batch: true,
                pageSize: 10,
                refresh: true,
                pageSizes: true,
                input: true
            },
            columns: [

                              {
                                  field: "AdDate",
                                  title: "Ad Date",
                                  width: "30%"
                              },
                              {
                                  field: "StoreCount",
                                  title: "Store Count",
                                  width: "40%"

                              },
                               {
                                   field: "AdTitle",
                                   title: "Total Jobs",
                                   width: "30%"
                               },

                            ],

            editable: false,
            sortable: true,
            selectable: true

        });


    }

    else if (type == "50") {

        if ($("#reportjDetailDategrid").length > 0)
        { $("#reportjDetailDategrid")[0].innerHTML = ""; }

        $("#btnexport")[0].style.display = "block";
        dataSourceStore = new kendo.data.DataSource({
        transport: {

            read: {

                url: "/DataTracker.svc/GetAnalysisReport",

                data: {

                    month: month,
                    week: Week,
                    year: yr

                }
            }
        },

        schema: {
                        data: "d",
                        total: function (d) {
                            if (d.d.length > 0)
                                return d.d[0].TotalCount
                        },

                        model: {
                        id: "ID",
                            fields: {
                                ID: { editable: false, type: "number" },
                                websitename: { editable: false, type: "string" },
                                Reportedby: { editable: false, type: "string" },
                                Adstype: { editable: false, type: "string" },
                                Reporteddate: { editable: false, type: "string" }
                            }
                        }
                    }

    });

            var element = $("#reportjDetailDategrid").kendoGrid({
            dataSource: dataSourceStore,
            dataBound: function () {

            },
            pageable: {
                pageSize: 15,
                refresh: true,
                pageSizes: true,
                input: true
            },
            columns: [
                            {
                                field: "websitename",
                                title: "Website Name",
                                width: "30%"
                            },
                                                        {
                                                            field: "Reportedby",
                                                            title: "ReportedBy",
                                                            width: "20%"
                                                        },
                                                        {
                                                            field: "Adstype",
                                                            title: "Ads type",
                                                            width: "20%"
                                                        },
                                                        {
                                                            field: "Reporteddate",
                                                            title: "Reported Date",
                                                            width: "30%"
                                                        }


                        ],
            editable: false,
            sortable: true,
            selectable: true

        });
    }

    else {

        dataSourceStore = "";
        if (IsGType == "a") {
            dataSourceStore = new kendo.data.DataSource({
                transport: {
                    read: {

                        url: "/DataTracker.svc/GetJobCountByTypeID",
                        data: {

                            date: month,
                            week: Week,
                            TypeID: type,
                            yyr: year
                        }
                    }
                },
                schema: {
                    data: "d",
                    total: function (d) {
                        if (d.d.length > 0)
                            return d.d[0].TotalCount
                    },
                    model: {

                        fields: {
                            GroupName: { editable: false, type: "string" },
                            TotalJobs: { editable: false, type: "int" }
                        }
                    }
                },

                pageSize: 10,
                serverPaging: true

            });
            $("#reportjDetailDategrid")[0].innerHTML = "";
            var element = $("#reportjDetailDategrid").kendoGrid({
                dataSource: dataSourceStore,
                detailInit: GetAllStorescountByGroup,
                dataBound: function () {
                },
                pageable: {
                    batch: true,
                    pageSize: 10,
                    refresh: true,
                    pageSizes: true,
                    input: true
                },


                columns: [
                            {
                                field: "GroupName",
                                title: "Group Name",
                                width: "60%"
                            },
                            {
                                field: "TotalJobs",
                                title: "Total jobs",
                                width: "40%"
                            }
                            ],




                editable: false,
                sortable: true,
                selectable: true

            });
        }
        else if (IsGType == "g") {

            dataSourceStore = new kendo.data.DataSource({
                transport: {
                    read: {
                        //using jsfiddle echo service to simulate JSON endpoint
                        url: "/DataTracker.svc/GetJobCountWithStoreByTypeID",
                        data: {
                            date: month,
                            Week: Week,
                            TypeID: type,
                            GroupName: GroupName,
                            IsExport: "1",
                            yyr: year
                        }
                    }
                },
                schema: {
                    data: "d",
                    total: function (d) {
                        if (d.d.length > 0)
                            return d.d[0].TotalCount
                    },
                    model: {
                        id: "ID",
                        fields: {

                            StoreName: { editable: false, type: "string" },
                            TotalJobs: { editable: false, type: "int" }
                        }
                    }
                },
                pageSize: 10,
                serverPaging: true


            });

            $("#reportjDetailDategrid")[0].innerHTML = "";
            var element = $("#reportjDetailDategrid").kendoGrid({
                dataSource: dataSourceStore,
                detailInit: GetAllStoresByGroup,
                dataBound: function () {
                },
                pageable: {
                    batch: true,
                    pageSize: 10,
                    refresh: true,
                    pageSizes: true,
                    input: true
                },
                columns: [
                            {
                                field: "StoreName",
                                title: "Store Name",
                                width: "50%"
                            },
                             {
                                 field: "TotalJobs",
                                 title: "Total jobs",
                                 width: "50%"
                             }

                            ],
                editable: false,
                selectable: true

            });

        }
        else if (IsGType == "s") {
            dataSourceStore = new kendo.data.DataSource({
                transport: {
                    read: {
                        //using jsfiddle echo service to simulate JSON endpoint
                        url: "/DataTracker.svc/GetJobReport_Details",
                        data: {
                            week: QWeek,
                            date: Mth,
                            type: TypeID,
                            GroupName: GroupName,
                            StoreName: StoreName,
                            IsExport: "1",
                            yyr: year


                        }
                    }
                },
                serverPaging: true,
                serverSorting: true,
                serverFiltering: true,
                pageSize: 20,
                schema: {
                    data: "d",
                    total: function (d) {
                        if (d.d.length > 0)
                            return d.d[0].TotalCount
                    },
                    model: {
                        id: "ID",
                        fields: {

                            StoreName: { editable: false, type: "string" },
                            StoreId: { editable: false, type: "string" },
                            Address: { editable: false, type: "string" },
                            Zip: { editable: false, type: "string" },
                            JobNumber: { editable: false, type: "int" }
                        }
                    }

                },
                pageSize: 10,
                serverPaging: true

            });

            $("#reportjDetailDategrid")[0].innerHTML = "";
            var element = $("#reportjDetailDategrid").kendoGrid({
                dataSource: dataSourceStore,
                dataBound: function () {
                },
                pageable: {
                    batch: true,
                    pageSize: 10,
                    refresh: true,
                    pageSizes: true,
                    input: true
                },
                columns: [


                              {
                                  field: "StoreId",
                                  title: "Store Id",
                                  width: "20%"
                              },
                               {
                                   field: "Address",
                                   title: "Address",
                                   width: "30%"

                               },
                            {
                                field: "Zip",
                                title: "Zip Code",
                                width: "20%"
                            },

                            {
                                field: "JobNumber",
                                title: "Job Number",
                                width: "30%"
                            }

                            ],
                editable: false,
                selectable: true

            });

        }
    }
}

function GetAdtitleByDate(e) {
    $("<div/>").appendTo(e.detailCell).kendoGrid({
        dataSource: {
            transport: {
                read: {
                    //using jsfiddle echo service to simulate JSON endpoint
                    url: "/DataTracker.svc/GetAdtitleByDate",
                    data: {
                        week: QWeek,
                        AdDate: e.data.AdDate,
                        GroupName: Gname

                    }
                }
            },

            serverPaging: true,
            serverSorting: true,
            serverFiltering: true,
            pageSize: 20,
            schema: {
                data: "d",
                total: function (d) {
                    if (d.d.length > 0)
                        return d.d[0].TotalCount
                },
                model: {
                    id: "ID",
                    fields: {

                        GroupName: { editable: false, type: "string" },
                        AdTitle: { editable: false, type: "string" },
                        AdDate: { editable: false, type: "string" },
                        JobCount: { editable: false, type: "int" }
                        //  Address: { editable: false, type: "string" },
                        //  Zip: { editable: false, type: "string" },
                        // JobNumber: { editable: false, type: "int" }
                    }
                }
            }
        },
        pageable: true,
        detailInit: GetDetailByAdtitle,
        dataBound: function () {
            // this.expandRow(this.tbody.find("tr.k-master-row").first());             
        },
        columns: [

                                {
                                    field: "GroupName",
                                    title: "Group Name",
                                    width: "40%"
                                },
                              {
                                  field: "AdTitle",
                                  title: "Ad Title",
                                  width: "40%"
                              },
                               {
                                   field: "JobCount",
                                   title: "Total Jobs",
                                   width: "20%"
                               }

                            ],
        editable: false,
        selectable: true

    });
}


function GetDetailByAdtitle(e) {
    $("<div/>").appendTo(e.detailCell).kendoGrid({
        dataSource: {
            transport: {
                read: {
                    //using jsfiddle echo service to simulate JSON endpoint
                    url: "/DataTracker.svc/GetDetailByAdtitle",
                    data: {
                        week: QWeek,
                        Adtitle: e.data.AdTitle,
                        GName: e.data.GroupName,
                        AdDate: e.data.AdDate,
                        yyr: year

                    }
                }
            },
            serverPaging: true,
            serverSorting: true,
            serverFiltering: true,
            pageSize: 20,
            schema: {
                data: "d",
                total: function (d) {
                    if (d.d.length > 0)
                        return d.d[0].TotalCount
                },
                model: {
                    id: "ID",
                    fields: {

                        StoreName: { editable: false, type: "string" },
                        StoreId: { editable: false, type: "string" },
                        Address: { editable: false, type: "string" },
                        Zip: { editable: false, type: "string" },
                        JobNumber: { editable: false, type: "int" }
                    }
                }
            }
        },
        pageable: true,
        dataBound: function () {
            // this.expandRow(this.tbody.find("tr.k-master-row").first());             
        },
        columns: [


                              {
                                  field: "StoreId",
                                  title: "Store Id",
                                  width: "20%"
                              },
                               {
                                   field: "Address",
                                   title: "Address",
                                   width: "30%"

                               },
                            {
                                field: "Zip",
                                title: "Zip Code",
                                width: "20%"
                            },

                            {
                                field: "JobNumber",
                                title: "Job Number",
                                width: "30%"
                            }

                            ],
        editable: false,
        selectable: true

    });
}


function GetAllStoresByGroup(e) {
    $("<div/>").appendTo(e.detailCell).kendoGrid({
        dataSource: {
            transport: {
                read: {
                    //using jsfiddle echo service to simulate JSON endpoint
                    url: "/DataTracker.svc/GetJobReport_Details",
                    data: {
                        week: QWeek,
                        date: Mth,
                        type: TypeID,
                        GroupName: e.data.GroupName,
                        StoreName: "",
                        IsExport: "0",
                        yyr: year


                    }
                }
            },
            serverPaging: true,
            serverSorting: true,
            serverFiltering: true,
            pageSize: 20,
            schema: {
                data: "d",
                total: function (d) {
                    if (d.d.length > 0)
                        return d.d[0].TotalCount
                },
                model: {
                    id: "ID",
                    fields: {

                        StoreName: { editable: false, type: "string" },
                        StoreId: { editable: false, type: "string" },
                        Address: { editable: false, type: "string" },
                        Zip: { editable: false, type: "string" },
                        JobNumber: { editable: false, type: "int" }
                    }
                }
            }
        },
        pageable: true,
        dataBound: function () {
            // this.expandRow(this.tbody.find("tr.k-master-row").first());             
        },
        columns: [
                                               {
                                                   field: "StoreName",
                                                   title: "Store Name",
                                                   width: "30%"
                                               },
                                          {
                                              field: "StoreId",
                                              title: "Store Id",
                                              width: "10%"
                                          },
                                           {
                                               field: "Address",
                                               title: "Address",
                                               width: "20%"

                                           },
                                           {
                                               field: "Zip",
                                               title: "Zip Code",
                                               width: "20%"
                                           },

                                            {
                                                field: "JobNumber",
                                                title: "Job Number",
                                                width: "20%"
                                            }

                                            ],
        editable: false,
        selectable: true

    });
}


//
function GetAllStoresByGroup(e) {
    $("<div/>").appendTo(e.detailCell).kendoGrid({
        dataSource: {
            transport: {
                read: {
                    //using jsfiddle echo service to simulate JSON endpoint
                    url: "/DataTracker.svc/GetJobReport_Details",
                    data: {
                        week: QWeek,
                        date: Mth,
                        type: TypeID,
                        GroupName: "",
                        StoreName: e.data.StoreName,
                        IsExport: "0",
                        yyr: year



                    }
                }
            },
            serverPaging: true,
            serverSorting: true,
            serverFiltering: true,
            pageSize: 20,
            schema: {
                data: "d",
                total: function (d) {
                    if (d.d.length > 0)
                        return d.d[0].TotalCount
                },
                model: {
                    id: "ID",
                    fields: {

                        StoreName: { editable: false, type: "string" },
                        StoreId: { editable: false, type: "string" },
                        Address: { editable: false, type: "string" },
                        Zip: { editable: false, type: "string" },
                        JobNumber: { editable: false, type: "int" }
                    }
                }
            }
        },
        pageable: true,
        dataBound: function () {
            // this.expandRow(this.tbody.find("tr.k-master-row").first());             
        },
        columns: [
                                               {
                                                   field: "StoreName",
                                                   title: "Store Name",
                                                   width: "30%"
                                               },
                                          {
                                              field: "StoreId",
                                              title: "Store Id",
                                              width: "10%"
                                          },
                                           {
                                               field: "Address",
                                               title: "Address",
                                               width: "20%"

                                           },
                                           {
                                               field: "Zip",
                                               title: "Zip Code",
                                               width: "20%"
                                           },

                                            {
                                                field: "JobNumber",
                                                title: "Job Number",
                                                width: "20%"
                                            }

                                            ],
        editable: false,
        selectable: true

    });
}


function GetAllStorescountByGroup(e) {
    $("<div/>").appendTo(e.detailCell).kendoGrid({
        dataSource: {
            transport: {
                read: {
                    //using jsfiddle echo service to simulate JSON endpoint
                    url: "/DataTracker.svc/GetJobCountWithStoreByTypeID",
                    data: {
                        Week: QWeek,
                        date: Mth,
                        TypeID: TypeID,
                        GroupName: e.data.GroupName,
                        IsExport: "0",
                        yyr: year

                    }
                }
            },
            serverPaging: true,
            serverSorting: true,
            serverFiltering: true,
            pageSize: 20,
            schema: {
                data: "d",
                total: function (d) {
                    if (d.d.length > 0)
                        return d.d[0].TotalCount
                },
                model: {
                    id: "ID",
                    fields: {

                        StoreName: { editable: false, type: "string" },
                        TotalJobs: { editable: false, type: "int" }
                    }
                }
            }
        },
        detailInit: GetAllStoresByGroup,
        pageable: true,
        dataBound: function () {
            // this.expandRow(this.tbody.find("tr.k-master-row").first());             
        },
        columns: [
                            {
                                field: "StoreName",
                                title: "Store Name",
                                width: "50%"
                            },
                             {
                                 field: "TotalJobs",
                                 title: "Total jobs",
                                 width: "50%"
                             }
                            ],
        editable: false,
        selectable: true

    });
}

function GetAllSecondaryByGroup(e) {
    $("<div/>").appendTo(e.detailCell).kendoGrid({
        dataSource: {
            transport: {
                read: {
                    //using jsfiddle echo service to simulate JSON endpoint
                    url: "/DataTracker.svc/GetJobforSecondaryGroup",
                    data: {
                        date: e.data.Date

                    }
                }
            },
            serverPaging: true,
            serverSorting: true,
            serverFiltering: true,
            pageSize: 20,
            schema: {
                data: "d",
                total: function (d) {
                    if (d.d.length > 0)
                        return d.d[0].TotalCount
                },
                model: {
                    id: "ID",
                    fields: {

                        GroupName: { editable: false, type: "string" },
                        TotalJobs: { editable: false, type: "int" },
                        Date: { editable: false, type: "string" }
                    }
                }
            }
        },
        detailInit: GetsecondaryAllDetail,
        pageable: true,
        dataBound: function () {
            // this.expandRow(this.tbody.find("tr.k-master-row").first());             
        },
        columns: [
                            {
                                field: "GroupName",
                                title: "Store Name",
                                width: "50%"
                            },
                             {
                                 field: "TotalJobs",
                                 title: "Total jobs",
                                 width: "50%"
                             }
                            ],
        editable: false,
        selectable: true

    });
}

function GetsecondaryAllDetail(e) {
    $("<div/>").appendTo(e.detailCell).kendoGrid({
        dataSource: {
            transport: {
                read: {
                    //using jsfiddle echo service to simulate JSON endpoint
                    url: "/DataTracker.svc/GetDetailSecondaryJobs",
                    data: {                  
                        date: e.data.Date,
                        sname: e.data.GroupName



                    }
                }
            },
            serverPaging: true,
            serverSorting: true,
            serverFiltering: true,
            pageSize: 20,
            schema: {
                data: "d",
                total: function (d) {
                    if (d.d.length > 0)
                        return d.d[0].TotalCount
                },
                model: {
                    id: "ID",
                    fields: {

                        storeid: { editable: false, type: "string" },
                        address: { editable: false, type: "string" },
                        zip: { editable: false, type: "string" },
                        job_Number: { editable: false, type: "string" }
                    }
                }
            }
        },
        pageable: true,
        dataBound: function () {
            // this.expandRow(this.tbody.find("tr.k-master-row").first());             
        },
        columns: [
                                               {
                                                   field: "storeid",
                                                   title: "Store Id",
                                                   width: "20%"
                                               },
                                           {
                                               field: "address",
                                               title: "Address",
                                               width: "30%"

                                           },
                                           {
                                               field: "zip",
                                               title: "Zip Code",
                                               width: "20%"
                                           },

                                            {
                                                field: "job_Number",
                                                title: "Job Number",
                                                width: "20%"
                                            }

                                            ],
        editable: false,
        selectable: true

    });
}