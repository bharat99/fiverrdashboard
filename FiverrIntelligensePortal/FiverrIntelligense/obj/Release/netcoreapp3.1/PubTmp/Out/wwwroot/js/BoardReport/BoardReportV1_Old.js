﻿var TotalBoardList = "";
var flag = 19;
var listIndex = 0;
var addvaluetomultiplylist = 1;
var filtersAllBoardid = [];
var currentText1 = "";
var DomainURL = "";
var usertypeid = 0;

$(document).ready(function () {
    $('#BordReportGrid').css('display', 'block');
    $("#DeletedSelectedForcast").css('display', 'none');
    $(".forecast_yellow").css('display', 'none');
    $("#displayselected").parent().css('display', 'none');
    $("#DeleteSelected").css('display', 'none');
    $('.list').addClass('grid-view');
    DomainURL = window.location.origin;
    BindGetboardteamclientaccountmanagerOwner();
    TotalBoardOnReport();
    $('.hover-frame').slideUp();
    var current_url = window.location.href;
    var urlightm = current_url.split('=');


    $('.navbar-toggle').live('click', function () {

        $('.collapse').slideToggle();
    });


    $('.tabular tr td:nth-child(1) a').live('hover', function () {
        //$('.board-sumary-hover-cont').toggle();
        $(this).parent().find('.board-sumary-hover-cont').animate({ width: 'toggle' });

    });

    if (urlightm[1] == "1") {
        $('#loadboard').css('display', 'none');
        $('#BordReportList').css('display', 'block');
        $('#BordReportGrid').css('display', 'none');

        $('.list-1').addClass("grid - view");

        $('.list-1').addClass('list-1-select');
        $('.listmethod ').removeClass('grid-view');
    }
    else {
        $('#loadboard').css('display', 'block');
        $('.list').addClass("grid - view");
        $('.list-1').removeClass("grid - view");
        $('#BordReportGrid').css('display', 'block');
        $('#BordReportList').css('display', 'none');
    }
    //TotalBoardOnReportList();
    $('.listmethod').click(function () {
        var viewType = $(this).attr('ViewTag');
        $('.tabular ').find('tr').children('th').removeAttr('style');
        if (viewType == 'list') {
            ChangeUrl('BoardReport', 'BoardReport.aspx?list=1');
            $('#BordReportGrid').css('display', 'none');
            $('#BordReportList').css('display', 'block');
            $('#loadboard').css('display', 'none');
            $('.list-1').removeClass('list-1-select');
            $('.list-1').addClass('list-1-select');
            $('.list').removeClass('grid-view');


        }
        else if (viewType == 'grid') {
            ChangeUrl('BoardReport', 'BoardReport.aspx');
            $('#BordReportGrid').css('display', 'block');
            $('#BordReportList').css('display', 'none');
            $('#loadboard').css('display', 'block');
            $('.list').removeClass('grid-view');
            $('.list').addClass('grid-view');
            $('.list-1').removeClass('list-1-select');
            var rowCount = $('#BordReportGrid .board-task-red').length;
            if (rowCount == TotalBoardList.length) {
                $('#loadboard').css('display', 'none');
            }
        }
    });

    $('.search-icon').click(function () {
        $("#DeletedSelectedForcast").css('display', 'none');
        $(".forecast_yellow").css('display', 'none');
        $("#displayselected").parent().css('display', 'none');
        $("#DeleteSelected").css('display', 'none');
        var searchText = $("#srch").val();
        $('.search-cntnr-drop-down').css('display', 'none');
        if (currentText1 == "") {
            currentText1 = 'B';
        }
        if (searchText != "") {
            TotalBoardList = "";
            flag = 19;
            listIndex = 0;
            addvaluetomultiplylist = 1;

            $.ajax({
                type: 'POST',
                url: 'DataTracker.svc/GetSeachboards',
                data: "{\"sort\":\"" + currentText1 + "\",\"seartext\":\"" + searchText + "\"}",
                contentType: "application/json; charset=utf-8",
                datatype: 'json',

                success: function (data) {
                    $('#BordReportGrid').empty();
                    $('.tabular').empty();
                    TotalBoardList = data.d;
                    if (data.d.length > 0) {
                        var fullvalue = "";
                        $('#loadboard').css('display', 'block');
                        if ($('#BordReportGrid').css('display') == 'none') {

                            $('#loadboard').css('display', 'none');
                        }

                        $('#BordReportGrid').empty();
                        var table = $('.tabular').DataTable();
                        table.destroy();
                        $('.tabular').empty();

                        if (data.d.length < 20) {
                            $('#loadboard').css('display', 'none');
                            for (var i = 0; i < data.d.length; i++) {

                                if (parseInt(TotalBoardList[i].EffortsToday) > 0) {
                                    if (TotalBoardList[i].Delay > 0) {
                                        $('#BordReportGrid').append("<div id=" + TotalBoardList[i].BoardID + " class='col-md-3 col-sm-6 col-xs-12 board-task-red'><div class='shadow-div'><h3 style='background:#2f77a5'>" + TotalBoardList[i].BoardName + "<div class='hover-frame'><div class='hoverdiv' myhovertag='board'><a target='_blank' href='" + DomainURL + "/board.aspx?" + TotalBoardList[i].BoardID + "'><img class='a' src='Styles/BoardReport/images/board-hiden-hover.png'/><label>Board</label></a></div> <div class='hoverdiv' myhovertag='Summary'><a target='_blank' href='" + DomainURL + "/boardSummeryReport.aspx?BID?=" + TotalBoardList[i].BoardID + "'><label class='pull-right'>Summary</label><img src='Styles/BoardReport/images/summary-hiden-hover.png' class='pull-right'/></a></div></div></h3><ul><li><img src='Styles/BoardReport/images/team_grey.png' title='Team Name'><span>" + TotalBoardList[i].TeamName + "</span></li><li><img src='Styles/BoardReport/images/owner_grey.png' title='Board Owner'><span>" + TotalBoardList[i].OwnerName + "</span></li><li><img src='Styles/BoardReport/images/efforts_today.png' title='Efforts'><span>Today's Efforts</span><label style='background: #44bf53' id='BID_" + TotalBoardList[i].BoardID + "'>" + TotalBoardList[i].EffortsToday + "</label></li><li><img src='Styles/BoardReport/images/date.png' title='Create Date'><span>" + TotalBoardList[i].DueDate + "</span></li><li style='padding:0px;'><div id='progressbar" + TotalBoardList[i].BoardID + "'><span id='check' class='pblabel" + TotalBoardList[i].BoardID + " pblabel'></span><img class='Clsdelay' src='Images/delay.png' title='Delay " + TotalBoardList[i].Delay + " hrs' alt=''/></div></li><li style='padding:0px;'><div id='progressbar2" + TotalBoardList[i].BoardID + "'><span id='check1' class='pblabel2" + TotalBoardList[i].BoardID + " pblabel1'></span></div></li></ul><div class='clearfix'></div><div id='bar1' class='barfiller'><div class='tipWrap'><span class='tip'></span></div><span class='fill' data-percentage='30'></span></div></div></div> ");
                                    }
                                    else { $('#BordReportGrid').append("<div id=" + TotalBoardList[i].BoardID + " class='col-md-3 col-sm-6 col-xs-12 board-task-red'><div class='shadow-div'><h3 style='background:#2f77a5'>" + TotalBoardList[i].BoardName + "<div class='hover-frame'><div class='hoverdiv' myhovertag='board'><a target='_blank' href='" + DomainURL + "/board.aspx?" + TotalBoardList[i].BoardID + "'><img class='a' src='Styles/BoardReport/images/board-hiden-hover.png'/><label>Board</label></a></div> <div class='hoverdiv' myhovertag='Summary'><a target='_blank' href='" + DomainURL + "/boardSummeryReport.aspx?BID?=" + TotalBoardList[i].BoardID + "'><label class='pull-right'>Summary</label><img src='Styles/BoardReport/images/summary-hiden-hover.png' class='pull-right'/></a></div></div></h3><ul><li><img src='Styles/BoardReport/images/team_grey.png' title='Team Name'><span>" + TotalBoardList[i].TeamName + "</span></li><li><img src='Styles/BoardReport/images/owner_grey.png' title='Board Owner'><span>" + TotalBoardList[i].OwnerName + "</span></li><li><img src='Styles/BoardReport/images/efforts_today.png' title='Efforts'><span>Today's Efforts</span><label style='background: #44bf53' id='BID_" + TotalBoardList[i].BoardID + "'>" + TotalBoardList[i].EffortsToday + "</label></li><li><img src='Styles/BoardReport/images/date.png' title='Create Date'><span>" + TotalBoardList[i].DueDate + "</span></li><li style='padding:0px;'><div id='progressbar" + TotalBoardList[i].BoardID + "'><span id='check' class='pblabel" + TotalBoardList[i].BoardID + " pblabel'></span></div></li><li style='padding:0px;'><div id='progressbar2" + TotalBoardList[i].BoardID + "'><span id='check1' class='pblabel2" + TotalBoardList[i].BoardID + " pblabel1'></span></div></li></ul><div class='clearfix'></div><div id='bar1' class='barfiller'><div class='tipWrap'><span class='tip'></span></div><span class='fill' data-percentage='30'></span></div></div></div> "); }

                                }
                                else {
                                    if (TotalBoardList[i].Delay > 0) {
                                        $('#BordReportGrid').append("<div id=" + TotalBoardList[i].BoardID + " class='col-md-3 col-sm-6 col-xs-12 board-task-red'><div class='shadow-div'><h3 style='background:#2f77a5'>" + TotalBoardList[i].BoardName + "<div class='hover-frame'><div class='hoverdiv' myhovertag='board'><a target='_blank' href='" + DomainURL + "/board.aspx?" + TotalBoardList[i].BoardID + "'><img class='a' src='Styles/BoardReport/images/board-hiden-hover.png'/><label>Board</label></a></div> <div class='hoverdiv' myhovertag='Summary'><a target='_blank' href='" + DomainURL + "/boardSummeryReport.aspx?BID?=" + TotalBoardList[i].BoardID + "'><label class='pull-right'>Summary</label><img src='Styles/BoardReport/images/summary-hiden-hover.png' class='pull-right'/></a></div></div></h3><ul><li><img src='Styles/BoardReport/images/team_grey.png' title='Team Name'><span>" + TotalBoardList[i].TeamName + "</span></li><li><img src='Styles/BoardReport/images/owner_grey.png' title='Board Owner'><span>" + TotalBoardList[i].OwnerName + "</span></li><li><img src='Styles/BoardReport/images/efforts_today.png' title='Efforts'><span>Today's Efforts</span><label id='BID_" + TotalBoardList[i].BoardID + "'>" + TotalBoardList[i].EffortsToday + "</label></li><li><img src='Styles/BoardReport/images/date.png' title='Create Date'><span>" + TotalBoardList[i].DueDate + "</span></li><li style='padding:0px;'><div id='progressbar" + TotalBoardList[i].BoardID + "'><span id='check' class='pblabel" + TotalBoardList[i].BoardID + " pblabel'></span><img class='Clsdelay' src='Images/delay.png' title='Delay " + TotalBoardList[i].Delay + " hrs' alt=''/></div></li><li style='padding:0px;'><div id='progressbar2" + TotalBoardList[i].BoardID + "'><span id='check1' class='pblabel2" + TotalBoardList[i].BoardID + " pblabel1'></span></div></li></ul><div class='clearfix'></div><div id='bar1' class='barfiller'><div class='tipWrap'><span class='tip'></span></div><span class='fill' data-percentage='30'></span></div></div></div> ");
                                    }
                                    else { $('#BordReportGrid').append("<div id=" + TotalBoardList[i].BoardID + " class='col-md-3 col-sm-6 col-xs-12 board-task-red'><div class='shadow-div'><h3 style='background:#2f77a5'>" + TotalBoardList[i].BoardName + "<div class='hover-frame'><div class='hoverdiv' myhovertag='board'><a target='_blank' href='" + DomainURL + "/board.aspx?" + TotalBoardList[i].BoardID + "'><img class='a' src='Styles/BoardReport/images/board-hiden-hover.png'/><label>Board</label></a></div> <div class='hoverdiv' myhovertag='Summary'><a target='_blank' href='" + DomainURL + "/boardSummeryReport.aspx?BID?=" + TotalBoardList[i].BoardID + "'><label class='pull-right'>Summary</label><img src='Styles/BoardReport/images/summary-hiden-hover.png' class='pull-right'/></a></div></div></h3><ul><li><img src='Styles/BoardReport/images/team_grey.png' title='Team Name'><span>" + TotalBoardList[i].TeamName + "</span></li><li><img src='Styles/BoardReport/images/owner_grey.png' title='Board Owner'><span>" + TotalBoardList[i].OwnerName + "</span></li><li><img src='Styles/BoardReport/images/efforts_today.png' title='Efforts'><span>Today's Efforts</span><label id='BID_" + TotalBoardList[i].BoardID + "'>" + TotalBoardList[i].EffortsToday + "</label></li><li><img src='Styles/BoardReport/images/date.png' title='Create Date'><span>" + TotalBoardList[i].DueDate + "</span></li><li style='padding:0px;'><div id='progressbar" + TotalBoardList[i].BoardID + "'><span id='check' class='pblabel" + TotalBoardList[i].BoardID + " pblabel'></span></div></li><li style='padding:0px;'><div id='progressbar2" + TotalBoardList[i].BoardID + "'><span id='check1' class='pblabel2" + TotalBoardList[i].BoardID + " pblabel1'></span></div></li></ul><div class='clearfix'></div><div id='bar1' class='barfiller'><div class='tipWrap'><span class='tip'></span></div><span class='fill' data-percentage='30'></span></div></div></div> "); }

                                }
                                //progress for board percentage
                                fullvalue = parseInt(TotalBoardList[i].TaskCompletePercent);
                                $('.pblabel' + TotalBoardList[i].BoardID).text(fullvalue + "%");
                                var progressbar = $('#progressbar' + TotalBoardList[i].BoardID).progressbar({
                                    value: fullvalue,
                                    change: function (event, ui) {
                                        var newVal = $(this).progressbar('option', 'value');
                                        var label = $('.pblabel' + TotalBoardList[i].BoardID, this).text(newVal + '%');
                                    }
                                });
                                var label = progressbar.find('.pblabel' + TotalBoardList[i].BoardID).clone().width(progressbar.width());
                                progressbar.find('.ui-progressbar-value').append(label);
                                $('#progressbar' + TotalBoardList[i].BoardID).css('background', '#f5f5f5');


                                if (fullvalue != 0) {
                                    $('#progressbar' + TotalBoardList[i].BoardID + ' .ui-widget-header').attr('style', 'background-color:' + TotalBoardList[i].boardcolor + ' !important; width:' + fullvalue + '%;');
                                }

                                listIndex = i + 1;


                                //progress for estimated hour percentage
                                fullvalue = parseInt(TotalBoardList[i].consumeHourPercentage);
                                consumedhour = TotalBoardList[i].consumehours + " hr " + "/ " + TotalBoardList[i].EstimatedHours + " hr ";
                                $('.pblabel2' + TotalBoardList[i].BoardID).text(consumedhour);
                                var progressbar = $('#progressbar2' + TotalBoardList[i].BoardID).progressbar({
                                    value: fullvalue,
                                    change: function (event, ui) {
                                        var newVal = $(this).progressbar('option', 'value');
                                        var label = $('.pblabel2' + TotalBoardList[i].BoardID, this).text(newVal + '%');
                                    }
                                });

                                var label = progressbar.find('.pblabel2' + TotalBoardList[i].BoardID).clone().width(progressbar.width());
                                progressbar.find('.ui-progressbar-value').append(label);
                                $('#progressbar2' + TotalBoardList[i].BoardID).css('background', '#ededed');

                                if (fullvalue != 0) {
                                    $('#progressbar2' + TotalBoardList[i].BoardID + ' .ui-widget-header').attr('style', 'background-color:#b2b2b2 !important; width:' + fullvalue + '%;');
                                }
                                $('#progressbar2' + TotalBoardList[i].BoardID + ' span').attr('style', 'padding:5px; background:#8f8f8f;margin-left: 0px;color: #fff;width: 100%;');
                                $('#progressbar2' + TotalBoardList[i].BoardID + ' div span').css('background', '#b2b2b2');
                            }
                        }
                        else {
                            for (var i = 0; i <= flag; i++) {

                                if (parseInt(TotalBoardList[i].EffortsToday) > 0) {
                                    if (TotalBoardList[i].Delay > 0) {
                                        $('#BordReportGrid').append("<div id=" + TotalBoardList[i].BoardID + " class='col-md-3 col-sm-6 col-xs-12 board-task-red'><div class='shadow-div'><h3 style='background:#2f77a5'>" + TotalBoardList[i].BoardName + "<div class='hover-frame'><div class='hoverdiv' myhovertag='board'><a target='_blank' href='" + DomainURL + "/board.aspx?" + TotalBoardList[i].BoardID + "'><img class='a' src='Styles/BoardReport/images/board-hiden-hover.png'/><label>Board</label></a></div> <div class='hoverdiv' myhovertag='Summary'><a target='_blank' href='" + DomainURL + "/boardSummeryReport.aspx?BID?=" + TotalBoardList[i].BoardID + "'><label class='pull-right'>Summary</label><img src='Styles/BoardReport/images/summary-hiden-hover.png' class='pull-right'/></a></div></div></h3><ul><li><img src='Styles/BoardReport/images/team_grey.png' title='Team Name'><span>" + TotalBoardList[i].TeamName + "</span></li><li><img src='Styles/BoardReport/images/owner_grey.png' title='Board Owner'><span>" + TotalBoardList[i].OwnerName + "</span></li><li><img src='Styles/BoardReport/images/efforts_today.png' title='Efforts'><span>Today's Efforts</span><label style='background: #44bf53' id='BID_" + TotalBoardList[i].BoardID + "'>" + TotalBoardList[i].EffortsToday + "</label></li><li><img src='Styles/BoardReport/images/date.png' title='Create Date'><span>" + TotalBoardList[i].DueDate + "</span></li><li style='padding:0px;'><div id='progressbar" + TotalBoardList[i].BoardID + "'><span id='check' class='pblabel" + TotalBoardList[i].BoardID + " pblabel'></span><img class='Clsdelay' src='Images/delay.png' title='Delay " + TotalBoardList[i].Delay + " hrs' alt=''/></div></li><li style='padding:0px;'><div id='progressbar2" + TotalBoardList[i].BoardID + "'><span id='check1' class='pblabel2" + TotalBoardList[i].BoardID + " pblabel1'></span></div></li></ul><div class='clearfix'></div><div id='bar1' class='barfiller'><div class='tipWrap'><span class='tip'></span></div><span class='fill' data-percentage='30'></span></div></div></div> ");
                                    }
                                    else { $('#BordReportGrid').append("<div id=" + TotalBoardList[i].BoardID + " class='col-md-3 col-sm-6 col-xs-12 board-task-red'><div class='shadow-div'><h3 style='background:#2f77a5'>" + TotalBoardList[i].BoardName + "<div class='hover-frame'><div class='hoverdiv' myhovertag='board'><a target='_blank' href='" + DomainURL + "/board.aspx?" + TotalBoardList[i].BoardID + "'><img class='a' src='Styles/BoardReport/images/board-hiden-hover.png'/><label>Board</label></a></div> <div class='hoverdiv' myhovertag='Summary'><a target='_blank' href='" + DomainURL + "/boardSummeryReport.aspx?BID?=" + TotalBoardList[i].BoardID + "'><label class='pull-right'>Summary</label><img src='Styles/BoardReport/images/summary-hiden-hover.png' class='pull-right'/></a></div></div></h3><ul><li><img src='Styles/BoardReport/images/team_grey.png' title='Team Name'><span>" + TotalBoardList[i].TeamName + "</span></li><li><img src='Styles/BoardReport/images/owner_grey.png' title='Board Owner'><span>" + TotalBoardList[i].OwnerName + "</span></li><li><img src='Styles/BoardReport/images/efforts_today.png' title='Efforts'><span>Today's Efforts</span><label style='background: #44bf53' id='BID_" + TotalBoardList[i].BoardID + "'>" + TotalBoardList[i].EffortsToday + "</label></li><li><img src='Styles/BoardReport/images/date.png' title='Create Date'><span>" + TotalBoardList[i].DueDate + "</span></li><li style='padding:0px;'><div id='progressbar" + TotalBoardList[i].BoardID + "'><span id='check' class='pblabel" + TotalBoardList[i].BoardID + " pblabel'></span></div></li><li style='padding:0px;'><div id='progressbar2" + TotalBoardList[i].BoardID + "'><span id='check1' class='pblabel2" + TotalBoardList[i].BoardID + " pblabel1'></span></div></li></ul><div class='clearfix'></div><div id='bar1' class='barfiller'><div class='tipWrap'><span class='tip'></span></div><span class='fill' data-percentage='30'></span></div></div></div> "); }

                                }
                                else {
                                    if (TotalBoardList[i].Delay > 0) {
                                        $('#BordReportGrid').append("<div id=" + TotalBoardList[i].BoardID + " class='col-md-3 col-sm-6 col-xs-12 board-task-red'><div class='shadow-div'><h3 style='background:#2f77a5'>" + TotalBoardList[i].BoardName + "<div class='hover-frame'><div class='hoverdiv' myhovertag='board'><a target='_blank' href='" + DomainURL + "/board.aspx?" + TotalBoardList[i].BoardID + "'><img class='a' src='Styles/BoardReport/images/board-hiden-hover.png'/><label>Board</label></a></div> <div class='hoverdiv' myhovertag='Summary'><a target='_blank' href='" + DomainURL + "/boardSummeryReport.aspx?BID?=" + TotalBoardList[i].BoardID + "'><label class='pull-right'>Summary</label><img src='Styles/BoardReport/images/summary-hiden-hover.png' class='pull-right'/></a></div></div></h3><ul><li><img src='Styles/BoardReport/images/team_grey.png' title='Team Name'><span>" + TotalBoardList[i].TeamName + "</span></li><li><img src='Styles/BoardReport/images/owner_grey.png' title='Board Owner'><span>" + TotalBoardList[i].OwnerName + "</span></li><li><img src='Styles/BoardReport/images/efforts_today.png' title='Efforts'><span>Today's Efforts</span><label id='BID_" + TotalBoardList[i].BoardID + "'>" + TotalBoardList[i].EffortsToday + "</label></li><li><img src='Styles/BoardReport/images/date.png' title='Create Date'><span>" + TotalBoardList[i].DueDate + "</span></li><li style='padding:0px;'><div id='progressbar" + TotalBoardList[i].BoardID + "'><span id='check' class='pblabel" + TotalBoardList[i].BoardID + " pblabel'></span><img class='Clsdelay' src='Images/delay.png' title='Delay " + TotalBoardList[i].Delay + " hrs' alt=''/></div></li><li style='padding:0px;'><div id='progressbar2" + TotalBoardList[i].BoardID + "'><span id='check1' class='pblabel2" + TotalBoardList[i].BoardID + " pblabel1'></span></div></li></ul><div class='clearfix'></div><div id='bar1' class='barfiller'><div class='tipWrap'><span class='tip'></span></div><span class='fill' data-percentage='30'></span></div></div></div> ");
                                    }
                                    else { $('#BordReportGrid').append("<div id=" + TotalBoardList[i].BoardID + " class='col-md-3 col-sm-6 col-xs-12 board-task-red'><div class='shadow-div'><h3 style='background:#2f77a5'>" + TotalBoardList[i].BoardName + "<div class='hover-frame'><div class='hoverdiv' myhovertag='board'><a target='_blank' href='" + DomainURL + "/board.aspx?" + TotalBoardList[i].BoardID + "'><img class='a' src='Styles/BoardReport/images/board-hiden-hover.png'/><label>Board</label></a></div> <div class='hoverdiv' myhovertag='Summary'><a target='_blank' href='" + DomainURL + "/boardSummeryReport.aspx?BID?=" + TotalBoardList[i].BoardID + "'><label class='pull-right'>Summary</label><img src='Styles/BoardReport/images/summary-hiden-hover.png' class='pull-right'/></a></div></div></h3><ul><li><img src='Styles/BoardReport/images/team_grey.png' title='Team Name'><span>" + TotalBoardList[i].TeamName + "</span></li><li><img src='Styles/BoardReport/images/owner_grey.png' title='Board Owner'><span>" + TotalBoardList[i].OwnerName + "</span></li><li><img src='Styles/BoardReport/images/efforts_today.png' title='Efforts'><span>Today's Efforts</span><label id='BID_" + TotalBoardList[i].BoardID + "'>" + TotalBoardList[i].EffortsToday + "</label></li><li><img src='Styles/BoardReport/images/date.png' title='Create Date'><span>" + TotalBoardList[i].DueDate + "</span></li><li style='padding:0px;'><div id='progressbar" + TotalBoardList[i].BoardID + "'><span id='check' class='pblabel" + TotalBoardList[i].BoardID + " pblabel'></span></div></li><li style='padding:0px;'><div id='progressbar2" + TotalBoardList[i].BoardID + "'><span id='check1' class='pblabel2" + TotalBoardList[i].BoardID + " pblabel1'></span></div></li></ul><div class='clearfix'></div><div id='bar1' class='barfiller'><div class='tipWrap'><span class='tip'></span></div><span class='fill' data-percentage='30'></span></div></div></div> "); }

                                }
                                //progress for board percentage
                                fullvalue = parseInt(TotalBoardList[i].TaskCompletePercent);
                                $('.pblabel' + TotalBoardList[i].BoardID).text(fullvalue + "%");
                                var progressbar = $('#progressbar' + TotalBoardList[i].BoardID).progressbar({
                                    value: fullvalue,
                                    change: function (event, ui) {
                                        var newVal = $(this).progressbar('option', 'value');
                                        var label = $('.pblabel' + TotalBoardList[i].BoardID, this).text(newVal + '%');
                                    }
                                });
                                var label = progressbar.find('.pblabel' + TotalBoardList[i].BoardID).clone().width(progressbar.width());
                                progressbar.find('.ui-progressbar-value').append(label);
                                $('#progressbar' + TotalBoardList[i].BoardID).css('background', '#f5f5f5');


                                if (fullvalue != 0) {
                                    $('#progressbar' + TotalBoardList[i].BoardID + ' .ui-widget-header').attr('style', 'background-color:' + TotalBoardList[i].boardcolor + ' !important; width:' + fullvalue + '%;');
                                }

                                listIndex = i + 1;


                                //progress for estimated hour percentage
                                fullvalue = parseInt(TotalBoardList[i].consumeHourPercentage);
                                consumedhour = TotalBoardList[i].consumehours + " hr " + "/ " + TotalBoardList[i].EstimatedHours + " hr ";
                                $('.pblabel2' + TotalBoardList[i].BoardID).text(consumedhour);
                                var progressbar = $('#progressbar2' + TotalBoardList[i].BoardID).progressbar({
                                    value: fullvalue,
                                    change: function (event, ui) {
                                        var newVal = $(this).progressbar('option', 'value');
                                        var label = $('.pblabel2' + TotalBoardList[i].BoardID, this).text(newVal + '%');
                                    }
                                });

                                var label = progressbar.find('.pblabel2' + TotalBoardList[i].BoardID).clone().width(progressbar.width());
                                progressbar.find('.ui-progressbar-value').append(label);

                                $('#progressbar2' + TotalBoardList[i].BoardID).css('background', '#ededed');
                                if (fullvalue != 0) {
                                    $('#progressbar2' + TotalBoardList[i].BoardID + ' .ui-widget-header').attr('style', 'background-color:#b2b2b2 !important; width:' + fullvalue + '%;');
                                }
                                $('#progressbar2' + TotalBoardList[i].BoardID + ' span').attr('style', 'padding:5px; background:#8f8f8f;margin-left: 0px;color: #fff;width: 100%;');
                                $('#progressbar2' + TotalBoardList[i].BoardID + ' div span').css('background', '#b2b2b2');
                            }

                        }
                        $('.hover-frame').slideUp();
                        $(".tabular").append('<thead><tr class="blue noExl"><th><img src="Styles/BoardReport/images/name-wh.png">Name</th><th><img src="Styles/BoardReport/images/team-wh.png">Team</th><th><img src="Styles/BoardReport/images/owner-wh.png">Owner</th><th><img src="Styles/BoardReport/images/start_date-wh.png">Start Date</th><th><img src="Styles/BoardReport/images/end_date-wh.png">End Date</th><th><img src="Styles/BoardReport/images/efforts-wh.png">Today' + "'" + 's Efforts</th><th><img src="Styles/BoardReport/images/hours-wh.png">Hours Consumed</th><th><img src="Styles/BoardReport/images/board-wh.png">Board Progress</th><th><img src="Images/action.png">Action</th></tr></thead><tbody>');
                        if (usertypeid == "12") {
                            $.each(data.d, function (index, item) {
                                filtersAllBoardid.push(item.BoardID);
                                if (parseInt(item.EffortsToday) > 0) {
                                    $('.tabular tbody').append("<tr><td style='border-left:solid 10px " + item.boardcolor + "'><a target='_blank'href=" + DomainURL + "/BoardSummeryReport.aspx?BID=" + item.BoardID + ">" + item.BoardName + "</a><span class='board-sumary-hover-cont' style='background:" + item.boardcolor + "'><a target='_blank' href='" + DomainURL + "/board.aspx?" + item.BoardID + "'><img src='Styles/BoardReport/images/board.png'></a><a target='_blank' href='" + DomainURL + "/boardSummeryReport.aspx?BID?=" + item.BoardID + "'><img src='Styles/BoardReport/images/summary.png'></a></span></td><td>" + item.TeamName + "</td><td>" + item.OwnerName + "</td><td>" + item.CreatedDate + "</td><td>" + item.DueDate + "</td><td><label style='background: #44bf53' id='BID_" + item.BoardID + "'>" + item.EffortsToday + "</label></td><td style='padding:0px;'><div id='progressbar3" + item.BoardID + "'><span id='check' class='pblabel3" + item.BoardID + " pblabel'></span></div></td><td style='padding:0px;'><div id='progressbar1" + item.BoardID + "'><span id='check' class='pblabel1" + item.BoardID + " pblabel'></span></div></td><td><img src='Images/icon1.png' class='archiveandcomplete' title='Archive Board' ArchiveTag='archive'/><img src='Images/icon2.png' title='Complete Board' ArchiveTag='complete' class='archiveandcomplete'/></td></tr>");
                                }
                                else {
                                    $('.tabular tbody').append("<tr><td style='border-left:solid 10px " + item.boardcolor + "'><a target='_blank'href=" + DomainURL + "/BoardSummeryReport.aspx?BID=" + item.BoardID + ">" + item.BoardName + "</a><span class='board-sumary-hover-cont' style='background:" + item.boardcolor + "'><a target='_blank' href='" + DomainURL + "/board.aspx?" + item.BoardID + "'><img src='Styles/BoardReport/images/board.png'></a><a target='_blank' href='" + DomainURL + "/boardSummeryReport.aspx?BID?=" + item.BoardID + "'><img src='Styles/BoardReport/images/summary.png'></a></span></td><td>" + item.TeamName + "</td><td>" + item.OwnerName + "</td><td>" + item.CreatedDate + "</td><td>" + item.DueDate + "</td><td><label id='BID_" + item.BoardID + "'>" + item.EffortsToday + "</label></td><td style='padding:0px;'><div id='progressbar3" + item.BoardID + "'><span id='check' class='pblabel3" + item.BoardID + " pblabel'></span></div></td><td style='padding:0px;'><div id='progressbar1" + item.BoardID + "'><span id='check' class='pblabel1" + item.BoardID + " pblabel'></span></div></td><td><img src='Images/icon1.png' title='Archive Board' class='archiveandcomplete' ArchiveTag='archive'/><img src='Images/icon2.png' class='archiveandcomplete' title='Complete Board' ArchiveTag='complete'/></td></tr>");
                                }

                                //progress for board percentage
                                fullvalue = parseInt(item.TaskCompletePercent);
                                $('.pblabel1' + item.BoardID).text(fullvalue + "%");
                                var progressbar = $('#progressbar1' + item.BoardID).progressbar({
                                    value: fullvalue,
                                    change: function (event, ui) {
                                        var newVal = $(this).progressbar('option', 'value');
                                        var label = $('.pblabel1' + item.BoardID, this).text(newVal + '%');
                                    }
                                });


                                if (fullvalue != 0) {
                                    var label = progressbar.find('.pblabel1' + item.BoardID).width(progressbar.width());
                                    progressbar.find('.ui-progressbar-value').append(label);
                                    $('#progressbar1' + item.BoardID + ' .ui-widget-header').attr('style', 'background-color:' + item.boardcolor + ' !important; width:' + parseInt(fullvalue) + '%;');
                                }
                                else {
                                    var label = progressbar.find('.pblabel1' + item.BoardID).clone().width(progressbar.width());
                                    progressbar.find('.ui-progressbar-value').append(label);
                                }

                                $('.pblabel1' + item.BoardID).css('width', '');

                                //progress for estimated hour percentage
                                fullvalue = parseInt(item.consumeHourPercentage);
                                consumedhour = item.consumehours + " hr " + "/ " + item.EstimatedHours + " hr ";
                                $('.pblabel3' + item.BoardID).text(consumedhour);
                                var progressbar = $('#progressbar3' + item.BoardID).progressbar({
                                    value: fullvalue,
                                    change: function (event, ui) {
                                        var newVal = $(this).progressbar('option', 'value');
                                        var label = $('.pblabel3' + item.BoardID, this).text(newVal + '%');
                                    }
                                });

                                if (fullvalue != 0) {
                                    var label = progressbar.find('.pblabel3' + item.BoardID).width(progressbar.width());
                                    progressbar.find('.ui-progressbar-value').append(label);
                                    $('#progressbar3' + item.BoardID + ' .ui-widget-header').attr('style', 'background-color:#8f8f8f !important; width:' + parseInt(fullvalue) + '%;');
                                }
                                else {
                                    var label = progressbar.find('.pblabel3' + item.BoardID).clone().width(progressbar.width());
                                    progressbar.find('.ui-progressbar-value').append(label);

                                }

                                if (parseInt(fullvalue + 2) < 50) {
                                    $('#progressbar3' + item.BoardID + ' div').addClass('fitdivspan');
                                    $('#progressbar3' + item.BoardID + ' span').css('color', 'white');
                                }

                                if (fullvalue == 0) {
                                    $('#progressbar3' + item.BoardID + ' .ui-widget-header   span').remove();
                                }
                                $('.pblabel3' + item.BoardID).css('width', '')
                                $('#progressbar3' + item.BoardID + ' div span').css('background', '#b2b2b2');
                                i++;

                            });
                        }
                        else {
                            $.each(data.d, function (index, item) {
                                filtersAllBoardid.push(item.BoardID);
                                if (item.flag == 1) {
                                    if (parseInt(item.EffortsToday) > 0) {
                                        $('.tabular tbody').append("<tr><td style='border-left:solid 10px " + item.boardcolor + "'><a target='_blank'href=" + DomainURL + "/BoardSummeryReport.aspx?BID=" + item.BoardID + ">" + item.BoardName + "</a><span class='board-sumary-hover-cont' style='background:" + item.boardcolor + "'><a target='_blank' href='" + DomainURL + "/board.aspx?" + item.BoardID + "'><img src='Styles/BoardReport/images/board.png'></a><a target='_blank' href='" + DomainURL + "/boardSummeryReport.aspx?BID?=" + item.BoardID + "'><img src='Styles/BoardReport/images/summary.png'></a></span></td><td>" + item.TeamName + "</td><td>" + item.OwnerName + "</td><td>" + item.CreatedDate + "</td><td>" + item.DueDate + "</td><td><label style='background: #44bf53' id='BID_" + item.BoardID + "'>" + item.EffortsToday + "</label></td><td style='padding:0px;'><div id='progressbar3" + item.BoardID + "'><span id='check' class='pblabel3" + item.BoardID + " pblabel'></span></div></td><td style='padding:0px;'><div id='progressbar1" + item.BoardID + "'><span id='check' class='pblabel1" + item.BoardID + " pblabel'></span></div></td><td><img src='Images/icon1.png' class='archiveandcomplete' title='Archive Board' ArchiveTag='archive'/><img src='Images/icon2.png' title='Complete Board' ArchiveTag='complete' class='archiveandcomplete'/></td></tr>");
                                    }
                                    else {
                                        $('.tabular tbody').append("<tr><td style='border-left:solid 10px " + item.boardcolor + "'><a target='_blank'href=" + DomainURL + "/BoardSummeryReport.aspx?BID=" + item.BoardID + ">" + item.BoardName + "</a><span class='board-sumary-hover-cont' style='background:" + item.boardcolor + "'><a target='_blank' href='" + DomainURL + "/board.aspx?" + item.BoardID + "'><img src='Styles/BoardReport/images/board.png'></a><a target='_blank' href='" + DomainURL + "/boardSummeryReport.aspx?BID?=" + item.BoardID + "'><img src='Styles/BoardReport/images/summary.png'></a></span></td><td>" + item.TeamName + "</td><td>" + item.OwnerName + "</td><td>" + item.CreatedDate + "</td><td>" + item.DueDate + "</td><td><label id='BID_" + item.BoardID + "'>" + item.EffortsToday + "</label></td><td style='padding:0px;'><div id='progressbar3" + item.BoardID + "'><span id='check' class='pblabel3" + item.BoardID + " pblabel'></span></div></td><td style='padding:0px;'><div id='progressbar1" + item.BoardID + "'><span id='check' class='pblabel1" + item.BoardID + " pblabel'></span></div></td><td><img src='Images/icon1.png' title='Archive Board' class='archiveandcomplete' ArchiveTag='archive'/><img src='Images/icon2.png' class='archiveandcomplete' title='Complete Board' ArchiveTag='complete'/></td></tr>");
                                    }

                                }
                                else {
                                    if (parseInt(item.EffortsToday) > 0) {
                                        $('.tabular tbody').append("<tr><td style='border-left:solid 10px " + item.boardcolor + "'><a target='_blank'href=" + DomainURL + "/BoardSummeryReport.aspx?BID=" + item.BoardID + ">" + item.BoardName + "</a><span class='board-sumary-hover-cont' style='background:" + item.boardcolor + "'><a target='_blank' href='" + DomainURL + "/board.aspx?" + item.BoardID + "'><img src='Styles/BoardReport/images/board.png'></a><a target='_blank' href='" + DomainURL + "/boardSummeryReport.aspx?BID?=" + item.BoardID + "'><img src='Styles/BoardReport/images/summary.png'></a></span></td><td>" + item.TeamName + "</td><td>" + item.OwnerName + "</td><td>" + item.CreatedDate + "</td><td>" + item.DueDate + "</td><td><label style='background: #44bf53' id='BID_" + item.BoardID + "'>" + item.EffortsToday + "</label></td><td style='padding:0px;'><div id='progressbar3" + item.BoardID + "'><span id='check' class='pblabel3" + item.BoardID + " pblabel'></span></div></td><td style='padding:0px;'><div id='progressbar1" + item.BoardID + "'><span id='check' class='pblabel1" + item.BoardID + " pblabel'></span></div></td><td></td></tr>");
                                    }
                                    else {
                                        $('.tabular tbody').append("<tr><td style='border-left:solid 10px " + item.boardcolor + "'><a target='_blank'href=" + DomainURL + "/BoardSummeryReport.aspx?BID=" + item.BoardID + ">" + item.BoardName + "</a><span class='board-sumary-hover-cont' style='background:" + item.boardcolor + "'><a target='_blank' href='" + DomainURL + "/board.aspx?" + item.BoardID + "'><img src='Styles/BoardReport/images/board.png'></a><a target='_blank' href='" + DomainURL + "/boardSummeryReport.aspx?BID?=" + item.BoardID + "'><img src='Styles/BoardReport/images/summary.png'></a></span></td><td>" + item.TeamName + "</td><td>" + item.OwnerName + "</td><td>" + item.CreatedDate + "</td><td>" + item.DueDate + "</td><td><label id='BID_" + item.BoardID + "'>" + item.EffortsToday + "</label></td><td style='padding:0px;'><div id='progressbar3" + item.BoardID + "'><span id='check' class='pblabel3" + item.BoardID + " pblabel'></span></div></td><td style='padding:0px;'><div id='progressbar1" + item.BoardID + "'><span id='check' class='pblabel1" + item.BoardID + " pblabel'></span></div></td><td></td></tr>");
                                    }

                                }

                                //progress for board percentage
                                fullvalue = parseInt(item.TaskCompletePercent);
                                $('.pblabel1' + item.BoardID).text(fullvalue + "%");
                                var progressbar = $('#progressbar1' + item.BoardID).progressbar({
                                    value: fullvalue,
                                    change: function (event, ui) {
                                        var newVal = $(this).progressbar('option', 'value');
                                        var label = $('.pblabel1' + item.BoardID, this).text(newVal + '%');
                                    }
                                });


                                if (fullvalue != 0) {
                                    var label = progressbar.find('.pblabel1' + item.BoardID).width(progressbar.width());
                                    progressbar.find('.ui-progressbar-value').append(label);
                                    $('#progressbar1' + item.BoardID + ' .ui-widget-header').attr('style', 'background-color:' + item.boardcolor + ' !important; width:' + parseInt(fullvalue) + '%;');
                                }
                                else {
                                    var label = progressbar.find('.pblabel1' + item.BoardID).clone().width(progressbar.width());
                                    progressbar.find('.ui-progressbar-value').append(label);
                                }

                                $('.pblabel1' + item.BoardID).css('width', '');

                                //progress for estimated hour percentage
                                fullvalue = parseInt(item.consumeHourPercentage);
                                consumedhour = item.consumehours + " hr " + "/ " + item.EstimatedHours + " hr ";
                                $('.pblabel3' + item.BoardID).text(consumedhour);
                                var progressbar = $('#progressbar3' + item.BoardID).progressbar({
                                    value: fullvalue,
                                    change: function (event, ui) {
                                        var newVal = $(this).progressbar('option', 'value');
                                        var label = $('.pblabel3' + item.BoardID, this).text(newVal + '%');
                                    }
                                });

                                if (fullvalue != 0) {
                                    var label = progressbar.find('.pblabel3' + item.BoardID).width(progressbar.width());
                                    progressbar.find('.ui-progressbar-value').append(label);
                                    $('#progressbar3' + item.BoardID + ' .ui-widget-header').attr('style', 'background-color:#8f8f8f !important; width:' + parseInt(fullvalue) + '%;');
                                }
                                else {
                                    var label = progressbar.find('.pblabel3' + item.BoardID).clone().width(progressbar.width());
                                    progressbar.find('.ui-progressbar-value').append(label);

                                }

                                if (parseInt(fullvalue + 2) < 50) {
                                    $('#progressbar3' + item.BoardID + ' div').addClass('fitdivspan');
                                    $('#progressbar3' + item.BoardID + ' span').css('color', 'white');
                                }

                                if (fullvalue == 0) {
                                    $('#progressbar3' + item.BoardID + ' .ui-widget-header   span').remove();
                                }
                                $('.pblabel3' + item.BoardID).css('width', '')
                                $('#progressbar3' + item.BoardID + ' div span').css('background', '#b2b2b2');
                                i++;

                            });


                        }

                        $(".tabular tbody").append("</tbody>");
                        BindGrid();

                    }
                    else {
                        $('#BordReportGrid').append("<h2>No Record Found</h2>");
                        $('#loadboard').css('display', 'none');
                    }
                },
                error: function (err) {

                }
            });

        }
        else {
            alert("Please type search text.");
        }

    });

    $('#loadboard').click(function () {

        var arrindex = 0;
        addvaluetomultiplylist = addvaluetomultiplylist + 1;

        flag = flag + 1;
        var multiplyboardlist = flag + 20;
        var fullvalue = "";
        for (var i = listIndex; i < multiplyboardlist; i++) {


            if (i < TotalBoardList.length) {
                if (parseInt(TotalBoardList[i].EffortsToday) > 0) {
                    if (TotalBoardList[i].Delay > 0) {
                        $('#BordReportGrid').append("<div id=" + TotalBoardList[i].BoardID + " class='col-md-3 col-sm-6 col-xs-12 board-task-red'><div class='shadow-div'><h3 style='background:#2f77a5'>" + TotalBoardList[i].BoardName + "<div class='hover-frame'><div class='hoverdiv' myhovertag='board'><a target='_blank' href='" + DomainURL + "/board.aspx?" + TotalBoardList[i].BoardID + "'><img class='a' src='Styles/BoardReport/images/board-hiden-hover.png'/><label>Board</label></a></div> <div class='hoverdiv' myhovertag='Summary'><a href='" + DomainURL + "/boardSummeryReport.aspx?BID?=" + TotalBoardList[i].BoardID + "'><label class='pull-right'>Summary</label><img src='Styles/BoardReport/images/summary-hiden-hover.png' class='pull-right'/></a></div></div></h3><ul><li><img src='Styles/BoardReport/images/team_grey.png' title='Team Name'><span>" + TotalBoardList[i].TeamName + "</span></li><li><img src='Styles/BoardReport/images/owner_grey.png' title='Board Owner'><span>" + TotalBoardList[i].OwnerName + "</span></li><li><img src='Styles/BoardReport/images/efforts_today.png' title='Efforts'><span>Today's Efforts</span><label style='background: #44bf53' id='BID_" + TotalBoardList[i].BoardID + "'>" + TotalBoardList[i].EffortsToday + "</label></li><li><img src='Styles/BoardReport/images/date.png' title='Create Date'><span>" + TotalBoardList[i].DueDate + "</span></li><li style='padding:0px;'><div id='progressbar" + TotalBoardList[i].BoardID + "'><span id='check' class='pblabel" + TotalBoardList[i].BoardID + " pblabel'></span><img class='Clsdelay' src='Images/delay.png' title='Delay " + TotalBoardList[i].Delay + " hrs' alt=''/></div></li><li style='padding:0px;'><div id='progressbar2" + TotalBoardList[i].BoardID + "'><span id='check1' class='pblabel2" + TotalBoardList[i].BoardID + " pblabel1'></span></div></li></ul><div class='clearfix'></div><div id='bar1' class='barfiller'><div class='tipWrap'><span class='tip'></span></div><span class='fill' data-percentage='30'></span></div></div></div> ");
                    }
                    else { $('#BordReportGrid').append("<div id=" + TotalBoardList[i].BoardID + " class='col-md-3 col-sm-6 col-xs-12 board-task-red'><div class='shadow-div'><h3 style='background:#2f77a5'>" + TotalBoardList[i].BoardName + "<div class='hover-frame'><div class='hoverdiv' myhovertag='board'><a target='_blank' href='" + DomainURL + "/board.aspx?" + TotalBoardList[i].BoardID + "'><img class='a' src='Styles/BoardReport/images/board-hiden-hover.png'/><label>Board</label></a></div> <div class='hoverdiv' myhovertag='Summary'><a href='" + DomainURL + "/boardSummeryReport.aspx?BID?=" + TotalBoardList[i].BoardID + "'><label class='pull-right'>Summary</label><img src='Styles/BoardReport/images/summary-hiden-hover.png' class='pull-right'/></a></div></div></h3><ul><li><img src='Styles/BoardReport/images/team_grey.png' title='Team Name'><span>" + TotalBoardList[i].TeamName + "</span></li><li><img src='Styles/BoardReport/images/owner_grey.png' title='Board Owner'><span>" + TotalBoardList[i].OwnerName + "</span></li><li><img src='Styles/BoardReport/images/efforts_today.png' title='Efforts'><span>Today's Efforts</span><label style='background: #44bf53' id='BID_" + TotalBoardList[i].BoardID + "'>" + TotalBoardList[i].EffortsToday + "</label></li><li><img src='Styles/BoardReport/images/date.png' title='Create Date'><span>" + TotalBoardList[i].DueDate + "</span></li><li style='padding:0px;'><div id='progressbar" + TotalBoardList[i].BoardID + "'><span id='check' class='pblabel" + TotalBoardList[i].BoardID + " pblabel'></span></div></li><li style='padding:0px;'><div id='progressbar2" + TotalBoardList[i].BoardID + "'><span id='check1' class='pblabel2" + TotalBoardList[i].BoardID + " pblabel1'></span></div></li></ul><div class='clearfix'></div><div id='bar1' class='barfiller'><div class='tipWrap'><span class='tip'></span></div><span class='fill' data-percentage='30'></span></div></div></div> "); }

                }
                else {
                    if (TotalBoardList[i].Delay > 0) {
                        $('#BordReportGrid').append("<div id=" + TotalBoardList[i].BoardID + " class='col-md-3 col-sm-6 col-xs-12 board-task-red'><div class='shadow-div'><h3 style='background:#2f77a5'>" + TotalBoardList[i].BoardName + "<div class='hover-frame'><div class='hoverdiv' myhovertag='board'><a target='_blank' href='" + DomainURL + "/board.aspx?" + TotalBoardList[i].BoardID + "'><img class='a' src='Styles/BoardReport/images/board-hiden-hover.png'/><label>Board</label></a></div> <div class='hoverdiv' myhovertag='Summary'><a href='" + DomainURL + "/boardSummeryReport.aspx?BID?=" + TotalBoardList[i].BoardID + "'><label class='pull-right'>Summary</label><img src='Styles/BoardReport/images/summary-hiden-hover.png' class='pull-right'/></a></div></div></h3><ul><li><img src='Styles/BoardReport/images/team_grey.png' title='Team Name'><span>" + TotalBoardList[i].TeamName + "</span></li><li><img src='Styles/BoardReport/images/owner_grey.png' title='Board Owner'><span>" + TotalBoardList[i].OwnerName + "</span></li><li><img src='Styles/BoardReport/images/efforts_today.png' title='Efforts'><span>Today's Efforts</span><label id='BID_" + TotalBoardList[i].BoardID + "'>" + TotalBoardList[i].EffortsToday + "</label></li><li><img src='Styles/BoardReport/images/date.png' title='Create Date'><span>" + TotalBoardList[i].DueDate + "</span></li><li style='padding:0px;'><div id='progressbar" + TotalBoardList[i].BoardID + "'><span id='check' class='pblabel" + TotalBoardList[i].BoardID + " pblabel'></span><img class='Clsdelay' src='Images/delay.png' title='Delay " + TotalBoardList[i].Delay + " hrs' alt=''/></div></li><li style='padding:0px;'><div id='progressbar2" + TotalBoardList[i].BoardID + "'><span id='check1' class='pblabel2" + TotalBoardList[i].BoardID + " pblabel1'></span></div></li></ul><div class='clearfix'></div><div id='bar1' class='barfiller'><div class='tipWrap'><span class='tip'></span></div><span class='fill' data-percentage='30'></span></div></div></div> ");
                    }
                    else { $('#BordReportGrid').append("<div id=" + TotalBoardList[i].BoardID + " class='col-md-3 col-sm-6 col-xs-12 board-task-red'><div class='shadow-div'><h3 style='background:#2f77a5'>" + TotalBoardList[i].BoardName + "<div class='hover-frame'><div class='hoverdiv' myhovertag='board'><a target='_blank' href='" + DomainURL + "/board.aspx?" + TotalBoardList[i].BoardID + "'><img class='a' src='Styles/BoardReport/images/board-hiden-hover.png'/><label>Board</label></a></div> <div class='hoverdiv' myhovertag='Summary'><a href='" + DomainURL + "/boardSummeryReport.aspx?BID?=" + TotalBoardList[i].BoardID + "'><label class='pull-right'>Summary</label><img src='Styles/BoardReport/images/summary-hiden-hover.png' class='pull-right'/></a></div></div></h3><ul><li><img src='Styles/BoardReport/images/team_grey.png' title='Team Name'><span>" + TotalBoardList[i].TeamName + "</span></li><li><img src='Styles/BoardReport/images/owner_grey.png' title='Board Owner'><span>" + TotalBoardList[i].OwnerName + "</span></li><li><img src='Styles/BoardReport/images/efforts_today.png' title='Efforts'><span>Today's Efforts</span><label id='BID_" + TotalBoardList[i].BoardID + "'>" + TotalBoardList[i].EffortsToday + "</label></li><li><img src='Styles/BoardReport/images/date.png' title='Create Date'><span>" + TotalBoardList[i].DueDate + "</span></li><li style='padding:0px;'><div id='progressbar" + TotalBoardList[i].BoardID + "'><span id='check' class='pblabel" + TotalBoardList[i].BoardID + " pblabel'></span></div></li><li style='padding:0px;'><div id='progressbar2" + TotalBoardList[i].BoardID + "'><span id='check1' class='pblabel2" + TotalBoardList[i].BoardID + " pblabel1'></span></div></li></ul><div class='clearfix'></div><div id='bar1' class='barfiller'><div class='tipWrap'><span class='tip'></span></div><span class='fill' data-percentage='30'></span></div></div></div> "); }

                }
                $('#loadboard').css('display', 'block');
            }
            else {
                $('#loadboard').css('display', 'none');
                return false;
            }

            //progress for board percentage
            fullvalue = parseInt(TotalBoardList[i].TaskCompletePercent);
            $('.pblabel' + TotalBoardList[i].BoardID).text(fullvalue + "%");
            var progressbar = $('#progressbar' + TotalBoardList[i].BoardID).progressbar({
                value: fullvalue,
                change: function (event, ui) {
                    var newVal = $(this).progressbar('option', 'value');
                    var label = $('.pblabel' + TotalBoardList[i].BoardID, this).text(newVal + '%');
                }
            });
            var label = progressbar.find('.pblabel' + TotalBoardList[i].BoardID).clone().width(progressbar.width());
            progressbar.find('.ui-progressbar-value').append(label);
            $('#progressbar' + TotalBoardList[i].BoardID).css('background', '#f5f5f5');

            if (fullvalue != 0) {
                $('#progressbar' + TotalBoardList[i].BoardID + ' .ui-widget-header').attr('style', 'background-color:' + TotalBoardList[i].boardcolor + ' !important; width:' + fullvalue + '%;');
            }

            listIndex = i + 1;


            //progress for estimated hour percentage
            fullvalue = parseInt(TotalBoardList[i].consumeHourPercentage);
            consumedhour = TotalBoardList[i].consumehours + " hr " + "/ " + TotalBoardList[i].EstimatedHours + " hr ";
            $('.pblabel2' + TotalBoardList[i].BoardID).text(consumedhour);
            var progressbar = $('#progressbar2' + TotalBoardList[i].BoardID).progressbar({
                value: fullvalue,
                change: function (event, ui) {
                    var newVal = $(this).progressbar('option', 'value');
                    var label = $('.pblabel2' + TotalBoardList[i].BoardID, this).text(newVal + '%');
                }
            });

            var label = progressbar.find('.pblabel2' + TotalBoardList[i].BoardID).clone().width(progressbar.width());
            progressbar.find('.ui-progressbar-value').append(label);

            $('#progressbar2' + TotalBoardList[i].BoardID).css('background', '#ededed');
            if (fullvalue != 0) {
                $('#progressbar2' + TotalBoardList[i].BoardID + ' .ui-widget-header').attr('style', 'background-color:#b2b2b2 !important; width:' + fullvalue + '%;');
            }
            $('#progressbar2' + TotalBoardList[i].BoardID + ' span').attr('style', 'padding:5px; background:#8f8f8f;margin-left: 0px;color: #fff;width: 100%;');
            $('#progressbar2' + TotalBoardList[i].BoardID + ' div span').css('background', '#b2b2b2');
            listIndex = i;
            flag = i;
            $('.hover-frame').slideUp();
        }

        listIndex = listIndex + 1;
    });

    //  BindGrid();
});


function BindGetboardteamclientaccountmanagerOwner() {
    $.ajax({
        url: "/DataTracker.svc/GetboardteamclientaccountmanagerOwner",
        type: "GET",
        contentType: "application/json;charset=utf-8",
        dataType: "json",
        success: function (data) {
            var xmlDoc = $.parseXML(data.d);
            var xml = $(xmlDoc);
            var table = xml.find("Table");
            var table1 = xml.find("Table1");
            var table2 = xml.find("Table2");
            var table3 = xml.find("Table3");
            if (table.length > 0) {
                $(table).each(function () {
                    $("#AllTeam").append('<li id="' + $(this).find("id").text() + '" class="filterboards"><a href="javascript:void(0)">' + $(this).find("TeamName").text() + '</a></li>');
                    SortListByAlphabets("AllTeam");
                });
            }
            else { $("#AllTeam").append("No Team"); }

            if (table1.length > 0) {
                $(table1).each(function () {
                    $("#AllClient").append('<li id="' + $(this).find("ClientID").text() + '" class="filterboards"><a href="javascript:void(0)">' + $(this).find("ClientName").text() + '</a></li>');
                });
                SortListByAlphabets("AllClient");
            }
            else { $("#AllClient").append("No client"); }

            if (table2.length > 0) {
                $(table2).each(function () {
                    $("#AllAccountManager").append('<li id="' + $(this).find("id").text() + '" class="filterboards"><a href="javascript:void(0)">' + $(this).find("Name").text() + '</a></li>');
                });
                SortListByAlphabets("AllAccountManager");
            }
            else { $("#AllAccountManager").append("No account manager"); }

            if (table3.length > 0) {
                $(table3).each(function () {
                    var OwnerName = $(this).find("Fname").text() + " " + $(this).find("Lname").text();
                    $("#AllOwner").append('<li id="' + $(this).find("OwnerId").text() + '" class="filterboards"><a href="javascript:void(0)">' + OwnerName + '</a></li>');
                });
                SortListByAlphabets("AllOwner");
            }
            else { $("#AllOwner").append("No Owner"); }
        },
        error: function () {

        }
    });
}

function SortListByAlphabets(listID) {
    var $list = $("#" + listID);
    $list.children().detach().sort(function (a, b) {
        return $(a).text().trim().localeCompare($(b).text().trim());
    }).appendTo($list);
}

function ChangeUrl(page, url) {

    if (typeof (history.pushState) != "undefined") {
        var obj = { Page: page, Url: url };
        history.pushState(obj, obj.Page, obj.Url);
    } else {

    }
}

function BindGrid() {
    $(".tabular").dataTable({

        "aoColumns": [
         { "bSortable": true },
         { "bSortable": true },
         { "bSortable": true },
         { "bSortable": false },
         { "bSortable": false },
         { "bSortable": true },
         { "bSortable": false },
         { "bSortable": true },//invisible column,
        { "bSortable": false }
        ],

        "order": [],
        "destroy": true,
        "bPaginate": true,
        "bInfo": true,
        "bFilter": false,
        "bLengthChange": false,
        "iDisplayLength": 20,
        "pagingType": "full_numbers"
    });
}
$('.tabular label').live('click', function () {
    $('#addboardname').text("");
    var id = $(this).attr('id');
    var BID = id.split('_');
    var BoardID = BID[1];
    var boardname = $(this).parent().siblings(":first").text();
    $('#addboardname').text(boardname);
    if (BoardID != "") {
        $.ajax({
            type: 'POST',
            url: 'DataTracker.svc/Getcurrentuser',
            data: "{\"boardid\":\"" + BoardID + "\"}",
            contentType: "application/json; charset=utf-8",
            datatype: 'json',
            success: function (data) {
                $('#tblCurrentUsers tbody').empty();
                if (data.d.length) {
                    $.each(data.d, function (index, item) {
                        if (item.CurrentStatus == 1) {
                            $('#tblCurrentUsers tbody').append("<tr style='color:green;'><td>" + item.UserName + "</td><td>" + item.title + "</td><td>" + item.Totalhoursandminute + "</td></tr>");
                        }
                        else { $('#tblCurrentUsers tbody').append("<tr><td>" + item.UserName + "</td><td>" + item.title + "</td><td>" + item.Totalhoursandminute + "</td></tr>"); }
                    });
                }
                else {
                    $('#tblCurrentUsers tbody').append("No Record");
                }
                $('.overlay-wrap').css({ 'display': 'block' });
                $('.overlay').css({ 'display': 'block' });
            },
            error: function () {

            }
        });
    }


});

$('#BordReportGrid ul li label').live('click', function () {

    $('#addboardname1').text("");
    var id = $(this).attr('id');
    var BID = id.split('_');
    var BoardID = BID[1];
    var boardname = $('#' + BoardID + ' a h3').text();
    $('#addboardname1').text(boardname);
    if (BoardID != "") {
        $.ajax({
            type: 'POST',
            url: 'DataTracker.svc/Getcurrentuser',
            data: "{\"boardid\":\"" + BoardID + "\"}",
            contentType: "application/json; charset=utf-8",
            datatype: 'json',
            success: function (data) {
                $('#tblCurrentUsers tbody').empty();

                if (data.d.length) {
                    $.each(data.d, function (index, item) {
                        if (item.CurrentStatus == 1) {
                            $('#tblCurrentUsers tbody').append("<tr style='color:green;'><td>" + item.UserName + "</td><td>" + item.title + "</td><td>" + item.Totalhoursandminute + "</td></tr>");
                        }
                        else { $('#tblCurrentUsers tbody').append("<tr><td>" + item.UserName + "</td><td>" + item.title + "</td><td>" + item.Totalhoursandminute + "</td></tr>"); }

                    });
                }
                else {
                    $('#tblCurrentUsers tbody').append("No Record");
                }
                $('.overlay-wrap').css({ 'display': 'block' });
                $('.overlayclose').css({ 'display': 'block' });
            },
            error: function () {

            }
        });
    }
});

function TotalBoardOnReport() {
    $.ajax({
        type: 'GET',
        url: 'DataTracker.svc/GetAllBoardDetails',
        contentType: "application/json; charset=utf-8",
        datatype: 'json',
        async: false,
        success: function (data) {
            var listcolor = 0;
            var boardcolor = "";
            if (data.d.length > 0) {


                filtersAllBoardid.length = 0;
                $('#loadboard').css('display', 'block');

                if ($('#BordReportGrid').css('display') == 'none') {

                    $('#loadboard').css('display', 'none');
                }
                $('#BordReportGrid').empty();

                var fullvalue = "";
                var consumedhour = "";
                TotalBoardList = data.d;
                if (data.d.length < 20) {
                    $('#loadboard').css('display', 'none');
                    for (var i = 0; i < data.d.length; i++) {

                        if (parseInt(TotalBoardList[i].EffortsToday) > 0) {
                            if (TotalBoardList[i].Delay > 0) {
                                $('#BordReportGrid').append("<div id=" + TotalBoardList[i].BoardID + " class='col-md-3 col-sm-6 col-xs-12 board-task-red'><div class='shadow-div'><div class='shadow-div'><h3 style='background:#2f77a5'>" + TotalBoardList[i].BoardName + "<div class='hover-frame'><div class='hoverdiv' myhovertag='board'><a target='_blank' href='" + DomainURL + "/board.aspx?" + TotalBoardList[i].BoardID + "'><img class='a' src='Styles/BoardReport/images/board-hiden-hover.png'/><label>Board</label></a></div> <div class='hoverdiv' myhovertag='Summary'><a target='_blank' href='" + DomainURL + "/boardSummeryReport.aspx?BID?=" + TotalBoardList[i].BoardID + "'><label class='pull-right'>Summary</label><img src='Styles/BoardReport/images/summary-hiden-hover.png' class='pull-right'/></a></div></div></h3><ul><li><img src='Styles/BoardReport/images/team_grey.png' title='Team Name'><span>" + TotalBoardList[i].TeamName + "</span></li><li><img src='Styles/BoardReport/images/owner_grey.png' title='Board Owner'><span>" + TotalBoardList[i].OwnerName + "</span></li><li><img src='Styles/BoardReport/images/efforts_today.png' title='Efforts'><span>Today's Efforts</span><label style='background: #44bf53' id='BID_" + TotalBoardList[i].BoardID + "'>" + TotalBoardList[i].EffortsToday + "</label></li><li><img src='Styles/BoardReport/images/date.png' title='Create Date'><span>" + TotalBoardList[i].DueDate + "</span></li><li style='padding:0px;'><div id='progressbar" + TotalBoardList[i].BoardID + "'><span id='check' class='pblabel" + TotalBoardList[i].BoardID + " pblabel'></span><img class='Clsdelay' src='Images/delay.png' title='Delay " + TotalBoardList[i].Delay + " hrs' alt=''/></div></li><li style='padding:0px;'><div id='progressbar2" + TotalBoardList[i].BoardID + "'><span id='check1' class='pblabel2" + TotalBoardList[i].BoardID + " pblabel1'></span></div></li></ul><div class='clearfix'></div><div id='bar1' class='barfiller'><div class='tipWrap'><span class='tip'></span></div><span class='fill' data-percentage='30'></span></div></div></div> ");
                            }
                            else { $('#BordReportGrid').append("<div id=" + TotalBoardList[i].BoardID + " class='col-md-3 col-sm-6 col-xs-12 board-task-red'><div class='shadow-div'><div class='shadow-div'><h3 style='background:#2f77a5'>" + TotalBoardList[i].BoardName + "<div class='hover-frame'><div class='hoverdiv' myhovertag='board'><a target='_blank' href='" + DomainURL + "/board.aspx?" + TotalBoardList[i].BoardID + "'><img class='a' src='Styles/BoardReport/images/board-hiden-hover.png'/><label>Board</label></a></div> <div class='hoverdiv' myhovertag='Summary'><a target='_blank' href='" + DomainURL + "/boardSummeryReport.aspx?BID?=" + TotalBoardList[i].BoardID + "'><label class='pull-right'>Summary</label><img src='Styles/BoardReport/images/summary-hiden-hover.png' class='pull-right'/></a></div></div></h3><ul><li><img src='Styles/BoardReport/images/team_grey.png' title='Team Name'><span>" + TotalBoardList[i].TeamName + "</span></li><li><img src='Styles/BoardReport/images/owner_grey.png' title='Board Owner'><span>" + TotalBoardList[i].OwnerName + "</span></li><li><img src='Styles/BoardReport/images/efforts_today.png' title='Efforts'><span>Today's Efforts</span><label style='background: #44bf53' id='BID_" + TotalBoardList[i].BoardID + "'>" + TotalBoardList[i].EffortsToday + "</label></li><li><img src='Styles/BoardReport/images/date.png' title='Create Date'><span>" + TotalBoardList[i].DueDate + "</span></li><li style='padding:0px;'><div id='progressbar" + TotalBoardList[i].BoardID + "'><span id='check' class='pblabel" + TotalBoardList[i].BoardID + " pblabel'></span></div></li><li style='padding:0px;'><div id='progressbar2" + TotalBoardList[i].BoardID + "'><span id='check1' class='pblabel2" + TotalBoardList[i].BoardID + " pblabel1'></span></div></li></ul><div class='clearfix'></div><div id='bar1' class='barfiller'><div class='tipWrap'><span class='tip'></span></div><span class='fill' data-percentage='30'></span></div></div></div> "); }

                        }
                        else {
                            if (TotalBoardList[i].Delay > 0) {
                                $('#BordReportGrid').append("<div id=" + TotalBoardList[i].BoardID + " class='col-md-3 col-sm-6 col-xs-12 board-task-red'><div class='shadow-div'><div class='shadow-div'><h3 style='background:#2f77a5'>" + TotalBoardList[i].BoardName + "<div class='hover-frame'><div class='hoverdiv' myhovertag='board'><a target='_blank' href='" + DomainURL + "/board.aspx?" + TotalBoardList[i].BoardID + "'><img class='a' src='Styles/BoardReport/images/board-hiden-hover.png'/><label>Board</label></a></div> <div class='hoverdiv' myhovertag='Summary'><a target='_blank' href='" + DomainURL + "/boardSummeryReport.aspx?BID?=" + TotalBoardList[i].BoardID + "'><label class='pull-right'>Summary</label><img src='Styles/BoardReport/images/summary-hiden-hover.png' class='pull-right'/></a></div></div></h3><ul><li><img src='Styles/BoardReport/images/team_grey.png' title='Team Name'><span>" + TotalBoardList[i].TeamName + "</span></li><li><img src='Styles/BoardReport/images/owner_grey.png' title='Board Owner'><span>" + TotalBoardList[i].OwnerName + "</span></li><li><img src='Styles/BoardReport/images/efforts_today.png' title='Efforts'><span>Today's Efforts</span><label id='BID_" + TotalBoardList[i].BoardID + "'>" + TotalBoardList[i].EffortsToday + "</label></li><li><img src='Styles/BoardReport/images/date.png' title='Create Date'><span>" + TotalBoardList[i].DueDate + "</span></li><li style='padding:0px;'><div id='progressbar" + TotalBoardList[i].BoardID + "'><span id='check' class='pblabel" + TotalBoardList[i].BoardID + " pblabel'></span><img class='Clsdelay' src='Images/delay.png' title='Delay " + TotalBoardList[i].Delay + " hrs' alt=''/></div></li><li style='padding:0px;'><div id='progressbar2" + TotalBoardList[i].BoardID + "'><span id='check1' class='pblabel2" + TotalBoardList[i].BoardID + " pblabel1'></span></div></li></ul><div class='clearfix'></div><div id='bar1' class='barfiller'><div class='tipWrap'><span class='tip'></span></div><span class='fill' data-percentage='30'></span></div></div></div> ");
                            }
                            else { $('#BordReportGrid').append("<div id=" + TotalBoardList[i].BoardID + " class='col-md-3 col-sm-6 col-xs-12 board-task-red'><div class='shadow-div'><div class='shadow-div'><h3 style='background:#2f77a5'>" + TotalBoardList[i].BoardName + "<div class='hover-frame'><div class='hoverdiv' myhovertag='board'><a target='_blank' href='" + DomainURL + "/board.aspx?" + TotalBoardList[i].BoardID + "'><img class='a' src='Styles/BoardReport/images/board-hiden-hover.png'/><label>Board</label></a></div> <div class='hoverdiv' myhovertag='Summary'><a target='_blank' href='" + DomainURL + "/boardSummeryReport.aspx?BID?=" + TotalBoardList[i].BoardID + "'><label class='pull-right'>Summary</label><img src='Styles/BoardReport/images/summary-hiden-hover.png' class='pull-right'/></a></div></div></h3><ul><li><img src='Styles/BoardReport/images/team_grey.png' title='Team Name'><span>" + TotalBoardList[i].TeamName + "</span></li><li><img src='Styles/BoardReport/images/owner_grey.png' title='Board Owner'><span>" + TotalBoardList[i].OwnerName + "</span></li><li><img src='Styles/BoardReport/images/efforts_today.png' title='Efforts'><span>Today's Efforts</span><label id='BID_" + TotalBoardList[i].BoardID + "'>" + TotalBoardList[i].EffortsToday + "</label></li><li><img src='Styles/BoardReport/images/date.png'title='Create Date'><span>" + TotalBoardList[i].DueDate + "</span></li><li style='padding:0px;'><div id='progressbar" + TotalBoardList[i].BoardID + "'><span id='check' class='pblabel" + TotalBoardList[i].BoardID + " pblabel'></span></div></li><li style='padding:0px;'><div id='progressbar2" + TotalBoardList[i].BoardID + "'><span id='check1' class='pblabel2" + TotalBoardList[i].BoardID + " pblabel1'></span></div></li></ul><div class='clearfix'></div><div id='bar1' class='barfiller'><div class='tipWrap'><span class='tip'></span></div><span class='fill' data-percentage='30'></span></div></div></div> "); }

                        }
                        //progress for board percentage
                        fullvalue = parseInt(TotalBoardList[i].TaskCompletePercent);
                        $('.pblabel' + TotalBoardList[i].BoardID).text(fullvalue + "%");
                        var progressbar = $('#progressbar' + TotalBoardList[i].BoardID).progressbar({
                            value: fullvalue,
                            change: function (event, ui) {
                                var newVal = $(this).progressbar('option', 'value');
                                var label = $('.pblabel' + TotalBoardList[i].BoardID, this).text(newVal + '%');
                            }
                        });
                        var label = progressbar.find('.pblabel' + TotalBoardList[i].BoardID).clone().width(progressbar.width());
                        //label+"<img src=Images/delay.png' alt='' style='float: right; position: absolute;right: 0px;z-index: 2;width: 20px;'>";
                        progressbar.find('.ui-progressbar-value').append(label);

                        $('#progressbar' + TotalBoardList[i].BoardID).css('background', '#f5f5f5');


                        if (fullvalue != 0) {
                            $('#progressbar' + TotalBoardList[i].BoardID + ' .ui-widget-header').attr('style', 'background-color:' + TotalBoardList[i].boardcolor + ' !important; width:' + fullvalue + '%;');
                        }

                        listIndex = i + 1;


                        //progress for estimated hour percentage
                        fullvalue = parseInt(TotalBoardList[i].consumeHourPercentage);
                        consumedhour = TotalBoardList[i].consumehours + " hr " + "/ " + TotalBoardList[i].EstimatedHours + " hr ";
                        $('.pblabel2' + TotalBoardList[i].BoardID).text(consumedhour);
                        var progressbar = $('#progressbar2' + TotalBoardList[i].BoardID).progressbar({
                            value: fullvalue,
                            change: function (event, ui) {
                                var newVal = $(this).progressbar('option', 'value');
                                var label = $('.pblabel2' + TotalBoardList[i].BoardID, this).text(newVal + '%');
                            }
                        });

                        var label = progressbar.find('.pblabel2' + TotalBoardList[i].BoardID).clone().width(progressbar.width());
                        progressbar.find('.ui-progressbar-value').append(label);
                        $('#progressbar2' + TotalBoardList[i].BoardID).css('background', '#ededed');

                        if (fullvalue != 0) {
                            $('#progressbar2' + TotalBoardList[i].BoardID + ' .ui-widget-header').attr('style', 'background-color:#b2b2b2 !important; width:' + fullvalue + '%;');
                        }
                        $('#progressbar2' + TotalBoardList[i].BoardID + ' span').attr('style', 'padding:5px; background:#8f8f8f;margin-left: 0px;color: #fff;width: 100%;');
                        $('#progressbar2' + TotalBoardList[i].BoardID + ' div span').css('background', '#b2b2b2');
                    }
                    $('.hover-frame').slideUp();
                }
                else {
                    for (var i = 0; i <= flag; i++) {

                        if (parseInt(TotalBoardList[i].EffortsToday) > 0) {
                            if (TotalBoardList[i].Delay > 0) {
                                $('#BordReportGrid').append("<div id=" + TotalBoardList[i].BoardID + " class='col-md-3 col-sm-6 col-xs-12 board-task-red'><div class='shadow-div'><div class='shadow-div'><h3 style='background:#2f77a5'>" + TotalBoardList[i].BoardName + "<div class='hover-frame'><div class='hoverdiv' myhovertag='board'><a target='_blank' href='" + DomainURL + "/board.aspx?" + TotalBoardList[i].BoardID + "'><img class='a' src='Styles/BoardReport/images/board-hiden-hover.png'/><label>Board</label></a></div> <div class='hoverdiv' myhovertag='Summary'><a target='_blank' href='" + DomainURL + "/boardSummeryReport.aspx?BID?=" + TotalBoardList[i].BoardID + "'><label class='pull-right'>Summary</label><img src='Styles/BoardReport/images/summary-hiden-hover.png' class='pull-right'/></a></div></div></h3><ul><li><img src='Styles/BoardReport/images/team_grey.png' title='Team Name'><span>" + TotalBoardList[i].TeamName + "</span></li><li><img src='Styles/BoardReport/images/owner_grey.png' title='Board Owner'><span>" + TotalBoardList[i].OwnerName + "</span></li><li><img src='Styles/BoardReport/images/efforts_today.png' title='Efforts'><span>Today's Efforts</span><label style='background: #44bf53' id='BID_" + TotalBoardList[i].BoardID + "'>" + TotalBoardList[i].EffortsToday + "</label></li><li><img src='Styles/BoardReport/images/date.png' title='Create Date'><span>" + TotalBoardList[i].DueDate + "</span></li><li style='padding:0px;'><div id='progressbar" + TotalBoardList[i].BoardID + "'><span id='check' class='pblabel" + TotalBoardList[i].BoardID + " pblabel'></span><img class='Clsdelay' src='Images/delay.png' title='Delay " + TotalBoardList[i].Delay + " hrs' alt=''/></div></li><li style='padding:0px;'><div id='progressbar2" + TotalBoardList[i].BoardID + "'><span id='check1' class='pblabel2" + TotalBoardList[i].BoardID + " pblabel1'></span></div></li></ul><div class='clearfix'></div><div id='bar1' class='barfiller'><div class='tipWrap'><span class='tip'></span></div><span class='fill' data-percentage='30'></span></div></div></div> ");
                            }
                            else { $('#BordReportGrid').append("<div id=" + TotalBoardList[i].BoardID + " class='col-md-3 col-sm-6 col-xs-12 board-task-red'><div class='shadow-div'><h3 style='background:#2f77a5'>" + TotalBoardList[i].BoardName + "<div class='hover-frame'><div class='hoverdiv' myhovertag='board'><a target='_blank' href='" + DomainURL + "/board.aspx?" + TotalBoardList[i].BoardID + "'><img class='a' src='Styles/BoardReport/images/board-hiden-hover.png'/><label>Board</label></a></div> <div class='hoverdiv' myhovertag='Summary'><a target='_blank' href='" + DomainURL + "/boardSummeryReport.aspx?BID?=" + TotalBoardList[i].BoardID + "'><label class='pull-right'>Summary</label><img src='Styles/BoardReport/images/summary-hiden-hover.png' class='pull-right'/></a></div></div></h3><ul><li><img src='Styles/BoardReport/images/team_grey.png' title='Team Name'><span>" + TotalBoardList[i].TeamName + "</span></li><li><img src='Styles/BoardReport/images/owner_grey.png' title='Board Owner'><span>" + TotalBoardList[i].OwnerName + "</span></li><li><img src='Styles/BoardReport/images/efforts_today.png' title='Efforts'><span>Today's Efforts</span><label style='background: #44bf53' id='BID_" + TotalBoardList[i].BoardID + "'>" + TotalBoardList[i].EffortsToday + "</label></li><li><img src='Styles/BoardReport/images/date.png' title='Create Date'><span>" + TotalBoardList[i].DueDate + "</span></li><li style='padding:0px;'><div id='progressbar" + TotalBoardList[i].BoardID + "'><span id='check' class='pblabel" + TotalBoardList[i].BoardID + " pblabel'></span></div></li><li style='padding:0px;'><div id='progressbar2" + TotalBoardList[i].BoardID + "'><span id='check1' class='pblabel2" + TotalBoardList[i].BoardID + " pblabel1'></span></div></li></ul><div class='clearfix'></div><div id='bar1' class='barfiller'><div class='tipWrap'><span class='tip'></span></div><span class='fill' data-percentage='30'></span></div></div></div> "); }

                        }
                        else {
                            if (TotalBoardList[i].Delay > 0) {
                                $('#BordReportGrid').append("<div id=" + TotalBoardList[i].BoardID + " class='col-md-3 col-sm-6 col-xs-12 board-task-red'><div class='shadow-div'><h3 style='background:#2f77a5'>" + TotalBoardList[i].BoardName + "<div class='hover-frame'><div class='hoverdiv' myhovertag='board'><a target='_blank' href='" + DomainURL + "/board.aspx?" + TotalBoardList[i].BoardID + "'><img class='a' src='Styles/BoardReport/images/board-hiden-hover.png'/><label>Board</label></a></div> <div class='hoverdiv' myhovertag='Summary'><a target='_blank' href='" + DomainURL + "/boardSummeryReport.aspx?BID?=" + TotalBoardList[i].BoardID + "'><label class='pull-right'>Summary</label><img src='Styles/BoardReport/images/summary-hiden-hover.png' class='pull-right'/></a></div></div></h3><ul><li><img src='Styles/BoardReport/images/team_grey.png' title='Team Name'><span>" + TotalBoardList[i].TeamName + "</span></li><li><img src='Styles/BoardReport/images/owner_grey.png' title='Board Owner'><span>" + TotalBoardList[i].OwnerName + "</span></li><li><img src='Styles/BoardReport/images/efforts_today.png' title='Efforts'><span>Today's Efforts</span><label id='BID_" + TotalBoardList[i].BoardID + "'>" + TotalBoardList[i].EffortsToday + "</label></li><li><img src='Styles/BoardReport/images/date.png' title='Create Date'><span>" + TotalBoardList[i].DueDate + "</span></li><li style='padding:0px;'><div id='progressbar" + TotalBoardList[i].BoardID + "'><span id='check' class='pblabel" + TotalBoardList[i].BoardID + " pblabel'></span><img class='Clsdelay' src='Images/delay.png' title='Delay " + TotalBoardList[i].Delay + " hrs' alt=''/></div></li><li style='padding:0px;'><div id='progressbar2" + TotalBoardList[i].BoardID + "'><span id='check1' class='pblabel2" + TotalBoardList[i].BoardID + " pblabel1'></span></div></li></ul><div class='clearfix'></div><div id='bar1' class='barfiller'><div class='tipWrap'><span class='tip'></span></div><span class='fill' data-percentage='30'></span></div></div></div> ");
                            }
                            else { $('#BordReportGrid').append("<div id=" + TotalBoardList[i].BoardID + " class='col-md-3 col-sm-6 col-xs-12 board-task-red'><div class='shadow-div'><h3 style='background:#2f77a5'>" + TotalBoardList[i].BoardName + "<div class='hover-frame'><div class='hoverdiv' myhovertag='board'><a target='_blank' href='" + DomainURL + "/board.aspx?" + TotalBoardList[i].BoardID + "'><img class='a' src='Styles/BoardReport/images/board-hiden-hover.png'/><label>Board</label></a></div> <div class='hoverdiv' myhovertag='Summary'><a target='_blank' href='" + DomainURL + "/boardSummeryReport.aspx?BID?=" + TotalBoardList[i].BoardID + "'><label class='pull-right'>Summary</label><img src='Styles/BoardReport/images/summary-hiden-hover.png' class='pull-right'/></a></div></div></h3><ul><li><img src='Styles/BoardReport/images/team_grey.png' title='Team Name'><span>" + TotalBoardList[i].TeamName + "</span></li><li><img src='Styles/BoardReport/images/owner_grey.png' title='Board Owner'><span>" + TotalBoardList[i].OwnerName + "</span></li><li><img src='Styles/BoardReport/images/efforts_today.png' title='Efforts'><span>Today's Efforts</span><label id='BID_" + TotalBoardList[i].BoardID + "'>" + TotalBoardList[i].EffortsToday + "</label></li><li><img src='Styles/BoardReport/images/date.png' title='Create Date'><span>" + TotalBoardList[i].DueDate + "</span></li><li style='padding:0px;'><div id='progressbar" + TotalBoardList[i].BoardID + "'><span id='check' class='pblabel" + TotalBoardList[i].BoardID + " pblabel'></span></div></li><li style='padding:0px;'><div id='progressbar2" + TotalBoardList[i].BoardID + "'><span id='check1' class='pblabel2" + TotalBoardList[i].BoardID + " pblabel1'></span></div></li></ul><div class='clearfix'></div><div id='bar1' class='barfiller'><div class='tipWrap'><span class='tip'></span></div><span class='fill' data-percentage='30'></span></div></div></div> "); }

                        }
                        //progress for board percentage
                        fullvalue = parseInt(TotalBoardList[i].TaskCompletePercent);
                        $('.pblabel' + TotalBoardList[i].BoardID).text(fullvalue + "%");
                        var progressbar = $('#progressbar' + TotalBoardList[i].BoardID).progressbar({
                            value: fullvalue,
                            change: function (event, ui) {
                                var newVal = $(this).progressbar('option', 'value');
                                var label = $('.pblabel' + TotalBoardList[i].BoardID, this).text(newVal + '%');
                            }
                        });
                        var label = progressbar.find('.pblabel' + TotalBoardList[i].BoardID).clone().width(progressbar.width());
                        progressbar.find('.ui-progressbar-value').append(label);
                        $('#progressbar' + TotalBoardList[i].BoardID).css('background', '#f5f5f5');


                        if (fullvalue != 0) {
                            $('#progressbar' + TotalBoardList[i].BoardID + ' .ui-widget-header').attr('style', 'background-color:' + TotalBoardList[i].boardcolor + ' !important; width:' + fullvalue + '%;');
                        }

                        listIndex = i + 1;


                        //progress for estimated hour percentage
                        fullvalue = parseInt(TotalBoardList[i].consumeHourPercentage);
                        consumedhour = TotalBoardList[i].consumehours + " hr " + "/ " + TotalBoardList[i].EstimatedHours + " hr ";
                        $('.pblabel2' + TotalBoardList[i].BoardID).text(consumedhour);
                        var progressbar = $('#progressbar2' + TotalBoardList[i].BoardID).progressbar({
                            value: fullvalue,
                            change: function (event, ui) {
                                var newVal = $(this).progressbar('option', 'value');
                                var label = $('.pblabel2' + TotalBoardList[i].BoardID, this).text(newVal + '%');
                            }
                        });

                        var label = progressbar.find('.pblabel2' + TotalBoardList[i].BoardID).clone().width(progressbar.width());
                        progressbar.find('.ui-progressbar-value').append(label);

                        $('#progressbar2' + TotalBoardList[i].BoardID).css('background', '#ededed');
                        if (fullvalue != 0) {
                            $('#progressbar2' + TotalBoardList[i].BoardID + ' .ui-widget-header').attr('style', 'background-color:#b2b2b2 !important; width:' + fullvalue + '%;');
                        }

                        $('#progressbar2' + TotalBoardList[i].BoardID + ' span').attr('style', 'padding:5px; background:#8f8f8f;margin-left: 0px;color: #fff;width: 100%;');
                        $('#progressbar2' + TotalBoardList[i].BoardID + ' div span').css('background', '#b2b2b2');
                    }
                    $('.hover-frame').slideUp();
                }


                $('.tabular').empty();
                $(".tabular").append('<thead><tr class="blue noExl"><th><img src="Styles/BoardReport/images/name-wh.png">Name</th><th><img src="Styles/BoardReport/images/team-wh.png">Team</th><th><img src="Styles/BoardReport/images/owner-wh.png">Owner</th><th><img src="Styles/BoardReport/images/start_date-wh.png">Start Date</th><th><img src="Styles/BoardReport/images/end_date-wh.png">End Date</th><th><img src="Styles/BoardReport/images/efforts-wh.png">Today' + "'" + 's Efforts</th><th><img src="Styles/BoardReport/images/hours-wh.png">Hours Consumed</th><th><img src="Styles/BoardReport/images/board-wh.png">Board Progress</th><th><img src="Images/action.png">Action</th></tr></thead><tbody>');
                if (usertypeid == "12") {
                    $.each(data.d, function (index, item) {
                        if (parseInt(item.EffortsToday) > 0) {
                            $('.tabular tbody').append("<tr><td style='border-left:solid 10px " + item.boardcolor + "'><a target='_blank'href=" + DomainURL + "/BoardSummeryReport.aspx?BID=" + item.BoardID + ">" + item.BoardName + "</a><span class='board-sumary-hover-cont' style='background:" + item.boardcolor + "'><a target='_blank' href='" + DomainURL + "/board.aspx?" + item.BoardID + "'><img src='Styles/BoardReport/images/board.png'></a><a target='_blank' href='" + DomainURL + "/boardSummeryReport.aspx?BID?=" + item.BoardID + "'><img src='Styles/BoardReport/images/summary.png'></a></span></td><td>" + item.TeamName + "</td><td>" + item.OwnerName + "</td><td>" + item.CreatedDate + "</td><td>" + item.DueDate + "</td><td><label style='background: #44bf53' id='BID_" + item.BoardID + "'>" + item.EffortsToday + "</label></td><td style='padding:0px;'><div id='progressbar3" + item.BoardID + "'><span id='check' class='pblabel3" + item.BoardID + " pblabel'></span></div></td><td style='padding:0px;'><div id='progressbar1" + item.BoardID + "'><span id='check' class='pblabel1" + item.BoardID + " pblabel'></span></div></td><td><img src='Images/icon1.png' class='archiveandcomplete' title='Archive Board' ArchiveTag='archive'/><img src='Images/icon2.png' title='Complete Board' ArchiveTag='complete' class='archiveandcomplete'/></td></tr>");
                        }
                        else {
                            $('.tabular tbody').append("<tr><td style='border-left:solid 10px " + item.boardcolor + "'><a target='_blank'href=" + DomainURL + "/BoardSummeryReport.aspx?BID=" + item.BoardID + ">" + item.BoardName + "</a><span class='board-sumary-hover-cont' style='background:" + item.boardcolor + "'><a target='_blank' href='" + DomainURL + "/board.aspx?" + item.BoardID + "'><img src='Styles/BoardReport/images/board.png'></a><a target='_blank' href='" + DomainURL + "/boardSummeryReport.aspx?BID?=" + item.BoardID + "'><img src='Styles/BoardReport/images/summary.png'></a></span></td><td>" + item.TeamName + "</td><td>" + item.OwnerName + "</td><td>" + item.CreatedDate + "</td><td>" + item.DueDate + "</td><td><label id='BID_" + item.BoardID + "'>" + item.EffortsToday + "</label></td><td style='padding:0px;'><div id='progressbar3" + item.BoardID + "'><span id='check' class='pblabel3" + item.BoardID + " pblabel'></span></div></td><td style='padding:0px;'><div id='progressbar1" + item.BoardID + "'><span id='check' class='pblabel1" + item.BoardID + " pblabel'></span></div></td><td><img src='Images/icon1.png' title='Archive Board' class='archiveandcomplete' ArchiveTag='archive'/><img src='Images/icon2.png' class='archiveandcomplete' title='Complete Board' ArchiveTag='complete'/></td></tr>");
                        }

                        //progress for board percentage
                        fullvalue = parseInt(item.TaskCompletePercent);
                        $('.pblabel1' + item.BoardID).text(fullvalue + "%");
                        var progressbar = $('#progressbar1' + item.BoardID).progressbar({
                            value: fullvalue,
                            change: function (event, ui) {
                                var newVal = $(this).progressbar('option', 'value');
                                var label = $('.pblabel1' + item.BoardID, this).text(newVal + '%');
                            }
                        });


                        if (fullvalue != 0) {
                            var label = progressbar.find('.pblabel1' + item.BoardID).width(progressbar.width());
                            progressbar.find('.ui-progressbar-value').append(label);
                            $('#progressbar1' + item.BoardID + ' .ui-widget-header').attr('style', 'background-color:' + item.boardcolor + ' !important; width:' + parseInt(fullvalue) + '%;');
                        }
                        else {
                            var label = progressbar.find('.pblabel1' + item.BoardID).clone().width(progressbar.width());
                            progressbar.find('.ui-progressbar-value').append(label);
                        }

                        $('.pblabel1' + item.BoardID).css('width', '');

                        //progress for estimated hour percentage
                        fullvalue = parseInt(item.consumeHourPercentage);
                        consumedhour = item.consumehours + " hr " + "/ " + item.EstimatedHours + " hr ";
                        $('.pblabel3' + item.BoardID).text(consumedhour);
                        var progressbar = $('#progressbar3' + item.BoardID).progressbar({
                            value: fullvalue,
                            change: function (event, ui) {
                                var newVal = $(this).progressbar('option', 'value');
                                var label = $('.pblabel3' + item.BoardID, this).text(newVal + '%');
                            }
                        });

                        if (fullvalue != 0) {
                            var label = progressbar.find('.pblabel3' + item.BoardID).width(progressbar.width());
                            progressbar.find('.ui-progressbar-value').append(label);
                            $('#progressbar3' + item.BoardID + ' .ui-widget-header').attr('style', 'background-color:#8f8f8f !important; width:' + parseInt(fullvalue) + '%;');
                        }
                        else {
                            var label = progressbar.find('.pblabel3' + item.BoardID).clone().width(progressbar.width());
                            progressbar.find('.ui-progressbar-value').append(label);

                        }

                        if (parseInt(fullvalue + 2) < 50) {
                            $('#progressbar3' + item.BoardID + ' div').addClass('fitdivspan');
                            $('#progressbar3' + item.BoardID + ' span').css('color', 'white');
                        }

                        if (fullvalue == 0) {
                            $('#progressbar3' + item.BoardID + ' .ui-widget-header   span').remove();
                        }
                        $('.pblabel3' + item.BoardID).css('width', '')
                        i++;
                        $('#progressbar3' + item.BoardID + ' div span').css('background', '#b2b2b2');
                    });
                    $('.tabular tbody td').css('padding', '');
                }
                else {
                    $.each(data.d, function (index, item) {

                        if (item.flag == 1) {
                            if (parseInt(item.EffortsToday) > 0) {
                                $('.tabular tbody').append("<tr><td style='border-left:solid 10px " + item.boardcolor + "'><a target='_blank'href=" + DomainURL + "/BoardSummeryReport.aspx?BID=" + item.BoardID + ">" + item.BoardName + "</a><span class='board-sumary-hover-cont' style='background:" + item.boardcolor + "'><a target='_blank' href='" + DomainURL + "/board.aspx?" + item.BoardID + "'><img src='Styles/BoardReport/images/board.png'></a><a target='_blank' href='" + DomainURL + "/boardSummeryReport.aspx?BID?=" + item.BoardID + "'><img src='Styles/BoardReport/images/summary.png'></a></span></td><td>" + item.TeamName + "</td><td>" + item.OwnerName + "</td><td>" + item.CreatedDate + "</td><td>" + item.DueDate + "</td><td><label style='background: #44bf53' id='BID_" + item.BoardID + "'>" + item.EffortsToday + "</label></td><td style='padding:0px;'><div id='progressbar3" + item.BoardID + "'><span id='check' class='pblabel3" + item.BoardID + " pblabel'></span></div></td><td style='padding:0px;'><div id='progressbar1" + item.BoardID + "'><span id='check' class='pblabel1" + item.BoardID + " pblabel'></span></div></td><td><img src='Images/icon1.png' class='archiveandcomplete' title='Archive Board' ArchiveTag='archive'/><img src='Images/icon2.png' title='Complete Board' ArchiveTag='complete' class='archiveandcomplete'/></td></tr>");
                            }
                            else {
                                $('.tabular tbody').append("<tr><td style='border-left:solid 10px " + item.boardcolor + "'><a target='_blank'href=" + DomainURL + "/BoardSummeryReport.aspx?BID=" + item.BoardID + ">" + item.BoardName + "</a><span class='board-sumary-hover-cont' style='background:" + item.boardcolor + "'><a target='_blank' href='" + DomainURL + "/board.aspx?" + item.BoardID + "'><img src='Styles/BoardReport/images/board.png'></a><a target='_blank' href='" + DomainURL + "/boardSummeryReport.aspx?BID?=" + item.BoardID + "'><img src='Styles/BoardReport/images/summary.png'></a></span></td><td>" + item.TeamName + "</td><td>" + item.OwnerName + "</td><td>" + item.CreatedDate + "</td><td>" + item.DueDate + "</td><td><label id='BID_" + item.BoardID + "'>" + item.EffortsToday + "</label></td><td style='padding:0px;'><div id='progressbar3" + item.BoardID + "'><span id='check' class='pblabel3" + item.BoardID + " pblabel'></span></div></td><td style='padding:0px;'><div id='progressbar1" + item.BoardID + "'><span id='check' class='pblabel1" + item.BoardID + " pblabel'></span></div></td><td><img src='Images/icon1.png' title='Archive Board' class='archiveandcomplete' ArchiveTag='archive'/><img src='Images/icon2.png' class='archiveandcomplete' title='Complete Board' ArchiveTag='complete'/></td></tr>");
                            }

                        }
                        else {
                            if (parseInt(item.EffortsToday) > 0) {
                                $('.tabular tbody').append("<tr><td style='border-left:solid 10px " + item.boardcolor + "'><a target='_blank'href=" + DomainURL + "/BoardSummeryReport.aspx?BID=" + item.BoardID + ">" + item.BoardName + "</a><span class='board-sumary-hover-cont' style='background:" + item.boardcolor + "'><a target='_blank' href='" + DomainURL + "/board.aspx?" + item.BoardID + "'><img src='Styles/BoardReport/images/board.png'></a><a target='_blank' href='" + DomainURL + "/boardSummeryReport.aspx?BID?=" + item.BoardID + "'><img src='Styles/BoardReport/images/summary.png'></a></span></td><td>" + item.TeamName + "</td><td>" + item.OwnerName + "</td><td>" + item.CreatedDate + "</td><td>" + item.DueDate + "</td><td><label style='background: #44bf53' id='BID_" + item.BoardID + "'>" + item.EffortsToday + "</label></td><td style='padding:0px;'><div id='progressbar3" + item.BoardID + "'><span id='check' class='pblabel3" + item.BoardID + " pblabel'></span></div></td><td style='padding:0px;'><div id='progressbar1" + item.BoardID + "'><span id='check' class='pblabel1" + item.BoardID + " pblabel'></span></div></td><td></td></tr>");
                            }
                            else {
                                $('.tabular tbody').append("<tr><td style='border-left:solid 10px " + item.boardcolor + "'><a target='_blank'href=" + DomainURL + "/BoardSummeryReport.aspx?BID=" + item.BoardID + ">" + item.BoardName + "</a><span class='board-sumary-hover-cont' style='background:" + item.boardcolor + "'><a target='_blank' href='" + DomainURL + "/board.aspx?" + item.BoardID + "'><img src='Styles/BoardReport/images/board.png'></a><a target='_blank' href='" + DomainURL + "/boardSummeryReport.aspx?BID?=" + item.BoardID + "'><img src='Styles/BoardReport/images/summary.png'></a></span></td><td>" + item.TeamName + "</td><td>" + item.OwnerName + "</td><td>" + item.CreatedDate + "</td><td>" + item.DueDate + "</td><td><label id='BID_" + item.BoardID + "'>" + item.EffortsToday + "</label></td><td style='padding:0px;'><div id='progressbar3" + item.BoardID + "'><span id='check' class='pblabel3" + item.BoardID + " pblabel'></span></div></td><td style='padding:0px;'><div id='progressbar1" + item.BoardID + "'><span id='check' class='pblabel1" + item.BoardID + " pblabel'></span></div></td><td></td></tr>");
                            }

                        }

                        //progress for board percentage
                        fullvalue = parseInt(item.TaskCompletePercent);
                        $('.pblabel1' + item.BoardID).text(fullvalue + "%");
                        var progressbar = $('#progressbar1' + item.BoardID).progressbar({
                            value: fullvalue,
                            change: function (event, ui) {
                                var newVal = $(this).progressbar('option', 'value');
                                var label = $('.pblabel1' + item.BoardID, this).text(newVal + '%');
                            }
                        });


                        if (fullvalue != 0) {
                            var label = progressbar.find('.pblabel1' + item.BoardID).width(progressbar.width());
                            progressbar.find('.ui-progressbar-value').append(label);
                            $('#progressbar1' + item.BoardID + ' .ui-widget-header').attr('style', 'background-color:' + item.boardcolor + ' !important; width:' + parseInt(fullvalue) + '%;');
                        }
                        else {
                            var label = progressbar.find('.pblabel1' + item.BoardID).clone().width(progressbar.width());
                            progressbar.find('.ui-progressbar-value').append(label);
                        }

                        $('.pblabel1' + item.BoardID).css('width', '');

                        //progress for estimated hour percentage
                        fullvalue = parseInt(item.consumeHourPercentage);
                        consumedhour = item.consumehours + " hr " + "/ " + item.EstimatedHours + " hr ";
                        $('.pblabel3' + item.BoardID).text(consumedhour);
                        var progressbar = $('#progressbar3' + item.BoardID).progressbar({
                            value: fullvalue,
                            change: function (event, ui) {
                                var newVal = $(this).progressbar('option', 'value');
                                var label = $('.pblabel3' + item.BoardID, this).text(newVal + '%');
                            }
                        });

                        if (fullvalue != 0) {
                            var label = progressbar.find('.pblabel3' + item.BoardID).width(progressbar.width());
                            progressbar.find('.ui-progressbar-value').append(label);
                            $('#progressbar3' + item.BoardID + ' .ui-widget-header').attr('style', 'background-color:#8f8f8f !important; width:' + parseInt(fullvalue) + '%;');
                        }
                        else {
                            var label = progressbar.find('.pblabel3' + item.BoardID).clone().width(progressbar.width());
                            progressbar.find('.ui-progressbar-value').append(label);

                        }

                        if (parseInt(fullvalue + 2) < 50) {
                            $('#progressbar3' + item.BoardID + ' div').addClass('fitdivspan');
                            $('#progressbar3' + item.BoardID + ' span').css('color', 'white');
                        }

                        if (fullvalue == 0) {
                            $('#progressbar3' + item.BoardID + ' .ui-widget-header   span').remove();
                        }
                        $('.pblabel3' + item.BoardID).css('width', '')
                        $('#progressbar3' + item.BoardID + ' div span').css('background', '#b2b2b2');
                        i++;

                    });


                }
                $('.tabular tbody').append("</tbody>");
                $('.hover-frame').slideUp();
                BindGrid();
            }
            else {
                $('#BordReportGrid').append("No Record");
                $('.tabular tbody').append("No Record");
            }


        },
        error: function (err) {

        }
    });
}













$(".filterboards").live("click", function () {

    var currentText = $(this).text();
    if (currentText == "Favourites" || currentText == "All") {
        filtersAllBoardid.length = 0;
        TotalBoardList = "";
        flag = 19;
        listIndex = 0;
        addvaluetomultiplylist = 1;
        $("#displayselected").empty();
        $("#displayselected").text(currentText);
        ChengeFilterIcon(currentText);
        $("#displayselected").parent().css('display', 'block');
        $("#DeleteSelected").css('display', 'block');
        $(".forecast_yellow").css('display', 'none');
        $("#DeletedSelectedForcast").css('display', 'none');

        $.ajax({
            type: 'POST',
            url: 'DataTracker.svc/GetFiltersboards',
            data: "{\"id\":\"0\",\"filter\":\"" + currentText + "\",\"sdate\":\"''\"}",
            contentType: "application/json; charset=utf-8",
            datatype: 'json',
            async: false,
            success: function (data) {
                $('#BordReportGrid').empty();

                TotalBoardList = data.d;
                if (data.d.length > 0) {
                    var fullvalue = "";
                    $('#loadboard').css('display', 'block');
                    if ($('#BordReportGrid').css('display') == 'none') {

                        $('#loadboard').css('display', 'none');
                    }

                    $('#BordReportGrid').empty();
                    var table = $('.tabular').DataTable();
                    table.destroy();
                    $('.tabular').empty();

                    if (data.d.length < 20) {
                        $('#loadboard').css('display', 'none');
                        for (var i = 0; i < data.d.length; i++) {
                            if (parseInt(TotalBoardList[i].EffortsToday) > 0) {
                                if (TotalBoardList[i].Delay > 0) {
                                    $('#BordReportGrid').append("<div id=" + TotalBoardList[i].BoardID + " class='col-md-3 col-sm-6 col-xs-12 board-task-red'><div class='shadow-div'><h3 style='background:#2f77a5'>" + TotalBoardList[i].BoardName + "<div class='hover-frame'><div class='hoverdiv' myhovertag='board'><a target='_blank' href='" + DomainURL + "/board.aspx?" + TotalBoardList[i].BoardID + "'><img class='a' src='Styles/BoardReport/images/board-hiden-hover.png'/><label>Board</label></a></div> <div class='hoverdiv' myhovertag='Summary'><a target='_blank' href='" + DomainURL + "/boardSummeryReport.aspx?BID?=" + TotalBoardList[i].BoardID + "'><label class='pull-right'>Summary</label><img src='Styles/BoardReport/images/summary-hiden-hover.png' class='pull-right'/></a></div></div></h3><ul><li><img src='Styles/BoardReport/images/team_grey.png' title='Team Name'><span>" + TotalBoardList[i].TeamName + "</span></li><li><img src='Styles/BoardReport/images/owner_grey.png' title='Board Owner'><span>" + TotalBoardList[i].OwnerName + "</span></li><li><img src='Styles/BoardReport/images/efforts_today.png' title='Efforts'><span>Today's Efforts</span><label style='background: #44bf53' id='BID_" + TotalBoardList[i].BoardID + "'>" + TotalBoardList[i].EffortsToday + "</label></li><li><img src='Styles/BoardReport/images/date.png' title='Create Date'><span>" + TotalBoardList[i].DueDate + "</span></li><li style='padding:0px;'><div id='progressbar" + TotalBoardList[i].BoardID + "'><span id='check' class='pblabel" + TotalBoardList[i].BoardID + " pblabel'></span><img class='Clsdelay' src='Images/delay.png' title='Delay " + TotalBoardList[i].Delay + " hrs' alt=''/></div></li><li style='padding:0px;'><div id='progressbar2" + TotalBoardList[i].BoardID + "'><span id='check1' class='pblabel2" + TotalBoardList[i].BoardID + " pblabel1'></span></div></li></ul><div class='clearfix'></div><div id='bar1' class='barfiller'><div class='tipWrap'><span class='tip'></span></div><span class='fill' data-percentage='30'></span></div></div></div> ");
                                }
                                else { $('#BordReportGrid').append("<div id=" + TotalBoardList[i].BoardID + " class='col-md-3 col-sm-6 col-xs-12 board-task-red'><div class='shadow-div'><h3 style='background:#2f77a5'>" + TotalBoardList[i].BoardName + "<div class='hover-frame'><div class='hoverdiv' myhovertag='board'><a target='_blank' href='" + DomainURL + "/board.aspx?" + TotalBoardList[i].BoardID + "'><img class='a' src='Styles/BoardReport/images/board-hiden-hover.png'/><label>Board</label></a></div> <div class='hoverdiv' myhovertag='Summary'><a target='_blank' href='" + DomainURL + "/boardSummeryReport.aspx?BID?=" + TotalBoardList[i].BoardID + "'><label class='pull-right'>Summary</label><img src='Styles/BoardReport/images/summary-hiden-hover.png' class='pull-right'/></a></div></div></h3><ul><li><img src='Styles/BoardReport/images/team_grey.png' title='Team Name'><span>" + TotalBoardList[i].TeamName + "</span></li><li><img src='Styles/BoardReport/images/owner_grey.png' title='Board Owner'><span>" + TotalBoardList[i].OwnerName + "</span></li><li><img src='Styles/BoardReport/images/efforts_today.png' title='Efforts'><span>Today's Efforts</span><label style='background: #44bf53' id='BID_" + TotalBoardList[i].BoardID + "'>" + TotalBoardList[i].EffortsToday + "</label></li><li><img src='Styles/BoardReport/images/date.png' title='Create Date'><span>" + TotalBoardList[i].DueDate + "</span></li><li style='padding:0px;'><div id='progressbar" + TotalBoardList[i].BoardID + "'><span id='check' class='pblabel" + TotalBoardList[i].BoardID + " pblabel'></span></div></li><li style='padding:0px;'><div id='progressbar2" + TotalBoardList[i].BoardID + "'><span id='check1' class='pblabel2" + TotalBoardList[i].BoardID + " pblabel1'></span></div></li></ul><div class='clearfix'></div><div id='bar1' class='barfiller'><div class='tipWrap'><span class='tip'></span></div><span class='fill' data-percentage='30'></span></div></div></div> "); }

                            }
                            else {
                                if (TotalBoardList[i].Delay > 0) {
                                    $('#BordReportGrid').append("<div id=" + TotalBoardList[i].BoardID + " class='col-md-3 col-sm-6 col-xs-12 board-task-red'><div class='shadow-div'><h3 style='background:#2f77a5'>" + TotalBoardList[i].BoardName + "<div class='hover-frame'><div class='hoverdiv' myhovertag='board'><a target='_blank' href='" + DomainURL + "/board.aspx?" + TotalBoardList[i].BoardID + "'><img class='a' src='Styles/BoardReport/images/board-hiden-hover.png'/><label>Board</label></a></div> <div class='hoverdiv' myhovertag='Summary'><a target='_blank' href='" + DomainURL + "/boardSummeryReport.aspx?BID?=" + TotalBoardList[i].BoardID + "'><label class='pull-right'>Summary</label><img src='Styles/BoardReport/images/summary-hiden-hover.png' class='pull-right'/></a></div></div></h3><ul><li><img src='Styles/BoardReport/images/team_grey.png' title='Team Name'><span>" + TotalBoardList[i].TeamName + "</span></li><li><img src='Styles/BoardReport/images/owner_grey.png' title='Board Owner'><span>" + TotalBoardList[i].OwnerName + "</span></li><li><img src='Styles/BoardReport/images/efforts_today.png' title='Efforts'><span>Today's Efforts</span><label id='BID_" + TotalBoardList[i].BoardID + "'>" + TotalBoardList[i].EffortsToday + "</label></li><li><img src='Styles/BoardReport/images/date.png' title='Create Date'><span>" + TotalBoardList[i].DueDate + "</span></li><li style='padding:0px;'><div id='progressbar" + TotalBoardList[i].BoardID + "'><span id='check' class='pblabel" + TotalBoardList[i].BoardID + " pblabel'></span><img class='Clsdelay' src='Images/delay.png' title='Delay " + TotalBoardList[i].Delay + " hrs' alt=''/></div></li><li style='padding:0px;'><div id='progressbar2" + TotalBoardList[i].BoardID + "'><span id='check1' class='pblabel2" + TotalBoardList[i].BoardID + " pblabel1'></span></div></li></ul><div class='clearfix'></div><div id='bar1' class='barfiller'><div class='tipWrap'><span class='tip'></span></div><span class='fill' data-percentage='30'></span></div></div></div> ");
                                }
                                else { $('#BordReportGrid').append("<div id=" + TotalBoardList[i].BoardID + " class='col-md-3 col-sm-6 col-xs-12 board-task-red'><div class='shadow-div'><h3 style='background:#2f77a5'>" + TotalBoardList[i].BoardName + "<div class='hover-frame'><div class='hoverdiv' myhovertag='board'><a target='_blank' href='" + DomainURL + "/board.aspx?" + TotalBoardList[i].BoardID + "'><img class='a' src='Styles/BoardReport/images/board-hiden-hover.png'/><label>Board</label></a></div> <div class='hoverdiv' myhovertag='Summary'><a target='_blank' href='" + DomainURL + "/boardSummeryReport.aspx?BID?=" + TotalBoardList[i].BoardID + "'><label class='pull-right'>Summary</label><img src='Styles/BoardReport/images/summary-hiden-hover.png' class='pull-right'/></a></div></div></h3><ul><li><img src='Styles/BoardReport/images/team_grey.png' title='Team Name'><span>" + TotalBoardList[i].TeamName + "</span></li><li><img src='Styles/BoardReport/images/owner_grey.png' title='Board Owner'><span>" + TotalBoardList[i].OwnerName + "</span></li><li><img src='Styles/BoardReport/images/efforts_today.png' title='Efforts'><span>Today's Efforts</span><label id='BID_" + TotalBoardList[i].BoardID + "'>" + TotalBoardList[i].EffortsToday + "</label></li><li><img src='Styles/BoardReport/images/date.png' title='Create Date'><span>" + TotalBoardList[i].DueDate + "</span></li><li style='padding:0px;'><div id='progressbar" + TotalBoardList[i].BoardID + "'><span id='check' class='pblabel" + TotalBoardList[i].BoardID + " pblabel'></span></div></li><li style='padding:0px;'><div id='progressbar2" + TotalBoardList[i].BoardID + "'><span id='check1' class='pblabel2" + TotalBoardList[i].BoardID + " pblabel1'></span></div></li></ul><div class='clearfix'></div><div id='bar1' class='barfiller'><div class='tipWrap'><span class='tip'></span></div><span class='fill' data-percentage='30'></span></div></div></div> "); }

                            }
                            //progress for board percentage
                            fullvalue = parseInt(TotalBoardList[i].TaskCompletePercent);
                            $('.pblabel' + TotalBoardList[i].BoardID).text(fullvalue + "%");
                            var progressbar = $('#progressbar' + TotalBoardList[i].BoardID).progressbar({
                                value: fullvalue,
                                change: function (event, ui) {
                                    var newVal = $(this).progressbar('option', 'value');
                                    var label = $('.pblabel' + TotalBoardList[i].BoardID, this).text(newVal + '%');
                                }
                            });
                            var label = progressbar.find('.pblabel' + TotalBoardList[i].BoardID).clone().width(progressbar.width());
                            progressbar.find('.ui-progressbar-value').append(label);
                            $('#progressbar' + TotalBoardList[i].BoardID).css('background', '#f5f5f5');


                            if (fullvalue != 0) {
                                $('#progressbar' + TotalBoardList[i].BoardID + ' .ui-widget-header').attr('style', 'background-color:' + TotalBoardList[i].boardcolor + ' !important; width:' + fullvalue + '%;');
                            }

                            listIndex = i + 1;


                            //progress for estimated hour percentage
                            fullvalue = parseInt(TotalBoardList[i].consumeHourPercentage);
                            consumedhour = TotalBoardList[i].consumehours + " hr " + "/ " + TotalBoardList[i].EstimatedHours + " hr ";
                            $('.pblabel2' + TotalBoardList[i].BoardID).text(consumedhour);
                            var progressbar = $('#progressbar2' + TotalBoardList[i].BoardID).progressbar({
                                value: fullvalue,
                                change: function (event, ui) {
                                    var newVal = $(this).progressbar('option', 'value');
                                    var label = $('.pblabel2' + TotalBoardList[i].BoardID, this).text(newVal + '%');
                                }
                            });

                            var label = progressbar.find('.pblabel2' + TotalBoardList[i].BoardID).clone().width(progressbar.width());
                            progressbar.find('.ui-progressbar-value').append(label);
                            $('#progressbar2' + TotalBoardList[i].BoardID).css('background', '#ededed');

                            if (fullvalue != 0) {
                                $('#progressbar2' + TotalBoardList[i].BoardID + ' .ui-widget-header').attr('style', 'background-color:#b2b2b2 !important; width:' + fullvalue + '%;');
                            }
                            $('#progressbar2' + TotalBoardList[i].BoardID + ' span').attr('style', 'padding:5px; background:#8f8f8f;margin-left: 0px;color: #fff;width: 100%;');
                            $('#progressbar2' + TotalBoardList[i].BoardID + ' div span').css('background', '#b2b2b2');
                        }
                    }
                    else {
                        for (var i = 0; i <= flag; i++) {

                            if (parseInt(TotalBoardList[i].EffortsToday) > 0) {
                                if (TotalBoardList[i].Delay > 0) {
                                    $('#BordReportGrid').append("<div id=" + TotalBoardList[i].BoardID + " class='col-md-3 col-sm-6 col-xs-12 board-task-red'><div class='shadow-div'><h3 style='background:#2f77a5'>" + TotalBoardList[i].BoardName + "<div class='hover-frame'><div class='hoverdiv' myhovertag='board'><a target='_blank' href='" + DomainURL + "/board.aspx?" + TotalBoardList[i].BoardID + "'><img class='a' src='Styles/BoardReport/images/board-hiden-hover.png'/><label>Board</label></a></div> <div class='hoverdiv' myhovertag='Summary'><a target='_blank' href='" + DomainURL + "/boardSummeryReport.aspx?BID?=" + TotalBoardList[i].BoardID + "'><label class='pull-right'>Summary</label><img src='Styles/BoardReport/images/summary-hiden-hover.png' class='pull-right'/></a></div></div></h3><ul><li><img src='Styles/BoardReport/images/team_grey.png' title='Team Name'><span>" + TotalBoardList[i].TeamName + "</span></li><li><img src='Styles/BoardReport/images/owner_grey.png' title='Board Owner'><span>" + TotalBoardList[i].OwnerName + "</span></li><li><img src='Styles/BoardReport/images/efforts_today.png' title='Efforts'><span>Today's Efforts</span><label style='background: #44bf53' id='BID_" + TotalBoardList[i].BoardID + "'>" + TotalBoardList[i].EffortsToday + "</label></li><li><img src='Styles/BoardReport/images/date.png' title='Create Date'><span>" + TotalBoardList[i].DueDate + "</span></li><li style='padding:0px;'><div id='progressbar" + TotalBoardList[i].BoardID + "'><span id='check' class='pblabel" + TotalBoardList[i].BoardID + " pblabel'></span><img class='Clsdelay' src='Images/delay.png' title='Delay " + TotalBoardList[i].Delay + " hrs' alt=''/></div></li><li style='padding:0px;'><div id='progressbar2" + TotalBoardList[i].BoardID + "'><span id='check1' class='pblabel2" + TotalBoardList[i].BoardID + " pblabel1'></span></div></li></ul><div class='clearfix'></div><div id='bar1' class='barfiller'><div class='tipWrap'><span class='tip'></span></div><span class='fill' data-percentage='30'></span></div></div></div> ");
                                }
                                else { $('#BordReportGrid').append("<div id=" + TotalBoardList[i].BoardID + " class='col-md-3 col-sm-6 col-xs-12 board-task-red'><div class='shadow-div'><h3 style='background:#2f77a5'>" + TotalBoardList[i].BoardName + "<div class='hover-frame'><div class='hoverdiv' myhovertag='board'><a target='_blank' href='" + DomainURL + "/board.aspx?" + TotalBoardList[i].BoardID + "'><img class='a' src='Styles/BoardReport/images/board-hiden-hover.png'/><label>Board</label></a></div> <div class='hoverdiv' myhovertag='Summary'><a target='_blank' href='" + DomainURL + "/boardSummeryReport.aspx?BID?=" + TotalBoardList[i].BoardID + "'><label class='pull-right'>Summary</label><img src='Styles/BoardReport/images/summary-hiden-hover.png' class='pull-right'/></a></div></div></h3><ul><li><img src='Styles/BoardReport/images/team_grey.png' title='Team Name'><span>" + TotalBoardList[i].TeamName + "</span></li><li><img src='Styles/BoardReport/images/owner_grey.png' title='Board Owner'><span>" + TotalBoardList[i].OwnerName + "</span></li><li><img src='Styles/BoardReport/images/efforts_today.png' title='Efforts'><span>Today's Efforts</span><label style='background: #44bf53' id='BID_" + TotalBoardList[i].BoardID + "'>" + TotalBoardList[i].EffortsToday + "</label></li><li><img src='Styles/BoardReport/images/date.png' title='Create Date'><span>" + TotalBoardList[i].DueDate + "</span></li><li style='padding:0px;'><div id='progressbar" + TotalBoardList[i].BoardID + "'><span id='check' class='pblabel" + TotalBoardList[i].BoardID + " pblabel'></span></div></li><li style='padding:0px;'><div id='progressbar2" + TotalBoardList[i].BoardID + "'><span id='check1' class='pblabel2" + TotalBoardList[i].BoardID + " pblabel1'></span></div></li></ul><div class='clearfix'></div><div id='bar1' class='barfiller'><div class='tipWrap'><span class='tip'></span></div><span class='fill' data-percentage='30'></span></div></div></div> "); }

                            }
                            else {
                                if (TotalBoardList[i].Delay > 0) {
                                    $('#BordReportGrid').append("<div id=" + TotalBoardList[i].BoardID + " class='col-md-3 col-sm-6 col-xs-12 board-task-red'><div class='shadow-div'><h3 style='background:#2f77a5'>" + TotalBoardList[i].BoardName + "<div class='hover-frame'><div class='hoverdiv' myhovertag='board'><a target='_blank' href='" + DomainURL + "/board.aspx?" + TotalBoardList[i].BoardID + "'><img class='a' src='Styles/BoardReport/images/board-hiden-hover.png'/><label>Board</label></a></div> <div class='hoverdiv' myhovertag='Summary'><a target='_blank' href='" + DomainURL + "/boardSummeryReport.aspx?BID?=" + TotalBoardList[i].BoardID + "'><label class='pull-right'>Summary</label><img src='Styles/BoardReport/images/summary-hiden-hover.png' class='pull-right'/></a></div></div></h3><ul><li><img src='Styles/BoardReport/images/team_grey.png' title='Team Name'><span>" + TotalBoardList[i].TeamName + "</span></li><li><img src='Styles/BoardReport/images/owner_grey.png' title='Board Owner'><span>" + TotalBoardList[i].OwnerName + "</span></li><li><img src='Styles/BoardReport/images/efforts_today.png' title='Efforts'><span>Today's Efforts</span><label id='BID_" + TotalBoardList[i].BoardID + "'>" + TotalBoardList[i].EffortsToday + "</label></li><li><img src='Styles/BoardReport/images/date.png' title='Create Date'><span>" + TotalBoardList[i].DueDate + "</span></li><li style='padding:0px;'><div id='progressbar" + TotalBoardList[i].BoardID + "'><span id='check' class='pblabel" + TotalBoardList[i].BoardID + " pblabel'></span><img class='Clsdelay' src='Images/delay.png' title='Delay " + TotalBoardList[i].Delay + " hrs' alt=''/></div></li><li style='padding:0px;'><div id='progressbar2" + TotalBoardList[i].BoardID + "'><span id='check1' class='pblabel2" + TotalBoardList[i].BoardID + " pblabel1'></span></div></li></ul><div class='clearfix'></div><div id='bar1' class='barfiller'><div class='tipWrap'><span class='tip'></span></div><span class='fill' data-percentage='30'></span></div></div></div> ");
                                }
                                else { $('#BordReportGrid').append("<div id=" + TotalBoardList[i].BoardID + " class='col-md-3 col-sm-6 col-xs-12 board-task-red'><div class='shadow-div'><h3 style='background:#2f77a5'>" + TotalBoardList[i].BoardName + "<div class='hover-frame'><div class='hoverdiv' myhovertag='board'><a target='_blank' href='" + DomainURL + "/board.aspx?" + TotalBoardList[i].BoardID + "'><img class='a' src='Styles/BoardReport/images/board-hiden-hover.png'/><label>Board</label></a></div> <div class='hoverdiv' myhovertag='Summary'><a target='_blank' href='" + DomainURL + "/boardSummeryReport.aspx?BID?=" + TotalBoardList[i].BoardID + "'><label class='pull-right'>Summary</label><img src='Styles/BoardReport/images/summary-hiden-hover.png' class='pull-right'/></a></div></div></h3><ul><li><img src='Styles/BoardReport/images/team_grey.png' title='Team Name'><span>" + TotalBoardList[i].TeamName + "</span></li><li><img src='Styles/BoardReport/images/owner_grey.png' title='Board Owner'><span>" + TotalBoardList[i].OwnerName + "</span></li><li><img src='Styles/BoardReport/images/efforts_today.png' title='Efforts'><span>Today's Efforts</span><label id='BID_" + TotalBoardList[i].BoardID + "'>" + TotalBoardList[i].EffortsToday + "</label></li><li><img src='Styles/BoardReport/images/date.png' title='Create Date'><span>" + TotalBoardList[i].DueDate + "</span></li><li style='padding:0px;'><div id='progressbar" + TotalBoardList[i].BoardID + "'><span id='check' class='pblabel" + TotalBoardList[i].BoardID + " pblabel'></span></div></li><li style='padding:0px;'><div id='progressbar2" + TotalBoardList[i].BoardID + "'><span id='check1' class='pblabel2" + TotalBoardList[i].BoardID + " pblabel1'></span></div></li></ul><div class='clearfix'></div><div id='bar1' class='barfiller'><div class='tipWrap'><span class='tip'></span></div><span class='fill' data-percentage='30'></span></div></div></div> "); }

                            }

                            //progress for board percentage
                            fullvalue = parseInt(TotalBoardList[i].TaskCompletePercent);
                            $('.pblabel' + TotalBoardList[i].BoardID).text(fullvalue + "%");
                            var progressbar = $('#progressbar' + TotalBoardList[i].BoardID).progressbar({
                                value: fullvalue,
                                change: function (event, ui) {
                                    var newVal = $(this).progressbar('option', 'value');
                                    var label = $('.pblabel' + TotalBoardList[i].BoardID, this).text(newVal + '%');
                                }
                            });
                            var label = progressbar.find('.pblabel' + TotalBoardList[i].BoardID).clone().width(progressbar.width());
                            progressbar.find('.ui-progressbar-value').append(label);
                            $('#progressbar' + TotalBoardList[i].BoardID).css('background', '#f5f5f5');


                            if (fullvalue != 0) {
                                $('#progressbar' + TotalBoardList[i].BoardID + ' .ui-widget-header').attr('style', 'background-color:' + TotalBoardList[i].boardcolor + ' !important; width:' + fullvalue + '%;');
                            }

                            listIndex = i + 1;


                            //progress for estimated hour percentage
                            fullvalue = parseInt(TotalBoardList[i].consumeHourPercentage);
                            consumedhour = TotalBoardList[i].consumehours + " hr " + "/ " + TotalBoardList[i].EstimatedHours + " hr ";
                            $('.pblabel2' + TotalBoardList[i].BoardID).text(consumedhour);
                            var progressbar = $('#progressbar2' + TotalBoardList[i].BoardID).progressbar({
                                value: fullvalue,
                                change: function (event, ui) {
                                    var newVal = $(this).progressbar('option', 'value');
                                    var label = $('.pblabel2' + TotalBoardList[i].BoardID, this).text(newVal + '%');
                                }
                            });

                            var label = progressbar.find('.pblabel2' + TotalBoardList[i].BoardID).clone().width(progressbar.width());
                            progressbar.find('.ui-progressbar-value').append(label);

                            $('#progressbar2' + TotalBoardList[i].BoardID).css('background', '#ededed');

                            if (fullvalue != 0) {
                                $('#progressbar2' + TotalBoardList[i].BoardID + ' .ui-widget-header').attr('style', 'background-color:#b2b2b2 !important; width:' + fullvalue + '%;');
                            }
                            $('#progressbar2' + TotalBoardList[i].BoardID + ' span').attr('style', 'padding:5px; background:#8f8f8f;margin-left: 0px;color: #fff;width: 100%;');
                            $('#progressbar2' + TotalBoardList[i].BoardID + ' div span').css('background', '#b2b2b2');
                        }
                    }

                    $(".tabular").append('<thead><tr class="blue noExl"><th><img src="Styles/BoardReport/images/name-wh.png">Name</th><th><img src="Styles/BoardReport/images/team-wh.png">Team</th><th><img src="Styles/BoardReport/images/owner-wh.png">Owner</th><th><img src="Styles/BoardReport/images/start_date-wh.png">Start Date</th><th><img src="Styles/BoardReport/images/end_date-wh.png">End Date</th><th><img src="Styles/BoardReport/images/efforts-wh.png">Today' + "'" + 's Efforts</th><th><img src="Styles/BoardReport/images/hours-wh.png">Hours Consumed</th><th><img src="Styles/BoardReport/images/board-wh.png">Board Progress</th><th><img src="Images/action.png">Action</th></tr></thead><tbody>');
                    if (usertypeid == "12") {
                        $.each(data.d, function (index, item) {
                            filtersAllBoardid.push(item.BoardID);
                            if (parseInt(item.EffortsToday) > 0) {
                                $('.tabular tbody').append("<tr><td style='border-left:solid 10px " + item.boardcolor + "'><a target='_blank'href=" + DomainURL + "/BoardSummeryReport.aspx?BID=" + item.BoardID + ">" + item.BoardName + "</a><span class='board-sumary-hover-cont' style='background:" + item.boardcolor + "'><a target='_blank' href='" + DomainURL + "/board.aspx?" + item.BoardID + "'><img src='Styles/BoardReport/images/board.png'></a><a target='_blank' href='" + DomainURL + "/boardSummeryReport.aspx?BID?=" + item.BoardID + "'><img src='Styles/BoardReport/images/summary.png'></a></span></td><td>" + item.TeamName + "</td><td>" + item.OwnerName + "</td><td>" + item.CreatedDate + "</td><td>" + item.DueDate + "</td><td><label style='background: #44bf53' id='BID_" + item.BoardID + "'>" + item.EffortsToday + "</label></td><td style='padding:0px;'><div id='progressbar3" + item.BoardID + "'><span id='check' class='pblabel3" + item.BoardID + " pblabel'></span></div></td><td style='padding:0px;'><div id='progressbar1" + item.BoardID + "'><span id='check' class='pblabel1" + item.BoardID + " pblabel'></span></div></td><td><img src='Images/icon1.png' class='archiveandcomplete' title='Archive Board' ArchiveTag='archive'/><img src='Images/icon2.png' title='Complete Board' ArchiveTag='complete' class='archiveandcomplete'/></td></tr>");
                            }
                            else {
                                $('.tabular tbody').append("<tr><td style='border-left:solid 10px " + item.boardcolor + "'><a target='_blank'href=" + DomainURL + "/BoardSummeryReport.aspx?BID=" + item.BoardID + ">" + item.BoardName + "</a><span class='board-sumary-hover-cont' style='background:" + item.boardcolor + "'><a target='_blank' href='" + DomainURL + "/board.aspx?" + item.BoardID + "'><img src='Styles/BoardReport/images/board.png'></a><a target='_blank' href='" + DomainURL + "/boardSummeryReport.aspx?BID?=" + item.BoardID + "'><img src='Styles/BoardReport/images/summary.png'></a></span></td><td>" + item.TeamName + "</td><td>" + item.OwnerName + "</td><td>" + item.CreatedDate + "</td><td>" + item.DueDate + "</td><td><label id='BID_" + item.BoardID + "'>" + item.EffortsToday + "</label></td><td style='padding:0px;'><div id='progressbar3" + item.BoardID + "'><span id='check' class='pblabel3" + item.BoardID + " pblabel'></span></div></td><td style='padding:0px;'><div id='progressbar1" + item.BoardID + "'><span id='check' class='pblabel1" + item.BoardID + " pblabel'></span></div></td><td><img src='Images/icon1.png' title='Archive Board' class='archiveandcomplete' ArchiveTag='archive'/><img src='Images/icon2.png' class='archiveandcomplete' title='Complete Board' ArchiveTag='complete'/></td></tr>");
                            }

                            //progress for board percentage
                            fullvalue = parseInt(item.TaskCompletePercent);
                            $('.pblabel1' + item.BoardID).text(fullvalue + "%");
                            var progressbar = $('#progressbar1' + item.BoardID).progressbar({
                                value: fullvalue,
                                change: function (event, ui) {
                                    var newVal = $(this).progressbar('option', 'value');
                                    var label = $('.pblabel1' + item.BoardID, this).text(newVal + '%');
                                }
                            });


                            if (fullvalue != 0) {
                                var label = progressbar.find('.pblabel1' + item.BoardID).width(progressbar.width());
                                progressbar.find('.ui-progressbar-value').append(label);
                                $('#progressbar1' + item.BoardID + ' .ui-widget-header').attr('style', 'background-color:' + item.boardcolor + ' !important; width:' + parseInt(fullvalue) + '%;');
                            }
                            else {
                                var label = progressbar.find('.pblabel1' + item.BoardID).clone().width(progressbar.width());
                                progressbar.find('.ui-progressbar-value').append(label);
                            }

                            $('.pblabel1' + item.BoardID).css('width', '');

                            //progress for estimated hour percentage
                            fullvalue = parseInt(item.consumeHourPercentage);
                            consumedhour = item.consumehours + " hr " + "/ " + item.EstimatedHours + " hr ";
                            $('.pblabel3' + item.BoardID).text(consumedhour);
                            var progressbar = $('#progressbar3' + item.BoardID).progressbar({
                                value: fullvalue,
                                change: function (event, ui) {
                                    var newVal = $(this).progressbar('option', 'value');
                                    var label = $('.pblabel3' + item.BoardID, this).text(newVal + '%');
                                }
                            });

                            if (fullvalue != 0) {
                                var label = progressbar.find('.pblabel3' + item.BoardID).width(progressbar.width());
                                progressbar.find('.ui-progressbar-value').append(label);
                                $('#progressbar3' + item.BoardID + ' .ui-widget-header').attr('style', 'background-color:#8f8f8f !important; width:' + parseInt(fullvalue) + '%;');
                            }
                            else {
                                var label = progressbar.find('.pblabel3' + item.BoardID).clone().width(progressbar.width());
                                progressbar.find('.ui-progressbar-value').append(label);

                            }

                            if (parseInt(fullvalue + 2) < 50) {
                                $('#progressbar3' + item.BoardID + ' div').addClass('fitdivspan');
                                $('#progressbar3' + item.BoardID + ' span').css('color', 'white');
                            }

                            if (fullvalue == 0) {
                                $('#progressbar3' + item.BoardID + ' .ui-widget-header   span').remove();
                            }
                            $('.pblabel3' + item.BoardID).css('width', '')
                            $('#progressbar3' + item.BoardID + ' div span').css('background', '#b2b2b2');
                            i++;

                        });
                    }
                    else {
                        $.each(data.d, function (index, item) {
                            filtersAllBoardid.push(item.BoardID);
                            if (item.flag == 1) {
                                if (parseInt(item.EffortsToday) > 0) {
                                    $('.tabular tbody').append("<tr><td style='border-left:solid 10px " + item.boardcolor + "'><a target='_blank'href=" + DomainURL + "/BoardSummeryReport.aspx?BID=" + item.BoardID + ">" + item.BoardName + "</a><span class='board-sumary-hover-cont' style='background:" + item.boardcolor + "'><a target='_blank' href='" + DomainURL + "/board.aspx?" + item.BoardID + "'><img src='Styles/BoardReport/images/board.png'></a><a target='_blank' href='" + DomainURL + "/boardSummeryReport.aspx?BID?=" + item.BoardID + "'><img src='Styles/BoardReport/images/summary.png'></a></span></td><td>" + item.TeamName + "</td><td>" + item.OwnerName + "</td><td>" + item.CreatedDate + "</td><td>" + item.DueDate + "</td><td><label style='background: #44bf53' id='BID_" + item.BoardID + "'>" + item.EffortsToday + "</label></td><td style='padding:0px;'><div id='progressbar3" + item.BoardID + "'><span id='check' class='pblabel3" + item.BoardID + " pblabel'></span></div></td><td style='padding:0px;'><div id='progressbar1" + item.BoardID + "'><span id='check' class='pblabel1" + item.BoardID + " pblabel'></span></div></td><td><img src='Images/icon1.png' class='archiveandcomplete' title='Archive Board' ArchiveTag='archive'/><img src='Images/icon2.png' title='Complete Board' ArchiveTag='complete' class='archiveandcomplete'/></td></tr>");
                                }
                                else {
                                    $('.tabular tbody').append("<tr><td style='border-left:solid 10px " + item.boardcolor + "'><a target='_blank'href=" + DomainURL + "/BoardSummeryReport.aspx?BID=" + item.BoardID + ">" + item.BoardName + "</a><span class='board-sumary-hover-cont' style='background:" + item.boardcolor + "'><a target='_blank' href='" + DomainURL + "/board.aspx?" + item.BoardID + "'><img src='Styles/BoardReport/images/board.png'></a><a target='_blank' href='" + DomainURL + "/boardSummeryReport.aspx?BID?=" + item.BoardID + "'><img src='Styles/BoardReport/images/summary.png'></a></span></td><td>" + item.TeamName + "</td><td>" + item.OwnerName + "</td><td>" + item.CreatedDate + "</td><td>" + item.DueDate + "</td><td><label id='BID_" + item.BoardID + "'>" + item.EffortsToday + "</label></td><td style='padding:0px;'><div id='progressbar3" + item.BoardID + "'><span id='check' class='pblabel3" + item.BoardID + " pblabel'></span></div></td><td style='padding:0px;'><div id='progressbar1" + item.BoardID + "'><span id='check' class='pblabel1" + item.BoardID + " pblabel'></span></div></td><td><img src='Images/icon1.png' title='Archive Board' class='archiveandcomplete' ArchiveTag='archive'/><img src='Images/icon2.png' class='archiveandcomplete' title='Complete Board' ArchiveTag='complete'/></td></tr>");
                                }

                            }
                            else {
                                if (parseInt(item.EffortsToday) > 0) {
                                    $('.tabular tbody').append("<tr><td style='border-left:solid 10px " + item.boardcolor + "'><a target='_blank'href=" + DomainURL + "/BoardSummeryReport.aspx?BID=" + item.BoardID + ">" + item.BoardName + "</a><span class='board-sumary-hover-cont' style='background:" + item.boardcolor + "'><a target='_blank' href='" + DomainURL + "/board.aspx?" + item.BoardID + "'><img src='Styles/BoardReport/images/board.png'></a><a target='_blank' href='" + DomainURL + "/boardSummeryReport.aspx?BID?=" + item.BoardID + "'><img src='Styles/BoardReport/images/summary.png'></a></span></td><td>" + item.TeamName + "</td><td>" + item.OwnerName + "</td><td>" + item.CreatedDate + "</td><td>" + item.DueDate + "</td><td><label style='background: #44bf53' id='BID_" + item.BoardID + "'>" + item.EffortsToday + "</label></td><td style='padding:0px;'><div id='progressbar3" + item.BoardID + "'><span id='check' class='pblabel3" + item.BoardID + " pblabel'></span></div></td><td style='padding:0px;'><div id='progressbar1" + item.BoardID + "'><span id='check' class='pblabel1" + item.BoardID + " pblabel'></span></div></td><td></td></tr>");
                                }
                                else {
                                    $('.tabular tbody').append("<tr><td style='border-left:solid 10px " + item.boardcolor + "'><a target='_blank'href=" + DomainURL + "/BoardSummeryReport.aspx?BID=" + item.BoardID + ">" + item.BoardName + "</a><span class='board-sumary-hover-cont' style='background:" + item.boardcolor + "'><a target='_blank' href='" + DomainURL + "/board.aspx?" + item.BoardID + "'><img src='Styles/BoardReport/images/board.png'></a><a target='_blank' href='" + DomainURL + "/boardSummeryReport.aspx?BID?=" + item.BoardID + "'><img src='Styles/BoardReport/images/summary.png'></a></span></td><td>" + item.TeamName + "</td><td>" + item.OwnerName + "</td><td>" + item.CreatedDate + "</td><td>" + item.DueDate + "</td><td><label id='BID_" + item.BoardID + "'>" + item.EffortsToday + "</label></td><td style='padding:0px;'><div id='progressbar3" + item.BoardID + "'><span id='check' class='pblabel3" + item.BoardID + " pblabel'></span></div></td><td style='padding:0px;'><div id='progressbar1" + item.BoardID + "'><span id='check' class='pblabel1" + item.BoardID + " pblabel'></span></div></td><td></td></tr>");
                                }

                            }

                            //progress for board percentage
                            fullvalue = parseInt(item.TaskCompletePercent);
                            $('.pblabel1' + item.BoardID).text(fullvalue + "%");
                            var progressbar = $('#progressbar1' + item.BoardID).progressbar({
                                value: fullvalue,
                                change: function (event, ui) {
                                    var newVal = $(this).progressbar('option', 'value');
                                    var label = $('.pblabel1' + item.BoardID, this).text(newVal + '%');
                                }
                            });


                            if (fullvalue != 0) {
                                var label = progressbar.find('.pblabel1' + item.BoardID).width(progressbar.width());
                                progressbar.find('.ui-progressbar-value').append(label);
                                $('#progressbar1' + item.BoardID + ' .ui-widget-header').attr('style', 'background-color:' + item.boardcolor + ' !important; width:' + parseInt(fullvalue) + '%;');
                            }
                            else {
                                var label = progressbar.find('.pblabel1' + item.BoardID).clone().width(progressbar.width());
                                progressbar.find('.ui-progressbar-value').append(label);
                            }

                            $('.pblabel1' + item.BoardID).css('width', '');

                            //progress for estimated hour percentage
                            fullvalue = parseInt(item.consumeHourPercentage);
                            consumedhour = item.consumehours + " hr " + "/ " + item.EstimatedHours + " hr ";
                            $('.pblabel3' + item.BoardID).text(consumedhour);
                            var progressbar = $('#progressbar3' + item.BoardID).progressbar({
                                value: fullvalue,
                                change: function (event, ui) {
                                    var newVal = $(this).progressbar('option', 'value');
                                    var label = $('.pblabel3' + item.BoardID, this).text(newVal + '%');
                                }
                            });

                            if (fullvalue != 0) {
                                var label = progressbar.find('.pblabel3' + item.BoardID).width(progressbar.width());
                                progressbar.find('.ui-progressbar-value').append(label);
                                $('#progressbar3' + item.BoardID + ' .ui-widget-header').attr('style', 'background-color:#8f8f8f !important; width:' + parseInt(fullvalue) + '%;');
                            }
                            else {
                                var label = progressbar.find('.pblabel3' + item.BoardID).clone().width(progressbar.width());
                                progressbar.find('.ui-progressbar-value').append(label);

                            }

                            if (parseInt(fullvalue + 2) < 50) {
                                $('#progressbar3' + item.BoardID + ' div').addClass('fitdivspan');
                                $('#progressbar3' + item.BoardID + ' span').css('color', '');
                            }

                            if (fullvalue == 0) {
                                $('#progressbar3' + item.BoardID + ' .ui-widget-header   span').remove();
                            }
                            $('.pblabel3' + item.BoardID).css('width', '')
                            $('#progressbar3' + item.BoardID + ' div span').css('background', '#b2b2b2');
                            i++;

                        });


                    }
                    $(".tabular tbody").append("</tbody>");

                    BindGrid();
                    if (currentText == "All") {
                        filtersAllBoardid.length = 0;
                    }

                }
                else {
                    $('#BordReportGrid').append("<h2>No Record Found</h2>");
                    $('#loadboard').css('display', 'none');
                }
            },
            error: function (err) {

            }
        });
    }

    if (currentText != "Favourites" && currentText != "All") {

        filtersAllBoardid.length = 0;
        TotalBoardList = "";
        flag = 19;
        listIndex = 0;
        addvaluetomultiplylist = 1;
        $('.tabular tbody').empty();
        var filter = $(this).parents('li').children("a:first").text();
        ChengeFilterIcon(filter);
        var id = $(this).closest("li").attr("id");
        $("#displayselected").empty();
        $("#displayselected").text(currentText);

        $("#displayselected").parent().css('display', 'block');
        $("#DeleteSelected").css('display', 'block');
        $(".forecast_yellow").css('display', 'none');
        $("#DeletedSelectedForcast").css('display', 'none');

        $.ajax({
            type: 'POST',
            url: 'DataTracker.svc/GetFiltersboards',
            data: "{\"id\":\"" + id + "\",\"filter\":\"" + filter + "\",\"sdate\":\"\"}",
            contentType: "application/json; charset=utf-8",
            datatype: 'json',
            async: false,
            success: function (data) {
                $('#BordReportGrid').empty();

                TotalBoardList = data.d;
                if (data.d.length > 0) {
                    var fullvalue = "";
                    $('#loadboard').css('display', 'block');
                    if ($('#BordReportGrid').css('display') == 'none') {

                        $('#loadboard').css('display', 'none');
                    }

                    $('#BordReportGrid').empty();
                    var table = $('.tabular').DataTable();
                    table.destroy();
                    $('.tabular').empty();


                    if (data.d.length < 20) {
                        $('#loadboard').css('display', 'none');
                        for (var i = 0; i < data.d.length; i++) {
                            if (parseInt(TotalBoardList[i].EffortsToday) > 0) {
                                if (TotalBoardList[i].Delay > 0) {
                                    $('#BordReportGrid').append("<div id=" + TotalBoardList[i].BoardID + " class='col-md-3 col-sm-6 col-xs-12 board-task-red'><div class='shadow-div'><h3 style='background:#2f77a5'>" + TotalBoardList[i].BoardName + "<div class='hover-frame'><div class='hoverdiv' myhovertag='board'><a target='_blank' href='" + DomainURL + "/board.aspx?" + TotalBoardList[i].BoardID + "'><img class='a' src='Styles/BoardReport/images/board-hiden-hover.png'/><label>Board</label></a></div> <div class='hoverdiv' myhovertag='Summary'><a target='_blank' href='" + DomainURL + "/boardSummeryReport.aspx?BID?=" + TotalBoardList[i].BoardID + "'><label class='pull-right'>Summary</label><img src='Styles/BoardReport/images/summary-hiden-hover.png' class='pull-right'/></a></div></div></h3><ul><li><img src='Styles/BoardReport/images/team_grey.png' title='Team Name'><span>" + TotalBoardList[i].TeamName + "</span></li><li><img src='Styles/BoardReport/images/owner_grey.png' title='Board Owner'><span>" + TotalBoardList[i].OwnerName + "</span></li><li><img src='Styles/BoardReport/images/efforts_today.png' title='Efforts'><span>Today's Efforts</span><label style='background: #44bf53' id='BID_" + TotalBoardList[i].BoardID + "'>" + TotalBoardList[i].EffortsToday + "</label></li><li><img src='Styles/BoardReport/images/date.png' title='Create Date'><span>" + TotalBoardList[i].DueDate + "</span></li><li style='padding:0px;'><div id='progressbar" + TotalBoardList[i].BoardID + "'><span id='check' class='pblabel" + TotalBoardList[i].BoardID + " pblabel'></span><img class='Clsdelay' src='Images/delay.png' title='Delay " + TotalBoardList[i].Delay + " hrs' alt=''/></div></li><li style='padding:0px;'><div id='progressbar2" + TotalBoardList[i].BoardID + "'><span id='check1' class='pblabel2" + TotalBoardList[i].BoardID + " pblabel1'></span></div></li></ul><div class='clearfix'></div><div id='bar1' class='barfiller'><div class='tipWrap'><span class='tip'></span></div><span class='fill' data-percentage='30'></span></div></div></div> ");
                                }
                                else { $('#BordReportGrid').append("<div id=" + TotalBoardList[i].BoardID + " class='col-md-3 col-sm-6 col-xs-12 board-task-red'><div class='shadow-div'><h3 style='background:#2f77a5'>" + TotalBoardList[i].BoardName + "<div class='hover-frame'><div class='hoverdiv' myhovertag='board'><a target='_blank' href='" + DomainURL + "/board.aspx?" + TotalBoardList[i].BoardID + "'><img class='a' src='Styles/BoardReport/images/board-hiden-hover.png'/><label>Board</label></a></div> <div class='hoverdiv' myhovertag='Summary'><a target='_blank' href='" + DomainURL + "/boardSummeryReport.aspx?BID?=" + TotalBoardList[i].BoardID + "'><label class='pull-right'>Summary</label><img src='Styles/BoardReport/images/summary-hiden-hover.png' class='pull-right'/></a></div></div></h3><ul><li><img src='Styles/BoardReport/images/team_grey.png' title='Team Name'><span>" + TotalBoardList[i].TeamName + "</span></li><li><img src='Styles/BoardReport/images/owner_grey.png' title='Board Owner'><span>" + TotalBoardList[i].OwnerName + "</span></li><li><img src='Styles/BoardReport/images/efforts_today.png' title='Efforts'><span>Today's Efforts</span><label style='background: #44bf53' id='BID_" + TotalBoardList[i].BoardID + "'>" + TotalBoardList[i].EffortsToday + "</label></li><li><img src='Styles/BoardReport/images/date.png' title='Create Date'><span>" + TotalBoardList[i].DueDate + "</span></li><li style='padding:0px;'><div id='progressbar" + TotalBoardList[i].BoardID + "'><span id='check' class='pblabel" + TotalBoardList[i].BoardID + " pblabel'></span></div></li><li style='padding:0px;'><div id='progressbar2" + TotalBoardList[i].BoardID + "'><span id='check1' class='pblabel2" + TotalBoardList[i].BoardID + " pblabel1'></span></div></li></ul><div class='clearfix'></div><div id='bar1' class='barfiller'><div class='tipWrap'><span class='tip'></span></div><span class='fill' data-percentage='30'></span></div></div></div> "); }

                            }
                            else {
                                if (TotalBoardList[i].Delay > 0) {
                                    $('#BordReportGrid').append("<div id=" + TotalBoardList[i].BoardID + " class='col-md-3 col-sm-6 col-xs-12 board-task-red'><div class='shadow-div'><h3 style='background:#2f77a5'>" + TotalBoardList[i].BoardName + "<div class='hover-frame'><div class='hoverdiv' myhovertag='board'><a target='_blank' href='" + DomainURL + "/board.aspx?" + TotalBoardList[i].BoardID + "'><img class='a' src='Styles/BoardReport/images/board-hiden-hover.png'/><label>Board</label></a></div> <div class='hoverdiv' myhovertag='Summary'><a target='_blank' href='" + DomainURL + "/boardSummeryReport.aspx?BID?=" + TotalBoardList[i].BoardID + "'><label class='pull-right'>Summary</label><img src='Styles/BoardReport/images/summary-hiden-hover.png' class='pull-right'/></a></div></div></h3><ul><li><img src='Styles/BoardReport/images/team_grey.png' title='Team Name'><span>" + TotalBoardList[i].TeamName + "</span></li><li><img src='Styles/BoardReport/images/owner_grey.png' title='Board Owner'><span>" + TotalBoardList[i].OwnerName + "</span></li><li><img src='Styles/BoardReport/images/efforts_today.png' title='Efforts'><span>Today's Efforts</span><label id='BID_" + TotalBoardList[i].BoardID + "'>" + TotalBoardList[i].EffortsToday + "</label></li><li><img src='Styles/BoardReport/images/date.png' title='Create Date'><span>" + TotalBoardList[i].DueDate + "</span></li><li style='padding:0px;'><div id='progressbar" + TotalBoardList[i].BoardID + "'><span id='check' class='pblabel" + TotalBoardList[i].BoardID + " pblabel'></span><img class='Clsdelay' src='Images/delay.png' title='Delay " + TotalBoardList[i].Delay + " hrs' alt=''/></div></li><li style='padding:0px;'><div id='progressbar2" + TotalBoardList[i].BoardID + "'><span id='check1' class='pblabel2" + TotalBoardList[i].BoardID + " pblabel1'></span></div></li></ul><div class='clearfix'></div><div id='bar1' class='barfiller'><div class='tipWrap'><span class='tip'></span></div><span class='fill' data-percentage='30'></span></div></div></div> ");
                                }
                                else { $('#BordReportGrid').append("<div id=" + TotalBoardList[i].BoardID + " class='col-md-3 col-sm-6 col-xs-12 board-task-red'><div class='shadow-div'><h3 style='background:#2f77a5'>" + TotalBoardList[i].BoardName + "<div class='hover-frame'><div class='hoverdiv' myhovertag='board'><a target='_blank' href='" + DomainURL + "/board.aspx?" + TotalBoardList[i].BoardID + "'><img class='a' src='Styles/BoardReport/images/board-hiden-hover.png'/><label>Board</label></a></div> <div class='hoverdiv' myhovertag='Summary'><a target='_blank' href='" + DomainURL + "/boardSummeryReport.aspx?BID?=" + TotalBoardList[i].BoardID + "'><label class='pull-right'>Summary</label><img src='Styles/BoardReport/images/summary-hiden-hover.png' class='pull-right'/></a></div></div></h3><ul><li><img src='Styles/BoardReport/images/team_grey.png' title='Team Name'><span>" + TotalBoardList[i].TeamName + "</span></li><li><img src='Styles/BoardReport/images/owner_grey.png' title='Board Owner'><span>" + TotalBoardList[i].OwnerName + "</span></li><li><img src='Styles/BoardReport/images/efforts_today.png' title='Efforts'><span>Today's Efforts</span><label id='BID_" + TotalBoardList[i].BoardID + "'>" + TotalBoardList[i].EffortsToday + "</label></li><li><img src='Styles/BoardReport/images/date.png' title='Create Date'><span>" + TotalBoardList[i].DueDate + "</span></li><li style='padding:0px;'><div id='progressbar" + TotalBoardList[i].BoardID + "'><span id='check' class='pblabel" + TotalBoardList[i].BoardID + " pblabel'></span></div></li><li style='padding:0px;'><div id='progressbar2" + TotalBoardList[i].BoardID + "'><span id='check1' class='pblabel2" + TotalBoardList[i].BoardID + " pblabel1'></span></div></li></ul><div class='clearfix'></div><div id='bar1' class='barfiller'><div class='tipWrap'><span class='tip'></span></div><span class='fill' data-percentage='30'></span></div></div></div> "); }

                            }

                            //progress for board percentage
                            fullvalue = parseInt(TotalBoardList[i].TaskCompletePercent);
                            $('.pblabel' + TotalBoardList[i].BoardID).text(fullvalue + "%");
                            var progressbar = $('#progressbar' + TotalBoardList[i].BoardID).progressbar({
                                value: fullvalue,
                                change: function (event, ui) {
                                    var newVal = $(this).progressbar('option', 'value');
                                    var label = $('.pblabel' + TotalBoardList[i].BoardID, this).text(newVal + '%');
                                }
                            });
                            var label = progressbar.find('.pblabel' + TotalBoardList[i].BoardID).clone().width(progressbar.width());
                            progressbar.find('.ui-progressbar-value').append(label);
                            $('#progressbar' + TotalBoardList[i].BoardID).css('background', '#f5f5f5');


                            if (fullvalue != 0) {
                                $('#progressbar' + TotalBoardList[i].BoardID + ' .ui-widget-header').attr('style', 'background-color:' + TotalBoardList[i].boardcolor + ' !important; width:' + fullvalue + '%;');
                            }

                            listIndex = i + 1;


                            //progress for estimated hour percentage
                            fullvalue = parseInt(TotalBoardList[i].consumeHourPercentage);
                            consumedhour = TotalBoardList[i].consumehours + " hr " + "/ " + TotalBoardList[i].EstimatedHours + " hr ";
                            $('.pblabel2' + TotalBoardList[i].BoardID).text(consumedhour);
                            var progressbar = $('#progressbar2' + TotalBoardList[i].BoardID).progressbar({
                                value: fullvalue,
                                change: function (event, ui) {
                                    var newVal = $(this).progressbar('option', 'value');
                                    var label = $('.pblabel2' + TotalBoardList[i].BoardID, this).text(newVal + '%');
                                }
                            });

                            var label = progressbar.find('.pblabel2' + TotalBoardList[i].BoardID).clone().width(progressbar.width());
                            progressbar.find('.ui-progressbar-value').append(label);

                            $('#progressbar2' + TotalBoardList[i].BoardID).css('background', '#ededed');
                            if (fullvalue != 0) {
                                $('#progressbar2' + TotalBoardList[i].BoardID + ' .ui-widget-header').attr('style', 'background-color:#b2b2b2 !important; width:' + fullvalue + '%;');
                            }
                            $('#progressbar2' + TotalBoardList[i].BoardID + ' span').attr('style', 'padding:5px; background:#8f8f8f;margin-left: 0px;color: #fff;width: 100%;');
                            $('#progressbar2' + TotalBoardList[i].BoardID + ' div span').css('background', '#b2b2b2');
                        }
                    }
                    else {
                        for (var i = 0; i <= flag; i++) {

                            if (parseInt(TotalBoardList[i].EffortsToday) > 0) {
                                if (TotalBoardList[i].Delay > 0) {
                                    $('#BordReportGrid').append("<div id=" + TotalBoardList[i].BoardID + " class='col-md-3 col-sm-6 col-xs-12 board-task-red'><div class='shadow-div'><h3 style='background:#2f77a5'>" + TotalBoardList[i].BoardName + "<div class='hover-frame'><div class='hoverdiv' myhovertag='board'><a target='_blank' href='" + DomainURL + "/board.aspx?" + TotalBoardList[i].BoardID + "'><img class='a' src='Styles/BoardReport/images/board-hiden-hover.png'/><label>Board</label></a></div> <div class='hoverdiv' myhovertag='Summary'><a target='_blank' href='" + DomainURL + "/boardSummeryReport.aspx?BID?=" + TotalBoardList[i].BoardID + "'><label class='pull-right'>Summary</label><img src='Styles/BoardReport/images/summary-hiden-hover.png' class='pull-right'/></a></div></div></h3><ul><li><img src='Styles/BoardReport/images/team_grey.png' title='Team Name'><span>" + TotalBoardList[i].TeamName + "</span></li><li><img src='Styles/BoardReport/images/owner_grey.png' title='Board Owner'><span>" + TotalBoardList[i].OwnerName + "</span></li><li><img src='Styles/BoardReport/images/efforts_today.png' title='Efforts'><span>Today's Efforts</span><label style='background: #44bf53' id='BID_" + TotalBoardList[i].BoardID + "'>" + TotalBoardList[i].EffortsToday + "</label></li><li><img src='Styles/BoardReport/images/date.png' title='Create Date'><span>" + TotalBoardList[i].DueDate + "</span></li><li style='padding:0px;'><div id='progressbar" + TotalBoardList[i].BoardID + "'><span id='check' class='pblabel" + TotalBoardList[i].BoardID + " pblabel'></span><img class='Clsdelay' src='Images/delay.png' title='Delay " + TotalBoardList[i].Delay + " hrs' alt=''/></div></li><li style='padding:0px;'><div id='progressbar2" + TotalBoardList[i].BoardID + "'><span id='check1' class='pblabel2" + TotalBoardList[i].BoardID + " pblabel1'></span></div></li></ul><div class='clearfix'></div><div id='bar1' class='barfiller'><div class='tipWrap'><span class='tip'></span></div><span class='fill' data-percentage='30'></span></div></div></div> ");
                                }
                                else { $('#BordReportGrid').append("<div id=" + TotalBoardList[i].BoardID + " class='col-md-3 col-sm-6 col-xs-12 board-task-red'><div class='shadow-div'><h3 style='background:#2f77a5'>" + TotalBoardList[i].BoardName + "<div class='hover-frame'><div class='hoverdiv' myhovertag='board'><a target='_blank' href='" + DomainURL + "/board.aspx?" + TotalBoardList[i].BoardID + "'><img class='a' src='Styles/BoardReport/images/board-hiden-hover.png'/><label>Board</label></a></div> <div class='hoverdiv' myhovertag='Summary'><a target='_blank' href='" + DomainURL + "/boardSummeryReport.aspx?BID?=" + TotalBoardList[i].BoardID + "'><label class='pull-right'>Summary</label><img src='Styles/BoardReport/images/summary-hiden-hover.png' class='pull-right'/></a></div></div></h3><ul><li><img src='Styles/BoardReport/images/team_grey.png' title='Team Name'><span>" + TotalBoardList[i].TeamName + "</span></li><li><img src='Styles/BoardReport/images/owner_grey.png' title='Board Owner'><span>" + TotalBoardList[i].OwnerName + "</span></li><li><img src='Styles/BoardReport/images/efforts_today.png' title='Efforts'><span>Today's Efforts</span><label style='background: #44bf53' id='BID_" + TotalBoardList[i].BoardID + "'>" + TotalBoardList[i].EffortsToday + "</label></li><li><img src='Styles/BoardReport/images/date.png' title='Create Date'><span>" + TotalBoardList[i].DueDate + "</span></li><li style='padding:0px;'><div id='progressbar" + TotalBoardList[i].BoardID + "'><span id='check' class='pblabel" + TotalBoardList[i].BoardID + " pblabel'></span></div></li><li style='padding:0px;'><div id='progressbar2" + TotalBoardList[i].BoardID + "'><span id='check1' class='pblabel2" + TotalBoardList[i].BoardID + " pblabel1'></span></div></li></ul><div class='clearfix'></div><div id='bar1' class='barfiller'><div class='tipWrap'><span class='tip'></span></div><span class='fill' data-percentage='30'></span></div></div></div> "); }

                            }
                            else {
                                if (TotalBoardList[i].Delay > 0) {
                                    $('#BordReportGrid').append("<div id=" + TotalBoardList[i].BoardID + " class='col-md-3 col-sm-6 col-xs-12 board-task-red'><div class='shadow-div'><h3 style='background:#2f77a5'>" + TotalBoardList[i].BoardName + "<div class='hover-frame'><div class='hoverdiv' myhovertag='board'><a target='_blank' href='" + DomainURL + "/board.aspx?" + TotalBoardList[i].BoardID + "'><img class='a' src='Styles/BoardReport/images/board-hiden-hover.png'/><label>Board</label></a></div> <div class='hoverdiv' myhovertag='Summary'><a target='_blank' href='" + DomainURL + "/boardSummeryReport.aspx?BID?=" + TotalBoardList[i].BoardID + "'><label class='pull-right'>Summary</label><img src='Styles/BoardReport/images/summary-hiden-hover.png' class='pull-right'/></a></div></div></h3><ul><li><img src='Styles/BoardReport/images/team_grey.png' title='Team Name'><span>" + TotalBoardList[i].TeamName + "</span></li><li><img src='Styles/BoardReport/images/owner_grey.png' title='Board Owner'><span>" + TotalBoardList[i].OwnerName + "</span></li><li><img src='Styles/BoardReport/images/efforts_today.png' title='Efforts'><span>Today's Efforts</span><label id='BID_" + TotalBoardList[i].BoardID + "'>" + TotalBoardList[i].EffortsToday + "</label></li><li><img src='Styles/BoardReport/images/date.png' title='Create Date'><span>" + TotalBoardList[i].DueDate + "</span></li><li style='padding:0px;'><div id='progressbar" + TotalBoardList[i].BoardID + "'><span id='check' class='pblabel" + TotalBoardList[i].BoardID + " pblabel'></span><img class='Clsdelay' src='Images/delay.png' title='Delay " + TotalBoardList[i].Delay + " hrs' alt=''/></div></li><li style='padding:0px;'><div id='progressbar2" + TotalBoardList[i].BoardID + "'><span id='check1' class='pblabel2" + TotalBoardList[i].BoardID + " pblabel1'></span></div></li></ul><div class='clearfix'></div><div id='bar1' class='barfiller'><div class='tipWrap'><span class='tip'></span></div><span class='fill' data-percentage='30'></span></div></div></div> ");
                                }
                                else { $('#BordReportGrid').append("<div id=" + TotalBoardList[i].BoardID + " class='col-md-3 col-sm-6 col-xs-12 board-task-red'><div class='shadow-div'><h3 style='background:#2f77a5'>" + TotalBoardList[i].BoardName + "<div class='hover-frame'><div class='hoverdiv' myhovertag='board'><a target='_blank' href='" + DomainURL + "/board.aspx?" + TotalBoardList[i].BoardID + "'><img class='a' src='Styles/BoardReport/images/board-hiden-hover.png'/><label>Board</label></a></div> <div class='hoverdiv' myhovertag='Summary'><a target='_blank' href='" + DomainURL + "/boardSummeryReport.aspx?BID?=" + TotalBoardList[i].BoardID + "'><label class='pull-right'>Summary</label><img src='Styles/BoardReport/images/summary-hiden-hover.png' class='pull-right'/></a></div></div></h3><ul><li><img src='Styles/BoardReport/images/team_grey.png' title='Team Name'><span>" + TotalBoardList[i].TeamName + "</span></li><li><img src='Styles/BoardReport/images/owner_grey.png' title='Board Owner'><span>" + TotalBoardList[i].OwnerName + "</span></li><li><img src='Styles/BoardReport/images/efforts_today.png' title='Efforts'><span>Today's Efforts</span><label id='BID_" + TotalBoardList[i].BoardID + "'>" + TotalBoardList[i].EffortsToday + "</label></li><li><img src='Styles/BoardReport/images/date.png' title='Create Date'><span>" + TotalBoardList[i].DueDate + "</span></li><li style='padding:0px;'><div id='progressbar" + TotalBoardList[i].BoardID + "'><span id='check' class='pblabel" + TotalBoardList[i].BoardID + " pblabel'></span></div></li><li style='padding:0px;'><div id='progressbar2" + TotalBoardList[i].BoardID + "'><span id='check1' class='pblabel2" + TotalBoardList[i].BoardID + " pblabel1'></span></div></li></ul><div class='clearfix'></div><div id='bar1' class='barfiller'><div class='tipWrap'><span class='tip'></span></div><span class='fill' data-percentage='30'></span></div></div></div> "); }

                            }
                            //progress for board percentage
                            fullvalue = parseInt(TotalBoardList[i].TaskCompletePercent);
                            $('.pblabel' + TotalBoardList[i].BoardID).text(fullvalue + "%");
                            var progressbar = $('#progressbar' + TotalBoardList[i].BoardID).progressbar({
                                value: fullvalue,
                                change: function (event, ui) {
                                    var newVal = $(this).progressbar('option', 'value');
                                    var label = $('.pblabel' + TotalBoardList[i].BoardID, this).text(newVal + '%');
                                }
                            });
                            var label = progressbar.find('.pblabel' + TotalBoardList[i].BoardID).clone().width(progressbar.width());
                            progressbar.find('.ui-progressbar-value').append(label);
                            $('#progressbar' + TotalBoardList[i].BoardID).css('background', '#f5f5f5');


                            if (fullvalue != 0) {
                                $('#progressbar' + TotalBoardList[i].BoardID + ' .ui-widget-header').attr('style', 'background-color:' + TotalBoardList[i].boardcolor + ' !important; width:' + fullvalue + '%;');
                            }

                            listIndex = i + 1;


                            //progress for estimated hour percentage
                            fullvalue = parseInt(TotalBoardList[i].consumeHourPercentage);
                            consumedhour = TotalBoardList[i].consumehours + " hr " + "/ " + TotalBoardList[i].EstimatedHours + " hr ";
                            $('.pblabel2' + TotalBoardList[i].BoardID).text(consumedhour);
                            var progressbar = $('#progressbar2' + TotalBoardList[i].BoardID).progressbar({
                                value: fullvalue,
                                change: function (event, ui) {
                                    var newVal = $(this).progressbar('option', 'value');
                                    var label = $('.pblabel2' + TotalBoardList[i].BoardID, this).text(newVal + '%');
                                }
                            });

                            var label = progressbar.find('.pblabel2' + TotalBoardList[i].BoardID).clone().width(progressbar.width());
                            progressbar.find('.ui-progressbar-value').append(label);

                            $('#progressbar2' + TotalBoardList[i].BoardID).css('background', '#ededed');
                            if (fullvalue != 0) {
                                $('#progressbar2' + TotalBoardList[i].BoardID + ' .ui-widget-header').attr('style', 'background-color:#b2b2b2 !important; width:' + fullvalue + '%;');
                            }
                            $('#progressbar2' + TotalBoardList[i].BoardID + ' span').attr('style', 'padding:5px; background:#8f8f8f;margin-left: 0px;color: #fff;width: 100%;');
                            $('#progressbar2' + TotalBoardList[i].BoardID + ' div span').css('background', '#b2b2b2');
                        }
                    }
                    $(".tabular").append('<thead><tr class="blue noExl"><th><img src="Styles/BoardReport/images/name-wh.png">Name</th><th><img src="Styles/BoardReport/images/team-wh.png">Team</th><th><img src="Styles/BoardReport/images/owner-wh.png">Owner</th><th><img src="Styles/BoardReport/images/start_date-wh.png">Start Date</th><th><img src="Styles/BoardReport/images/end_date-wh.png">End Date</th><th><img src="Styles/BoardReport/images/efforts-wh.png">Today' + "'" + 's Efforts</th><th><img src="Styles/BoardReport/images/hours-wh.png">Hours Consumed</th><th><img src="Styles/BoardReport/images/board-wh.png">Board Progress</th><th><img src="Images/action.png">Action</th></tr></thead><tbody>');
                    if (usertypeid == "12") {
                        $.each(data.d, function (index, item) {
                            filtersAllBoardid.push(item.BoardID);
                            if (parseInt(item.EffortsToday) > 0) {
                                $('.tabular tbody').append("<tr><td style='border-left:solid 10px " + item.boardcolor + "'><a target='_blank'href=" + DomainURL + "/BoardSummeryReport.aspx?BID=" + item.BoardID + ">" + item.BoardName + "</a><span class='board-sumary-hover-cont' style='background:" + item.boardcolor + "'><a target='_blank' href='" + DomainURL + "/board.aspx?" + item.BoardID + "'><img src='Styles/BoardReport/images/board.png'></a><a target='_blank' href='" + DomainURL + "/boardSummeryReport.aspx?BID?=" + item.BoardID + "'><img src='Styles/BoardReport/images/summary.png'></a></span></td><td>" + item.TeamName + "</td><td>" + item.OwnerName + "</td><td>" + item.CreatedDate + "</td><td>" + item.DueDate + "</td><td><label style='background: #44bf53' id='BID_" + item.BoardID + "'>" + item.EffortsToday + "</label></td><td style='padding:0px;'><div id='progressbar3" + item.BoardID + "'><span id='check' class='pblabel3" + item.BoardID + " pblabel'></span></div></td><td style='padding:0px;'><div id='progressbar1" + item.BoardID + "'><span id='check' class='pblabel1" + item.BoardID + " pblabel'></span></div></td><td><img src='Images/icon1.png' class='archiveandcomplete' title='Archive Board' ArchiveTag='archive'/><img src='Images/icon2.png' title='Complete Board' ArchiveTag='complete' class='archiveandcomplete'/></td></tr>");
                            }
                            else {
                                $('.tabular tbody').append("<tr><td style='border-left:solid 10px " + item.boardcolor + "'><a target='_blank'href=" + DomainURL + "/BoardSummeryReport.aspx?BID=" + item.BoardID + ">" + item.BoardName + "</a><span class='board-sumary-hover-cont' style='background:" + item.boardcolor + "'><a target='_blank' href='" + DomainURL + "/board.aspx?" + item.BoardID + "'><img src='Styles/BoardReport/images/board.png'></a><a target='_blank' href='" + DomainURL + "/boardSummeryReport.aspx?BID?=" + item.BoardID + "'><img src='Styles/BoardReport/images/summary.png'></a></span></td><td>" + item.TeamName + "</td><td>" + item.OwnerName + "</td><td>" + item.CreatedDate + "</td><td>" + item.DueDate + "</td><td><label id='BID_" + item.BoardID + "'>" + item.EffortsToday + "</label></td><td style='padding:0px;'><div id='progressbar3" + item.BoardID + "'><span id='check' class='pblabel3" + item.BoardID + " pblabel'></span></div></td><td style='padding:0px;'><div id='progressbar1" + item.BoardID + "'><span id='check' class='pblabel1" + item.BoardID + " pblabel'></span></div></td><td><img src='Images/icon1.png' title='Archive Board' class='archiveandcomplete' ArchiveTag='archive'/><img src='Images/icon2.png' class='archiveandcomplete' title='Complete Board' ArchiveTag='complete'/></td></tr>");
                            }

                            //progress for board percentage
                            fullvalue = parseInt(item.TaskCompletePercent);
                            $('.pblabel1' + item.BoardID).text(fullvalue + "%");
                            var progressbar = $('#progressbar1' + item.BoardID).progressbar({
                                value: fullvalue,
                                change: function (event, ui) {
                                    var newVal = $(this).progressbar('option', 'value');
                                    var label = $('.pblabel1' + item.BoardID, this).text(newVal + '%');
                                }
                            });


                            if (fullvalue != 0) {
                                var label = progressbar.find('.pblabel1' + item.BoardID).width(progressbar.width());
                                progressbar.find('.ui-progressbar-value').append(label);
                                $('#progressbar1' + item.BoardID + ' .ui-widget-header').attr('style', 'background-color:' + item.boardcolor + ' !important; width:' + parseInt(fullvalue) + '%;');
                            }
                            else {
                                var label = progressbar.find('.pblabel1' + item.BoardID).clone().width(progressbar.width());
                                progressbar.find('.ui-progressbar-value').append(label);
                            }

                            $('.pblabel1' + item.BoardID).css('width', '');

                            //progress for estimated hour percentage
                            fullvalue = parseInt(item.consumeHourPercentage);
                            consumedhour = item.consumehours + " hr " + "/ " + item.EstimatedHours + " hr ";
                            $('.pblabel3' + item.BoardID).text(consumedhour);
                            var progressbar = $('#progressbar3' + item.BoardID).progressbar({
                                value: fullvalue,
                                change: function (event, ui) {
                                    var newVal = $(this).progressbar('option', 'value');
                                    var label = $('.pblabel3' + item.BoardID, this).text(newVal + '%');
                                }
                            });

                            if (fullvalue != 0) {
                                var label = progressbar.find('.pblabel3' + item.BoardID).width(progressbar.width());
                                progressbar.find('.ui-progressbar-value').append(label);
                                $('#progressbar3' + item.BoardID + ' .ui-widget-header').attr('style', 'background-color:#8f8f8f !important; width:' + parseInt(fullvalue) + '%;');
                            }
                            else {
                                var label = progressbar.find('.pblabel3' + item.BoardID).clone().width(progressbar.width());
                                progressbar.find('.ui-progressbar-value').append(label);

                            }

                            if (parseInt(fullvalue + 2) < 50) {
                                $('#progressbar3' + item.BoardID + ' div').addClass('fitdivspan');
                                $('#progressbar3' + item.BoardID + ' span').css('color', 'white');
                            }

                            if (fullvalue == 0) {
                                $('#progressbar3' + item.BoardID + ' .ui-widget-header   span').remove();
                            }
                            $('.pblabel3' + item.BoardID).css('width', '')
                            $('#progressbar3' + item.BoardID + ' div span').css('background', '#b2b2b2');
                            i++;

                        });
                    }
                    else {
                        $.each(data.d, function (index, item) {
                            filtersAllBoardid.push(item.BoardID);
                            if (item.flag == 1) {
                                if (parseInt(item.EffortsToday) > 0) {
                                    $('.tabular tbody').append("<tr><td style='border-left:solid 10px " + item.boardcolor + "'><a target='_blank'href=" + DomainURL + "/BoardSummeryReport.aspx?BID=" + item.BoardID + ">" + item.BoardName + "</a><span class='board-sumary-hover-cont' style='background:" + item.boardcolor + "'><a target='_blank' href='" + DomainURL + "/board.aspx?" + item.BoardID + "'><img src='Styles/BoardReport/images/board.png'></a><a target='_blank' href='" + DomainURL + "/boardSummeryReport.aspx?BID?=" + item.BoardID + "'><img src='Styles/BoardReport/images/summary.png'></a></span></td><td>" + item.TeamName + "</td><td>" + item.OwnerName + "</td><td>" + item.CreatedDate + "</td><td>" + item.DueDate + "</td><td><label style='background: #44bf53' id='BID_" + item.BoardID + "'>" + item.EffortsToday + "</label></td><td style='padding:0px;'><div id='progressbar3" + item.BoardID + "'><span id='check' class='pblabel3" + item.BoardID + " pblabel'></span></div></td><td style='padding:0px;'><div id='progressbar1" + item.BoardID + "'><span id='check' class='pblabel1" + item.BoardID + " pblabel'></span></div></td><td><img src='Images/icon1.png' class='archiveandcomplete' title='Archive Board' ArchiveTag='archive'/><img src='Images/icon2.png' title='Complete Board' ArchiveTag='complete' class='archiveandcomplete'/></td></tr>");
                                }
                                else {
                                    $('.tabular tbody').append("<tr><td style='border-left:solid 10px " + item.boardcolor + "'><a target='_blank'href=" + DomainURL + "/BoardSummeryReport.aspx?BID=" + item.BoardID + ">" + item.BoardName + "</a><span class='board-sumary-hover-cont' style='background:" + item.boardcolor + "'><a target='_blank' href='" + DomainURL + "/board.aspx?" + item.BoardID + "'><img src='Styles/BoardReport/images/board.png'></a><a target='_blank' href='" + DomainURL + "/boardSummeryReport.aspx?BID?=" + item.BoardID + "'><img src='Styles/BoardReport/images/summary.png'></a></span></td><td>" + item.TeamName + "</td><td>" + item.OwnerName + "</td><td>" + item.CreatedDate + "</td><td>" + item.DueDate + "</td><td><label id='BID_" + item.BoardID + "'>" + item.EffortsToday + "</label></td><td style='padding:0px;'><div id='progressbar3" + item.BoardID + "'><span id='check' class='pblabel3" + item.BoardID + " pblabel'></span></div></td><td style='padding:0px;'><div id='progressbar1" + item.BoardID + "'><span id='check' class='pblabel1" + item.BoardID + " pblabel'></span></div></td><td><img src='Images/icon1.png' title='Archive Board' class='archiveandcomplete' ArchiveTag='archive'/><img src='Images/icon2.png' class='archiveandcomplete' title='Complete Board' ArchiveTag='complete'/></td></tr>");
                                }

                            }
                            else {
                                if (parseInt(item.EffortsToday) > 0) {
                                    $('.tabular tbody').append("<tr><td style='border-left:solid 10px " + item.boardcolor + "'><a target='_blank'href=" + DomainURL + "/BoardSummeryReport.aspx?BID=" + item.BoardID + ">" + item.BoardName + "</a><span class='board-sumary-hover-cont' style='background:" + item.boardcolor + "'><a target='_blank' href='" + DomainURL + "/board.aspx?" + item.BoardID + "'><img src='Styles/BoardReport/images/board.png'></a><a target='_blank' href='" + DomainURL + "/boardSummeryReport.aspx?BID?=" + item.BoardID + "'><img src='Styles/BoardReport/images/summary.png'></a></span></td><td>" + item.TeamName + "</td><td>" + item.OwnerName + "</td><td>" + item.CreatedDate + "</td><td>" + item.DueDate + "</td><td><label style='background: #44bf53' id='BID_" + item.BoardID + "'>" + item.EffortsToday + "</label></td><td style='padding:0px;'><div id='progressbar3" + item.BoardID + "'><span id='check' class='pblabel3" + item.BoardID + " pblabel'></span></div></td><td style='padding:0px;'><div id='progressbar1" + item.BoardID + "'><span id='check' class='pblabel1" + item.BoardID + " pblabel'></span></div></td><td></td></tr>");
                                }
                                else {
                                    $('.tabular tbody').append("<tr><td style='border-left:solid 10px " + item.boardcolor + "'><a target='_blank'href=" + DomainURL + "/BoardSummeryReport.aspx?BID=" + item.BoardID + ">" + item.BoardName + "</a><span class='board-sumary-hover-cont' style='background:" + item.boardcolor + "'><a target='_blank' href='" + DomainURL + "/board.aspx?" + item.BoardID + "'><img src='Styles/BoardReport/images/board.png'></a><a target='_blank' href='" + DomainURL + "/boardSummeryReport.aspx?BID?=" + item.BoardID + "'><img src='Styles/BoardReport/images/summary.png'></a></span></td><td>" + item.TeamName + "</td><td>" + item.OwnerName + "</td><td>" + item.CreatedDate + "</td><td>" + item.DueDate + "</td><td><label id='BID_" + item.BoardID + "'>" + item.EffortsToday + "</label></td><td style='padding:0px;'><div id='progressbar3" + item.BoardID + "'><span id='check' class='pblabel3" + item.BoardID + " pblabel'></span></div></td><td style='padding:0px;'><div id='progressbar1" + item.BoardID + "'><span id='check' class='pblabel1" + item.BoardID + " pblabel'></span></div></td><td></td></tr>");
                                }

                            }

                            //progress for board percentage
                            fullvalue = parseInt(item.TaskCompletePercent);
                            $('.pblabel1' + item.BoardID).text(fullvalue + "%");
                            var progressbar = $('#progressbar1' + item.BoardID).progressbar({
                                value: fullvalue,
                                change: function (event, ui) {
                                    var newVal = $(this).progressbar('option', 'value');
                                    var label = $('.pblabel1' + item.BoardID, this).text(newVal + '%');
                                }
                            });


                            if (fullvalue != 0) {
                                var label = progressbar.find('.pblabel1' + item.BoardID).width(progressbar.width());
                                progressbar.find('.ui-progressbar-value').append(label);
                                $('#progressbar1' + item.BoardID + ' .ui-widget-header').attr('style', 'background-color:' + item.boardcolor + ' !important; width:' + parseInt(fullvalue) + '%;');
                            }
                            else {
                                var label = progressbar.find('.pblabel1' + item.BoardID).clone().width(progressbar.width());
                                progressbar.find('.ui-progressbar-value').append(label);
                            }

                            $('.pblabel1' + item.BoardID).css('width', '');

                            //progress for estimated hour percentage
                            fullvalue = parseInt(item.consumeHourPercentage);
                            consumedhour = item.consumehours + " hr " + "/ " + item.EstimatedHours + " hr ";
                            $('.pblabel3' + item.BoardID).text(consumedhour);
                            var progressbar = $('#progressbar3' + item.BoardID).progressbar({
                                value: fullvalue,
                                change: function (event, ui) {
                                    var newVal = $(this).progressbar('option', 'value');
                                    var label = $('.pblabel3' + item.BoardID, this).text(newVal + '%');
                                }
                            });

                            if (fullvalue != 0) {
                                var label = progressbar.find('.pblabel3' + item.BoardID).width(progressbar.width());
                                progressbar.find('.ui-progressbar-value').append(label);
                                $('#progressbar3' + item.BoardID + ' .ui-widget-header').attr('style', 'background-color:#8f8f8f !important; width:' + parseInt(fullvalue) + '%;');
                            }
                            else {
                                var label = progressbar.find('.pblabel3' + item.BoardID).clone().width(progressbar.width());
                                progressbar.find('.ui-progressbar-value').append(label);

                            }

                            if (parseInt(fullvalue + 2) < 50) {
                                $('#progressbar3' + item.BoardID + ' div').addClass('fitdivspan');
                                $('#progressbar3' + item.BoardID + ' span').css('color', '');
                            }

                            if (fullvalue == 0) {
                                $('#progressbar3' + item.BoardID + ' .ui-widget-header   span').remove();
                            }
                            $('.pblabel3' + item.BoardID).css('width', '')
                            $('#progressbar3' + item.BoardID + ' div span').css('background', '#b2b2b2');
                            i++;

                        });


                    }
                    $(".tabular tbody").append("</tbody>");
                    BindGrid();
                    if (currentText == "All") {
                        filtersAllBoardid.length = 0;
                    }

                }
                else {
                    $('#BordReportGrid').append("<h2>No Record Found</h2>");
                    $('#loadboard').css('display', 'none');
                }
            },
            error: function (err) {

            }
        });
    }
    $('.hover-frame').slideUp();
});



$("#DeleteSelected").live("click", function () {
    $("#displayselected").parent().css('display', 'none');
    $("#DeleteSelected").css('display', 'none');
    $(".forecast_yellow").css('display', 'none');
    $("#DeletedSelectedForcast").css('display', 'none');
    TotalBoardOnReport();
});

$("#DeletedSelectedForcast").live("click", function () {

    $(".forecast_yellow").css('display', 'none');
    $("#DeletedSelectedForcast").css('display', 'none');

    TotalBoardList = "";
    flag = 19;
    listIndex = 0;
    addvaluetomultiplylist = 1;

    $.ajax({
        type: 'POST',
        url: 'DataTracker.svc/RemoveForcars',
        data: JSON.stringify({ boardies: filtersAllBoardid }),
        contentType: "application/json; charset=utf-8",
        datatype: 'json',
        async: false,
        success: function (data) {
            $('#BordReportGrid').empty();

            TotalBoardList = data.d;
            if (data.d.length > 0) {
                var fullvalue = "";
                $('#loadboard').css('display', 'block');
                if ($('#BordReportGrid').css('display') == 'none') {

                    $('#loadboard').css('display', 'none');
                }

                $('#BordReportGrid').empty();
                var table = $('.tabular').DataTable();
                table.destroy();
                $('.tabular').empty();

                if (data.d.length < 20) {
                    $('#loadboard').css('display', 'none');
                    for (var i = 0; i < data.d.length; i++) {

                        if (parseInt(TotalBoardList[i].EffortsToday) > 0) {
                            if (TotalBoardList[i].Delay > 0) {
                                $('#BordReportGrid').append("<div id=" + TotalBoardList[i].BoardID + " class='col-md-3 col-sm-6 col-xs-12 board-task-red'><div class='shadow-div'><h3 style='background:#2f77a5'>" + TotalBoardList[i].BoardName + "<div class='hover-frame'><div class='hoverdiv' myhovertag='board'><a target='_blank' href='" + DomainURL + "/board.aspx?" + TotalBoardList[i].BoardID + "'><img class='a' src='Styles/BoardReport/images/board-hiden-hover.png'/><label>Board</label></a></div> <div class='hoverdiv' myhovertag='Summary'><a target='_blank' href='" + DomainURL + "/boardSummeryReport.aspx?BID?=" + TotalBoardList[i].BoardID + "'><label class='pull-right'>Summary</label><img src='Styles/BoardReport/images/summary-hiden-hover.png' class='pull-right'/></a></div></div></h3><ul><li><img src='Styles/BoardReport/images/team_grey.png' title='Team Name'><span>" + TotalBoardList[i].TeamName + "</span></li><li><img src='Styles/BoardReport/images/owner_grey.png' title='Board Owner'><span>" + TotalBoardList[i].OwnerName + "</span></li><li><img src='Styles/BoardReport/images/efforts_today.png' title='Efforts'><span>Today's Efforts</span><label style='background: #44bf53' id='BID_" + TotalBoardList[i].BoardID + "'>" + TotalBoardList[i].EffortsToday + "</label></li><li><img src='Styles/BoardReport/images/date.png' title='Create Date'><span>" + TotalBoardList[i].DueDate + "</span></li><li style='padding:0px;'><div id='progressbar" + TotalBoardList[i].BoardID + "'><span id='check' class='pblabel" + TotalBoardList[i].BoardID + " pblabel'></span><img class='Clsdelay' src='Images/delay.png' title='Delay " + TotalBoardList[i].Delay + " hrs' alt=''/></div></li><li style='padding:0px;'><div id='progressbar2" + TotalBoardList[i].BoardID + "'><span id='check1' class='pblabel2" + TotalBoardList[i].BoardID + " pblabel1'></span></div></li></ul><div class='clearfix'></div><div id='bar1' class='barfiller'><div class='tipWrap'><span class='tip'></span></div><span class='fill' data-percentage='30'></span></div></div></div> ");
                            }
                            else { $('#BordReportGrid').append("<div id=" + TotalBoardList[i].BoardID + " class='col-md-3 col-sm-6 col-xs-12 board-task-red'><div class='shadow-div'><h3 style='background:#2f77a5'>" + TotalBoardList[i].BoardName + "<div class='hover-frame'><div class='hoverdiv' myhovertag='board'><a target='_blank' href='" + DomainURL + "/board.aspx?" + TotalBoardList[i].BoardID + "'><img class='a' src='Styles/BoardReport/images/board-hiden-hover.png'/><label>Board</label></a></div> <div class='hoverdiv' myhovertag='Summary'><a target='_blank' href='" + DomainURL + "/boardSummeryReport.aspx?BID?=" + TotalBoardList[i].BoardID + "'><label class='pull-right'>Summary</label><img src='Styles/BoardReport/images/summary-hiden-hover.png' class='pull-right'/></a></div></div></h3><ul><li><img src='Styles/BoardReport/images/team_grey.png' title='Team Name'><span>" + TotalBoardList[i].TeamName + "</span></li><li><img src='Styles/BoardReport/images/owner_grey.png' title='Board Owner'><span>" + TotalBoardList[i].OwnerName + "</span></li><li><img src='Styles/BoardReport/images/efforts_today.png' title='Efforts'><span>Today's Efforts</span><label style='background: #44bf53' id='BID_" + TotalBoardList[i].BoardID + "'>" + TotalBoardList[i].EffortsToday + "</label></li><li><img src='Styles/BoardReport/images/date.png' title='Create Date'><span>" + TotalBoardList[i].DueDate + "</span></li><li style='padding:0px;'><div id='progressbar" + TotalBoardList[i].BoardID + "'><span id='check' class='pblabel" + TotalBoardList[i].BoardID + " pblabel'></span></div></li><li style='padding:0px;'><div id='progressbar2" + TotalBoardList[i].BoardID + "'><span id='check1' class='pblabel2" + TotalBoardList[i].BoardID + " pblabel1'></span></div></li></ul><div class='clearfix'></div><div id='bar1' class='barfiller'><div class='tipWrap'><span class='tip'></span></div><span class='fill' data-percentage='30'></span></div></div></div> "); }

                        }
                        else {
                            if (TotalBoardList[i].Delay > 0) {
                                $('#BordReportGrid').append("<div id=" + TotalBoardList[i].BoardID + " class='col-md-3 col-sm-6 col-xs-12 board-task-red'><div class='shadow-div'><h3 style='background:#2f77a5'>" + TotalBoardList[i].BoardName + "<div class='hover-frame'><div class='hoverdiv' myhovertag='board'><a target='_blank' href='" + DomainURL + "/board.aspx?" + TotalBoardList[i].BoardID + "'><img class='a' src='Styles/BoardReport/images/board-hiden-hover.png'/><label>Board</label></a></div> <div class='hoverdiv' myhovertag='Summary'><a target='_blank' href='" + DomainURL + "/boardSummeryReport.aspx?BID?=" + TotalBoardList[i].BoardID + "'><label class='pull-right'>Summary</label><img src='Styles/BoardReport/images/summary-hiden-hover.png' class='pull-right'/></a></div></div></h3><ul><li><img src='Styles/BoardReport/images/team_grey.png' title='Team Name'><span>" + TotalBoardList[i].TeamName + "</span></li><li><img src='Styles/BoardReport/images/owner_grey.png' title='Board Owner'><span>" + TotalBoardList[i].OwnerName + "</span></li><li><img src='Styles/BoardReport/images/efforts_today.png' title='Efforts'><span>Today's Efforts</span><label id='BID_" + TotalBoardList[i].BoardID + "'>" + TotalBoardList[i].EffortsToday + "</label></li><li><img src='Styles/BoardReport/images/date.png' title='Create Date'><span>" + TotalBoardList[i].DueDate + "</span></li><li style='padding:0px;'><div id='progressbar" + TotalBoardList[i].BoardID + "'><span id='check' class='pblabel" + TotalBoardList[i].BoardID + " pblabel'></span><img class='Clsdelay' src='Images/delay.png' title='Delay " + TotalBoardList[i].Delay + " hrs' alt=''/></div></li><li style='padding:0px;'><div id='progressbar2" + TotalBoardList[i].BoardID + "'><span id='check1' class='pblabel2" + TotalBoardList[i].BoardID + " pblabel1'></span></div></li></ul><div class='clearfix'></div><div id='bar1' class='barfiller'><div class='tipWrap'><span class='tip'></span></div><span class='fill' data-percentage='30'></span></div></div></div> ");
                            }
                            else { $('#BordReportGrid').append("<div id=" + TotalBoardList[i].BoardID + " class='col-md-3 col-sm-6 col-xs-12 board-task-red'><div class='shadow-div'><h3 style='background:#2f77a5'>" + TotalBoardList[i].BoardName + "<div class='hover-frame'><div class='hoverdiv' myhovertag='board'><a target='_blank' href='" + DomainURL + "/board.aspx?" + TotalBoardList[i].BoardID + "'><img class='a' src='Styles/BoardReport/images/board-hiden-hover.png'/><label>Board</label></a></div> <div class='hoverdiv' myhovertag='Summary'><a target='_blank' href='" + DomainURL + "/boardSummeryReport.aspx?BID?=" + TotalBoardList[i].BoardID + "'><label class='pull-right'>Summary</label><img src='Styles/BoardReport/images/summary-hiden-hover.png' class='pull-right'/></a></div></div></h3><ul><li><img src='Styles/BoardReport/images/team_grey.png' title='Team Name'><span>" + TotalBoardList[i].TeamName + "</span></li><li><img src='Styles/BoardReport/images/owner_grey.png' title='Board Owner'><span>" + TotalBoardList[i].OwnerName + "</span></li><li><img src='Styles/BoardReport/images/efforts_today.png' title='Efforts'><span>Today's Efforts</span><label id='BID_" + TotalBoardList[i].BoardID + "'>" + TotalBoardList[i].EffortsToday + "</label></li><li><img src='Styles/BoardReport/images/date.png' title='Create Date'><span>" + TotalBoardList[i].DueDate + "</span></li><li style='padding:0px;'><div id='progressbar" + TotalBoardList[i].BoardID + "'><span id='check' class='pblabel" + TotalBoardList[i].BoardID + " pblabel'></span></div></li><li style='padding:0px;'><div id='progressbar2" + TotalBoardList[i].BoardID + "'><span id='check1' class='pblabel2" + TotalBoardList[i].BoardID + " pblabel1'></span></div></li></ul><div class='clearfix'></div><div id='bar1' class='barfiller'><div class='tipWrap'><span class='tip'></span></div><span class='fill' data-percentage='30'></span></div></div></div> "); }

                        }

                        //progress for board percentage
                        fullvalue = parseInt(TotalBoardList[i].TaskCompletePercent);
                        $('.pblabel' + TotalBoardList[i].BoardID).text(fullvalue + "%");
                        var progressbar = $('#progressbar' + TotalBoardList[i].BoardID).progressbar({
                            value: fullvalue,
                            change: function (event, ui) {
                                var newVal = $(this).progressbar('option', 'value');
                                var label = $('.pblabel' + TotalBoardList[i].BoardID, this).text(newVal + '%');
                            }
                        });
                        var label = progressbar.find('.pblabel' + TotalBoardList[i].BoardID).clone().width(progressbar.width());
                        progressbar.find('.ui-progressbar-value').append(label);
                        $('#progressbar' + TotalBoardList[i].BoardID).css('background', '#f5f5f5');


                        if (fullvalue != 0) {
                            $('#progressbar' + TotalBoardList[i].BoardID + ' .ui-widget-header').attr('style', 'background-color:' + TotalBoardList[i].boardcolor + ' !important; width:' + fullvalue + '%;');
                        }

                        listIndex = i + 1;


                        //progress for estimated hour percentage
                        fullvalue = parseInt(TotalBoardList[i].consumeHourPercentage);
                        consumedhour = TotalBoardList[i].consumehours + " hr " + "/ " + TotalBoardList[i].EstimatedHours + " hr ";
                        $('.pblabel2' + TotalBoardList[i].BoardID).text(consumedhour);
                        var progressbar = $('#progressbar2' + TotalBoardList[i].BoardID).progressbar({
                            value: fullvalue,
                            change: function (event, ui) {
                                var newVal = $(this).progressbar('option', 'value');
                                var label = $('.pblabel2' + TotalBoardList[i].BoardID, this).text(newVal + '%');
                            }
                        });

                        var label = progressbar.find('.pblabel2' + TotalBoardList[i].BoardID).clone().width(progressbar.width());
                        progressbar.find('.ui-progressbar-value').append(label);
                        $('#progressbar2' + TotalBoardList[i].BoardID).css('background', '#ededed');

                        if (fullvalue != 0) {
                            $('#progressbar2' + TotalBoardList[i].BoardID + ' .ui-widget-header').attr('style', 'background-color:#b2b2b2 !important; width:' + fullvalue + '%;');
                        }
                        $('#progressbar2' + TotalBoardList[i].BoardID + ' span').attr('style', 'padding:5px; background:#8f8f8f;margin-left: 0px;color: #fff;width: 100%;');
                        $('#progressbar2' + TotalBoardList[i].BoardID + ' div span').css('background', '#b2b2b2');
                    }
                }
                else {
                    for (var i = 0; i <= flag; i++) {

                        if (parseInt(TotalBoardList[i].EffortsToday) > 0) {
                            if (TotalBoardList[i].Delay > 0) {
                                $('#BordReportGrid').append("<div id=" + TotalBoardList[i].BoardID + " class='col-md-3 col-sm-6 col-xs-12 board-task-red'><div class='shadow-div'><h3 style='background:#2f77a5'>" + TotalBoardList[i].BoardName + "<div class='hover-frame'><div class='hoverdiv' myhovertag='board'><a target='_blank' href='" + DomainURL + "/board.aspx?" + TotalBoardList[i].BoardID + "'><img class='a' src='Styles/BoardReport/images/board-hiden-hover.png'/><label>Board</label></a></div> <div class='hoverdiv' myhovertag='Summary'><a target='_blank' href='" + DomainURL + "/boardSummeryReport.aspx?BID?=" + TotalBoardList[i].BoardID + "'><label class='pull-right'>Summary</label><img src='Styles/BoardReport/images/summary-hiden-hover.png' class='pull-right'/></a></div></div></h3><ul><li><img src='Styles/BoardReport/images/team_grey.png' title='Team Name'><span>" + TotalBoardList[i].TeamName + "</span></li><li><img src='Styles/BoardReport/images/owner_grey.png' title='Board Owner'><span>" + TotalBoardList[i].OwnerName + "</span></li><li><img src='Styles/BoardReport/images/efforts_today.png' title='Efforts'><span>Today's Efforts</span><label style='background: #44bf53' id='BID_" + TotalBoardList[i].BoardID + "'>" + TotalBoardList[i].EffortsToday + "</label></li><li><img src='Styles/BoardReport/images/date.png' title='Create Date'><span>" + TotalBoardList[i].DueDate + "</span></li><li style='padding:0px;'><div id='progressbar" + TotalBoardList[i].BoardID + "'><span id='check' class='pblabel" + TotalBoardList[i].BoardID + " pblabel'></span><img class='Clsdelay' src='Images/delay.png' title='Delay " + TotalBoardList[i].Delay + " hrs' alt=''/></div></li><li style='padding:0px;'><div id='progressbar2" + TotalBoardList[i].BoardID + "'><span id='check1' class='pblabel2" + TotalBoardList[i].BoardID + " pblabel1'></span></div></li></ul><div class='clearfix'></div><div id='bar1' class='barfiller'><div class='tipWrap'><span class='tip'></span></div><span class='fill' data-percentage='30'></span></div></div></div> ");
                            }
                            else { $('#BordReportGrid').append("<div id=" + TotalBoardList[i].BoardID + " class='col-md-3 col-sm-6 col-xs-12 board-task-red'><div class='shadow-div'><h3 style='background:#2f77a5'>" + TotalBoardList[i].BoardName + "<div class='hover-frame'><div class='hoverdiv' myhovertag='board'><a target='_blank' href='" + DomainURL + "/board.aspx?" + TotalBoardList[i].BoardID + "'><img class='a' src='Styles/BoardReport/images/board-hiden-hover.png'/><label>Board</label></a></div> <div class='hoverdiv' myhovertag='Summary'><a target='_blank' href='" + DomainURL + "/boardSummeryReport.aspx?BID?=" + TotalBoardList[i].BoardID + "'><label class='pull-right'>Summary</label><img src='Styles/BoardReport/images/summary-hiden-hover.png' class='pull-right'/></a></div></div></h3><ul><li><img src='Styles/BoardReport/images/team_grey.png' title='Team Name'><span>" + TotalBoardList[i].TeamName + "</span></li><li><img src='Styles/BoardReport/images/owner_grey.png' title='Board Owner'><span>" + TotalBoardList[i].OwnerName + "</span></li><li><img src='Styles/BoardReport/images/efforts_today.png' title='Efforts'><span>Today's Efforts</span><label style='background: #44bf53' id='BID_" + TotalBoardList[i].BoardID + "'>" + TotalBoardList[i].EffortsToday + "</label></li><li><img src='Styles/BoardReport/images/date.png' title='Create Date'><span>" + TotalBoardList[i].DueDate + "</span></li><li style='padding:0px;'><div id='progressbar" + TotalBoardList[i].BoardID + "'><span id='check' class='pblabel" + TotalBoardList[i].BoardID + " pblabel'></span></div></li><li style='padding:0px;'><div id='progressbar2" + TotalBoardList[i].BoardID + "'><span id='check1' class='pblabel2" + TotalBoardList[i].BoardID + " pblabel1'></span></div></li></ul><div class='clearfix'></div><div id='bar1' class='barfiller'><div class='tipWrap'><span class='tip'></span></div><span class='fill' data-percentage='30'></span></div></div></div> "); }

                        }
                        else {
                            if (TotalBoardList[i].Delay > 0) {
                                $('#BordReportGrid').append("<div id=" + TotalBoardList[i].BoardID + " class='col-md-3 col-sm-6 col-xs-12 board-task-red'><div class='shadow-div'><h3 style='background:#2f77a5'>" + TotalBoardList[i].BoardName + "<div class='hover-frame'><div class='hoverdiv' myhovertag='board'><a target='_blank' href='" + DomainURL + "/board.aspx?" + TotalBoardList[i].BoardID + "'><img class='a' src='Styles/BoardReport/images/board-hiden-hover.png'/><label>Board</label></a></div> <div class='hoverdiv' myhovertag='Summary'><a target='_blank' href='" + DomainURL + "/boardSummeryReport.aspx?BID?=" + TotalBoardList[i].BoardID + "'><label class='pull-right'>Summary</label><img src='Styles/BoardReport/images/summary-hiden-hover.png' class='pull-right'/></a></div></div></h3><ul><li><img src='Styles/BoardReport/images/team_grey.png' title='Team Name'><span>" + TotalBoardList[i].TeamName + "</span></li><li><img src='Styles/BoardReport/images/owner_grey.png' title='Board Owner'><span>" + TotalBoardList[i].OwnerName + "</span></li><li><img src='Styles/BoardReport/images/efforts_today.png' title='Efforts'><span>Today's Efforts</span><label id='BID_" + TotalBoardList[i].BoardID + "'>" + TotalBoardList[i].EffortsToday + "</label></li><li><img src='Styles/BoardReport/images/date.png' title='Create Date'><span>" + TotalBoardList[i].DueDate + "</span></li><li style='padding:0px;'><div id='progressbar" + TotalBoardList[i].BoardID + "'><span id='check' class='pblabel" + TotalBoardList[i].BoardID + " pblabel'></span><img class='Clsdelay' src='Images/delay.png' title='Delay " + TotalBoardList[i].Delay + " hrs' alt=''/></div></li><li style='padding:0px;'><div id='progressbar2" + TotalBoardList[i].BoardID + "'><span id='check1' class='pblabel2" + TotalBoardList[i].BoardID + " pblabel1'></span></div></li></ul><div class='clearfix'></div><div id='bar1' class='barfiller'><div class='tipWrap'><span class='tip'></span></div><span class='fill' data-percentage='30'></span></div></div></div> ");
                            }
                            else { $('#BordReportGrid').append("<div id=" + TotalBoardList[i].BoardID + " class='col-md-3 col-sm-6 col-xs-12 board-task-red'><div class='shadow-div'><h3 style='background:#2f77a5'>" + TotalBoardList[i].BoardName + "<div class='hover-frame'><div class='hoverdiv' myhovertag='board'><a target='_blank' href='" + DomainURL + "/board.aspx?" + TotalBoardList[i].BoardID + "'><img class='a' src='Styles/BoardReport/images/board-hiden-hover.png'/><label>Board</label></a></div> <div class='hoverdiv' myhovertag='Summary'><a target='_blank' href='" + DomainURL + "/boardSummeryReport.aspx?BID?=" + TotalBoardList[i].BoardID + "'><label class='pull-right'>Summary</label><img src='Styles/BoardReport/images/summary-hiden-hover.png' class='pull-right'/></a></div></div></h3><ul><li><img src='Styles/BoardReport/images/team_grey.png' title='Team Name'><span>" + TotalBoardList[i].TeamName + "</span></li><li><img src='Styles/BoardReport/images/owner_grey.png' title='Board Owner'><span>" + TotalBoardList[i].OwnerName + "</span></li><li><img src='Styles/BoardReport/images/efforts_today.png' title='Efforts'><span>Today's Efforts</span><label id='BID_" + TotalBoardList[i].BoardID + "'>" + TotalBoardList[i].EffortsToday + "</label></li><li><img src='Styles/BoardReport/images/date.png' title='Create Date'><span>" + TotalBoardList[i].DueDate + "</span></li><li style='padding:0px;'><div id='progressbar" + TotalBoardList[i].BoardID + "'><span id='check' class='pblabel" + TotalBoardList[i].BoardID + " pblabel'></span></div></li><li style='padding:0px;'><div id='progressbar2" + TotalBoardList[i].BoardID + "'><span id='check1' class='pblabel2" + TotalBoardList[i].BoardID + " pblabel1'></span></div></li></ul><div class='clearfix'></div><div id='bar1' class='barfiller'><div class='tipWrap'><span class='tip'></span></div><span class='fill' data-percentage='30'></span></div></div></div> "); }

                        }

                        //progress for board percentage
                        fullvalue = parseInt(TotalBoardList[i].TaskCompletePercent);
                        $('.pblabel' + TotalBoardList[i].BoardID).text(fullvalue + "%");
                        var progressbar = $('#progressbar' + TotalBoardList[i].BoardID).progressbar({
                            value: fullvalue,
                            change: function (event, ui) {
                                var newVal = $(this).progressbar('option', 'value');
                                var label = $('.pblabel' + TotalBoardList[i].BoardID, this).text(newVal + '%');
                            }
                        });
                        var label = progressbar.find('.pblabel' + TotalBoardList[i].BoardID).clone().width(progressbar.width());
                        progressbar.find('.ui-progressbar-value').append(label);
                        $('#progressbar' + TotalBoardList[i].BoardID).css('background', '#f5f5f5');


                        if (fullvalue != 0) {
                            $('#progressbar' + TotalBoardList[i].BoardID + ' .ui-widget-header').attr('style', 'background-color:' + TotalBoardList[i].boardcolor + ' !important; width:' + fullvalue + '%;');
                        }

                        listIndex = i + 1;


                        //progress for estimated hour percentage
                        fullvalue = parseInt(TotalBoardList[i].consumeHourPercentage);
                        consumedhour = TotalBoardList[i].consumehours + " hr " + "/ " + TotalBoardList[i].EstimatedHours + " hr ";
                        $('.pblabel2' + TotalBoardList[i].BoardID).text(consumedhour);
                        var progressbar = $('#progressbar2' + TotalBoardList[i].BoardID).progressbar({
                            value: fullvalue,
                            change: function (event, ui) {
                                var newVal = $(this).progressbar('option', 'value');
                                var label = $('.pblabel2' + TotalBoardList[i].BoardID, this).text(newVal + '%');
                            }
                        });

                        var label = progressbar.find('.pblabel2' + TotalBoardList[i].BoardID).clone().width(progressbar.width());
                        progressbar.find('.ui-progressbar-value').append(label);

                        $('#progressbar2' + TotalBoardList[i].BoardID).css('background', '#ededed');
                        if (fullvalue != 0) {
                            $('#progressbar2' + TotalBoardList[i].BoardID + ' .ui-widget-header').attr('style', 'background-color:#b2b2b2 !important; width:' + fullvalue + '%;');
                        }
                        $('#progressbar2' + TotalBoardList[i].BoardID + ' span').attr('style', 'padding:5px; background:#8f8f8f;margin-left: 0px;color: #fff;width: 100%;');
                        $('#progressbar2' + TotalBoardList[i].BoardID + ' div span').css('background', '#b2b2b2');
                    }
                }
                $(".tabular").append('<thead><tr class="blue noExl"><th><img src="Styles/BoardReport/images/name-wh.png">Name</th><th><img src="Styles/BoardReport/images/team-wh.png">Team</th><th><img src="Styles/BoardReport/images/owner-wh.png">Owner</th><th><img src="Styles/BoardReport/images/start_date-wh.png">Start Date</th><th><img src="Styles/BoardReport/images/end_date-wh.png">End Date</th><th><img src="Styles/BoardReport/images/efforts-wh.png">Today' + "'" + 's Efforts</th><th><img src="Styles/BoardReport/images/hours-wh.png">Hours Consumed</th><th><img src="Styles/BoardReport/images/board-wh.png">Board Progress</th><th><img src="Images/action.png">Action</th></tr></thead><tbody>');
                if (usertypeid == "12") {
                    $.each(data.d, function (index, item) {
                        if (parseInt(item.EffortsToday) > 0) {
                            $('.tabular tbody').append("<tr><td style='border-left:solid 10px " + item.boardcolor + "'><a target='_blank'href=" + DomainURL + "/BoardSummeryReport.aspx?BID=" + item.BoardID + ">" + item.BoardName + "</a><span class='board-sumary-hover-cont' style='background:" + item.boardcolor + "'><a target='_blank' href='" + DomainURL + "/board.aspx?" + item.BoardID + "'><img src='Styles/BoardReport/images/board.png'></a><a target='_blank' href='" + DomainURL + "/boardSummeryReport.aspx?BID?=" + item.BoardID + "'><img src='Styles/BoardReport/images/summary.png'></a></span></td><td>" + item.TeamName + "</td><td>" + item.OwnerName + "</td><td>" + item.CreatedDate + "</td><td>" + item.DueDate + "</td><td><label style='background: #44bf53' id='BID_" + item.BoardID + "'>" + item.EffortsToday + "</label></td><td style='padding:0px;'><div id='progressbar3" + item.BoardID + "'><span id='check' class='pblabel3" + item.BoardID + " pblabel'></span></div></td><td style='padding:0px;'><div id='progressbar1" + item.BoardID + "'><span id='check' class='pblabel1" + item.BoardID + " pblabel'></span></div></td><td><img src='Images/icon1.png' class='archiveandcomplete' title='Archive Board' ArchiveTag='archive'/><img src='Images/icon2.png' title='Complete Board' ArchiveTag='complete' class='archiveandcomplete'/></td></tr>");
                        }
                        else {
                            $('.tabular tbody').append("<tr><td style='border-left:solid 10px " + item.boardcolor + "'><a target='_blank'href=" + DomainURL + "/BoardSummeryReport.aspx?BID=" + item.BoardID + ">" + item.BoardName + "</a><span class='board-sumary-hover-cont' style='background:" + item.boardcolor + "'><a target='_blank' href='" + DomainURL + "/board.aspx?" + item.BoardID + "'><img src='Styles/BoardReport/images/board.png'></a><a target='_blank' href='" + DomainURL + "/boardSummeryReport.aspx?BID?=" + item.BoardID + "'><img src='Styles/BoardReport/images/summary.png'></a></span></td><td>" + item.TeamName + "</td><td>" + item.OwnerName + "</td><td>" + item.CreatedDate + "</td><td>" + item.DueDate + "</td><td><label id='BID_" + item.BoardID + "'>" + item.EffortsToday + "</label></td><td style='padding:0px;'><div id='progressbar3" + item.BoardID + "'><span id='check' class='pblabel3" + item.BoardID + " pblabel'></span></div></td><td style='padding:0px;'><div id='progressbar1" + item.BoardID + "'><span id='check' class='pblabel1" + item.BoardID + " pblabel'></span></div></td><td><img src='Images/icon1.png' title='Archive Board' class='archiveandcomplete' ArchiveTag='archive'/><img src='Images/icon2.png' class='archiveandcomplete' title='Complete Board' ArchiveTag='complete'/></td></tr>");
                        }

                        //progress for board percentage
                        fullvalue = parseInt(item.TaskCompletePercent);
                        $('.pblabel1' + item.BoardID).text(fullvalue + "%");
                        var progressbar = $('#progressbar1' + item.BoardID).progressbar({
                            value: fullvalue,
                            change: function (event, ui) {
                                var newVal = $(this).progressbar('option', 'value');
                                var label = $('.pblabel1' + item.BoardID, this).text(newVal + '%');
                            }
                        });


                        if (fullvalue != 0) {
                            var label = progressbar.find('.pblabel1' + item.BoardID).width(progressbar.width());
                            progressbar.find('.ui-progressbar-value').append(label);
                            $('#progressbar1' + item.BoardID + ' .ui-widget-header').attr('style', 'background-color:' + item.boardcolor + ' !important; width:' + parseInt(fullvalue) + '%;');
                        }
                        else {
                            var label = progressbar.find('.pblabel1' + item.BoardID).clone().width(progressbar.width());
                            progressbar.find('.ui-progressbar-value').append(label);
                        }

                        $('.pblabel1' + item.BoardID).css('width', '');

                        //progress for estimated hour percentage
                        fullvalue = parseInt(item.consumeHourPercentage);
                        consumedhour = item.consumehours + " hr " + "/ " + item.EstimatedHours + " hr ";
                        $('.pblabel3' + item.BoardID).text(consumedhour);
                        var progressbar = $('#progressbar3' + item.BoardID).progressbar({
                            value: fullvalue,
                            change: function (event, ui) {
                                var newVal = $(this).progressbar('option', 'value');
                                var label = $('.pblabel3' + item.BoardID, this).text(newVal + '%');
                            }
                        });

                        if (fullvalue != 0) {
                            var label = progressbar.find('.pblabel3' + item.BoardID).width(progressbar.width());
                            progressbar.find('.ui-progressbar-value').append(label);
                            $('#progressbar3' + item.BoardID + ' .ui-widget-header').attr('style', 'background-color:#8f8f8f !important; width:' + parseInt(fullvalue) + '%;');
                        }
                        else {
                            var label = progressbar.find('.pblabel3' + item.BoardID).clone().width(progressbar.width());
                            progressbar.find('.ui-progressbar-value').append(label);

                        }

                        if (parseInt(fullvalue + 2) < 50) {
                            $('#progressbar3' + item.BoardID + ' div').addClass('fitdivspan');
                            $('#progressbar3' + item.BoardID + ' span').css('color', 'white');
                        }

                        if (fullvalue == 0) {
                            $('#progressbar3' + item.BoardID + ' .ui-widget-header   span').remove();
                        }
                        $('.pblabel3' + item.BoardID).css('width', '')
                        $('#progressbar3' + item.BoardID + ' div span').css('background', '#b2b2b2');
                        i++;

                    });
                }
                else {
                    $.each(data.d, function (index, item) {

                        if (item.flag == 1) {
                            if (parseInt(item.EffortsToday) > 0) {
                                $('.tabular tbody').append("<tr><td style='border-left:solid 10px " + item.boardcolor + "'><a target='_blank'href=" + DomainURL + "/BoardSummeryReport.aspx?BID=" + item.BoardID + ">" + item.BoardName + "</a><span class='board-sumary-hover-cont' style='background:" + item.boardcolor + "'><a target='_blank' href='" + DomainURL + "/board.aspx?" + item.BoardID + "'><img src='Styles/BoardReport/images/board.png'></a><a target='_blank' href='" + DomainURL + "/boardSummeryReport.aspx?BID?=" + item.BoardID + "'><img src='Styles/BoardReport/images/summary.png'></a></span></td><td>" + item.TeamName + "</td><td>" + item.OwnerName + "</td><td>" + item.CreatedDate + "</td><td>" + item.DueDate + "</td><td><label style='background: #44bf53' id='BID_" + item.BoardID + "'>" + item.EffortsToday + "</label></td><td style='padding:0px;'><div id='progressbar3" + item.BoardID + "'><span id='check' class='pblabel3" + item.BoardID + " pblabel'></span></div></td><td style='padding:0px;'><div id='progressbar1" + item.BoardID + "'><span id='check' class='pblabel1" + item.BoardID + " pblabel'></span></div></td><td><img src='Images/icon1.png' class='archiveandcomplete' title='Archive Board' ArchiveTag='archive'/><img src='Images/icon2.png' title='Complete Board' ArchiveTag='complete' class='archiveandcomplete'/></td></tr>");
                            }
                            else {
                                $('.tabular tbody').append("<tr><td style='border-left:solid 10px " + item.boardcolor + "'><a target='_blank'href=" + DomainURL + "/BoardSummeryReport.aspx?BID=" + item.BoardID + ">" + item.BoardName + "</a><span class='board-sumary-hover-cont' style='background:" + item.boardcolor + "'><a target='_blank' href='" + DomainURL + "/board.aspx?" + item.BoardID + "'><img src='Styles/BoardReport/images/board.png'></a><a target='_blank' href='" + DomainURL + "/boardSummeryReport.aspx?BID?=" + item.BoardID + "'><img src='Styles/BoardReport/images/summary.png'></a></span></td><td>" + item.TeamName + "</td><td>" + item.OwnerName + "</td><td>" + item.CreatedDate + "</td><td>" + item.DueDate + "</td><td><label id='BID_" + item.BoardID + "'>" + item.EffortsToday + "</label></td><td style='padding:0px;'><div id='progressbar3" + item.BoardID + "'><span id='check' class='pblabel3" + item.BoardID + " pblabel'></span></div></td><td style='padding:0px;'><div id='progressbar1" + item.BoardID + "'><span id='check' class='pblabel1" + item.BoardID + " pblabel'></span></div></td><td><img src='Images/icon1.png' title='Archive Board' class='archiveandcomplete' ArchiveTag='archive'/><img src='Images/icon2.png' class='archiveandcomplete' title='Complete Board' ArchiveTag='complete'/></td></tr>");
                            }

                        }
                        else {
                            if (parseInt(item.EffortsToday) > 0) {
                                $('.tabular tbody').append("<tr><td style='border-left:solid 10px " + item.boardcolor + "'><a target='_blank'href=" + DomainURL + "/BoardSummeryReport.aspx?BID=" + item.BoardID + ">" + item.BoardName + "</a><span class='board-sumary-hover-cont' style='background:" + item.boardcolor + "'><a target='_blank' href='" + DomainURL + "/board.aspx?" + item.BoardID + "'><img src='Styles/BoardReport/images/board.png'></a><a target='_blank' href='" + DomainURL + "/boardSummeryReport.aspx?BID?=" + item.BoardID + "'><img src='Styles/BoardReport/images/summary.png'></a></span></td><td>" + item.TeamName + "</td><td>" + item.OwnerName + "</td><td>" + item.CreatedDate + "</td><td>" + item.DueDate + "</td><td><label style='background: #44bf53' id='BID_" + item.BoardID + "'>" + item.EffortsToday + "</label></td><td style='padding:0px;'><div id='progressbar3" + item.BoardID + "'><span id='check' class='pblabel3" + item.BoardID + " pblabel'></span></div></td><td style='padding:0px;'><div id='progressbar1" + item.BoardID + "'><span id='check' class='pblabel1" + item.BoardID + " pblabel'></span></div></td><td></td></tr>");
                            }
                            else {
                                $('.tabular tbody').append("<tr><td style='border-left:solid 10px " + item.boardcolor + "'><a target='_blank'href=" + DomainURL + "/BoardSummeryReport.aspx?BID=" + item.BoardID + ">" + item.BoardName + "</a><span class='board-sumary-hover-cont' style='background:" + item.boardcolor + "'><a target='_blank' href='" + DomainURL + "/board.aspx?" + item.BoardID + "'><img src='Styles/BoardReport/images/board.png'></a><a target='_blank' href='" + DomainURL + "/boardSummeryReport.aspx?BID?=" + item.BoardID + "'><img src='Styles/BoardReport/images/summary.png'></a></span></td><td>" + item.TeamName + "</td><td>" + item.OwnerName + "</td><td>" + item.CreatedDate + "</td><td>" + item.DueDate + "</td><td><label id='BID_" + item.BoardID + "'>" + item.EffortsToday + "</label></td><td style='padding:0px;'><div id='progressbar3" + item.BoardID + "'><span id='check' class='pblabel3" + item.BoardID + " pblabel'></span></div></td><td style='padding:0px;'><div id='progressbar1" + item.BoardID + "'><span id='check' class='pblabel1" + item.BoardID + " pblabel'></span></div></td><td></td></tr>");
                            }

                        }

                        //progress for board percentage
                        fullvalue = parseInt(item.TaskCompletePercent);
                        $('.pblabel1' + item.BoardID).text(fullvalue + "%");
                        var progressbar = $('#progressbar1' + item.BoardID).progressbar({
                            value: fullvalue,
                            change: function (event, ui) {
                                var newVal = $(this).progressbar('option', 'value');
                                var label = $('.pblabel1' + item.BoardID, this).text(newVal + '%');
                            }
                        });


                        if (fullvalue != 0) {
                            var label = progressbar.find('.pblabel1' + item.BoardID).width(progressbar.width());
                            progressbar.find('.ui-progressbar-value').append(label);
                            $('#progressbar1' + item.BoardID + ' .ui-widget-header').attr('style', 'background-color:' + item.boardcolor + ' !important; width:' + parseInt(fullvalue) + '%;');
                        }
                        else {
                            var label = progressbar.find('.pblabel1' + item.BoardID).clone().width(progressbar.width());
                            progressbar.find('.ui-progressbar-value').append(label);
                        }

                        $('.pblabel1' + item.BoardID).css('width', '');

                        //progress for estimated hour percentage
                        fullvalue = parseInt(item.consumeHourPercentage);
                        consumedhour = item.consumehours + " hr " + "/ " + item.EstimatedHours + " hr ";
                        $('.pblabel3' + item.BoardID).text(consumedhour);
                        var progressbar = $('#progressbar3' + item.BoardID).progressbar({
                            value: fullvalue,
                            change: function (event, ui) {
                                var newVal = $(this).progressbar('option', 'value');
                                var label = $('.pblabel3' + item.BoardID, this).text(newVal + '%');
                            }
                        });

                        if (fullvalue != 0) {
                            var label = progressbar.find('.pblabel3' + item.BoardID).width(progressbar.width());
                            progressbar.find('.ui-progressbar-value').append(label);
                            $('#progressbar3' + item.BoardID + ' .ui-widget-header').attr('style', 'background-color:#8f8f8f !important; width:' + parseInt(fullvalue) + '%;');
                        }
                        else {
                            var label = progressbar.find('.pblabel3' + item.BoardID).clone().width(progressbar.width());
                            progressbar.find('.ui-progressbar-value').append(label);

                        }

                        if (parseInt(fullvalue + 2) < 50) {
                            $('#progressbar3' + item.BoardID + ' div').addClass('fitdivspan');
                            $('#progressbar3' + item.BoardID + ' span').css('color', '');
                        }

                        if (fullvalue == 0) {
                            $('#progressbar3' + item.BoardID + ' .ui-widget-header   span').remove();
                        }
                        $('.pblabel3' + item.BoardID).css('width', '')
                        $('#progressbar3' + item.BoardID + ' div span').css('background', '#b2b2b2');
                        i++;

                    });


                }
                $(".tabular tbody").append("</tbody>");
                BindGrid();

            }
            else {
                $('#BordReportGrid').append("<h2>No Record Found</h2>");
                $('#loadboard').css('display', 'none');
            }
        },
        error: function (err) {

        }
    });
    $('.hover-frame').slideUp();
});


$("#FilterByForCast").live("click", function () {

    if (filtersAllBoardid.length > 0) {

        $('.tabular tbody').empty();
        $("#displayselected").parent().css('display', 'block');
        $("#DeleteSelected").css('display', 'block');
        $(".forecast_yellow").css('display', 'block');
        $("#DeletedSelectedForcast").css('display', 'block');
        TotalBoardList = "";
        flag = 19;
        listIndex = 0;
        addvaluetomultiplylist = 1;

        $.ajax({
            type: 'POST',
            url: 'DataTracker.svc/FilterByForCast',
            data: JSON.stringify({ boardies: filtersAllBoardid }),
            contentType: "application/json; charset=utf-8",
            datatype: 'json',
            async: false,
            success: function (data) {
                $('#BordReportGrid').empty();

                TotalBoardList = data.d;
                if (data.d.length > 0) {
                    var fullvalue = "";
                    $('#loadboard').css('display', 'block');
                    if ($('#BordReportGrid').css('display') == 'none') {

                        $('#loadboard').css('display', 'none');
                    }

                    $('#BordReportGrid').empty();
                    var table = $('.tabular').DataTable();
                    table.destroy();
                    $('.tabular').empty();

                    if (data.d.length < 20) {
                        $('#loadboard').css('display', 'none');
                        for (var i = 0; i < data.d.length; i++) {
                            if (parseInt(TotalBoardList[i].EffortsToday) > 0) {
                                if (TotalBoardList[i].Delay > 0) {
                                    $('#BordReportGrid').append("<div id=" + TotalBoardList[i].BoardID + " class='col-md-3 col-sm-6 col-xs-12 board-task-red'><div class='shadow-div'><h3 style='background:#2f77a5'>" + TotalBoardList[i].BoardName + "<div class='hover-frame'><div class='hoverdiv' myhovertag='board'><a target='_blank' href='" + DomainURL + "/board.aspx?" + TotalBoardList[i].BoardID + "'><img class='a' src='Styles/BoardReport/images/board-hiden-hover.png'/><label>Board</label></a></div> <div class='hoverdiv' myhovertag='Summary'><a target='_blank' href='" + DomainURL + "/boardSummeryReport.aspx?BID?=" + TotalBoardList[i].BoardID + "'><label class='pull-right'>Summary</label><img src='Styles/BoardReport/images/summary-hiden-hover.png' class='pull-right'/></a></div></div></h3><ul><li><img src='Styles/BoardReport/images/team_grey.png' title='Team Name'><span>" + TotalBoardList[i].TeamName + "</span></li><li><img src='Styles/BoardReport/images/owner_grey.png' title='Board Owner'><span>" + TotalBoardList[i].OwnerName + "</span></li><li><img src='Styles/BoardReport/images/efforts_today.png' title='Efforts'><span>Today's Efforts</span><label style='background: #44bf53' id='BID_" + TotalBoardList[i].BoardID + "'>" + TotalBoardList[i].EffortsToday + "</label></li><li><img src='Styles/BoardReport/images/date.png' title='Create Date'><span>" + TotalBoardList[i].DueDate + "</span></li><li style='padding:0px;'><div id='progressbar" + TotalBoardList[i].BoardID + "'><span id='check' class='pblabel" + TotalBoardList[i].BoardID + " pblabel'></span><img class='Clsdelay' src='Images/delay.png' title='Delay " + TotalBoardList[i].Delay + " hrs' alt=''/></div></li><li style='padding:0px;'><div id='progressbar2" + TotalBoardList[i].BoardID + "'><span id='check1' class='pblabel2" + TotalBoardList[i].BoardID + " pblabel1'></span></div></li></ul><div class='clearfix'></div><div id='bar1' class='barfiller'><div class='tipWrap'><span class='tip'></span></div><span class='fill' data-percentage='30'></span></div></div></div> ");
                                }
                                else { $('#BordReportGrid').append("<div id=" + TotalBoardList[i].BoardID + " class='col-md-3 col-sm-6 col-xs-12 board-task-red'><div class='shadow-div'><h3 style='background:#2f77a5'>" + TotalBoardList[i].BoardName + "<div class='hover-frame'><div class='hoverdiv' myhovertag='board'><a target='_blank' href='" + DomainURL + "/board.aspx?" + TotalBoardList[i].BoardID + "'><img class='a' src='Styles/BoardReport/images/board-hiden-hover.png'/><label>Board</label></a></div> <div class='hoverdiv' myhovertag='Summary'><a target='_blank' href='" + DomainURL + "/boardSummeryReport.aspx?BID?=" + TotalBoardList[i].BoardID + "'><label class='pull-right'>Summary</label><img src='Styles/BoardReport/images/summary-hiden-hover.png' class='pull-right'/></a></div></div></h3><ul><li><img src='Styles/BoardReport/images/team_grey.png' title='Team Name'><span>" + TotalBoardList[i].TeamName + "</span></li><li><img src='Styles/BoardReport/images/owner_grey.png' title='Board Owner'><span>" + TotalBoardList[i].OwnerName + "</span></li><li><img src='Styles/BoardReport/images/efforts_today.png' title='Efforts'><span>Today's Efforts</span><label style='background: #44bf53' id='BID_" + TotalBoardList[i].BoardID + "'>" + TotalBoardList[i].EffortsToday + "</label></li><li><img src='Styles/BoardReport/images/date.png' title='Create Date'><span>" + TotalBoardList[i].DueDate + "</span></li><li style='padding:0px;'><div id='progressbar" + TotalBoardList[i].BoardID + "'><span id='check' class='pblabel" + TotalBoardList[i].BoardID + " pblabel'></span></div></li><li style='padding:0px;'><div id='progressbar2" + TotalBoardList[i].BoardID + "'><span id='check1' class='pblabel2" + TotalBoardList[i].BoardID + " pblabel1'></span></div></li></ul><div class='clearfix'></div><div id='bar1' class='barfiller'><div class='tipWrap'><span class='tip'></span></div><span class='fill' data-percentage='30'></span></div></div></div> "); }

                            }
                            else {
                                if (TotalBoardList[i].Delay > 0) {
                                    $('#BordReportGrid').append("<div id=" + TotalBoardList[i].BoardID + " class='col-md-3 col-sm-6 col-xs-12 board-task-red'><div class='shadow-div'><h3 style='background:#2f77a5'>" + TotalBoardList[i].BoardName + "<div class='hover-frame'><div class='hoverdiv' myhovertag='board'><a target='_blank' href='" + DomainURL + "/board.aspx?" + TotalBoardList[i].BoardID + "'><img class='a' src='Styles/BoardReport/images/board-hiden-hover.png'/><label>Board</label></a></div> <div class='hoverdiv' myhovertag='Summary'><a target='_blank' href='" + DomainURL + "/boardSummeryReport.aspx?BID?=" + TotalBoardList[i].BoardID + "'><label class='pull-right'>Summary</label><img src='Styles/BoardReport/images/summary-hiden-hover.png' class='pull-right'/></a></div></div></h3><ul><li><img src='Styles/BoardReport/images/team_grey.png' title='Team Name'><span>" + TotalBoardList[i].TeamName + "</span></li><li><img src='Styles/BoardReport/images/owner_grey.png' title='Board Owner'><span>" + TotalBoardList[i].OwnerName + "</span></li><li><img src='Styles/BoardReport/images/efforts_today.png' title='Efforts'><span>Today's Efforts</span><label id='BID_" + TotalBoardList[i].BoardID + "'>" + TotalBoardList[i].EffortsToday + "</label></li><li><img src='Styles/BoardReport/images/date.png' title='Create Date'><span>" + TotalBoardList[i].DueDate + "</span></li><li style='padding:0px;'><div id='progressbar" + TotalBoardList[i].BoardID + "'><span id='check' class='pblabel" + TotalBoardList[i].BoardID + " pblabel'></span><img class='Clsdelay' src='Images/delay.png' title='Delay " + TotalBoardList[i].Delay + " hrs' alt=''/></div></li><li style='padding:0px;'><div id='progressbar2" + TotalBoardList[i].BoardID + "'><span id='check1' class='pblabel2" + TotalBoardList[i].BoardID + " pblabel1'></span></div></li></ul><div class='clearfix'></div><div id='bar1' class='barfiller'><div class='tipWrap'><span class='tip'></span></div><span class='fill' data-percentage='30'></span></div></div></div> ");
                                }
                                else { $('#BordReportGrid').append("<div id=" + TotalBoardList[i].BoardID + " class='col-md-3 col-sm-6 col-xs-12 board-task-red'><div class='shadow-div'><h3 style='background:#2f77a5'>" + TotalBoardList[i].BoardName + "<div class='hover-frame'><div class='hoverdiv' myhovertag='board'><a target='_blank' href='" + DomainURL + "/board.aspx?" + TotalBoardList[i].BoardID + "'><img class='a' src='Styles/BoardReport/images/board-hiden-hover.png'/><label>Board</label></a></div> <div class='hoverdiv' myhovertag='Summary'><a target='_blank' href='" + DomainURL + "/boardSummeryReport.aspx?BID?=" + TotalBoardList[i].BoardID + "'><label class='pull-right'>Summary</label><img src='Styles/BoardReport/images/summary-hiden-hover.png' class='pull-right'/></a></div></div></h3><ul><li><img src='Styles/BoardReport/images/team_grey.png' title='Team Name'><span>" + TotalBoardList[i].TeamName + "</span></li><li><img src='Styles/BoardReport/images/owner_grey.png' title='Board Owner'><span>" + TotalBoardList[i].OwnerName + "</span></li><li><img src='Styles/BoardReport/images/efforts_today.png' title='Efforts'><span>Today's Efforts</span><label id='BID_" + TotalBoardList[i].BoardID + "'>" + TotalBoardList[i].EffortsToday + "</label></li><li><img src='Styles/BoardReport/images/date.png' title='Create Date'><span>" + TotalBoardList[i].DueDate + "</span></li><li style='padding:0px;'><div id='progressbar" + TotalBoardList[i].BoardID + "'><span id='check' class='pblabel" + TotalBoardList[i].BoardID + " pblabel'></span></div></li><li style='padding:0px;'><div id='progressbar2" + TotalBoardList[i].BoardID + "'><span id='check1' class='pblabel2" + TotalBoardList[i].BoardID + " pblabel1'></span></div></li></ul><div class='clearfix'></div><div id='bar1' class='barfiller'><div class='tipWrap'><span class='tip'></span></div><span class='fill' data-percentage='30'></span></div></div></div> "); }

                            }
                            //progress for board percentage
                            fullvalue = parseInt(TotalBoardList[i].TaskCompletePercent);
                            $('.pblabel' + TotalBoardList[i].BoardID).text(fullvalue + "%");
                            var progressbar = $('#progressbar' + TotalBoardList[i].BoardID).progressbar({
                                value: fullvalue,
                                change: function (event, ui) {
                                    var newVal = $(this).progressbar('option', 'value');
                                    var label = $('.pblabel' + TotalBoardList[i].BoardID, this).text(newVal + '%');
                                }
                            });
                            var label = progressbar.find('.pblabel' + TotalBoardList[i].BoardID).clone().width(progressbar.width());
                            progressbar.find('.ui-progressbar-value').append(label);
                            $('#progressbar' + TotalBoardList[i].BoardID).css('background', '#f5f5f5');


                            if (fullvalue != 0) {
                                $('#progressbar' + TotalBoardList[i].BoardID + ' .ui-widget-header').attr('style', 'background-color:' + TotalBoardList[i].boardcolor + ' !important; width:' + fullvalue + '%;');
                            }

                            listIndex = i + 1;


                            //progress for estimated hour percentage
                            fullvalue = parseInt(TotalBoardList[i].consumeHourPercentage);
                            // consumedhour = TotalBoardList[i].consumehours + "/" + TotalBoardList[i].EstimatedHours;
                            consumedhour = TotalBoardList[i].consumehours + " hr " + "/ " + TotalBoardList[i].EstimatedHours + " hr ";
                            $('.pblabel2' + TotalBoardList[i].BoardID).text(consumedhour);
                            var progressbar = $('#progressbar2' + TotalBoardList[i].BoardID).progressbar({
                                value: fullvalue,
                                change: function (event, ui) {
                                    var newVal = $(this).progressbar('option', 'value');
                                    var label = $('.pblabel2' + TotalBoardList[i].BoardID, this).text(newVal + '%');
                                }
                            });

                            var label = progressbar.find('.pblabel2' + TotalBoardList[i].BoardID).clone().width(progressbar.width());
                            progressbar.find('.ui-progressbar-value').append(label);

                            $('#progressbar2' + TotalBoardList[i].BoardID).css('background', '#ededed');
                            if (fullvalue != 0) {
                                $('#progressbar2' + TotalBoardList[i].BoardID + ' .ui-widget-header').attr('style', 'background-color:#b2b2b2 !important; width:' + fullvalue + '%;');
                            }
                            $('#progressbar2' + TotalBoardList[i].BoardID + ' span').attr('style', 'padding:5px; background:#8f8f8f;margin-left: 0px;color: #fff;width: 100%;');
                            $('#progressbar2' + TotalBoardList[i].BoardID + ' div span').css('background', '#b2b2b2');
                        }
                    }
                    else {
                        for (var i = 0; i <= flag; i++) {

                            if (parseInt(TotalBoardList[i].EffortsToday) > 0) {
                                if (TotalBoardList[i].Delay > 0) {
                                    $('#BordReportGrid').append("<div id=" + TotalBoardList[i].BoardID + " class='col-md-3 col-sm-6 col-xs-12 board-task-red'><div class='shadow-div'><h3 style='background:#2f77a5'>" + TotalBoardList[i].BoardName + "<div class='hover-frame'><div class='hoverdiv' myhovertag='board'><a target='_blank' href='" + DomainURL + "/board.aspx?" + TotalBoardList[i].BoardID + "'><img class='a' src='Styles/BoardReport/images/board-hiden-hover.png'/><label>Board</label></a></div> <div class='hoverdiv' myhovertag='Summary'><a target='_blank' href='" + DomainURL + "/boardSummeryReport.aspx?BID?=" + TotalBoardList[i].BoardID + "'><label class='pull-right'>Summary</label><img src='Styles/BoardReport/images/summary-hiden-hover.png' class='pull-right'/></a></div></div></h3><ul><li><img src='Styles/BoardReport/images/team_grey.png' title='Team Name'><span>" + TotalBoardList[i].TeamName + "</span></li><li><img src='Styles/BoardReport/images/owner_grey.png' title='Board Owner'><span>" + TotalBoardList[i].OwnerName + "</span></li><li><img src='Styles/BoardReport/images/efforts_today.png' title='Efforts'><span>Today's Efforts</span><label style='background: #44bf53' id='BID_" + TotalBoardList[i].BoardID + "'>" + TotalBoardList[i].EffortsToday + "</label></li><li><img src='Styles/BoardReport/images/date.png' title='Create Date'><span>" + TotalBoardList[i].DueDate + "</span></li><li style='padding:0px;'><div id='progressbar" + TotalBoardList[i].BoardID + "'><span id='check' class='pblabel" + TotalBoardList[i].BoardID + " pblabel'></span><img class='Clsdelay' src='Images/delay.png' title='Delay " + TotalBoardList[i].Delay + " hrs' alt=''/></div></li><li style='padding:0px;'><div id='progressbar2" + TotalBoardList[i].BoardID + "'><span id='check1' class='pblabel2" + TotalBoardList[i].BoardID + " pblabel1'></span></div></li></ul><div class='clearfix'></div><div id='bar1' class='barfiller'><div class='tipWrap'><span class='tip'></span></div><span class='fill' data-percentage='30'></span></div></div> ");
                                }
                                else { $('#BordReportGrid').append("<div id=" + TotalBoardList[i].BoardID + " class='col-md-3 col-sm-6 col-xs-12 board-task-red'><div class='shadow-div'><h3 style='background:#2f77a5'>" + TotalBoardList[i].BoardName + "<div class='hover-frame'><div class='hoverdiv' myhovertag='board'><a target='_blank' href='" + DomainURL + "/board.aspx?" + TotalBoardList[i].BoardID + "'><img class='a' src='Styles/BoardReport/images/board-hiden-hover.png'/><label>Board</label></a></div> <div class='hoverdiv' myhovertag='Summary'><a target='_blank' href='" + DomainURL + "/boardSummeryReport.aspx?BID?=" + TotalBoardList[i].BoardID + "'><label class='pull-right'>Summary</label><img src='Styles/BoardReport/images/summary-hiden-hover.png' class='pull-right'/></a></div></div></h3><ul><li><img src='Styles/BoardReport/images/team_grey.png' title='Team Name'><span>" + TotalBoardList[i].TeamName + "</span></li><li><img src='Styles/BoardReport/images/owner_grey.png' title='Board Owner'><span>" + TotalBoardList[i].OwnerName + "</span></li><li><img src='Styles/BoardReport/images/efforts_today.png' title='Efforts'><span>Today's Efforts</span><label style='background: #44bf53' id='BID_" + TotalBoardList[i].BoardID + "'>" + TotalBoardList[i].EffortsToday + "</label></li><li><img src='Styles/BoardReport/images/date.png' title='Create Date'><span>" + TotalBoardList[i].DueDate + "</span></li><li style='padding:0px;'><div id='progressbar" + TotalBoardList[i].BoardID + "'><span id='check' class='pblabel" + TotalBoardList[i].BoardID + " pblabel'></span></div></li><li style='padding:0px;'><div id='progressbar2" + TotalBoardList[i].BoardID + "'><span id='check1' class='pblabel2" + TotalBoardList[i].BoardID + " pblabel1'></span></div></li></ul><div class='clearfix'></div><div id='bar1' class='barfiller'><div class='tipWrap'><span class='tip'></span></div><span class='fill' data-percentage='30'></span></div></div> "); }

                            }
                            else {
                                if (TotalBoardList[i].Delay > 0) {
                                    $('#BordReportGrid').append("<div id=" + TotalBoardList[i].BoardID + " class='col-md-3 col-sm-6 col-xs-12 board-task-red'><div class='shadow-div'><h3 style='background:#2f77a5'>" + TotalBoardList[i].BoardName + "<div class='hover-frame'><div class='hoverdiv' myhovertag='board'><a target='_blank' href='" + DomainURL + "/board.aspx?" + TotalBoardList[i].BoardID + "'><img class='a' src='Styles/BoardReport/images/board-hiden-hover.png'/><label>Board</label></a></div> <div class='hoverdiv' myhovertag='Summary'><a target='_blank' href='" + DomainURL + "/boardSummeryReport.aspx?BID?=" + TotalBoardList[i].BoardID + "'><label class='pull-right'>Summary</label><img src='Styles/BoardReport/images/summary-hiden-hover.png' class='pull-right'/></a></div></div></h3><ul><li><img src='Styles/BoardReport/images/team_grey.png' title='Team Name'><span>" + TotalBoardList[i].TeamName + "</span></li><li><img src='Styles/BoardReport/images/owner_grey.png' title='Board Owner'><span>" + TotalBoardList[i].OwnerName + "</span></li><li><img src='Styles/BoardReport/images/efforts_today.png' title='Efforts'><span>Today's Efforts</span><label id='BID_" + TotalBoardList[i].BoardID + "'>" + TotalBoardList[i].EffortsToday + "</label></li><li><img src='Styles/BoardReport/images/date.png' title='Create Date'><span>" + TotalBoardList[i].DueDate + "</span></li><li style='padding:0px;'><div id='progressbar" + TotalBoardList[i].BoardID + "'><span id='check' class='pblabel" + TotalBoardList[i].BoardID + " pblabel'></span><img class='Clsdelay' src='Images/delay.png' title='Delay " + TotalBoardList[i].Delay + " hrs' alt=''/></div></li><li style='padding:0px;'><div id='progressbar2" + TotalBoardList[i].BoardID + "'><span id='check1' class='pblabel2" + TotalBoardList[i].BoardID + " pblabel1'></span></div></li></ul><div class='clearfix'></div><div id='bar1' class='barfiller'><div class='tipWrap'><span class='tip'></span></div><span class='fill' data-percentage='30'></span></div></div> ");
                                }
                                else { $('#BordReportGrid').append("<div id=" + TotalBoardList[i].BoardID + " class='col-md-3 col-sm-6 col-xs-12 board-task-red'><div class='shadow-div'><h3 style='background:#2f77a5'>" + TotalBoardList[i].BoardName + "<div class='hover-frame'><div class='hoverdiv' myhovertag='board'><a target='_blank' href='" + DomainURL + "/board.aspx?" + TotalBoardList[i].BoardID + "'><img class='a' src='Styles/BoardReport/images/board-hiden-hover.png'/><label>Board</label></a></div> <div class='hoverdiv' myhovertag='Summary'><a target='_blank' href='" + DomainURL + "/boardSummeryReport.aspx?BID?=" + TotalBoardList[i].BoardID + "'><label class='pull-right'>Summary</label><img src='Styles/BoardReport/images/summary-hiden-hover.png' class='pull-right'/></a></div></div></h3><ul><li><img src='Styles/BoardReport/images/team_grey.png' title='Team Name'><span>" + TotalBoardList[i].TeamName + "</span></li><li><img src='Styles/BoardReport/images/owner_grey.png' title='Board Owner'><span>" + TotalBoardList[i].OwnerName + "</span></li><li><img src='Styles/BoardReport/images/efforts_today.png' title='Efforts'><span>Today's Efforts</span><label id='BID_" + TotalBoardList[i].BoardID + "'>" + TotalBoardList[i].EffortsToday + "</label></li><li><img src='Styles/BoardReport/images/date.png' title='Create Date'><span>" + TotalBoardList[i].DueDate + "</span></li><li style='padding:0px;'><div id='progressbar" + TotalBoardList[i].BoardID + "'><span id='check' class='pblabel" + TotalBoardList[i].BoardID + " pblabel'></span></div></li><li style='padding:0px;'><div id='progressbar2" + TotalBoardList[i].BoardID + "'><span id='check1' class='pblabel2" + TotalBoardList[i].BoardID + " pblabel1'></span></div></li></ul><div class='clearfix'></div><div id='bar1' class='barfiller'><div class='tipWrap'><span class='tip'></span></div><span class='fill' data-percentage='30'></span></div></div> "); }

                            }

                            //progress for board percentage
                            fullvalue = parseInt(TotalBoardList[i].TaskCompletePercent);
                            $('.pblabel' + TotalBoardList[i].BoardID).text(fullvalue + "%");
                            var progressbar = $('#progressbar' + TotalBoardList[i].BoardID).progressbar({
                                value: fullvalue,
                                change: function (event, ui) {
                                    var newVal = $(this).progressbar('option', 'value');
                                    var label = $('.pblabel' + TotalBoardList[i].BoardID, this).text(newVal + '%');
                                }
                            });
                            var label = progressbar.find('.pblabel' + TotalBoardList[i].BoardID).clone().width(progressbar.width());
                            progressbar.find('.ui-progressbar-value').append(label);
                            $('#progressbar' + TotalBoardList[i].BoardID).css('background', '#f5f5f5');


                            if (fullvalue != 0) {
                                $('#progressbar' + TotalBoardList[i].BoardID + ' .ui-widget-header').attr('style', 'background-color:' + TotalBoardList[i].boardcolor + ' !important; width:' + fullvalue + '%;');
                            }

                            listIndex = i + 1;


                            //progress for estimated hour percentage
                            fullvalue = parseInt(TotalBoardList[i].consumeHourPercentage);
                            consumedhour = TotalBoardList[i].consumehours + " hr " + "/ " + TotalBoardList[i].EstimatedHours + " hr ";
                            $('.pblabel2' + TotalBoardList[i].BoardID).text(consumedhour);
                            var progressbar = $('#progressbar2' + TotalBoardList[i].BoardID).progressbar({
                                value: fullvalue,
                                change: function (event, ui) {
                                    var newVal = $(this).progressbar('option', 'value');
                                    var label = $('.pblabel2' + TotalBoardList[i].BoardID, this).text(newVal + '%');
                                }
                            });

                            var label = progressbar.find('.pblabel2' + TotalBoardList[i].BoardID).clone().width(progressbar.width());
                            progressbar.find('.ui-progressbar-value').append(label);

                            $('#progressbar2' + TotalBoardList[i].BoardID).css('background', '#ededed');
                            if (fullvalue != 0) {
                                $('#progressbar2' + TotalBoardList[i].BoardID + ' .ui-widget-header').attr('style', 'background-color:#b2b2b2 !important; width:' + fullvalue + '%;');
                            }
                            $('#progressbar2' + TotalBoardList[i].BoardID + ' span').attr('style', 'padding:5px; background:#8f8f8f;margin-left: 0px;color: #fff;width: 100%;');
                        }
                    }
                    $(".tabular").append('<thead><tr class="blue noExl"><th><img src="Styles/BoardReport/images/name-wh.png">Name</th><th><img src="Styles/BoardReport/images/team-wh.png">Team</th><th><img src="Styles/BoardReport/images/owner-wh.png">Owner</th><th><img src="Styles/BoardReport/images/start_date-wh.png">Start Date</th><th><img src="Styles/BoardReport/images/end_date-wh.png">End Date</th><th><img src="Styles/BoardReport/images/efforts-wh.png">Today' + "'" + 's Efforts</th><th><img src="Styles/BoardReport/images/hours-wh.png">Hours Consumed</th><th><img src="Styles/BoardReport/images/board-wh.png">Board Progress</th><th><img src="Images/action.png">Action</th></tr></thead><tbody>');
                    if (usertypeid == "12") {
                        $.each(data.d, function (index, item) {
                            if (parseInt(item.EffortsToday) > 0) {
                                $('.tabular tbody').append("<tr><td style='border-left:solid 10px " + item.boardcolor + "'><a target='_blank'href=" + DomainURL + "/BoardSummeryReport.aspx?BID=" + item.BoardID + ">" + item.BoardName + "</a><span class='board-sumary-hover-cont' style='background:" + item.boardcolor + "'><a target='_blank' href='" + DomainURL + "/board.aspx?" + item.BoardID + "'><img src='Styles/BoardReport/images/board.png'></a><a target='_blank' href='" + DomainURL + "/boardSummeryReport.aspx?BID?=" + item.BoardID + "'><img src='Styles/BoardReport/images/summary.png'></a></span></td><td>" + item.TeamName + "</td><td>" + item.OwnerName + "</td><td>" + item.CreatedDate + "</td><td>" + item.DueDate + "</td><td><label style='background: #44bf53' id='BID_" + item.BoardID + "'>" + item.EffortsToday + "</label></td><td style='padding:0px;'><div id='progressbar3" + item.BoardID + "'><span id='check' class='pblabel3" + item.BoardID + " pblabel'></span></div></td><td style='padding:0px;'><div id='progressbar1" + item.BoardID + "'><span id='check' class='pblabel1" + item.BoardID + " pblabel'></span></div></td><td><img src='Images/icon1.png' class='archiveandcomplete' title='Archive Board' ArchiveTag='archive'/><img src='Images/icon2.png' title='Complete Board' ArchiveTag='complete' class='archiveandcomplete'/></td></tr>");
                            }
                            else {
                                $('.tabular tbody').append("<tr><td style='border-left:solid 10px " + item.boardcolor + "'><a target='_blank'href=" + DomainURL + "/BoardSummeryReport.aspx?BID=" + item.BoardID + ">" + item.BoardName + "</a><span class='board-sumary-hover-cont' style='background:" + item.boardcolor + "'><a target='_blank' href='" + DomainURL + "/board.aspx?" + item.BoardID + "'><img src='Styles/BoardReport/images/board.png'></a><a target='_blank' href='" + DomainURL + "/boardSummeryReport.aspx?BID?=" + item.BoardID + "'><img src='Styles/BoardReport/images/summary.png'></a></span></td><td>" + item.TeamName + "</td><td>" + item.OwnerName + "</td><td>" + item.CreatedDate + "</td><td>" + item.DueDate + "</td><td><label id='BID_" + item.BoardID + "'>" + item.EffortsToday + "</label></td><td style='padding:0px;'><div id='progressbar3" + item.BoardID + "'><span id='check' class='pblabel3" + item.BoardID + " pblabel'></span></div></td><td style='padding:0px;'><div id='progressbar1" + item.BoardID + "'><span id='check' class='pblabel1" + item.BoardID + " pblabel'></span></div></td><td><img src='Images/icon1.png' title='Archive Board' class='archiveandcomplete' ArchiveTag='archive'/><img src='Images/icon2.png' class='archiveandcomplete' title='Complete Board' ArchiveTag='complete'/></td></tr>");
                            }

                            //progress for board percentage
                            fullvalue = parseInt(item.TaskCompletePercent);
                            $('.pblabel1' + item.BoardID).text(fullvalue + "%");
                            var progressbar = $('#progressbar1' + item.BoardID).progressbar({
                                value: fullvalue,
                                change: function (event, ui) {
                                    var newVal = $(this).progressbar('option', 'value');
                                    var label = $('.pblabel1' + item.BoardID, this).text(newVal + '%');
                                }
                            });


                            if (fullvalue != 0) {
                                var label = progressbar.find('.pblabel1' + item.BoardID).width(progressbar.width());
                                progressbar.find('.ui-progressbar-value').append(label);
                                $('#progressbar1' + item.BoardID + ' .ui-widget-header').attr('style', 'background-color:' + item.boardcolor + ' !important; width:' + parseInt(fullvalue) + '%;');
                            }
                            else {
                                var label = progressbar.find('.pblabel1' + item.BoardID).clone().width(progressbar.width());
                                progressbar.find('.ui-progressbar-value').append(label);
                            }

                            $('.pblabel1' + item.BoardID).css('width', '');

                            //progress for estimated hour percentage
                            fullvalue = parseInt(item.consumeHourPercentage);
                            consumedhour = item.consumehours + " hr " + "/ " + item.EstimatedHours + " hr ";
                            $('.pblabel3' + item.BoardID).text(consumedhour);
                            var progressbar = $('#progressbar3' + item.BoardID).progressbar({
                                value: fullvalue,
                                change: function (event, ui) {
                                    var newVal = $(this).progressbar('option', 'value');
                                    var label = $('.pblabel3' + item.BoardID, this).text(newVal + '%');
                                }
                            });

                            if (fullvalue != 0) {
                                var label = progressbar.find('.pblabel3' + item.BoardID).width(progressbar.width());
                                progressbar.find('.ui-progressbar-value').append(label);
                                $('#progressbar3' + item.BoardID + ' .ui-widget-header').attr('style', 'background-color:#8f8f8f !important; width:' + parseInt(fullvalue) + '%;');
                            }
                            else {
                                var label = progressbar.find('.pblabel3' + item.BoardID).clone().width(progressbar.width());
                                progressbar.find('.ui-progressbar-value').append(label);

                            }

                            if (parseInt(fullvalue + 2) < 50) {
                                $('#progressbar3' + item.BoardID + ' div').addClass('fitdivspan');
                                $('#progressbar3' + item.BoardID + ' span').css('color', 'white');
                            }

                            if (fullvalue == 0) {
                                $('#progressbar3' + item.BoardID + ' .ui-widget-header   span').remove();
                            }
                            $('.pblabel3' + item.BoardID).css('width', '')
                            $('#progressbar3' + item.BoardID + ' div span').css('background', '#b2b2b2');
                            i++;

                        });
                    }
                    else {
                        $.each(data.d, function (index, item) {

                            if (item.flag == 1) {
                                if (parseInt(item.EffortsToday) > 0) {
                                    $('.tabular tbody').append("<tr><td style='border-left:solid 10px " + item.boardcolor + "'><a target='_blank'href=" + DomainURL + "/BoardSummeryReport.aspx?BID=" + item.BoardID + ">" + item.BoardName + "</a><span class='board-sumary-hover-cont' style='background:" + item.boardcolor + "'><a target='_blank' href='" + DomainURL + "/board.aspx?" + item.BoardID + "'><img src='Styles/BoardReport/images/board.png'></a><a target='_blank' href='" + DomainURL + "/boardSummeryReport.aspx?BID?=" + item.BoardID + "'><img src='Styles/BoardReport/images/summary.png'></a></span></td><td>" + item.TeamName + "</td><td>" + item.OwnerName + "</td><td>" + item.CreatedDate + "</td><td>" + item.DueDate + "</td><td><label style='background: #44bf53' id='BID_" + item.BoardID + "'>" + item.EffortsToday + "</label></td><td style='padding:0px;'><div id='progressbar3" + item.BoardID + "'><span id='check' class='pblabel3" + item.BoardID + " pblabel'></span></div></td><td style='padding:0px;'><div id='progressbar1" + item.BoardID + "'><span id='check' class='pblabel1" + item.BoardID + " pblabel'></span></div></td><td><img src='Images/icon1.png' class='archiveandcomplete' title='Archive Board' ArchiveTag='archive'/><img src='Images/icon2.png' title='Complete Board' ArchiveTag='complete' class='archiveandcomplete'/></td></tr>");
                                }
                                else {
                                    $('.tabular tbody').append("<tr><td style='border-left:solid 10px " + item.boardcolor + "'><a target='_blank'href=" + DomainURL + "/BoardSummeryReport.aspx?BID=" + item.BoardID + ">" + item.BoardName + "</a><span class='board-sumary-hover-cont' style='background:" + item.boardcolor + "'><a target='_blank' href='" + DomainURL + "/board.aspx?" + item.BoardID + "'><img src='Styles/BoardReport/images/board.png'></a><a target='_blank' href='" + DomainURL + "/boardSummeryReport.aspx?BID?=" + item.BoardID + "'><img src='Styles/BoardReport/images/summary.png'></a></span></td><td>" + item.TeamName + "</td><td>" + item.OwnerName + "</td><td>" + item.CreatedDate + "</td><td>" + item.DueDate + "</td><td><label id='BID_" + item.BoardID + "'>" + item.EffortsToday + "</label></td><td style='padding:0px;'><div id='progressbar3" + item.BoardID + "'><span id='check' class='pblabel3" + item.BoardID + " pblabel'></span></div></td><td style='padding:0px;'><div id='progressbar1" + item.BoardID + "'><span id='check' class='pblabel1" + item.BoardID + " pblabel'></span></div></td><td><img src='Images/icon1.png' title='Archive Board' class='archiveandcomplete' ArchiveTag='archive'/><img src='Images/icon2.png' class='archiveandcomplete' title='Complete Board' ArchiveTag='complete'/></td></tr>");
                                }

                            }
                            else {
                                if (parseInt(item.EffortsToday) > 0) {
                                    $('.tabular tbody').append("<tr><td style='border-left:solid 10px " + item.boardcolor + "'><a target='_blank'href=" + DomainURL + "/BoardSummeryReport.aspx?BID=" + item.BoardID + ">" + item.BoardName + "</a><span class='board-sumary-hover-cont' style='background:" + item.boardcolor + "'><a target='_blank' href='" + DomainURL + "/board.aspx?" + item.BoardID + "'><img src='Styles/BoardReport/images/board.png'></a><a target='_blank' href='" + DomainURL + "/boardSummeryReport.aspx?BID?=" + item.BoardID + "'><img src='Styles/BoardReport/images/summary.png'></a></span></td><td>" + item.TeamName + "</td><td>" + item.OwnerName + "</td><td>" + item.CreatedDate + "</td><td>" + item.DueDate + "</td><td><label style='background: #44bf53' id='BID_" + item.BoardID + "'>" + item.EffortsToday + "</label></td><td style='padding:0px;'><div id='progressbar3" + item.BoardID + "'><span id='check' class='pblabel3" + item.BoardID + " pblabel'></span></div></td><td style='padding:0px;'><div id='progressbar1" + item.BoardID + "'><span id='check' class='pblabel1" + item.BoardID + " pblabel'></span></div></td><td></td></tr>");
                                }
                                else {
                                    $('.tabular tbody').append("<tr><td style='border-left:solid 10px " + item.boardcolor + "'><a target='_blank'href=" + DomainURL + "/BoardSummeryReport.aspx?BID=" + item.BoardID + ">" + item.BoardName + "</a><span class='board-sumary-hover-cont' style='background:" + item.boardcolor + "'><a target='_blank' href='" + DomainURL + "/board.aspx?" + item.BoardID + "'><img src='Styles/BoardReport/images/board.png'></a><a target='_blank' href='" + DomainURL + "/boardSummeryReport.aspx?BID?=" + item.BoardID + "'><img src='Styles/BoardReport/images/summary.png'></a></span></td><td>" + item.TeamName + "</td><td>" + item.OwnerName + "</td><td>" + item.CreatedDate + "</td><td>" + item.DueDate + "</td><td><label id='BID_" + item.BoardID + "'>" + item.EffortsToday + "</label></td><td style='padding:0px;'><div id='progressbar3" + item.BoardID + "'><span id='check' class='pblabel3" + item.BoardID + " pblabel'></span></div></td><td style='padding:0px;'><div id='progressbar1" + item.BoardID + "'><span id='check' class='pblabel1" + item.BoardID + " pblabel'></span></div></td><td></td></tr>");
                                }

                            }

                            //progress for board percentage
                            fullvalue = parseInt(item.TaskCompletePercent);
                            $('.pblabel1' + item.BoardID).text(fullvalue + "%");
                            var progressbar = $('#progressbar1' + item.BoardID).progressbar({
                                value: fullvalue,
                                change: function (event, ui) {
                                    var newVal = $(this).progressbar('option', 'value');
                                    var label = $('.pblabel1' + item.BoardID, this).text(newVal + '%');
                                }
                            });


                            if (fullvalue != 0) {
                                var label = progressbar.find('.pblabel1' + item.BoardID).width(progressbar.width());
                                progressbar.find('.ui-progressbar-value').append(label);
                                $('#progressbar1' + item.BoardID + ' .ui-widget-header').attr('style', 'background-color:' + item.boardcolor + ' !important; width:' + parseInt(fullvalue) + '%;');
                            }
                            else {
                                var label = progressbar.find('.pblabel1' + item.BoardID).clone().width(progressbar.width());
                                progressbar.find('.ui-progressbar-value').append(label);
                            }

                            $('.pblabel1' + item.BoardID).css('width', '');

                            //progress for estimated hour percentage
                            fullvalue = parseInt(item.consumeHourPercentage);
                            consumedhour = item.consumehours + " hr " + "/ " + item.EstimatedHours + " hr ";
                            $('.pblabel3' + item.BoardID).text(consumedhour);
                            var progressbar = $('#progressbar3' + item.BoardID).progressbar({
                                value: fullvalue,
                                change: function (event, ui) {
                                    var newVal = $(this).progressbar('option', 'value');
                                    var label = $('.pblabel3' + item.BoardID, this).text(newVal + '%');
                                }
                            });

                            if (fullvalue != 0) {
                                var label = progressbar.find('.pblabel3' + item.BoardID).width(progressbar.width());
                                progressbar.find('.ui-progressbar-value').append(label);
                                $('#progressbar3' + item.BoardID + ' .ui-widget-header').attr('style', 'background-color:#8f8f8f !important; width:' + parseInt(fullvalue) + '%;');
                            }
                            else {
                                var label = progressbar.find('.pblabel3' + item.BoardID).clone().width(progressbar.width());
                                progressbar.find('.ui-progressbar-value').append(label);

                            }

                            if (parseInt(fullvalue + 2) < 50) {
                                $('#progressbar3' + item.BoardID + ' div').addClass('fitdivspan');
                                $('#progressbar3' + item.BoardID + ' span').css('color', '');
                            }

                            if (fullvalue == 0) {
                                $('#progressbar3' + item.BoardID + ' .ui-widget-header   span').remove();
                            }
                            $('.pblabel3' + item.BoardID).css('width', '')
                            $('#progressbar3' + item.BoardID + ' div span').css('background', '#b2b2b2');
                            i++;

                        });


                    }

                    $(".tabular tbody").append("</tbody>");
                    BindGrid();

                }
                else {
                    $('#BordReportGrid').append("<h2>No Record Found</h2>")
                }

            },
            error: function (err) {

            }
        });
    }
    else {
        alert("Please select any filter");
    }
    $('.hover-frame').slideUp();
});

function ExporttoExcel() {

    alert(1);
    $.ajax({
        type: 'POST',
        url: 'DataTracker.svc/ExportToExcelBoardReport',
        data: {},
        contentType: "application/json; charset=utf-8",
        datatype: 'json',
        async: false,
        success: function (data) {
        },
        error: function (err) {
        }

    });
}
$("#Exportboardreport").live("click", (function () {

    if ($('#BordReportGrid').css('display') == 'block') {
        var idArray = [];
        $('.board-task-red').each(function () {

            idArray.push(this.id);
        });

        $.ajax({
            type: 'POST',
            url: 'DataTracker.svc/ExportToExcelBoardReport',
            data: JSON.stringify({ arrayofboardid: idArray }),
            contentType: "application/json; charset=utf-8",
            datatype: 'json',
            async: false,
            success: function (data) {
                if (data.d.length > 0) {
                    var Path = "/Downloadfiles.aspx?" + data.d;
                    document.location.href = Path;
                }
            },
            error: function (err) {
            }

        });
    }
    else {
        $(".tabular").table2excel({
            exclude: "",
            name: "BoardReport",
            filename: "BoardReport" //do not include extension
        });
    }

}));

$(".sorting li p a").live("click", function () {

    TotalBoardList = "";
    flag = 19;
    listIndex = 0;
    addvaluetomultiplylist = 1;

    $("#DeletedSelectedForcast").css('display', 'none');
    $(".forecast_yellow").css('display', 'none');
    $("#displayselected").parent().css('display', 'none');
    $("#DeleteSelected").css('display', 'none');

    var sortBy = $(this).attr("SortTag");
    var sortOrder = $(this).attr("SortByTag");

    if (sortOrder == "A") {

        $('.sorting li p').each(function () {
            $(this).children('a:eq(1)').children('label').css('border-left', '12px solid #626262');
        });
        $('.sorting li p').each(function () {
            $(this).children('a:first').children('label').css('border-right', '12px solid #b1b0b0');
        });
        $(this).children('label').css('border-left', '12px solid #626262');
        $(this).parent().children('a:first').children('label').css('border-right', '12px solid #b1b0b0');
    }
    else {
        $('.sorting li p').each(function () {
            $(this).children('a:first').children('label').css('border-right', '12px solid #b1b0b0');
        });
        $('.sorting li p').each(function () {
            $(this).children('a:eq(1)').children('label').css('border-left', '12px solid #626262');
        });
        $(this).children('label').css('border-right', '12px solid #626262');
        $(this).parent().children('a').next().children('label').css('border-left', '12px solid #b1b0b0');
    }
    //if (currentText == "Board Name") {
    $.ajax({
        type: 'POST',
        url: 'DataTracker.svc/GetSortedboards',
        data: "{\"sortby\":\"" + sortBy + "\",\"sortorder\":\"" + sortOrder + "\"}",
        contentType: "application/json; charset=utf-8",
        datatype: 'json',

        success: function (data) {
            $('#BordReportGrid').empty();

            TotalBoardList = data.d;
            if (data.d.length > 0) {
                var fullvalue = "";
                $('#loadboard').css('display', 'block');
                if ($('#BordReportGrid').css('display') == 'none') {

                    $('#loadboard').css('display', 'none');
                }

                $('#BordReportGrid').empty();
                var table = $('.tabular').DataTable();
                table.destroy();
                $('.tabular').empty();

                if (data.d.length < 20) {
                    $('#loadboard').css('display', 'none');
                    for (var i = 0; i < data.d.length; i++) {

                        if (parseInt(TotalBoardList[i].EffortsToday) > 0) {
                            if (TotalBoardList[i].Delay > 0) {
                                $('#BordReportGrid').append("<div id=" + TotalBoardList[i].BoardID + " class='col-md-3 col-sm-6 col-xs-12 board-task-red'><div class='shadow-div'><h3 style='background:#2f77a5'>" + TotalBoardList[i].BoardName + "<div class='hover-frame'><div class='hoverdiv' myhovertag='board'><a target='_blank' href='" + DomainURL + "/board.aspx?" + TotalBoardList[i].BoardID + "'><img class='a' src='Styles/BoardReport/images/board-hiden-hover.png'/><label>Board</label></a></div> <div class='hoverdiv' myhovertag='Summary'><a target='_blank' href='" + DomainURL + "/boardSummeryReport.aspx?BID?=" + TotalBoardList[i].BoardID + "'><label class='pull-right'>Summary</label><img src='Styles/BoardReport/images/summary-hiden-hover.png' class='pull-right'/></a></div></div></h3><ul><li><img src='Styles/BoardReport/images/team_grey.png' title='Team Name'><span>" + TotalBoardList[i].TeamName + "</span></li><li><img src='Styles/BoardReport/images/owner_grey.png' title='Board Owner'><span>" + TotalBoardList[i].OwnerName + "</span></li><li><img src='Styles/BoardReport/images/efforts_today.png' title='Efforts'><span>Today's Efforts</span><label style='background: #44bf53' id='BID_" + TotalBoardList[i].BoardID + "'>" + TotalBoardList[i].EffortsToday + "</label></li><li><img src='Styles/BoardReport/images/date.png' title='Create Date'><span>" + TotalBoardList[i].DueDate + "</span></li><li style='padding:0px;'><div id='progressbar" + TotalBoardList[i].BoardID + "'><span id='check' class='pblabel" + TotalBoardList[i].BoardID + " pblabel'></span><img class='Clsdelay' src='Images/delay.png' title='Delay " + TotalBoardList[i].Delay + " hrs' alt=''/></div></li><li style='padding:0px;'><div id='progressbar2" + TotalBoardList[i].BoardID + "'><span id='check1' class='pblabel2" + TotalBoardList[i].BoardID + " pblabel1'></span></div></li></ul><div class='clearfix'></div><div id='bar1' class='barfiller'><div class='tipWrap'><span class='tip'></span></div><span class='fill' data-percentage='30'></span></div></div> ");
                            }
                            else { $('#BordReportGrid').append("<div id=" + TotalBoardList[i].BoardID + " class='col-md-3 col-sm-6 col-xs-12 board-task-red'><div class='shadow-div'><h3 style='background:#2f77a5'>" + TotalBoardList[i].BoardName + "<div class='hover-frame'><div class='hoverdiv' myhovertag='board'><a target='_blank' href='" + DomainURL + "/board.aspx?" + TotalBoardList[i].BoardID + "'><img class='a' src='Styles/BoardReport/images/board-hiden-hover.png'/><label>Board</label></a></div> <div class='hoverdiv' myhovertag='Summary'><a target='_blank' href='" + DomainURL + "/boardSummeryReport.aspx?BID?=" + TotalBoardList[i].BoardID + "'><label class='pull-right'>Summary</label><img src='Styles/BoardReport/images/summary-hiden-hover.png' class='pull-right'/></a></div></div></h3><ul><li><img src='Styles/BoardReport/images/team_grey.png' title='Team Name'><span>" + TotalBoardList[i].TeamName + "</span></li><li><img src='Styles/BoardReport/images/owner_grey.png' title='Board Owner'><span>" + TotalBoardList[i].OwnerName + "</span></li><li><img src='Styles/BoardReport/images/efforts_today.png' title='Efforts'><span>Today's Efforts</span><label style='background: #44bf53' id='BID_" + TotalBoardList[i].BoardID + "'>" + TotalBoardList[i].EffortsToday + "</label></li><li><img src='Styles/BoardReport/images/date.png' title='Create Date'><span>" + TotalBoardList[i].DueDate + "</span></li><li style='padding:0px;'><div id='progressbar" + TotalBoardList[i].BoardID + "'><span id='check' class='pblabel" + TotalBoardList[i].BoardID + " pblabel'></span></div></li><li style='padding:0px;'><div id='progressbar2" + TotalBoardList[i].BoardID + "'><span id='check1' class='pblabel2" + TotalBoardList[i].BoardID + " pblabel1'></span></div></li></ul><div class='clearfix'></div><div id='bar1' class='barfiller'><div class='tipWrap'><span class='tip'></span></div><span class='fill' data-percentage='30'></span></div></div> "); }

                        }
                        else {
                            if (TotalBoardList[i].Delay > 0) {
                                $('#BordReportGrid').append("<div id=" + TotalBoardList[i].BoardID + " class='col-md-3 col-sm-6 col-xs-12 board-task-red'><div class='shadow-div'><h3 style='background:#2f77a5'>" + TotalBoardList[i].BoardName + "<div class='hover-frame'><div class='hoverdiv' myhovertag='board'><a target='_blank' href='" + DomainURL + "/board.aspx?" + TotalBoardList[i].BoardID + "'><img class='a' src='Styles/BoardReport/images/board-hiden-hover.png'/><label>Board</label></a></div> <div class='hoverdiv' myhovertag='Summary'><a target='_blank' href='" + DomainURL + "/boardSummeryReport.aspx?BID?=" + TotalBoardList[i].BoardID + "'><label class='pull-right'>Summary</label><img src='Styles/BoardReport/images/summary-hiden-hover.png' class='pull-right'/></a></div></div></h3><ul><li><img src='Styles/BoardReport/images/team_grey.png' title='Team Name'><span>" + TotalBoardList[i].TeamName + "</span></li><li><img src='Styles/BoardReport/images/owner_grey.png' title='Board Owner'><span>" + TotalBoardList[i].OwnerName + "</span></li><li><img src='Styles/BoardReport/images/efforts_today.png' title='Efforts'><span>Today's Efforts</span><label id='BID_" + TotalBoardList[i].BoardID + "'>" + TotalBoardList[i].EffortsToday + "</label></li><li><img src='Styles/BoardReport/images/date.png' title='Create Date'><span>" + TotalBoardList[i].DueDate + "</span></li><li style='padding:0px;'><div id='progressbar" + TotalBoardList[i].BoardID + "'><span id='check' class='pblabel" + TotalBoardList[i].BoardID + " pblabel'></span><img class='Clsdelay' src='Images/delay.png' title='Delay " + TotalBoardList[i].Delay + " hrs' alt=''/></div></li><li style='padding:0px;'><div id='progressbar2" + TotalBoardList[i].BoardID + "'><span id='check1' class='pblabel2" + TotalBoardList[i].BoardID + " pblabel1'></span></div></li></ul><div class='clearfix'></div><div id='bar1' class='barfiller'><div class='tipWrap'><span class='tip'></span></div><span class='fill' data-percentage='30'></span></div></div> ");
                            }
                            else { $('#BordReportGrid').append("<div id=" + TotalBoardList[i].BoardID + " class='col-md-3 col-sm-6 col-xs-12 board-task-red'><div class='shadow-div'><h3 style='background:#2f77a5'>" + TotalBoardList[i].BoardName + "<div class='hover-frame'><div class='hoverdiv' myhovertag='board'><a target='_blank' href='" + DomainURL + "/board.aspx?" + TotalBoardList[i].BoardID + "'><img class='a' src='Styles/BoardReport/images/board-hiden-hover.png'/><label>Board</label></a></div> <div class='hoverdiv' myhovertag='Summary'><a target='_blank' href='" + DomainURL + "/boardSummeryReport.aspx?BID?=" + TotalBoardList[i].BoardID + "'><label class='pull-right'>Summary</label><img src='Styles/BoardReport/images/summary-hiden-hover.png' class='pull-right'/></a></div></div></h3><ul><li><img src='Styles/BoardReport/images/team_grey.png' title='Team Name'><span>" + TotalBoardList[i].TeamName + "</span></li><li><img src='Styles/BoardReport/images/owner_grey.png' title='Board Owner'><span>" + TotalBoardList[i].OwnerName + "</span></li><li><img src='Styles/BoardReport/images/efforts_today.png' title='Efforts'><span>Today's Efforts</span><label id='BID_" + TotalBoardList[i].BoardID + "'>" + TotalBoardList[i].EffortsToday + "</label></li><li><img src='Styles/BoardReport/images/date.png' title='Create Date'><span>" + TotalBoardList[i].DueDate + "</span></li><li style='padding:0px;'><div id='progressbar" + TotalBoardList[i].BoardID + "'><span id='check' class='pblabel" + TotalBoardList[i].BoardID + " pblabel'></span></div></li><li style='padding:0px;'><div id='progressbar2" + TotalBoardList[i].BoardID + "'><span id='check1' class='pblabel2" + TotalBoardList[i].BoardID + " pblabel1'></span></div></li></ul><div class='clearfix'></div><div id='bar1' class='barfiller'><div class='tipWrap'><span class='tip'></span></div><span class='fill' data-percentage='30'></span></div></div> "); }

                        }
                        //progress for board percentage
                        fullvalue = parseInt(TotalBoardList[i].TaskCompletePercent);
                        $('.pblabel' + TotalBoardList[i].BoardID).text(fullvalue + "%");
                        var progressbar = $('#progressbar' + TotalBoardList[i].BoardID).progressbar({
                            value: fullvalue,
                            change: function (event, ui) {
                                var newVal = $(this).progressbar('option', 'value');
                                var label = $('.pblabel' + TotalBoardList[i].BoardID, this).text(newVal + '%');
                            }
                        });
                        var label = progressbar.find('.pblabel' + TotalBoardList[i].BoardID).clone().width(progressbar.width());
                        progressbar.find('.ui-progressbar-value').append(label);
                        $('#progressbar' + TotalBoardList[i].BoardID).css('background', '#f5f5f5');


                        if (fullvalue != 0) {
                            $('#progressbar' + TotalBoardList[i].BoardID + ' .ui-widget-header').attr('style', 'background-color:' + TotalBoardList[i].boardcolor + ' !important; width:' + fullvalue + '%;');
                        }

                        listIndex = i + 1;


                        //progress for estimated hour percentage
                        fullvalue = parseInt(TotalBoardList[i].consumeHourPercentage);
                        consumedhour = TotalBoardList[i].consumehours + " hr " + "/ " + TotalBoardList[i].EstimatedHours + " hr ";
                        $('.pblabel2' + TotalBoardList[i].BoardID).text(consumedhour);
                        var progressbar = $('#progressbar2' + TotalBoardList[i].BoardID).progressbar({
                            value: fullvalue,
                            change: function (event, ui) {
                                var newVal = $(this).progressbar('option', 'value');
                                var label = $('.pblabel2' + TotalBoardList[i].BoardID, this).text(newVal + '%');
                            }
                        });

                        var label = progressbar.find('.pblabel2' + TotalBoardList[i].BoardID).clone().width(progressbar.width());
                        progressbar.find('.ui-progressbar-value').append(label);
                        $('#progressbar2' + TotalBoardList[i].BoardID).css('background', '#ededed');

                        if (fullvalue != 0) {
                            $('#progressbar2' + TotalBoardList[i].BoardID + ' .ui-widget-header').attr('style', 'background-color:#b2b2b2 !important; width:' + fullvalue + '%;');
                        }
                        $('#progressbar2' + TotalBoardList[i].BoardID + ' span').attr('style', 'padding:5px; background:#8f8f8f;margin-left: 0px;color: #fff;width: 100%;');
                        $('#progressbar2' + TotalBoardList[i].BoardID + ' div span').css('background', '#b2b2b2');
                    }
                }
                else {
                    for (var i = 0; i <= flag; i++) {
                        if (parseInt(TotalBoardList[i].EffortsToday) > 0) {
                            if (TotalBoardList[i].Delay > 0) {
                                $('#BordReportGrid').append("<div id=" + TotalBoardList[i].BoardID + " class='col-md-3 col-sm-6 col-xs-12 board-task-red'><div class='shadow-div'><h3 style='background:#2f77a5'>" + TotalBoardList[i].BoardName + "<div class='hover-frame'><div class='hoverdiv' myhovertag='board'><a target='_blank' href='" + DomainURL + "/board.aspx?" + TotalBoardList[i].BoardID + "'><img class='a' src='Styles/BoardReport/images/board-hiden-hover.png'/><label>Board</label></a></div> <div class='hoverdiv' myhovertag='Summary'><a target='_blank' href='" + DomainURL + "/boardSummeryReport.aspx?BID?=" + TotalBoardList[i].BoardID + "'><label class='pull-right'>Summary</label><img src='Styles/BoardReport/images/summary-hiden-hover.png' class='pull-right'/></a></div></div></h3><ul><li><img src='Styles/BoardReport/images/team_grey.png' title='Team Name'><span>" + TotalBoardList[i].TeamName + "</span></li><li><img src='Styles/BoardReport/images/owner_grey.png' title='Board Owner'><span>" + TotalBoardList[i].OwnerName + "</span></li><li><img src='Styles/BoardReport/images/efforts_today.png' title='Efforts'><span>Today's Efforts</span><label style='background: #44bf53' id='BID_" + TotalBoardList[i].BoardID + "'>" + TotalBoardList[i].EffortsToday + "</label></li><li><img src='Styles/BoardReport/images/date.png' title='Create Date'><span>" + TotalBoardList[i].DueDate + "</span></li><li style='padding:0px;'><div id='progressbar" + TotalBoardList[i].BoardID + "'><span id='check' class='pblabel" + TotalBoardList[i].BoardID + " pblabel'></span><img class='Clsdelay' src='Images/delay.png' title='Delay " + TotalBoardList[i].Delay + " hrs' alt=''/></div></li><li style='padding:0px;'><div id='progressbar2" + TotalBoardList[i].BoardID + "'><span id='check1' class='pblabel2" + TotalBoardList[i].BoardID + " pblabel1'></span></div></li></ul><div class='clearfix'></div><div id='bar1' class='barfiller'><div class='tipWrap'><span class='tip'></span></div><span class='fill' data-percentage='30'></span></div></div> ");
                            }
                            else { $('#BordReportGrid').append("<div id=" + TotalBoardList[i].BoardID + " class='col-md-3 col-sm-6 col-xs-12 board-task-red'><div class='shadow-div'><h3 style='background:#2f77a5'>" + TotalBoardList[i].BoardName + "<div class='hover-frame'><div class='hoverdiv' myhovertag='board'><a target='_blank' href='" + DomainURL + "/board.aspx?" + TotalBoardList[i].BoardID + "'><img class='a' src='Styles/BoardReport/images/board-hiden-hover.png'/><label>Board</label></a></div> <div class='hoverdiv' myhovertag='Summary'><a target='_blank' href='" + DomainURL + "/boardSummeryReport.aspx?BID?=" + TotalBoardList[i].BoardID + "'><label class='pull-right'>Summary</label><img src='Styles/BoardReport/images/summary-hiden-hover.png' class='pull-right'/></a></div></div></h3><ul><li><img src='Styles/BoardReport/images/team_grey.png' title='Team Name'><span>" + TotalBoardList[i].TeamName + "</span></li><li><img src='Styles/BoardReport/images/owner_grey.png' title='Board Owner'><span>" + TotalBoardList[i].OwnerName + "</span></li><li><img src='Styles/BoardReport/images/efforts_today.png' title='Efforts'><span>Today's Efforts</span><label style='background: #44bf53' id='BID_" + TotalBoardList[i].BoardID + "'>" + TotalBoardList[i].EffortsToday + "</label></li><li><img src='Styles/BoardReport/images/date.png' title='Create Date'><span>" + TotalBoardList[i].DueDate + "</span></li><li style='padding:0px;'><div id='progressbar" + TotalBoardList[i].BoardID + "'><span id='check' class='pblabel" + TotalBoardList[i].BoardID + " pblabel'></span></div></li><li style='padding:0px;'><div id='progressbar2" + TotalBoardList[i].BoardID + "'><span id='check1' class='pblabel2" + TotalBoardList[i].BoardID + " pblabel1'></span></div></li></ul><div class='clearfix'></div><div id='bar1' class='barfiller'><div class='tipWrap'><span class='tip'></span></div><span class='fill' data-percentage='30'></span></div></div> "); }

                        }
                        else {
                            if (TotalBoardList[i].Delay > 0) {
                                $('#BordReportGrid').append("<div id=" + TotalBoardList[i].BoardID + " class='col-md-3 col-sm-6 col-xs-12 board-task-red'><div class='shadow-div'><h3 style='background:#2f77a5'>" + TotalBoardList[i].BoardName + "<div class='hover-frame'><div class='hoverdiv' myhovertag='board'><a target='_blank' href='" + DomainURL + "/board.aspx?" + TotalBoardList[i].BoardID + "'><img class='a' src='Styles/BoardReport/images/board-hiden-hover.png'/><label>Board</label></a></div> <div class='hoverdiv' myhovertag='Summary'><a target='_blank' href='" + DomainURL + "/boardSummeryReport.aspx?BID?=" + TotalBoardList[i].BoardID + "'><label class='pull-right'>Summary</label><img src='Styles/BoardReport/images/summary-hiden-hover.png' class='pull-right'/></a></div></div></h3><ul><li><img src='Styles/BoardReport/images/team_grey.png' title='Team Name'><span>" + TotalBoardList[i].TeamName + "</span></li><li><img src='Styles/BoardReport/images/owner_grey.png' title='Board Owner'><span>" + TotalBoardList[i].OwnerName + "</span></li><li><img src='Styles/BoardReport/images/efforts_today.png' title='Efforts'><span>Today's Efforts</span><label id='BID_" + TotalBoardList[i].BoardID + "'>" + TotalBoardList[i].EffortsToday + "</label></li><li><img src='Styles/BoardReport/images/date.png' title='Create Date'><span>" + TotalBoardList[i].DueDate + "</span></li><li style='padding:0px;'><div id='progressbar" + TotalBoardList[i].BoardID + "'><span id='check' class='pblabel" + TotalBoardList[i].BoardID + " pblabel'></span><img class='Clsdelay' src='Images/delay.png' title='Delay " + TotalBoardList[i].Delay + " hrs' alt=''/></div></li><li style='padding:0px;'><div id='progressbar2" + TotalBoardList[i].BoardID + "'><span id='check1' class='pblabel2" + TotalBoardList[i].BoardID + " pblabel1'></span></div></li></ul><div class='clearfix'></div><div id='bar1' class='barfiller'><div class='tipWrap'><span class='tip'></span></div><span class='fill' data-percentage='30'></span></div></div> ");
                            }
                            else { $('#BordReportGrid').append("<div id=" + TotalBoardList[i].BoardID + " class='col-md-3 col-sm-6 col-xs-12 board-task-red'><div class='shadow-div'><h3 style='background:#2f77a5'>" + TotalBoardList[i].BoardName + "<div class='hover-frame'><div class='hoverdiv' myhovertag='board'><a target='_blank' href='" + DomainURL + "/board.aspx?" + TotalBoardList[i].BoardID + "'><img class='a' src='Styles/BoardReport/images/board-hiden-hover.png'/><label>Board</label></a></div> <div class='hoverdiv' myhovertag='Summary'><a target='_blank' href='" + DomainURL + "/boardSummeryReport.aspx?BID?=" + TotalBoardList[i].BoardID + "'><label class='pull-right'>Summary</label><img src='Styles/BoardReport/images/summary-hiden-hover.png' class='pull-right'/></a></div></div></h3><ul><li><img src='Styles/BoardReport/images/team_grey.png' title='Team Name'><span>" + TotalBoardList[i].TeamName + "</span></li><li><img src='Styles/BoardReport/images/owner_grey.png' title='Board Owner'><span>" + TotalBoardList[i].OwnerName + "</span></li><li><img src='Styles/BoardReport/images/efforts_today.png' title='Efforts'><span>Today's Efforts</span><label id='BID_" + TotalBoardList[i].BoardID + "'>" + TotalBoardList[i].EffortsToday + "</label></li><li><img src='Styles/BoardReport/images/date.png' title='Create Date'><span>" + TotalBoardList[i].DueDate + "</span></li><li style='padding:0px;'><div id='progressbar" + TotalBoardList[i].BoardID + "'><span id='check' class='pblabel" + TotalBoardList[i].BoardID + " pblabel'></span></div></li><li style='padding:0px;'><div id='progressbar2" + TotalBoardList[i].BoardID + "'><span id='check1' class='pblabel2" + TotalBoardList[i].BoardID + " pblabel1'></span></div></li></ul><div class='clearfix'></div><div id='bar1' class='barfiller'><div class='tipWrap'><span class='tip'></span></div><span class='fill' data-percentage='30'></span></div></div> "); }

                        }
                        //progress for board percentage
                        fullvalue = parseInt(TotalBoardList[i].TaskCompletePercent);
                        $('.pblabel' + TotalBoardList[i].BoardID).text(fullvalue + "%");
                        var progressbar = $('#progressbar' + TotalBoardList[i].BoardID).progressbar({
                            value: fullvalue,
                            change: function (event, ui) {
                                var newVal = $(this).progressbar('option', 'value');
                                var label = $('.pblabel' + TotalBoardList[i].BoardID, this).text(newVal + '%');
                            }
                        });
                        var label = progressbar.find('.pblabel' + TotalBoardList[i].BoardID).clone().width(progressbar.width());
                        progressbar.find('.ui-progressbar-value').append(label);
                        $('#progressbar' + TotalBoardList[i].BoardID).css('background', '#f5f5f5');


                        if (fullvalue != 0) {
                            $('#progressbar' + TotalBoardList[i].BoardID + ' .ui-widget-header').attr('style', 'background-color:' + TotalBoardList[i].boardcolor + ' !important; width:' + fullvalue + '%;');
                        }

                        listIndex = i + 1;


                        //progress for estimated hour percentage
                        fullvalue = parseInt(TotalBoardList[i].consumeHourPercentage);
                        consumedhour = TotalBoardList[i].consumehours + " hr " + "/ " + TotalBoardList[i].EstimatedHours + " hr ";
                        $('.pblabel2' + TotalBoardList[i].BoardID).text(consumedhour);
                        var progressbar = $('#progressbar2' + TotalBoardList[i].BoardID).progressbar({
                            value: fullvalue,
                            change: function (event, ui) {
                                var newVal = $(this).progressbar('option', 'value');
                                var label = $('.pblabel2' + TotalBoardList[i].BoardID, this).text(newVal + '%');
                            }
                        });

                        var label = progressbar.find('.pblabel2' + TotalBoardList[i].BoardID).clone().width(progressbar.width());
                        progressbar.find('.ui-progressbar-value').append(label);

                        $('#progressbar2' + TotalBoardList[i].BoardID).css('background', '#ededed');
                        if (fullvalue != 0) {
                            $('#progressbar2' + TotalBoardList[i].BoardID + ' .ui-widget-header').attr('style', 'background-color:#b2b2b2 !important; width:' + fullvalue + '%;');
                        }
                        $('#progressbar2' + TotalBoardList[i].BoardID + ' span').attr('style', 'padding:5px; background:#8f8f8f;margin-left: 0px;color: #fff;width: 100%;');
                        $('#progressbar2' + TotalBoardList[i].BoardID + ' div span').css('background', '#b2b2b2');
                    }
                    //  $('.hover-frame').slideUp();
                }
                //slidup
                $('.hover-frame').slideUp();
                $(".tabular").append('<thead><tr class="blue noExl"><th><img src="Styles/BoardReport/images/name-wh.png">Name</th><th><img src="Styles/BoardReport/images/team-wh.png">Team</th><th><img src="Styles/BoardReport/images/owner-wh.png">Owner</th><th><img src="Styles/BoardReport/images/start_date-wh.png">Start Date</th><th><img src="Styles/BoardReport/images/end_date-wh.png">End Date</th><th><img src="Styles/BoardReport/images/efforts-wh.png">Today' + "'" + 's Efforts</th><th><img src="Styles/BoardReport/images/hours-wh.png">Hours Consumed</th><th><img src="Styles/BoardReport/images/board-wh.png">Board Progress</th><th><img src="Images/action.png">Action</th></tr></thead><tbody>');
                if (usertypeid == "12") {
                    $.each(data.d, function (index, item) {
                        filtersAllBoardid.push(item.BoardID);
                        if (parseInt(item.EffortsToday) > 0) {
                            $('.tabular tbody').append("<tr><td style='border-left:solid 10px " + item.boardcolor + "'><a target='_blank'href=" + DomainURL + "/BoardSummeryReport.aspx?BID=" + item.BoardID + ">" + item.BoardName + "</a><span class='board-sumary-hover-cont' style='background:" + item.boardcolor + "'><a target='_blank' href='" + DomainURL + "/board.aspx?" + item.BoardID + "'><img src='Styles/BoardReport/images/board.png'></a><a target='_blank' href='" + DomainURL + "/boardSummeryReport.aspx?BID?=" + item.BoardID + "'><img src='Styles/BoardReport/images/summary.png'></a></span></td><td>" + item.TeamName + "</td><td>" + item.OwnerName + "</td><td>" + item.CreatedDate + "</td><td>" + item.DueDate + "</td><td><label style='background: #44bf53' id='BID_" + item.BoardID + "'>" + item.EffortsToday + "</label></td><td style='padding:0px;'><div id='progressbar3" + item.BoardID + "'><span id='check' class='pblabel3" + item.BoardID + " pblabel'></span></div></td><td style='padding:0px;'><div id='progressbar1" + item.BoardID + "'><span id='check' class='pblabel1" + item.BoardID + " pblabel'></span></div></td><td><img src='Images/icon1.png' class='archiveandcomplete' title='Archive Board' ArchiveTag='archive'/><img src='Images/icon2.png' title='Complete Board' ArchiveTag='complete' class='archiveandcomplete'/></td></tr>");
                        }
                        else {
                            $('.tabular tbody').append("<tr><td style='border-left:solid 10px " + item.boardcolor + "'><a target='_blank'href=" + DomainURL + "/BoardSummeryReport.aspx?BID=" + item.BoardID + ">" + item.BoardName + "</a><span class='board-sumary-hover-cont' style='background:" + item.boardcolor + "'><a target='_blank' href='" + DomainURL + "/board.aspx?" + item.BoardID + "'><img src='Styles/BoardReport/images/board.png'></a><a target='_blank' href='" + DomainURL + "/boardSummeryReport.aspx?BID?=" + item.BoardID + "'><img src='Styles/BoardReport/images/summary.png'></a></span></td><td>" + item.TeamName + "</td><td>" + item.OwnerName + "</td><td>" + item.CreatedDate + "</td><td>" + item.DueDate + "</td><td><label id='BID_" + item.BoardID + "'>" + item.EffortsToday + "</label></td><td style='padding:0px;'><div id='progressbar3" + item.BoardID + "'><span id='check' class='pblabel3" + item.BoardID + " pblabel'></span></div></td><td style='padding:0px;'><div id='progressbar1" + item.BoardID + "'><span id='check' class='pblabel1" + item.BoardID + " pblabel'></span></div></td><td><img src='Images/icon1.png' title='Archive Board' class='archiveandcomplete' ArchiveTag='archive'/><img src='Images/icon2.png' class='archiveandcomplete' title='Complete Board' ArchiveTag='complete'/></td></tr>");
                        }

                        //progress for board percentage
                        fullvalue = parseInt(item.TaskCompletePercent);
                        $('.pblabel1' + item.BoardID).text(fullvalue + "%");
                        var progressbar = $('#progressbar1' + item.BoardID).progressbar({
                            value: fullvalue,
                            change: function (event, ui) {
                                var newVal = $(this).progressbar('option', 'value');
                                var label = $('.pblabel1' + item.BoardID, this).text(newVal + '%');
                            }
                        });


                        if (fullvalue != 0) {
                            var label = progressbar.find('.pblabel1' + item.BoardID).width(progressbar.width());
                            progressbar.find('.ui-progressbar-value').append(label);
                            $('#progressbar1' + item.BoardID + ' .ui-widget-header').attr('style', 'background-color:' + item.boardcolor + ' !important; width:' + parseInt(fullvalue) + '%;');
                        }
                        else {
                            var label = progressbar.find('.pblabel1' + item.BoardID).clone().width(progressbar.width());
                            progressbar.find('.ui-progressbar-value').append(label);
                        }

                        $('.pblabel1' + item.BoardID).css('width', '');

                        //progress for estimated hour percentage
                        fullvalue = parseInt(item.consumeHourPercentage);
                        consumedhour = item.consumehours + " hr " + "/ " + item.EstimatedHours + " hr ";
                        $('.pblabel3' + item.BoardID).text(consumedhour);
                        var progressbar = $('#progressbar3' + item.BoardID).progressbar({
                            value: fullvalue,
                            change: function (event, ui) {
                                var newVal = $(this).progressbar('option', 'value');
                                var label = $('.pblabel3' + item.BoardID, this).text(newVal + '%');
                            }
                        });

                        if (fullvalue != 0) {
                            var label = progressbar.find('.pblabel3' + item.BoardID).width(progressbar.width());
                            progressbar.find('.ui-progressbar-value').append(label);
                            $('#progressbar3' + item.BoardID + ' .ui-widget-header').attr('style', 'background-color:#8f8f8f !important; width:' + parseInt(fullvalue) + '%;');
                        }
                        else {
                            var label = progressbar.find('.pblabel3' + item.BoardID).clone().width(progressbar.width());
                            progressbar.find('.ui-progressbar-value').append(label);

                        }

                        if (parseInt(fullvalue + 2) < 50) {
                            $('#progressbar3' + item.BoardID + ' div').addClass('fitdivspan');
                            $('#progressbar3' + item.BoardID + ' span').css('color', 'white');
                        }

                        if (fullvalue == 0) {
                            $('#progressbar3' + item.BoardID + ' .ui-widget-header   span').remove();
                        }
                        $('.pblabel3' + item.BoardID).css('width', '')
                        $('#progressbar3' + item.BoardID + ' div span').css('background', '#b2b2b2');
                        i++;

                    });
                }
                else {
                    $.each(data.d, function (index, item) {
                        filtersAllBoardid.push(item.BoardID);
                        if (item.flag == 1) {
                            if (parseInt(item.EffortsToday) > 0) {
                                $('.tabular tbody').append("<tr><td style='border-left:solid 10px " + item.boardcolor + "'><a target='_blank' href=" + DomainURL + "/BoardSummeryReport.aspx?BID=" + item.BoardID + ">" + item.BoardName + "</a><span class='board-sumary-hover-cont' style='background:" + item.boardcolor + "'><a target='_blank' href='" + DomainURL + "/board.aspx?" + item.BoardID + "'><img src='Styles/BoardReport/images/board.png'></a><a target='_blank' href='" + DomainURL + "/boardSummeryReport.aspx?BID?=" + item.BoardID + "'><img src='Styles/BoardReport/images/summary.png'></a></span></td><td>" + item.TeamName + "</td><td>" + item.OwnerName + "</td><td>" + item.CreatedDate + "</td><td>" + item.DueDate + "</td><td><label style='background: #44bf53' id='BID_" + item.BoardID + "'>" + item.EffortsToday + "</label></td><td style='padding:0px;'><div id='progressbar3" + item.BoardID + "'><span id='check' class='pblabel3" + item.BoardID + " pblabel'></span></div></td><td style='padding:0px;'><div id='progressbar1" + item.BoardID + "'><span id='check' class='pblabel1" + item.BoardID + " pblabel'></span></div></td><td><img src='Images/icon1.png' class='archiveandcomplete' title='Archive Board' ArchiveTag='archive'/><img src='Images/icon2.png' title='Complete Board' ArchiveTag='complete' class='archiveandcomplete'/></td></tr>");
                            }
                            else {
                                $('.tabular tbody').append("<tr><td style='border-left:solid 10px " + item.boardcolor + "'><a target='_blank'href=" + DomainURL + "/BoardSummeryReport.aspx?BID=" + item.BoardID + ">" + item.BoardName + "</a><span class='board-sumary-hover-cont' style='background:" + item.boardcolor + "'><a target='_blank' href='" + DomainURL + "/board.aspx?" + item.BoardID + "'><img src='Styles/BoardReport/images/board.png'></a><a target='_blank' href='" + DomainURL + "/boardSummeryReport.aspx?BID?=" + item.BoardID + "'><img src='Styles/BoardReport/images/summary.png'></a></span></td><td>" + item.TeamName + "</td><td>" + item.OwnerName + "</td><td>" + item.CreatedDate + "</td><td>" + item.DueDate + "</td><td><label id='BID_" + item.BoardID + "'>" + item.EffortsToday + "</label></td><td style='padding:0px;'><div id='progressbar3" + item.BoardID + "'><span id='check' class='pblabel3" + item.BoardID + " pblabel'></span></div></td><td style='padding:0px;'><div id='progressbar1" + item.BoardID + "'><span id='check' class='pblabel1" + item.BoardID + " pblabel'></span></div></td><td><img src='Images/icon1.png' title='Archive Board' class='archiveandcomplete' ArchiveTag='archive'/><img src='Images/icon2.png' class='archiveandcomplete' title='Complete Board' ArchiveTag='complete'/></td></tr>");
                            }

                        }
                        else {
                            if (parseInt(item.EffortsToday) > 0) {
                                $('.tabular tbody').append("<tr><td style='border-left:solid 10px " + item.boardcolor + "'><a target='_blank'href=" + DomainURL + "/BoardSummeryReport.aspx?BID=" + item.BoardID + ">" + item.BoardName + "</a><span class='board-sumary-hover-cont' style='background:" + item.boardcolor + "'><a target='_blank' href='" + DomainURL + "/board.aspx?" + item.BoardID + "'><img src='Styles/BoardReport/images/board.png'></a><a target='_blank' href='" + DomainURL + "/boardSummeryReport.aspx?BID?=" + item.BoardID + "'><img src='Styles/BoardReport/images/summary.png'></a></span></td><td>" + item.TeamName + "</td><td>" + item.OwnerName + "</td><td>" + item.CreatedDate + "</td><td>" + item.DueDate + "</td><td><label style='background: #44bf53' id='BID_" + item.BoardID + "'>" + item.EffortsToday + "</label></td><td style='padding:0px;'><div id='progressbar3" + item.BoardID + "'><span id='check' class='pblabel3" + item.BoardID + " pblabel'></span></div></td><td style='padding:0px;'><div id='progressbar1" + item.BoardID + "'><span id='check' class='pblabel1" + item.BoardID + " pblabel'></span></div></td><td></td></tr>");
                            }
                            else {
                                $('.tabular tbody').append("<tr><td style='border-left:solid 10px " + item.boardcolor + "'><a target='_blank'href=" + DomainURL + "/BoardSummeryReport.aspx?BID=" + item.BoardID + ">" + item.BoardName + "</a><span class='board-sumary-hover-cont' style='background:" + item.boardcolor + "'><a target='_blank' href='" + DomainURL + "/board.aspx?" + item.BoardID + "'><img src='Styles/BoardReport/images/board.png'></a><a target='_blank' href='" + DomainURL + "/boardSummeryReport.aspx?BID?=" + item.BoardID + "'><img src='Styles/BoardReport/images/summary.png'></a></span></td><td>" + item.TeamName + "</td><td>" + item.OwnerName + "</td><td>" + item.CreatedDate + "</td><td>" + item.DueDate + "</td><td><label id='BID_" + item.BoardID + "'>" + item.EffortsToday + "</label></td><td style='padding:0px;'><div id='progressbar3" + item.BoardID + "'><span id='check' class='pblabel3" + item.BoardID + " pblabel'></span></div></td><td style='padding:0px;'><div id='progressbar1" + item.BoardID + "'><span id='check' class='pblabel1" + item.BoardID + " pblabel'></span></div></td><td></td></tr>");
                            }

                        }

                        //progress for board percentage
                        fullvalue = parseInt(item.TaskCompletePercent);
                        $('.pblabel1' + item.BoardID).text(fullvalue + "%");
                        var progressbar = $('#progressbar1' + item.BoardID).progressbar({
                            value: fullvalue,
                            change: function (event, ui) {
                                var newVal = $(this).progressbar('option', 'value');
                                var label = $('.pblabel1' + item.BoardID, this).text(newVal + '%');
                            }
                        });


                        if (fullvalue != 0) {
                            var label = progressbar.find('.pblabel1' + item.BoardID).width(progressbar.width());
                            progressbar.find('.ui-progressbar-value').append(label);
                            $('#progressbar1' + item.BoardID + ' .ui-widget-header').attr('style', 'background-color:' + item.boardcolor + ' !important; width:' + parseInt(fullvalue) + '%;');
                        }
                        else {
                            var label = progressbar.find('.pblabel1' + item.BoardID).clone().width(progressbar.width());
                            progressbar.find('.ui-progressbar-value').append(label);
                        }

                        $('.pblabel1' + item.BoardID).css('width', '');

                        //progress for estimated hour percentage
                        fullvalue = parseInt(item.consumeHourPercentage);
                        consumedhour = item.consumehours + " hr " + "/ " + item.EstimatedHours + " hr ";
                        $('.pblabel3' + item.BoardID).text(consumedhour);
                        var progressbar = $('#progressbar3' + item.BoardID).progressbar({
                            value: fullvalue,
                            change: function (event, ui) {
                                var newVal = $(this).progressbar('option', 'value');
                                var label = $('.pblabel3' + item.BoardID, this).text(newVal + '%');
                            }
                        });

                        if (fullvalue != 0) {
                            var label = progressbar.find('.pblabel3' + item.BoardID).width(progressbar.width());
                            progressbar.find('.ui-progressbar-value').append(label);
                            $('#progressbar3' + item.BoardID + ' .ui-widget-header').attr('style', 'background-color:#8f8f8f !important; width:' + parseInt(fullvalue) + '%;');
                        }
                        else {
                            var label = progressbar.find('.pblabel3' + item.BoardID).clone().width(progressbar.width());
                            progressbar.find('.ui-progressbar-value').append(label);

                        }

                        if (parseInt(fullvalue + 2) < 50) {
                            $('#progressbar3' + item.BoardID + ' div').addClass('fitdivspan');
                            $('#progressbar3' + item.BoardID + ' span').css('color', '');
                        }

                        if (fullvalue == 0) {
                            $('#progressbar3' + item.BoardID + ' .ui-widget-header   span').remove();
                        }
                        $('.pblabel3' + item.BoardID).css('width', '')
                        $('#progressbar3' + item.BoardID + ' div span').css('background', '#b2b2b2');
                        i++;

                    });


                }
                $(".tabular tbody").append("</tbody>");
                BindGrid();

            }
            else {
                $('#BordReportGrid').append("<h2>No Record Found</h2>");
                $('#loadboard').css('display', 'none');
            }
        },
        error: function (err) {

        }
    });
    //  }
    $('.hover-frame').slideUp();
});

$(".searchbysort li").live("click", function () {
    currentText1 = $(this).attr("mytag");
});

function OpenFilter() {
    var checktxt = $("#srch").val();

    //   $('.icon-div-nselect-one').addClass('icon-one-select');
    if (checktxt != "" && checktxt != undefined) {
        $('.search-cntnr-drop-down').css('display', 'block');
    }
    else {
        $('.search-cntnr-drop-down').css('display', 'none');
    }
}

function ChengeFilterIcon(text) {
    $('#displayselectedIcon').attr('src', '');
    if (text == "Favourites") {
        $("#displayselectedIcon").attr("src", "Styles/BoardReport/images/favourite_click.png");
    }
    else if (text == "Alerts") {
        $("#displayselectedIcon").attr("src", "Styles/BoardReport/images/forecast_click.png");
    }
    else if (text == "All") {
        $("#displayselectedIcon").attr("src", "Styles/BoardReport/images/all-nav.png");
    }
    else if (text == "Account Manager") {
        $("#displayselectedIcon").attr("src", "Styles/BoardReport/images/account_click.png");
    }
    else if (text == "Client") {
        $("#displayselectedIcon").attr("src", "Styles/BoardReport/images/client_click.png");
    }
    else if (text == "Team") {
        $("#displayselectedIcon").attr("src", "Styles/BoardReport/images/team_click.png");
    }
    else if (text == "Owner") {
        $("#displayselectedIcon").attr("src", "Styles/BoardReport/images/owner_click.png");
    }
    else if (text == "End Date") {
        $("#displayselectedIcon").attr("src", "Styles/BoardReport/images/enddate_click.png");
    }
    if (text == "All")
    { $('#displayselectedIcon').css("background-color", "white"); }
    else
    { $('#displayselectedIcon').css("background-color", ""); }
}

$('.archiveandcomplete').live('click', function () {

    var type = $(this).attr('ArchiveTag');
    var boardid = $(this).parent().parent().children('td:eq(5)').children().attr('id');
    var n = boardid.lastIndexOf('_');
    var result = boardid.substring(n + 1);
    $.ajax({
        url: 'Datatracker.svc/ChangeBoardAction?boardid=' + result + '&type="' + type + '"',
        type: 'GET',
        contentType: 'application/json;charset=utf-8',
        dataType: 'json',
        success: function (data) {

            if (data.d == "ok") {
                if (type == "archive") {
                    alert("Archive Successfully");
                }
                else if ("complete") {
                    alert("Complete Successfully");
                }
                TotalBoardOnReport();
            }
            else if ("faild") {
                if (type == "archive") {
                    alert("Archive failed");
                }
                else if ("complete") {
                    alert("Complete failed");
                }
            }
        },
        error: function () {

        }
    });
});



$(".Clsdelay").live('mouseover', function () {
    $(this).attr("src", "Images/delay_click.png");
});
$(".Clsdelay").live('mouseout', function () {
    $(this).attr("src", "Images/delay.png");
});


$('.board-task-red h3').live('mouseenter', function () {

    $(this).find('.hover-frame').slideDown();

});

$('.board-task-red h3').live('mouseleave', function () {

    $(this).find('.hover-frame').slideUp();

});

$('.hoverdiv').live('mouseenter', function () {

    var tagText = $(this).attr('myhovertag');
    if (tagText == 'board') {

        $(this).children('a').children('img:first').attr('src', 'Styles/BoardReport/images/board_click-hover-after.png');

    }
    else if (tagText == 'Summary') {
        $(this).children('a').children('img:first').attr('src', 'Styles/BoardReport/images/summary_click-hover-after.png');

    }

});
$('.hoverdiv').live('mouseleave', function () {

    var tagText = $(this).attr('myhovertag');
    if (tagText == 'board') {
        $(this).children('a').children('img:first').attr("src", "Styles/BoardReport/images//board-hiden-hover.png");

    }
    else if (tagText == 'Summary') {
        $(this).children('a').children('img:first').attr("src", "Styles/BoardReport/images/summary-hiden-hover.png");

    }


});