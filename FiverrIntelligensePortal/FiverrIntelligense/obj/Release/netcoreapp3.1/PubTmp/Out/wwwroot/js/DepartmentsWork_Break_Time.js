﻿
var late_Date = '';
var current_userId = '';
var current_userTypeid = '';
var ldatee = '';
var today_Daterr = '';
var today_Date = '';
var excelfilename = "";

function Setusersids(userid, utypeid) {
    current_userId = userid;
    current_userTypeid = utypeid;
}

function ShowDepartmentworkList(Fdate, Ldate) {
    $("#Departmentgrid")[0].innerHTML = "";
    if ($("#Departmentgrid").data("kendoWindow") != null) {
        $('#Departmentgrid').data().kendoGrid.destroy();
        $('#Departmentgrid').empty();
    }
    var dataSourceDetail = "";
    dataSourceDetail = new kendo.data.DataSource({
        pageSize: 10,
        serverPaging: true,
        schema: {
            data: "d",
            total: function (d) {
                if (d.d.length > 0)
                    return d.d[0].TotalCount
            },
            model: {
                id: "ID",
                fields: {
                    ID: { editable: false, type: "number" },
                    DeName: { editable: false, type: "string" },
                    TotalWorkTime: { editable: false, type: "string" },
                    TotalBreakTime: { editable: false, type: "string" },
                    TotalIdleTimes: { editable: false, type: "string" },
                    dId: { editable: false, type: "number" }

                }
            }

        },

        transport: {
            read: function (options) {
                $.ajax({
                    type: "GET",
                    url: "/DataTracker.svc/GetDepartmentsTaskDetails",
                    dataType: "json",
                    data: {

                        Page: options.data.page,
                        PageSize: options.data.pageSize,
                        skip: options.data.skip,
                        take: options.data.take,
                        Fdate: Fdate,
                        Ldate: Ldate
                    },

                    success: function (result) {
                        options.success(result);

                    }
                });
            }
        }

    });
    if ($("#Departmentgrid").length > 0)
    { $("#Departmentgrid")[0].innerHTML = ""; }
    var element = $("#Departmentgrid").kendoGrid({
        dataSource: dataSourceDetail,
        //        toolbar: ["excel"],
        excel: {
            fileName: excelfilename + ".xlsx",
            filterable: true,
            allPages: true
        },
        detailInit: TeamMemberWorkCount,
        dataBound: function () {

        },
        pageable: {
            // batch: true,
            pageSize: 10,
            refresh: true,
            pageSizes: true,
            input: true
        },
        columns: [
                            {
                                field: "DeName",
                                title: "Team Name",
                                width: "40%"
                            },
                            {
                                field: "TotalWorkTime",
                                title: "Work Time (hh:mm:ss)",
                                width: "20%"
                            },
                            {
                                field: "TotalBreakTime",
                                title: "Break Time (hh:mm:ss)",
                                width: "20%"


                            },
                            {
                                field: "TotalIdleTimes",
                                title: "Idle  Time (hh:mm:ss)",
                                 width: "20%"
                            }

                        ],
        editable: false,
        sortable: true,
        selectable: true

    });
}

function lastseven() {
    $("#Departmentgrid").innerHTML = "";
    $("#Departmentgrid").html("");
    var cdate = new Date();
    var dd = cdate.getDate();
    var mm = cdate.getMonth() + 1;
    var yyyy = cdate.getFullYear();
    today_Date = yyyy + '/' + mm + '/' + dd;
    ShowDatediv();
    setdatevall();
    if ($("#Lastsevendays").is(':checked')) {
        //ShowDepartmentworkList(Pre_Date, today_Date);
        var msecPerDay = 7 * 24 * 60 * 60 * 1000;
        var Seven_days_ago = new Date(cdate.getTime() - msecPerDay);
        var sdd = Seven_days_ago.getDate();
        var smm = Seven_days_ago.getMonth() + 1;
        var syyyy = Seven_days_ago.getFullYear();
        var Pre_Date = syyyy + '/' + smm + '/' + sdd;
        late_Date = Pre_Date;
        excelfilename = "Last_7_days_TimeReport";
        if (current_userTypeid == '12')
            ShowDepartmentworkList(Pre_Date, today_Date);
        else {
            $("#Departmentgrid")[0].innerHTML = "";
            if ($("#Departmentgrid").data("kendoWindow") != null) {
                $('#Departmentgrid').data().kendoGrid.destroy();
                $('#Departmentgrid').empty();
            }
            ShowDepartmentworkTL(current_userId, Pre_Date, today_Date);
        }
    }
    else if ($("#Lastthirty").is(':checked')) {
        var msecPerDay = 30 * 24 * 60 * 60 * 1000;
        var Seven_days_ago = new Date(cdate.getTime() - msecPerDay);
        var sdd = Seven_days_ago.getDate();
        var smm = Seven_days_ago.getMonth() + 1;
        var syyyy = Seven_days_ago.getFullYear();
        var Pre_Date = syyyy + '/' + smm + '/' + sdd;
        late_Date = Pre_Date;
        excelfilename = "Last_30_days_TimeReport";
        // ShowDepartmentworkList(Pre_Date, today_Date);
        if (current_userTypeid == '12')
            ShowDepartmentworkList(Pre_Date, today_Date);
        else {
            $("#Departmentgrid")[0].innerHTML = "";
            if ($("#Departmentgrid").data("kendoWindow") != null) {
                $('#Departmentgrid').data().kendoGrid.destroy();
                $('#Departmentgrid').empty();
            }
            ShowDepartmentworkTL(current_userId, Pre_Date, today_Date);
        }
    }
    else if ($("#CustomRadiobt").is(':checked')) {

        Pre_Date = $("#dtpStartDate").val();
        today_Date = $("#dtpEndDate").val();
        late_Date = Pre_Date;
        excelfilename = "From" + Pre_Date + "-" + today_Date + "Timereport";
        if (current_userTypeid == '12')
            ShowDepartmentworkList(Pre_Date, today_Date);
        else {
            $("#Departmentgrid")[0].innerHTML = "";
            if ($("#Departmentgrid").data("kendoWindow") != null) {
                $('#Departmentgrid').data().kendoGrid.destroy();
                $('#Departmentgrid').empty();
            }
            ShowDepartmentworkTL(current_userId, Pre_Date, today_Date);
        }
    }
    else {
        //alert("hh");
        return false;
    }
}

function TeamMemberWorkCount(e) {
    $("<div/>").appendTo(e.detailCell).kendoGrid({
        dataSource: {
            transport: {
                read: {
                    url: "/DataTracker.svc/GetIndividualWorkTime",
                    data: {
                        TeamID: e.data.dId,
                        FDate: late_Date,
                        Ldate: today_Date
                    }

                }
            },
            serverPaging: true,
            serverSorting: true,
            serverFiltering: true,
            pageSize: 100,
            schema: {
                data: "d",
                total: function (d) {
                    if (d != null) {
                        if (d.d.length > 0)
                            return d.d[0].TotalCount
                    }
                },
                model: {
                    id: "ID",
                    fields: {
                        ID: { editable: false, type: "number" },
                        UName: { editable: false, type: "string" },
                        TotalWorkTimeI: { editable: false, type: "string" },
                        TotalBreakTimeI: { editable: false, type: "string" },
                        TotalIdleTimeI: { editable: false, type: "string" },
                        UserID: { editable: false, type: "string" }

                    }
                }

            }

        },
        columns: [
                            {
                                field: "UName",
                                title: "Name",
                                width: "40%"
                            },
                            {
                                field: "TotalWorkTimeI",
                                title: "Work Time (hh:mm:ss)",
                                width: "15%"


                            },
                            {
                                field: "TotalBreakTimeI",
                                title: "Break Time (hh:mm:ss)",
                                width: "15%"
                            },
                             {
                                 field: "TotalIdleTimeI",
                                 title: "Idle Time (hh:mm:ss)",
                                 width: "15%"
                             },
                            { command: { text: "Show tasks list", click: ShowTaskDetailsd }, title: "", width: "15%" }
                        ],
        editable: false,
        sortable: true,
        selectable: true

    });


}

function ShowDepartmentworkTL(Cuser, Fdate, Ldate) {
    $("#Departmentgrid").innerHTML = "";
    $("#Departmentgrid").html("");
    $("#Departmentgrid")[0].innerHTML = "";
    if ($("#Departmentgrid").data("kendoWindow") != null) {
        $('#Departmentgrid').data().kendoGrid.destroy();
        $('#Departmentgrid').empty();
    }
    var dataSourceDetail = "";
    dataSourceDetail = new kendo.data.DataSource({
        pageSize: 15,
        serverPaging: true,
        schema: {
            data: "d",
            total: function (d) {
                if (d.d.length > 0)
                    return d.d[0].TotalCount
            },
            model: {
                id: "ID",
                fields: {
                    ID: { editable: false, type: "number" },
                    UName: { editable: false, type: "string" },
                    TotalWorkTimeI: { editable: false, type: "string" },
                    TotalBreakTimeI: { editable: false, type: "string" },
                    TotalIdleTimeI: { editable: false, type: "string" },
                    UserID: { editable: false, type: "string" }

                }
            }

        },

        transport: {
            read: function (options) {
                $.ajax({
                    type: "GET",
                    url: "/DataTracker.svc/GetIndividualWorkTimeForTL",
                    dataType: "json",
                    data: {

                        Page: options.data.page,
                        PageSize: options.data.pageSize,
                        skip: options.data.skip,
                        take: options.data.take,
                        Cuser: Cuser,
                        Fdate: Fdate,
                        Ldate: Ldate
                    },

                    success: function (result) {
                        options.success(result);

                    }
                });
            }
        }

    });
    if ($("#Departmentgrid").length > 0)
    { $("#Departmentgrid")[0].innerHTML = ""; }
    var element = $("#Departmentgrid").kendoGrid({
        dataSource: dataSourceDetail,
        //        toolbar: ["excel"],
        excel: {
            fileName: excelfilename + ".xlsx",
            filterable: true,
            allPages: true
        },
        dataBound: function () {

        },
        pageable: {
            batch: true,
            pageSize: 15,
            refresh: true,
            pageSizes: true,
            input: true
        },
        columns: [
                            {
                                field: "UName",
                                title: "Name",
                                
                                width: "20%"
                            },
                            {
                                field: "TotalWorkTimeI",
                                title: "Work Time (hh:mm:ss)",
                                width: "20%"
                            },
                            {
                                field: "TotalBreakTimeI",
                                title: "Break Time (hh:mm:ss)",
                                width: "20%"


                            },
                            {
                                field: "TotalIdleTimeI",
                                title: "Idle Time (hh:mm:ss)",
                                width: "20%"


                            },
                            { command: { text: "Show tasks list", click: ShowTaskDetailsd2 }, title: "", width: "20%" }

                        ],
        editable: false,
        sortable: true,
        selectable: true

    });
}
function ShowDatediv() {
    if ($("#CustomRadiobt").is(':checked')) {
        $('#AllDateDIv').css('display', 'block');
    }
    else {
        $('#AllDateDIv').css('display', 'none');
    }

}
function callbingridd() {
    var fdateval = $("#dtpStartDate").val();
    var ldateval = $("#dtpEndDate").val();
    if ($("#CustomRadiobt").is(':checked')) {

        if (current_userTypeid == '12')
            ShowDepartmentworkList(fdateval, ldateval);
        else {

            ShowDepartmentworkTL(current_userId, fdateval, ldateval);
        }
    }
    else {
    }

}

function ShowTaskDetailsd(e) {
    e.preventDefault();
    if ($("#ShowworkCoubtdiv").data("kendoWindow") != null) {
        var StatusReportwindow = $("#ShowworkCoubtdiv").data("kendoWindow");
        StatusReportwindow.close();
    }

    var dataItem = this.dataItem($(e.currentTarget).closest("tr"));
    if (dataItem != null) {
        var unamee = dataItem.UName;
        var cUid = dataItem.UserID;
        GetDatafromCustomDate(unamee, cUid, ldatee, today_Daterr);
    }

}

function ShowTaskDetailsd2(e) {
    e.preventDefault();
    if ($("#ShowworkCoubtdiv").data("kendoWindow") != null) {
        var StatusReportwindow = $("#ShowworkCoubtdiv").data("kendoWindow");
        StatusReportwindow.close();
    }

    var dataItem = this.dataItem($(e.currentTarget).closest("tr"));
    if (dataItem != null) {
        var unamee = dataItem.UName;
        var cUid = dataItem.UserID;
        GetDatafromCustomDatePopup(unamee, cUid, ldatee, today_Daterr);
    }
}


function GetDatafromCustomDate(username, cUid, ldatee, today_Daterr) {

    if ($("#ShowworkCoubtdiv").data("kendoWindow") != null) {
        var StatusReport = $("#ShowworkCoubtdiv").data("kendoWindow");
        StatusReport.close();
    }
    cUid = cUid.trim() + ',' + ldatee.trim() + ',' + today_Daterr.trim() + ',' + excelfilename.trim();
    cUid = cUid.trim();
    var Title = "Task List (" + username + ")";

    var windowElement = $("#ShowworkCoubtdiv").kendoWindow({
        iframe: true,
        content: "ShowDeskTopWorkList.htm?id=" + cUid + "",
        modal: true,
        width: "990px",
        height: "575px",
        position: {
            top: 10,
            left: 2
        },
        resizable: false,
        actions: ["Close"],
        draggable: false

    });

    var dialog = $("#ShowworkCoubtdiv").data("kendoWindow");
    dialog.setOptions({
        title: Title
    });
    dialog.open();
    dialog.center();
    return false;
}
function GetDatafromCustomDatePopup(username, cUid, ldatee, today_Daterr) {

    if ($("#ShowworkCoubtdiv").data("kendoWindow") != null) {
        var StatusReport = $("#ShowworkCoubtdiv").data("kendoWindow");
        StatusReport.close();
    }
    cUid = cUid.trim() + ',' + ldatee.trim() + ',' + today_Daterr.trim() + ',' + excelfilename.trim();
    cUid = cUid.trim();
    var Title = "Task List (" + username + ")";

    var windowElement = $("#ShowworkCoubtdiv").kendoWindow({
        iframe: true,
        content: "ShowDeskTopWorkList.htm?id=" + cUid + "",
        modal: true,
        width: "990px",
        height: "575px",
        position: {
            top: 10,
            left: 2
        },
        resizable: false,
        actions: ["Close"],
        draggable: false

    });
    var dialog = $("#ShowworkCoubtdiv").data("kendoWindow");
    dialog.setOptions({
        title: Title
    });
    dialog.open();
    dialog.center();
    $("#ShowworkCoubtdiv")[0].Style.display = "block";
    return false;
}


function BindIndividualTaskGrid(id, ldatee, today_Daterr, fexename) {

    $("#DivIndividualWorkTaskListGrid")[0].innerHTML = "";
    $("#DivIndividualWorkTaskListGrid").innerHTML = "";
    $("#DivIndividualWorkTaskListGrid").html("");

    if ($("#DivIndividualWorkTaskListGrid").data("kendoWindow") != null) {
        $("#DivIndividualWorkTaskListGrid").data("kendoGrid").dataSource.read();
        $('#DivIndividualWorkTaskListGrid').data().kendoGrid.destroy();
        $('#DivIndividualWorkTaskListGrid').empty();
    }
    var dataSourceDetail = "";
    dataSourceDetail = new kendo.data.DataSource({
        pageSize: 10,
        serverPaging: true,
        schema: {
            data: "d",
            total: function (d) {
                if (d.d.length > 0)
                    return d.d[0].TotalCount
            },
            model: {
                id: "ID",
                fields: {
                    ID: { editable: false, type: "number" },
                    TTitle: { editable: false, type: "string" },
                    StartTime: { editable: false, type: "string" },
                    Createddate: { editable: false, type: "string" },
                    Lastupdatetime: { editable: false, type: "string" },
                    CreatedBy: { editable: false, type: "string" },
                    prority: { editable: false, type: "string" },
                    TotalWorkTime: { editable: false, type: "string" },
                    uiddd: { editable: false, type: "string" },
                    sdated: { editable: false, type: "string" },
                    enddated: { editable: false, type: "string" }


                }
            }

        },

        transport: {
            read: function (options) {
                $.ajax({
                    type: "GET",
                    url: "/DataTracker.svc/GetIndividualWorkList",
                    dataType: "json",
                    data: {

                        Page: options.data.page,
                        PageSize: options.data.pageSize,
                        skip: options.data.skip,
                        take: options.data.take,
                        Cuser: id,
                        FDate: ldatee,
                        Ldate: today_Daterr
                    },

                    success: function (result) {
                        options.success(result);

                    }
                });
            }
        }

    });
    if ($("#DivIndividualWorkTaskListGrid").length > 0)
    { $("#DivIndividualWorkTaskListGrid")[0].innerHTML = ""; }
    var element = $("#DivIndividualWorkTaskListGrid").kendoGrid({
        dataSource: dataSourceDetail,
        //        toolbar: ["excel"],
        excel: {
            fileName: fexename + ".xlsx",
            filterable: true,
            allPages: true
        },
        detailInit: GetallTasksforEveryEntry,
        dataBound: function () {

        },
        pageable: {
            batch: true,
            pageSize: 10,
            refresh: true,
            pageSizes: true,
            input: true
        },
        columns: [
                            {
                                field: "TTitle",
                                title: "Title",
                                resizable: true
                                //width: "50%"
                            },
                            {
                                field: "StartTime",
                                title: "Date",
                                width: 170
                            },
                            {
                                field: "prority",
                                title: "Prority",
                                hidden: true
                            },
                            {
                                field: "TotalWorkTime",
                                title: "Work Time (hh:mm:ss)",
                                width: 150


                            },
                            { command: { text: "Screenshot", click: 


foruser }, title: "", width: 100 }, //OpenScreenshotWindow
                                                        {
                                                        field: "uiddd",
                                                        title: "uid",
                                                        hidden: true


                                                    },
                                                                                    {
                                                                                        field: "sdated",
                                                                                        title: "sd",
                                                                                        hidden: true


                                                                                    },
                                                                                                                {
                                                                                                                    field: "enddated",
                                                                                                                    title: "ed",
                                                                                                                    hidden: true


                                                                                                                }

                        ],
        editable: false,
        sortable: true,
        selectable: true

    });
}
function setdatevall() {
    var cdate = new Date();
    var dd = cdate.getDate();
    var mm = cdate.getMonth() + 1;
    var yyyy = cdate.getFullYear();
    today_Daterr = yyyy + '/' + mm + '/' + dd;
    if ($("#Lastsevendays").is(':checked')) {
        var msecPerDay = 7 * 24 * 60 * 60 * 1000;
        var Seven_days_ago = new Date(cdate.getTime() - msecPerDay);
        var sdd = Seven_days_ago.getDate();
        var smm = Seven_days_ago.getMonth() + 1;
        var syyyy = Seven_days_ago.getFullYear();
        var Pre_Date = syyyy + '/' + smm + '/' + sdd;
        ldatee = Pre_Date;
    }
    else if ($("#Lastthirty").is(':checked')) {
        var msecPerDay = 30 * 24 * 60 * 60 * 1000;
        var Seven_days_ago = new Date(cdate.getTime() - msecPerDay);
        var sdd = Seven_days_ago.getDate();
        var smm = Seven_days_ago.getMonth() + 1;
        var syyyy = Seven_days_ago.getFullYear();
        var Pre_Date = syyyy + '/' + smm + '/' + sdd;
        ldatee = Pre_Date;
    }
    else if ($("#CustomRadiobt").is(':checked')) {
        ldatee = $("#dtpStartDate").val();
        today_Daterr = $("#dtpEndDate").val();
    }
    else {
        return false;
    }

}
function printcurrentpage() {
    window.print();
    return false;
}

function printMaindepGrid(sv) {
    if (sv == '1') {
        var gridElement = $('#Departmentgrid'),
        printableContent = '',
        win = window.open('', '', 'width=1200, height=500'),
        doc = win.document.open();
    }
    else if (sv == '2') {
        var gridElement = $('#DivIndividualWorkTaskListGrid'),
        printableContent = '',
        win = window.open('', '', 'width=800, height=500'),
        doc = win.document.open();
    }
    else if (sv == '3') {
        var gridElement = $('#SlipagetimeGrid'),
        printableContent = '',
        win = window.open('', '', 'width=800, height=500'),
        doc = win.document.open();
    }
    else if (sv == '4') {
        var gridElement = $('#TeamTeadList'),
        printableContent = '',
        win = window.open('', '', 'width=800, height=500'),
        doc = win.document.open();
    }
    var htmlStart =
            '<!DOCTYPE html>' +
            '<html>' +
            '<head>' +
            '<meta charset="utf-8" />' +
            '<title>Time Report</title>' +
            '<link href="/Styles/kendo.common.min.css" rel="stylesheet" type="text/css" /> ' +
            '<style>' +
            'html { font: 11pt sans-serif; }' +
            '.k-grid { border-top-width: 0; }' +
            '.k-grid, .k-grid-content { height: auto !important; }' +
            '.k-grid-content { overflow: visible !important; }' +
            'div.k-grid table { table-layout: auto; width: 100% !important; }' +
            '.k-grid .k-grid-header th { border-top: 1px solid; }' +
            '.k-grid-toolbar, .k-grid-pager > .k-link { display: none; }' +
            '</style>' +
            '</head>' +
            '<body>';

    var htmlEnd =
            '</body>' +
            '</html>';

    var gridHeader = gridElement.children('.k-grid-header');
    if (gridHeader[0]) {
        var thead = gridHeader.find('thead').clone().addClass('k-grid-header');
        printableContent = gridElement
            .clone()
                .children('.k-grid-header').remove()
            .end()
                .children('.k-grid-content')
                    .find('table')
                        .first()
                            .children('tbody').before(thead)
                        .end()
                    .end()
                .end()
            .end()[0].outerHTML;
    } else {
        printableContent = gridElement.clone()[0].outerHTML;
    }

    doc.write(htmlStart + printableContent + htmlEnd);
    doc.close();
    win.print();
}

function GetallTasksforEveryEntry(e) {
    $("<div/>").appendTo(e.detailCell).kendoGrid({
        dataSource: {
            transport: {
                read: {
                    url: "/DataTracker.svc/GetTaskforallTitle",
                    data: {
                        Cuser: e.data.uiddd,
                        FDate: e.data.sdated,
                        Ldate: e.data.enddated,
                        TTitle: e.data.TTitle
                    }

                }
            },
            serverPaging: true,
            serverSorting: true,
            serverFiltering: true,
            pageSize: 100,
            schema: {
                data: "d",
                total: function (d) {
                    if (d != null) {
                        if (d.d.length > 0)
                            return d.d[0].TotalCount
                    }
                },
                model: {
                    id: "ID",
                    fields: {
                        ID: { editable: false, type: "number" },
                        TTitle: { editable: false, type: "string" },
                        StartTime: { editable: false, type: "string" },
                        prority: { editable: false, type: "string" },
                        TotalWorkTime: { editable: false, type: "string" }

                    }
                }

            }

        },
        columns: [
                            {
                                field: "TTitle",
                                title: "Title",
                                resizable: true
                            },
                            {
                                field: "StartTime",
                                title: "Date",
                                width: 170


                            },
                                                                                                                {
                                                                                                                    field: "prority",
                                                                                                                    title: "Priorty",
                                                                                                                    width: 100
                                                                                                                },
                                                                                    {
                                                                                        field: "TotalWorkTime",
                                                                                        title: "Work Time (hh:mm:ss)",
                                                                                        width: 150
                                                                                    }

                        ],
        editable: false,
        sortable: true,
        selectable: true

    });


}

function OpenScreenshotWindowforuser(e) {
    e.preventDefault();

    var dataItem = this.dataItem($(e.currentTarget).closest("tr"));
    if (dataItem != null) {
        var ID = dataItem.uiddd;
        var sdate = dataItem.Createddate;
        var ldate = dataItem.Lastupdatetime;
        OpenScreenshotWindowfromgrid(sdate + "~" + ldate + "~" + ID+"~"+"11", "");
    }
}

//function OpenScreenshotWindowforusernested(e) {
//    e.preventDefault();
//    var dataItem = this.dataItem($(e.currentTarget).closest("tr"));
//    if (dataItem != null) {
//        var ID = dataItem.uiddd;
//        var sdate = dataItem.Createddate;
//        var ldate = dataItem.Lastupdatetime;
//        OpenScreenshotWindowinit(sdate + "~" + ldate + "~" + ID, "");
//    }
//}