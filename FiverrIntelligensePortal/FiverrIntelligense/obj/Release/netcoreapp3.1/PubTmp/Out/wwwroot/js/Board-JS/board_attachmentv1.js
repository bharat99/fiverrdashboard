﻿///*!
// * shobhana
// * script for board_attachment with mail to members
// * Date: Sat june 6  2016 
// */



function FileSelected() {
 
    Upload();
}

function boardClearAttch() {
    $('#fileToUpload').val('');
    $('#fileToUpload1').val('');
   
    
    
}

//function for saved and upload attachment
function boardajaxFileUpload(TID, UID) {
 
    var fileName = "";
    $.ajaxFileUpload
            ({
                url: 'AjaxFileUploaderForBoard.ashx?tid=' + TID + "&Cuid=" + UID,
                secureuri: false,
                fileElementId: 'fileToUpload',
                dataType: 'json',
                data: { name: 'logan', id: 'id' },
                success: function (data, status) {
                 
                    if (typeof (data.error) != 'undefined') {
                        if (data.error != '') {

                            alert("Some technical problem occur, please retry!");

                        } else {
                            fileName = data.msg;
                            if (fileName != "") {
                                var attch = $("#allattachmentlist")[0].innerHTML;
                                // if (attch.indexOf("No attachments") != -1) {
                                if (attch.indexOf(" ") != -1) {
                                    if (fileName == '0') {
                                        alert("File already exist with the same name");
                                        return false;
                                    }
                                    else {
                                        NoOfComments(TID); // added by ravi
                                        bindAttachmentonPop(TID, UID);
                                        UpdateActivityListItem("in " + cardNameOfComment, 33);

                                        var fileNameIndex = fileName.lastIndexOf("/") + 1;
                                        var filenameofcard = fileName.substr(fileNameIndex);
                                        SendNotificationMailForAttachmentOnCard(filenameofcard, cardNameOfComment, TID);
                                    }

                                }
                                else {
                                    NoOfComments(TID); // added by ravi
                                    bindAttachmentonPop(TID, UID);
                                    var fileNameIndex = fileName.lastIndexOf("/") + 1;
                                    var filenameofcard = fileName.substr(fileNameIndex);
                                    SendNotificationMailForAttachmentOnCard(filenameofcard, cardNameOfComment, TID);
                                    SavedMessagePopUp(4);
                                    ClearAttch();
                                    UpdateActivityListItem("in " + cardNameOfComment, 33);

                                }
                                $('#fileToUpload').val('');
                                $('#fileToUpload1').val('');



                            }
                        }
                    }
                },
                error: function (data, status, e) {

                    alert(data.d);
                }
            }
            )

    return fileName;

}

// function for delete the attachment from card popup
function deleteBoardAttachment(taskID, fileID) {
 var r = confirm("Are you sure you want to Delete?");
    if (r == true) {
        $("#allattachmentlist")[0].innerHTML = "";
        $.ajax({
            type: "POST",
            url: "/DataTracker.svc/DeleteAttachment",
            data: "{\"TaskID\": \"" + taskID + "\",\"FileID\": \"" + fileID + "\"",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                NoOfComments(taskID); // added by ravi
                SavedMessagePopUp(7);
                bindAttachmentonPop(taskID, cuid);
                UpdateActivityListItem("in " + cardNameOfComment, 32);
            },
            error: function (data, status, e) {
                alert("Some technical problem occur, please retry!");
            }
        });
    }
    else {
        return false;
    }
}
// function  for bind attachment on popup 
//function bindAttachmentonPop(TasksID, UserID) {
//     $("#allattachmentlist")[0].innerHTML = "";
//    $.ajax({
//        type: "GET",
//        contenttype: "application/json; charset=utf-8",
//        url: "/datatracker.svc/SelectAttachment?cardid=" + TasksID + "&userid=" + UserID + "",
//        datatype: "json",
//        success: function (data) {
//                 if (data.d.length > 0) {
//                $.each(data.d, function (index, item) {
//                    var attachmenttime = timeSince(item.Attachedtime,item.ServerTime);
//                    var fileandextension = item.fileandextension;
//                    var extension = fileandextension.split('.').pop().toLowerCase();
//                    if (extension.toLowerCase().trim() == "jpeg" || extension.toLowerCase().trim() == "png" || extension.toLowerCase().trim() == "jpg" || extension.toLowerCase().trim() == "gif" || extension.toLowerCase().trim() == "jif" || extension.toLowerCase().trim() == "jfif" || extension.toLowerCase().trim() == "bmp" || extension.toLowerCase().trim() == "psd" || extension.toLowerCase().trim() == "tif") {
//                        $("#allattachmentlist").append("<div class='u-gutter'><div class='u-clearfix js-attachment-list'><div class='attachment-thumbnail'><a style='' title='" + item.filename + "'   class='attachment-thumbnail-preview js-open-viewer'><span class='attachment-thumbnail-preview-ext'><img src='" + item.imgurlpath + "'></span></a><p class='attachment-thumbnail-details js-open-viewer'><a  title='" + item.filename + "'  class='attachment-thumbnail-details-title js-attachment-thumbnail-details'> " + item.filename + " <span class='u-block quiet'>Added <span dt='2016-06-06T08:05:03.996Z' class='date' title=" + item.Attachedtime + "> " + attachmenttime + " </span></span></a><span class='quiet attachment-thumbnail-details-options'><a class='attachment-thumbnail-details-options-item dark-hover' href='/downloadfiles.aspx?tid=" + TasksID + "&fid=" + item.FileId + "' title='You can download here'><span class='icon-sm icon-external-link'></span> <span class='attachment-thumbnail-details-options-item-text js-direct-link'><u>Download</u></span></a><a  class='attachment-thumbnail-details-options-item attachment-thumbnail-details-options-item-delete dark-hover js-confirm-delete'><span class='icon-sm icon-close'></span> <span class='attachment-thumbnail-details-options-item-text' onclick='javascript:deleteBoardAttachment(" + TasksID + "," + item.FileId + ");'><u>Delete</u></span></a><a target='_blank' class='attachment-thumbnail-details-options-item dark-hover' href='" + item.imgurlpath + "'><span class='icon-sm icon-external-link'></span> <span class='attachment-thumbnail-details-options-item-text js-direct-link'><u>Open In New Tab</u></span></a></span></p></div></div>");
//                    }
//                    else {
//                        $("#allattachmentlist").append("<div class='u-gutter'><div class='u-clearfix js-attachment-list'><div class='attachment-thumbnail'><a style='' title='" + item.filename + "'   class='attachment-thumbnail-preview js-open-viewer'><span class='attachment-thumbnail-preview-ext'>" + extension + "</span></a><p class='attachment-thumbnail-details js-open-viewer'><a  title='" + item.filename + "'  class='attachment-thumbnail-details-title js-attachment-thumbnail-details'> " + item.filename + " <span class='u-block quiet'>Added <span dt='2016-06-06T08:05:03.996Z' class='date' title=" + item.Attachedtime + "> " + attachmenttime + " </span></span></a><span class='quiet attachment-thumbnail-details-options'><a class='attachment-thumbnail-details-options-item dark-hover' href='/downloadfiles.aspx?tid=" + TasksID + "&fid=" + item.FileId + "' title='You can download here'><span class='icon-sm icon-external-link'></span> <span class='attachment-thumbnail-details-options-item-text js-direct-link'><u>Download</u></span></a><a  class='attachment-thumbnail-details-options-item attachment-thumbnail-details-options-item-delete dark-hover js-confirm-delete'><span class='icon-sm icon-close'></span> <span class='attachment-thumbnail-details-options-item-text' onclick='javascript:deleteBoardAttachment(" + TasksID + "," + item.FileId + ");'><u>Delete</u></span></a><a target='_blank' class='attachment-thumbnail-details-options-item dark-hover' href='" + item.imgurlpath + "'><span class='icon-sm icon-external-link'></span> <span class='attachment-thumbnail-details-options-item-text js-direct-link'><u>Open In New Tab</u></span></a></span></p></div></div>");

//                    }
//                })
//            }
//        }
//    })
//}


function bindAttachmentonPop(TasksID, UserID) {
    $("#allattachmentlist")[0].innerHTML = "";
    $.ajax({
        type: "GET",
        contenttype: "application/json; charset=utf-8",
        url: "/datatracker.svc/SelectAttachment?cardid=" + TasksID + "&userid=" + UserID + "",
        datatype: "json",
        success: function (data) {
          
            if (data.d.length > 0) {
                $.each(data.d, function (index, item) {
                   
                    var attachmenttime = timeSince(item.Attachedtime, item.ServerTime);
                    var fileandextension = item.fileandextension;
                    var ext = fileandextension.split('.').pop().toLowerCase();
                    if (ext.trim() == "jpg" || ext.trim() == "jpeg" || ext.trim() == "gif" || ext.trim() == "png" || ext.trim() == "bmp") {
                        $("#allattachmentlist").append("<div class='u-gutter'><div class='u-clearfix js-attachment-list'><div class='attachment-thumbnail'><a style='' title='" + item.filename + "'   class='attachment-thumbnail-preview js-open-viewer'><span class='attachment-thumbnail-preview-ext'><img src='" + item.imgurlpath + "'></span></a><p class='attachment-thumbnail-details js-open-viewer'><p  title='" + item.filename + "'  class='attachment-thumbnail-details-title js-attachment-thumbnail-details'> " + item.filename + " <span class='u-block quiet'>Added <span dt='2016-06-06T08:05:03.996Z' class='date' title=" + item.Attachedtime + "> " + attachmenttime + " </span></span></p><span class='quiet attachment-thumbnail-details-options'><a class='attachment-thumbnail-details-options-item dark-hover' href='/downloadfiles.aspx?tid=" + TasksID + "&fid=" + item.FileId + "' title='You can download here'><span class='icon-sm icon-external-link'></span> <span class='attachment-thumbnail-details-options-item-text js-direct-link'><u>Download</u></span></a><a href='#'  class='attachment-thumbnail-details-options-item attachment-thumbnail-details-options-item-delete dark-hover js-confirm-delete'><span class='icon-sm icon-close'></span> <span class='attachment-thumbnail-details-options-item-text' onclick='javascript:deleteBoardAttachment(" + TasksID + "," + item.FileId + ");'><u>Delete</u></span></a><a target='_blank' class='attachment-thumbnail-details-options-item dark-hover' href='" + item.imgurlpath + "'><span class='icon-sm icon-external-link'></span> <span class='attachment-thumbnail-details-options-item-text js-direct-link'><u>Open In New Tab</u></span></a></span></p></div></div>");
                    }
                    else if (ext.trim() == "doc") {
                        $("#allattachmentlist").append("<div class='u-gutter'><div class='u-clearfix js-attachment-list'><div class='attachment-thumbnail'><a style='' title='" + item.filename + "'   class='attachment-thumbnail-preview js-open-viewer'><span class='attachment-thumbnail-preview-ext'><img width:75px; height:75px; src='/TaskTempDoc/doc-icon.png' alt='file' /></span></a><p class='attachment-thumbnail-details js-open-viewer'><p  title='" + item.filename + "'  class='attachment-thumbnail-details-title js-attachment-thumbnail-details'> " + item.filename + " <span class='u-block quiet'>Added <span dt='2016-06-06T08:05:03.996Z' class='date' title=" + item.Attachedtime + "> " + attachmenttime + " </span></span></p><span class='quiet attachment-thumbnail-details-options'><a class='attachment-thumbnail-details-options-item dark-hover' href='/downloadfiles.aspx?tid=" + TasksID + "&fid=" + item.FileId + "' title='You can download here'><span class='icon-sm icon-external-link'></span> <span class='attachment-thumbnail-details-options-item-text js-direct-link'><u>Download</u></span></a><a href='#'  class='attachment-thumbnail-details-options-item attachment-thumbnail-details-options-item-delete dark-hover js-confirm-delete'><span class='icon-sm icon-close'></span> <span class='attachment-thumbnail-details-options-item-text' onclick='javascript:deleteBoardAttachment(" + TasksID + "," + item.FileId + ");'><u>Delete</u></span></a></span></p></div></div>");

                    }
                    else if (ext.trim() == "docx") {
                        $("#allattachmentlist").append("<div class='u-gutter'><div class='u-clearfix js-attachment-list'><div class='attachment-thumbnail'><a style='' title='" + item.filename + "'   class='attachment-thumbnail-preview js-open-viewer'><span class='attachment-thumbnail-preview-ext'><img width:75px; height:75px; src='/TaskTempDoc/doc-icon.png' alt='file' /></span></a><p class='attachment-thumbnail-details js-open-viewer'><p title='" + item.filename + "'  class='attachment-thumbnail-details-title js-attachment-thumbnail-details'> " + item.filename + " <span class='u-block quiet'>Added <span dt='2016-06-06T08:05:03.996Z' class='date' title=" + item.Attachedtime + "> " + attachmenttime + " </span></span></p><span class='quiet attachment-thumbnail-details-options'><a class='attachment-thumbnail-details-options-item dark-hover' href='/downloadfiles.aspx?tid=" + TasksID + "&fid=" + item.FileId + "' title='You can download here'><span class='icon-sm icon-external-link'></span> <span class='attachment-thumbnail-details-options-item-text js-direct-link'><u>Download</u></span></a><a  href='#' class='attachment-thumbnail-details-options-item attachment-thumbnail-details-options-item-delete dark-hover js-confirm-delete'><span class='icon-sm icon-close'></span> <span class='attachment-thumbnail-details-options-item-text' onclick='javascript:deleteBoardAttachment(" + TasksID + "," + item.FileId + ");'><u>Delete</u></span></a></span></p></div></div>");

                    }
                    else if (ext.trim() == "pdf") {
                        $("#allattachmentlist").append("<div class='u-gutter'><div class='u-clearfix js-attachment-list'><div class='attachment-thumbnail'><a style='' title='" + item.filename + "'   class='attachment-thumbnail-preview js-open-viewer'><span class='attachment-thumbnail-preview-ext'><img width:75px; height:75px; src='/TaskTempDoc/pdf.png' alt='file' /></span></a><p class='attachment-thumbnail-details js-open-viewer'><p  title='" + item.filename + "'  class='attachment-thumbnail-details-title js-attachment-thumbnail-details'> " + item.filename + " <span class='u-block quiet'>Added <span dt='2016-06-06T08:05:03.996Z' class='date' title=" + item.Attachedtime + "> " + attachmenttime + " </span></span></p><span class='quiet attachment-thumbnail-details-options'><a class='attachment-thumbnail-details-options-item dark-hover' href='/downloadfiles.aspx?tid=" + TasksID + "&fid=" + item.FileId + "' title='You can download here'><span class='icon-sm icon-external-link'></span> <span class='attachment-thumbnail-details-options-item-text js-direct-link'><u>Download</u></span></a><a  href='#' class='attachment-thumbnail-details-options-item attachment-thumbnail-details-options-item-delete dark-hover js-confirm-delete'><span class='icon-sm icon-close'></span> <span class='attachment-thumbnail-details-options-item-text' onclick='javascript:deleteBoardAttachment(" + TasksID + "," + item.FileId + ");'><u>Delete</u></span></a><a target='_blank' class='attachment-thumbnail-details-options-item dark-hover' href='" + item.imgurlpath + "'><span class='icon-sm icon-external-link'></span> <span class='attachment-thumbnail-details-options-item-text js-direct-link'><u>Open In New Tab</u></span></a></span></p></div></div>");

                    }
                    else if (ext.trim() == "txt") {
                        $("#allattachmentlist").append("<div class='u-gutter'><div class='u-clearfix js-attachment-list'><div class='attachment-thumbnail'><a style='' title='" + item.filename + "'   class='attachment-thumbnail-preview js-open-viewer'><span class='attachment-thumbnail-preview-ext'><img width:75px; height:75px; src='/TaskTempDoc/txt-icon.png' alt='file' /></span></a><p class='attachment-thumbnail-details js-open-viewer'><p  title='" + item.filename + "'  class='attachment-thumbnail-details-title js-attachment-thumbnail-details'> " + item.filename + " <span class='u-block quiet'>Added <span dt='2016-06-06T08:05:03.996Z' class='date' title=" + item.Attachedtime + "> " + attachmenttime + " </span></span></p><span class='quiet attachment-thumbnail-details-options'><a class='attachment-thumbnail-details-options-item dark-hover' href='/downloadfiles.aspx?tid=" + TasksID + "&fid=" + item.FileId + "' title='You can download here'><span class='icon-sm icon-external-link'></span> <span class='attachment-thumbnail-details-options-item-text js-direct-link'><u>Download</u></span></a><a  href='#' class='attachment-thumbnail-details-options-item attachment-thumbnail-details-options-item-delete dark-hover js-confirm-delete'><span class='icon-sm icon-close'></span> <span class='attachment-thumbnail-details-options-item-text' onclick='javascript:deleteBoardAttachment(" + TasksID + "," + item.FileId + ");'><u>Delete</u></span></a><a target='_blank' class='attachment-thumbnail-details-options-item dark-hover' href='" + item.imgurlpath + "'><span class='icon-sm icon-external-link'></span> <span class='attachment-thumbnail-details-options-item-text js-direct-link'><u>Open In New Tab</u></span></a></span></p></div></div>");

                    }
                    else if (ext.trim() == "xml") {
                        $("#allattachmentlist").append("<div class='u-gutter'><div class='u-clearfix js-attachment-list'><div class='attachment-thumbnail'><a style='' title='" + item.filename + "'   class='attachment-thumbnail-preview js-open-viewer'><span class='attachment-thumbnail-preview-ext'><img width:75px; height:75px; src='/TaskTempDoc/xml.png' alt='file' /></span></a><p class='attachment-thumbnail-details js-open-viewer'><p  title='" + item.filename + "'  class='attachment-thumbnail-details-title js-attachment-thumbnail-details'> " + item.filename + " <span class='u-block quiet'>Added <span dt='2016-06-06T08:05:03.996Z' class='date' title=" + item.Attachedtime + "> " + attachmenttime + " </span></span></p><span class='quiet attachment-thumbnail-details-options'><a class='attachment-thumbnail-details-options-item dark-hover' href='/downloadfiles.aspx?tid=" + TasksID + "&fid=" + item.FileId + "' title='You can download here'><span class='icon-sm icon-external-link'></span> <span class='attachment-thumbnail-details-options-item-text js-direct-link'><u>Download</u></span></a><a  href='#' class='attachment-thumbnail-details-options-item attachment-thumbnail-details-options-item-delete dark-hover js-confirm-delete'><span class='icon-sm icon-close'></span> <span class='attachment-thumbnail-details-options-item-text' onclick='javascript:deleteBoardAttachment(" + TasksID + "," + item.FileId + ");'><u>Delete</u></span></a><a target='_blank' class='attachment-thumbnail-details-options-item dark-hover' href='" + item.imgurlpath + "'><span class='icon-sm icon-external-link'></span> <span class='attachment-thumbnail-details-options-item-text js-direct-link'><u>Open In New Tab</u></span></a></span></p></div></div>");

                    }
                    else if (ext.trim() == "sql") {
                        $("#allattachmentlist").append("<div class='u-gutter'><div class='u-clearfix js-attachment-list'><div class='attachment-thumbnail'><a style='' title='" + item.filename + "'   class='attachment-thumbnail-preview js-open-viewer'><span class='attachment-thumbnail-preview-ext'><img width:75px; height:75px; src='/TaskTempDoc/sql-icon.png' alt='file' /></span></a><p class='attachment-thumbnail-details js-open-viewer'><p  title='" + item.filename + "'  class='attachment-thumbnail-details-title js-attachment-thumbnail-details'> " + item.filename + " <span class='u-block quiet'>Added <span dt='2016-06-06T08:05:03.996Z' class='date' title=" + item.Attachedtime + "> " + attachmenttime + " </span></span></p><span class='quiet attachment-thumbnail-details-options'><a class='attachment-thumbnail-details-options-item dark-hover' href='/downloadfiles.aspx?tid=" + TasksID + "&fid=" + item.FileId + "' title='You can download here'><span class='icon-sm icon-external-link'></span> <span class='attachment-thumbnail-details-options-item-text js-direct-link'><u>Download</u></span></a><a href='#' class='attachment-thumbnail-details-options-item attachment-thumbnail-details-options-item-delete dark-hover js-confirm-delete'><span class='icon-sm icon-close'></span> <span class='attachment-thumbnail-details-options-item-text' onclick='javascript:deleteBoardAttachment(" + TasksID + "," + item.FileId + ");'><u>Delete</u></span></a></span></p></div></div>");

                    }
                    else if (ext.trim() == "ods") {
                        $("#allattachmentlist").append("<div class='u-gutter'><div class='u-clearfix js-attachment-list'><div class='attachment-thumbnail'><a style='' title='" + item.filename + "'   class='attachment-thumbnail-preview js-open-viewer'><span class='attachment-thumbnail-preview-ext'><img width:75px; height:75px; src='/TaskTempDoc/ods-icon.png' alt='file' /></span></a><p class='attachment-thumbnail-details js-open-viewer'><p  title='" + item.filename + "'  class='attachment-thumbnail-details-title js-attachment-thumbnail-details'> " + item.filename + " <span class='u-block quiet'>Added <span dt='2016-06-06T08:05:03.996Z' class='date' title=" + item.Attachedtime + "> " + attachmenttime + " </span></span></p><span class='quiet attachment-thumbnail-details-options'><a class='attachment-thumbnail-details-options-item dark-hover' href='/downloadfiles.aspx?tid=" + TasksID + "&fid=" + item.FileId + "' title='You can download here'><span class='icon-sm icon-external-link'></span> <span class='attachment-thumbnail-details-options-item-text js-direct-link'><u>Download</u></span></a><a href='#' class='attachment-thumbnail-details-options-item attachment-thumbnail-details-options-item-delete dark-hover js-confirm-delete'><span class='icon-sm icon-close'></span> <span class='attachment-thumbnail-details-options-item-text' onclick='javascript:deleteBoardAttachment(" + TasksID + "," + item.FileId + ");'><u>Delete</u></span></a></span></p></div></div>");

                    }
                    else if (ext.trim() == "odt") {
                        $("#allattachmentlist").append("<div class='u-gutter'><div class='u-clearfix js-attachment-list'><div class='attachment-thumbnail'><a style='' title='" + item.filename + "'   class='attachment-thumbnail-preview js-open-viewer'><span class='attachment-thumbnail-preview-ext'><img width:75px; height:75px; src='/TaskTempDoc/odt-icon.png' alt='file' /></span></a><p class='attachment-thumbnail-details js-open-viewer'><p  title='" + item.filename + "'  class='attachment-thumbnail-details-title js-attachment-thumbnail-details'> " + item.filename + " <span class='u-block quiet'>Added <span dt='2016-06-06T08:05:03.996Z' class='date' title=" + item.Attachedtime + "> " + attachmenttime + " </span></span></p><span class='quiet attachment-thumbnail-details-options'><a class='attachment-thumbnail-details-options-item dark-hover' href='/downloadfiles.aspx?tid=" + TasksID + "&fid=" + item.FileId + "' title='You can download here'><span class='icon-sm icon-external-link'></span> <span class='attachment-thumbnail-details-options-item-text js-direct-link'><u>Download</u></span></a><a href='#' class='attachment-thumbnail-details-options-item attachment-thumbnail-details-options-item-delete dark-hover js-confirm-delete'><span class='icon-sm icon-close'></span> <span class='attachment-thumbnail-details-options-item-text' onclick='javascript:deleteBoardAttachment(" + TasksID + "," + item.FileId + ");'><u>Delete</u></span></a></span></p></div></div>");

                    }
                    else if (ext.trim() == "xls") {
                        $("#allattachmentlist").append("<div class='u-gutter'><div class='u-clearfix js-attachment-list'><div class='attachment-thumbnail'><a style='' title='" + item.filename + "'   class='attachment-thumbnail-preview js-open-viewer'><span class='attachment-thumbnail-preview-ext'><img width:75px; height:75px; src='/TaskTempDoc/xls_logo.png' alt='file' /></span></a><p class='attachment-thumbnail-details js-open-viewer'><p  title='" + item.filename + "'  class='attachment-thumbnail-details-title js-attachment-thumbnail-details'> " + item.filename + " <span class='u-block quiet'>Added <span dt='2016-06-06T08:05:03.996Z' class='date' title=" + item.Attachedtime + "> " + attachmenttime + " </span></span></p><span class='quiet attachment-thumbnail-details-options'><a class='attachment-thumbnail-details-options-item dark-hover' href='/downloadfiles.aspx?tid=" + TasksID + "&fid=" + item.FileId + "' title='You can download here'><span class='icon-sm icon-external-link'></span> <span class='attachment-thumbnail-details-options-item-text js-direct-link'><u>Download</u></span></a><a  href='#' class='attachment-thumbnail-details-options-item attachment-thumbnail-details-options-item-delete dark-hover js-confirm-delete'><span class='icon-sm icon-close'></span> <span class='attachment-thumbnail-details-options-item-text' onclick='javascript:deleteBoardAttachment(" + TasksID + "," + item.FileId + ");'><u>Delete</u></span></a></span></p></div></div>");

                    }
                    else if (ext.trim() == "xlsx") {
                        $("#allattachmentlist").append("<div class='u-gutter'><div class='u-clearfix js-attachment-list'><div class='attachment-thumbnail'><a style='' title='" + item.filename + "'   class='attachment-thumbnail-preview js-open-viewer'><span class='attachment-thumbnail-preview-ext'><img width:75px; height:75px; src='/TaskTempDoc/xls_logo.png' alt='file' /></span></a><p class='attachment-thumbnail-details js-open-viewer'><p  title='" + item.filename + "'  class='attachment-thumbnail-details-title js-attachment-thumbnail-details'> " + item.filename + " <span class='u-block quiet'>Added <span dt='2016-06-06T08:05:03.996Z' class='date' title=" + item.Attachedtime + "> " + attachmenttime + " </span></span></p><span class='quiet attachment-thumbnail-details-options'><a class='attachment-thumbnail-details-options-item dark-hover' href='/downloadfiles.aspx?tid=" + TasksID + "&fid=" + item.FileId + "' title='You can download here'><span class='icon-sm icon-external-link'></span> <span class='attachment-thumbnail-details-options-item-text js-direct-link'><u>Download</u></span></a><a href='#'  class='attachment-thumbnail-details-options-item attachment-thumbnail-details-options-item-delete dark-hover js-confirm-delete'><span class='icon-sm icon-close'></span> <span class='attachment-thumbnail-details-options-item-text' onclick='javascript:deleteBoardAttachment(" + TasksID + "," + item.FileId + ");'><u>Delete</u></span></a></span></p></div></div>");

                    }
                    else if (ext.trim() == "ppt") {
                        $("#allattachmentlist").append("<div class='u-gutter'><div class='u-clearfix js-attachment-list'><div class='attachment-thumbnail'><a style='' title='" + item.filename + "'   class='attachment-thumbnail-preview js-open-viewer'><span class='attachment-thumbnail-preview-ext'><img width:75px; height:75px; src='/TaskTempDoc/ppt-icon.png' alt='file' /></span></a><p class='attachment-thumbnail-details js-open-viewer'><p  title='" + item.filename + "'  class='attachment-thumbnail-details-title js-attachment-thumbnail-details'> " + item.filename + " <span class='u-block quiet'>Added <span dt='2016-06-06T08:05:03.996Z' class='date' title=" + item.Attachedtime + "> " + attachmenttime + " </span></span></p><span class='quiet attachment-thumbnail-details-options'><a class='attachment-thumbnail-details-options-item dark-hover' href='/downloadfiles.aspx?tid=" + TasksID + "&fid=" + item.FileId + "' title='You can download here'><span class='icon-sm icon-external-link'></span> <span class='attachment-thumbnail-details-options-item-text js-direct-link'><u>Download</u></span></a><a href='#'  class='attachment-thumbnail-details-options-item attachment-thumbnail-details-options-item-delete dark-hover js-confirm-delete'><span class='icon-sm icon-close'></span> <span class='attachment-thumbnail-details-options-item-text' onclick='javascript:deleteBoardAttachment(" + TasksID + "," + item.FileId + ");'><u>Delete</u></span></a></span></p></div></div>");

                    }
                    else if (ext.trim() == "pptx") {
                        $("#allattachmentlist").append("<div class='u-gutter'><div class='u-clearfix js-attachment-list'><div class='attachment-thumbnail'><a style='' title='" + item.filename + "'   class='attachment-thumbnail-preview js-open-viewer'><span class='attachment-thumbnail-preview-ext'><img width:75px; height:75px; src='/TaskTempDoc/ppt-icon.png' alt='file' /></span></a><p class='attachment-thumbnail-details js-open-viewer'><p  title='" + item.filename + "'  class='attachment-thumbnail-details-title js-attachment-thumbnail-details'> " + item.filename + " <span class='u-block quiet'>Added <span dt='2016-06-06T08:05:03.996Z' class='date' title=" + item.Attachedtime + "> " + attachmenttime + " </span></span></p><span class='quiet attachment-thumbnail-details-options'><a class='attachment-thumbnail-details-options-item dark-hover' href='/downloadfiles.aspx?tid=" + TasksID + "&fid=" + item.FileId + "' title='You can download here'><span class='icon-sm icon-external-link'></span> <span class='attachment-thumbnail-details-options-item-text js-direct-link'><u>Download</u></span></a><a href='#'  class='attachment-thumbnail-details-options-item attachment-thumbnail-details-options-item-delete dark-hover js-confirm-delete'><span class='icon-sm icon-close'></span> <span class='attachment-thumbnail-details-options-item-text' onclick='javascript:deleteBoardAttachment(" + TasksID + "," + item.FileId + ");'><u>Delete</u></span></a></span></p></div></div>");

                    }
                    else if (ext.trim() == "rtf") {
                        $("#allattachmentlist").append("<div class='u-gutter'><div class='u-clearfix js-attachment-list'><div class='attachment-thumbnail'><a style='' title='" + item.filename + "'   class='attachment-thumbnail-preview js-open-viewer'><span class='attachment-thumbnail-preview-ext'><img width:75px; height:75px; src='/TaskTempDoc/rtf-icon.png' alt='file' /></span></a><p class='attachment-thumbnail-details js-open-viewer'><p  title='" + item.filename + "'  class='attachment-thumbnail-details-title js-attachment-thumbnail-details'> " + item.filename + " <span class='u-block quiet'>Added <span dt='2016-06-06T08:05:03.996Z' class='date' title=" + item.Attachedtime + "> " + attachmenttime + " </span></span></p><span class='quiet attachment-thumbnail-details-options'><a class='attachment-thumbnail-details-options-item dark-hover' href='/downloadfiles.aspx?tid=" + TasksID + "&fid=" + item.FileId + "' title='You can download here'><span class='icon-sm icon-external-link'></span> <span class='attachment-thumbnail-details-options-item-text js-direct-link'><u>Download</u></span></a><a href='#' class='attachment-thumbnail-details-options-item attachment-thumbnail-details-options-item-delete dark-hover js-confirm-delete'><span class='icon-sm icon-close'></span> <span class='attachment-thumbnail-details-options-item-text' onclick='javascript:deleteBoardAttachment(" + TasksID + "," + item.FileId + ");'><u>Delete</u></span></a></span></p></div></div>");

                    }
                    else if (ext.trim() == "csv") {
                        $("#allattachmentlist").append("<div class='u-gutter'><div class='u-clearfix js-attachment-list'><div class='attachment-thumbnail'><a style='' title='" + item.filename + "'   class='attachment-thumbnail-preview js-open-viewer'><span class='attachment-thumbnail-preview-ext'><img width:75px; height:75px; src='/TaskTempDoc/csv-icon.png' alt='file' /></span></a><p class='attachment-thumbnail-details js-open-viewer'><p  title='" + item.filename + "'  class='attachment-thumbnail-details-title js-attachment-thumbnail-details'> " + item.filename + " <span class='u-block quiet'>Added <span dt='2016-06-06T08:05:03.996Z' class='date' title=" + item.Attachedtime + "> " + attachmenttime + " </span></span></p><span class='quiet attachment-thumbnail-details-options'><a class='attachment-thumbnail-details-options-item dark-hover' href='/downloadfiles.aspx?tid=" + TasksID + "&fid=" + item.FileId + "' title='You can download here'><span class='icon-sm icon-external-link'></span> <span class='attachment-thumbnail-details-options-item-text js-direct-link'><u>Download</u></span></a><a href='#'  class='attachment-thumbnail-details-options-item attachment-thumbnail-details-options-item-delete dark-hover js-confirm-delete'><span class='icon-sm icon-close'></span> <span class='attachment-thumbnail-details-options-item-text' onclick='javascript:deleteBoardAttachment(" + TasksID + "," + item.FileId + ");'><u>Delete</u></span></a></span></p></div></div>");

                    }
                    else if (ext.trim() == "html") {
                        $("#allattachmentlist").append("<div class='u-gutter'><div class='u-clearfix js-attachment-list'><div class='attachment-thumbnail'><a style='' title='" + item.filename + "'   class='attachment-thumbnail-preview js-open-viewer'><span class='attachment-thumbnail-preview-ext'><img width:75px; height:75px; src='/TaskTempDoc/html-icon.jpg' alt='file' /></span></a><p class='attachment-thumbnail-details js-open-viewer'><p  title='" + item.filename + "'  class='attachment-thumbnail-details-title js-attachment-thumbnail-details'> " + item.filename + " <span class='u-block quiet'>Added <span dt='2016-06-06T08:05:03.996Z' class='date' title=" + item.Attachedtime + "> " + attachmenttime + " </span></span></p><span class='quiet attachment-thumbnail-details-options'><a class='attachment-thumbnail-details-options-item dark-hover' href='/downloadfiles.aspx?tid=" + TasksID + "&fid=" + item.FileId + "' title='You can download here'><span class='icon-sm icon-external-link'></span> <span class='attachment-thumbnail-details-options-item-text js-direct-link'><u>Download</u></span></a><a href='#'  class='attachment-thumbnail-details-options-item attachment-thumbnail-details-options-item-delete dark-hover js-confirm-delete'><span class='icon-sm icon-close'></span> <span class='attachment-thumbnail-details-options-item-text' onclick='javascript:deleteBoardAttachment(" + TasksID + "," + item.FileId + ");'><u>Delete</u></span></a></span></p></div></div>");

                    }
                    else if (ext.trim() == "js") {
                        $("#allattachmentlist").append("<div class='u-gutter'><div class='u-clearfix js-attachment-list'><div class='attachment-thumbnail'><a style='' title='" + item.filename + "'   class='attachment-thumbnail-preview js-open-viewer'><span class='attachment-thumbnail-preview-ext'><img width:75px; height:75px; src='/TaskTempDoc/js-icon.png' alt='file' /></span></a><p class='attachment-thumbnail-details js-open-viewer'><p  title='" + item.filename + "'  class='attachment-thumbnail-details-title js-attachment-thumbnail-details'> " + item.filename + " <span class='u-block quiet'>Added <span dt='2016-06-06T08:05:03.996Z' class='date' title=" + item.Attachedtime + "> " + attachmenttime + " </span></span></p><span class='quiet attachment-thumbnail-details-options'><a class='attachment-thumbnail-details-options-item dark-hover' href='/downloadfiles.aspx?tid=" + TasksID + "&fid=" + item.FileId + "' title='You can download here'><span class='icon-sm icon-external-link'></span> <span class='attachment-thumbnail-details-options-item-text js-direct-link'><u>Download</u></span></a><a href='#'  class='attachment-thumbnail-details-options-item attachment-thumbnail-details-options-item-delete dark-hover js-confirm-delete'><span class='icon-sm icon-close'></span> <span class='attachment-thumbnail-details-options-item-text' onclick='javascript:deleteBoardAttachment(" + TasksID + "," + item.FileId + ");'><u>Delete</u></span></a></span></p></div></div>");

                    }
                    else if (ext.trim() == "css") {
                        $("#allattachmentlist").append("<div class='u-gutter'><div class='u-clearfix js-attachment-list'><div class='attachment-thumbnail'><a style='' title='" + item.filename + "'   class='attachment-thumbnail-preview js-open-viewer'><span class='attachment-thumbnail-preview-ext'><img width:75px; height:75px; src='/TaskTempDoc/css-icon.png' alt='file' /></span></a><p class='attachment-thumbnail-details js-open-viewer'><p  title='" + item.filename + "'  class='attachment-thumbnail-details-title js-attachment-thumbnail-details'> " + item.filename + " <span class='u-block quiet'>Added <span dt='2016-06-06T08:05:03.996Z' class='date' title=" + item.Attachedtime + "> " + attachmenttime + " </span></span></p><span class='quiet attachment-thumbnail-details-options'><a class='attachment-thumbnail-details-options-item dark-hover' href='/downloadfiles.aspx?tid=" + TasksID + "&fid=" + item.FileId + "' title='You can download here'><span class='icon-sm icon-external-link'></span> <span class='attachment-thumbnail-details-options-item-text js-direct-link'><u>Download</u></span></a><a href='#'  class='attachment-thumbnail-details-options-item attachment-thumbnail-details-options-item-delete dark-hover js-confirm-delete'><span class='icon-sm icon-close'></span> <span class='attachment-thumbnail-details-options-item-text' onclick='javascript:deleteBoardAttachment(" + TasksID + "," + item.FileId + ");'><u>Delete</u></span></a></span></p></div></div>");

                    }
                    else if (ext.trim() == "zip") {
                        $("#allattachmentlist").append("<div class='u-gutter'><div class='u-clearfix js-attachment-list'><div class='attachment-thumbnail'><a style='' title='" + item.filename + "'   class='attachment-thumbnail-preview js-open-viewer'><span class='attachment-thumbnail-preview-ext'><img width:75px; height:75px; src='/TaskTempDoc/zip-icon.jpg' alt='file' /></span></a><p class='attachment-thumbnail-details js-open-viewer'><p  title='" + item.filename + "'  class='attachment-thumbnail-details-title js-attachment-thumbnail-details'> " + item.filename + " <span class='u-block quiet'>Added <span dt='2016-06-06T08:05:03.996Z' class='date' title=" + item.Attachedtime + "> " + attachmenttime + " </span></span></p><span class='quiet attachment-thumbnail-details-options'><a class='attachment-thumbnail-details-options-item dark-hover' href='/downloadfiles.aspx?tid=" + TasksID + "&fid=" + item.FileId + "' title='You can download here'><span class='icon-sm icon-external-link'></span> <span class='attachment-thumbnail-details-options-item-text js-direct-link'><u>Download</u></span></a><a href='#'  class='attachment-thumbnail-details-options-item attachment-thumbnail-details-options-item-delete dark-hover js-confirm-delete'><span class='icon-sm icon-close'></span> <span class='attachment-thumbnail-details-options-item-text' onclick='javascript:deleteBoardAttachment(" + TasksID + "," + item.FileId + ");'><u>Delete</u></span></a></span></p></div></div>");

                    }
                    else {
                        $("#allattachmentlist").append("<div class='u-gutter'><div class='u-clearfix js-attachment-list'><div class='attachment-thumbnail'><a style='' title='" + item.filename + "'   class='attachment-thumbnail-preview js-open-viewer'><span class='attachment-thumbnail-preview-ext'><img width:75px; height:75px; src='/TaskTempDoc/else-icon.png' alt='file' /></span></a><p class='attachment-thumbnail-details js-open-viewer'><p  title='" + item.filename + "'  class='attachment-thumbnail-details-title js-attachment-thumbnail-details'> " + item.filename + " <span class='u-block quiet'>Added <span dt='2016-06-06T08:05:03.996Z' class='date' title=" + item.Attachedtime + "> " + attachmenttime + " </span></span></p><span class='quiet attachment-thumbnail-details-options'><a class='attachment-thumbnail-details-options-item dark-hover' href='/downloadfiles.aspx?tid=" + TasksID + "&fid=" + item.FileId + "' title='You can download here'><span class='icon-sm icon-external-link'></span> <span class='attachment-thumbnail-details-options-item-text js-direct-link'><u>Download</u></span></a><a href='#'  class='attachment-thumbnail-details-options-item attachment-thumbnail-details-options-item-delete dark-hover js-confirm-delete'><span class='icon-sm icon-close'></span> <span class='attachment-thumbnail-details-options-item-text' onclick='javascript:deleteBoardAttachment(" + TasksID + "," + item.FileId + ");'><u>Delete</u></span></a></span></p></div></div>");
                    }
                })
            }
        }
    })
}


// function for finding time ago
function timeSince(ts, ServerTime) {
    var now = new Date(ServerTime);
    var ts1 = new Date(ts);
    var delta = now.getTime()-ts1.getTime();

    delta = delta / 1000; //us to s
    var Remaining = delta > 0 ? "ago" : "left";

    var ps, pm, ph, pd, min, hou, sec, days;

    if (delta <= 59) {
        ps = (delta > 1) ? "s" : "";
        return Math.floor(delta) + " " + "second" + ps + " " + Remaining;
    }

    if (delta >= 60 && delta <= 3599) {
        min = Math.floor(delta / 60);
        sec = Math.floor(delta - (min * 60));
        pm = (min > 1) ? "s" : "";
        ps = (sec > 1) ? "s" : "";
        return min +" "+ "minute" + pm + " " + sec + " second" + ps + " " + Remaining;
    }

    if (delta >= 3600 && delta <= 86399) {
        hou = Math.floor(delta / 3600);
        min = Math.floor((delta - (hou * 3600)) / 60);
        ph = (hou > 1) ? "s" : "";
        pm = (min > 1) ? "s" : "";
        return hou +" "+ "hour" + ph + " " + min + " minute" + pm + " " + Remaining;
    }

    if (delta >= 86400) {
        days = Math.floor(delta / 86400);
        hou = Math.floor((delta - (days * 86400)) / 60 / 60);
        pd = (days > 1) ? "s" : "";
        ph = (hou > 1) ? "s" : "";
        return days +" "+ "day" + pd + " " + hou + " hour" + ph + " " + Remaining;
    }

}

function boardUploadAttach(UID, taskid) {
 
    if (taskid != null) {
        boardajaxFileUpload(taskid, UID);
        setTimeout("boardClearAttch()", 5000);
    }
}


