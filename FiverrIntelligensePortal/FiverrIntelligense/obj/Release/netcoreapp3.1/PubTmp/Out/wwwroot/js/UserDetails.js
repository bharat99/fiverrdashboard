﻿$(document).ready(function () {
    GetUserDetails();
    $('.Close_Modal').click(function () {
        $(this).parents('.CustomPopup').fadeOut();
    });
    bindhtmteamlist();
    var eyepassword = true;
    var newpassword = true;
    var confirmpassword = true;
    $('#eye-password').click(function () {
        if (eyepassword == true) {
            $('#password').replaceWith("<input type='text' id='password' value='" + $('#password').val() + "' readonly/>");
            eyepassword = false;
        }
        else {
            $('#password').replaceWith("<input type='password' id='password' value='" + $('#password').val() + "' readonly/>");
            eyepassword = true;
        }
    });
    $('#eye-newpassword').click(function () {
        if (newpassword == true) {
            $('#NewPassword').replaceWith("<input type='text' id='NewPassword' value='" + $('#NewPassword').val() + "'/>");
            newpassword = false;
        }
        else {
            $('#NewPassword').replaceWith("<input type='password' id='NewPassword' value='" + $('#NewPassword').val() + "'/>");
            newpassword = true;
        }
    });
    $('#eye-confirmpassword').click(function () {
        if (confirmpassword == true) {
            $('#ConfirmPassword').replaceWith("<input type='text' id='ConfirmPassword' value='" + $('#ConfirmPassword').val() + "'/>");
            confirmpassword = false;
        }
        else {
            $('#ConfirmPassword').replaceWith("<input type='password' id='ConfirmPassword' value='" + $('#ConfirmPassword').val() + "'/>");
            confirmpassword = true;
        }
    });
});

function GetUserDetails() {
    $.ajax({
        type: "POST",
        url: "/DataTracker.svc/GetAllUserDetails",
        data: {},
        dataType: 'json',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            var d = JSON.parse(data.d);
            $('#tblUsers').empty();
            $('#tblUsers').append("<thead><tr><th>Full Name</th><th>First name</th><th>Last name</th><th>Email</th><th>Team Name</th><th>Created date</th><th>Action</th></tr></thead><tbody>");
            $.each(d, function (index, item) {
                var Createddate = [];
                if (item.Createddate != null) {
                    Createddate = item.Createddate.split('T');
                }
                else {
                    Createddate[0] = '';
                }
                var items = JSON.stringify({ Userid: item.Userid, FullName: item.FullName, Fname: item.Fname, Lname: item.Lname, Email: item.Email, Password: item.password, TeamId: item.TeamId, TeamName: item.TeamName, Createddate: Createddate[0] });
                $('#tblUsers').append("<tr id=" + item.Userid + "><td>" + item.FullName + "</td><td>" + item.Fname + "</td><td>" + item.Lname + "</td><td>" + item.Email + "</td><td id=" + item.TeamId + ">" + item.TeamName + "</td><td>" + Createddate[0] + "</td><td><i class='fa fa-pencil-square-o' aria-hidden='true' id='Edit' title='Edit detail' onclick='ShowUserDetail(" + items + ");'></i><i class='fa fa-unlock-alt' aria-hidden='true' onclick='UpdatePasswordModel(" + items + ");'></i></span></td></tr>");
            });
            $('#tblUsers').append("</tbody>");
        },
        error: function (data) {
        }
    });
}

function ShowUserDetail(items) {
    $("#lbl_FullName").css('display', 'none');
    $("#lbl_Email").css('display', 'none');
    $('#UserId').val(items.Userid);
    $('#fullname').val(items.FullName);
    $('#firstname').val(items.Fname);
    $('#lastname').val(items.Lname);
    $('#email').val(items.Email);
    $('#password').val(items.Password);
    $('#teamname').val(items.TeamId);
    $('#IsActive').val('1');
    $('.Userdetail').fadeIn();
}

function UpdatePasswordModel(items) {
    $("#lbl_password").css('display', 'none');
    $("#lbl_NewPassword").css('display', 'none');
    $("#lbl_ConfirmPassword").css('display', 'none');
    $('#password').val(items.Password);
    $('#NewPassword').val('');
    $('#ConfirmPassword').val('');
    $('#UserId').val(items.Userid);
    $('#UserName').val(items.FullName);
    $('#Email').val(items.Email);
    $('.UpdatePassword').fadeIn();
}

function UpdatePassword() {
    $("#lbl_password").css('display', 'none');
    $("#lbl_NewPassword").css('display', 'none');
    $("#lbl_ConfirmPassword").css('display', 'none');
    var NewPassword = $('#NewPassword').val();
    var ConfirmPassword = $('#ConfirmPassword').val();
    if (NewPassword == "") {
        $("#lbl_NewPassword").text('New password should not be blank!');
        $("#lbl_NewPassword").css('display', 'block');
        return false;
    }
    else if (ConfirmPassword == "") {
        $("#lbl_ConfirmPassword").text('Confirm password should not be blank!');
        $("#lbl_ConfirmPassword").css('display', 'block');
        return false;
    }
    else if (NewPassword == ConfirmPassword) {
        $("#lbl_password").css('display', 'none');
        $("#lbl_NewPassword").css('display', 'none');
        $("#lbl_ConfirmPassword").css('display', 'none');
        if (confirm('Are you sure you want to update details?')) {
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "/DataTracker.svc/UpdatePassword",
                data: JSON.stringify({ UserId: $('#UserId').val(), UserName: $('#UserName').val(), NewPassword: NewPassword, Email: $('#Email').val() }),
                dataType: "json",
                success: function (data) {
                    if (data.d == 1) {
                        alert('Successfully updated.');
                        GetUserDetails();
                    }
                },
                error: function (result) {

                }
            });
            $('.CustomPopup').fadeOut();
            return true;
        }
        else {
            return false;
        }
    }
    else {
        $("#lbl_NewPassword").text('New password and confirm password must be matched!');
        $("#lbl_NewPassword").css('display', 'block');
        return false;
    }
}

function UpdateUserDetail() {
    $("#lbl_FullName").css('display', 'none');
    $("#lbl_Email").css('display', 'none');
    Validation();
    var _UserId = $('#UserId').val();
    var _FullName = $('#fullname').val();
    var _Fname = $('#firstname').val();
    var _Lname = $('#lastname').val();
    var _Email = $('#email').val();
    var _Password = $('#password').val();
    var _TeamId = $('#teamname').val();
    var _TeamName = $('#teamname option:selected').text();
    var _IsActive = $('#IsActive').val();
    if (confirm('Are you sure you want to update details?')) {
        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: "/DataTracker.svc/UpdateUserDetail",
            data: JSON.stringify({ UserId: _UserId, FullName: _FullName, Fname: _Fname, Lname: _Lname, Email: _Email, Password: _Password, TeamId: _TeamId, TeamName: _TeamName, IsActive: _IsActive }),
            dataType: "json",
            success: function (data) {
                if (data.d > 0) {
                    alert('Successfully updated.');
                    GetUserDetails();
                }
            },
            error: function (result) {

            }
        });
        $('.CustomPopup').fadeOut();
        return true;
    }
    else {
        return false;
    }
}

function Validation() {
    var _FullName = $('#fullname').val();
    var _Email = $('#email').val();
    if (_FullName == null || _FullName == '') {
        $("#lbl_FullName").text("Please enter full name!");
        $("#lbl_FullName").css('display', 'block');
        return false;
    }
    if (_Email == null || _Email == '') {
        $("#lbl_Email").text("Please enter Email!");
        $("#lbl_Email").css('display', 'block');
        return false;
    }
}


function bindhtmteamlist() {
    $.ajax({
        type: "GET",
        contentType: "application/json; charset=utf-8",
        url: "/DataTracker.svc/BindTeamListdropdown",
        dataType: "json",
        success: function (data) {
            $("#teamname").empty();
            $.each(data.d, function (index, item) {
                $('#teamname').append($('<option></option>').val(item.Teamid).html(item.TeamName));
            });
        },
        error: function (result) {

        }
    });
}
