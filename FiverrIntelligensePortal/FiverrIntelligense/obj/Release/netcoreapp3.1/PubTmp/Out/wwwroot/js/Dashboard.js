﻿var r1 = $(window).height() - 250;
$(window).on("load resize scroll", function (e) {
    if ($(this).height() <= $('#DivOtherTask').height() + 200) {
        $('#DivOtherTask .k-grid-content').css('max-height', r1); //set max height
    } else {
        //alert("hh");
        $('#DivOtherTask .k-grid-content').css('max-height', r1 - 200);
    }
});

function SetCalenderValue(sdate) {
    if (sdate != "") {

        var yr = "";
        var cday = "";
        var splitdate = sdate.split('/');
        yr = splitdate[2];
        var mth = splitdate[0] - 1;
        cday = splitdate[1];
        var OffMonth = 03;
        var OffDay = 1;
        if (IsClient) {
            OffMonth = 11;
            OffDay = 06;

        }
        setTimeout("AutoAllUserList('0',0);", 2);

        $("#datepickerStartDate").kendoDatePicker({
            change: onChange,
            format: "MM/dd/yyyy",
            max: new Date(yr, mth, cday),
            min: new Date(2014, 03, 1)

        });
        $("#DivOffCal").kendoDatePicker({
            change: onOffCyChange,
            format: "MM/dd/yyyy",
            min: new Date(2014, OffMonth, OffDay)

        });
    }
    else {

        var ds = new Date();
        day = ds.getDate();
        if (day < 10) {
            day = '0' + day;
        }
        var mt = ('0' + (ds.getMonth() + 1)).slice(-2)
        var year = ds.getFullYear();
        $("#datepickerStartDate").kendoDatePicker({
            change: onChange,
            format: "MM/dd/yyyy",
            max: new Date(year, mt, day),
            min: new Date(2014, 03, 1)

        });
        $("#DivOffCal").kendoDatePicker({
            change: onOffCyChange,
            format: "MM/dd/yyyy",
            min: new Date(2014, OffMonth, OffDay)

        });

    }
    $("#TxtScreentcal").kendoDatePicker({

        format: "MM/dd/yyyy",
        min: new Date(2014, 03, 1)

    });
    //this is commented becoz of this retails.cs calling twice
   // KeepSessionAlive();   
    $("#tabstripcom").kendoTabStrip({
        animation: {
            open: {
                effects: "fadeIn"
            }

        }

    });


    $(window).resize(function () {
        SetRegularBandwidth($(window).width());
        SetComboBandwidth($(window).width());
    });

   

        $(window).ready(function () {
        var wi = $(window).width();
        SetRegularBandwidth(wi);
        SetComboBandwidth(wi);
        $(window).resize(function () {
            var wi = $(window).width();
            SetRegularBandwidth(wi);
            SetComboBandwidth(wi);

        });
    });

    }

    //Till here

function SetTabStrip(Combo) {
    var utidd = 0;
    var tabStrip1 = $('#tabstripDashboard').kendoTabStrip().data("kendoTabStrip");
    tabStrip1.select(3);
    var subsscc = $("#Checkforresponsible").val();
    GetAllOtherTasks(Combo, false, false, subsscc);

}
function SetDefaultStartDate() {
    var ds = new Date();
    var curr_day1 = ds.getDate();
    if (curr_day1 < 10) {
        curr_day1 = '0' + curr_day1;
    }
    var curr_month1 = ('0' + (ds.getMonth() + 1)).slice(-2)
    var curr_year1 = ds.getFullYear();
    var Start_date = curr_month1 + "/" + curr_day1 + "/" + curr_year1;
    var datepicker = $("#datepickerStartDate").data("kendoDatePicker");
    datepicker.value(Start_date);
}
function GetadsDatas() {
    //alert("nn");
    $("#ExportadsGridData").removeAttr("onclick");
    var dataSource = $('#DetailGrid').data().kendoGrid.dataSource.view();
    var len = dataSource.length;
    var ddd = dataSource[0].StoreName;

    var finalVal = '';
    finalVal += "StoreName" + "," + "TotalJob_Number" + "," + "User" + "," + "LastUpdatedTime" + "," + "StatusName";
    finalVal += '\n';
    for (var i = 0; i < dataSource.length; i++) {
        //content.push("['" + dataSource[i].StoreName + "','" + dataSource[i].TotalJob_Number + "','" + dataSource[i].User + "','" + dataSource[i].LastUpdatedTime + "','" + dataSource[i].StatusName + "']");
        for (var j = 0; j < 1; j++) {
            finalVal += dataSource[i].StoreName + "," + dataSource[i].TotalJob_Number + "," + dataSource[i].User + "," + dataSource[i].LastUpdatedTime + "," + dataSource[i].StatusName;
            finalVal = finalVal.replace(/"/g, '""');
        }
        finalVal += '\n';
    }
    // alert(finalVal);

    // var mydiv = document.getElementById("ContentMain_MainTab");
    // var aTag = document.createElement('a');
    var aTag = document.getElementById("ancorexportAds");
    aTag.setAttribute('href', 'data:text/csv;charset=utf-8,' + encodeURIComponent(finalVal));
    aTag.setAttribute('download', 'test.csv');
    aTag.setAttribute('target', '_blank');


    //aTag.setAttribute('id', 'downDataAds');
    //aTag.innerHTML = "link text";
    //mydiv.appendChild(aTag);
    // aTag.click();
}


function getadsexport() {

    if ($.browser.safari) {
        alert("Safari will not properly Export as excel");
        return false;
    }
    else {
        fadname = 'AdsData.xlsx';
        var grid = $("#DetailGrid").getKendoGrid();
        var rows = [{
            cells: [
        { value: "StoreName" },
        { value: "TotalJob_Number" },
        { value: "User" },
        { value: "LastUpdatedTime" },
        { value: "StatusName" }
      ]
        }];

        var trs = grid.dataSource;

        var filteredDataSource = new kendo.data.DataSource({
            data: trs.data(),
            filter: trs.filter()
        });

        filteredDataSource.read();
        var data = filteredDataSource.view();

        for (var i = 0; i < data.length; i++) {

            var dataItem = data[i];

            rows.push({
                cells: [
            { value: dataItem.StoreName },
            { value: dataItem.TotalJob_Number },
            { value: dataItem.User },
            { value: dataItem.LastUpdatedTime },
            { value: dataItem.StatusName }
          ]
            })

        }
        if (rows != undefined && rows != '') {
            excelExport(rows);
            e.preventDefault();
        }
        else {
            alert('Please wait and try again');
            return false;
        }
    }
}
function excelExport(rows) {
    var workbook = new kendo.ooxml.Workbook({
        sheets: [
        {
            columns: [
            { autoWidth: true },
            { autoWidth: true }
          ],
            title: "AdsData",
            filterable: true,
            allPages: true,
            rows: rows
        }
      ]
    });
    kendo.saveAs({ dataURI: workbook.toDataURL(), fileName: fadname });

}


function excelExport(rows) {
    var workbook = new kendo.ooxml.Workbook({
        sheets: [
        {
            columns: [
            { autoWidth: true },
            { autoWidth: true }
          ],
            title: "WebscrapReport",
            rows: rows
        }
      ]
    });
    kendo.saveAs({ dataURI: workbook.toDataURL(), fileName: fadname });

}

function getnewadsexport() {
    if ($.browser.safari) {
        alert("Safari will not properly Export as excel");
        return false;
    }
    else {
        fadname = 'NewAds.xlsx';
        var grid = $("#DivOffcycleNewAdsGrid").getKendoGrid();
        var rows = [{
            cells: [
        { value: "WebSite" },
        { value: "Title" },
        { value: "AdDate" },
        { value: "Count" },
        { value: "LastUpdateby" },
        { value: "LastUpdatedTime" },
        { value: "Status" }
      ]
        }];

        var trs = grid.dataSource;

        var filteredDataSource = new kendo.data.DataSource({
            data: trs.data(),
            filter: trs.filter()
        });

        filteredDataSource.read();
        var data = filteredDataSource.view();

        for (var i = 0; i < data.length; i++) {

            var dataItem = data[i];

            rows.push({
                cells: [
            { value: dataItem.WebSite },
            { value: dataItem.Title },
            { value: dataItem.AdDate },
            { value: dataItem.TotalZipCodeforexport },
            { value: dataItem.UserName },
            { value: dataItem.LastUpdatedTime },
            { value: dataItem.StatusName }
          ]
            })

        }
        if (rows != undefined && rows != '') {
            excelExport(rows);
            e.preventDefault();
        }
        else {
            alert('Please wait and try again');
            return false;
        }
    }
}

function excelExport(rows) {
    var workbook = new kendo.ooxml.Workbook({
        sheets: [
        {
            columns: [
            { autoWidth: true },
            { autoWidth: true }
          ],
            title: "NewAdsReport",
            rows: rows
        }
      ]
    });
    kendo.saveAs({ dataURI: workbook.toDataURL(), fileName: fadname });

}

function getunprocessedexport() {
    if ($.browser.safari) {
        alert("Safari will not properly Export as excel");
        return false;
    }
    else {
        fadname = 'UnprocessAds.xlsx';
        var grid = $("#DivUnprocessAdsGrid").getKendoGrid();
        var rows = [{
            cells: [
        { value: "WebSite" },
        { value: "Title" },
        { value: "AdDate" },
        { value: "Count" },
        { value: "DateFound" },
        { value: "LastUpdatedBy" },
        { value: "LastUpdatedTime" },
        { value: "Status" }
      ]
        }];

        var trs = grid.dataSource;

        var filteredDataSource = new kendo.data.DataSource({
            data: trs.data(),
            filter: trs.filter()
        });

        filteredDataSource.read();
        var data = filteredDataSource.view();

        for (var i = 0; i < data.length; i++) {

            var dataItem = data[i];

            rows.push({
                cells: [
            { value: dataItem.WebSite },
            { value: dataItem.Title },
            { value: dataItem.AdDate },
            { value: dataItem.TotalZipCodeforexport },
            { value: dataItem.DateDetected },
            { value: dataItem.UserName },
            { value: dataItem.LastUpdatedTime },
            { value: dataItem.StatusName }
          ]
            })

        }
        if (rows != undefined && rows != '') {
            excelExport(rows);
            e.preventDefault();
        }
        else {
            alert('Please wait and try again');
            return false;
        }
    }
}
function excelExport(rows) {
    var workbook = new kendo.ooxml.Workbook({
        sheets: [
        {
            columns: [
            { autoWidth: true },
            { autoWidth: true }
          ],
            title: "UnprocessAdsReport",
            rows: rows
        }
      ]
    });
    kendo.saveAs({ dataURI: workbook.toDataURL(), fileName: fadname });

}
function getalreadyexport() {
    if ($.browser.safari) {
        alert("Safari will not properly Export as excel");
        return false;
    }
    else {
        fadname = 'OffcycleAds.xlsx';
        var grid = $("#DivOffcycleOldDeAdsGrid").getKendoGrid();
        var rows = [{
            cells: [
        { value: "WebSite" },
        { value: "Title" },
        { value: "AdDate" },
        { value: "Count" },
        { value: "DateFound" },
        { value: "LastUpdatedBy" },
        { value: "LastUpdatedTime" },
        { value: "Status" }
      ]
        }];

        var trs = grid.dataSource;

        var filteredDataSource = new kendo.data.DataSource({
            data: trs.data(),
            filter: trs.filter()
        });

        filteredDataSource.read();
        var data = filteredDataSource.view();

        for (var i = 0; i < data.length; i++) {

            var dataItem = data[i];

            rows.push({
                cells: [
            { value: dataItem.WebSite },
            { value: dataItem.Title },
            { value: dataItem.AdDate },
            { value: dataItem.TotalZipCodeforexport },
            { value: dataItem.DateDetected },
            { value: dataItem.UserName },
            { value: dataItem.LastUpdatedTime },
            { value: dataItem.StatusName }
          ]
            })

        }
        if (rows != undefined && rows != '') {
            excelExport(rows);
            e.preventDefault();
        }
        else {
            alert('Please wait and try again');
            return false;
        }
    }
}
function excelExport(rows) {
    var workbook = new kendo.ooxml.Workbook({
        sheets: [
        {
            columns: [
            { autoWidth: true },
            { autoWidth: true }
          ],
            title: "OffcycleAdsReport",
            rows: rows
        }
      ]
    });
    kendo.saveAs({ dataURI: workbook.toDataURL(), fileName: fadname });

}
function gettimesheetexport() {
    if ($.browser.safari) {
        alert("Safari will not properly Export as excel");
        return false;
    }
    else {
        fadname = 'Timesheet.xlsx';
        var grid = $("#divfortasksheetexport").getKendoGrid();
        var rows = [{
            cells: [
        { value: "Date" },
        { value: "Name" },
        { value: "Worktime(hh:mm:ss)" }
      ]
        }];

        var trs = grid.dataSource;

        var filteredDataSource = new kendo.data.DataSource({
            data: trs.data(),
            filter: trs.filter()
        });

        filteredDataSource.read();
        var data = filteredDataSource.view();

        for (var i = 0; i < data.length; i++) {

            var dataItem = data[i];

            rows.push({
                cells: [
            { value: dataItem.dateforexport },
            { value: dataItem.WUser },
            { value: dataItem.worktimeinformate }
          ]
            })

        }
        if (rows != undefined && rows != '') {
            excelExport(rows);
            e.preventDefault();
        }
        else {
            alert('Please wait and try again');
            return false;
        }
    }
}
function excelExport(rows) {
    var workbook = new kendo.ooxml.Workbook({
        sheets: [
        {
            columns: [
            { autoWidth: true },
            { autoWidth: true }
          ],
            title: "TimeSheet",
            rows: rows
        }
      ]
    });
    kendo.saveAs({ dataURI: workbook.toDataURL(), fileName: fadname });

}




function getBindOffcycleFilterSt(ComboStatus) {
    if (ComboStatus != null) {
        OffcycleAdsStbind(ComboStatus);
    }
}

function ForGetOffCycleNewAdsFilters(ComboStatus) {
    if (ComboStatus != null) {
        var CDate = $("#DivOffCal").val();
        GetOffcycleNewStAds(CDate, ComboStatus);
    }
}
function Getonisresponscheckchange(checkstatuscombo) {
    var utidd = 0;
    isRespons = "";
    isRespons = false;
    var isRespons = $("#Checkforresponsible").val();
    GetAllOtherTasks(checkstatuscombo, false, false, isRespons);
    isRespons = "";

}


function ForGetAllWorkItemClear(CombStatusList9) {
    var utidd = 0;
    document.getElementById("filtext").value = "";
    var WorkStatusclear = "";
    var isRespons = $("#Checkforresponsible").val().trim();
    $("#Checkforresponsible").val("11");
    // var CombStatusList9 = $find("<%= checkstatuscombo.ClientID %>");
    var items = CombStatusList9.get_checkedItems();
    for (var chkCount = 0; chkCount < items.length; chkCount++) {
        text += items[chkCount].get_text() + ", ";
        WorkStatusclear += items[chkCount].get_value() + ",";

    }
    if (WorkStatusclear != "")
        WorkStatusclear = removeLastComma(WorkStatusclear);
    if (WorkStatusclear.trim() != "8,9,10,33" || isRespons != '11') {
        GetAllOtherTasks(CombStatusList9, true, true, isRespons);
    }
}

function GetLoadWebsiteZipStoreID(RadZipStoreID, WebSiteCombo) {

    if (RadZipStoreID != null) {
        RadZipStoreID.clearSelection();
        RadZipStoreID.set_emptyMessage("---Search storeid or address or zip---");
        LoadStoreIdZipAddress(RadZipStoreID, WebSiteCombo, $("#TxtScreentcal").val());

    }
}

function GetOnStoreZipSelectedIndexchanged(sender, RadComboWebsite) {
    var WebSite = "";
    if (RadComboWebsite != null && RadComboWebsite.get_selectedItem() != null) {
        WebSite = RadComboWebsite.get_selectedItem().get_value()
    }
    ShowNextPreScreenShot(sender, WebSite, sender.get_text(), $("#TxtScreentcal").val(), 2)
}

function GetNexPreScreenShot(RadComboWebsite, RadZipStoreID, IsNext) {
    var WebSite = "";
    var ZipStoreID = "";
    if (RadComboWebsite != null && RadComboWebsite.get_selectedItem() != null) {
        WebSite = RadComboWebsite.get_selectedItem().get_value()
    }
    if (RadZipStoreID != null && RadZipStoreID.get_selectedItem() != null) {
        ZipStoreID = RadZipStoreID.get_selectedItem().get_value();
    }
    ShowNextPreScreenShot(RadZipStoreID, WebSite, RadZipStoreID.get_text(), $("#TxtScreentcal").val(), IsNext)
}

function GetClearWebsiteScreenShotDrop(combo2) {
    $("#BtnPrescreen")[0].style.display = "none";
    $("#btnNextscreen")[0].style.display = "none";
    combo2.clearSelection();
    combo2.set_emptyMessage("---Search storeid or address or zip---");
    $("#ImgscreenShot")[0].src = "";
    $("#DivScreeImglist")[0].style.display = "none";
}
function GetClearScreenShotDrop(combo1, combo2) {
    $("#ImgscreenShot")[0].src = "";
    $("#DivScreeImglist")[0].style.display = "none";
    $("#DivScreenShotTime")[0].style.display = "none";
    $("#DivScreenShotTime")[0].innerHTML = "";
    $("#BtnPrescreen")[0].style.display = "none";
    $("#btnNextscreen")[0].style.display = "none";

    combo1.clearSelection();
    combo1.commitChanges();
    combo1.set_emptyMessage("---Please select website---");
    combo2.clearSelection();
    combo2.commitChanges();
    combo2.set_emptyMessage("---Search storeid or address or zip---");

}

function FileSelected() {
    Upload();
}

function CallShowScreenShotImage(RadComboWebsite, RadZipStoreID) {

    var WebSite = "";
    var ZipStoreID = "";
    if (RadComboWebsite != null && RadComboWebsite.get_selectedItem() != null) {
        WebSite = RadComboWebsite.get_selectedItem().get_value();
    }
    if (RadZipStoreID != null && RadZipStoreID.get_selectedItem() != null) {
        ZipStoreID = RadZipStoreID.get_selectedItem().get_value();
    }
    ShowScreenShotByWebSite(RadZipStoreID, WebSite, RadZipStoreID.get_text(), $("#TxtScreentcal").val(), 2)
}

$('#TxtMessageasana').focus(function () {
    $(this).attr('data-default', $(this).height());
    $(this).animate({ height: 150 }, 'slow');
}).blur(function () {

    var w = $(this).attr('data-default');
    $(this).animate({ height: w }, 'slow');
});
function oncommentfoc() {

    $('#TxtMessageasana').animate({ height: 110 }, 'slow');
}
function oncommentblur() {

    $('#TxtMessageasana').animate({ height: 25 }, 'slow');
}

function spanaddsunTaskClick() {
    $("#spanAddsubtask")[0].style.display = "none";
    $("#txtadd-subtask")[0].style.display = "inline";
    $("#txtadd-subtask").focus();
    
}
function outfromtextsub() {

    $("#txtadd-subtask")[0].style.display = "none";
    $("#spanAddsubtask")[0].style.display = "inline";

}

function LisubEdittask(tid) {
    $("#sub_" + tid)[0].style.display = "none";
    $("#editsub_" + tid)[0].style.display = "inline";
    $("#editsub_" + tid).focus();
}
function editSubtask(tid) {
    $("#editsub_" + tid)[0].style.display = "none";
    $("#sub_" + tid)[0].style.display = "inline";
}


function UpdateSubTask(sender, args,id) {
    if (args.keyCode == 13) {
        var subtitle = $("#editsub_"+id).val();
        $("#txtadd-subtask")[0].value = "";
        if (subtitle != "") {
            $.ajax({
                type: "GET",
                contentType: "application/json; charset=utf-8",
                url: "/DataTracker.svc/UpdateSubTasks?taskid=" + id + "&SubTitle=" + subtitle + "",
                dataType: "json",
                success: function (data) {
                    if (data.d == 0) {
                        GetallSubtasks(s);
                    }


                },
                error: function (result) {

                }
            });
        }
        else {
            alert("please enter title !");
            return false;
        }
    }
}

function deleteSubTask(id) {
    $.ajax({
        type: "GET",
        contentType: "application/json; charset=utf-8",
        url: "/DataTracker.svc/DeleteSub?taskid=" + id + "",
        dataType: "json",
        success: function (data) {
            if (data.d == 0) {
                GetallSubtasks(s);
            }
        },
        error: function (result) {

        }
    });
}

function CompleteSubTask(id) {
    $.ajax({
        type: "GET",
        contentType: "application/json; charset=utf-8",
        url: "/DataTracker.svc/UpdatedSubTasks?taskid=" + id + "&istrue=" + $("#check"+id).is(':checked') + "",
        dataType: "json",
        success: function (data) {
            if (data.d == 0) {
                GetallSubtasks(s);
            }


        },
        error: function (result) {

        }
    });
 }