﻿var BoardNameHeadingOnPage = "";
var boardName = "";
var BoardAccesslity = "";
var BoardID = 0;
var subtitle = "";
var G_CommentID = 0;
var projectpriority;
var CardIDForLabelColor;
var oldCardName = "";
var oldboardname = "";
var cardNameOfComment = "";
var listNameForArchiveList = "";
var spnpopupcolor = "";
var setcolorindex;
var txtmsgval = "";
var BoardComplePer = 0;
var teamnameforhover = "";
var accessmodifiernameforhover = "";
var carididArray = [];
var sss = "";
var estboardtime1 = "";
var estboardcardtime1 = "";
var estboardcardtime2 = "";
var flagWhenBoardCreate = 0;
var boardnamerestrict = "";
var boardnameenterFromsetting = "";
var teamIdForOwner = 0;
var tempForCreatedBy = 0;
var text = "";
var boardnameenter = "";
var cardnameenter = "";
var listnameenter = "";
var flagexport = "";
var index = 0;
var globlaUserid = 0;
var globalboardname = "";
var lblhelptext = "";
var lbltextval = "";
/********************************************* Code for Recentboardlist,Boardsearch,Myboardlist*********************************************/
$(function () {
    $(".blocker").live("click", function () {
        $('.webui-popover').hide();
    });

    $('#btnArchiveBoard').click(function () {
        $('#txtArchiveBoard').show();
    })

});

//function HomeBoard() {
//    var Board_home = [];
//    document.getElementById("BoardList").innerHTML = "";
//    $.ajax({
//        type: "GET",
//        url: "/DataTracker.svc/HomeBoard",
//        contentType: "application/json; charset=utf-8",
//        dataType: "json",
//        success: function (data) {
//            var id = data.d[0].BoardID; // id contain the recently open Board ID.
//            GetCardAndList(id);
//        }
//    });
//}

function GetRecentBoards() {
    document.getElementById("BoardList").innerHTML = "";
    $myList = $('#BoardList');
    if ($myList.children().length === 0) {
        document.getElementById("BoardList").innerHTML = "";
        $.ajax({
            type: "GET",
            url: "/DataTracker.svc/GetRecentBoards",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                document.getElementById("BoardList").innerHTML = "";
                $.each(data.d, function (index, item) {
                    $('#BoardList').append("<li id='list" + item.BoardID + "' style='margin:0; padding:0 0 5px 0;'><a style='float:left;' href='javascript:RecentBoardurl(" + item.BoardID + ");' title='" + item.BoardName + "'>" + item.BoardName + "</a><a  class='testclass'  href='javascript:removerecentboard(" + item.BoardID + ");'>x</a><div class='clr'></div><div id='progress" + item.BoardID + "'></div></li>");
                    $('#progress' + item.BoardID + '').goalProgress({
                        goalAmount: 100,
                        currentAmount: item.TaskCompletePercent,
                        textBefore: '',
                        textAfter: ''
                    });
                });
            }
        });
    }
}

function MyBoardsList() {
    //document.getElementById('MergeBoardspanid').style.display = 'none';
    document.getElementById("MyBoardRecent").innerHTML = "";
    $myList = $('#MyBoardRecent');
    if ($myList.children().length === 0) {
        document.getElementById("MyBoardRecent").innerHTML = "";
        $.ajax({
            type: "GET",
            url: "/DataTracker.svc/GetBoards",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                document.getElementById("MyBoardRecent").innerHTML = "";
                $.each(data.d, function (index, item) {
                    $('#MyBoardRecent').append("<li id='list" + item.BoardID + "' style='margin:0; padding:0 0 5px 0;'><a style='float:left;' href='javascript:myBoardurl(" + item.BoardID + ");' title='" + item.BoardName + "'>" + item.BoardName + "</a><a class='testclass' href='javascript:removeboard(" + item.BoardID + ");'>x</a><div class='clr'></div><div id='progress1" + item.BoardID + "'></div></li>");
                    $('#ddl_FromBoard').append($("<option></option>").val(item.BoardID).html(item.BoardName));
                    $('#progress1' + item.BoardID + '').goalProgress({
                        goalAmount: 100,
                        currentAmount: item.TaskCompletePercent,
                        textBefore: '',
                        textAfter: ''
                    });
                });
            }
        });
    }
}

function GetBoardsSearchs(sender, args) {
    if (args.keyCode != 13) {
        var availableTags = [];
        subtitle = $("#txtSrch").val();
        var len = subtitle.length;
        if ((len > 1)) {
            $.ajax({
                type: "GET",
                url: "/DataTracker.svc/GetBoardsSearch?BoardsName=" + subtitle + "",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    $.each(data.d, function (index, item) {
                        availableTags = availableTags.concat(item.BoardName);
                        $("#txtSrch").autocomplete({ source: availableTags });
                    });
                }
            });
        }
    }
}


$(".ui-autocomplete").live("click", ".ui-menu-item", function () {
    var boardname1 = $.trim($('#txtSrch').val()).replace(/'/g, "").replace(/\n/g, "").replace(/"/g, "").replace(/\\/g, "").replace(/\//g, "");
    if (boardname1 != "") {
        $.ajax({
            type: "POST",
            url: "/DataTracker.svc/GetBoarddata",
            data: "{\"Boardname\": \"" + boardname1 + "\"}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",

            success: function (data) {
                var boardid = data.d;
                var pathname = window.location.pathname;
                if (pathname == "/AdminDashboard.aspx") {
                    window.location.href = "/board.aspx?" + boardid + "";
                }
                else {
                    window.location.href = "/board.aspx?" + boardid + "";
                }
                flagWhenBoardCreate = 1;
                BoardID = boardid;
                CalculateBoardComplete(BoardID);
                second();
                $('#txtSrch').val('');
            }
        })
    }

});


/********************************************* Code for Recentboardlist,Boardsearch,Myboardlist*********************************************/

/********************************************* Code for Create,Remove,Edit Board***********************************************************/

function CreateBoard() {
    
    boardName = $.trim($("#txtCreateBoard").val()).replace(/'/g, "").replace(/\n/g, "").replace(/"/g, "").replace(/\\/g, "").replace(/\//g, "");
    if (boardName.length == 0) {
        document.getElementById('createboardspan').innerText = "* Please enter the board name!";
        document.getElementById('createboardspan').style.color = 'red';
    }
    else {
        document.getElementById('createboardspan').style.display = 'none';
        BoardAccesslity = $("#BoardAccessModifier").text();
        var Is_Public = 2;
        if (BoardAccesslity == 'Public') {
            Is_Public = 2;
        }
        else if (BoardAccesslity == 'Private') {
            Is_Public = 1;
        }
        var TeamTitle = "";                                       // Added By Ravi For Team Works
        var TeamTitle = $("#ddl_Team option:selected").text();      // Added By Ravi For Team Works
        var TeamID = $("#ddl_Team").val();
        var templateId = $("#templates").val();
        // Added By Ravi For Team Works
        $.ajax({
            type: "POST",
            url: "/DataTracker.svc/CreateBoard",
            data: "{\"boardname\": \"" + boardName + "\",\"ispublic\": \"" + Is_Public + "\",\"createdBy\": \"" + cuid + "\",\"boardteamID\": \"" + TeamID + "\",\"templateId\": \"" + templateId + "\"}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                if (data.d == 0) {
                    document.getElementById('createboardspan').style.display = 'Block';
                    document.getElementById('createboardspan').innerText = "* Board Name Already Exists!";
                    document.getElementById('createboardspan').style.color = 'red';
                }
                else {

                    flagWhenBoardCreate = 1;
                    BoardID = data.d;
                    SetDefaultCardColor();
                    SendEmailToBoardTeamMember(boardName, TeamID);
                    $("#progressbar").css('display', 'none');
                    window.location.href = "/board.aspx?" + data.d + "";
                }
            },
            error: function (error) {
            }
        });
    }
}

function SendEmailToBoardTeamMember(BName, teamid) {
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "/DataTracker.svc/SendEmailToBoardTeam",
        data: "{\"Bid\": \"" + BoardID + "\",\"boardname\": \"" + BName + "\",\"teamid\": \"" + teamid + "\"}",
        dataType: "json",
        async: false,
        success: function (data) {
        },
        error: function (error) {
        }
    });
}

function removeboard(id) {
    var p = confirm("Are you sure you want to permanently delete this item ?");
    if (p) {
        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: "/DataTracker.svc/RemoveBoard",
            data: "{\"Bid\": \"" + id + "\"}",
            dataType: "json",
            success: function (data) {
                $('#BoardList').empty();
                $('#MyBoardRecent').empty();
                var boardId = $('#boardID').val();
                GetRecentBoards();
                MyBoardsList();
                HomeBoard();
                window.location.href = "/Board.aspx?" + boardId
            },
            error: function (error) {
            }
        });
    }
}

function removerecentboard(id) {
    var p = confirm("Are you sure you want to delete this item ?");
    if (p) {
        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: "/DataTracker.svc/removerecentboard",
            data: "{\"Bid\": \"" + id + "\"}",
            dataType: "json",
            success: function (data) {
                $('#BoardList').empty();
                var boardId = $('#boardID').val();
                GetRecentBoards();
                window.location.href = "/Board.aspx?" + boardId
            },
            error: function (error) {
            }
        })
    }
}

function editboardname() {
    var boarnamewidth = $("#BoardName").width();
    var board__name = $('#BoardName').text();
    oldboardname = board__name;
    $('#BoardName').hide();
    $('#BoardName').after("<textarea id='txtareaboardname' maxlength='75' onblur='javascript:editboardonblur(" + BoardID + ");'  onkeyup='javascript:editboardonenter(" + BoardID + ",this,event);'>" + board__name + "</textarea>");

    $("#txtareaboardname").keydown(function () {
        if ($("#txtareaboardname").val().length > 1) {
            $("#txtareaboardname")[0].cols = $("#txtareaboardname").val().length + 8;
        }
    });
    document.getElementById('txtareaboardname').focus();

}

function editboardonblur(ID) {
    var editedboardname = $.trim($('#txtareaboardname').val()).replace(/'/g, "").replace(/\n/g, "").replace(/"/g, "").replace(/\\/g, "").replace(/\//g, "");
    if (boardnameenter == "Enter") {
        boardnameenter = "";
        return false;
    }
    else {
        if ($.trim(editedboardname).length > 0) {
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "/DataTracker.svc/EditBoardonEnterandBlur",
                data: "{\"BoardNAME\": \"" + editedboardname + "\",\"boardID\": \"" + ID + "\"}",
                dataType: "json",
                success: function (data) {
                    if (data.d != "exist") {
                        alert("Board name added/updated successfully.");
                        var updatedboardname = oldboardname + " to " + editedboardname;
                        UpdateActivityListItem(updatedboardname, 20);
                        $('#txtareaboardname').remove();
                        $("#BoardName").show().html(data.d + "<img src='/Images/pencil5.png' alt='Edit' title='Click for edit.' />");
                        $("#hboardname").show().html(data.d + "<img src='/Images/pencil5.png' alt='Edit' title='Click for edit.' />");
                        var favoriteboardlistid = "#Favorite" + ' ' + "#list" + ID;
                        var favoriteboardlistidd = favoriteboardlistid + ' ' + 'a' + "#" + ID;
                        $(favoriteboardlistidd).text(editedboardname);
                        boardName = data.d;
                        GetRecentBoards();
                        MyBoardsList();
                    }
                    else {
                        alert("Board name added/updated successfully.");
                        $('#txtareaboardname').remove();
                        $("#BoardName").show();
                        $("#hboardname").show()
                    }
                }
            })
        }
        else {
            var board__name = $('#BoardName').text();
            alert("board cannot be empty");
            $('#txtareaboardname').val(board__name).focus();
        }
    }
}

function editboardonenter(ID, sender, args) {
    if (args.keyCode == 13) {
        var editedboardname = $.trim($('#txtareaboardname').val()).replace(/'/g, "").replace(/\n/g, "").replace(/"/g, "").replace(/\\/g, "").replace(/\//g, "");
        if ($.trim(editedboardname).length > 0) {
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "/DataTracker.svc/EditBoardonEnterandBlur",
                data: "{\"BoardNAME\": \"" + editedboardname + "\",\"boardID\": \"" + ID + "\"}",
                dataType: "json",
                success: function (data) {
                    if (data.d != "exist") {
                        alert("Board name added/updated successfully.");
                        boardnameenter = "Enter";
                        var updatedboardname = oldboardname + " to " + editedboardname;
                        UpdateActivityListItem(updatedboardname, 20);
                        $('#txtareaboardname').remove();
                        $("#BoardName").show().html(data.d + "<img src='/Images/pencil5.png' alt='Edit' title='Click for edit.' />");
                        $("#hboardname").show().html(data.d + "<img src='/Images/pencil5.png' alt='Edit' title='Click for edit.' />");
                        var favoriteboardlistid = "#Favorite" + ' ' + "#list" + ID;
                        var favoriteboardlistidd = favoriteboardlistid + ' ' + 'a' + "#" + ID;
                        $(favoriteboardlistidd).text(editedboardname);
                        boardName = data.d;
                        GetRecentBoards();
                        MyBoardsList();
                    }
                    else {
                        boardnameenter = "Enter";
                        alert("Board name added/updated successfully.");
                        $('#txtareaboardname').remove();
                        $("#BoardName").show();
                        $("#hboardname").show()
                    }
                }
            })
        }
        else {
            boardnameenter = "Enter";
            var board__name = $('#BoardName').text();
            alert("board cannot be empty");
            $('#txtareaboardname').val(board__name).focus();
        }
    }
}

function UpdateEstimatedTime(spid) {

    $('#hdsprintestimatetime').val(spid);
    $('#Inviteonboard').css('display', 'none');
    $('#Updateestimatedtime').css('display', 'block');
    $('#txtestimatetime').val($('.hourcomplete_' + spid).html().replace(':00', ''));
    $('#myModal').modal();
    //alert(spid);
}

/********************************************* Code for Create,Remove,Edit Board***********************************************************/

/********************************************* Code for Create,Remove,Edit List************************************************************/
//Ameeq Changes for Swimlanes 08/03/18//
var checkSpName = 1;
var ams = 0;


function renderBoardDetail(item) {

    $('#firstsprint').val(item.SprintBoardRelationId);
    $('#BoardName').html(item.BoardName + "<img src='/Images/pencil5.png' alt='Edit' title='Click for edit.' />");
    $('#hboardname').html(item.BoardName + "<img src='/Images/pencil5.png' alt='Edit' title='Click for edit.' />");
    //Ameeq Changes for Swimlanes 08/03/18//
    $("#SprintName").html("<div id=composer class='card-composer hide'><div style='color:red; font-size:80%;clear:both;' id='span'></div><textarea maxlength='100'  id='textaera'  class='js-search-boards' placeholder=''></textarea><a id='addNewcard1' class='fl create_button' onclick='javascript:AddSprintToListOnAddButtonClick()'  href='#'>Add</a><div id='cancelcard' onclick='mysprint();' class='cross'>Cancel</div><p id='loadstage' class='displayloadstage'>Adding...</p><a href='#' class='list_action show-pop btn btn-default bottom-right' data-placement='auto-top' ></a>   </div><a id=cardaddid  href='#exsp' rel='modal:open' draggable='false' class='add_sprint'></a>");
    if (item.IsPublic == 2) {
        $('#AccessModifier').html("&#x2606;Public");
        $('#AccessModifier1').html("&#x2606;Public");
    }
    else if (item.IsPublic == 1) {
        $('#AccessModifier').html("&#x2606;Private");
        $('#AccessModifier1').html("&#x2606;Private");
    }
    $("#boardID").val(item.BoardID);
    $('#favoriteID').val('false');
    $('#FavoriteModifier').html("&#9825;");

    tempForCreatedBy = item.CreatedBy;
    if (item.TeamID != 0) {
        $("#TeamName").show();
        $("#teamID").val(item.TeamID);
        $("#TeamName").html("&#x2600;" + item.TeamName);
        teamIdForOwner = item.TeamID;

        $("#TeamName1").show();
        $("#TeamName1").html("&#x2600;" + item.TeamName);
        $("#teamnameforheader").text(item.TeamName);
        $("#teamnameforheader1").text(item.TeamName);
        $("#teamlabel").hide();
        $("#change2").hide();
        bindddlforchangeteam(item.TeamID);

        var selectedText = item.TeamName;
        $('#ddlteamtoboard option').map(function () {
            if ($(this).text() == selectedText) return this;
        }).attr('selected', 'selected');
    }
    else {
        $("#TeamName").hide();
        $("#TeamName1").hide();
        $("#teamlabel").show();
        $("#teamID").val('0');
        $("#ddlteamtoboard").val('0');
        $("#ddlteamtoboard1").val('0');
        $("#ddlteamtoboard2").val('0');
        $("#ddlteamtoboard3").val('0');
        bindddlforchangeteam(item.TeamID);
    }
    if (item.IsFavorite == 'true') {
        $('#favoriteID').val('true');
        $('#FavoriteModifier').html("&#9829;");
    }
    else {
        $('#favoriteID').val('false');
        $('#FavoriteModifier').html("&#9825;");
    }
    if (item.IsArchived == true) {
        $("#DivSchedule").css({ "background": "#808080", "pointer-events": "none", "cursor": "default", "opacity": "0.5" });
    }
    var ownerid = $('#ownerID').val();
    if (cuserid == ownerid || usertypeid == 12 || usertypeid == 14) {
        if (item.IsEnabledBoardComplete == true) {
            $('#ckCompleteBoardId').attr('checked', true);
            $('#progressbar').css('display', 'block');
        }
        else {
            $('#progressbar').css('display', 'none');
        }
    }
    else {
        $(".newboard1").removeAttr("href");
        $(".newboard1").removeAttr("rel");
    }
    globalboardname = item.BoardName;
    var usertype = $('#usertypeid').val();
    if (usertype == 12) {
        if (item.Hide_showPublicBoard > 0) {
            $("#HideCardsFromOtherUsers").css('display', 'none');
            $("#ShowCardsToOtherUsers").css('display', 'block');
        }
        else {
            $("#HideCardsFromOtherUsers").css('display', 'block');
            $("#ShowCardsToOtherUsers").css('display', 'none');
        }
    }
    else {
        $("a.Hide_showCardsFromOtherUsers").css('display', 'none');
    }
}

function GetBoardDetails(BoardID) {
    if (window.location.pathname == "/board.aspx") {
        $.ajax({
            type: "POST",
            url: "/DataTracker.svc/GetBoardDetails",
            data: JSON.stringify({ BoardID: BoardID }),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                // Basic Variables
                 
                var currSprint = 0;
                var currList = 0;
                var currCart = 0;
                var sphight = 0;
                
                var usertypeid = $('#usertypeid').val();
                var cuserid = $('#cuserid').val();
                var d = JSON.parse(data.d);
                if (d.length > 0) {
                    //Render Srpints
                    $.each(d, function (index, item) {
                        if (item.SprintBoardRelationId != null) {
                            //Bind Sprint data
                            if (currSprint == 0) {
                                renderBoardDetail(item);
                            }
                            if (currSprint !== item.SprintBoardRelationId) {
                                currSprint = item.SprintBoardRelationId;
                                sphight = 0;
                                var temp = 0;
                                var TotalSpenttimeBySprint = 0;
                                //Hidden fild for Dynamic Sprint Height
                                $("#multi").append("<input class='Spheight_" + item.SprintBoardRelationId + "' type='hidden'/>");
                                renderSprint(item);
                                var date = new Date(item.DueDate);
                                var month = date.getMonth() + 1 < 10 ? '0' + (date.getMonth() + 1) : date.getMonth() + 1;
                                var day = date.getDate() < 10 ? '0' + date.getDate() : date.getDate();
                                $("#boardduedate").val(date.getFullYear() + '-' + month + '-' + day);
                                //Bind Sprint Deudate
                                checkdays(item.SprintDueDate, item.SprintBoardRelationId);
                                var currSprintList = d.filter(function (el) {
                                    return (el.SprintBoardRelationId === item.SprintBoardRelationId);
                                });
                                if (currSprintList.length > 0) {
                                    var spwidth = 0;
                                    setcolorindex = 0;
                                    //Bind Sprint List Data
                                    $.each(currSprintList, function (ListIndex, ListItem) {
                                        if (ListItem.ProjectID != null) {
                                            if (currList !== ListItem.ProjectID) {
                                                currList = ListItem.ProjectID;
                                                spwidth++;
                                                temp++;
                                                
                                                var currSprintCart = d.filter(function (el) {
                                                    return (el.ProjectID === ListItem.ProjectID);
                                                });
                                                //Bind List
                                                renderList(0, 0, 12, 2, ListIndex, ListItem, setcolorindex);
                                                setcolorindex++;
                                                var CC = 1;
                                                if (currSprintCart.length > 0) {
                                                    //Make Dynamic Selectors
                                                    var composer = "composer" + ListItem.ProjectID;
                                                    var dfdfdf = "cardaddid" + ListItem.ProjectID;
                                                    var abc = "#cardlist" + ListItem.ProjectID + "_" + item.SprintBoardRelationId;
                                                    var composerSprint2 = "composerSprint2" + ListItem.ProjectID;
                                                    //Bind List Card data
                                                    $.each(currSprintCart, function (CartIndex, CartItem) {
                                                        debugger;
                                                        if (CartItem.TaskID != null) {
                                                            if (currCart !== CartItem.TaskID) {
                                                                ////Task Comment count
                                                                //var currCartComments = currSprintList.filter(function (el) {
                                                                //    return (el.TaskID === ListItem.TaskID &&
                                                                //        el.CommentID !== null);
                                                                //});
                                                                ////Task Attachment Count
                                                                //var currCartAttachments = currSprintList.filter(function (el) {
                                                                //    return (el.TaskID === ListItem.TaskID &&
                                                                //        el.FileID !== null);
                                                                //});
                                                                var CardsCount = CC++;
                                                                $('#listCount' + CartItem.ProjectID).html("");
                                                                $('#listCount' + CartItem.ProjectID).html("<div class='listCount' id='listCount" + CartItem.ProjectID + "' title='Total Cards'>" + CardsCount + "</div>");
                                                                currCart = CartItem.TaskID;
                                                                //Bind card
                                                                //renderCard(abc, CartIndex, CartItem, currCartComments, currCartAttachments, CartItem.CommentID, CartItem.FileID);
                                                                 
                                                                if (CartItem.Hide_showPublicBoard > 0) {
                                                                    if (usertypeid == "12" || CartItem.OwnerId == cuserid || CartItem.BoardTeamMembers.indexOf(cuserid) != -1) {
                                                                        renderCard(abc, CartIndex, CartItem, CartItem.CommentCount, CartItem.AttachmentCount, CartItem.CommentID, CartItem.FileID);
                                                                    }
                                                                    else if (CartItem.TaskCreatedBy == cuserid) {
                                                                        renderCard(abc, CartIndex, CartItem, CartItem.CommentCount, CartItem.AttachmentCount, CartItem.CommentID, CartItem.FileID);
                                                                    }
                                                                }
                                                                else {
                                                                    renderCard(abc, CartIndex, CartItem, CartItem.CommentCount, CartItem.AttachmentCount, CartItem.CommentID, CartItem.FileID);
                                                                }
                                                                TotalSpenttimeBySprint = TotalSpenttimeBySprint + CartItem.TotalHours;
                                                            }
                                                            //BindMembersInCard(CartItem.TaskID, CartItem.Email);
                                                        }
                                                        else {
                                                            $('#listCount' + CartItem.ProjectID).html("");
                                                            $('#listCount' + CartItem.ProjectID).html("<div class='listCount' id='listCount" + CartItem.ProjectID + "' title='Total Cards'>0</div>");
                                                        }
                                                    });
                                                    //Bind Add New Card Option
                                                    
                                                    if (ListIndex > 0) {
                                                        $("#cardaddid" + ListItem.ProjectID).css("display", "none");
                                                    }
                                                    else {
                                                        renderAddNewCard(composer, dfdfdf, abc, ListItem.ProjectID, ListItem.ProjectType, ListItem.BoardID, item.SprintBoardRelationId);
                                                    }
                                                    //Make Sprint Dynamic Height
                                                    var SprintLength = $('#' + ListItem.ProjectID).height();
                                                    if (SprintLength >= sphight) {
                                                        sphight = SprintLength;
                                                        SprintLength = 50 + sphight + 'px';
                                                        $(".Spheight_" + item.SprintBoardRelationId).val(SprintLength);
                                                    }
                                                }
                                            }
                                        }
                                    });
                                }
                                //Bind Add New List Option
                                renderAddnewList(spwidth, item.SprintBoardRelationId);
                                // Bind swaping details
                                sortable_add_newList(item.SprintBoardRelationId);
                                //Get Sprint Estimated time
                                GetTotalTimeEstimate(TotalSpenttimeBySprint, item.EstimatedHours, item.SprintBoardRelationId);
                            }
                        }
                    });
                    GetActivityLogDateWise();
                    var firstsprintid = $("#firstsprint").val();
                    ShowSprint(firstsprintid);
                }
            },
            error: function (error) {
            }
        });
    }
    else {
        //var urll = urlforboard + BoardID + "";
        //window.location.href = urll;
    }
}
function Selectsprintduedate(sprintid) {
    //alert(sprintid);
    $('#boardduedatedays_' + sprintid).pickmeup({
        position: 'bottom',
        hide_on_select: true,
        change: function (formatted) {
            ChangeSprintDuedate(sprintid, formatted);
            $('.pickmeup').css('display', 'none');
            min: date1
        }
    });
}

function SetSwimlanes(swimlanseItem) {
    $('#hdSwimlanesid').val(swimlanseItem.SprintBoardRelationId);
    $('#rename_Swimlanes').val($('#HideSprint_' + swimlanseItem.SprintBoardRelationId + ' span').html());
    $("#setSwimlanesmodal").modal('show');
}

function SetSwimlanesOnSaveNewSprint(SprintBoardRelationId) {
    $('#hdSwimlanesid').val(SprintBoardRelationId);
    $('#rename_Swimlanes').val($('#HideSprint_' + SprintBoardRelationId + ' span').html());
    $("#setSwimlanesmodal").modal('show');
}

function renderSprint(item) {
    $("#multi").append("<div class='panel-heading' role='tab' id='ShowSprint_" + item.SprintBoardRelationId + "' style='display:block;'><img src='Images/CaptureColapsed.JPG' onclick='ShowSprint(" + item.SprintBoardRelationId + ");'><i class='fa fa-cog' aria-hidden='true' id='sprint" + item.SprintBoardRelationId + "' onclick='javascript:SetSwimlanes(" + JSON.stringify(item) + ");'></i><span onclick='ShowSprint(" + item.SprintBoardRelationId + ");'>" + item.Sprint_Name + "</span></div><div class='estimatedprogress multi_" + item.SprintBoardRelationId + "' style='height:auto; display:none;' class='Sprintbdr'><hr class='hr1'><div class='SprintProgress_" + item.SprintBoardRelationId + "' id='SprintProgress'><div id='barforestimate_" + item.SprintBoardRelationId + "' class='curentindex barforestimate' style='right: 405px; border-radius: 7px;' onclick='UpdateEstimatedTime(" + item.SprintBoardRelationId + ");'></div><div id='progressbar_" + item.SprintBoardRelationId + "' class='ui-progressbar ui-widget ui-widget-content ui-corner-all progressbar' role='progressbar' > <span id='check' class='pblabel_" + item.SprintBoardRelationId + "'></span> </div>  <div id='boardduedatedays_" + item.SprintBoardRelationId + "' class='boardduedatedays' onclick='Selectsprintduedate(" + item.SprintBoardRelationId + ")'></div> </div> <hr class='hr2'><div id='auto' class='content'></div></div><div class='accordion-sprint-header' id='HideSprint_" + item.SprintBoardRelationId + "'  style='display:none'><img src='Images/CaptureExpanded.JPG' onclick='HideSprint(" + item.SprintBoardRelationId + ");'><i class='fa fa-cog' aria-hidden='true' id='sprint" + item.SprintBoardRelationId + "' onclick='javascript:SetSwimlanes(" + JSON.stringify(item) + ");'></i><span onclick='HideSprint(" + item.SprintBoardRelationId + ");'>" + item.Sprint_Name + "</span></div>");
}

function renderAddnewList(spwidth, sprintID) {
    $('#multi').append("<div id=" + sprintID + " class='add_list Sprint_" + sprintID + "' style='display:none'><div id=" + sprintID + " class='card-composer hide'><input id='txtlistbox" + sprintID + "' maxlength='75' class='js-search-boards' onkeyup='javascript:AddList(this,event)' type='text' placeholder='Add a list'/> <br/> <div id='CardNoLimit'> <h3>Max Card Limit:</h3> <input  onkeypress='return IsNumerictextbox(event);' maxlength='3' type='text' id='NoOfCards" + sprintID + "'/> <div class='clr'></div></div> <div id='IsSequentialdiv' ><h3>Is sequential :</h3> <input type='CheckBox' id='IsSeqentialCheckBox" + sprintID + "' value='1'/> <div class='clr'></div> </div> <a id='addlist' class='fl create_button' href='#' onclick='javascript:AddListBySaveButton(" + sprintID + ")'>Save</a><div id='cancellist' onclick='crossl();' class='crossl'>Cancel</div><p id='listloadstage' class='displayloadstage'>Adding List...</p><div class='clr'></div></div><a id='sprint" + sprintID + "' onclick='myaddl(" + sprintID + ");' class='addl'>Add a list</a>  </div><hr id='hr0_" + sprintID + "' class='hr0'></div>");

    var swidth = $('.cd-main-header').width();
    spwidth = spwidth * 350;
    if (spwidth > swidth) {
        var hdwd = $(".Spwidth").val();
        var wd = 50 + spwidth;
        $(".Spwidth").val(wd);
        if (hdwd > wd) {
            $(".Spwidth").val(hdwd);
            spwidth = hdwd + 'px';
            $('#multi').css('width', spwidth);
        }
        else {
            spwidth = wd + 'px';
            $('#multi').css('width', spwidth);
        }

    }
    else {
        swidth = spwidth;
    }
}

function renderAddNewCard(composer, dfdfdf, abc, ProjectID, ProjectType, BoardID, CardSprintId) {
    $(abc).after("<div id=" + composer + " class='card-composer hide'><div style='color:red; font-size:80%;clear:both;' id='span" + ProjectID + "'></div><textarea maxlength='200'  id='textaera" + ProjectID + "' onkeyup='javascript:AddCardToList(this, event," + ProjectID + "," + BoardID + ",\"" + ProjectType + "\")' class='js-search-boards' placeholder=''></textarea><a id='addNewcard1' class='fl create_button' onclick='javascript:AddCardToListOnAddButtonClick(" + ProjectID + "," + BoardID + "," + CardSprintId + ",\"" + ProjectType + "\")'  href='#'>Add</a><div id='cancelcard' onclick='mycross(" + ProjectID + ");' class='cross'>Cancel</div><p id='loadstage' class='displayloadstage'>Adding...</p><a href='#' class='list_action show-pop btn btn-default bottom-right' data-placement='auto-top' ></a>   </div>    <a id=" + dfdfdf + "  onclick='myFunction(" + ProjectID + ");' draggable='false' class='add_card'>Add a card...</a> ");
    $(".enablecardclick" + ProjectID + "").html("");
    $(".enablecardclick" + ProjectID + "").append("Disable Add a Card...");
}

function renderList(CardsNo, IsSequential, AdminTypeID, i, index, item, setcolorindex) {
    if (item.IsSequential == 1) {
        IsSequential = "Type: Sequential";
    }
    else {
        IsSequential = "Type: Normal";
    }
    if (item.CardsNo != 0) {
        CardsNo = "Cards Limit: " + item.CardsNo;
    }
    else { CardsNo = "Cards Limit: Max"; }

    var projectname = item.ProjectType;
    if (projectname.length >= 20) { projectname = projectname.substr(0, 20) + '.....'; }
    else { projectname = projectname; }

    var cuserid = $('#cuserid').val();
    var usertypeid = $('#usertypeid').val();
    var ownerid = $('#ownerID').val();
    if (cuserid == ownerid || usertypeid == 12 || usertypeid == 14) {
        $('#ArchiveBoard').css('display', 'block');
    }
    //if (index >= AdminTypeID) {
    //    setcolorindex = 0;
    //    AdminTypeID = AdminTypeID * i;
    //    i++;
    //}
    var colorcollection = ["#ee9f31", "#ef662a", "#ee4526", "#d1101f", "#a20080", "#780992", "#8742a6", "#1061e0", "#129eb1", "#008e55", "#84ab3d", "#dcc609"];
    var randomcolor = colorcollection[setcolorindex];
    setcolorindex++;
    $("#multi").append("<span id='" + item.ProjectID + "' class='layer tile Sprint_" + item.SprintBoardRelationId + "' ondrop='javascript:OnClickProject(" + item.ProjectID + ");' data-force='30'><div  class='project_header' style='background:" + randomcolor + "'><div class='tile__name' title='Click for edit.' onclick='editlistname(" + item.ProjectID + ");' >" + item.ProjectType + "<img src='/Images/pencil5.png' title='Click for edit.' /></div><div id='list_action'><a href='#' class='linkin' onclick='openarchive(" + item.ProjectID + ")' id=linkin" + item.ProjectID + ">&#8230;</a><div id='pops" + item.ProjectID + "' class='webui-popoverab'><div class='js-pop-over-header' style='position:relative;'><div class='pop-over-header-title1' >List Actions</div><a onclick='closearchive()'   id='archiveclose' href='#' class='pop-close'>×</a></div><div><div class='pop-over-content js-pop-over-content u-fancy-scrollbar js-tab-parent'><ul class='pop-over-list'> <li> <a class='js-close-list' onclick='exportsExcel(" + item.ProjectID + "," + item.BoardID + "," + item.SprintBoardRelationId + ")'>Export</a>  </li> </ul><ul class='pop-over-list'><li><a class='js-close-list' onclick='ArchiveList(" + item.ProjectID + "," + item.BoardID + "," + item.SprintBoardRelationId + ")'>Archive This List</a></li></ul><ul class='pop-over-list'><li><a onclick='showArchivepage(" + item.ProjectID + "," + item.BoardID + "," + item.SprintBoardRelationId + ");'>Show Archived List..</a></li></ul><hr><ul class='pop-over-list'><li>	<a onclick='openarchivelist(" + item.ProjectID + ")' class='js-archive-cards'>Archive All Cards in This List…</a></li><li>	<a onclick='openundoarchivelist(" + item.ProjectID + "," + item.BoardID + ")' class='js-move-cards'>Show Archived Card…</a></li><li>	<a class='enablecardclick" + item.ProjectID + "' onclick='enablenewcard(" + item.ProjectID + "," + item.BoardID + "," + item.SprintBoardRelationId + ",\"" + item.ProjectType + "\")' class='js-archive-cards'>Enable Add a Card...</a></li><li>	<a class='MemberlistId' onclick='AddMemberToList(" + item.ProjectID + "," + item.BoardID + ")' class='js-move-cards'>Define list owner</a></li></ul><hr><span><input type='text' class='Cardhoursbylist' id='List_" + item.ProjectID + "'/><span>Hour<span><input type='button' class='btnsetcardhoursbylist' value='Set Card Hours' onclick='UpdateCardDueHoursByList(" + item.ProjectID + ")'/></span></div></div></div></div><div class='clr'></div>    <div id='webui" + item.ProjectID + "' class='pop-over is-shown webuiabc'><div><div class='pop-over-header2 js-pop-over-header'><a onclick='openarchivelistleftarrow(" + item.ProjectID + ")' class='leftarrow2' href='#'>←</a><h3>Archive All Cards in this List?</h3><a onclick='openarchivelistpopclose()'class='pop-close2' href='#'>×</a><div class='clr'></div></div><div><div class='pop-over-content js-pop-over-content u-fancy-scrollbar js-tab-parent' style='max-height: 529px;'><div class='para-part'><p>This will remove all the cards in this list from the board. To view archived cards and bring them back to the board, click  “Show Archived Card…”</p><a href='#' class='js-confirm negate full archive_button' onclick='ArchiveAllCardfromList(" + item.ProjectID + ")' >Archive All</a></div></div></div></div></div> <a onclick='boxdiv(" + item.ProjectID + ")' id='listproprety" + item.ProjectID + "' herf='#'><small class='IsSequential' >" + IsSequential + "</small><small>,&nbsp;</small><small class='Cardlimit'>" + CardsNo + "</small><small>&nbsp;</small><small><i class='fa fa-pencil' aria-hidden='true' title='Click for edit'></i></small></a><div class='listCount' id='listCount" + item.ProjectID + "' title='Total Cards'></div><div id='isseq" + item.ProjectID + "' class='box-ab'><div class='box-1a'><h3 id='pname' title='" + item.ProjectType + "'>" + projectname + "<input type='hidden' id='projectId' value=" + item.ProjectID + " /></h3><a onclick='divboxclose(" + item.ProjectID + ")'class='box-close' href='#'>&#10006</a><div class='clr'></div></div><div class='banner'></div><div class='box-1b'><form class='card-box'><label> Max Card Limit:</label><input type='text' id='max-card' onkeypress='return IsNumerictextbox(event);' maxlength='3'>&nbsp;<div class='clr'></div><label>sequential</label><input type='checkbox' name='sequential' value='sequential' id='sequentialcheckbox' ><div class='clr'></div> <div id='rmsg' style='color: #69aa6f;display:none'>Update Successfully</div> <br><a onclick='ChangelimitofCard(" + item.ProjectID + "," + item.SprintBoardRelationId + ")' class='edit-box' href='#'>Submit</a><div class='clr'></div> </form></div></div> </div> <div class='tile__list ui-sortable' spid='sprintlist" + item.SprintBoardRelationId + "' id='cardlist" + item.ProjectID + "_" + item.SprintBoardRelationId + "'></div></span>");

    if (item.TotalHours != null) {
        var txtEstimated = item.TotalHours;
        if (txtEstimated == 0) {
            txtEstimated = txtEstimated + '.0';
            var txtval = txtEstimated.split('.');
            if (txtval[0] == 0) {
                $('#txtEstimated').val("");
                estboardtime1 = "";
            }
            else {
                $('#txtEstimated').val(txtval[0]);
                estboardtime1 = txtval[0];
            }
        }
    }
    if (item.EstimatedTimeForCard != null) {
        var estimatedval = item.EstimatedTimeForCard;
        var esval1 = estimatedval.split('.');
        if (esval1[0] == 0) {
            $('#txtCardEstimatedTime').val("0");
        }
        else { $('#txtCardEstimatedTime').val(esval1[0]); }

        var estime = esval1[1];
        estboardcardtime1 = esval1[0];

        if (estime == "00") {
            $('#txtCardEstimatedminute').val("");
        }
        else {
            $('#txtCardEstimatedminute').val(estime);
            estboardcardtime2 = estime;
        }
    }
}

function renderCard(abc, index, item, currCartComments, currCartAttachments, cardCommentID, cardAttachmentID) {
    //var SprintCount = listinformation.length;
    var due_date = "";
    var Show_due_date = "";
    var Task_Description = "";
    var texts = "";
    var bg_color = "";
    var today = new Date();
    var MoveHours = "";
    if (item.MoveDate != null) {
        if (item.MoveDate.length > 0) {
            MoveHours = Math.round((today - new Date(item.MoveDate)) / 3600000);
            var movehours1 = Math.round(MoveHours / 24);
            if (MoveHours > 24) {
                MoveHours = "This has not been moved from last " + Math.round(MoveHours / 24) + " Days";
            }
            else {
                MoveHours = "This has not been moved from last " + MoveHours + " Hours";
            }
        }
    }
    if (item.DueDate != null) {
        if (item.DueDate.length > 0) {
            due_date = formatDate(item.DueDate);
            if (due_date == '01 Jan 1990') {
                //due_date = "";
                due_date = due_date.replace(/\s+/g, '-');
                var due_date_time1 = (item.DueDate).split('T')[1];
                Show_due_date = due_date + " " + due_date_time1;
            }
            else {
                due_date = due_date.replace(/\s+/g, '-');
                var due_date_time = (item.DueDate).split('T')[1];
                Show_due_date = due_date +" "+ due_date_time;
                due_date = "&#x1f550; " + due_date;
                someday = new Date(item.DueDate);
                if (someday > today) {
                    var hours = Math.round((someday - today) / 3600000);
                    if (hours < 24) {
                        bg_color = 'e6b800';
                    }
                    else {
                        texts = "This card is due later.";
                        bg_color = '8cd98c';
                    }
                } else {
                    texts = "This card has been overdue.";
                    bg_color = 'ff8080';
                }
            }
            if (!(item.Description == "" || item.Description == null)) {
                Task_Description = "";
            }
        }
    }

    var TotalExpandTime = 0;
    if (item.TotalHours != null) {
        var milliseconds = Math.abs(item.TotalHours);
        var hours = Math.floor(milliseconds / 60);
        var hr = hours;
        var minutes = Math.floor(milliseconds - (hours * 60));
        var min = minutes;
        if (hours < 9) {
            hr = '0' + hours;
        }
        if (minutes < 9) {
            min = '0' + hours;
        }
        if (hours > 0 && minutes > 0) {
            TotalExpandTime = hr + ":" + min;
        }
        else if (hours <= 0 && minutes > 0) {
            TotalExpandTime = "00:" + min;
        }
        else if (hours > 0 && minutes <= 0) {
            TotalExpandTime = hr + ":" + " 00";
        }
        else {
            TotalExpandTime = "00:" + "00";
        }
    }
    var Color = item.Color;
    //
    var h = "";
    h += "<div><ul class='lbl-ul'> <li><span id='cardcolorset" + item.TaskID + "' style='background-color:" + Color + ";height:20px;width:6px; display:block; margin-top: -7px;'></li></div>"
    cardtime = minutes;
    if (cardtime != "0") {
        carididArray.push(cardtime + "," + item.TaskID);
    }
    $(abc).append("<li id=" + item.TaskID + " class='ui-state-default movecard' title='" + MoveHours + "'><input type='checkbox' class='tesxer' id=" + item.TaskID + " name='cbcard' value='cbcard'><input type='hidden' id='MoveDate' value='" + item.MoveDate + "'/><a id='cardsids' class='cards' onclick='javascript:ShowModal1(" + item.TaskID + "," + item.BoardID + "," + item.SprintBoardRelationId + ",\"" + item.priority + "\",\"" + item.Description + "\");'><div id='set" + item.TaskID + "' title='" + item.Message + "'>" + h + "</div><div class='cRdNmE' style='margin-left: 18px;'>" + item.Title + "</div></a><div id='holder' class='card_holder" + item.TaskID + "'><div class='card_bottom_sec' id='card_bottom_sec" + item.TaskID + "' onclick='javascript:ShowModal1(" + item.TaskID + "," + item.BoardID + "," + item.SprintBoardRelationId + ",\"" + item.priority + "\",\"" + item.Description + "\");'><p id='comment" + item.TaskID + "' title='Comments' class='badge-text2' ></p><p id='attachment" + item.TaskID + "' title='Attachments'></p><p class='badge-text3' title='This card has description.'>" + Task_Description + "</p><p class='badge-textclock' title='Due Date for this task is:"+" "+"" + Show_due_date + "'></p><div class='showhim' id='showhim" + item.TaskID + "'><img src='Images/icons-cardmen.png' alt='per'/><div class='showme'><ul><li id='mem_task" + item.TaskID + "'></li></ul><div class='clr'></div></div></div><div class='clr'></div></div><div  id='settimecolor" + item.TaskID + "'  class='timex'  onclick='javascript:OpenScreenshotWindow(" + item.TaskID + ")' style='color:#000;'>" + TotalExpandTime + "</div><div class='clr'></div></div>  <div> </div>");
    var avgtime = parseInt($("#txtCardEstimatedTime").val());
    var avgtime1 = parseInt($("#txtCardEstimatedminute").val());
    var cardtime = milliseconds;
    if (isNaN(avgtime)) {
        avgtime = 0;
    }
    if (isNaN(avgtime1)) {
        avgtime1 = 0;
    }
    var minute = (avgtime * 60) + avgtime1;
    var totalminute = (minute * 0.25) + minute;
    if (totalminute > 0) {
        if (totalminute > cardtime) {
            $('#settimecolor' + item.TaskID).css('background-color', 'red');
        }
        else {
            $('#settimecolor' + item.TaskID).css('background-color', 'orange');
        }
    }
    else {
        if (cardtime > 0) {
            $('#settimecolor' + item.TaskID).css('background-color', 'orange');
        }
        else {
            $('#settimecolor' + item.TaskID).css('background-color', '#efefef');
        }
    }
    NoOfComments1(currCartAttachments, currCartComments, item.TaskID, cardCommentID, cardAttachmentID);
    var cardid_des = "#" + item.TaskID;
    var cardid_clock = "#" + item.TaskID;
    $(cardid_clock).find('.badge-textclock').empty();
    $(cardid_clock).find('.badge-textclock').append("<img src='images/clock-icon.png' style='width: 12px;margin: 5px 0px -3px 0px;border-radius: 0px !important;'/>");
    if (item.Description != null) {
        if (item.Description.length > 0) {
            $("#Description_Heading").show();
            $("#description_list").show();

            $(cardid_des).find('.badge-text3').empty();
            $(cardid_des).find('.badge-text3').append("<img src='images/description-icon.png' style='width: 12px;margin: 5px 0px -3px 0px;border-radius: 0px !important;'/>");
            $("#description_list").html("<pre><h6><a href='#' id='descr' style='display: block !important;word-break: normal !important;font-size: 14px; font-family: arial; ' onclick='javascript:showDescription();'>" + item.Description.replace(/m1/g, '"').replace(/m2/g, "'").replace(/m3/g, "\\").replace(/m4/g, "/").replace(/m5/g, "\n") + "</a></h6><pre>");

            $(cardid_des).find('#description').append("<img src='Images/ibooks.png' alt='not found image.' style='height: 10px;float: right;'/>");
        }
        else {

            $("#Description_Heading").hide();
            $("#description_list").show();
            $("#description_list").html("<pre><a href='#' id='descr'  style='display:block;'  onclick='javascript:showDescription();'>Edit the description</a><pre>");
        }
    }
    else {
        $("#Description_Heading").hide();
        $("#description_list").show();
        $("#description_list").html("<pre><a href='#' id='descr'  style='display:block;'  onclick='javascript:showDescription();'>Edit the description</a><pre>");
    }
    GetDescription(item.TaskID, item.Description);
    GetMembersInCard(item.TaskID);
    //BindMembersInCard(item.TaskID, item.Email);
    sortable_add_newList(item.SprintBoardRelationId);
    var cardtime = $('#settimecolor' + item.TaskID).width();
    var rest = 180 - cardtime + "px";
    $('#card_bottom_sec' + item.TaskID).css('width', rest);
}

function SearchByCards() {
    var BoardID = $("#boardID").val();
    var Search_CardName = $('#txtCardSearch').val();
    if (Search_CardName != "") {
        $.ajax({
            url: "/DataTracker.svc/SearchCardOnClick",
            type: "POST",
            data: "{\"BoardId\":\"" + BoardID + "\",\"CardName\":\"" + Search_CardName + "\"}",
            dataType: "JSON",
            contentType: "application/json;charset=utf-8",
            success: function (data) {
                var currSprint = 0;
                var currList = 0;
                var currCart = 0;
                var sphight = 0;
                var usertypeid = $('#usertypeid').val();
                var cuserid = $('#cuserid').val();
                var d = JSON.parse(data.d);
                if (d.length > 0) {
                    //Render Srpints
                    $("#multi").html("");
                    $.each(d, function (index, item) {
                        if (item.SprintBoardRelationId != null) {
                            //Bind Sprint data
                            if (currSprint == 0) {
                                //renderBoardDetail(item);  //This function not runs while searching cards across All Boards.
                            }
                            if (currSprint !== item.SprintBoardRelationId) {
                                currSprint = item.SprintBoardRelationId;
                                sphight = 0;
                                var temp = 0;
                                var TotalSpenttimeBySprint = 0;
                                //Hidden field for Dynamic Sprint Height
                                $("#multi").append("<input class='Spheight_" + item.SprintBoardRelationId + "' type='hidden'/>");
                                renderSprint(item);
                                var date = new Date(item.DueDate);
                                var month = date.getMonth() + 1 < 10 ? '0' + (date.getMonth() + 1) : date.getMonth() + 1;
                                var day = date.getDate() < 10 ? '0' + date.getDate() : date.getDate();
                                $("#boardduedate").val(date.getFullYear() + '-' + month + '-' + day);
                                //Bind Sprint Deudate
                                checkdays(item.SprintDueDate, item.SprintBoardRelationId);
                                var currSprintList = d.filter(function (el) {
                                    return (el.SprintBoardRelationId === item.SprintBoardRelationId);
                                });
                                if (currSprintList.length > 0) {
                                    var spwidth = 0;
                                    setcolorindex = 0;
                                    //Bind Sprint List Data
                                    $.each(currSprintList, function (ListIndex, ListItem) {
                                        
                                        if (ListItem.ProjectID != null) {
                                            if (currList !== ListItem.ProjectID) {
                                                currList = ListItem.ProjectID;
                                                spwidth++;
                                                temp++;

                                                var currSprintCart = d.filter(function (el) {
                                                    return (el.ProjectID === ListItem.ProjectID);
                                                });
                                                //Bind List
                                                renderList(0, 0, 12, 2, ListIndex, ListItem, setcolorindex, currSprintCart.length);
                                                setcolorindex++;
                                                var CC = 1;
                                                if (currSprintCart.length > 0) {
                                                    //Make Dynamic Selectors
                                                    var composer = "composer" + ListItem.ProjectID;
                                                    var dfdfdf = "cardaddid" + ListItem.ProjectID;
                                                    var abc = "#cardlist" + ListItem.ProjectID + "_" + item.SprintBoardRelationId;
                                                    var composerSprint2 = "composerSprint2" + ListItem.ProjectID;
                                                    //Bind List Card data
                                                    $.each(currSprintCart, function (CartIndex, CartItem) {
                                                        if (CartItem.TaskID != null) {
                                                            if (currCart !== CartItem.TaskID) {
                                                                var CardsCount = CC++;
                                                                $('#listCount' + CartItem.ProjectID).html("");
                                                                $('#listCount' + CartItem.ProjectID).html("<div class='listCount' id='listCount" + CartItem.ProjectID + "' title='Total Cards'>" + CardsCount + "</div>");
                                                                currCart = CartItem.TaskID;
                                                                //Bind card
                                                                //renderCard(abc, CartIndex, CartItem, currCartComments, currCartAttachments, CartItem.CommentID, CartItem.FileID);

                                                                if (CartItem.Hide_showPublicBoard > 0) {
                                                                    if (usertypeid == "12" || CartItem.OwnerId == cuserid || CartItem.BoardTeamMembers.indexOf(cuserid) != -1) {
                                                                        renderCard(abc, CartIndex, CartItem, CartItem.CommentCount, CartItem.AttachmentCount, CartItem.CommentID, CartItem.FileID);
                                                                    }
                                                                    else if (CartItem.TaskCreatedBy == cuserid) {
                                                                        renderCard(abc, CartIndex, CartItem, CartItem.CommentCount, CartItem.AttachmentCount, CartItem.CommentID, CartItem.FileID);
                                                                    }
                                                                }
                                                                else {
                                                                    renderCard(abc, CartIndex, CartItem, CartItem.CommentCount, CartItem.AttachmentCount, CartItem.CommentID, CartItem.FileID);
                                                                }
                                                                TotalSpenttimeBySprint = TotalSpenttimeBySprint + CartItem.TotalHours;
                                                            }
                                                            //BindMembersInCard(CartItem.TaskID, CartItem.Email);
                                                        }
                                                        else {
                                                            $('#listCount' + CartItem.ProjectID).html("");
                                                            $('#listCount' + CartItem.ProjectID).html("<div class='listCount' id='listCount" + CartItem.ProjectID + "' title='Total Cards'>0</div>");
                                                        }
                                                    });
                                                    //Bind Add New Card Option

                                                    if (ListIndex > 0) {
                                                        $("#cardaddid" + ListItem.ProjectID).css("display", "none");
                                                    }
                                                    else {
                                                        renderAddNewCard(composer, dfdfdf, abc, ListItem.ProjectID, ListItem.ProjectType, ListItem.BoardID, item.SprintBoardRelationId);
                                                    }
                                                    //Make Sprint Dynamic Height
                                                    var SprintLength = $('#' + ListItem.ProjectID).height();
                                                    if (SprintLength >= sphight) {
                                                        sphight = SprintLength;
                                                        SprintLength = 50 + sphight + 'px';
                                                        $(".Spheight_" + item.SprintBoardRelationId).val(SprintLength);
                                                    }
                                                }
                                            }
                                        }
                                    });
                                }
                                //Bind Add New List Option
                                renderAddnewList(spwidth, item.SprintBoardRelationId);
                                // Bind swaping details
                                sortable_add_newList(item.SprintBoardRelationId);
                                //Get Sprint Estimated time
                                GetTotalTimeEstimate(TotalSpenttimeBySprint, item.EstimatedHours, item.SprintBoardRelationId);
                            }
                        }
                    });
                    GetActivityLogDateWise();
                    var firstsprintid = $("#firstsprint").val();
                    ShowSprint(firstsprintid);
                }
                else {
                    alert("Search Card does not Exist.");
                }
            },
            error: function () {

            }
        });
    }else
    {
        $("#multi").html("");
        GetBoardDetails(BoardID);
    }
}

function GetBoardList(BoardID, SprintId) {
    $.ajax({
        type: "POST",
        url: "/DataTracker.svc/GetListDetails",
        data: JSON.stringify({ BoardID: BoardID, SprintId: SprintId }),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: false,
        success: function (data) {
            var d = JSON.parse(data.d);
            var CardsNo = 0;
            var IsSequential = 0;
            var cuserid = $('#cuserid').val();
            var usertypeid = $('#usertypeid').val();
            var ownerid = $('#ownerID').val();
            var temp1 = 12;
            var temp2 = 12;
            var i = 2;
            var spwidth = 0;
            setcolorindex = 0;

            if (d.length > 0) {
                $.each(d, function (index, item) {
                    spwidth = d.length;
                    if (item.IsSequential == 1) {
                        IsSequential = "Type: Sequential";
                    }
                    else {
                        IsSequential = "Type: Normal";
                    }
                    if (item.CardsNo != 0) {
                        CardsNo = "Cards Limit: " + item.CardsNo;
                    }
                    else { CardsNo = "Cards Limit: Max"; }

                    var projectname = item.ProjectType;
                    if (projectname.length >= 20) { projectname = projectname.substr(0, 20) + '.....'; }
                    else { projectname = projectname; }

                    if (cuserid == ownerid || usertypeid == 12 || usertypeid == 14) {
                        $('#ArchiveBoard').css('display', 'block');
                    }
                    if (index >= temp1) {
                        setcolorindex = 0;
                        temp1 = temp2 * i;
                        i++;
                    }
                    var colorcollection = ["#ee9f31", "#ef662a", "#ee4526", "#d1101f", "#a20080", "#780992", "#8742a6", "#1061e0", "#129eb1", "#008e55", "#84ab3d", "#dcc609"];
                    var randomcolor = colorcollection[setcolorindex];
                    setcolorindex++;

                    $("#multi").append("<span id='" + item.ProjectID + "' class='layer tile Sprint_" + BoardSprintId + "' ondrop='javascript:OnClickProject(" + item.ProjectID + ");' data-force='30'><div  class='project_header' style='background:" + randomcolor + "'><div class='tile__name' title='Click for edit.' onclick='editlistname(" + item.ProjectID + ");' >" + item.ProjectType + "<img src='/Images/pencil5.png' title='Click for edit.' /></div><div id='list_action'><a href='#' class='linkin' onclick='openarchive(" + item.ProjectID + ")' id=linkin" + item.ProjectID + ">&#8230;</a><div id='pops" + item.ProjectID + "' class='webui-popoverab'><div class='js-pop-over-header' style='position:relative;'><div class='pop-over-header-title1' >List Actions</div><a onclick='closearchive()'   id='archiveclose' href='#' class='pop-close'>×</a></div><div><div class='pop-over-content js-pop-over-content u-fancy-scrollbar js-tab-parent'><ul class='pop-over-list'> <li> <a class='js-close-list'  href='#' onclick='exportsExcel(" + item.ProjectID + "," + BoardID + ","+BoardSprintId+")'>Export</a>  </li> </ul><ul class='pop-over-list'><li><a class='js-close-list' href='#' onclick='ArchiveList(" + item.ProjectID + "," + BoardID + "," + BoardSprintId + ")'>Archive This List</a></li></ul><ul class='pop-over-list'><li><a onclick='showArchivepage(" + item.ProjectID + "," + BoardID + "," + BoardSprintId + ");' href='#' >Show Archived List..</a></li></ul><hr><ul class='pop-over-list'><li>	<a onclick='openarchivelist(" + item.ProjectID + ")' class='js-archive-cards' href='#'>Archive All Cards in This List…</a></li><li>	<a onclick='openundoarchivelist(" + item.ProjectID + "," + BoardID + ")' class='js-move-cards' href='#'>Show Archived Card…</a></li><li id='MemberlistId'>	<a onclick='AddMemberToList(" + item.ProjectID + "," + BoardID + ")' class='js-move-cards' href='#'>Define list owner</a></li></ul><hr><span><input type='text' class='Cardhoursbylist' id='List_" + item.ProjectID + "'/><span>Hour<span><input type='button' class='btnsetcardhoursbylist' value='Set Card Hours' onclick='UpdateCardDueHoursByList(" + item.ProjectID + ")'/></span></div></div></div></div><div class='clr'></div>    <div id='webui" + item.ProjectID + "' class='pop-over is-shown webuiabc'><div><div class='pop-over-header2 js-pop-over-header'><a onclick='openarchivelistleftarrow(" + item.ProjectID + ")' class='leftarrow2' href='#'>←</a><h3>Archive All Cards in this List?</h3><a onclick='openarchivelistpopclose()'class='pop-close2' href='#'>×</a><div class='clr'></div></div><div><div class='pop-over-content js-pop-over-content u-fancy-scrollbar js-tab-parent' style='max-height: 529px;'><div class='para-part'><p>This will remove all the cards in this list from the board. To view archived cards and bring them back to the board, click  “Show Archived Card…”</p><a href='#' class='js-confirm negate full archive_button' onclick='ArchiveAllCardfromList(" + item.ProjectID + ")' >Archive All</a></div></div></div></div></div> <a onclick='boxdiv(" + item.ProjectID + ")' id='listproprety" + item.ProjectID + "' herf='#'><small class='IsSequential' >" + IsSequential + "</small><small>,&nbsp;</small><small class='Cardlimit'>" + CardsNo + "</small></a><div id='isseq" + item.ProjectID + "' class='box-ab'><div class='box-1a'><h3 id='pname' title='" + item.ProjectType + "'>" + projectname + "<input type='hidden' id='projectId' value=" + item.ProjectID + " /></h3><a onclick='divboxclose(" + item.ProjectID + ")'class='box-close' href='#'>&#10006</a><div class='clr'></div></div><div class='banner'></div><div class='box-1b'><form class='card-box'><label> Max Card Limit:</label><input type='text' id='max-card' onkeypress='return IsNumerictextbox(event);' maxlength='3'>&nbsp;<div class='clr'></div><label>sequential</label><input type='checkbox' name='sequential' value='sequential' id='sequentialcheckbox' ><div class='clr'></div> <div id='rmsg' style='color: #69aa6f;display:none'>Update Successfully</div> <br><a onclick='ChangelimitofCard(" + item.ProjectID + "," + BoardSprintId + ")' class='edit-box' href='#'>Submit</a><div class='clr'></div> </form></div></div> </div> <div class='tile__list ui-sortable' spid='sprintlist" + BoardSprintId + "' id='cardlist" + item.ProjectID + "_" + BoardSprintId + "'></div></span>");
                });
                var swidth = $('.cd-main-header').width();
                spwidth = spwidth * 350;
                if (spwidth > swidth) {
                    var hdwd = $(".Spwidth").val();
                    var wd = 50 + spwidth;
                    $(".Spwidth").val(wd);
                    if (hdwd > wd) {
                        $(".Spwidth").val(hdwd);
                        spwidth = hdwd + 'px';
                        $('#multi').css('width', spwidth);
                    }
                    else {
                        spwidth = wd + 'px';
                        $('#multi').css('width', spwidth);
                    }

                }
                else {
                    swidth = spwidth;
                }
            }
        },
        error: function () {
        }
    });
}


function CheckAuthorizeUser(id) {
    var res;
    $.ajax({
        type: "GET",
        url: "/DataTracker.svc/CheckAuthorizeUser?boardid=" + id + "",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: false,
        success: function (data) {
            res = data.d;
        },
        error: function (data) {
        }
    });
    return res;
}

function myBoardurl(id) {
    window.location.href = "board.aspx?" + id;
}

function RecentBoardurl(id) {
    window.location.href = "board.aspx?" + id;
}

function divboxclose(ProjectID) {
    var id = '#isseq' + ProjectID;
    $(id).css('display', 'none');
    $('#isseq' + ProjectID).find("#rmsg").css("display", "none")
}

function AddListBySaveButton(SprintId) {
    var CountSprintId = 0;
    $.ajax({
        type: "POST",
        url: "/DataTracker.svc/GetTotalListOnSprint",
        data: "{\"sprintId\": \"" + SprintId + "\"}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            var res = JSON.parse(data.d);
            $.each(res, function (index, item) {
                CountSprintId = item.CountSPID;
            });
        }
    });
    $("#addlist").removeClass('create_button');
    $("#cancellist").removeClass('crossl');
    $("#addlist").hide();
    $("#cancellist").hide();
    $("#listloadstage").removeClass('displayloadstage');

    var lastprojecttypeid = 0;
    var sprint_id = SprintId;
    var findspan = $("#sp_" + SprintId).children('span').first();
    if (findspan.length > 0) {
        var span = findspan[0].tagName.toLowerCase();
        if (span == 'span') {
            lastprojecttypeid = $("#sp_" + SprintId).children('span').last().attr('id');
        }
    }
    var textboxid = "#txtlistbox" + SprintId;
    var randomcolor = "";
    var list_name = $.trim($(textboxid).val()).replace(/'/g, "").replace(/\n/g, "").replace(/"/g, "").replace(/\\/g, "").replace(/\//g, "");
    var IsSeq = 0;
    var IsSequential = 'Type: Normal';
    var checkIsSeqSp = "#IsSeqentialCheckBox" + SprintId;
    if ($(checkIsSeqSp).is(':checked')) { IsSeq = 1; IsSequential = 'Type: Sequential'; } else { IsSeq = 0 };       //Ameeq Changes for Swimlanes
    var NoOfCards = 0;
    var NoOfCards2;
    var nocwithSp = "#NoOfCards" + SprintId;    //Ameeq Changes for Swimlanes
    var noc = parseInt($(nocwithSp).val());
    if (isNaN(noc)) {
        noc = 0;
    }

    var NoOfCards = NoOfCards + noc;
    if (NoOfCards == 0) { NoOfCards2 = 'Cards limit :Max'; } else { NoOfCards2 = 'Cards limit :' + NoOfCards; };
    if ($.trim(list_name).length > 0) {
        $.ajax({
            type: "POST",
            url: "/DataTracker.svc/AddNewListWithSprintId",
            data: "{\"listname\": \"" + list_name + "\",\"BoardId\": \"" + BoardID + "\",\"Sprint_Id\": \"" + sprint_id + "\",\"IsSeq\": \"" + IsSeq + "\",\"NoOfCards\": \"" + NoOfCards + "\", \"lastprojecttypeid\": \"" + lastprojecttypeid + "\"}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                
                var SetSprintId = "#" + sprint_id;
                var projectname = list_name;
                if (projectname.length >= 20) { projectname = projectname.substr(0, 20) + '.....'; }
                else { projectname = projectname; }
                if (data.d != 0) {
                    var spwidth = $('#multi').width();
                    spwidth = spwidth + 250 + 'px';
                    $('#multi').css('width', spwidth);
                    var composer = "composer" + data.d;
                    var dfdfdf = "cardaddid" + data.d;
                    var colorcollection = ["#ee9f31", "#ef662a", "#ee4526", "#d1101f", "#a20080", "#780992", "#8742a6", "#0d62e7", "#129eb1", "#008e55", "#84ab3d", "#dcc609"];
                    if (setcolorindex >= 12) {
                        setcolorindex = 0;
                    }
                    randomcolor = colorcollection[setcolorindex];
                    setcolorindex++;
                    
                    if ($("#sp_" + SprintId + " .layer").length > 0) {
                        if (CountSprintId == 0) {
                            $(SetSprintId).last().before("<span id='" + data.d + "' class='layer tile Sprint_" + SprintId + "' ondrop='javascript:OnClickProject(" + data.d + ");' data-force='30'><div class='project_header' style='background:" + randomcolor + "'><div class='tile__name' title='Click for edit.' onclick='editlistname(" + data.d + ");'>" + list_name + "<img src='/Images/pencil5.png' title='Click for edit.' /></div><div id='list_action'><a href='#' class='linkin' onclick='openarchive(" + data.d + ")' id=linkin" + data.d + ">&#8230;</a><div id='pops" + data.d + "' class='webui-popoverab'><div class='js-pop-over-header' style='position:relative;'><div class='pop-over-header-title1' >List Actions</div><a onclick='closearchive()'  id='archiveclose' class='pop-close'>×</a></div><div><div class='pop-over-content js-pop-over-content u-fancy-scrollbar js-tab-parent'><ul class='pop-over-list'> <li> <a class='js-close-list' onclick='exportsExcel(" + data.d + "," + BoardID + "," + SprintId + ")'>Export</a>  </li> </ul><ul class='pop-over-list'><li><a class='js-close-list'  onclick='ArchiveList(" + data.d + "," + BoardID + "," + sprint_id + ")'>Archive This List</a></li></ul><ul class='pop-over-list'><li><a onclick='showArchivepage(" + data.d + "," + BoardID + "," + sprint_id + ");'>Show Archived List..</a></li></ul><hr><ul class='pop-over-list'><li>	<a onclick='openarchivelist(" + data.d + ")' class='js-archive-cards'>Archive All Cards in This List…</a></li><li>	<a onclick='openundoarchivelist(" + data.d + "," + BoardID + ")' class='js-move-cards'>Show Archived Card…</a></li><li>	<a onclick='AddMemberToList(" + data.d + "," + BoardID + ")' class='js-move-cards'>Define list owner</a></li></ul><hr><span><input type='text' class='Cardhoursbylist' id='List_" + data.d + "'/><span>Hour<span><input type='button' class='btnsetcardhoursbylist' value='Set Card Hours' onclick='UpdateCardDueHoursByList(" + data.d + ")'/></span></div></div></div></div><div class='clr'>    <div id='webui" + data.d + "' class='pop-over is-shown webuiabc'><div><div class='pop-over-header2 js-pop-over-header'><a onclick='openarchivelistleftarrow(" + data.d + ")' class='leftarrow2'>←</a><h3>Archive All Cards in this List?</h3><a onclick='openarchivelistpopclose()'class='pop-close2' href='#'>×</a><div class='clr'></div></div><div><div class='pop-over-content js-pop-over-content u-fancy-scrollbar js-tab-parent' style='max-height: 529px;'><div class='para-part'><p>This will remove all the cards in this list from the board. To view archived cards and bring them back to the board, click  “Show Archived Card…”</p><a href='#' class='js-confirm negate full archive_button' onclick='ArchiveAllCardfromList(" + data.d + ")' >Archive All</a></div></div></div></div></div><a href='#' onclick='boxdiv(" + data.d + ")' id=listproprety" + data.d + " ><small class='IsSequential' >" + IsSequential + "</small><small>,&nbsp;</small><small class='Cardlimit'>" + NoOfCards2 + "</small><small>&nbsp;</small><small><i class='fa fa-pencil' aria-hidden='true' title='Click for edit'></i></small></a><div class='listCount' id='listCount" + data.d + "' title='Total Cards'>0</div>   </div></div><div id='isseq" + data.d + "'+ class='box-ab'><div class='box-1a'><h3 title='" + list_name + "'>" + projectname + "<input type='hidden' id='projectId' value=" + data.d + " /></h3><a onclick='divboxclose(" + data.d + ")'class='box-close' href='#'>&#10006</a><div class='clr'></div></div><div class='banner'></div><div class='box-1b'><form class='card-box'><label>Max Card Limit:</label><input type='text' id='max-card'onkeypress='return IsNumerictextbox(event);' maxlength='3'><div class='clr'></div><label >sequential</label><input type='checkbox' name='sequential' value='sequential' id='sequentialcheckbox' ><div class='clr'></div> <div id='rmsg' style='color: #69aa6f;display:none'>Update Successfully</div> <br><a onclick='ChangelimitofCard(" + data.d + "," + sprint_id + ")' class='edit-box' href='#'>Submit</a><div class='clr'></div> </form></div></div> </div> <div class='tile__list ui-sortable' spid='sprintlist" + sprint_id + "' id='cardlist" + data.d + "_" + sprint_id + "'></div><div id=" + composer + " class='card-composer hide'><div style='color:red; font-size:80%;clear:both;' id='span" + data.d + "'></div><textarea maxlength='200' id='textaera" + data.d + "' onkeyup='javascript:AddCardToList(this, event," + data.d + "," + BoardID + ",\"" + list_name + "\")' class='js-search-boards' placeholder=''></textarea><a id='addNewcard1' class='fl create_button' onclick='javascript:AddCardToListOnAddButtonClick(" + data.d + "," + BoardID + "," + sprint_id + ",\"" + list_name + "\")'  href='#'>Add</a><div id='cancelcard' onclick='mycross(" + data.d + ");' class='cross'>Cancel </div><p id='loadstage' class='displayloadstage'>Adding...</p><a href='#' class='list_action show-pop btn btn-default bottom-right' data-placement='auto-top' >...</a>   </div><a id=" + dfdfdf + "  onclick='myFunction(" + data.d + ");'  class='add_card'>Add a card...</a></span>");
                        } else {
                            $(SetSprintId).last().before("<span id='" + data.d + "' class='layer tile Sprint_" + SprintId + "' ondrop='javascript:OnClickProject(" + data.d + ");' data-force='30'><div class='project_header' style='background:" + randomcolor + "'><div class='tile__name' title='Click for edit.' onclick='editlistname(" + data.d + ");'>" + list_name + "<img src='/Images/pencil5.png' title='Click for edit.' /></div><div id='list_action'><a href='#' class='linkin' onclick='openarchive(" + data.d + ")' id=linkin" + data.d + ">&#8230;</a><div id='pops" + data.d + "' class='webui-popoverab'><div class='js-pop-over-header' style='position:relative;'><div class='pop-over-header-title1' >List Actions</div><a onclick='closearchive()'  id='archiveclose' class='pop-close'>×</a></div><div><div class='pop-over-content js-pop-over-content u-fancy-scrollbar js-tab-parent'><ul class='pop-over-list'> <li> <a class='js-close-list' onclick='exportsExcel(" + data.d + "," + BoardID + "," + SprintId + ")'>Export</a>  </li> </ul><ul class='pop-over-list'><li><a class='js-close-list'  onclick='ArchiveList(" + data.d + "," + BoardID + "," + sprint_id + ")'>Archive This List</a></li></ul><ul class='pop-over-list'><li><a onclick='showArchivepage(" + data.d + "," + BoardID + "," + sprint_id + ");'>Show Archived List..</a></li></ul><hr><ul class='pop-over-list'><li>	<a onclick='openarchivelist(" + data.d + ")' class='js-archive-cards'>Archive All Cards in This List…</a></li><li>	<a onclick='openundoarchivelist(" + data.d + "," + BoardID + ")' class='js-move-cards'>Show Archived Card…</a></li><li>	<a class='enablecardclick" + data.d + "' onclick='enablenewcard(" + data.d + "," + BoardID + "," + sprint_id + ",\"" + projectname + "\")' class='js-archive-cards'>Enable Add a Card...</a></li><li>	<a class='MemberlistId' onclick='AddMemberToList(" + data.d + "," + BoardID + ")' class='js-move-cards'>Define list owner</a></li></ul><hr><span><input type='text' class='Cardhoursbylist' id='List_" + data.d + "'/><span>Hour<span><input type='button' class='btnsetcardhoursbylist' value='Set Card Hours' onclick='UpdateCardDueHoursByList(" + data.d + ")'/></span></div></div></div></div><div class='clr'>    <div id='webui" + data.d + "' class='pop-over is-shown webuiabc'><div><div class='pop-over-header2 js-pop-over-header'><a onclick='openarchivelistleftarrow(" + data.d + ")' class='leftarrow2' href='#'>←</a><h3>Archive All Cards in this List?</h3><a onclick='openarchivelistpopclose()'class='pop-close2' href='#'>×</a><div class='clr'></div></div><div><div class='pop-over-content js-pop-over-content u-fancy-scrollbar js-tab-parent' style='max-height: 529px;'><div class='para-part'><p>This will remove all the cards in this list from the board. To view archived cards and bring them back to the board, click  “Show Archived Card…”</p><a href='#' class='js-confirm negate full archive_button' onclick='ArchiveAllCardfromList(" + data.d + ")' >Archive All</a></div></div></div></div></div><a href='#' onclick='boxdiv(" + data.d + ")' id=listproprety" + data.d + " ><small class='IsSequential' >" + IsSequential + "</small><small>,&nbsp;</small><small class='Cardlimit'>" + NoOfCards2 + "</small><small>&nbsp;</small><small><i class='fa fa-pencil' aria-hidden='true' title='Click for edit'></i></small></a><div class='listCount' id='listCount" + data.d + "' title='Total Cards'>0</div>   </div></div><div id='isseq" + data.d + "'+ class='box-ab'><div class='box-1a'><h3 title='" + list_name + "'>" + projectname + "<input type='hidden' id='projectId' value=" + data.d + " /></h3><a onclick='divboxclose(" + data.d + ")'class='box-close' href='#'>&#10006</a><div class='clr'></div></div><div class='banner'></div><div class='box-1b'><form class='card-box'><label>Max Card Limit:</label><input type='text' id='max-card'onkeypress='return IsNumerictextbox(event);' maxlength='3'><div class='clr'></div><label >sequential</label><input type='checkbox' name='sequential' value='sequential' id='sequentialcheckbox' ><div class='clr'></div> <div id='rmsg' style='color: #69aa6f;display:none'>Update Successfully</div> <br><a onclick='ChangelimitofCard(" + data.d + "," + sprint_id + ")' class='edit-box' href='#'>Submit</a><div class='clr'></div> </form></div></div> </div> <div class='tile__list ui-sortable' spid='sprintlist" + sprint_id + "' id='cardlist" + data.d + "_" + sprint_id + "'></div><div id=" + composer + " class='card-composer hide'><div style='color:red; font-size:80%;clear:both;' id='span" + data.d + "'></div><textarea maxlength='200' id='textaera" + data.d + "' onkeyup='javascript:AddCardToList(this, event," + data.d + "," + BoardID + ",\"" + list_name + "\")' class='js-search-boards' placeholder=''></textarea><a id='addNewcard1' class='fl create_button' onclick='javascript:AddCardToListOnAddButtonClick(" + data.d + "," + BoardID + "," + sprint_id + ",\"" + list_name + "\")'  href='#'>Add</a><div id='cancelcard' onclick='mycross(" + data.d + ");' class='cross'>Cancel </div><p id='loadstage' class='displayloadstage'>Adding...</p><a href='#' class='list_action show-pop btn btn-default bottom-right' data-placement='auto-top' >...</a>   </div></span>");
                        }
                        sortable_add_newList();
                        $(textboxid).val("").focus();     //Ameeq Changes
                        $(nocwithSp).val("").focus();
                        $(checkIsSeqSp).each(function () { this.checked = false; });
                        UpdateActivityListItem(list_name, 1);
                    }
                    else {
                        if (CountSprintId == 0) {
                            $(SetSprintId).last().before("<span id='" + data.d + "' class='layer tile Sprint_" + SprintId + "' ondrop='javascript:OnClickProject(" + data.d + ");' data-force='30' style='display:block'><div class='project_header' style='background:" + randomcolor + "'><div class='tile__name' title='Click for edit.' onclick='editlistname(" + data.d + ");'>" + list_name + "<img src='/Images/pencil5.png' title='Click for edit.' /></div><div id='list_action'><a href='#' class='linkin' onclick='openarchive(" + data.d + ")' id=linkin" + data.d + ">&#8230;</a><div id='pops" + data.d + "' class='webui-popoverab'><div class='js-pop-over-header' style='position:relative;'><div class='pop-over-header-title1' >List Actions</div><a onclick='closearchive()'  id='archiveclose' class='pop-close'>×</a></div><div><div class='pop-over-content js-pop-over-content u-fancy-scrollbar js-tab-parent'><ul class='pop-over-list'> <li> <a class='js-close-list'  onclick='exportsExcel(" + data.d + "," + BoardID + "," + SprintId + ")'>Export</a>  </li> </ul><ul class='pop-over-list'><li><a class='js-close-list'  onclick='ArchiveList(" + data.d + "," + BoardID + "," + sprint_id + ")'>Archive This List</a></li></ul><ul class='pop-over-list'><li><a onclick='showArchivepage(" + data.d + "," + BoardID + "," + sprint_id + ");'>Show Archived List..</a></li></ul><hr><ul class='pop-over-list'><li>	<a onclick='openarchivelist(" + data.d + ")' class='js-archive-cards'>Archive All Cards in This List…</a></li><li>	<a onclick='openundoarchivelist(" + data.d + "," + BoardID + ")' class='js-move-cards'>Show Archived Card…</a></li><li>	<a onclick='AddMemberToList(" + data.d + "," + BoardID + ")' class='js-move-cards'>Define list owner</a></li></ul><hr><span><input type='text' class='Cardhoursbylist' id='List_" + data.d + "'/><span>Hour<span><input type='button' class='btnsetcardhoursbylist' value='Set Card Hours' onclick='UpdateCardDueHoursByList(" + data.d + ")'/></span></div></div></div></div><div class='clr'>    <div id='webui" + data.d + "' class='pop-over is-shown webuiabc'><div><div class='pop-over-header2 js-pop-over-header'><a onclick='openarchivelistleftarrow(" + data.d + ")' class='leftarrow2' href='#'>←</a><h3>Archive All Cards in this List?</h3><a onclick='openarchivelistpopclose()'class='pop-close2' href='#'>×</a><div class='clr'></div></div><div><div class='pop-over-content js-pop-over-content u-fancy-scrollbar js-tab-parent' style='max-height: 529px;'><div class='para-part'><p>This will remove all the cards in this list from the board. To view archived cards and bring them back to the board, click  “Show Archived Card…”</p><a href='#' class='js-confirm negate full archive_button' onclick='ArchiveAllCardfromList(" + data.d + ")' >Archive All</a></div></div></div></div></div><a href='#' onclick='boxdiv(" + data.d + ")' id=listproprety" + data.d + " ><small class='IsSequential' >" + IsSequential + "</small><small>,&nbsp;</small><small class='Cardlimit'>" + NoOfCards2 + "</small><small>&nbsp;</small><small><i class='fa fa-pencil' aria-hidden='true' title='Click for edit'></i></small></a><div class='listCount' id='listCount" + data.d + "' title='Total Cards'>0</div>   </div></div><div id='isseq" + data.d + "'+ class='box-ab'><div class='box-1a'><h3 title='" + list_name + "'>" + projectname + "<input type='hidden' id='projectId' value=" + data.d + " /></h3><a onclick='divboxclose(" + data.d + ")'class='box-close' href='#'>&#10006</a><div class='clr'></div></div><div class='banner'></div><div class='box-1b'><form class='card-box'><label>Max Card Limit:</label><input type='text' id='max-card'onkeypress='return IsNumerictextbox(event);' maxlength='3'><div class='clr'></div><label >sequential</label><input type='checkbox' name='sequential' value='sequential' id='sequentialcheckbox' ><div class='clr'></div> <div id='rmsg' style='color: #69aa6f;display:none'>Update Successfully</div> <br><a onclick='ChangelimitofCard(" + data.d + "," + sprint_id + ")' class='edit-box' href='#'>Submit</a><div class='clr'></div> </form></div></div> </div> <div class='tile__list ui-sortable' spid='sprintlist" + sprint_id + "' id='cardlist" + data.d + "_" + sprint_id + "'></div><div id=" + composer + " class='card-composer hide'><div style='color:red; font-size:80%;clear:both;' id='span" + data.d + "'></div><textarea maxlength='200' id='textaera" + data.d + "' onkeyup='javascript:AddCardToList(this, event," + data.d + "," + BoardID + ",\"" + list_name + "\")' class='js-search-boards' placeholder=''></textarea><a id='addNewcard1' class='fl create_button' onclick='javascript:AddCardToListOnAddButtonClick(" + data.d + "," + BoardID + "," + sprint_id + ",\"" + list_name + "\")'  href='#'>Add</a><div id='cancelcard' onclick='mycross(" + data.d + ");' class='cross'>Cancel </div><p id='loadstage' class='displayloadstage'>Adding...</p><a href='#' class='list_action show-pop btn btn-default bottom-right' data-placement='auto-top' >...</a>   </div><a id=" + dfdfdf + "  onclick='myFunction(" + data.d + ");'  class='add_card'>Add a card...</a></span>");
                        } else {
                            $(SetSprintId).last().before("<span id='" + data.d + "' class='layer tile Sprint_" + SprintId + "' ondrop='javascript:OnClickProject(" + data.d + ");' data-force='30' style='display:block'><div class='project_header' style='background:" + randomcolor + "'><div class='tile__name' title='Click for edit.' onclick='editlistname(" + data.d + ");'>" + list_name + "<img src='/Images/pencil5.png' title='Click for edit.' /></div><div id='list_action'><a href='#' class='linkin' onclick='openarchive(" + data.d + ")' id=linkin" + data.d + ">&#8230;</a><div id='pops" + data.d + "' class='webui-popoverab'><div class='js-pop-over-header' style='position:relative;'><div class='pop-over-header-title1' >List Actions</div><a onclick='closearchive()'  id='archiveclose' class='pop-close'>×</a></div><div><div class='pop-over-content js-pop-over-content u-fancy-scrollbar js-tab-parent'><ul class='pop-over-list'> <li> <a class='js-close-list'  onclick='exportsExcel(" + data.d + "," + BoardID + "," + SprintId + ")'>Export</a>  </li> </ul><ul class='pop-over-list'><li><a class='js-close-list'  onclick='ArchiveList(" + data.d + "," + BoardID + "," + sprint_id + ")'>Archive This List</a></li></ul><ul class='pop-over-list'><li><a onclick='showArchivepage(" + data.d + "," + BoardID + "," + sprint_id + ");'>Show Archived List..</a></li></ul><hr><ul class='pop-over-list'><li>	<a onclick='openarchivelist(" + data.d + ")' class='js-archive-cards'>Archive All Cards in This List…</a></li><li>	<a onclick='openundoarchivelist(" + data.d + "," + BoardID + ")' class='js-move-cards'>Show Archived Card…</a></li><li>	<a class='enablecardclick" + data.d + "' onclick='enablenewcard(" + data.d + "," + BoardID + "," + sprint_id + ",\"" + projectname + "\")' class='js-archive-cards'>Enable Add a Card...</a></li><li>	<a class='MemberlistId' onclick='AddMemberToList(" + data.d + "," + BoardID + ")' class='js-move-cards'>Define list owner</a></li></ul><hr><span><input type='text' class='Cardhoursbylist' id='List_" + data.d + "'/><span>Hour<span><input type='button' class='btnsetcardhoursbylist' value='Set Card Hours' onclick='UpdateCardDueHoursByList(" + data.d + ")'/></span></div></div></div></div><div class='clr'>    <div id='webui" + data.d + "' class='pop-over is-shown webuiabc'><div><div class='pop-over-header2 js-pop-over-header'><a onclick='openarchivelistleftarrow(" + data.d + ")' class='leftarrow2' href='#'>←</a><h3>Archive All Cards in this List?</h3><a onclick='openarchivelistpopclose()'class='pop-close2' href='#'>×</a><div class='clr'></div></div><div><div class='pop-over-content js-pop-over-content u-fancy-scrollbar js-tab-parent' style='max-height: 529px;'><div class='para-part'><p>This will remove all the cards in this list from the board. To view archived cards and bring them back to the board, click  “Show Archived Card…”</p><a href='#' class='js-confirm negate full archive_button' onclick='ArchiveAllCardfromList(" + data.d + ")' >Archive All</a></div></div></div></div></div><a href='#' onclick='boxdiv(" + data.d + ")' id=listproprety" + data.d + " ><small class='IsSequential' >" + IsSequential + "</small><small>,&nbsp;</small><small class='Cardlimit'>" + NoOfCards2 + "</small><small>&nbsp;</small><small><i class='fa fa-pencil' aria-hidden='true' title='Click for edit'></i></small></a><div class='listCount' id='listCount" + data.d + "' title='Total Cards'>0</div>   </div></div><div id='isseq" + data.d + "'+ class='box-ab'><div class='box-1a'><h3 title='" + list_name + "'>" + projectname + "<input type='hidden' id='projectId' value=" + data.d + " /></h3><a onclick='divboxclose(" + data.d + ")'class='box-close' href='#'>&#10006</a><div class='clr'></div></div><div class='banner'></div><div class='box-1b'><form class='card-box'><label>Max Card Limit:</label><input type='text' id='max-card'onkeypress='return IsNumerictextbox(event);' maxlength='3'><div class='clr'></div><label >sequential</label><input type='checkbox' name='sequential' value='sequential' id='sequentialcheckbox' ><div class='clr'></div> <div id='rmsg' style='color: #69aa6f;display:none'>Update Successfully</div> <br><a onclick='ChangelimitofCard(" + data.d + "," + sprint_id + ")' class='edit-box' href='#'>Submit</a><div class='clr'></div> </form></div></div> </div> <div class='tile__list ui-sortable' spid='sprintlist" + sprint_id + "' id='cardlist" + data.d + "_" + sprint_id + "'></div><div id=" + composer + " class='card-composer hide'><div style='color:red; font-size:80%;clear:both;' id='span" + data.d + "'></div><textarea maxlength='200' id='textaera" + data.d + "' onkeyup='javascript:AddCardToList(this, event," + data.d + "," + BoardID + ",\"" + list_name + "\")' class='js-search-boards' placeholder=''></textarea><a id='addNewcard1' class='fl create_button' onclick='javascript:AddCardToListOnAddButtonClick(" + data.d + "," + BoardID + "," + sprint_id + ",\"" + list_name + "\")'  href='#'>Add</a><div id='cancelcard' onclick='mycross(" + data.d + ");' class='cross'>Cancel </div><p id='loadstage' class='displayloadstage'>Adding...</p><a href='#' class='list_action show-pop btn btn-default bottom-right' data-placement='auto-top' >...</a>   </div></span>");
                        }
                        sortable_add_newList();
                        $(textboxid).val("").focus();     //Ameeq Changes
                        $(nocwithSp).val("").focus();
                        $(checkIsSeqSp).each(function () { this.checked = false; });
                        UpdateActivityListItem(list_name, 1);
                    }
                    CalculateBoardCompleteWithSprint(BoardID, sprint_id);
                    SetProgressBarPer(sprint_id);
                    second();
                }
                else {
                    alert("Project title already exists");
                }
            },
            error: function (error) {
                alert("Some error in adding list!" + error.Message);
            }
        });
        $('#IsSeqentialCheckBox').attr('checked', false);
        $("#NoOfCards").val("");
    }
    else {
        alert("Please enter list name.");
        $("#NoOfCards").val('');
        $('#IsSeqentialCheckBox').attr('checked', false);
    }
    $('.card-composer').addClass('hide');
    $('.addl').removeClass('hide');
    $("#listloadstage").addClass('displayloadstage');
    $("#addlist").addClass('create_button');
    $("#cancellist").addClass('crossl');
    $("#addlist").show();
    $("#cancellist").show();
}

function AddList(sender, arg) {
    if (arg.keyCode == 13) {
        $("#addlist").removeClass('create_button');
        $("#cancellist").removeClass('crossl');
        $("#addlist").hide();
        $("#cancellist").hide();
        $("#listloadstage").removeClass('displayloadstage');
        var lastprojecttypeid = 0;
        var sprintid = $(this).parent('div').attr('id');
        var findspan = $("#multi").children().first();
        var span = findspan[0].tagName.toLowerCase();
        if (span == 'span') {
            lastprojecttypeid = $("#multi").children('span').children('span').last().attr('id');
        }
        var randomcolor = "";
        var list_name = $.trim($("#txtlistbox").val()).replace(/'/g, "").replace(/\n/g, "").replace(/"/g, "").replace(/\\/g, "").replace(/\//g, "");
        var IsSeq = 0;
        var IsSequential = 'Type: Normal';
        if ($('#IsSeqentialCheckBox').is(':checked')) { IsSeq = 1; IsSequential = 'Type: Sequential'; } else { IsSeq = 0 };
        var NoOfCards = 0;
        var NoOfCards2;
        var NoOfCards = NoOfCards + $("#NoOfCards").val();
        if (NoOfCards == 0) { NoOfCards2 = 'Cards limit :Max'; } else { NoOfCards2 = 'Cards limit :' + NoOfCards; };
        if ($.trim(list_name).length > 0) {
            $.ajax({
                type: "POST",
                url: "/DataTracker.svc/AddNewList",
                data: "{\"listname\": \"" + list_name + "\",\"BoardId\": \"" + BoardID + "\",\"IsSeq\": \"" + IsSeq + "\",\"NoOfCards\": \"" + NoOfCards + "\", \"lastprojecttypeid\": \"" + lastprojecttypeid + "\"}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    var projectname = list_name;
                    if (projectname.length >= 20) { projectname = projectname.substr(0, 20) + '.....'; }
                    else { projectname = projectname; }
                    if (data.d != 0) {
                        var composer = "composer" + data.d;
                        var dfdfdf = "cardaddid" + data.d;
                        var colorcollection = ["#ee9f31", "#ef662a", "#ee4526", "#d1101f", "#a20080", "#780992", "#8742a6", "#0d62e7", "#129eb1", "#008e55", "#84ab3d", "#dcc609"];
                        if (setcolorindex >= 12) {
                            setcolorindex = 0;
                        }
                        randomcolor = colorcollection[setcolorindex];
                        setcolorindex++;
                        if ($("#multi .layer").length > 0) {
                            $("#multi .layer").last().after("<span id='" + data.d + "' class='layer tile' ondrop='javascript:OnClickProject(" + data.d + ");' data-force='30'><div class='project_header' style='background:" + randomcolor + "'><div class='tile__name' title='Click for edit.' onclick='editlistname(" + data.d + ");'>" + list_name + "<img src='/Images/pencil5.png' title='Click for edit.' /></div><div id='list_action'><a href='#' class='linkin' onclick='openarchive(" + data.d + ")' id=linkin" + data.d + ">&#8230;</a><div id='pops" + data.d + "' class='webui-popoverab'><div class='js-pop-over-header' style='position:relative;'><div class='pop-over-header-title1' >List Actions</div><a onclick='closearchive()'   id='archiveclose' href='#' class='pop-close'>×</a></div><div><div class='pop-over-content js-pop-over-content u-fancy-scrollbar js-tab-parent'><ul class='pop-over-list'> <li> <a class='js-close-list'  href='#' onclick='exportsExcel(" + data.d + "," + BoardID + ")'>Export</a>  </li> </ul><ul class='pop-over-list'><li><a class='js-close-list'  href='#' onclick='ArchiveList(" + data.d + ")'>Archive This List</a></li></ul><ul class='pop-over-list'><li><a onclick='showArchivepage(" + data.d + "," + BoardID + ");' href='#' >Show Archived List..</a></li></ul><hr><ul class='pop-over-list'><li>	<a onclick='openarchivelist(" + data.d + ")' class='js-archive-cards' href='#'>Archive All Cards in This List…</a></li><li>	<a onclick='openundoarchivelist(" + data.d + "," + BoardID + ")' class='js-move-cards' href='#'>Show Archived Card…</a></li><li>	<a onclick='AddMemberToList(" + data.d + "," + BoardID + ")' class='js-move-cards' href='#'>Define list owner</a></li></ul><hr><span><input type='text' class='Cardhoursbylist' id='List_" + data.d + "'/><span>Hour<span><input type='button' class='btnsetcardhoursbylist' value='Set Card Hours' onclick='UpdateCardDueHoursByList(" + data.d + ")'/></span></div></div></div></div><div class='clr'>    <div id='webui" + data.d + "' class='pop-over is-shown webuiabc'><div><div class='pop-over-header2 js-pop-over-header'><a onclick='openarchivelistleftarrow(" + data.d + ")' class='leftarrow2' href='#'>←</a><h3>Archive All Cards in this List?</h3><a onclick='openarchivelistpopclose()'class='pop-close2' href='#'>×</a><div class='clr'></div></div><div><div class='pop-over-content js-pop-over-content u-fancy-scrollbar js-tab-parent' style='max-height: 529px;'><div class='para-part'><p>This will remove all the cards in this list from the board. To view archived cards and bring them back to the board, click  “Show Archived Card…”</p><a href='#' class='js-confirm negate full archive_button' onclick='ArchiveAllCardfromList(" + data.d + ")' >Archive All</a></div></div></div></div></div><a href='#' onclick='boxdiv(" + data.d + ")' id=listproprety" + data.d + " ><small class='IsSequential' >" + IsSequential + "</small><small>,&nbsp;</small><small class='Cardlimit'>" + NoOfCards2 + "</small></a>   </div></div><div id='isseq" + data.d + "'+ class='box-ab'><div class='box-1a'><h3 title='" + list_name + "'>" + projectname + "<input type='hidden' id='projectId' value=" + data.d + " /></h3><a onclick='divboxclose(" + data.d + ")'class='box-close' href='#'>&#10006</a><div class='clr'></div></div><div class='banner'></div><div class='box-1b'><form class='card-box'><label>Max Card Limit:</label><input type='text' id='max-card'onkeypress='return IsNumerictextbox(event);' maxlength='3'><div class='clr'></div><label >sequential</label><input type='checkbox' name='sequential' value='sequential' id='sequentialcheckbox' ><div class='clr'></div> <div id='rmsg' style='color: #69aa6f;display:none'>Update Successfully</div> <br><a onclick='ChangelimitofCard(" + data.d + ")' class='edit-box' href='#'>Submit</a><div class='clr'></div> </form></div></div> </div> <div class='tile__list ui-sortable' id='cardlist" + data.d + "'></div><div id=" + composer + " class='card-composer hide'><div style='color:red; font-size:80%;clear:both;' id='span" + data.d + "'></div><textarea maxlength='200' id='textaera" + data.d + "' onkeyup='javascript:AddCardToList(this, event," + data.d + "," + BoardID + ",\"" + list_name + "\")' class='js-search-boards' placeholder=''></textarea><a id='addNewcard1' class='fl create_button' onclick='javascript:AddCardToListOnAddButtonClick(" + data.d + "," + BoardID + ",\"" + list_name + "\")'  href='#'>Add</a><div id='cancelcard' onclick='mycross(" + data.d + ");' class='cross'>Cancel </div><p id='loadstage' class='displayloadstage'>Adding...</p><a href='#' class='list_action show-pop btn btn-default bottom-right' data-placement='auto-top' >...</a>   </div><a id=" + dfdfdf + "  onclick='myFunction(" + data.d + ");'  class='add_card'>Add a card...</a></span>");
                            sortable_add_newList();
                            $("#txtlistbox").val("").focus();
                            UpdateActivityListItem(list_name, 1);
                        }
                        else {
                            $("#multi").prepend("<span id='" + data.d + "' class='layer tile' ondrop='javascript:OnClickProject(" + data.d + ");' data-force='30'><div class='project_header' style='background:" + randomcolor + "'><div class='tile__name' title='Click for edit.' onclick='editlistname(" + data.d + ");'>" + list_name + "<img src='/Images/pencil5.png' title='Click for edit.' /></div><div id='list_action'><a href='#' class='linkin' onclick='openarchive(" + data.d + ")' id=linkin" + data.d + ">&#8230;</a><div id='pops" + data.d + "' class='webui-popoverab'><div class='js-pop-over-header' style='position:relative;'><div class='pop-over-header-title1' >List Actions</div><a onclick='closearchive()'   id='archiveclose' href='#' class='pop-close'>×</a></div><div><div class='pop-over-content js-pop-over-content u-fancy-scrollbar js-tab-parent'><ul class='pop-over-list'> <li> <a class='js-close-list'  href='#' onclick='exportsExcel(" + data.d + "," + BoardID + ")'>Export</a>  </li> </ul><ul class='pop-over-list'><li><a class='js-close-list'  href='#' onclick='ArchiveList(" + data.d + ")'>Archive This List</a></li></ul><ul class='pop-over-list'><li><a onclick='showArchivepage(" + data.d + "," + BoardID + ");' href='#' >Show Archived List..</a></li></ul><hr><ul class='pop-over-list'><li>	<a onclick='openarchivelist(" + data.d + ")' class='js-archive-cards' href='#'>Archive All Cards in This List…</a></li><li>	<a onclick='openundoarchivelist(" + data.d + "," + BoardID + ")' class='js-move-cards' href='#'>Show Archived Card…</a></li><li>	<a onclick='AddMemberToList(" + data.d + "," + BoardID + ")' class='js-move-cards' href='#'>Define list owner</a></li></ul><hr><span><input type='text' class='Cardhoursbylist' id='List_" + data.d + "'/><span>Hour<span><input type='button' class='btnsetcardhoursbylist' value='Set Card Hours' onclick='UpdateCardDueHoursByList(" + data.d + ")'/></span></div></div></div></div><div class='clr'>    <div id='webui" + data.d + "' class='pop-over is-shown webuiabc'><div><div class='pop-over-header2 js-pop-over-header'><a onclick='openarchivelistleftarrow(" + data.d + ")' class='leftarrow2' href='#'>←</a><h3>Archive All Cards in this List?</h3><a onclick='openarchivelistpopclose()'class='pop-close2' href='#'>×</a><div class='clr'></div></div><div><div class='pop-over-content js-pop-over-content u-fancy-scrollbar js-tab-parent' style='max-height: 529px;'><div class='para-part'><p>This will remove all the cards in this list from the board. To view archived cards and bring them back to the board, click  “Show Archived Card…”</p><a href='#' class='js-confirm negate full archive_button' onclick='ArchiveAllCardfromList(" + data.d + ")' >Archive All</a></div></div></div></div></div><a href='#' onclick='boxdiv(" + data.d + ")' id=listproprety" + data.d + "><small class='IsSequential' >" + IsSequential + "</small><small>,&nbsp;</small><small class='Cardlimit'>" + NoOfCards2 + "</small></a>   </div><div id='isseq" + data.d + "'+ class='box-ab'><div class='box-1a'><h3 title='" + list_name + "'>" + projectname + "<input type='hidden' id='projectId' value=" + data.d + " /></h3><a onclick='divboxclose(" + data.d + ")'class='box-close' href='#'>&#10006</a><div class='clr'></div></div><div class='banner'></div><div class='box-1b'><form class='card-box'><label>Max Card Limit:</label><input type='text' id='max-card'onkeypress='return IsNumerictextbox(event);' maxlength='3'><div class='clr'></div><label >sequential</label><input type='checkbox' name='sequential' value='sequential' id='sequentialcheckbox' ><div class='clr'></div> <div id='rmsg' style='color: #69aa6f;display:none'>Update Successfully</div> <br><a onclick='ChangelimitofCard(" + data.d + ")' class='edit-box' href='#'>Submit</a><div class='clr'></div> </form></div></div> </div> <div class='tile__list ui-sortable' id='cardlist" + data.d + "'></div>            <div id=" + composer + " class='card-composer hide'><div style='color:red; font-size:80%;clear:both;' id='span" + data.d + "'></div><textarea maxlength='200' id='textaera" + data.d + "' onkeyup='javascript:AddCardToList(this, event," + data.d + "," + BoardID + ",\"" + list_name + "\")' class='js-search-boards' placeholder=''></textarea><a id='addNewcard1' class='fl create_button' onclick='javascript:AddCardToListOnAddButtonClick(" + data.d + "," + BoardID + ",\"" + list_name + "\")'  href='#'>Add</a><div id='cancelcard' onclick='mycross(" + data.d + ");' class='cross'>Cancel</div><p id='loadstage' class='displayloadstage'>Adding...</p><a href='#' class='list_action show-pop btn btn-default bottom-right' data-placement='auto-top' >...</a>   </div><a id=" + dfdfdf + "  onclick='myFunction(" + data.d + ");'  class='add_card'>Add a card...</a></span>");
                            sortable_add_newList();
                            $("#txtlistbox").val("").focus();
                            UpdateActivityListItem(list_name, 1);
                        }
                        CalculateBoardComplete(BoardID);
                        second();
                    }
                    else {
                        alert("Project title already exists");
                    }
                },
                error: function (error) {
                }
            });
            $('#IsSeqentialCheckBox').attr('checked', false);
            $("#NoOfCards").val("");
        }
        $('.card-composer').addClass('hide');
        $('.addl').removeClass('hide');
        $("#listloadstage").addClass('displayloadstage');
        $("#addlist").addClass('create_button');
        $("#cancellist").addClass('crossl');
        $("#addlist").show();
        $("#cancellist").show();
    }
}

function editlistname(id) {
    var list_id_with_hash = "#" + id;
    var project_header_with_hash = "#" + id + " #list_action";
    var values = $(list_id_with_hash).find('.tile__name').text();
    $(list_id_with_hash).find('.tile__name').hide();
    $(project_header_with_hash).prepend("<textarea maxlength='75' onblur='editlistonblur(" + id + ");'  onkeyup='javascript:editlistonenter(" + id + ",this,event);' class='TextEditList' >" + values + "</textarea>");
    $(project_header_with_hash).find('.TextEditList').focus();
}

function editlistonblur(id) {
    var ids = "#" + id;
    var editedlist = $.trim($(ids).find('.TextEditList').val()).replace(/'/g, "").replace(/\n/g, "").replace(/"/g, "").replace(/\\/g, "").replace(/\//g, "");
    var values = $(ids).find('.tile__name').text();
    if (listnameenter == "Enter") {
        listnameenter = "";
        return false;
    }
    else {
        if ($.trim(editedlist).length > 0) {
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "/DataTracker.svc/EditListonEnterandBlur",
                data: "{\"Listname\": \"" + editedlist + "\",\"ListId\": \"" + id + "\",\"boardid\": \"" + BoardID + "\"}",
                dataType: "json",
                success: function (data) {
                    if (data.d != "exist") {
                        var updateProjectName = values + " to " + editedlist;
                        UpdateActivityListItem(updateProjectName, 11);
                        $(ids).find('.TextEditList').remove();
                        $(ids).find('.tile__name').show();
                        $(ids).find('.tile__name').html(data.d + "<img src='/Images/pencil5.png' title='Click for edit.' />");
                        if (editedlist.length >= 20) {
                            var projectname = editedlist.substr(0, 20) + '.....';
                            $('#isseq' + id).find('#pname').html(projectname);
                        }
                        else {
                            $('#isseq' + id).find('#pname').html(editedlist);
                        }
                    }
                    else {
                        alert("List Name Already Exist.");
                        $(ids).find('.TextEditList').remove();
                        $(ids).find('.tile__name').show();
                    }
                }
            })
        }
        else {
            alert("List cannot be empty");
            $(ids).find('.TextEditList').val(values).focus();
        }
    }
}

function editlistonenter(id, sender, args) {
    if (args.keyCode == 13) {
        var ids = "#" + id;
        var editedlist = $.trim($(ids).find('.TextEditList').val()).replace(/'/g, "").replace(/\n/g, "").replace(/"/g, "").replace(/\\/g, "").replace(/\//g, "");
        var values = $(ids).find('.tile__name').text();
        if ($.trim(editedlist).length > 0) {
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "/DataTracker.svc/EditListonEnterandBlur",
                data: "{\"Listname\": \"" + editedlist + "\",\"ListId\": \"" + id + "\",\"boardid\": \"" + BoardID + "\"}",
                dataType: "json",
                success: function (data) {
                    if (data.d != "exist") {
                        listnameenter = "Enter";
                        $(ids).find('.TextEditList').remove();
                        $(ids).find('.tile__name').show();
                        $(ids).find('.tile__name').html(data.d + "<img src='/Images/pencil5.png' title='Click for edit.' />");
                        if (editedlist.length >= 20) {
                            var projectname = editedlist.substr(0, 20) + '.....';
                            $('#isseq' + id).find('#pname').html(projectname);
                        }
                        else {
                            $('#isseq' + id).find('#pname').html(editedlist);
                        }
                        var updateProjectName = values + " to " + editedlist;
                        UpdateActivityListItem(updateProjectName, 11);
                    }
                    else {
                        listnameenter = "Enter";
                        alert("List Name Already Exist.");
                        $(ids).find('.TextEditList').remove();
                        $(ids).find('.tile__name').show();
                    }
                }
            })
        }
        else {
            listnameenter = "Enter";
            alert("list cannot be empty");
            $(ids).find('.TextEditList').val(values).focus();
        }
    }
}

function ArchiveList(projecttypeid, bid, spid) {

    var p = confirm("Are you sure you want to archive this list ?");

    var lastprojecttypeid = $("#multi").children('span').last().attr('id');
    var boardid = $("#boardID").val();

    var spanid = "#" + projecttypeid;
    if (p) {
        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: "/DataTracker.svc/ArchiveListWithSpId",
            data: "{\"Projectypeid\": \"" + projecttypeid + "\",\"bid\": \"" + boardid + "\", \"spid\": \"" + spid + "\",\"lastprojecttypeid\": \"" + lastprojecttypeid + "\" }",
            dataType: "json",
            success: function (data) {

                UpdateActivityListItem(listNameForArchiveList, 26);
                if (data.d > 0) {

                    $(spanid).remove();
                    projecttypecompletestatus();


                }
                else {

                    $(spanid).remove();
                    $("#Archivelist").show();
                }

            },
            error: function (error) {

            }
        })
    }

}

function projecttypecompletestatus(spid) {
    var boardid = $("#boardID").val();
    var lastprojecttypeid = $('#multi').children('span').last().attr('id');
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "/DataTracker.svc/projecttypecompletestatus",
        data: "{\"lastprojecttypeid\": \"" + lastprojecttypeid + "\", \"boardid\": \"" + boardid + "\" }",
        dataType: "json",
        success: function (data) {


        },
        error: function (error) {
        }
    })
}

function ArchiveAllCardfromList(projecttypeid) {

    var p = confirm("Are you sure you want to archieve this list?");
    var spanid = "#cardlist" + projecttypeid;
    if (p) {
        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: "/DataTracker.svc/ArchiveAllCardFromThisList",
            data: "{\"Projectypeid\": \"" + projecttypeid + "\"}",
            dataType: "json",
            success: function (data) {
                UpdateActivityListItem(listNameForArchiveList, 28);
                $(spanid).find('li').remove();
                $('.webuiabc').hide();
                $('.webui-popoverab').hide();
            }

        })
    }
}



/********************************************* Code for Create,Archive,Edit List************************************************************/

/********************************************* Code for Create,Archive,Edit Card************************************************************/

function GetList(ProjectID, ProjectType, boardid, SpId, sprintid) {

    var composer = "composer" + ProjectID;
    var dfdfdf = "cardaddid" + ProjectID;
    var abc = "#cardlist" + ProjectID + "_" + SpId;
    avgtime = parseInt($("#txtCardEstimatedTime").val());
    var avgtime1 = parseInt($("#txtCardEstimatedminute").val());
    var cardtime = 0;
    if (isNaN(avgtime)) {
        avgtime = 0;
    }
    if (isNaN(avgtime1)) {
        avgtime1 = 0;
    }


    var minute = (avgtime * 60) + avgtime1;
    var totalminute = (minute * 0.25) + minute;

    $("#" + dfdfdf).remove();
    $(abc).empty();
    $.ajax({
        type: "POST",
        url: "/DataTracker.svc/GetList",
        data: "{\"Project_ID\": \"" + ProjectID + "\",\"boardid\": \"" + boardid + "\"}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {

            $.each(data.d, function (index, item) {

                var due_date = "";
                var Task_Description = "";
                var texts = "";
                var bg_color = "";
                var today = new Date();
                var MoveHours = "";

                if (item.MoveDate.length > 0) {
                    MoveHours = Math.round((today - new Date(item.MoveDate)) / 3600000);
                    var movehours1 = Math.round(MoveHours / 24);
                    if (MoveHours > 24) {
                        MoveHours = "This has not been moved from last " + Math.round(MoveHours / 24) + " Days";
                    }
                    else {
                        MoveHours = "This has not been moved from last " + MoveHours + " Hours";
                    }
                }
                if (item.DueDate.length > 0) {
                    due_date = formatDate(item.DueDate);
                    if (due_date == '01 Jan 1990') {
                        due_date = due_date.replace(/\s+/g, '-');
                        var due_date_time1 = (item.DueDate).split('T')[1];
                        Show_due_date = due_date + " " + due_date_time1;
                    }
                    else {
                        //due_date = "&#x1f550; " + due_date;
                        due_date = due_date.replace(/\s+/g, '-');
                        var due_date_time = (item.DueDate).split('T')[1];
                        Show_due_date = due_date + " " + due_date_time;
                        someday = new Date(item.DueDate);
                        if (someday > today) {
                            var hours = Math.round((someday - today) / 3600000);
                            if (hours < 24) {
                                bg_color = 'e6b800';
                            }
                            else {
                                texts = "This card is due later.";
                                bg_color = '8cd98c';
                            }
                        } else {
                            texts = "This card has been overdue.";
                            bg_color = 'ff8080';
                        }
                    }
                    if (!(item.Description == "" || item.Description == null)) {
                        Task_Description = "";
                    }
                }
                var Color = item.Color;

                var h = "";

                h += "<div><ul class='lbl-ul'> <li><span id='cardcolorset" + item.TaskID + "' style='background-color:" + Color + ";height:20px;width:6px; display:block; margin-top: -7px;'></li></div>"
                cardtime = item.minute;
                if (cardtime != "0") {
                    carididArray.push(cardtime + "," + item.TaskID);
                }
                if (minute <= cardtime && cardtime <= totalminute) {
                    $(abc).append("<li id=" + item.TaskID + " class='ui-state-default movecard' title='" + MoveHours + "'><input type='checkbox' class='tesxer' id=" + item.TaskID + " name='cbcard' value='cbcard'><input type='hidden' id='MoveDate' value='" + item.MoveDate + "'/><a class='cards' onclick='javascript:ShowModal1(" + item.TaskID + ",\"" + item.Color + "\");'><div id='set" + item.TaskID + "' title='" + item.Message + "'>" + h + "</div><div class='cRdNmE' style='margin-left: 18px;'>" + item.Title + "</div></a><div><div class='card_bottom_sec' id='card_bottom_sec" + item.TaskID + "' onclick='javascript:ShowModal1(" + item.TaskID + ",\"" + item.Color + "\");'><p id='comment" + item.TaskID + "' title='Comments' class='badge-text2' ></p><p id='attachment" + item.TaskID + "' title='Attachments' ></p><p class='badge-text3' title='This card has description.'>" + Task_Description + "</p><p class='badge-textclock' title='Due Date for this task is:"+" "+"" + Show_due_date + "'></p><div class='showhim' id='showhim" + item.TaskID + "'><img src='Images/icons-cardmen.png' alt='per'/><div class='showme'><ul><li id='mem_task" + item.TaskID + "'></li></ul><div class='clr'></div></div></div><div class='clr'></div></div><div  id='settimecolor" + item.TaskID + "' class='timex' onclick='javascript:OpenScreenshotWindow(" + item.TaskID + ")' style='color:#000;background-color: orange;'>" + item.TotalExpandTime + "</div><div class='clr'></div> <div  id='Createddate" + item.TaskID + "' <div class='clr'></div></div>  <div> </div>");
                }
                else {
                    if (cardtime < minute) {
                        $(abc).append("<li id=" + item.TaskID + " class='ui-state-default movecard' title='" + MoveHours + "'><input type='checkbox' class='tesxer' id=" + item.TaskID + " name='cbcard' value='cbcard'><input type='hidden' id='MoveDate' value='" + item.MoveDate + "'/><a class='cards' onclick='javascript:ShowModal1(" + item.TaskID + ",\"" + item.Color + "\");'><div id='set" + item.TaskID + "' title='" + item.Message + "'>" + h + "</div><div class='cRdNmE' style='margin-left: 18px;'>" + item.Title + "</div></a><div><div class='card_bottom_sec' id='card_bottom_sec" + item.TaskID + "' onclick='javascript:ShowModal1(" + item.TaskID + ",\"" + item.Color + "\");'><p id='comment" + item.TaskID + "' title='Comments' class='badge-text2' ></p><p id='attachment" + item.TaskID + "' title='Attachments' ></p><p class='badge-text3' title='This card has description.'>" + Task_Description + "</p><p class='badge-textclock' title='Due Date for this task is:" + " " + "" + Show_due_date + "'></p><div class='showhim' id='showhim" + item.TaskID + "'><img src='Images/icons-cardmen.png' alt='per'/><div class='showme'><ul><li id='mem_task" + item.TaskID + "'></li></ul><div class='clr'></div></div></div><div class='clr'></div></div><div  id='settimecolor" + item.TaskID + "'  class='timex'  onclick='javascript:OpenScreenshotWindow(" + item.TaskID + ")' style='color:#000;background-color: #efefef;'>" + item.TotalExpandTime + "</div><div class='clr'></div> <div  id='Createddate" + item.TaskID + "' <div class='clr'></div></div>  <div> </div>");
                    }
                    else {
                        $(abc).append("<li id=" + item.TaskID + " class='ui-state-default movecard' title='" + MoveHours + "'><input type='checkbox' class='tesxer' id=" + item.TaskID + " name='cbcard' value='cbcard'><input type='hidden' id='MoveDate' value='" + item.MoveDate + "'/><a class='cards' onclick='javascript:ShowModal1(" + item.TaskID + ",\"" + item.Color + "\");'><div id='set" + item.TaskID + "' title='" + item.Message + "'>" + h + "</div><div class='cRdNmE' style='margin-left: 18px;'>" + item.Title + "</div></a><div><div class='card_bottom_sec' id='card_bottom_sec" + item.TaskID + "' onclick='javascript:ShowModal1(" + item.TaskID + ",\"" + item.Color + "\");'><p id='comment" + item.TaskID + "' title='Comments' class='badge-text2' ></p><p id='attachment" + item.TaskID + "' title='Attachments' ></p><p class='badge-text3' title='This card has description.'>" + Task_Description + "</p><p class='badge-textclock' title='Due Date for this task is:" + " " + " " + Show_due_date + "'></p><div class='showhim' id='showhim" + item.TaskID + "'><img src='Images/icons-cardmen.png' alt='per'/><div class='showme'><ul><li id='mem_task" + item.TaskID + "'></li></ul><div class='clr'></div></div></div><div class='clr'></div></div><div  id='settimecolor" + item.TaskID + "' class='timex' onclick='javascript:OpenScreenshotWindow(" + item.TaskID + ")' style='color:#000;background-color: red;'>" + item.TotalExpandTime + "</div><div class='clr'></div><div  id='Createddate" + item.TaskID + "' <div class='clr'></div> </div>  <div> </div>");
                    }
                }
                NoOfComments(item.TaskID);
                GetDescription(item.TaskID);
                GetMembersInCard(item.TaskID);
                sortable_add_newList(SpId, sprintid);
                var cardtime = $('#settimecolor' + item.TaskID).width();
                var rest = 210 - cardtime + "px";
                $('#card_bottom_sec' + item.TaskID).css('width', rest);

            });
            $(abc).after("<div id=" + composer + " class='card-composer hide'><div style='color:red; font-size:80%;clear:both;' id='span" + ProjectID + "'></div><textarea maxlength='200'  id='textaera" + ProjectID + "' onkeyup='javascript:AddCardToList(this, event," + ProjectID + "," + BoardID + ",\"" + ProjectType + "\")' class='js-search-boards' placeholder=''></textarea><a id='addNewcard1' class='fl create_button' onclick='javascript:AddCardToListOnAddButtonClick(" + ProjectID + "," + BoardID + ",\"" + ProjectType + "\")'  href='#'>Add</a><div id='cancelcard' onclick='mycross(" + ProjectID + ");' class='cross'>Cancel</div><p id='loadstage' class='displayloadstage'>Adding...</p><a href='#' class='list_action show-pop btn btn-default bottom-right' data-placement='auto-top' ></a>   </div>    <a id=" + dfdfdf + "  onclick='myFunction(" + ProjectID + ");' draggable='false' class='add_card'>Add a card...</a> ");
            $(abc).val('');
        }
    });
}


$(".movecard").live("mouseenter", function () {
    $(this).find('.tesxer').css("display", "block");
    $(".tesxer").change(function () {
        if ($(this).is(":checked")) {
            $(this).css("display", "block");
        }
    });
});

$(".movecard").live("mouseleave", function () {
    if ($(this).find('.tesxer').is(":checked")) {
        $(this).find('.tesxer').css("display", "block");
    }
    else { $(this).find('.tesxer').css("display", "none"); }
});

function Savecolor(id) {

    var objcolor;
    var chck = "rgba(0, 0, 0, 0)";

    var color = $('.spndynamiccolor').css("background-color");
    var colorid = id;

    var selected = [];
    $(".tile__list  :checked").each(function () {

        selected.push(this.id);

    });
    if (selected.length > 0) {
        var arr = JSON.stringify({ TaskId: selected });
    }
    else {
        selected.push(CardIDForLabelColor);
        var arr = JSON.stringify({ TaskId: selected });
    }


    $.ajax({
        type: "POST",
        url: "/DataTracker.svc/AddCardColor",
        data: "{\"LabelColorId\": \"" + colorid + "\"," + arr + "}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {

            cardcolorsetdynamically(selected, color)
            hexc1(color, selected);

        },
        error: function (err) {

        }

    });


}

function Deletetaskprioritycolor(lblid_delete) {

    var coloriddel = lblid_delete;

    CardIDForLabelColor
    var selected = [];
    $(".tile__list  :checked").each(function () {

        selected.push(this.id);

    });
    if (selected.length > 0) {
        var arr1 = JSON.stringify({ tidfordel: selected });
    }
    else {
        selected.push(CardIDForLabelColor);
        var arr1 = JSON.stringify({ tidfordel: selected })
    }

    $.ajax({
        type: "POST",
        url: "/DataTracker.svc/DeleteCardColor",
        data: "{\"lblidfordel\": \"" + coloriddel + "\"," + arr1 + "}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            cardcolorsetdynamically1(selected);
        },
        error: function (err) {

        }

    });


}

function AddCardToList(sender, arg, ProjectID, BoardID, ProjectType) {
    //
    var card_already_exists = "#span" + ProjectID;
    $(card_already_exists).text("");
    var Id_List_To_Add_Card = "#cardlist" + ProjectID + " li";
    var Id_textaera = "#textaera" + ProjectID;
    if (arg.keyCode == 13) {
        var cardname = $.trim($(Id_textaera).val()).replace(/'/g, "").replace(/\n/g, "").replace(/"/g, "").replace(/\\/g, "").replace(/\//g, "");
        var carddiv = $('.card_row').last();
        if ($.trim(cardname).length > 0) {
            SaveNewCard(ProjectID, cardname, ProjectType);
            $(Id_textaera).val("");
        }
    }
}

//Ameeq Changes for AddingSprintId that takes 4 Arguments
function AddCardToListOnAddButtonClick(ProjectID, BoardID, SprintId, ProjectType) {
    var card_already_exists = "#span" + ProjectID;
    $(card_already_exists).text("");
    var Id_textaera = "#textaera" + ProjectID;
    var Id_sprint = SprintId;
    var cardname = $.trim($(Id_textaera).val()).replace(/"/g, '\\"');
    var carddiv = $('.card_row').last();
    if ($.trim(cardname).length > 0) {
        SaveNewCard(ProjectID, cardname, Id_sprint, ProjectType);
        $(Id_textaera).val("");
    }
    else {
        $(card_already_exists).text("Please enter card name.");
    }
}

//Ameeq Changes for AddingSprintId that takes 4 Arguments
function SaveNewCard(ProjectID, cardname, AddsprintId, ProjectType) {
    
    $("#addNewcard1").removeClass('create_button');
    $("#cancelcard").removeClass('cross');
    $("#addNewcard1").hide();
    $("#cancelcard").hide();
    $("#loadstage").removeClass('displayloadstage');

    var editedcardname = "";
    var boaridforcard = $("#boardID").val();
    var CardLimits = $("#" + ProjectID).find(".Cardlimit").text();
    CardLimits = CardLimits.split(":");
    var CardLimit = CardLimits[1].trim();

    //Currendate
    var curdate = new Date();//11/20/2017 11:57:58 AM
    var month = curdate.getMonth() + 1;
    var day = curdate.getDate();
    var time = curdate.toLocaleTimeString();
    curdate = "";
    if ($("#" + ProjectID).find(".Cardlimit").text().toLowerCase().indexOf("max") >= 0) {

        var Id_List_To_Add_Card = "#cardlist" + ProjectID + "_" + AddsprintId + " li";
        var Add_Card_To_Blank_list = "#cardlist" + ProjectID + "_" + AddsprintId;
        var card_already_exists = "#span" + ProjectID;
        var dataParams = { "Card_Name": cardname, "Project_ID": ProjectID, "SprintId": AddsprintId, "boardid": boaridforcard };
        $.ajax({
            type: "POST",
            url: "/DataTracker.svc/SaveNewCardWithSprintId",
            data: "{\"Card_Name\": \"" + cardname + "\",\"Project_ID\": \"" + ProjectID + "\",\"InSprintId\": \"" + AddsprintId + "\",\"boardid\": \"" + boaridforcard + "\"}",
            dataType: "json",
            contentType: "application/json",
            success: function (data) {
                editedcardname = $('#' + ProjectID + ' .tile__name').text();
                var cardMessage = cardname + ' ' + 'in' + ' ' + editedcardname;
                var due_date = "";
                var Show_due_date = "";
                var Task_Description = "";
                var texts = "";
                var bg_color = "";
                var today = new Date();
                var task_id = data.d;
                var decodecardname = cardname.replace("\\", "").replace("\\", "").replace("\\", "").replace("\\", "").replace("\\", "").replace("\\", "");
                $.ajax({
                    type: "POST",
                    url: "/DataTracker.svc/GetNewCardDueDate",
                    data: "{\"Task_ID\": \"" + task_id + "\"}",
                    dataType: "json",
                    contentType: "application/json",
                    success: function (data) {
                        var Response = JSON.parse(data.d);
                        $.each(Response, function (index, item) {
                            var due_date_OnCard = formatDate(item.DueDate);
                            due_date_OnCard = due_date_OnCard.replace(/\s+/g, '-');
                            var due_date_time = (item.DueDate).split('T')[1];
                            Show_due_date = due_date_OnCard + " " + due_date_time;
                            if (task_id != 0) {
                                if ($(Id_List_To_Add_Card).length > 1) {
                                    UpdateActivityListItem(cardMessage, 7, task_id);
                                    $(Add_Card_To_Blank_list).append("<li id=" + task_id + " class='ui-state-default movecard' title='This card is just created.'><input type='checkbox' class='tesxer' id=" + task_id + " name='cbcard' value='cbcard'><a class='cards' onclick='javascript:ShowModal1(" + task_id + ",\"\");'><div id='set" + task_id + "' title=''><div><ul class='lbl-ul'> <li><span id='cardcolorset" + task_id + "' style='background-color:;height:20px;width:6px; display:block; margin-top: -7px;'></li></div></div><div class='cRdNmE'>" + decodecardname + "</div></a><div><div class='card_bottom_sec' id='card_bottom_sec" + task_id + "' onclick='javascript:ShowModal1(" + task_id + ",\"\");'><p class='badge-text' style='background-color: #" + bg_color + ";' title='" + texts + "'>" + due_date + "</p><p id='comment" + task_id + "' title='Comments' ></p><p id='attachment" + task_id + "' title='Attachments' ></p><p class='badge-text3' title='This card has description.'>" + Task_Description + "</p><p class='badge-textclock' title='Due Date for this task is:" + " " + " " + Show_due_date + "'><img src='images/clock-icon.png' style='width: 12px;margin: 5px 0px -3px 0px;border-radius: 0px !important;'/></p><div class='showhim' id='showhim" + task_id + "'><img src='Images/icons-cardmen.png' alt='per'/><div class='showme'><ul><li id='mem_task" + task_id + "'></li></ul></div></div><div class='clr'></div></div></div><div id='settimecolor" + task_id + "' class='timex' onclick='javascript:OpenScreenshotWindow(" + task_id + ")' style='color:#000;background-color:#efefef;'>00:00</div><div class='clr'></div><div id='Createddate" + task_id + "' class='Createddate' style='float-left:5px'>" + curdate + "</div></div>  </div> </div>");
                                    sortable_add_newList();

                                } else {
                                    $(Add_Card_To_Blank_list).append("<li id=" + task_id + " class='ui-state-default movecard' title='This card is just created.'><input type='checkbox' class='tesxer' id=" + task_id + " name='cbcard' value='cbcard'><a class='cards' onclick='javascript:ShowModal1(" + task_id + ",\"\");'><div id='set" + task_id + "' title=''><div><ul class='lbl-ul'> <li><span id='cardcolorset" + task_id + "' style='background-color:;height:20px;width:6px; display:block; margin-top: -7px;'></li></div></div><div class='cRdNmE'>" + decodecardname + "</div></a><div><div class='card_bottom_sec' id='card_bottom_sec" + task_id + "' onclick='javascript:ShowModal1(" + task_id + ",\"\");'><p class='badge-text' style='background-color: #" + bg_color + ";' title='" + texts + "'>" + due_date + "</p><p id='comment" + task_id + "' title='Comments' ></p><p id='attachment" + task_id + "' title='Attachments' ></p><p class='badge-text3' title='This card has description.'>" + Task_Description + "</p><p class='badge-textclock' title='Due Date for this task is:" + " " + " " + Show_due_date + "'><img src='images/clock-icon.png' style='width: 12px;margin: 5px 0px -3px 0px;border-radius: 0px !important;'/></p><div class='showhim'  id='showhim" + task_id + "'><img src='Images/icons-cardmen.png' alt='per'/><div class='showme'><ul><li id='mem_task" + task_id + "'></li></ul></div></div><div class='clr'></div></div></div><div id='settimecolor" + task_id + "' class='timex' onclick='javascript:OpenScreenshotWindow(" + task_id + ")' style='color:#000;background-color:#efefef;'>00:00</div><div class='clr'></div><div id='Createddate" + task_id + "' class='Createddate'>" + curdate + "</div></div>  </div> </div>");
                                    sortable_add_newList();
                                    UpdateActivityListItem(cardMessage, 7, task_id);
                                }
                                AddNewEditTask(task_id, decodecardname, ProjectType, ProjectID);
                                CalculateBoardCompleteWithSprint(BoardID, AddsprintId);
                                SetProgressBarPer(AddsprintId);
                                second();
                                GetCardsCount(ProjectID);
                                var cardtime = $('#settimecolor' + task_id).width();
                                var rest = 210 - cardtime + "px";
                                $('#card_bottom_sec' + task_id).css('width', rest);
                            }
                            else {
                                $(card_already_exists).append("Card already exists.");
                            }
                        });
                    }
                });
            },
            error: function (error) {
                swal("Oops!", "Some error in adding card.", "error");
            }
        });
    }

    else if ($("#cardlist" + ProjectID + "_" + AddsprintId + "> li").length < CardLimit) {

        var Id_List_To_Add_Card = "#cardlist" + ProjectID + "_" + AddsprintId + " li";
        var Add_Card_To_Blank_list = "#cardlist" + ProjectID + "_" + AddsprintId;
        var card_already_exists = "#span" + ProjectID;
        var dataParams = { "Card_Name": cardname, "Project_ID": ProjectID, "SprintId": AddsprintId, "boardid": boaridforcard };
        $.ajax({
            type: "POST",
            url: "/DataTracker.svc/SaveNewCardWithSprintId",
            data: "{\"Card_Name\": \"" + cardname + "\",\"Project_ID\": \"" + ProjectID + "\",\"InSprintId\": \"" + AddsprintId + "\",\"boardid\": \"" + boaridforcard + "\"}",
            dataType: "json",
            contentType: "application/json",
            success: function (data) {
                
                editedcardname = $('#' + ProjectID + ' .tile__name').text();
                var cardMessage = cardname + ' ' + 'in' + ' ' + editedcardname;
                var due_date = "";
                var Show_due_date = "";
                var Task_Description = "";
                var texts = "";

                var bg_color = "";
                var today = new Date();
                var task_id = data.d;
                var decodecardname = cardname.replace("\\", "").replace("\\", "").replace("\\", "").replace("\\", "").replace("\\", "").replace("\\", "");
                $.ajax({
                    type: "POST",
                    url: "/DataTracker.svc/GetNewCardDueDate",
                    data: "{\"Task_ID\": \"" + task_id + "\"}",
                    dataType: "json",
                    contentType: "application/json",
                    success: function (data) {
                        var Response = JSON.parse(data.d);
                        $.each(Response, function (index, item) {
                            var due_date_OnCard = formatDate(item.DueDate);
                            due_date_OnCard = due_date_OnCard.replace(/\s+/g, '-');
                            var due_date_time = (item.DueDate).split('T')[1];
                            Show_due_date = due_date_OnCard + " " + due_date_time;
                            if (task_id != 0) {
                                if ($(Id_List_To_Add_Card).length > 1) {
                                    UpdateActivityListItem(cardMessage, 7, task_id);

                                    $(Add_Card_To_Blank_list).append("<li id=" + task_id + " class='ui-state-default movecard' title='This card is just created.'><input type='checkbox' class='tesxer' id=" + task_id + " name='cbcard' value='cbcard'><a class='cards' onclick='javascript:ShowModal1(" + task_id + ",\"\");'><div id='set" + task_id + "' title=''><div><ul class='lbl-ul'> <li><span id='cardcolorset" + task_id + "' style='background-color:;height:20px;width:6px; display:block; margin-top: -7px;'></li></div></div><div class='cRdNmE'>" + decodecardname + "</div></a><div><div class='card_bottom_sec' id='card_bottom_sec" + task_id + "' onclick='javascript:ShowModal1(" + task_id + ",\"\");'><p class='badge-text' style='background-color: #" + bg_color + ";' title='" + texts + "'>" + due_date + "</p><p id='comment" + task_id + "' title='Comments' ></p><p id='attachment" + task_id + "' title='Attachments' ></p><p class='badge-text3' title='This card has description.'>" + Task_Description + "</p><p class='badge-textclock' title='Due Date for this task is:" + " " + " " + Show_due_date + "'><img src='images/clock-icon.png' style='width: 12px;margin: 5px 0px -3px 0px;border-radius: 0px !important;'/></p><div class='showhim' id='showhim" + task_id + "'><img src='Images/icons-cardmen.png' alt='per'/><div class='showme'><ul><li id='mem_task" + task_id + "'></li></ul></div></div><div class='clr'></div></div></div><div id='settimecolor" + task_id + "' class='timex' onclick='javascript:OpenScreenshotWindow(" + task_id + ")' style='color:#000;background-color:#efefef;'>00:00</div><div class='clr'></div><div id='Createddate" + task_id + "' class='Createddate'>" + curdate + "</div></div>  </div> </div>");

                                    sortable_add_newList();
                                } else {

                                    $(Add_Card_To_Blank_list).append("<li id=" + task_id + " class='ui-state-default movecard' title='This card is just created.'><input type='checkbox' class='tesxer' id=" + task_id + " name='cbcard' value='cbcard'><a class='cards' onclick='javascript:ShowModal1(" + task_id + ",\"\");'><div id='set" + task_id + "' title=''><div><ul class='lbl-ul'> <li><span id='cardcolorset" + task_id + "' style='background-color:;height:20px;width:6px; display:block; margin-top: -7px;'></li></div></div><div class='cRdNmE'>" + decodecardname + "</div></a><div><div class='card_bottom_sec' id='card_bottom_sec" + task_id + "' onclick='javascript:ShowModal1(" + task_id + ",\"\");'><p class='badge-text' style='background-color: #" + bg_color + ";' title='" + texts + "'>" + due_date + "</p><p id='comment" + task_id + "' title='Comments' ></p><p id='attachment" + task_id + "' title='Attachments' ></p><p class='badge-text3' title='This card has description.'>" + Task_Description + "</p><p class='badge-textclock' title='Due Date for this task is:" + " " + " " + Show_due_date + "'><img src='images/clock-icon.png' style='width: 12px;margin: 5px 0px -3px 0px;border-radius: 0px !important;'/></p><div class='showhim' id='showhim" + task_id + "'><img src='Images/icons-cardmen.png' alt='per'/><div class='showme'><ul><li id='mem_task" + task_id + "'></li></ul></div></div><div class='clr'></div></div></div><div id='settimecolor" + task_id + "' class='timex' onclick='javascript:OpenScreenshotWindow(" + task_id + ")' style='color:#000;background-color:#efefef;'>00:00</div><div class='clr'></div><div id='Createddate" + task_id + "' class='Createddate'>" + curdate + "</div></div>  </div> </div>");

                                    sortable_add_newList();
                                }
                                AddNewEditTask(task_id, decodecardname, ProjectType, ProjectID);
                                CalculateBoardCompleteWithSprint(BoardID, AddsprintId);
                                SetProgressBarPer(AddsprintId);
                                second();
                                GetCardsCount(ProjectID);
                                var cardtime = $('#settimecolor' + task_id).width();
                                var rest = 210 - cardtime + "px";

                                $('#card_bottom_sec' + task_id).css('width', rest);

                            }
                            else {
                                $(card_already_exists).append("Card already exists.");
                            }
                        });
                    }
                });
            },
            error: function (error) {
                swal("Oops!", "Some error in adding card.", "error");
            }
        });
    }
    else {
        swal("Card can't be added!", "Please update the card limit.", "error");
    }
    $("#loadstage").addClass('displayloadstage');
    $("#addNewcard1").addClass('create_button');
    $("#cancelcard").addClass('cross');


    $("#addNewcard1").show();
    $("#cancelcard").show();
}


function GetCardsCount(ProjectID) {
    $.ajax({
        type: "POST",
        url: "/DataTracker.svc/GetTotalCardCountOnList",
        data: "{\"Project_ID\": \"" + ProjectID + "\"}",
        dataType: "json",
        contentType: "application/json",
        success: function (data) {
            var Response = JSON.parse(data.d);
            $.each(Response, function (index, item) {
                $('#listCount' + ProjectID).html("");
                $('#listCount' + ProjectID).html("<div class='listCount' id='listCount" + ProjectID + "' title='Total Cards'>" + item.CardsCount + "</div>");
            });
            }
    });
}

function EditCardName() {
    var CARDNAME = $("#popup_cardname").text();
    oldCardName = CARDNAME;
    var ID = $("#hide_cardid").val();
    var SPID = $("#hide_sprintid").val();
    $("#popup_cardname").hide();
    $('#popup_cardname').after("<textarea id='txtareacardname' maxlength='200' style='width:100%' onblur='javascript:editcardonblur(" + ID + ");'  onkeyup='javascript:editcardonenter(" + ID + ",this,event);'>" + CARDNAME + "</textarea>");
    document.getElementById('txtareacardname').focus();
}

function editcardonblur(id) {
    var idforcard = "#" + id;
    var lid = ($(idforcard).closest('div').attr('id').split("cardlist")[1]).split('_')[0];
    var editedcard = $.trim($('#txtareacardname').val());
    var editedcardname = $.trim($('#txtareacardname').val()).replace(/"/g, '\\"');
    var cardNameUpdateMsg = oldCardName + " <strong>updated to </strong>" + editedcardname;
    if (cardnameenter == "Enter") {
        cardnameenter = "";
        return false;
    }
    else {
        if ($.trim(editedcardname).length > 0) {
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "/DataTracker.svc/EditCardonEnterandBlur",
                data: "{\"Cardname\": \"" + editedcardname + "\",\"CardId\": \"" + id + "\",\"listid\": \"" + lid + "\"}",
                dataType: "json",
                success: function (data) {


                    if (data.d != "exist") {
                        $('#txtareacardname').remove();
                        $("#popup_cardname").show().html(data.d + "<img src='/Images/pencil5.png' alt='Edit' title='Click for edit.' />");
                        $(idforcard).find('.cards .cRdNmE').text(data.d);
                        UpdateActivityListItem(cardNameUpdateMsg, 10, id);
                    }
                    else {
                        alert("Card Name Already Exist.");
                        $('#txtareacardname').remove();
                        $("#popup_cardname").show();
                    }
                }
            })
        }
        else {
            alert("card cannot be empty");
            var CARDNAME = $("#popup_cardname").text();
            $('#txtareacardname').val(CARDNAME).focus();
        }
    }
}

function editcardonenter(id, sender, args) {
    var idforcard = "#" + id;
    var lid = ($(idforcard).closest('div').attr('id').split("cardlist")[1]).split('_')[0];
    if (args.keyCode == 13) {

        //var editedcardname = $.trim($('#txtareacardname').val()).replace(/\n/g, "").replace(/"/g, "").replace(/\\/g, "").replace(/\//g, "").replace(/"/g, "''");
        var editedcardname = $.trim($('#txtareacardname').val()).replace(/"/g, '\\"');
        var cardNameUpdateMsg = oldCardName + " <strong>updated to </strong>" + editedcardname;
        if ($.trim(editedcardname).length > 0) {
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "/DataTracker.svc/EditCardonEnterandBlur",
                data: "{\"Cardname\": \"" + editedcardname + "\",\"CardId\": \"" + id + "\",\"listid\": \"" + lid + "\"}",
                dataType: "json",
                success: function (data) {
                    if (data.d != "exist") {
                        cardnameenter = "Enter";
                        $('#txtareacardname').remove();
                        $("#popup_cardname").show().html(data.d + "<img src='/Images/pencil5.png' alt='Edit' title='Click for edit.' />");
                        $(idforcard).find('.cards .cRdNmE').text(data.d);
                        UpdateActivityListItem(cardNameUpdateMsg, 10, id);
                    }
                    else {
                        cardnameenter = "Enter";
                        alert("Card Name Already Exist.");
                        $('#txtareacardname').remove();
                        $("#popup_cardname").show();
                    }
                }
            })
        }
        else {
            cardnameenter = "Enter";
            alert("card cannot be empty");
            var CARDNAME = $("#popup_cardname").text();
            $('#txtareacardname').val(CARDNAME).focus();
        }
    }
}

function ArchiveCard() {
    var selectedcardidArchive = [];
    var ID = $("#hide_cardid").val();
    $(".tile__list  :checked").each(function () {
        selectedcardidArchive.push(this.id);
        $("#ArchiveCardmultiSelection").hide();
        $("#SendtoboardmultiSelection").css("display", "block");
    });

    if (selectedcardidArchive.length > 0) {

    }
    else {
        selectedcardidArchive.push(ID);

    }


    var cardforid = "#" + ID;
    var ListID = $(cardforid).parents("span").attr("id");
    var listforid = "#" + ListID;
    $.ajax({
        type: "POST",
        url: "/DataTracker.svc/Archivecard",
        data: JSON.stringify({ CardId: selectedcardidArchive }),

        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            //GetCardsCount(ListID);
            SendNotificationMailForArchiveCard(cardNameOfComment, ID);
            UpdateActivityListItem(cardNameOfComment, 21);
            $("#ArchiveID").addClass('archivedisplay');
            $("#SendtoboardID").removeClass('archivedisplay');

            for (var i = 0; i < selectedcardidArchive.length; i++) {

                var cardidarchive = selectedcardidArchive[i];
                var ListID = $("#" + cardidarchive).parents("span").attr("id");
                var listforid = "#" + ListID;
                GetCardsCount(ListID);
                $(listforid).find("#" + cardidarchive).hide();

            }

        }
    });
}

function SendtoboardArchive() {
    var ID = $("#hide_cardid").val();
    var cardidArchiveshow = [];
    $(".tile__list  :checked").each(function () {
        cardidArchiveshow.push(this.id);
        $("#ArchiveCardmultiSelection").show();
        $("#SendtoboardmultiSelection").css("display", "none");
    });
    if (cardidArchiveshow.length > 0) {
    }
    else {
        cardidArchiveshow.push(ID);
    }

    if (cardidArchiveshow.length > 0) {

    }
    var cardforid = "#" + ID;

    var ListID = $(cardforid).parents("span").attr("id");
    var listforid = "#" + ListID;
    $.ajax({
        type: "POST",
        url: "/DataTracker.svc/SendtoboardArchive",
        data: JSON.stringify({ CardId: cardidArchiveshow }),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            UpdateActivityListItem(cardNameOfComment, 25, ID);
            $("#ArchiveID").removeClass('archivedisplay');
            $("#SendtoboardID").addClass('archivedisplay');
            for (var i = 0; i < cardidArchiveshow.length; i++) {
                var cardidarchive = cardidArchiveshow[i];
                var ListID = $("#" + cardidarchive).parents("span").attr("id");
                var listforid = "#" + ListID;
                $(listforid).find("#" + cardidarchive).show();
            }

        }
    });
}

/********************************************* Code for Create,Archive,Edit Card************************************************************/

/********************************************* Code for show,cancel,save & edit Description***********************************************/

function GetDescription(CardId, Description) {
    $("#divedit").hide();
    var cardid_des = "#" + CardId;

    if (Description != null) {
        if (Description.length > 0) {
            $("#Description_Heading").show();
            $("#description_list").show();

            $(cardid_des).find('.badge-text3').empty();
            $(cardid_des).find('.badge-text3').append("<img src='images/description-icon.png' style='width: 12px;margin: 5px 0px -3px 0px;border-radius: 0px !important;'/>");
            $("#description_list").html("<pre><h6><a href='#' id='descr' style='display: block !important;word-break: normal !important;font-size: 14px; font-family: arial; ' onclick='javascript:showDescription();'>" + Description.replace(/m1/g, '"').replace(/m2/g, "'").replace(/m3/g, "\\").replace(/m4/g, "/").replace(/m5/g, "\n") + "</a></h6><pre>");

            $(cardid_des).find('#description').append("<img src='Images/ibooks.png' alt='not found image.' style='height: 10px;float: right;'/>");
        }
        else {

            $("#Description_Heading").hide();
            $("#description_list").show();
            $("#description_list").html("<pre><a href='#' id='descr'  style='display:block;'  onclick='javascript:showDescription();'>Edit the description</a><pre>");
        }
    }
}


function GetTaskDescription(CardId) {
    $("#divedit").hide();
    var cardid_des = "#" + CardId;

    $.ajax({
        type: "POST",
        url: "/DataTracker.svc/GetDescriptionByTaskID",
        data: "{\"TaskID\": \"" + CardId + "\"}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            $.each(data.d, function (index, item) {
                if (item.Description.length > 0) {
                    $("#Description_Heading").show();
                    $("#description_list").show();

                    $(cardid_des).find('.badge-text3').empty();
                    $(cardid_des).find('.badge-text3').append("<img src='images/description-icon.png' style='width: 12px;margin: 5px 0px -3px 0px;border-radius: 0px !important;'/>");
                    $("#description_list").html("<pre><h6><a href='#' id='descr' style='display: block !important;word-break: normal !important;font-size: 14px; font-family: arial; ' onclick='javascript:showDescription();'>" + item.Description.replace(/m1/g, '"').replace(/m2/g, "'").replace(/m3/g, "\\").replace(/m4/g, "/").replace(/m5/g, "\n") + "</a></h6><pre>");

                    $(cardid_des).find('#description').append("<img src='Images/ibooks.png' alt='not found image.' style='height: 10px;float: right;'/>");
                }
                else {

                    $("#Description_Heading").hide();
                    $("#description_list").show();
                    $("#description_list").html("<pre><a href='#' id='descr'  style='display:block;'  onclick='javascript:showDescription();'>Edit the description</a><pre>");
                }
            });
        }
    });
}

function SaveDescription() {

    $("#showdesc").removeClass("fl create_button");
    $("#hdescription").removeClass("cross");

    $("#showdesc").css('display', 'none');
    $("#hdescription").css('display', 'none');
    $("#lblsend1").show();


    $("#finddesc").show();
    var cardid = $("#hide_cardid").val();
    var cardid_description = "#" + cardid;
    var cardid_with_hash = "#" + cardid + ' .badge-text3';

    var description = $('#txteditdescription').val().trim();
    var newdescription = description;

    if (newdescription == "" || newdescription == undefined || newdescription == null) {

        $("#showdesc").addClass("fl create_button");
        $("#hdescription").addClass("cross");

        $("#showdesc").show();
        $("#hdescription").show();
        $("#lblsend1").css('display', 'none');
        alert('description required');
    }
    else {
        $(cardid_with_hash).html("");
        description = $.trim($('#txteditdescription').val().replace(/\n/g, "m5").replace(/"/g, "m1").replace(/'/g, "m2").replace(/\\/g, "m3").replace(/\//g, "m4"));
        newdescription = description.replace(/m1/g, '"').replace(/m2/g, "'").replace(/m3/g, "\\").replace(/m4/g, "/").replace(/m5/g, "\n");
        $.ajax({
            type: "POST",
            url: "/DataTracker.svc/UpdateDescripition",
            data: "{\"taskid\": \"" + cardid + "\",\"description\": \"" + description + "\"}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                var newMember = "in " + cardNameOfComment;
                UpdateActivityListItem(newMember, 17, cardid);
                if (data.d > 0) {
                    if (newdescription.length > 0) {
                        $("#divedit").hide();
                        $("#Description_Heading").show();
                        $("#description_list").show();
                        $('#descr').text(newdescription);
                        $(cardid_with_hash).html("<img src='images/description-icon.png' style='width: 12px;margin: 5px 0px -3px 0px;border-radius: 0px !important;'/>");
                        $("#showdesc").addClass("fl create_button");
                        $("#hdescription").addClass("cross");


                        $("#showdesc").show();
                        $("#hdescription").show();
                        $("#lblsend1").css('display', 'none');
                    }
                    else {
                        $("#Description_Heading").hide();
                        $("#divedit").hide();
                        $("#description_list").show();
                        $("#descr").text("Edit the description");

                        $("#showdesc").addClass("fl create_button");
                        $("#hdescription").addClass("cross");

                        $("#showdesc").show();
                        $("#hdescription").show();
                        $("#lblsend1").css('display', 'none');
                    }
                }
            }
        });
    }

}

function showDescription() {
    //
    if ($("#descr").text() == "Edit the description") {
        $('#description_list').hide();
        $("#Description_Heading").hide();
        $("#divedit").removeClass('hide').show();
        $("#txteditdescription").val("").focus();

    }
    else {
        $("#divedit").show();
        $("#txteditdescription").val($("#descr").text()).focus();
        $('#description_list').hide();
        $("#finddesc").hide();
    }
}

function CancelDescription() {
    $("#txteditdescription").val("");
    $("#finddesc").show();
    $("#divedit").hide();
    $('#description_list').show();
}

/********************************************* Code for show,cancel,save & edit Description***********************************************/

/********************************************* Code for Create,Remove,Edit Comment********************************************************/

function removeComment(cid, card_id) {

    var p = confirm("Are you sure you want to delete this item ?");
    if (p) {
        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: "/DataTracker.svc/removeComment",
            data: "{\"Cid\": \"" + cid + "\"}",
            dataType: "json",
            success: function (data) {

                G_CommentID = 0;
                UpdateActivityListItem("in " + cardNameOfComment, 13, card_id);
                NoOfComments(card_id);
                showboardcomment(card_id);
            },
            error: function (error) {
                alert("Some error!");
            }
        })
    }
}

function showboardcomment(card_id) {

    var desc_title = "";
    $("#popup_tasks_comments").html('');
    $.ajax({
        type: "POST",
        url: "/DataTracker.svc/GetCommentsByTaskID",
        data: "{\"TaskID\": \"" + card_id + "\"}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            if (data.d != "") {

                $("#CommentID").empty();
                $("#activityandassignby").empty();
                $("#CreaterName").empty();

                var activityandassignduser = data.d[0].lstCardActivityandassignby;
                var commentandcreatedby = data.d[0].lsttask;
                var createby = data.d[0].lstCreatedBy;
                if (commentandcreatedby.length == 0) {
                    $('.comment-cntnr-inr').css({ "height": "50px", "margin-left": "22px" });
                    $("#CommentID").append("No comments");
                }
                else { $('.comment-cntnr-inr').css({ "height": "208px", "margin-left": "0px" }); $('.comment-cntnr-inr').css('display', 'block'); }
                if (activityandassignduser.length == 0) {
                    $('.activity-inr').css({ "height": "50px" });
                    $("#activityandassignby").append("No activity");
                }
                else { $('.activity-inr').css({ "height": "208px" }); $('.activity-inr').css('display', 'block'); }
                if (commentandcreatedby.length > 0 || activityandassignduser.length > 0 || createby.length > 0) {
                    $('.activity-cntnr').css('display', 'block');

                    $.each(createby, function (index, item) {
                        var datestring = item.CreatedDate.split(' ');
                        var dateformate = myFormatDate(datestring[0]);
                        $("#CreaterName").append(" <h3>Created by</h3><div class='first-alpha'>" + item.fname.charAt(0) + "</div><div class='name'>" + item.fname + " " + item.lname + "</div><div class='activ-cal'><i class='fa fa-calendar' aria-hidden='true'></i></div><span>" + dateformate + "</span><div class='activ-cal'><i class='fa fa-clock-o' aria-hidden='true'></i></div><span>" + datestring[1] + " " + datestring[2] + "</span>");
                    });

                    $.each(commentandcreatedby, function (index, item) {

                        $("#CommentID").append(" <div class='comment-cntnr'><div class='first-alpha'>" + item.fname.charAt(0) + "</div><div class='name'>" + item.fname + " " + item.lname + "</div><i class='fa fa-trash-o float-right'  onclick='javascript:removeComment(" + item.CommentID + "," + card_id + ");' aria-hidden='true'></i><i class='fa fa-pencil float-right' onclick='javascript:EditComment(" + item.CommentID + "," + card_id + ")' aria-hidden='true'></i><p class='Comments" + item.CommentID + "'>" + item.Comments + "</p><div class='date'>" + item.commenteddate + "</div></div>");

                    });

                    $.each(activityandassignduser, function (index, item) {
                        if (item.uniqueID == 2) {
                            $("#activityandassignby").append("<ul><li><span><img src='Images/icon-1.png'></span><p>Card Assigned to " + item.assignedUserName + " " + item.assignedUserLname + " by " + item.AssignByUserFname + " " + item.AssignByUserLname + "</p></li></ul>");
                        }
                        else {
                            var msg = item.Message.split('</b>');
                            var res = msg[1].substr(11);
                            $("#activityandassignby").append("<ul><li><span><img src='Images/icon-2.png'></span><p>Card Moved from " + res + " " + msg[2] + " by " + item.SwapByFname + " " + item.SwapByLname + "</p></li></ul>");
                        }

                    });
                }
                else { $('.activity-cntnr').css('display', 'none'); }

            }

        }
    });
}

function myFormatDate(d) {
    var mothes = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
    var d_y = d.split('/')[2];
    var d_m = d.split('/')[0];
    var d_d = d.split('/')[1];

    var mon = mothes[parseInt(d_m, 10) - 1];
    var dd = parseInt(d_d, 10);

    if (d_y == "0000") {
        return dd + " " + mon;
    } else {
        return dd + " " + mon + " " + d_y;
    }
}

function Popup_Save_Comment() {
    
    $("#sendComment").removeClass("create_button");

    $("#sendComment").css('display', 'none');
    $("#lblsend2").show();

    var cardName = "in " + cardNameOfComment;
    var comments = $('#popup_list_comment').val();
    var modifiedcomment = comments;
    var formail = comments;
    document.getElementById("popup_list_comment").value = "";
    var cardid = $("#hide_cardid").val();
    var date_time = new Date();
    if (modifiedcomment == "" || modifiedcomment == undefined || modifiedcomment == null) {

        $("#sendComment").addClass("create_button");

        $("#sendComment").show();
        $("#lblsend2").css('display', 'none');

        alert("Add comment is required.");
    }
    else {
        comments = comments.replace(/\n/g, '<br/>').replace(/"/g, "'"); // Ameeq Changes for Error in Adding Comments and Updation in Comments on 13/04/2018.
        modifiedcomment = comments.replace(/"/g, "'").replace(/\n/g, '<br/>');
        //modifiedcomment = comments.replace(/"/g, "'");
        if (G_CommentID == 0) {
            $.ajax({
                type: "POST",
                url: "/DataTracker.svc/AddCommentsByTaskID",
                data: "{\"Comment\": \"" + comments + "\",\"Task_ID\": \"" + cardid + "\"}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    UpdateActivityListItem(cardName, 12, cardid);
                    SendNotificationMailForMessageOnCard(cardNameOfComment, cardid, formail.replace(/\n/g, "<br/>"));
                    $.each(data.d, function (index, item) {
                        
                        $("#CommentID").prepend(" <div class='comment-cntnr'><div class='first-alpha'>" + item.fname.charAt(0) + "</div><div class='name'>" + item.fname + " " + item.lname + "</div><i class='fa fa-trash-o float-right'  onclick='javascript:removeComment(" + item.CommentID + "," + item.TaskID + ");' aria-hidden='true'></i><i class='fa fa-pencil float-right' onclick='javascript:EditComment(" + item.CommentID + "," + item.TaskID + ")' aria-hidden='true'></i><p class='Comments" + item.CommentID + "'>" + item.Comments + "</p><div class='date'>" + item.CreatedDate + "</div></div>");


                        $('.comment-cntnr-inr').css({ "height": "208px", "margin-left": "0px" });
                        NoOfComments(cardid);
                    });
                    $("#sendComment").removeClass("create_button");
                    $("#sendComment").addClass("create_button_disabled");
                    $("#sendComment").attr('onclick', '');
                    $("#sendComment").show();
                    $("#lblsend2").css('display', 'none');
                    showboardcomment(cardid);

                },
                error: function (error) {

                    $("#sendComment").addClass("create_button");

                    $("#sendComment").show();
                    $("#lblsend2").css('display', 'none');

                    alert("Some error in adding comments!");
                }
            });
        }
        else {
            //$(".Comments" + G_CommentID).text(modifiedcomment.replace(/<br\s*\/?>/gi, '\n'));
            $.ajax({
                type: "POST",
                url: "/DataTracker.svc/UpdateComments",
                data: "{\"Comment\": \"" + comments + "\",\"Comment_ID\": \"" + G_CommentID + "\"}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    //Ameeq Changes
                    $.ajax({
                        type: "POST",
                        url: "/DataTracker.svc/GetUpdatedComment",
                        data: "{\"Comment_ID\": \"" + G_CommentID + "\",\"Task_ID\": \"" + cardid + "\"}",
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (data) {
                            var res = JSON.parse(data.d);
                            $.each(res, function (index, item) {
                                $(".Comments" + item.CommentID).html("");
                                $(".Comments" + item.CommentID).html(item.Comments);
                            });
                        }
                    });
                    UpdateActivityListItem(cardName, 12, cardid);
                    SendNotificationMailForMessageOnCard(cardNameOfComment, cardid, formail.replace(/\n/g, "<br/>"));
                    G_CommentID = 0;
                    $("#sendComment").removeClass("create_button");
                    $("#sendComment").addClass("create_button_disabled");
                    $("#sendComment").attr('onclick', '');

                    $("#sendComment").show();
                    $("#lblsend2").css('display', 'none');
                },
                error: function (error) {

                    $("#sendComment").addClass("create_button");

                    $("#sendComment").show();
                    $("#lblsend2").css('display', 'none');

                    alert("Some error in Editing comments!");
                }
            });
        }
    }


}

function EditComment(CommentID, card_id) {
    
    G_CommentID = CommentID;
    var Comment_id = ".Comments" + CommentID;
    var Comment_text = $(Comment_id).html();
     
    var modifiedcomment = Comment_text.replace(/m1/g, '"').replace(/m2/g, "'").replace(/m3/g, "\\").replace(/m4/g, "/").replace(/m5/g, "\n").replace(/<br\s*\/?>/gi, '\n');
    $("#popup_list_comment").removeClass();
    $("#sendComment").removeClass("create_button_disabled");
    $("#sendComment").addClass("create_button");
    $("#sendComment").attr('onclick', 'javascript:Popup_Save_Comment()');
    $("#popup_list_comment").val(modifiedcomment.replace('<br>', '\n'));
}

function cardcolorsetdynamically1(taskid) {

    for (var i = 0; i < taskid.length; i++) {
        var taskid1 = taskid[i];
        var id = "#cardcolorset" + taskid1;
        $(id).css('background-color', '');
        $("#set" + taskid1).prop("title", '');
    }
}

function cardcolorsetdynamically(taskid, colorofcard) {

    for (var i = 0; i < taskid.length; i++) {

        var taskid1 = taskid[i];
        var id = "#cardcolorset" + taskid1;
        $(id).css('background-color', colorofcard)
    }

}

function NoOfComments(TasksID) {

    var Comments_number = 0;
    var Attachment_number = 0;

    $.ajax({
        type: "POST",
        url: "/DataTracker.svc/GetNumberOfCommentsByTaskID",
        data: "{\"TaskID\": \"" + TasksID + "\"}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {

            $.each(data.d, function (index, item) {

                Comments_number = item.CommentID;
                Attachment_number = item.NoOfAttachment;
                var id = TasksID;
                var attach_id = TasksID;
                if (Comments_number != 0) {
                    $("#comment" + id).addClass("badge-text2");
                    $("#comment" + id).html(+Comments_number);
                }
                else {
                    $("#comment" + id).removeClass("badge-text2");
                    $("#comment" + id).html("");
                }
                if (Attachment_number != 0) {
                    $("#attachment" + attach_id).addClass("badge-text5");
                    $("#attachment" + attach_id).html(+Attachment_number);
                }
                else {
                    $("#attachment" + attach_id).removeClass("badge-text5");
                    $("#attachment" + attach_id).html("");

                }
            });

        },
        error: function (request, status, error) {
        }
    });
}

function NoOfComments1(NoOfAttachment, NoOfComments, TasksID, cardCommentID, cardAttachmentID) {

    if (NoOfComments != null && cardCommentID != null) {
        if (NoOfComments > 0) {
            $("#comment" + TasksID).addClass("badge-text2");
            $("#comment" + TasksID).html(NoOfComments);
        }
        else {
            $("#comment" + TasksID).removeClass("badge-text2");
            $("#comment" + TasksID).html("");
        }
    }
    else {
        $("#comment" + TasksID).removeClass("badge-text2");
        $("#comment" + TasksID).html("");

    }
    if (NoOfAttachment != null && cardAttachmentID != null) {
        if (NoOfAttachment > 0) {
            $("#attachment" + TasksID).addClass("badge-text5");
            $("#attachment" + TasksID).html(NoOfAttachment);
        }
        else {
            $("#attachment" + TasksID).removeClass("badge-text5");
            $("#attachment" + TasksID).html("");
        }
    }
    else {
        $("#attachment" + TasksID).removeClass("badge-text5");
        $("#attachment" + TasksID).html("");
    }
}

function ShowAddAttachment(NoOfAttachment, TasksID) {
    if (NoOfAttachment != null || cardAttachmentID != null) {
        if (NoOfAttachment > 0) {
            $("#attachment" + TasksID).addClass("badge-text5");
            $("#attachment" + TasksID).html(NoOfAttachment);
        }
        else {
            $("#attachment" + TasksID).removeClass("badge-text5");
            $("#attachment" + TasksID).html("");
        }
    }
    else {
        $("#attachment" + TasksID).removeClass("badge-text5");
        $("#attachment" + TasksID).html("");
    }
}

function CheckCommentFill() {
    var text_length = $('#popup_list_comment').val().length;
    if (text_length >= 1) {
        $("#sendComment").removeClass("create_button_disabled");
        $("#sendComment").addClass("create_button");
        $("#sendComment").attr('onclick', 'javascript:Popup_Save_Comment()');
    }
    else {
        $("#sendComment").removeClass("create_button");
        $("#sendComment").addClass("create_button_disabled");
        $("#sendComment").attr('onclick', '');
    }
}

/********************************************* Code for Create,Remove,Edit Comment********************************************************/

/********************************************* Code for Create,Remove,Edit Members*******************************************************/

function showownermember(card_id) {

    $("#showsubsuser").empty("");
    $.ajax({
        type: "get",
        url: "/DataTracker.svc/showownermembers?cardid=" + card_id,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            $.each(data.d, function (index, item) {

                var str = "";
                str = item.Email;
                var username = item.username;

                username = username != null ? username : '';

                var firstletter = str.charAt(0).toUpperCase();
                var assignedto = item.AssignedTo;
                //Ameeq Changes 16/03/18 for Email Notifications
                $("#showsubsuser").append("<div class='subuser-pop-cont'><a id=userfirstletter href='#' >" + firstletter + "</a><div class='subuser-pop-data'><div class='cardmember_cont'><div class='cardmember_cont_upper'><h2>" + firstletter + "</h2><div class='member_id'><h5>" + username + "</h5><p>" + str + "</p>     </div><div class='clr'></div></div><a onclick='remove_members_from_board(" + card_id + ",\"" + str + "\"," + assignedto + ");'>Remove from Card</a></div></div></div>");


            });
        }
    });
}

function remove_members_from_board(card_id, email, AssignedTo) {

    var r = confirm("Are you sure You want to remove members from this task?");
    if (r == true) {

        var selected = [];
        $(".tile__list  :checked").each(function () {

            selected.push(this.id);

        });
        if (selected.length > 0) {
            var arr3 = JSON.stringify({ card_id: selected });
        }
        else {
            selected.push(card_id);
            var arr3 = JSON.stringify({ card_id: selected })
        }

        $("#showsubsuser").empty("");
        SendNotificationMailForRemoveMemberOnCard(cardNameOfComment, card_id, email);            //Ameeq Changes 16/03/18 for Email Notifications
        $.ajax({
            type: "POST",
            url: "/DataTracker.svc/remove_members_from_board",
            data: "{\"AssignedTo\": \"" + AssignedTo + "\"," + arr3 + "}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {

                var newMember = "in " + cardNameOfComment;
                UpdateActivityListItem(newMember, 16, card_id);
                if (data.d == 0) {

                    for (var i = 0; i < selected.length; i++) {
                        var showmembeid = selected[i];
                        showownermember(showmembeid);
                        GetMembersInCard(showmembeid);
                    }

                    $('.tile__list').find('input[type=checkbox]:checked').removeAttr('checked');
                }
            }
        });
    }
    else {
        return false;
    }
}

function GetFirstLetters(user_name) {
    var arr = user_name.split(" ");
    var short_name;
    if ($.isArray(arr)) {
        short_name = arr[0].charAt(0);
    }
    else {
    }
    return short_name;
}

function showmembers(sender, arg) {
    var membersname = "";
    var teamid = 0;
    teamid = teamid + parseInt($("#teamID").val(), 10);
    $('.member_cont').empty();
    var membersname1 = $('#txtmembers').val();
    if (membersname1.length > 0) {
        membersname = membersname1;
    }
    else {
        membersname = $('#Text1').val();
    }
    if (membersname.length > 0) {
        $.ajax({
            type: "GET",
            url: "/DataTracker.svc/Getmembername?membersname=" + membersname + "&TeamID=" + teamid + "",
            dataType: "json",
            async: false,
            success: function (data) {
                if (membersname1.length > 0) {
                    if (data.d.length > 0) {
                        $.each(data.d, function (index, item) {
                            var str = item.Email;
                            var firstletter = str.charAt(0);
                            var username = item.username;
                            $("#appendmembersright").append("<span onclick='subscribetouser(\"" + firstletter + "\",\"" + str + "\"  ," + item.userid + ",\"" + username + "\");'> <a href='#'><h2>" + firstletter + "</h2><div class='member_id'><p title='" + item.Email + "'>" + item.Email + "</p></div><div class='clr'></div></a></span>")
                            $("#appendmembersright").show();
                        });
                    }
                    else {
                        $("#appendmembersright").append("<span><p id='noResult' style='width:250px;padding: 24px 6px;'>No Result</p><div class='member_id'></div><div class='clr'></div></span>")
                    }
                }
                else {
                    if (data.d.length > 0) {
                        $.each(data.d, function (index, item) {
                            var str = item.Email;
                            var firstletter = str.charAt(0);
                            var username = item.username;
                            $("#appendmembersright1").append("<span onclick='subscribetouser(\"" + firstletter + "\",\"" + str + "\"  ," + item.userid + ",\"" + username + "\");'> <a href='#'><h2>" + firstletter + "</h2><div class='member_id'><p title='" + item.Email + "'>" + item.Email + "</p></div><div class='clr'></div></a></span>")
                            $("#appendmembersright1").show();
                        });
                    }
                    else {
                        $("#appendmembersright1").append("<span><p id='noResult' style='width:250px;padding: 24px 6px;'>No Result</p><div class='member_id'></div><div class='clr'></div></span>")
                    }
                }

            },
            error: function (error) { alert('Error has occurred!'); alert(JSON.stringify(error)) }
        });
    }
}

function showeditmembers(sender, arg) {
    
    var teamid = 0;
    teamid = teamid + parseInt($("#teamID").val(), 10);
    $('.member_cont').empty();
    var oneditmembers = $("#txteditmembers").val();
    if (oneditmembers.length > 0) {
        $.ajax({
            type: "GET",
            url: "/DataTracker.svc/Getmembername?membersname=" + oneditmembers + "&TeamID=" + teamid + "",
            dataType: "json",
            async: false,
            success: function (data) {

                if (data.d.length > 0) {
                    $.each(data.d, function (index, item) {

                        var str = item.Email;
                        var firstletter = str.charAt(0);
                        var username = item.username;
                        $("#appendmembers").append("<span onclick='subscribetouser(\"" + firstletter + "\",\"" + str + "\"  ," + item.userid + ",\"" + username + "\");'> <a href='#'><h2>" + firstletter + "</h2><div class='member_id'><p title='" + item.Email + "'>" + item.Email + "</p></div><div class='clr'></div></a></span>");
                        $("#appendmembers").show();
                    });
                }
                else {
                    $("#appendmembers").append("<span><p id='noResult' style='width:246px;padding: 24px 6px;'>No Result</p><div class='member_id'></div><div class='clr'></div></span>");
                }
            },
            error: function (error) { alert('Error has occurred!'); alert(JSON.stringify(error)) }
        });
    }
}

function BindMembersInCard(TaskID, Email) {

    var id = '#mem_task' + TaskID;
    var showhim = '#showhim' + TaskID;
    if (Email != null) {

        if ($(id).html() != "") {

            $(showhim).find(".showme").css({ "right": "19px", "width": "95px" });

            $(showhim).find("img").hover(function () {
                $(".showhim").find(".showme").css("display", "block !important");

            }, function () {
                $(this).find(".badge-text4").css({ "display": "none" });

            });
            var str = "";
            str = Email;
            var firstletter = str.charAt(0);
            var active = 0;
            firstletter = firstletter.toUpperCase();
            if (active == 1) {
                $(id).append("<p class='badge-text4' style='background-color: green;' title='" + Email + "'>" + firstletter + "</p>");
                $(showhim).find("img").css("display", "block");
                $(showhim).find("img").attr("src", "Images/user_grn.png");
            }
            else {
                $(id).append("<p class='badge-text4' title='" + Email + "'>" + firstletter + "</p>");
                $(showhim).find("img").css("display", "block");
            }
        }
        else {
            var str = "";
            str = Email;
            var firstletter = str.charAt(0);
            var active = 0;
            firstletter = firstletter.toUpperCase();
            if (active == 1) {
                $(showhim).find("img").css("display", "none");
                $(id).append("<p class='badge-text7' style='background-color: green; margin-right: 5px;' title='" + Email + "'>" + firstletter + "</p>");
                $(showhim).find(".showme").css({ "right": "0px", "width": "95px" });

            }
            else {
                $(id).append("<p class='badge-text7' style='margin-right: 5px;' title='" + Email + "'>" + firstletter + "</p>");

                $(showhim).find("img").css("display", "none");
                $(showhim).find(".showme").css({ "right": "0px", "width": "95px" });
            }
        }
    }

}

function GetMembersInCard(TaskID) {

    var id = '#mem_task' + TaskID;
    var showhim = '#showhim' + TaskID;
    $(id).empty();
    $.ajax({
        type: "GET",
        url: "/DataTracker.svc/GetMembersInCardDetail?Task_ID=" + TaskID + "",
        dataType: "json",
        async: true,
        success: function (data) {

            if (data.d.length > 1) {

                $(showhim).find(".showme").css({ "right": "19px", "width": "95px" });

                $(showhim).find("img").hover(function () {
                    $(".showhim").find(".showme").css("display", "block !important");

                }, function () {
                    $(this).find(".badge-text4").css({ "display": "none" });

                });

                $.each(data.d, function (index, item) {

                    var str = "";
                    str = item.Email;
                    var firstletter = str.charAt(0);
                    var active = item.Active;
                    firstletter = firstletter.toUpperCase();
                    if (active == 1) {
                        $(id).append("<p class='badge-text4' style='background-color: green;' title='" + item.Email + "'>" + firstletter + "</p>");
                        $(showhim).find("img").css("display", "block");
                        $(showhim).find("img").attr("src", "Images/user_grn.png");
                    }
                    else {
                        $(id).append("<p class='badge-text4' title='" + item.Email + "'>" + firstletter + "</p>");
                        $(showhim).find("img").css("display", "block");

                    }

                });

            }
            else {
                $.each(data.d, function (index, item) {

                    var str = "";
                    str = item.Email;
                    var firstletter = str.charAt(0);

                    var active = item.Active;
                    firstletter = firstletter.toUpperCase();
                    if (active == 1) {
                        $(showhim).find("img").css("display", "none");
                        $(id).append("<p class='badge-text7' style='background-color: green; margin-right: 5px;' title='" + item.Email + "'>" + firstletter + "</p>");
                        $(showhim).find(".showme").css({ "right": "0px", "width": "95px" });

                    }
                    else {
                        $(id).append("<p class='badge-text7' style='margin-right: 5px;' title='" + item.Email + "'>" + firstletter + "</p>");

                        $(showhim).find("img").css("display", "none");
                        $(showhim).find(".showme").css({ "right": "0px", "width": "95px" });
                    }

                });
            }


        },
        error: function (error) {
        }
    });
}

function GetMembersInCard1(members, TaskID) {
    var id = '#mem_task' + TaskID;
    var showhim = '#showhim' + TaskID;
    $(id).empty();

    if (members.length > 1) {

        $(showhim).find(".showme").css({ "right": "19px", "width": "95px" });

        $(showhim).find("img").hover(function () {
            $(".showhim").find(".showme").css("display", "block !important");

        }, function () {
            $(this).find(".badge-text4").css({ "display": "none" });

        });

        $.each(members, function (index, item) {

            var str = "";
            str = item.Email;
            var firstletter = str.charAt(0);
            var active = item.Active;
            firstletter = firstletter.toUpperCase();
            if (active == 1) {
                $(id).append("<p class='badge-text4' style='background-color: green;' title='" + item.Email + "'>" + firstletter + "</p>");
                $(showhim).find("img").css("display", "block");
                $(showhim).find("img").attr("src", "Images/user_grn.png");
            }
            else {
                $(id).append("<p class='badge-text4' title='" + item.Email + "'>" + firstletter + "</p>");
                $(showhim).find("img").css("display", "block");

            }

        });
    }
    else {
        $.each(members, function (index, item) {

            var str = "";
            str = item.Email;
            var firstletter = str.charAt(0);

            var active = item.Active;
            firstletter = firstletter.toUpperCase();
            if (active == 1) {
                $(showhim).find("img").css("display", "none");
                $(id).append("<p class='badge-text7' style='background-color: green; margin-right: 5px;' title='" + item.Email + "'>" + firstletter + "</p>");
                $(showhim).find(".showme").css({ "right": "0px", "width": "95px" });


            }
            else {
                $(id).append("<p class='badge-text7' style='margin-right: 5px;' title='" + item.Email + "'>" + firstletter + "</p>");

                $(showhim).find("img").css("display", "none");
                $(showhim).find(".showme").css({ "right": "0px", "width": "95px" });
            }

        });
    }
}

function subscribetouser(firstletter, email, assignedto, username) {

    var selected = [];
    var membersname = $('#Text1').val();
    var cardid = $("#hide_cardid").val();
    if (membersname.length > 0) {
        $(".tile__list  :checked").each(function () {

            selected.push(this.id);

        });
        if (selected.length > 0) {
            var arr2 = JSON.stringify({ cardid: selected });
        }

    }
    else {
        selected.push(cardid);
        var arr2 = JSON.stringify({ cardid: selected });
    }
    firstletter = firstletter.toUpperCase();
    $.ajax({
        type: "POST",
        url: "/DataTracker.svc/assigneduserfortask",
        data: "{\"assignedto\": \"" + assignedto + "\"," + arr2 + "}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {

            var newMember = "in " + cardNameOfComment;

            if (data.d != 0) {
                SendNotificationMailForAddMemberOnCard(cardNameOfComment, cardid, email);
                username = username != (null) ? username : '';
                $("#showsubsuser").append("<div class='subuser-pop-cont'><a id=userfirstletter href='#' >" + firstletter + "</a><div class='subuser-pop-data'><div class='cardmember_cont'><div class='cardmember_cont_upper'><h2>" + firstletter + "</h2><div class='member_id'><h5>" + username + "</h5><p>" + email + "</p>     </div><div class='clr'></div></div><a onclick='remove_members_from_board(" + cardid + ",\"" + email + "\"," + assignedto + ");'>Remove from Card</a></div></div></div>");

                for (var i = 0; i < selected.length; i++) {
                    var showmembeid = selected[i];

                    GetMembersInCard(showmembeid);
                    //
                    AddMemeberToTask(showmembeid);
                    //
                    UpdateActivityListItem(newMember, 15, showmembeid);
                }

                $('#Text1').val("");
                $('#appendmembersright1').empty();
            }
            else {
                alert("User already assigned for this task.");
            }
        }
    });
}

/********************************************* Code for Create,Remove,Edit Members*******************************************************/

/********************************************* Code for change and save Accessibility***************************************************/

function SaveAccessility(Modifier) {
    $.ajax({
        type: "GET",
        contentType: "application/json; charset=utf-8",
        url: "/DataTracker.svc/GetAccessModifier?Access=" + Modifier + "&BoardID=" + BoardID + "",
        dataType: "json",
        success: function (data) {
        }
    })
}

function ChangeAccessilityofboard(AccessModifier) {

    var existing_value_accessmodifier = $('#AccessModifier').html();
    var existing_value_accessmodifier = existing_value_accessmodifier.substring(1);
    if (AccessModifier == "Public") {
        $("#board_public").text("Public");
        $("#board_public").append("&#10004;");
        $("#board_public1").text("Public");
        $("#board_public1").append("&#10004;");
        $("#board_cont_team").append("Team");
        $('#AccessModifier').html("&#x2606;" + "Public");
        $('#AccessModifier1').html("&#x2606;" + "Public");
        $("#board_private").text("Private");
        $("#board_private1").text("Private");
        SaveAccessility("Public");
        UpdateActivityListItem(oldboardname + " is " + AccessModifier + ".", 22);
    }
    else {
        $("#board_private").text("Private");
        $("#board_private").append("&#10004;");
        $("#board_private1").text("Private");
        $("#board_private1").append("&#10004;");
        $('#AccessModifier').html("&#x2606;" + "Private");
        $('#AccessModifier1').html("&#x2606;" + "Private");
        $("#board_public").text("Public");
        $("#board_public1").text("Public");
        $("#board_cont_team").append("Team");
        SaveAccessility("Private");
        UpdateActivityListItem(oldboardname + " is " + AccessModifier + ".", 23);
    }
}

function GetBoardNameAndAccessModifier(id, sprintId) {
    var isarchive = "0";
    var iscomplete = "0";


    var cuserid = $('#cuserid').val();
    var usertypeid = $('#usertypeid').val();

    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "/DataTracker.svc/GetBoardNameAndAccessModifier",
        data: "{\"B_id\": \"" + id + "\"}",
        dataType: "json",
        success: function (data) {

            if (data.d.length > 0) {
                GetTotalTimeEstimate(sprintId);

                if (data.d[0].Teamid != 0) {
                    $("#TeamName").show();
                    $("#teamID").val(data.d[0].Teamid);
                    $("#TeamName").html("&#x2600;" + data.d[0].TeamName);
                    $("#TeamName1").show();
                    $("#TeamName1").html("&#x2600;" + data.d[0].TeamName);
                    $("#teamnameforheader").text(data.d[0].TeamName);
                    $("#teamnameforheader1").text(data.d[0].TeamName);
                    $("#teamlabel").hide();
                    $("#change2").hide();
                    teamIdForOwner = data.d[0].Teamid;
                    bindddlforchangeteam(data.d[0].Teamid);

                    var selectedText = data.d[0].TeamName;
                    $('#ddlteamtoboard option').map(function () {
                        if ($(this).text() == selectedText) return this;
                    }).attr('selected', 'selected');

                }
                else {

                    $("#TeamName").hide();
                    $("#TeamName1").hide();
                    $("#teamlabel").show();
                    $("#teamID").val('0');
                    $("#ddlteamtoboard").val('0');
                    $("#ddlteamtoboard1").val('0');
                    $("#ddlteamtoboard2").val('0');
                    $("#ddlteamtoboard3").val('0');

                    bindddlforchangeteam(data.d[0].Teamid);
                }
                tempForCreatedBy = data.d[0].CreatedBy;
                globalboardname = data.d[0].BoardName;
                if (data.d[0].Isfavourite == 'true') {
                    $('#favoriteID').val('true');
                    $('#FavoriteModifier').html("&#9829;");
                }
                else {
                    $('#favoriteID').val('false');
                    $('#FavoriteModifier').html("&#9825;");
                }
                if (data.d[0].CreatedBy == cuserid) {
                    $('#ArchiveBoard').css('display', 'block');

                }
                if (data.d[0].IsCompleted == 1) {
                    $('#ArchiveBoard').css('display', 'none');
                }
                var ownerid = $('#ownerID').val();
                if (cuserid == ownerid || usertypeid == 12 || usertypeid == 14) {
                    if (data.d[0].IsEnabledBoardComplete == true) {
                        $('#ckCompleteBoardId').attr('checked', true);
                        $('#progressbar').css('display', 'block');
                    }
                    else { $('#progressbar').css('display', 'none'); }
                }

                var txtEstimated = data.d[0].EstimatedHours;
                var txtval = txtEstimated.split('.');

                if (txtval[0] == 0) {
                    $('#txtEstimated').val("");
                    estboardtime1 = "";
                }
                else {
                    $('#txtEstimated').val(txtval[0]);
                    estboardtime1 = txtval[0];
                }



                var estimatedval = data.d[0].EstimatedTimeForCard;
                var esval1 = estimatedval.split('.');

                if (esval1[0] == 0) {
                    $('#txtCardEstimatedTime').val("0");
                }
                else { $('#txtCardEstimatedTime').val(esval1[0]); }

                var estime = esval1[1];
                estboardcardtime1 = esval1[0];

                if (estime == "00") {
                    $('#txtCardEstimatedminute').val("");
                }

                else {
                    $('#txtCardEstimatedminute').val(estime);
                    estboardcardtime2 = estime;
                }
                if (data.d[0].IsArchived == true) {
                    $("#DivSchedule").css({ "background": "#808080", "pointer-events": "none", "cursor": "default", "opacity": "0.5" });
                }


                curboardduedate = data.d[0].DueDate;

                var date = new Date(data.d[0].DueDate);
                var month = date.getMonth() + 1 < 10 ? '0' + (date.getMonth() + 1) : date.getMonth() + 1;
                var day = date.getDate() < 10 ? '0' + date.getDate() : date.getDate();
                $("#boardduedate").val(date.getFullYear() + '-' + month + '-' + day);


                $("#boardduedatedays").empty();
                if (data.d[0].daysleft != 'Default') {
                    var checknumber = parseInt(data.d[0].daysleft);
                    if (checknumber < 0) {
                        var number = Math.abs(checknumber);
                        if (number == 1) {
                            $("#boardduedatedays").append("<div class='datalist1'><p>" + number + "</p></div>");

                        }
                        else {
                            $("#boardduedatedays").append("<div class='datalist1'><p>" + number + "</p></div>");
                        }

                    }
                    else {
                        if (checknumber == 1) {
                            $("#boardduedatedays").append("<div class='datalist'><p>" + checknumber + "</p></div>");
                        }
                        else {
                            $("#boardduedatedays").append("<div class='datalist'><p>" + checknumber + "</p></div>");
                        }

                    }
                }
                checkdays(data.d[0].duedate, data.d[0].daysleft, spid);
                $.ajax({
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    url: "/DataTracker.svc/GetBoardIsarchived",
                    data: "{\"B_id\": \"" + id + "\"}",
                    dataType: "json",
                    async: false,
                    success: function (data) {
                        isarchive = data.d[0].IsArchived;
                        iscomplete = data.d[0].isCompleted;

                    }
                });

                $.each(data.d, function (index, item) {
                    $('#Archivelist').show();
                    $('#boardID').val(item.BoardID);
                    $('#BoardName').html(item.BoardName + "<img src='/Images/pencil5.png' alt='Edit' title='Click for edit.' />");
                    $('#hboardname').html(item.BoardName + "<img src='/Images/pencil5.png' alt='Edit' title='Click for edit.' />");
                    boardnamerestrict = item.BoardName;
                    if (item.IsPublic == 2) {
                        $('#AccessModifier').html("&#x2606;Public");
                        $('#AccessModifier1').html("&#x2606;Public");
                    }
                    else if (item.IsPublic == 1) {
                        $('#AccessModifier').html("&#x2606;Private");
                        $('#AccessModifier1').html("&#x2606;Private");
                    }
                    if (isarchive == "1") {
                        $("#Archivelist").css("display", "none");

                    }
                    if (iscomplete == "1") {
                        $("#multi").empty();
                        $("#archiveboardcomplete").css("display", "block");
                        $("#Archivelist").css("display", "none");
                    }

                    GetRecentBoards();
                    MyBoardsList();

                });
                GetManagerName();
                GetClientName();
                ShowPopupcolor();
                GetOwnerName();
                ArchieveBoardlist();
            }
        },
        error: function (error) {
            alert("Some error in getting boardname and access modidfier!");
        }
    })
}

function ChangeSprintDuedate(sprintid, sprintduedate) {

    var boardid = $("#boardID").val();
    $.ajax({
        type: "POST",
        url: "/DataTracker.svc/ChangeSprintDuedate",
        data: JSON.stringify({ boardid: boardid, sprintduedate: sprintduedate, sprintid: sprintid }),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            if (data.d != "exception") {
                if (data.d == "invalid") {
                    alert('Access Denied');
                    var date = new Date(curboardduedate);
                    var month = date.getMonth() + 1 < 10 ? '0' + (date.getMonth() + 1) : date.getMonth() + 1;
                    var day = date.getDate() < 10 ? '0' + date.getDate() : date.getDate();
                    //$("#boardduedate").val(date.getFullYear() + '-' + month + '-' + day);

                }
                else {
                    $("#boardduedatedays_" + sprintid).empty();

                    var checknumber = parseInt(data.d);
                    if (checknumber < 0) {
                        var number = Math.abs(checknumber);
                        if (number == 1) {
                            $("#boardduedatedays_" + sprintid).append("<div class='datalist1'><p>" + number + "</p></div>");
                        }
                        else {
                            $("#boardduedatedays_" + sprintid).append("<div class='datalist'><p>" + number + "</p></div>");
                        }

                    }
                    else {
                        if (checknumber == 1) {
                            $("#boardduedatedays_" + sprintid).append("<div class='datalist1'><p>" + checknumber + "</p></div>");
                        }
                        else {
                            $("#boardduedatedays_" + sprintid).append("<div class='datalist'><p>" + checknumber + "</p></div>");
                        }
                    }

                    //checkdays(boardduedate);
                }
            }
            else {
                //$("#boardduedate").val(curboardduedate);
            }
        }
    });


}

function ChangeBoardDuedate() {
    var boardid = $("#boardID").val();
    var boardduedate = $("#boardduedate").val();
    $.ajax({
        type: "POST",
        url: "/DataTracker.svc/ChangeBoardDuedate",
        data: JSON.stringify({ boardid: boardid, boardduedate: boardduedate }),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            if (data.d != "exception") {
                if (data.d == "invalid") {
                    alert('Access Denied');
                    var date = new Date(curboardduedate);
                    var month = date.getMonth() + 1 < 10 ? '0' + (date.getMonth() + 1) : date.getMonth() + 1;
                    var day = date.getDate() < 10 ? '0' + date.getDate() : date.getDate();
                    $("#boardduedate").val(date.getFullYear() + '-' + month + '-' + day);

                }
                else {
                    $("#boardduedatedays").empty();

                    var checknumber = parseInt(data.d);
                    if (checknumber < 0) {
                        var number = Math.abs(checknumber);
                        if (number == 1) {
                            $("#boardduedatedays").append("<div class='datalist1'><p>" + number + "</p></div>");
                        }
                        else {
                            $("#boardduedatedays").append("<div class='datalist1'><p>" + number + "</p></div>");
                        }

                    }
                    else {
                        if (checknumber == 1) {
                            $("#boardduedatedays").append("<div class='datalist'><p>" + checknumber + "</p></div>");
                        }
                        else {
                            $("#boardduedatedays").append("<div class='datalist'><p>" + checknumber + "</p></div>");
                        }
                    }

                    // checkdays(boardduedate);
                }
            }
            else {
                $("#boardduedate").val(curboardduedate);
            }
        }
    });


}

function checkdays(duedate, SpId) {
    var cur = new Date();
    var date = new Date(duedate);
    //date = date.setDate(date.getDate() + 1);
    var curyear = cur.getFullYear();
    var curmonth = cur.getMonth() + 1;
    var curday = cur.getDate();
    var dateyear = date.getFullYear();
    var datemonth = date.getMonth() + 1;
    var dateday = date.getDate();
    var daysleft = 0;
    daysleft = (cur - date) / (1000 * 60 * 60 * 24);
    daysleft = Math.round(daysleft);
    $("#boardduedatedays_" + SpId).empty();
    if (duedate != null) {

        var checknumber = parseInt(daysleft);
        if (checknumber < 0) {
            var number = Math.abs(checknumber);
            number = number + 1;
            if (number == 1) {
                $("#boardduedatedays_" + SpId).append("<div class='datalist1'><p>" + number + "</p></div>");
            }
            else {
                if (number > 365) {
                    number = number / 365;
                    $("#boardduedatedays_" + SpId).append("<div class='datalist'><p>" + number + "</p></div>");
                }
                else {
                    $("#boardduedatedays_" + SpId).append("<div class='datalist'><p>" + number + "</p></div>");
                }
            }
        }
        else {
            checknumber = checknumber + 1;
            if (checknumber == 1) {
                $("#boardduedatedays_" + SpId).append("<div class='datalist1'><p>" + checknumber + "</p></div>");
            }
            else {
                $("#boardduedatedays_" + SpId).append("<div class='datalist'><p>" + checknumber + "</p></div>");
            }
        }
    }
    else {
        $("#boardduedatedays_" + SpId).append("<div class='datalist'><p>0</p></div>");
    }
    if (curyear <= dateyear) {
        if (curmonth <= datemonth) {
            if (curmonth == datemonth) {
                if (curday <= dateday) {
                    $("#boardduedatetime").removeClass('.datalist1');

                }
                else {
                    $("#boardduedatetime").addClass('.datalist1');
                }
            }
            else {
                $("#boardduedatetime").addClass('.datalist1');
            }
        }
        else {
            $("#boardduedatetime").addClass('.datalist1');
        }
    }
    else {
        $("#boardduedatetime").addClass('.datalist1');
    }
}

function ChangeAccessility(AccessModifier) {

    document.getElementById("BoardAccessModifier").innerHTML = AccessModifier;
    if (AccessModifier == "Public") {
        document.getElementById("board_cont_public").innerHTML = "";
        $("#board_cont_public").append("Public &#10004;");
        document.getElementById("board_cont_private").innerHTML = "";
        $("#board_cont_private").append("Private");
    }
    else {
        document.getElementById("board_cont_private").innerHTML = "";
        $("#board_cont_private").append("Private &#10004;");
        document.getElementById("board_cont_public").innerHTML = "";
        $("#board_cont_public").append("Public");
    }
}

function AccessilityBoard() {
    var AccessModifier = $("#AccessModifier").text();
    AccessModifier = AccessModifier.substr(1, AccessModifier.len);
    if (AccessModifier == "Public") {
        document.getElementById("board_public").innerHTML = "";
        $("#board_public").append("Public &#10004;");
        document.getElementById("board_public1").innerHTML = "";
        $("#board_public1").append("Public &#10004;");
        document.getElementById("board_private").innerHTML = "";
        $("#board_private").append("Private");
        document.getElementById("board_private1").innerHTML = "";
        $("#board_private1").append("Private");
    }
    else {
        document.getElementById("board_private").innerHTML = "";
        $("#board_private").append("Private &#10004;");
        document.getElementById("board_public").innerHTML = "";
        $("#board_public").append("Public");
        document.getElementById("board_private1").innerHTML = "";
        $("#board_private1").append("Private &#10004;");
        document.getElementById("board_public1").innerHTML = "";
        $("#board_public1").append("Public");
    }
}

/********************************************* Code for change and save Accessibility***************************************************/

/*********************************************code for due date************************************************************************/

function createtimeforupdate() {
    var h11 = 0, h22 = 0, m11 = 0, m22 = 0, s11 = 0, s22 = 0;
    if ($("#hr11").val() != "") {
        h11 = $("#hr11").val();
    }
    if ($("#hr22").val() != "") {
        h22 = $("#hr22").val();
    }
    if ($("#mm11").val() != "") {
        var m11 = $("#mm11").val();
    }
    if ($("#mm22").val() != "") {
        var m22 = $("#mm22").val();
    }
    if ($("#ss11").val() != "") {
        var s11 = $("#ss11").val();
    }
    if ($("#ss22").val() != "") {
        var s21 = $("#ss22").val();
    }
    var Time1 = h11 + h22 + ":" + m11 + m22 + ":" + s11 + s22;
    return Time1;
}

function ChangeDuedate() {

    var selected = [];
    var Duedate = "";
    var membersname = $('#Text2').val();
    var cardid = $("#hide_cardid").val();

    if (membersname.length > 0) {
        $(".tile__list  :checked").each(function () {

            selected.push(this.id);

        });
        if (selected.length > 0) {
            var arr4 = JSON.stringify({ cid: selected });
            Duedate = $('#Text2').val();
        }

    }
    else {
        selected.push(cardid);
        var arr4 = JSON.stringify({ cid: selected });
        Duedate = $('#duedate').val();
    }
    if ($('#duedate').val() != "" || membersname != "") {

        var Duetime = "00:00"
        var Card_id_with_hash = "#" + cardid + " .badge-text";

        var dueinformation = Duedate + " " + Duetime;
        var today = new Date();
        var someday = new Date(dueinformation);
        var bg_color = "";
        if (someday > today) {
            var hours = Math.round((someday - today) / 3600000);
            if (hours < 24) {
                texts = "This card is due in less than " + hours + "hours.";
                bg_color = '#e6b800';
            }
            else {
                texts = "This card is due later.";
                bg_color = '#8cd98c';
            }
        } else {
            texts = "This card has been overdue.";
            bg_color = '#ff8080';
        }
        formatDate(Duedate);
        var dueinformation_with_symbol = "&#x1f550; " + formatDate(Duedate);
        $.ajax({
            type: "POST",
            url: "/DataTracker.svc/ChangeDuedate",
            data: "{\"dueinformation\": \"" + dueinformation + "\"," + arr4 + "}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {

                Bindduedate(cardid);
                $('#lblduedate').show();
                $(".webui-popover").removeClass('webui-popover bottom-right in');

                OnDueDateChange($("#hide_cardid").val());

                $('#Text2').val("");
                $('#appendmembersright1').empty();
            },
            error: function (err) {
            }
        })
    }
    else {
        $('#duedate').focus();
        clearfields();
    }
}

function createtime() {
    var h1 = 0, h2 = 0, m1 = 0, m2 = 0, s1 = 0, s2 = 0;
    if ($("#hr1").val() != "") {
        h1 = $("#hr1").val();
    }
    if ($("#hr2").val() != "") {
        h2 = $("#hr2").val();
    }
    if ($("#mm1").val() != "") {
        var m1 = $("#mm1").val();
    }
    if ($("#mm2").val() != "") {
        var m2 = $("#mm2").val();
    }
    if ($("#ss1").val() != "") {
        var s1 = $("#ss1").val();
    }
    if ($("#ss2").val() != "") {
        var s2 = $("#ss2").val();
    }
    var Time = h1 + h2 + ":" + m1 + m2 + ":" + s1 + s2;
    return Time;
}

function ChangeDuedatefromdisplay() {
    if ($('#duedatefrm').val() != "") {
        var Ddate = $('#duedatefrm').val();
        var Dtime = createtime();
        var cardid = $("#hide_cardid").val();
        var Card_id_with_hash = "#" + cardid + " .badge-text";
        var dueinformation = Ddate + " " + Dtime;
        var today = new Date();
        var someday = new Date(dueinformation);
        var bg_color = "";
        if (someday > today) {
            var hours = Math.round((someday - today) / 3600000);
            if (hours < 24) {
                texts = "This card is due in less than " + hours + "hours.";
                bg_color = '#e6b800';
            }
            else {
                texts = "This card is due later.";
                bg_color = '#8cd98c';
            }
        } else {
            texts = "This card has been overdue.";
            bg_color = '#ff8080';
        }
        formatDate(Ddate);
        var dueinformation_with_symbol = "&#x1f550; " + formatDate(Ddate);
        $.ajax({
            type: "GET",
            contenttype: "application/json; charset=utf-8",
            url: "/datatracker.svc/ChangeDuedate?dueinformation=" + dueinformation + "&cid=" + cardid + "",
            datatype: "json",
            success: function (data) {
                var dafrcheck = data.d;
                dafrcheck = dafrcheck.slice(4, 8);
                if (dafrcheck == '1990') {
                    $('#lblduedate').hide();
                    $('#btnduedate').text('Save');
                }
                else {
                    Bindduedate(cardid);
                    $('#lblduedate').show();
                    $('#btnduedate').text('Update');
                    $(".webui-popover").removeClass('webui-popover bottom-right in');
                    $(Card_id_with_hash).html(dueinformation_with_symbol);
                    $(Card_id_with_hash).css('background-color', bg_color);
                    OnDueDateChange($("#hide_cardid").val());
                }
            }
        })
    }
    else {
        $('#duedatefrm').focus();
        clearfields();
    }
}

function Bindduedate(cardid) {
    $.ajax({
        type: "GET",
        contenttype: "application/json; charset=utf-8",
        url: "/datatracker.svc/selectduedate?cardid=" + cardid + "",

        async: false,
        success: function (data) {
            var datefrcheck = data.d;
            datefrcheck = datefrcheck.slice(4, 8);
            if (datefrcheck == '1990' || datefrcheck == "") {
                $('#lblduedate').hide();
                $('#btnduedate').text('Save');
                $(".webui-popover").removeClass('webui-popover bottom-right in');
                clearfields();
                $('#duedate').val('');
                $('#duedatefrm').val('');
                $('#removebtn').addClass('hidedata');
            }
            else {
                var dd = data.d;
                var dt = data.d;
                var dfulltime = data.d;
                var dlable1 = dd.split(" ");
                var dlable = dlable1[0];
                var datef = new Date(dlable).toLocaleDateString('en-GB', {
                    day: 'numeric',
                    month: 'short',
                    year: 'numeric'
                }).split(' ').join(" ");
                var tlable1 = dlable1[1].split(" ");
                var tlable = tlable1[0];
                var Duetime71 = tlable.split(":");
                var Duetime7 = ((Duetime71[0] <= 9) ? "0" + Duetime71[0] : Duetime71[0]) + ":" + Duetime71[1];
                var Duetime72 = ((Duetime71[0] <= 9) ? "0" + Duetime71[0] : Duetime71[0]) + ":" + Duetime71[1] + ":" + Duetime71[2];
                var dfr = datef;

                $('#btnduedate').text('Update');
                $('#lblduedate').show();
                var dlable = dlable.split("/")

                var dlb = ($.trim(dlable[2]) + "-" + ((dlable[0] <= 9) ? "0" + dlable[0] : dlable[0]) + "-" + ((dlable[1] <= 9) ? "0" + dlable[1] : dlable[1]));

                $('#duedatefrm').val(dlb);
                $('#duedate').val(dlb);
                Storeduedate(Duetime72);
                Sduedate(Duetime72);
                $('#removebtn').removeClass('hidedata');
                var now = new Date();
                var d = new Date();
                var ts1 = new Date(datef);
                var delta = now.getTime() - ts1.getTime();
                if (delta > 0) {
                    $('#lblduedate').removeClass('duedatepending');
                    $('#lblduedate').removeClass('duedateisinlimit');
                    $('#lblduedate').addClass('duedateexpire');
                    $('#lblduedate').html("Due Date:" + " " + dfr + "  " + "(past due)");
                }
                if (delta < 0) {
                    $('#lblduedate').removeClass('duedateexpire');
                    $('#lblduedate').removeClass('duedateisinlimit');
                    $('#lblduedate').addClass('duedatepending');
                    $('#lblduedate').html("Due Date:" + " " + dfr);
                }
                if ((d.getDate() == dlable[1] && (d.getMonth() + 1) == dlable[0] && (d.getFullYear()) == dlable[2])) {

                    $('#lblduedate').removeClass('duedateexpire');
                    $('#lblduedate').removeClass('duedatepending');
                    $('#lblduedate').addClass('duedateisinlimit');
                    $('#lblduedate').html("Due Date:" + " " + dfr + "  " + "(due soon)");
                }
            }
        }
    })
}

function Storeduedate(timestring) {
    var time1 = timestring.split(":");
    var h1 = time1[0].substring(0, 1);
    $("#hr1").val(h1[0])
    var h2 = time1[0].substring(1, 2);
    $("#hr2").val(h2[0]);
    var m1 = time1[1].substring(0, 1);
    $("#mm1").val(m1[0]);
    var m2 = time1[1].substring(1, 2);
    $("#mm2").val(m2[0])
    var s1 = time1[2].substring(0, 1);
    $("#ss1").val(s1[0])
    var s2 = time1[2].substring(1, 2);
    $("#ss2").val(s2[0])
}

function Sduedate(timestring) {
    var time1 = timestring.split(":");
    var h1 = time1[0].substring(0, 1);
    $("#hr11").val(h1[0])
    var h2 = time1[0].substring(1, 2);
    $("#hr22").val(h2[0]);
    var m1 = time1[1].substring(0, 1);
    $("#mm11").val(m1[0]);
    var m2 = time1[1].substring(1, 2);
    $("#mm22").val(m2[0])
    var s1 = time1[2].substring(0, 1);
    $("#ss11").val(s1[0])
    var s2 = time1[2].substring(1, 2);
    $("#ss22").val(s2[0])
}

function RemoveDueDate() {
    var cardid = $("#hide_cardid").val();

    var r = confirm("Are you sure you want to Delete?");
    var Card_id_with_hash = "#" + cardid + " .badge-text";
    if (r == true) {

        $(Card_id_with_hash).html("");

        var removeduwdatecardid = [];
        $(".tile__list  :checked").each(function () {
            removeduwdatecardid.push(this.id);
        });
        if (removeduwdatecardid.length > 0) {
        }
        else {
            removeduwdatecardid.push(cardid);
        }

        $.ajax({
            type: "POST",
            url: "/DataTracker.svc/RemoveDueDate",
            data: JSON.stringify({ cardid: removeduwdatecardid }),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                for (var i = 0; i < removeduwdatecardid.length; i++) {
                    var cardid = removeduwdatecardid[i];
                    Bindduedate(cardid);
                }

                $(".webui-popover").removeClass('webui-popover bottom-right in');
                clearfields();
            }
        })
    }
    else {
        return false;
    }
}

function clearfields() {
    $("#hr1").val('');
    $("#hr2").val('');
    $("#mm1").val('');
    $("#mm2").val('');
    $("#ss1").val('');
    $("#ss2").val('');
    $("#hr11").val('');
    $("#hr22").val('');
    $("#mm11").val('');
    $("#mm22").val('');
    $("#ss11").val('');
    $("#ss22").val('');
    $('#duedatefrm').val('');
    $('#duedate').val('');
}

function IsNumeric(e) {
    var keyCode = e.which ? e.which : e.keyCode;
    if ((keyCode >= 48 && keyCode <= 57 || keyCode == 8 || keyCode == 46 || keyCode == 9)) {
        return true;
    }
    else {
        return false;
    }
}

function IsNumericHourSec(e) {
    var hours1 = $("#hr11").val();
    if (hours1 == 2) {
        var keyCode = e.which ? e.which : e.keyCode;
        if ((keyCode >= 48 && keyCode <= 51 || keyCode == 8 || keyCode == 46 || keyCode == 9)) {
            return true;
        }
        else {
            return false;
        }
    }
    if (hours1 < 2) {
        var keyCode = e.which ? e.which : e.keyCode;
        if ((keyCode >= 48 && keyCode <= 57 || keyCode == 8 || keyCode == 46 || keyCode == 9)) {
            return true;
        }
        else {
            return false;
        }
    }
}

function IsNumericHourSecDisplay(e) {
    var hours1 = $("#hr1").val();
    if (hours1 == 2) {
        var keyCode = e.which ? e.which : e.keyCode;
        if ((keyCode >= 48 && keyCode <= 51 || keyCode == 8 || keyCode == 46 || keyCode == 9)) {
            return true;
        }
        else {
            return false;
        }
    }
    if (hours1 < 2) {
        var keyCode = e.which ? e.which : e.keyCode;
        if ((keyCode >= 48 && keyCode <= 57 || keyCode == 8 || keyCode == 46 || keyCode == 9)) {
            return true;
        }
        else {
            return false;
        }
    }
}

function IsNumerichour(e) {
    var keyCode = e.which ? e.which : e.keyCode;
    if ((keyCode >= 48 && keyCode <= 50 || keyCode == 8 || keyCode == 9 || keyCode == 46)) {
        return true;
    }
    else {
        return false;
    }
}

function IsNumericmintsandSec(e) {
    var keyCode = e.which ? e.which : e.keyCode;
    if ((keyCode >= 48 && keyCode <= 53 || keyCode == 8 || keyCode == 46 || keyCode == 9)) {
        return true;
    }
    else {
        return false;
    }
}

function formatDate(date1) {
    var date = new Date(date1);
    var day = date.getDate();
    if (day < 10) {
        day = "0" + day;
    }
    var month = new Array();
    month[0] = " Jan ";
    month[1] = " Feb ";
    month[2] = " Mar ";
    month[3] = " Apr ";
    month[4] = " May ";
    month[5] = " Jun ";
    month[6] = " Jul ";
    month[7] = " Aug ";
    month[8] = " Sep ";
    month[9] = " Oct ";
    month[10] = " Nov ";
    month[11] = " Dec ";
    var month_in_words = month[date.getMonth()];
    var year = date.getFullYear();
    return day + month_in_words + year;
}

/*********************************************code for due date************************************************************************/

/*********************************************code for Other Functions*****************************************************************/

function ShowModal(card_id) {

    //
    $("#SendtoboardID").addClass('archivedisplay');
    $("#ArchiveID").removeClass('archivedisplay');
    $('#popup_list_comment').val("");
    var cardforid = "#" + card_id;
    var card_name = $(cardforid).find(".cards .cRdNmE").text();
    cardNameOfComment = card_name;
    var list_name = $(cardforid).parents("span").find(".tile__name").text();

    $("#hide_cardid").val(card_id);
    $("#popup_cardname").html(card_name + "<img src='/Images/pencil5.png' alt='Edit' title='Click for edit.' />");
    $("#popup_cards_listname").html("'" + list_name + "'");
    GetDescription(card_id);
    Bindduedate(card_id);
    bindAttachmentonPop(card_id, cuid);
    showownermember(card_id);
    showboardcomment(card_id);
    var contentRequested = $(".cards").attr("name");
    if (contentRequested == "ankur") {
        $('.cards').attr('name', '');
    }
    else {
        $("#cardpop").modal('show');
    }
}

function closeactivtysec() {
    $('#Activity').hide();
    $('#Attachment').hide();
    $('#Message').hide();
    $('#Favourite').hide();
    $('#Settings').hide();
}

function closearchive() {
    $('.webui-popoverab').hide();
}

function openarchivelist(project_id) {
    var ID = "#pops" + project_id;
    $('#webui-popoverabc').show();
    $(ID).hide();
}

function sortable_add_newList(spId) {
    //
    var boaridforcard = $("#boardID").val();
    var card_name = "";
    var CardId = 0;
    var P_ID;
    var indexer;
    var projectids = [];
    var IsSequential;
    var cardindex;
    var newlistname = "";
    var oldlistname = "";

    $('.tile__list').sortable({

        scrollSpeed: 40,
        cursor: 'move',
        delay: 200,
        opacity: 0.7,
        connectWith: '.tile__list',
        start: function (evt, ui) {
            P_ID = "#" + this.id;
            var listid = '#' + $(P_ID.substring(9)).selector.split('_')[0];

            oldlistname = $(listid).children('div:eq(0)').children('div:eq(0)').text();

            if ($.browser.mozilla) {
                $(".cards").attr("name", "ankur");
            }
            $("#multi").find("span").each(function () {
                projectids = projectids.concat(this.id);
            });
            $('.tile__list').css({ "margin-top": "10px", "padding-top": "10px" });

            var CardId12 = ui.item.attr('id');
            indexer = $('#' + CardId12).index();

            if ($("#" + P_ID.substring(9)).find(".IsSequential").text().toLowerCase().indexOf("sequential") >= 0)
            { IsSequential = "sequential"; }
            else
            { IsSequential = "normal"; }
        },
        update: function (event, ui) {
            var CardId1 = ui.item.attr('id');
            var card_name1 = $('#' + CardId1 + '').find(".cards .cRdNmE").text();
            card_name = card_name1;
            var listid = "#" + this.id;
            var pid = listid.replace(/^\D+/g, '').split('_')[0];
            var spids = listid.replace(/^\D+/g, '').split('_')[1];
            var CardLimits = $("#" + pid).find(".Cardlimit").text();
            CardLimits = CardLimits.split(":");
            var CardLimit = CardLimits[1].trim();
            if (($("#" + pid).find(".Cardlimit").text().toLowerCase().indexOf("max") >= 0) || ($("#cardlist" + pid + "_" + spids + " li").length <= CardLimit)) {
                var taskids = [];
                $(listid).find("li").each(function () {
                    if (this.id != "" && !isNaN(this.id))
                        taskids = taskids.concat(this.id);
                        GetCardsCount(pid);

                });
                MailOnCardSwaping(taskids, pid, CardId);
            }

        },
        change: function (event, ui) {
            CardId = ui.item.attr('id');
            cardindex = $("#" + CardId).index();

            $("#" + CardId).attr("title", "This card is moved recently.");
        },

        receive: function (event, ui) {

            var CardId1 = ui.item.attr('id');
            var card_name1 = $('#' + CardId1 + '').find(".cards .cRdNmE").text();

            var listid = "#" + this.id;
            var revlistid = listid.replace(/^\D+/g, '').split('_')[0];
            var sprintlistid = "#" + $(this).attr('spid');
            var revsprintlistid = listid.replace(/^\D+/g, '').split('_')[1];
            var pids = revlistid;
            var spids = revsprintlistid;
            var listnameselector = '#' + revlistid;
            newlistname = $(listnameselector).children('div:eq(0)').children('div:eq(0)').text();

            var CardId12 = ui.item.attr('id');

            var indexer2 = $('#' + CardId12).index();
            if (IsSequential == 'sequential') {
                var CardLimits = $("#" + pids).find(".Cardlimit").text();

                CardLimits = CardLimits.split(":");
                var CardLimit = CardLimits[1].trim();
                if (($("#" + pids).find(".Cardlimit").text().toLowerCase().indexOf("max") >= 0) || ($("#cardlist" + pids + "_" + spids + "> li").length <= CardLimit)) {
                    if (cardindex == 0) {
                        var taskids = [];
                        $(listid).find("li").each(function () {
                            if (this.id != "" && !isNaN(this.id))
                                taskids = taskids.concat(this.id);
                        });
                        $.ajax({
                            type: 'POST',
                            url: "/DataTracker.svc/SetTasksPriorityOnOther",
                            data: "{\"ProjectID\": \"" + pids + "\",\"boardid\": \"" + boaridforcard + "\",\"CardId\": \"" + CardId + "\",\"SprintId\": \"" + spids + "\"," + JSON.stringify({ TasksList: taskids }) + "}",
                            contentType: "application/json; charset=utf-8",
                            datatype: 'json',
                            success: function (data) {
                                var msg = "<b>" + card_name1 + "</b> swaped <b> " + oldlistname + "</b> To <b>" + newlistname + "</b>";
                                UpdateActivityListItem(msg, 2, CardId1);

                                $("#" + CardId).find("#MoveDate").val(new Date());
                                //GetTotalTimeEstimate(spids, spId);
                                GetCardsCount(pids);
                                CalculateBoardCompleteWithSprint(BoardID, spids);
                                SetProgressBarPer(spids);
                                second();
                            },
                            error: function (error) {
                                swal("Oops!", "Some error in swapping card.", "error");
                            }
                        });
                    }
                    else {
                        //
                        swal("Swapping is not possible!", "Please swap the top most card.", "error");

                        var lspid = spids;
                        var ProjectType1 = $("#" + pids + ".tile__name").text();
                        var lid = $(P_ID.substring(9)).selector.split('_')[0];
                        var lspid2 = $(P_ID).selector.split('_')[1];
                        var ProjectType2 = $("#" + lid + ".tile__name").text();
                        GetList(pids, ProjectType1, BoardID, lspid, spId);
                        GetList(lid, ProjectType2, BoardID, lspid2, spId);
                    }
                }
                else {

                    swal("Swapping is not possible!", "Please update the card limit.", "error");
                    var lspid = spids;
                    var ProjectType1 = $("#" + pids + ".tile__name").text();
                    var lid = $(P_ID.substring(9)).selector.split('_')[0];
                    var lspid2 = $(P_ID).selector.split('_')[1];
                    var ProjectType2 = $("#" + lid + ".tile__name").text();
                    GetList(pids, ProjectType1, BoardID, lspid, spId);
                    GetList(lid, ProjectType2, BoardID, lspid2, spId);
                }
            }
            else if (IsSequential == 'normal') {

                var CardLimits = $("#" + pids).find(".Cardlimit").text();
                CardLimits = CardLimits.split(":");
                var CardLimit = CardLimits[1].trim();
                if (($("#" + pids).find(".Cardlimit").text().toLowerCase().indexOf("max") >= 0) || ($("#cardlist" + pids + "_" + spids + "> li").length <= CardLimit)) {
                    var taskids = [];
                    $(listid).find("li").each(function () {
                        if (this.id != "" && !isNaN(this.id))
                            taskids = taskids.concat(this.id);
                    });
                    $.ajax({
                        type: 'POST',
                        url: "/DataTracker.svc/SetTasksPriorityOnOther",
                        data: "{\"ProjectID\": \"" + pids + "\",\"boardid\": \"" + boaridforcard + "\",\"CardId\": \"" + CardId + "\",\"SprintId\": \"" + spids + "\"," + JSON.stringify({ TasksList: taskids }) + "}",
                        contentType: "application/json; charset=utf-8",
                        datatype: 'json',
                        success: function (data) {
                            var msg = "<b>" + card_name1 + "</b> swaped <b> " + oldlistname + "</b> To <b>" + newlistname + "</b>";
                            UpdateActivityListItem(msg, 2, CardId1);
                            $("#" + CardId).find("#MoveDate").val(new Date());
                            GetCardsCount(pids);
                            GetTotalTimeEstimate(spids, spId);
                            CalculateBoardCompleteWithSprint(BoardID, spids);
                            SetProgressBarPer(spids);
                            second();
                        },
                        error: function (error) {
                            swal("Oops!", "Some error in swapping card.", "error");


                        }
                    });
                }
                else {
                    //
                    swal("Swapping is not possible!", "Please update the card limit.", "error");

                    var lspid = spids;
                    var ProjectType1 = $("#" + pids + ".tile__name").text();
                    var lid = $(P_ID.substring(9)).selector.split('_')[0];
                    var lspid2 = $(P_ID).selector.split('_')[1];
                    var ProjectType2 = $("#" + lid + ".tile__name").text();
                    GetList(pids, ProjectType1, BoardID, lspid, spId);
                    GetList(lid, ProjectType2, BoardID, lspid2, spId);
                }
            }
            else {

                swal("Swapping is not possible!", "Swapping should be in sequential manner.", "error");
                var lspid = spids;
                var ProjectType1 = $("#" + pids + ".tile__name").text();
                var lid = $(P_ID.substring(9)).selector.split('_')[0];
                var lspid2 = $(P_ID).selector.split('_')[1];
                var ProjectType2 = $("#" + lid + ".tile__name").text();
                GetList(pids, ProjectType1, BoardID, lspid, spId);
                GetList(lid, ProjectType2, BoardID, lspid2, spId);
            }
        },
        stop: function (event, ui) {

            var listid = "#" + this.id;
            var revlistid = listid.replace(/^\D+/g, '').split('_')[0];
            var revsprintlistid = listid.replace(/^\D+/g, '').split('_')[1];
            var pid = revlistid;
            var spids = revsprintlistid;
            var CardLimits = $("#" + pid).find(".Cardlimit").text();
            CardLimits = CardLimits.split(":");
            var CardLimit = CardLimits[1].trim();
            if (($("#" + pid).find(".Cardlimit").text().toLowerCase().indexOf("max") >= 0) || ($("#cardlist" + pid + "_" + spids + "> li").length <= CardLimit)) {
                var taskids = [];
                $(listid).find("li").each(function () {
                    if (this.id != "" && !isNaN(this.id))
                        taskids = taskids.concat(this.id);
                });
                $.ajax({
                    type: "POST",
                    url: "/DataTracker.svc/SetTasksPriority",
                    data: "{\"ProjectID\": \"" + pid + "\",\"CardId\": \"" + CardId + "\"," + JSON.stringify({ TasksList: taskids }) + "}",
                    contentType: "application/json; charset=utf-8",
                    datatype: 'json',
                    success: function (data) {
                        $("#" + CardId).find("#MoveDate").val(new Date());
                        GetCardsCount(pid);
                        GetTotalTimeEstimate(spids, spId);
                    },
                    error: function (error) {
                        swal("Oops!", "Some error in swapping card.", "error");

                    }
                });
            }
        }
    });
}

function OnClickProject(listidforswaptext) {

    //var SprintId = listidforswaptext.split('_').last();
    //alert(SprintId);
    //var listidforswaptext = listidforswaptext.split('_').first();
    //alert(listidforswaptext);
    var list_id_with_hash = "#" + listidforswaptext;
    var values = $(list_id_with_hash).find('.tile__name').text();
    var boardid = $("#boardID").val();
    var lastprojecttypeid = 0;
    var listpriority = 0;
    projectpriority = [];
    var count = 0;
    $('#multi').children("span").each(function () {
        count = count + 1;
        if (this.id != "")
            if (isNaN(this.id)) {
                var splt = this.id.split("t");
                projectpriority = projectpriority.concat(splt[1]);
            }
            else {
                projectpriority = projectpriority.concat(this.id);
            }
        if (this.id == listidforswaptext) {
            listpriority = count;
        }

    });
    lastprojecttypeid = $('#multi').children('span').last().attr('id');
    //var currentspid = $('#' + this.id).after("span").attr('id');
    //alert(currentspid);
    var serviceurl = "/DataTracker.svc/SetProjectPriority";
    $.ajax({
        url: "/DataTracker.svc/SetProjectPriority",
        type: 'POST',
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify({ p_priority: projectpriority, boardid: boardid, lastprojecttypeid: lastprojecttypeid, SwaplistId: listidforswaptext, projectpriority: listpriority }),
        dataType: "json",
        success: function (msg) {
            UpdateActivityListItem(values, 34);
            var currenturl = window.location.href;

            var index = currenturl.indexOf('#');

            if (index > -1) {
                currenturl = currenturl.substring(0, index);
            }
            CalculateBoardComplete(BoardID);
            second();

        },
        error: function (error) {

        }
    });
}


var dragId; var DropId; var myString; var my;

function allowDrop(ev) {

    ev.preventDefault();
}

function drag(ev) {

    dragId = ev.target.id;
    console.log("drop:" + dragId);

}

function drop(ev) {

    ev.preventDefault();
    DropId = ev.target.id;
    console.log("drop:" + DropId);
}

function myFunction(ProjectID) {
    //var listclass = $('#' + ProjectID).attr('class');
    //var listheight = $('#' + ProjectID).height();
    //var listsprint = listclass.replace("layer tile ", "");
    //var spheight = $('#Hide' + listsprint).height();
    //listheight = listheight + 30;
    //if (spheight == listheight)
    //{
    //    spheight = spheight + 67;
    //    $('#Hide' + listsprint).height(spheight + "px");
    //}

    var ProjectIDww = "#composer" + ProjectID;
    var cardaddid = "#cardaddid" + ProjectID;
    $('.card-composer').addClass('hide');
    $('.add_card').removeClass('hide');
    $('.addl').removeClass('hide');
    $(ProjectIDww).removeClass('hide');
    $(cardaddid).addClass('hide');
    $(".card-composer textarea").focus();
}

function mycross(ProjectID) {
    //var listclass = $('#' + ProjectID).attr('class');
    //var listheight = $('#' + ProjectID).height();
    //var listsprint = listclass.replace("layer tile ", "");
    //var spheight = $('#Hide' + listsprint).height();
    //
    //listheight = listheight + 30;
    //if (spheight == listheight) {
    //    spheight = spheight - 67;
    //    $('#Hide' + listsprint).height(spheight + "px");
    //}
    var card_already_exists = "#span" + ProjectID;
    $(card_already_exists).text("");
    $(".card-composer").find('textarea').val('');
    $("#textaera367").val('');
    $(".card-composer").addClass('hide');
    $('.add_card').removeClass('hide');

}

function myaddl(SprintId) {

    var sprintadid = "#sprint" + SprintId;
    $('.card-composer').addClass('hide');
    $('.add_card').removeClass('hide');
    $('.addl').parent().find("#" + SprintId).removeClass('hide');
    $(sprintadid).addClass('hide');
    $(".card-composer input").focus();
}

function crossl() {

    $("#txtlistbox").val('');
    $("#NoOfCards").val('');
    $('#IsSeqentialCheckBox').attr('checked', false);
    $('.crossl').parent().addClass('hide');
    $('.addl').removeClass('hide');
}

function emtxt() {
    $("#txtSrch").val('');
}

/*********************************************code for Other Functions*****************************************************************/

/*********************************************Abhash function*****************************************************************/

function openarchive(project_id) {
    var GetOwnerId = 0;
    if (GetOwnerId == 0) {
        $.ajax({
            type: "POST",
            url: "/DataTracker.svc/GetListOwnerByListID",
            data: "{\"projectid\": \"" + project_id + "\"}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                var res = JSON.parse(data.d);
                $.each(res, function (index, item) {
                    GetOwnerId = item.ListId;
                    if (GetOwnerId > 0) {
                        $(".MemberlistId").html("");
                        $(".MemberlistId").append("Change List Owners");
                        GetOpenArchive(GetOwnerId, project_id);
                    } else {
                        $(".MemberlistId").html("");
                        $(".MemberlistId").append("Define list Owner");
                        GetOpenArchive(GetOwnerId, project_id);
                    }
                });
            },
            error: function (data) { }
        });
    } else {
        
    }
}

function GetOpenArchive(GetOwnerId, project_id) {
    GetCardDueHoursByList(project_id);
    var ID = "#pops" + project_id;
    var listid = "#" + project_id;
    listNameForArchiveList = $(listid).find('.tile__name').text();
    var offset = $("#linkin" + project_id).offset();
    var posY = offset.top - $(window).scrollTop() - 60 + 'px';
    var posX = offset.left - $(window).scrollLeft() + 2 + 'px';
    if (($(document).width() - (offset.left + $("#linkin" + project_id).width())) >= 250) {

        $(ID).css({ left: posX, top: posY });
    }
    else {

        $(ID).css({ right: 5 + 'px', top: posY });
    }
    $('.webuiabc').hide();
    $('.webui-popoverab').hide();

    $(ID).show();
}

function GetCardDueHoursByList(project_id) {
    $.ajax({
        type: "POST",
        url: "/DataTracker.svc/GetCardDueHoursByList",
        data: "{\"projectid\": \"" + project_id + "\"}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            $("#List_" + project_id + "").val(data.d);
        },
        error: function (data) { }
    });
}

function UpdateCardDueHoursByList(project_id) {
    var hour = $("#List_" + project_id + "").val();
    $.ajax({
        type: "POST",
        url: "/DataTracker.svc/UpdateCardDueHoursByList",
        data: "{\"projectid\": \"" + project_id + "\",\"hour\": \"" + hour + "\"}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            alert("Hours Updated Successfully.");
        },
        error: function (data) { }
    });
}

function closearchive() {
    $(".MemberlistId").html("");
    $(".MemberlistId").append("Define list Owner");
    $('.webui-popoverab').hide();
}

function openarchivelist(project_id) {
    var offset = $("#linkin" + project_id).offset();
    var posY = offset.top - $(window).scrollTop() - 40 + 'px';
    var posX = offset.left - $(window).scrollLeft() + 2 + 'px';

    var ID = "#pops" + project_id;
    var ID2 = "#webui" + project_id;
    if (($(document).width() - (offset.left + $("#linkin" + project_id).width())) >= 300) {

        $(ID2).css({ left: posX, top: posY });
    }
    else {

        $(ID2).css({ right: 5 + 'px', top: posY });
    }

    $(ID2).show();
    $(ID).hide();
}

//Ameeq Changes for Swimlanes 08/03/18//
function myFunctionsprint() {

    var ProjectIDww = "#composer";
    var cardaddid = "#cardaddid";
    $('.card-composer').addClass('hide');
    $('.add_sprint').removeClass('hide');
    $('.addl').removeClass('hide');
    $(ProjectIDww).removeClass('hide');
    $(cardaddid).addClass('hide');
    $(".card-composer textarea").focus();
}

function mysprint() {

    var card_already_exists = "#span";
    $(card_already_exists).text("");
    $(".card-composer").find('textarea').val('');
    $("#textaera367").val('');
    $(".card-composer").addClass('hide');
    $('.add_sprint').removeClass('hide');

}

function AddSprintToListOnAddButtonClick() {

    var Id_textaera = "#txtCreateSprint";
    var cardname = $.trim($(Id_textaera).val()).replace(/'/g, "").replace(/\n/g, "").replace(/\\/g, "").replace(/\//g, "");
    if ($.trim(cardname).length > 0) {
        SaveNewSprint(cardname);
        $(Id_textaera).val("");
        $("#validatedesc").click();
    }
    else {
        $("#popMsg").text("Please enter Sprint Name");
    }
}

function SaveNewSprint(cardname) {
    
    $("#addNewcard1").removeClass('create_button');
    $("#cancelcard").removeClass('cross');
    $("#addNewcard1").hide();
    $("#cancelcard").hide();
    $("#loadstage").removeClass('displayloadstage');
    setcolorindex = 0;
    var temp1 = 12;
    var temp2 = 12;
    var i = 2;
    var BoardSprintId = "";
    var sp = 1;
    var editedcardname = "";
    var AddedSprint = "";
    var boaridforcard = $("#boardID").val();

    $.ajax({
        type: "POST",
        url: "/DataTracker.svc/SaveNewSprint",
        data: "{\"Card_Name\": \"" + cardname + "\",\"boardid\": \"" + boaridforcard + "\"}",
        dataType: "json",
        contentType: "application/json",
        success: function (data) {
            if (data.d == "") {
                $.ajax({
                    type: "POST",
                    url: "/DataTracker.svc/GetAddSprint",
                    data: "{\"Card_Name\": \"" + cardname + "\",\"boardid\": \"" + boaridforcard + "\"}",
                    dataType: "json",
                    contentType: "application/json",
                    success: function (data) {

                        var d = JSON.parse(data.d);
                        $.each(d, function (index, item) {

                            AddedSprint = cardname;
                            var spides = d.split('_');
                            SpID = spides[1];
                            BoardSprintId = spides[0];

                        });
                        $.ajax({
                            type: "POST",
                            url: "/DataTracker.svc/GetAllList",
                            data: "{\"boardid\": \"" + boaridforcard + "\",\"SprintBoardid\": \"" + SpID + "\"}",
                            dataType: "json",
                            contentType: "application/json",
                            success: function (data) {
                                var ownerid = $('#ownerID').val();
                                $.each(data.d, function (index, item) {

                                    if (item.IsSequential == 1) {
                                        IsSequential = "Type: Sequential";
                                    }
                                    else {
                                        IsSequential = "Type: Normal";
                                    }
                                    if (item.CardsNo != 0) {
                                        CardsNo = "Cards Limit: " + item.CardsNo;
                                    }
                                    else { CardsNo = "Cards Limit: Max"; }

                                    var projectname = item.ProjectType;
                                    if (projectname.length >= 20) { projectname = projectname.substr(0, 20) + '.....'; }
                                    else { projectname = projectname; }

                                    if (index >= temp1) {
                                        setcolorindex = 0;

                                        temp1 = temp2 * i;
                                        i++;
                                    }

                                    var colorcollection = ["#ee9f31", "#ef662a", "#ee4526", "#d1101f", "#a20080", "#780992", "#8742a6", "#1061e0", "#129eb1", "#008e55", "#84ab3d", "#dcc609"];
                                    var randomcolor = colorcollection[setcolorindex];
                                    setcolorindex++;
                                });

                                $("#multi").append("<div id='bdr_" + SpID + "'></div><div class='panel-heading' role='tab' id='ShowSprint_" + SpID + "' style='display:none;'><img src='Images/CaptureColapsed.JPG' onclick='ShowSprint(" + SpID + ");'><i class='fa fa-cog' aria-hidden='true' id='sprint" + SpID + "' onclick='javascript:SetSwimlanesOnSaveNewSprint(" + SpID + ");'></i><span onclick='ShowSprint(" + SpID + ");'>" + AddedSprint + "</span></div><div class='estimatedprogress multi_" + SpID + "' style='height:auto;  display:block;' class='Sprintbdr'><hr class='hr1'><div class='SprintProgress_" + SpID + "' id='SprintProgress'><div id='barforestimate_" + SpID + "' class='curentindex barforestimate' style='right: 405px; border-radius: 7px;' onclick='UpdateEstimatedTime(" + SpID + ");'></div><div id='progressbar_" + SpID + "' class='ui-progressbar ui-widget ui-widget-content ui-corner-all progressbar' role='progressbar' > <span id='check' class='pblabel_" + SpID + "'></span></div>  <div id='boardduedatedays_" + SpID + "' class='boardduedatedays' onclick='Selectsprintduedate(" + SpID + ")'></div> </div><hr class='hr2'><div id='auto' class='content'></div></div><div class='accordion-sprint-header' id='HideSprint_" + SpID + "' style='display:block; margin-top:5px;'><img src='Images/CaptureExpanded.JPG'  onclick='HideSprint(" + SpID + ");'><i class='fa fa-cog' aria-hidden='true' id='sprint" + SpID + "' onclick='javascript:SetSwimlanesOnSaveNewSprint(" + SpID + ");'></i><span  onclick='HideSprint(" + SpID + ");'>" + AddedSprint + "</span></div><div id=" + SpID + " class='add_list Sprint_" + SpID + "'><div id=" + SpID + " class='card-composer hide'><input id='txtlistbox" + SpID + "' maxlength='75' class='js-search-boards' onkeyup='javascript:AddList(this,event)' type='text' placeholder='Add a list'/> <br/> <div id='CardNoLimit'> <h3>Max Card Limit:</h3> <input  onkeypress='return IsNumerictextbox(event);' maxlength='3' type='text' id='NoOfCards" + SpID + "'/> <div class='clr'></div></div> <div id='IsSequentialdiv' ><h3>Is sequential :</h3> <input type='CheckBox' id='IsSeqentialCheckBox" + SpID + "' value='1'/> <div class='clr'></div> </div> <a id='addlist' class='fl create_button' href='#' onclick='javascript:AddListBySaveButton(" + SpID + ")'>Save</a><div id='cancellist' onclick='crossl();' class='crossl'>Cancel</div><p id='listloadstage' class='displayloadstage'>Adding List...</p><div class='clr'></div></div><a id='sprint" + SpID + "' onclick='myaddl(" + SpID + ");' class='addl'>Add a list</a>  </div><br/>");
                                GetTotalTimeEstimate(0, 0, SpID);
                                $("#boardduedatedays_" + SpID).append("<div class='datalist'><p>0</p></div>");
                            }
                        });
                    }
                });
            }
            else {
                alert("Sprint Allready Exist.");
            }

        }
    });
    $("#loadstage").addClass('displayloadstage');
    $("#addNewcard1").addClass('create_button');
    $("#cancelcard").addClass('cross');

    $("#addNewcard1").show();
    $("#cancelcard").show();
}
//Ameeq Changes for Swimlanes 08/03/18//

function openarchivelistpopclose(project_id) {
    $('.webuiabc').hide();
    $('.webui-popoverab').hide();
}

function openarchivelistleftarrow(project_id) {
    var ID = "#pops" + project_id;
    var ID2 = "#webui" + project_id;
    $(ID2).hide();
    $(ID).show();
}

function AddMemberToList(listid, boardid) {
    window.location.href = "AddMemberToList.aspx?Bid=" + boardid + "&Lid=" + listid;
}

function openundoarchivelist(listid, boardid) {
    window.location.href = "Showarchievedallcardoflist.aspx?Bid=" + boardid + "&Lid=" + listid;
}

function showArchivepage(projectid, bid, sid) {
    window.location.href = "displayArchivedAllList.aspx?Pid=" + projectid + "&bid=" + bid + "&sid=" + sid;
}
function ShowArchiveBoardpage() {
    window.location.href = "displayArchivedAllBoards1.aspx?boardid=" + BoardID + "";
}
function enablenewcard(projectid, bid, sid, ptypeid) {
    
    var TName = $(".enablecardclick"+projectid+"").text().trim();
    if (TName === "Disable Add a Card...") {
        $("#cardaddid" + projectid + "").hide(".enablecardclick" + projectid + "");
        $(".enablecardclick" + projectid + "").html("");
        $(".enablecardclick" + projectid + "").append("Enable Add a Card...");
    }
    else {
        $("#cardlist" + projectid + "_" + sid).after("<div id='composer" + projectid + "' class='card-composer hide'><div style='color:red; font-size:80%;clear:both;' id='span" + projectid + "'></div><textarea maxlength='200'  id='textaera" + projectid + "' onkeyup='javascript:AddCardToList(this, event," + projectid + "," + bid + ",\"" + ptypeid + "\")' class='js-search-boards' placeholder=''></textarea><a id='addNewcard1' class='fl create_button' onclick='javascript:AddCardToListOnAddButtonClick(" + projectid + "," + bid + "," + sid + ",\"" + ptypeid + "\")'  href='#'>Add</a><div id='cancelcard' onclick='mycross(" + projectid + ");' class='cross'>Cancel</div><p id='loadstage' class='displayloadstage'>Adding...</p><a href='#' class='list_action show-pop btn btn-default bottom-right' data-placement='auto-top' ></a>   </div>    <a id='cardaddid" + projectid + "'  onclick='myFunction(" + projectid + ");' draggable='false' class='add_card'>Add a card...</a> ");
        $(".enablecardclick" + projectid + "").html("");
        $(".enablecardclick" + projectid + "").append("Disable Add a Card...");
    }
}
//***************************** abhash **************************//
$(document).mouseup(function (e) {
    var container = $(".box-ab");

    if (!container.is(e.target)
        && container.has(e.target).length === 0) {
        container.hide();
    }
});

function boxdiv(project_id) {
    $('#isseq' + project_id).find("#rmsg").css("display", "none");
    var ID = "#isseq" + project_id;
    oldcardlimit = $('#listproprety' + project_id).find('.Cardlimit').html();
    oldcardlimit = oldcardlimit.split(":")[1].trim();
    var offset = $("#listproprety" + project_id).offset();
    var posY = offset.top - $(window).scrollTop() - 40 + 'px';
    var posX = offset.left - $(window).scrollLeft() + 60 + 'px';
    if (($(document).width() - (offset.left + $("#listproprety" + project_id).width())) >= 200) {

        $(ID).css({ left: posX, top: posY });
    }
    else {

        $(ID).css({ right: 5 + 'px', top: posY });
    }
    var climit = $('#listproprety' + project_id).find('.Cardlimit').html();
    climit = climit.split(":");
    var card = 'Max';
    if (climit[1].trim() == card.trim()) {
        $('#isseq' + project_id).find('#max-card').val("");
    }
    else {
        $('#isseq' + project_id).find('#max-card').val(climit[1].trim());
    }

    var type = $('#listproprety' + project_id).find('.IsSequential').html();
    type = type.split(":");
    var cardtype = 'Sequential';
    if (type[1].trim() == cardtype.trim()) {
        $('#isseq' + project_id).find('#sequentialcheckbox').attr('checked', true);
    }
    else {
        $('#isseq' + project_id).find('#sequentialcheckbox').attr('checked', false);
    }
    $(ID).show();
}

//function for number only
function IsNumerictextbox(e) {
    if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
        return false;
    }
}

function ChangelimitofCard(lid) {

    $('.box-ab').delay(1000).fadeOut(300);
    var listid = "#" + lid;
    var limitofcardtoshowactivity;
    listNameForArchiveList = $(listid).find('.tile__name').text();
    var noofcard = $("#cardlist" + lid + "> li").length;
    var climitchck = $('#listproprety' + lid).find('.Cardlimit').html();
    climitchck1 = climitchck.split(":");
    if ($('#isseq' + lid).find('#max-card').val() == "" && climitchck1[1].trim() == 'Max') {

        var issequential1 = $('#isseq' + lid).find("#sequentialcheckbox").is(':checked') ? 1 : 0;
        var limitofcard1 = 0;
        $.ajax({
            type: "POST",
            url: "/DataTracker.svc/Changelimitofcard",
            data: "{\"cardno\": \"" + limitofcard1 + "\",\"seqstatus\": \"" + issequential1 + "\",\"projectid\": \"" + lid + "\"}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {

                $('#isseq' + lid).find("#rmsg").css("display", "block");



                if (issequential1 == 1) {
                    $('#listproprety' + lid).find('.IsSequential').html("Type:Sequential");
                    $('#isseq' + lid).find('#sequentialcheckbox').attr('checked', true);
                }
                else {
                    $('#listproprety' + lid).find('.IsSequential').html("Type:Normal");
                    $('#isseq' + lid).find('#sequentialcheckbox').attr('checked', false);
                }
                var ss = listNameForArchiveList + "(" + oldcardlimit + " to Max" + ")";
                UpdateActivityListItem(ss, 14);



            },
            error: function (error) {
            }
        });

        return false;
    }
    else {
        if (noofcard <= $('#isseq' + lid).find('#max-card').val() || $('#isseq' + lid).find('#max-card').val() == "") {
            var limitofcard = $('#isseq' + lid).find('#max-card').val();
            if ($('#isseq' + lid).find('#max-card').val() == "") {
                limitofcardtoshowactivity = 0;
                limitofcard = 0;
            }
            else {
                limitofcardtoshowactivity = limitofcard;
            }

        }
        else {
            alert("Please enter max limit greater than existing no of card.");
            var climit = $('#listproprety' + lid).find('.Cardlimit').html();
            climit = climit.split(":");
            var card = 'Max';
            if (climit[1].trim() == card.trim()) {
                $('#isseq' + lid).find('#max-card').val(" ");
            }
            else {
                $('#isseq' + lid).find('#max-card').val(climit[1].trim());
            }


            var type = $('#listproprety' + lid).find('.IsSequential').html();
            type = type.split(":");
            var cardtype = 'Sequential';
            if (type[1].trim() == cardtype.trim()) {
                $('#isseq' + lid).find('#sequentialcheckbox').attr('checked', true);
            }
            else {
                $('#isseq' + lid).find('#sequentialcheckbox').attr('checked', false);
            }
            return false;
        }

    }

    var issequential = $('#isseq' + lid).find("#sequentialcheckbox").is(':checked') ? 1 : 0;
    $.ajax({
        type: "POST",
        url: "/DataTracker.svc/Changelimitofcard",
        data: "{\"cardno\": \"" + limitofcard + "\",\"seqstatus\": \"" + issequential + "\",\"projectid\": \"" + lid + "\"}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {

            $('#isseq' + lid).find("#rmsg").css("display", "block");
            if (limitofcard == 0) {
                $('#listproprety' + lid).find('.Cardlimit').html("Cards Limit: Max");
                $('#isseq' + lid).find('#max-card').val(" ");
            }
            else {
                $('#listproprety' + lid).find('.Cardlimit').html("");
                $('#listproprety' + lid).find('.Cardlimit').html("Cards Limit: " + limitofcard);
                $('#isseq' + lid).find('#max-card').val(limitofcard);
            }

            if (issequential == 1) {
                $('#listproprety' + lid).find('.IsSequential').html("Type:Sequential");
                $('#isseq' + lid).find('#sequentialcheckbox').attr('checked', true);
            }
            else {
                $('#listproprety' + lid).find('.IsSequential').html("Type:Normal");
                $('#isseq' + lid).find('#sequentialcheckbox').attr('checked', false);
            }
            var ss = listNameForArchiveList + " (" + oldcardlimit + " to " + limitofcardtoshowactivity + ")";
            UpdateActivityListItem(ss, 14);



        },
        error: function (error) {
        }
    });
}

// Ameeq Changes for update Card limit functionality 
function ChangelimitofCard(lid, spid) {

    $('.box-ab').delay(1000).fadeOut(300);
    var listid = "#" + lid;
    var limitofcardtoshowactivity;
    listNameForArchiveList = $(listid).find('.tile__name').text();
    var noofcard = $("#cardlist" + lid + "_" + spid + "> li").length;
    var climitchck = $('#listproprety' + lid).find('.Cardlimit').html();
    climitchck1 = climitchck.split(":");
    if ($('#isseq' + lid).find('#max-card').val() == "" && climitchck1[1].trim() == 'Max') {

        var issequential1 = $('#isseq' + lid).find("#sequentialcheckbox").is(':checked') ? 1 : 0;
        var limitofcard1 = 0;
        $.ajax({
            type: "POST",
            url: "/DataTracker.svc/Changelimitofcard",
            data: "{\"cardno\": \"" + limitofcard1 + "\",\"seqstatus\": \"" + issequential1 + "\",\"projectid\": \"" + lid + "\"}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {

                $('#isseq' + lid).find("#rmsg").css("display", "block");



                if (issequential1 == 1) {
                    $('#listproprety' + lid).find('.IsSequential').html("Type:Sequential");
                    $('#isseq' + lid).find('#sequentialcheckbox').attr('checked', true);
                }
                else {
                    $('#listproprety' + lid).find('.IsSequential').html("Type:Normal");
                    $('#isseq' + lid).find('#sequentialcheckbox').attr('checked', false);
                }
                var ss = listNameForArchiveList + "(" + oldcardlimit + " to Max" + ")";
                UpdateActivityListItem(ss, 14);



            },
            error: function (error) {
            }
        });

        return false;
    }
    else {
        if (noofcard <= $('#isseq' + lid).find('#max-card').val() || $('#isseq' + lid).find('#max-card').val() == "") {
            var limitofcard = $('#isseq' + lid).find('#max-card').val();
            if ($('#isseq' + lid).find('#max-card').val() == "") {
                limitofcardtoshowactivity = 0;
                limitofcard = 0;
            }
            else {
                limitofcardtoshowactivity = limitofcard;
            }

        }
        else {
            alert("Please enter max limit greater than existing no of card.");
            var climit = $('#listproprety' + lid).find('.Cardlimit').html();
            climit = climit.split(":");
            var card = 'Max';
            if (climit[1].trim() == card.trim()) {
                $('#isseq' + lid).find('#max-card').val(" ");
            }
            else {
                $('#isseq' + lid).find('#max-card').val(climit[1].trim());
            }


            var type = $('#listproprety' + lid).find('.IsSequential').html();
            type = type.split(":");
            var cardtype = 'Sequential';
            if (type[1].trim() == cardtype.trim()) {
                $('#isseq' + lid).find('#sequentialcheckbox').attr('checked', true);
            }
            else {
                $('#isseq' + lid).find('#sequentialcheckbox').attr('checked', false);
            }
            return false;
        }

    }

    var issequential = $('#isseq' + lid).find("#sequentialcheckbox").is(':checked') ? 1 : 0;
    $.ajax({
        type: "POST",
        url: "/DataTracker.svc/Changelimitofcard",
        data: "{\"cardno\": \"" + limitofcard + "\",\"seqstatus\": \"" + issequential + "\",\"projectid\": \"" + lid + "\"}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {

            $('#isseq' + lid).find("#rmsg").css("display", "block");
            if (limitofcard == 0) {
                $('#listproprety' + lid).find('.Cardlimit').html("Cards Limit: Max");
                $('#isseq' + lid).find('#max-card').val(" ");
            }
            else {
                $('#listproprety' + lid).find('.Cardlimit').html("");
                $('#listproprety' + lid).find('.Cardlimit').html("Cards Limit: " + limitofcard);
                $('#isseq' + lid).find('#max-card').val(limitofcard);
            }

            if (issequential == 1) {
                $('#listproprety' + lid).find('.IsSequential').html("Type:Sequential");
                $('#isseq' + lid).find('#sequentialcheckbox').attr('checked', true);
            }
            else {
                $('#listproprety' + lid).find('.IsSequential').html("Type:Normal");
                $('#isseq' + lid).find('#sequentialcheckbox').attr('checked', false);
            }
            var ss = listNameForArchiveList + " (" + oldcardlimit + " to " + limitofcardtoshowactivity + ")";
            UpdateActivityListItem(ss, 14);



        },
        error: function (error) {
        }
    });
}

function MailOnCardSwaping(taskids, pid, CardId) {

    var boardid = $("#boardID").val();
    var Board_Name = $("#BoardName").text();
    var TeamID = $("#teamID").val();
    var ProjectType = $("#" + pid).find('.tile__name').text();
    $.ajax({
        type: "POST",
        url: "/DataTracker.svc/MailOnCardSwaping",
        data: "{\"boardid\": \"" + boardid + "\",\"ListID\": \"" + pid + "\",\"CardId\": \"" + CardId + "\",\"teamid\": \"" + TeamID + "\"," + JSON.stringify({ TasksList: taskids }) + "}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
        },
        error: function (error) {
        }
    });

}

function AddNewEditTask(CardID, CardTitle, ProjectType, ProjectID) {

    var boardid = $("#boardID").val();

    var TeamID = $("#teamID").val();
    var Board_Name = $("#BoardName").text();
    var Team_Type = $("#AccessModifier").text().substr(1, $("#AccessModifier").text().length);
    if (TeamID != 0 && Team_Type == 'Private') {
        $.ajax({
            type: "POST",
            url: "/DataTracker.svc/addnewcardmail",
            data: "{\"boardid\": \"" + boardid + "\",\"listid\": \"" + ProjectID + "\",\"CardID\": \"" + CardID + "\",\"teamid\": \"" + TeamID + "\"}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
            },
            error: function (error) {
            }
        });
    }
    else {
    }
}

function AddMemeberToTask(CardID) {
    var boardid = $("#boardID").val();
    var TeamID = $("#teamID").val();
    var ProjectID = $("#" + CardID).parent().parent(".layer").attr('id');
    $.ajax({
        type: "POST",
        url: "/DataTracker.svc/AddMemberInCard",
        data: "{\"boardidd\": \"" + boardid + "\",\"listid\": \"" + ProjectID + "\",\"TaskID\": \"" + CardID + "\",\"TeamID\": \"" + TeamID + "\"}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
        },
        error: function (error) {
        }
    });
}

function OnDueDateChange(CardID) {
    //  
    var boardid = $("#boardID").val();
    var TeamID = $("#teamID").val();
    var ProjectID = $("#" + CardID).parent().parent(".layer").attr('id');
    $.ajax({
        type: "POST",
        url: "/DataTracker.svc/OnDueDateUpdate",
        data: "{\"boardidd\": \"" + boardid + "\",\"listid\": \"" + ProjectID + "\",\"TaskID\": \"" + CardID + "\",\"TeamID\": \"" + TeamID + "\"}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
        },
        error: function (error) {
        }
    });

}
//RJ-- Function to get event 
function GetActivityOnDrop(dragEl) {
    var itemName = dragEl.firstChild.firstChild.innerText;
    if (itemName != undefined && itemName != "") {
        UpdateActivityListItem(itemName, 34);
    }
}

function UpdateActivityListItem(itemName, ActivityLogType, cardid) {

    var Ui_Id = 1;
    itemName = itemName.replace(/\n/g, "<br />");
    var cardidd = 0;
    if (cardid != null && cardid != "" && cardid != 'undefined') {
        cardidd = cardid;
    }
    var BoardId = $("#boardID").val();
    $.ajax({
        type: "POST",
        url: "/DataTracker.svc/AddActivityLog",
        data: "{\"Message\": \"" + itemName + "\",\"ActivityLogType\": \"" + ActivityLogType + "\",\"boardid\": \"" + BoardId + "\", \"cardid\": \"" + cardidd + "\"}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",

        success: function (data) {

            if (ActivityLogType == 3) {
                sendmessage();
            }

            specificactivitylog(ActivityLogType);

        },
        error: function (error) {

        }
    });


}

function GetActivityLogDateWise() {
    var BoardId = $("#boardID").val(); $myList = $('#activitylist'); if ($myList.children().length === 0) {
        $.ajax({
            type: "POST", url: "/DataTracker.svc/GetActivityLogList", data: "{\"boardid\": \"" + BoardId + "\"}", contentType: "application/json; charset=utf-8", dataType: "json", success: function (data) {
                var HeaderId = data.d[0].ActDate.replace("-", "").replace("-", ""); $("#Datecontainer").empty(); $.each(data.d, function (index1, item1) {

                    var HeaderIdWithDate = item1.ActDate.replace("-", "").replace("-", ""); var hdate = item1.ActDate; $('#Datecontainer').append("<div id='dropdown-" + HeaderIdWithDate + "' class='dropdown dropdown-processed'><a onclick='javascript:ShowRecentActivityMsg(" + HeaderIdWithDate + " )' id='content" + HeaderIdWithDate + "' class='dropdown-link plus' href='#' >" + item1.ActDate + "</a><p class='pboardno' id='p-" + HeaderIdWithDate + "' style='font-weight:bold; margin-left:4px; font-size:13px; display:inline;'>(" + item1.total + ")</p><p class='icon-cntnr'><p title='No of new card created' id='t-" + HeaderIdWithDate + "' class='pboardno sqicn1' style='font-weight:bold; margin-left:4px; font-size:13px; display:inline;'><span>" + item1.newCardTotal + "</span></p><p title='No of Progress in task or list' id='c-" + HeaderIdWithDate + "' class='pboardno sqicn2' style='font-weight:bold; margin-left:4px; font-size:13px; display:inline;'>" + item1.swappedcardtotal + "</p><p title='Hours report daily' id='e-" + HeaderIdWithDate + "' class='pboardno sqicn3' style='font-weight:bold; margin-left:4px; font-size:13px; display:inline;'></i>" + item1.totalConsumedHours + "</p></p><div id='dropdowncontainer" + HeaderIdWithDate + "' class='dropdown-container1' style='display: none;'><ul id='activitylist_" + HeaderIdWithDate + "'></ul></div>");

                    var ID = '#activitylist_' + HeaderIdWithDate; $.each(item1.ActLogList, function (index, item) {

                        var filename; if (item.filename.length > 0) { filename = item.filename; }
                        else { filename = "DefaultImage.jpg"; }
                        if (item.ActivityLogType == 2 || item.ActivityLogType == 7 || item.ActivityLogType == 10 || item.ActivityLogType == 12 || item.ActivityLogType == 13 || item.ActivityLogType == 15 || item.ActivityLogType == 16 || item.ActivityLogType == 17) { $("#Datecontainer > div:first-child a").removeClass('plus').addClass('minus'); $(ID).append("<li id = activity_" + item.Activitylogid + " onmouseenter='javascript:hoverinactivity(" + item.Activitylogid + "," + item.taskid + ")' onmouseleave='javascript:hoveroutactivity(" + item.Activitylogid + ")'><div class='activitysectionleft'><div class='activityimg'><img class=profileimage" + item.userid + " src='userimage/" + filename + "'  /></div><p>" + item.UserName + "</p></div><div class='activitysectionright'><div class='activitysectionright_cont'><h6>" + item.ActivityTypeMsg + "</h6><p>" + item.Message + "</p><div class='right_bottom_time'>" + item.ActivityDate + "</div></div></div><div class='clr'></div></li>"); }
                        else { $("#Datecontainer > div:first-child a").removeClass('plus').addClass('minus'); $(ID).append("<li id = activity_" + item.Activitylogid + " onmouseenter='javascript:hoverinactivity(" + item.Activitylogid + "," + item.taskid + ")' onmouseleave='javascript:hoveroutactivity(" + item.Activitylogid + ")'><div class='activitysectionleft'><div class='activityimg'><img class=profileimage" + item.userid + " src='userimage/" + filename + "' /></div><p>" + item.UserName + "</p></div><div class='activitysectionright'><div class='activitysectionright_cont'><h6>" + item.ActivityTypeMsg + "</h6><p>" + item.Message + "</p><div class='right_bottom_time'>" + item.ActivityDate + "</div></div></div><div class='clr'></div></li>"); }
                    });
                }); $('#dropdowncontainer' + HeaderId).css('display', 'block');
            }, error: function (data) { }
        });
    }
}

function GetActivityLog(ActivityDate, hdate) {

    var ID = '#activitylist_' + ActivityDate;

    var BoardId = $("#boardID").val();
    $myList = $('#activitylist');
    if ($myList.children().length === 0) {
        $.ajax({
            type: "POST",
            url: "/DataTracker.svc/GetActivityLogList",
            data: "{\"boardid\": \"" + BoardId + "\",\"ActivityDate\": \"" + hdate + "\"}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {

                $.each(data.d, function (index, item) {

                    var filename;
                    if (item.filename.length > 0) {
                        filename = item.filename;
                    }
                    else {
                        filename = "DefaultImage.jpg";
                    }
                    if (item.ActivityLogType == 2 || item.ActivityLogType == 7 || item.ActivityLogType == 10 || item.ActivityLogType == 12 || item.ActivityLogType == 13 || item.ActivityLogType == 15 || item.ActivityLogType == 16 || item.ActivityLogType == 17) {
                        $("#Datecontainer > div:first-child a").removeClass('plus').addClass('minus');
                        $(ID).append("<li id = activity_" + item.Activitylogid + " onmouseenter='javascript:hoverinactivity(" + item.Activitylogid + "," + item.taskid + ")' onmouseleave='javascript:hoveroutactivity(" + item.Activitylogid + ")'><div class='activitysectionleft'><div class='activityimg'><img class=profileimage" + item.userid + " src='userimage/" + filename + "'  /></div><p>" + item.UserName + "</p></div><div class='activitysectionright'><div class='activitysectionright_cont'><h6>" + item.ActivityTypeMsg + "</h6><p>" + item.Message + "</p><div class='right_bottom_time'>" + item.ActivityDate + "</div></div></div><div class='clr'></div></li>");

                    }
                    else {
                        $("#Datecontainer > div:first-child a").removeClass('plus').addClass('minus');
                        $(ID).append("<li id = activity_" + item.Activitylogid + " onmouseenter='javascript:hoverinactivity(" + item.Activitylogid + "," + item.taskid + ")' onmouseleave='javascript:hoveroutactivity(" + item.Activitylogid + ")'><div class='activitysectionleft'><div class='activityimg'><img class=profileimage" + item.userid + " src='userimage/" + filename + "' /></div><p>" + item.UserName + "</p></div><div class='activitysectionright'><div class='activitysectionright_cont'><h6>" + item.ActivityTypeMsg + "</h6><p>" + item.Message + "</p><div class='right_bottom_time'>" + item.ActivityDate + "</div></div></div><div class='clr'></div></li>");
                    }
                });


            },
            error: function (data) {

            }
        });
    }
}

function ShowRecentActivityMsg(headeriddate) {

    $('#Datecontainer').scrollTop(0);
    var id = "#content" + headeriddate;

    $(id).toggleClass('plus');
    $(id).toggleClass('minus');

    $(id).siblings().not('.pboardno').slideToggle();

    $(id).parent().siblings().each(function (index, element) {

        $(element).find(".dropdown-container1:visible").hide();
    });
    $(id).parent().siblings().each(function (index, element) {

        $(element).find("a.dropdown-link").removeClass('minus').addClass('plus');
    });
}

function specificactivitylog(ActivityLogType) {

    var BoardId = $("#boardID").val();
    $.ajax({
        type: "POST",
        url: "/DataTracker.svc/specificactivitylogs",
        data: "{\"boardid\": \"" + BoardId + "\",\"ActivityLogType\": \"" + ActivityLogType + "\"}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",

        success: function (data) {

            var errorstatus = data.d[0].errorstatus;
            if (errorstatus == 0) {
                var item = data.d[0];
                var taskid = data.d[0].taskid;
                var ActivityDate = data.d[0].ActivityDate.replace("-", "").replace("-", "");
                var ID = '#activitylist_' + ActivityDate;
                var dropdownid = "dropdown-" + ActivityDate;
                var UserId = data.d[0].UserId;
                var UserName = data.d[0].UserName;
                var filename = data.d[0].profileimage;
                var activitylogtypemessage = data.d[0].activitylogtypemessage;
                var activitylogmessage = data.d[0].activitylogmessage;
                var datetime = 'just now';
                var profileimage;
                if (filename.length > 0) {
                    profileimage = filename;
                }
                else {
                    profileimage = "DefaultImage.jpg";
                }

                var match = $("#Datecontainer").children().first().attr('id');
                if (ActivityLogType == 2 || ActivityLogType == 7 || ActivityLogType == 10 || ActivityLogType == 12 || ActivityLogType == 13 || ActivityLogType == 15 || ActivityLogType == 16 || ActivityLogType == 17) {
                    if (dropdownid == match) {
                        $("#Datecontainer > div > .dropdown-container1").css("display", "none");
                        $("#Datecontainer > div:first-child > .dropdown-container1").show();
                        $("#Datecontainer > div a").removeClass('minus').addClass('plus');
                        $("#Datecontainer > div:first-child a").removeClass('plus').addClass('minus');
                        $(ID).prepend("<li id = activity_" + item.Activitylogid + " onmouseenter='javascript:hoverinactivity(" + item.Activitylogid + "," + taskid + ")' onmouseleave='javascript:hoveroutactivity(" + item.Activitylogid + ")'><div class='activitysectionleft'><div class='activityimg'><img class=profileimage" + UserId + " src='userimage/" + profileimage + "' /></div><p> " + UserName + " </p></div><div class='activitysectionright'><div class='activitysectionright_cont'><h6>" + activitylogtypemessage + "</h6><p>" + activitylogmessage + "</p><div class='right_bottom_time'>" + datetime + "</div></div></div><div class='clr'></div></li>");
                        var totals = ID + ' ' + 'li';
                        var total = $(totals).length;
                        var countid = "#p-" + ActivityDate;
                        $(countid).html("(" + total + ")");
                    }
                    else {
                        $('#Datecontainer').prepend("<div id='dropdown-" + ActivityDate + "' class='dropdown dropdown-processed'><a onclick='javascript:ShowRecentActivityMsg(" + ActivityDate + " )' id='content" + ActivityDate + "' class='dropdown-link plus' href='#' >" + data.d[0].ActivityDate + "</a><p class='pboardno' id='p-" + ActivityDate + "' style='font-weight:bold; margin-left:4px; font-size:13px; display:inline;'></p><p class='icon-cntnr'><p title='No of new card created' id='t-" + ActivityDate + "' class='pboardno sqicn1' style='font-weight:bold; margin-left:4px; font-size:13px; display:inline;'><span></span></p><p title='No of Progress in task or list' id='c-" + ActivityDate + "' class='pboardno sqicn2' style='font-weight:bold; margin-left:4px; font-size:13px; display:inline;'></p><p title='Hours report daily' id='e-" + ActivityDate + "' class='pboardno sqicn3' style='font-weight:bold; margin-left:4px; font-size:13px; display:inline;'></i>0</p></p><div id='dropdowncontainer" + ActivityDate + "' class='dropdown-container1' style='display: none;'><ul id='activitylist_" + ActivityDate + "'></ul></div>");
                        $(ID).prepend("<li id = activity_" + item.Activitylogid + " onmouseenter='javascript:hoverinactivity(" + item.Activitylogid + "," + taskid + ")' onmouseleave='javascript:hoveroutactivity(" + item.Activitylogid + ")'><div class='activitysectionleft'><div class='activityimg'><img class=profileimage" + UserId + " src='userimage/" + profileimage + "' /></div><p> " + UserName + " </p></div><div class='activitysectionright'><div class='activitysectionright_cont'><h6>" + activitylogtypemessage + "</h6><p>" + activitylogmessage + "</p><div class='right_bottom_time'>" + datetime + "</div></div></div><div class='clr'></div></li>");
                        var totals = ID + ' ' + 'li';
                        var total = $(totals).length;
                        var countid = "#p-" + ActivityDate;
                        $(countid).html("(" + total + ")");
                        $("#Datecontainer > div:first-child a").removeClass('plus').addClass('minus');
                    }
                }
                else {
                    if (dropdownid == match) {
                        $("#Datecontainer > div:first-child a").removeClass('plus').addClass('minus');
                        $(ID).prepend("<li id = activity_" + item.Activitylogid + " onmouseenter='javascript:hoverinactivity(" + item.Activitylogid + "," + taskid + ")' onmouseleave='javascript:hoveroutactivity(" + item.Activitylogid + ")'><div class='activitysectionleft'><div class='activityimg'><img class=profileimage" + UserId + " src='userimage/" + profileimage + "' /></div><p> " + UserName + " </p></div><div class='activitysectionright'><div class='activitysectionright_cont'><h6>" + activitylogtypemessage + "</h6><p>" + activitylogmessage + "</p><div class='right_bottom_time'>" + datetime + "</div></div></div><div class='clr'></div></li>");
                        var totals = ID + ' ' + 'li';
                        var total = $(totals).length;
                        var countid = "#p-" + ActivityDate;
                        $(countid).html("(" + total + ")");
                    }
                    else {
                        $('#Datecontainer').prepend("<div id='dropdown-" + ActivityDate + "' class='dropdown dropdown-processed'><a onclick='javascript:ShowRecentActivityMsg(" + ActivityDate + " )' id='content" + ActivityDate + "' class='dropdown-link plus' href='#' >" + data.d[0].ActivityDate + "</a><p class='pboardno' id='p-" + ActivityDate + "' style='font-weight:bold; margin-left:4px; font-size:13px; display:inline;'></p><p class='icon-cntnr'><p title='No of new card created' id='t-" + ActivityDate + "' class='pboardno sqicn1' style='font-weight:bold; margin-left:4px; font-size:13px; display:inline;'><span></span></p><p title='No of Progress in task or list' id='c-" + ActivityDate + "' class='pboardno sqicn2' style='font-weight:bold; margin-left:4px; font-size:13px; display:inline;'></p><p title='Hours report daily' id='e-" + ActivityDate + "' class='pboardno sqicn3' style='font-weight:bold; margin-left:4px; font-size:13px; display:inline;'></i>0</p></p><div id='dropdowncontainer" + ActivityDate + "' class='dropdown-container1' style='display: none;'><ul id='activitylist_" + ActivityDate + "'></ul></div>");
                        $(ID).prepend("<li id = activity_" + item.Activitylogid + " onmouseenter='javascript:hoverinactivity(" + item.Activitylogid + "," + taskid + ")' onmouseleave='javascript:hoveroutactivity(" + item.Activitylogid + ")'><div class='activitysectionleft'><div class='activityimg'><img class=profileimage" + UserId + " src='userimage/" + profileimage + "'/></div><p> " + UserName + " </p></div><div class='activitysectionright'><div class='activitysectionright_cont'><h6>" + activitylogtypemessage + "</h6><p>" + activitylogmessage + "</p><div class='right_bottom_time'>" + datetime + "</div></div></div><div class='clr'></div></li>");
                        var totals = ID + ' ' + 'li';
                        var total = $(totals).length;
                        var countid = "#p-" + ActivityDate;
                        $(countid).html("(" + total + ")");
                        $("#Datecontainer > div:first-child a").removeClass('plus').addClass('minus');
                    }
                }
            }
            else {

            }

            if (ActivityLogType == 7) {
                var newcardpid = "#t-" + ActivityDate;
                var newcardpdata = $(newcardpid).text();
                if (newcardpdata != "") {
                    var newcardpcount = parseInt($(newcardpid).text().replace('(', '').replace(')', '')) + 1;
                    $(newcardpid).empty();
                    $(newcardpid).text("" + newcardpcount + "");
                }
                else {
                    $(newcardpid).text("" + 1 + "");
                }

            }
            if (ActivityLogType == 2 || ActivityLogType == 34) {
                var swapcardpid = "#c-" + ActivityDate;
                var swapcardpdata = $(swapcardpid).text();
                if (swapcardpdata != "") {
                    var swapcardpcount = parseInt($(swapcardpid).text().replace('(', '').replace(')', '')) + 1;
                    $(swapcardpid).empty();
                    $(swapcardpid).html("" + swapcardpcount + "");
                }
                else {
                    $(swapcardpid).text("" + 1 + "");
                }

            }

            var newcardpid = "#t-" + ActivityDate;
            var newcardpcount = $(newcardpid).text();

            if (newcardpcount == "") {
                $(newcardpid).text("" + 0 + "");
            }

            var swapcardpid = "#c-" + ActivityDate;
            var swapcardpdata = $(swapcardpid).text();
            if (swapcardpdata == "") {
                $(swapcardpid).text("" + 0 + "");
            }

        },
        error: function (data) {

        }
    });
}

function hoverinactivity(activitylogid, cardid) {
    var activitylogidd = '#activity_' + activitylogid;
    $(activitylogidd).css('border', '1px solid red');

    var taskid = '#' + cardid;

    $(taskid).css('border', '1px solid red');

    var aa = $(taskid).position();
    $(".board_sec").animate({
        scrollLeft: aa.left - 10,
        scrollTop: aa.top - 150
    }, 1000);

    $(taskid).animate({
        borderLeftColor: "white",
        borderTopColor: "white",
        borderRightColor: "white",
        borderBottomColor: "white",
    }, 3000);
}

function hoveroutactivity(activitylogid) {
    var activitylogidd = '#activity_' + activitylogid;
    $(activitylogidd).css('border', '');
}

function showAttachmentList(attachmentidd) {
    if ($(this).hasClass('selected')) {
        closeMessageBox('att-list')
    } else {
        ActivityLogAttachmentList(attachmentidd)
        $(this).addClass('selected');
        $('.att-list').slideFadeToggle();

    }

}

function SaveActivityLogAttachment(fileName) {

    var BoardId = $("#boardID").val();
    $.ajax({
        type: "POST",
        url: "/DataTracker.svc/AddActivityLogAttachment",
        data: "{\"FileName\": \"" + fileName + "\",\"BoardId\": \"" + BoardId + "\"}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {

            addattachment(fileName);

        },
        error: function (error) {
        }
    });
}

function GetActivityLogAttachmentCount() {
    var BoardId = $("#boardID").val();
    $.ajax({
        type: "POST",
        url: "/DataTracker.svc/AddActivityLogAttachmentCount",
        data: "{\"BoardId\": \"" + BoardId + "\"}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            UpdateAttachmentCount(data.d);
        },
        error: function (error) {
        }
    });
}

function SendInvitation(email, boardID, ActivityLogType) {
    $("#btninvsend").attr("href", "#close-modal");
    $("#btninvsend").attr("rel", "modal:close");
    var teamid = $("#teamID").val();
    $.ajax({
        type: "POST",
        url: "/DataTracker.svc/InviteUser",
        data: "{\"email\": \"" + email + "\",\"boardID\": \"" + boardID + "\",\"ActivityLogType\": \"" + ActivityLogType + "\",\"teamid\": \"" + teamid + "\"}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            if (data.d == "True") {
                alert('Invited Successfully.');
                $("#txtinvemail").val('');
                $("#btninvsend").attr("href", "#close-modal");
                $("#btninvsend").attr("rel", "modal:close");
            }
            else {
                alert('Allready Exist.');
            }
            GetActivityLogDateWise();
        },
        error: function (error) {
        }
    });
}

function ActivityLogAttachmentList() {

    var BoardId = $("#boardID").val();

    $.ajax({
        type: "POST",
        url: "/DataTracker.svc/GetActivityLogAttachmentList",
        data: "{\"boardid\": \"" + BoardId + "\"}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {

            $("#attachlist").empty();
            $.each(data.d, function (index, item) {

                var filenamepic = item.filenamee;
                var filename = item.FileName;
                var fname = filename.split('.');
                var ext = fname[1];

                var filenameShow;
                if (filenamepic.length > 0) {
                    filenameShow = filenamepic;
                }
                else {
                    filenameShow = "DefaultImage.jpg";
                }

                if (ext == 'sql') {
                    $('#attachlist').append("<ul><li> <div class='memname'><div class='attachment_user_img'><img class=profileimage" + item.userid + " src='userimage/" + filenameShow + "'/></div><h3>" + item.UserName + "</h3><div class='clr'></div></div>  <div class='attachment_details'><div class='attachment_detail_row'><div class='attach_detail'><img width:75px; hieght:75px; src='/TaskTempDoc/sql-icon.png' alt='file' /> <p>" + item.FileName + "</p><p style='font-size:x-small'>" + item.UplodedDate + "</p></div><div class='attach_icons'><a class='download_icon' id='File" + item.ActivityLogAttachmentId + "' href='/downloadfiles.aspx?FileNamee=" + item.FileName + "' >" + item.FileName + "</a> <a class='cancel_icon' href='javascript:RemoveAttachmentList(" + item.ActivityLogAttachmentId + ");'>cancel</a></div><div class='clr'></div></div></div>  </li></ul>");
                }
                else if (ext == 'ods') {
                    $('#attachlist').append("<ul><li> <div class='memname'><div class='attachment_user_img'><img class=profileimage" + item.userid + " src='userimage/" + filenameShow + "' /></div><h3>" + item.UserName + "</h3><div class='clr'></div></div>  <div class='attachment_details'><div class='attachment_detail_row'><div class='attach_detail'><img width:75px; hieght:75px; src='/TaskTempDoc/ods-icon.png' alt='file' /> <p>" + item.FileName + "</p><p style='font-size:x-small'>" + item.UplodedDate + "</p></div><div class='attach_icons'><a class='download_icon' id='File" + item.ActivityLogAttachmentId + "' href='/downloadfiles.aspx?FileNamee=" + item.FileName + "'>" + item.FileName + "</a> <a class='cancel_icon' href='javascript:RemoveAttachmentList(" + item.ActivityLogAttachmentId + ");'>cancel</a></div><div class='clr'></div></div></div>  </li></ul>");
                }
                else if (ext == 'odt') {
                    $('#attachlist').append("<ul><li> <div class='memname'><div class='attachment_user_img'><img class=profileimage" + item.userid + " src='userimage/" + filenameShow + "' /></div><h3>" + item.UserName + "</h3><div class='clr'></div></div>  <div class='attachment_details'><div class='attachment_detail_row'><div class='attach_detail'><img width:75px; hieght:75px; src='/TaskTempDoc/odt-icon.png' alt='file' /> <p>" + item.FileName + "</p><p style='font-size:x-small'>" + item.UplodedDate + "</p></div><div class='attach_icons'><a class='download_icon' id='File" + item.ActivityLogAttachmentId + "' href='/downloadfiles.aspx?FileNamee=" + item.FileName + "'>" + item.FileName + "</a> <a class='cancel_icon' href='javascript:RemoveAttachmentList(" + item.ActivityLogAttachmentId + ");'>cancel</a></div><div class='clr'></div></div></div>  </li></ul>");
                }
                else if (ext == 'ppt') {
                    $('#attachlist').append("<ul><li> <div class='memname'><div class='attachment_user_img'><img class=profileimage" + item.userid + " src='userimage/" + filenameShow + "' /></div><h3>" + item.UserName + "</h3><div class='clr'></div></div>  <div class='attachment_details'><div class='attachment_detail_row'><div class='attach_detail'><img width:75px; hieght:75px; src='/TaskTempDoc/ppt-icon.png' alt='file' /> <p>" + item.FileName + "</p><p style='font-size:x-small'>" + item.UplodedDate + "</p></div><div class='attach_icons'><a class='download_icon' id='File" + item.ActivityLogAttachmentId + "' href='/downloadfiles.aspx?FileNamee=" + item.FileName + "'>" + item.FileName + "</a> <a class='cancel_icon' href='javascript:RemoveAttachmentList(" + item.ActivityLogAttachmentId + ");'>cancel</a></div><div class='clr'></div></div></div>  </li></ul>");
                }
                else if (ext == 'pptx') {
                    $('#attachlist').append("<ul><li> <div class='memname'><div class='attachment_user_img'><img class=profileimage" + item.userid + " src='userimage/" + filenameShow + "' /></div><h3>" + item.UserName + "</h3><div class='clr'></div></div>  <div class='attachment_details'><div class='attachment_detail_row'><div class='attach_detail'><img width:75px; hieght:75px; src='/TaskTempDoc/ppt-icon.png' alt='file' /> <p>" + item.FileName + "</p><p style='font-size:x-small'>" + item.UplodedDate + "</p></div><div class='attach_icons'><a class='download_icon' id='File" + item.ActivityLogAttachmentId + "' href='/downloadfiles.aspx?FileNamee=" + item.FileName + "'>" + item.FileName + "</a> <a class='cancel_icon' href='javascript:RemoveAttachmentList(" + item.ActivityLogAttachmentId + ");'>cancel</a></div><div class='clr'></div></div></div>  </li></ul>");
                }
                else if (ext == 'rtf') {
                    $('#attachlist').append("<ul><li> <div class='memname'><div class='attachment_user_img'><img class=profileimage" + item.userid + " src='userimage/" + filenameShow + "' /></div><h3>" + item.UserName + "</h3><div class='clr'></div></div>  <div class='attachment_details'><div class='attachment_detail_row'><div class='attach_detail'><img width:75px; hieght:75px; src='/TaskTempDoc/rtf-icon.png' alt='file' /> <p>" + item.FileName + "</p><p style='font-size:x-small'>" + item.UplodedDate + "</p></div><div class='attach_icons'><a class='download_icon' id='File" + item.ActivityLogAttachmentId + "' href='/downloadfiles.aspx?FileNamee=" + item.FileName + "'>" + item.FileName + "</a> <a class='cancel_icon' href='javascript:RemoveAttachmentList(" + item.ActivityLogAttachmentId + ");'>cancel</a></div><div class='clr'></div></div></div>  </li></ul>");
                }
                else if (ext == 'csv') {
                    $('#attachlist').append("<ul><li> <div class='memname'><div class='attachment_user_img'><img class=profileimage" + item.userid + " src='userimage/" + filenameShow + "' /></div><h3>" + item.UserName + "</h3><div class='clr'></div></div>  <div class='attachment_details'><div class='attachment_detail_row'><div class='attach_detail'><img width:75px; hieght:75px; src='/TaskTempDoc/csv-icon.png' alt='file' /> <p>" + item.FileName + "</p><p style='font-size:x-small'>" + item.UplodedDate + "</p></div><div class='attach_icons'><a class='download_icon' id='File" + item.ActivityLogAttachmentId + "' href='/downloadfiles.aspx?FileNamee=" + item.FileName + "'>" + item.FileName + "</a> <a class='cancel_icon' href='javascript:RemoveAttachmentList(" + item.ActivityLogAttachmentId + ");'>cancel</a></div><div class='clr'></div></div></div>  </li></ul>");
                }
                else if (ext == 'xls') {

                    $('#attachlist').append("<ul><li> <div class='memname'><div class='attachment_user_img'><img class=profileimage" + item.userid + " src='userimage/" + filenameShow + "'/></div><h3>" + item.UserName + "</h3><div class='clr'></div></div>  <div class='attachment_details'><div class='attachment_detail_row'><div class='attach_detail'><img width:75px; hieght:75px; src='/TaskTempDoc/xls_logo.png' alt='file' /> <p>" + item.FileName + "</p><p style='font-size:x-small'>" + item.UplodedDate + "</p></div><div class='attach_icons'><a class='download_icon' id='File" + item.ActivityLogAttachmentId + "' href='/downloadfiles.aspx?FileNamee=" + item.FileName + "'>" + item.FileName + "</a> <a class='cancel_icon' href='javascript:RemoveAttachmentList(" + item.ActivityLogAttachmentId + ");'>cancel</a></div><div class='clr'></div></div></div>  </li></ul>");
                }
                else if (ext == 'docx') {
                    $('#attachlist').append("<ul><li> <div class='memname'><div class='attachment_user_img'><img class=profileimage" + item.userid + " src='userimage/" + filenameShow + "' /></div><h3>" + item.UserName + "</h3><div class='clr'></div></div>  <div class='attachment_details'><div class='attachment_detail_row'><div class='attach_detail'><img width:75px; hieght:75px; src='/TaskTempDoc/doc-icon.png' alt='file' /> <p>" + item.FileName + "</p><p style='font-size:x-small'>" + item.UplodedDate + "</p></div><div class='attach_icons'><a class='download_icon' id='File" + item.ActivityLogAttachmentId + "' href='/downloadfiles.aspx?FileNamee=" + item.FileName + "'>" + item.FileName + "</a> <a class='cancel_icon' href='javascript:RemoveAttachmentList(" + item.ActivityLogAttachmentId + ");'>cancel</a></div><div class='clr'></div></div></div>  </li></ul>");
                }
                else if (ext == 'doc') {
                    $('#attachlist').append("<ul><li> <div class='memname'><div class='attachment_user_img'><img class=profileimage" + item.userid + " src='userimage/" + filenameShow + "' /></div><h3>" + item.UserName + "</h3><div class='clr'></div></div>  <div class='attachment_details'><div class='attachment_detail_row'><div class='attach_detail'><img width:75px; hieght:75px; src='/TaskTempDoc/doc-icon.png' alt='file' /> <p>" + item.FileName + "</p><p style='font-size:x-small'>" + item.UplodedDate + "</p></div><div class='attach_icons'><a class='download_icon' id='File" + item.ActivityLogAttachmentId + "' href='/downloadfiles.aspx?FileNamee=" + item.FileName + "'>" + item.FileName + "</a> <a class='cancel_icon' href='javascript:RemoveAttachmentList(" + item.ActivityLogAttachmentId + ");'>cancel</a></div><div class='clr'></div></div></div>  </li></ul>");
                }
                else if (ext == 'txt') {
                    $('#attachlist').append("<ul><li> <div class='memname'><div class='attachment_user_img'><img class=profileimage" + item.userid + " src='userimage/" + filenameShow + "' /></div><h3>" + item.UserName + "</h3><div class='clr'></div></div>  <div class='attachment_details'><div class='attachment_detail_row'><div class='attach_detail'><img width:75px; hieght:75px; src='/TaskTempDoc/txt-icon.png' alt='file' /> <p>" + item.FileName + "</p><p style='font-size:x-small'>" + item.UplodedDate + "</p></div><div class='attach_icons'><a class='download_icon' id='File" + item.ActivityLogAttachmentId + "' href='/downloadfiles.aspx?FileNamee=" + item.FileName + "'>" + item.FileName + "</a> <a class='cancel_icon' href='javascript:RemoveAttachmentList(" + item.ActivityLogAttachmentId + ");'>cancel</a></div><div class='clr'></div></div></div>  </li></ul>");
                }
                else if (ext == 'pdf') {

                    $('#attachlist').append("<ul><li> <div class='memname'><div class='attachment_user_img'><img class=profileimage" + item.userid + " src='userimage/" + filenameShow + "' /></div><h3>" + item.UserName + "</h3><div class='clr'></div></div>  <div class='attachment_details'><div class='attachment_detail_row'><div class='attach_detail'><img width:75px; hieght:75px; src='/TaskTempDoc/pdf.png' alt='file' /> <p>" + item.FileName + "</p><p style='font-size:x-small'>" + item.UplodedDate + "</p></div><div class='attach_icons'><a class='download_icon' id='File" + item.ActivityLogAttachmentId + "' href='/downloadfiles.aspx?FileNamee=" + item.FileName + "'>" + item.FileName + "</a> <a class='cancel_icon' href='javascript:RemoveAttachmentList(" + item.ActivityLogAttachmentId + ");'>cancel</a></div><div class='clr'></div></div></div>  </li></ul>");
                }
                else if (ext == 'xlsx') {

                    $('#attachlist').append("<ul><li> <div class='memname'><div class='attachment_user_img'><img class=profileimage" + item.userid + " src='userimage/" + filenameShow + "' /></div><h3>" + item.UserName + "</h3><div class='clr'></div></div>  <div class='attachment_details'><div class='attachment_detail_row'><div class='attach_detail'><img width:75px; hieght:75px; src='/TaskTempDoc/xls_logo.png' alt='file' /> <p>" + item.FileName + "</p><p style='font-size:x-small'>" + item.UplodedDate + "</p></div><div class='attach_icons'><a class='download_icon' id='File" + item.ActivityLogAttachmentId + "' href='/downloadfiles.aspx?FileNamee=" + item.FileName + "'>" + item.FileName + "</a> <a class='cancel_icon' href='javascript:RemoveAttachmentList(" + item.ActivityLogAttachmentId + ");'>cancel</a></div><div class='clr'></div></div></div>  </li></ul>");
                }
                else if (ext == 'xml') {

                    $('#attachlist').append("<ul><li> <div class='memname'><div class='attachment_user_img'><img class=profileimage" + item.userid + " src='userimage/" + filenameShow + "' /></div><h3>" + item.UserName + "</h3><div class='clr'></div></div>  <div class='attachment_details'><div class='attachment_detail_row'><div class='attach_detail'><img width:75px; hieght:75px; src='/TaskTempDoc/xml.png' alt='file' /> <p>" + item.FileName + "</p><p style='font-size:x-small'>" + item.UplodedDate + "</p></div><div class='attach_icons'><a class='download_icon' id='File" + item.ActivityLogAttachmentId + "' href='/downloadfiles.aspx?FileNamee=" + item.FileName + "'>" + item.FileName + "</a> <a class='cancel_icon' href='javascript:RemoveAttachmentList(" + item.ActivityLogAttachmentId + ");'>cancel</a></div><div class='clr'></div></div></div>  </li></ul>");
                }
                else {
                    $('#attachlist').append("<ul><li> <div class='memname'><div class='attachment_user_img'><img class=profileimage" + item.userid + " src='userimage/" + filenameShow + "' /></div><h3>" + item.UserName + "</h3><div class='clr'></div></div>  <div class='attachment_details'><div class='attachment_detail_row'><div class='attach_detail'><img width:75px; hieght:75px; src='/TaskTempDoc/" + item.FileName + "' alt='file' /> <p>" + item.FileName + "</p><p style='font-size:x-small'>" + item.UplodedDate + "</p></div><div class='attach_icons'><a class='download_icon' id='File" + item.ActivityLogAttachmentId + "' href='/downloadfiles.aspx?FileNamee=" + item.FileName + "'>" + item.FileName + "</a> <a class='cancel_icon' href='javascript:RemoveAttachmentList(" + item.ActivityLogAttachmentId + ");'>cancel</a></div><div class='clr'></div></div></div>  </li></ul>");
                }
            });

            $('#attachlist').scrollTop(0);
        },
        error: function (error) {

        }
    });


}

function addattachment(fileName) {

    var boardid = $("#boardID").val();
    $.ajax({
        type: "POST",
        url: "/DataTracker.svc/addattachment",
        data: "{\"boardid\": \"" + boardid + "\"}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {

            var userid = data.d[0].userid;
            var username = data.d[0].username;
            var profilepic = data.d[0].filename;
            var datetime = 'just now';
            var lastattachmentid = data.d[0].lastattachmentid;

            var fname = fileName.split('.');
            var ext = fname[1];

            var filenameShow;
            if (profilepic.length > 0) {
                filenameShow = profilepic;
            }
            else {
                filenameShow = "DefaultImage.jpg";
            }
            if (ext == 'sql') {
                $('#attachlist').prepend("<ul><li> <div class='memname'><div class='attachment_user_img'><img class=profileimage" + userid + " src='userimage/" + filenameShow + "' /></div><h3> " + username + "</h3><div class='clr'></div></div>  <div class='attachment_details'><div class='attachment_detail_row'><div class='attach_detail'><img width:75px; height:75px; src='/TaskTempDoc/sql-icon.png' alt='file' /> <p>" + fileName + "</p><p style='font-size:x-small'>" + datetime + "</p></div><div class='attach_icons'><a class='download_icon' id='File" + lastattachmentid + "'  href='/downloadfiles.aspx?FileNamee=" + fileName + "'>" + fileName + "</a> <a class='cancel_icon' href='javascript:RemoveAttachmentList(" + lastattachmentid + ");'>cancel</a></div><div class='clr'></div></div></div>  </li></ul>");
            }
            else if (ext == 'ods') {
                $('#attachlist').prepend("<ul><li> <div class='memname'><div class='attachment_user_img'><img class=profileimage" + userid + " src='userimage/" + filenameShow + "' /></div><h3> " + username + "</h3><div class='clr'></div></div>  <div class='attachment_details'><div class='attachment_detail_row'><div class='attach_detail'><img width:75px; height:75px; src='/TaskTempDoc/ods-icon.png' alt='file' /> <p>" + fileName + "</p><p style='font-size:x-small'>" + datetime + "</p></div><div class='attach_icons'><a class='download_icon' id='File" + lastattachmentid + "'  href='/downloadfiles.aspx?FileNamee=" + fileName + "'>" + fileName + "</a> <a class='cancel_icon' href='javascript:RemoveAttachmentList(" + lastattachmentid + ");'>cancel</a></div><div class='clr'></div></div></div>  </li></ul>");
            }
            else if (ext == 'odt') {
                $('#attachlist').prepend("<ul><li> <div class='memname'><div class='attachment_user_img'><img class=profileimage" + userid + " src='userimage/" + filenameShow + "' /></div><h3> " + username + "</h3><div class='clr'></div></div>  <div class='attachment_details'><div class='attachment_detail_row'><div class='attach_detail'><img width:75px; height:75px; src='/TaskTempDoc/odt-icon.png' alt='file' /> <p>" + fileName + "</p><p style='font-size:x-small'>" + datetime + "</p></div><div class='attach_icons'><a class='download_icon' id='File" + lastattachmentid + "'  href='/downloadfiles.aspx?FileNamee=" + fileName + "'>" + fileName + "</a> <a class='cancel_icon' href='javascript:RemoveAttachmentList(" + lastattachmentid + ");'>cancel</a></div><div class='clr'></div></div></div>  </li></ul>");
            }
            else if (ext == 'ppt') {
                $('#attachlist').prepend("<ul><li> <div class='memname'><div class='attachment_user_img'><img class=profileimage" + userid + " src='userimage/" + filenameShow + "' /></div><h3> " + username + "</h3><div class='clr'></div></div>  <div class='attachment_details'><div class='attachment_detail_row'><div class='attach_detail'><img width:75px; height:75px; src='/TaskTempDoc/ppt-icon.png' alt='file' /> <p>" + fileName + "</p><p style='font-size:x-small'>" + datetime + "</p></div><div class='attach_icons'><a class='download_icon' id='File" + lastattachmentid + "' href='/downloadfiles.aspx?FileNamee=" + fileName + "'>" + fileName + "</a> <a class='cancel_icon' href='javascript:RemoveAttachmentList(" + lastattachmentid + ");'>cancel</a></div><div class='clr'></div></div></div>  </li></ul>");
            }
            else if (ext == 'pptx') {
                $('#attachlist').prepend("<ul><li> <div class='memname'><div class='attachment_user_img'><img class=profileimage" + userid + " src='userimage/" + filenameShow + "' /></div><h3> " + username + "</h3><div class='clr'></div></div>  <div class='attachment_details'><div class='attachment_detail_row'><div class='attach_detail'><img width:75px; height:75px; src='/TaskTempDoc/ppt-icon.png' alt='file' /> <p>" + fileName + "</p><p style='font-size:x-small'>" + datetime + "</p></div><div class='attach_icons'><a class='download_icon' id='File" + lastattachmentid + "'  href='/downloadfiles.aspx?FileNamee=" + fileName + "'>" + fileName + "</a> <a class='cancel_icon' href='javascript:RemoveAttachmentList(" + lastattachmentid + ");'>cancel</a></div><div class='clr'></div></div></div>  </li></ul>");
            }
            else if (ext == 'rtf') {
                $('#attachlist').prepend("<ul><li> <div class='memname'><div class='attachment_user_img'><img class=profileimage" + userid + " src='userimage/" + filenameShow + "' /></div><h3> " + username + "</h3><div class='clr'></div></div>  <div class='attachment_details'><div class='attachment_detail_row'><div class='attach_detail'><img width:75px; height:75px; src='/TaskTempDoc/rtf-icon.png' alt='file' /> <p>" + fileName + "</p><p style='font-size:x-small'>" + datetime + "</p></div><div class='attach_icons'><a class='download_icon' id='File" + lastattachmentid + "' href='/downloadfiles.aspx?FileNamee=" + fileName + "'>" + fileName + "</a> <a class='cancel_icon' href='javascript:RemoveAttachmentList(" + lastattachmentid + ");'>cancel</a></div><div class='clr'></div></div></div>  </li></ul>");
            }
            else if (ext == 'csv') {
                $('#attachlist').prepend("<ul><li> <div class='memname'><div class='attachment_user_img'><img class=profileimage" + userid + " src='userimage/" + filenameShow + "'/></div><h3> " + username + "</h3><div class='clr'></div></div>  <div class='attachment_details'><div class='attachment_detail_row'><div class='attach_detail'><img width:75px; height:75px; src='/TaskTempDoc/csv-icon.png' alt='file' /> <p>" + fileName + "</p><p style='font-size:x-small'>" + datetime + "</p></div><div class='attach_icons'><a class='download_icon' id='File" + lastattachmentid + "' href='/downloadfiles.aspx?FileNamee=" + fileName + "'>" + fileName + "</a> <a class='cancel_icon' href='javascript:RemoveAttachmentList(" + lastattachmentid + ");'>cancel</a></div><div class='clr'></div></div></div>  </li></ul>");
            }
            else if (ext == 'xls') {
                $('#attachlist').prepend("<ul><li> <div class='memname'><div class='attachment_user_img'><img class=profileimage" + userid + " src='userimage/" + filenameShow + "' /></div><h3> " + username + "</h3><div class='clr'></div></div>  <div class='attachment_details'><div class='attachment_detail_row'><div class='attach_detail'><img width:75px; height:75px; src='/TaskTempDoc/xls_logo.png' alt='file' /> <p>" + fileName + "</p><p style='font-size:x-small'>" + datetime + "</p></div><div class='attach_icons'><a class='download_icon' id='File" + lastattachmentid + "' href='/downloadfiles.aspx?FileNamee=" + fileName + "'>" + fileName + "</a> <a class='cancel_icon' href='javascript:RemoveAttachmentList(" + lastattachmentid + ");'>cancel</a></div><div class='clr'></div></div></div>  </li></ul>");
            }
            else if (ext == 'docx') {
                $('#attachlist').prepend("<ul><li> <div class='memname'><div class='attachment_user_img'><img class=profileimage" + userid + " src='userimage/" + filenameShow + "' /></div><h3> " + username + "</h3><div class='clr'></div></div>  <div class='attachment_details'><div class='attachment_detail_row'><div class='attach_detail'><img width:75px; height:75px; src='/TaskTempDoc/doc-icon.png' alt='file' /> <p>" + fileName + "</p><p style='font-size:x-small'>" + datetime + "</p></div><div class='attach_icons'><a class='download_icon' id='File" + lastattachmentid + "' href='/downloadfiles.aspx?FileNamee=" + fileName + "'>" + fileName + "</a> <a class='cancel_icon' href='javascript:RemoveAttachmentList(" + lastattachmentid + ");'>cancel</a></div><div class='clr'></div></div></div>  </li></ul>");
            }
            else if (ext == 'doc') {
                $('#attachlist').prepend("<ul><li> <div class='memname'><div class='attachment_user_img'><img class=profileimage" + userid + " src='userimage/" + filenameShow + "' /></div><h3> " + username + "</h3><div class='clr'></div></div>  <div class='attachment_details'><div class='attachment_detail_row'><div class='attach_detail'><img width:75px; height:75px; src='/TaskTempDoc/doc-icon.png' alt='file' /> <p>" + fileName + "</p><p style='font-size:x-small'>" + datetime + "</p></div><div class='attach_icons'><a class='download_icon' id='File" + lastattachmentid + "'  href='/downloadfiles.aspx?FileNamee=" + fileName + "'>" + fileName + "</a> <a class='cancel_icon' href='javascript:RemoveAttachmentList(" + lastattachmentid + ");'>cancel</a></div><div class='clr'></div></div></div>  </li></ul>");
            }
            else if (ext == 'txt') {
                $('#attachlist').prepend("<ul><li> <div class='memname'><div class='attachment_user_img'><img class=profileimage" + userid + " src='userimage/" + filenameShow + "' /></div><h3> " + username + "</h3><div class='clr'></div></div>  <div class='attachment_details'><div class='attachment_detail_row'><div class='attach_detail'><img width:75px; height:75px; src='/TaskTempDoc/txt-icon.png' alt='file' /> <p>" + fileName + "</p><p style='font-size:x-small'>" + datetime + "</p></div><div class='attach_icons'><a class='download_icon' id='File" + lastattachmentid + "'  href='/downloadfiles.aspx?FileNamee=" + fileName + "'>" + fileName + "</a> <a class='cancel_icon' href='javascript:RemoveAttachmentList(" + lastattachmentid + ");'>cancel</a></div><div class='clr'></div></div></div>  </li></ul>");
            }
            else if (ext == 'pdf') {

                $('#attachlist').prepend("<ul><li> <div class='memname'><div class='attachment_user_img'><img class=profileimage" + userid + " src='userimage/" + filenameShow + "' /></div><h3>" + username + "</h3><div class='clr'></div></div>  <div class='attachment_details'><div class='attachment_detail_row'><div class='attach_detail'><img width:75px; hieght:75px; src='/TaskTempDoc/pdf.png' alt='file' /> <p>" + fileName + "</p><p style='font-size:x-small'>" + datetime + "</p></div><div class='attach_icons'><a class='download_icon' id='File" + lastattachmentid + "' href='/downloadfiles.aspx?FileNamee=" + fileName + "'>" + fileName + "</a> <a class='cancel_icon' href='javascript:RemoveAttachmentList(" + lastattachmentid + ");'>cancel</a></div><div class='clr'></div></div></div>  </li></ul>");
            }
            else if (ext == 'xlsx') {

                $('#attachlist').prepend("<ul><li> <div class='memname'><div class='attachment_user_img'><img class=profileimage" + userid + " src='userimage/" + filenameShow + "' /></div><h3>" + username + "</h3><div class='clr'></div></div>  <div class='attachment_details'><div class='attachment_detail_row'><div class='attach_detail'><img width:75px; hieght:75px; src='/TaskTempDoc/xls_logo.png' alt='file' /> <p>" + fileName + "</p><p style='font-size:x-small'>" + datetime + "</p></div><div class='attach_icons'><a class='download_icon' id='File" + lastattachmentid + "' href='/downloadfiles.aspx?FileNamee=" + fileName + "'>" + fileName + "</a> <a class='cancel_icon' href='javascript:RemoveAttachmentList(" + lastattachmentid + ");'>cancel</a></div><div class='clr'></div></div></div>  </li></ul>");
            }
            else if (ext == 'xml') {

                $('#attachlist').prepend("<ul><li> <div class='memname'><div class='attachment_user_img'><img class=profileimage" + userid + " src='userimage/" + filenameShow + "' /></div><h3>" + username + "</h3><div class='clr'></div></div>  <div class='attachment_details'><div class='attachment_detail_row'><div class='attach_detail'><img width:75px; hieght:75px; src='/TaskTempDoc/xml.png' alt='file' /> <p>" + fileName + "</p><p style='font-size:x-small'>" + datetime + "</p></div><div class='attach_icons'><a class='download_icon' id='File" + lastattachmentid + "' href='/downloadfiles.aspx?FileNamee=" + fileName + "'>" + fileName + "</a> <a class='cancel_icon' href='javascript:RemoveAttachmentList(" + lastattachmentid + ");'>cancel</a></div><div class='clr'></div></div></div>  </li></ul>");
            }
            else {

                $('#attachlist').prepend("<ul><li> <div class='memname'><div class='attachment_user_img'><img class=profileimage" + userid + " src='userimage/" + filenameShow + "'/></div><h3> " + username + "</h3><div class='clr'></div></div>  <div class='attachment_details'><div class='attachment_detail_row'><div class='attach_detail'><img width:75px; height:75px; src='/TaskTempDoc/" + fileName + "' alt='file' /> <p>" + fileName + "</p><p style='font-size:x-small'>" + datetime + "</p></div><div class='attach_icons'><a class='download_icon' id='File" + lastattachmentid + "'  href='/downloadfiles.aspx?FileNamee=" + fileName + "'>" + fileName + "</a> <a class='cancel_icon' href='javascript:RemoveAttachmentList(" + lastattachmentid + ");'>cancel</a></div><div class='clr'></div></div></div>  </li></ul>");
            }

            SendNotificationMailForAttachmentOnBoard(fileName);
            UpdateActivityListItem(fileName, 33);
        },
        error: function (error) {


        }
    });
}

function RemoveAttachmentList(Id) {
    fileId = "#File" + Id;
    var FileName = $(fileId).text();
    var BoardId = $("#boardID").val();
    var check = confirm('Do you want to delete this file ?');
    if (check)
        $.ajax({
            type: "POST",
            url: "/DataTracker.svc/RemoveAttachmentList",
            data: "{\"AttachmentId\": \"" + Id + "\"}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                UpdateActivityListItem(FileName, 32);
                ActivityLogAttachmentList(0);
            },
            error: function (error) {

            }
        });

}

/********************************************* Start Code for make board favourite*********************************************/
/// Created by:- Manoranjan Tiwari
/// Created Date:- 05-Oct-2016
/// Description:-Use for get list of favourite board list.
function FavouriteBoardlist() {

    $("#Favorite").empty();
    $myList = $('#Favorite');
    if ($myList.children().length === 0) {
        $.ajax({
            type: "GET",
            url: "/DataTracker.svc/GetFavoriteBoards",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                $("#Favorite").empty();
                $.each(data.d, function (index, item) {
                    $('#Favorite').append("<li id='list" + item.BoardID + "' style='margin:0; padding:0 0 5px 0;'><a style='float:left;' href='javascript:myBoardurl(" + item.BoardID + ");'  id='" + item.BoardID + "' title='" + item.BoardName + "'>" + item.BoardName + "</a><a  class='testclass'  href='javascript:removeFavouriteboard(" + item.BoardID + ");'>x</a><div class='clr'></div><div id='progress_bar" + item.BoardID + "'></div></li>");

                    $('#progress_bar' + item.BoardID + '').goalProgress({
                        goalAmount: 100,
                        currentAmount: item.TaskCompletePercent,
                        textBefore: '',
                        textAfter: ''
                    });
                });
            }
        });
    }
}

/// Created by:- Manoranjan Tiwari
/// Created Date:- 12-Dec-2016
/// Description:-Use for get archieve board list.
function ArchieveBoardlist() {

    $("#Archive").empty();
    $myList = $('#Archive');
    var operation = "show";
    if ($myList.children().length === 0) {
        $.ajax({
            type: "POST",
            url: "/DataTracker.svc/ShowAllArchivedboards",
            data: "{\"OperationStatus\": \"" + operation + "\"}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                $("#Archive").empty();
                $.each(data.d, function (index, item) {
                    $('#Archive').append("<li id='list" + item.BoardID + "' style='margin:0; padding:0 0 5px 0;'><a style='float:left;' href='javascript:myBoardurl(" + item.BoardID + ");'  id='" + item.BoardID + "' title='" + item.BName + "'>" + item.BName + "</a><a  class='testclass'  href='javascript:removeFavouriteboard(" + item.BoardID + ");'>x</a><div class='clr'></div><div id='progress_bar" + item.BoardID + "'></div></li>");
                });
            }
        });




    }
}

/// Created by:- Manoranjan Tiwari
/// Created Date:- 05-Oct-2016
/// Description:-Use for add board in favourite list.
function AddtoFavorite() {
    var favoriteboardName = $("#BoardName").text();
    var a = BoardID;
    var boardid = $("#boardID").val();
    if ($('#favoriteID').val() == "false") {
        $.ajax({
            type: "POST",
            url: "/DataTracker.svc/InsertFavoriteBoard",
            data: "{\"BoardID\": \"" + boardid + "\",\"createdBy\": \"" + cuid + "\"}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                document.getElementById("FavoriteModifier").innerHTML = "";
                $('#FavoriteModifier').html("&#9829;");
                $('#favoriteID').val("true");
                FavouriteBoardlist();
                UpdateActivityListItem(favoriteboardName, 18);
            }
        });
    }
    else if ($('#favoriteID').val() == "true") {
        $.ajax({
            type: "POST",
            url: "/DataTracker.svc/DeleteFavoriteBoard",
            data: "{\"BoardID\": \"" + boardid + "\",\"createdBy\": \"" + cuid + "\"}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                document.getElementById("FavoriteModifier").innerHTML = "";
                $('#FavoriteModifier').html("&#9825;");
                $('#favoriteID').val("false");
                FavouriteBoardlist();
                UpdateActivityListItem(favoriteboardName, 19);
            }
        });
    }
}
/// Created by:- Manoranjan Tiwari
/// Created Date:- 05-Oct-2016
/// Description:-Use for remove board from favourite list.
function removeFavouriteboard(BoardID) {
    var favoritename = this.$('#' + BoardID + '').text();
    var p = confirm("Are you sure you want to delete this comment ?");
    if (p) {
        $.ajax({
            type: "POST",
            url: "/DataTracker.svc/DeleteFavoriteBoard",
            data: "{\"BoardID\": \"" + BoardID + "\",\"createdBy\": \"" + cuid + "\"}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                document.getElementById("FavoriteModifier").innerHTML = "";
                $('#FavoriteModifier').html("&#9825;");
                $('#favoriteID').val("false");
                var boardId = $('#boardID').val();
                FavouriteBoardlist();
                UpdateActivityListItem(favoritename, 19);
                window.location.href = "/board.aspx?" + boardId;
            }
        });
    }
}

/********************************************* End Code for make board favourite*********************************************/




/*********************************************Start Search card from Board*********************************************/
function GetBoardsCardSearch(sender, args) {
    subtitle = $("#seachcard1").val();
    if (args.keyCode == 13) {
        $("#multi").html("");
        $('#seachcard1').val("");
        $.ajax({
            type: "GET",
            url: "/DataTracker.svc/GetBoardsCardSearch?CartTitle=" + subtitle + "&BoardId=" + BoardID + "",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                $.each(data.d, function (index, item) {
                    if (item.IsSequential == 1) {
                        IsSequential = "Type: Sequential";
                    }
                    else {
                        IsSequential = "Type: Normal";
                    }
                    if (item.CardsNo != 0) {
                        CardsNo = "Cards Limit: " + item.CardsNo;
                    }
                    else { CardsNo = "Cards Limit: Max"; }

                    var projectname = item.ProjectType;

                    if (projectname.length >= 20) { projectname = projectname.substr(0, 20) + '.....'; }
                    else { projectname = projectname; }

                    $('#multi').append("<span id='" + item.ProjectID + "' class='layer tile' ondrop='javascript:OnClickProject(" + item.ProjectID + ");' data-force='30'><div class='project_header'><div class='tile__name' title='Click for edit.' onclick='editlistname(" + item.ProjectID + ");' >" + item.ProjectType + "<img src='/Images/pencil5.png' title='Click for edit.' /></div><div id='list_action'><a href='#' class='linkin' onclick='openarchive(" + item.ProjectID + ")' id=linkin" + item.ProjectID + ">&#8230;</a><div id='pops" + item.ProjectID + "' class='webui-popoverab'><div class='js-pop-over-header' style='position:relative;'><div class='pop-over-header-title1' >List Actions</div><a onclick='closearchive()'   id='archiveclose' href='#' class='pop-close'>×</a></div><div><div class='pop-over-content js-pop-over-content u-fancy-scrollbar js-tab-parent'><ul class='pop-over-list'> <li> <a class='js-close-list'  href='#' onclick='exportsExcel(" + item.ProjectID + "," + BoardID + ")'>Export</a>  </li> </ul><ul class='pop-over-list'><li><a class='js-close-list' href='#' onclick='ArchiveList(" + item.ProjectID + "," + BoardID + ")'>Archive This List</a></li></ul><ul class='pop-over-list'><li><a onclick='showArchivepage(" + item.ProjectID + "," + BoardID + ");' href='#' >Show Archived List..</a></li></ul><hr><ul class='pop-over-list'><li>	<a onclick='openarchivelist(" + item.ProjectID + ")' class='js-archive-cards' href='#'>Archive All Cards in This List…</a></li><li>	<a onclick='openundoarchivelist(" + item.ProjectID + "," + BoardID + ")' class='js-move-cards' href='#'>Show Archived Card…</a></li><li>	<a onclick='AddMemberToList(" + item.ProjectID + "," + BoardID + ")' class='js-move-cards' href='#'>Define list owner</a></li></ul><hr><span><input type='text' class='Cardhoursbylist' id='List_" + item.ProjectID + "'/><span>Hour<span><input type='button' class='btnsetcardhoursbylist' value='Set Card Hours' onclick='UpdateCardDueHoursByList(" + item.ProjectID + ")'/></span></div></div></div></div><div class='clr'></div>    <div id='webui" + item.ProjectID + "' class='pop-over is-shown webuiabc'><div><div class='pop-over-header2 js-pop-over-header'><a onclick='openarchivelistleftarrow(" + item.ProjectID + ")' class='leftarrow2' href='#'>?</a><h3>Archive All Cards in this List?</h3><a onclick='openarchivelistpopclose()'class='pop-close2' href='#'>×</a><div class='clr'></div></div><div><div class='pop-over-content js-pop-over-content u-fancy-scrollbar js-tab-parent' style='max-height: 529px;'><div class='para-part'><p>This will remove all the cards in this list from the board. To view archived cards and bring them back to the board, click  “Show Archived Card…”</p><a href='#' class='js-confirm negate full archive_button' onclick='ArchiveAllCardfromList(" + item.ProjectID + ")' >Archive All</a></div></div></div></div></div> <a onclick='boxdiv(" + item.ProjectID + ")' id='listproprety" + item.ProjectID + "' herf='#'><small class='IsSequential' >" + IsSequential + "</small><small>,&nbsp;</small><small class='Cardlimit'>" + CardsNo + "</small></a><div id='isseq" + item.ProjectID + "' class='box-ab'><div class='box-1a'><h3 id='pname' title='" + item.ProjectType + "'>" + projectname + "<input type='hidden' id='projectId' value=" + item.ProjectID + " /></h3><a onclick='divboxclose(" + item.ProjectID + ")'class='box-close' href='#'>&#10006</a><div class='clr'></div></div><div class='banner'></div><div class='box-1b'><form class='card-box'><label> Max Card Limit:</label><input type='text' id='max-card' onkeypress='return IsNumerictextbox(event);' maxlength='3'>&nbsp;<div class='clr'></div><label>sequential</label><input type='checkbox' name='sequential' value='sequential' id='sequentialcheckbox' ><div class='clr'></div> <div id='rmsg' style='color: #69aa6f;display:none'>Update Successfully</div> <br><a onclick='ChangelimitofCard(" + item.ProjectID + ")' class='edit-box' href='#'>Submit</a><div class='clr'></div> </form></div></div> </div> <div class='tile__list ui-sortable' id='cardlist" + item.ProjectID + "'></div></span>");
                    GetListofcart(item.ProjectID, item.ProjectType, subtitle);
                    $("#boardID").val(BoardID);
                });
                $("#multi").append("<span class='sp'><div class='add_list'><div class='card-composer hide'><input id='txtlistbox' maxlength='75' class='js-search-boards' onkeyup='javascript:AddList(this,event)' type='text' placeholder='Add a list'/> <br/> <div id='CardNoLimit'> <h3>Max Card Limit:</h3> <input  onkeypress='return IsNumerictextbox(event);' maxlength='3' type='text' id='NoOfCards'/> <div class='clr'></div></div> <div id='IsSequentialdiv' ><h3>Is sequential :</h3> <input type='CheckBox' id='IsSeqentialCheckBox' value='1'/> <div class='clr'></div> </div> <a id='addlist' class='fl create_button' href='#' onclick='javascript:AddListBySaveButton()'>Save</a><div id='cancellist' onclick='crossl();' class='crossl'>Cancel</div><p id='listloadstage' class='displayloadstage'>Adding List...</p><div class='clr'></div></div><a onclick='myaddl();' class='addl'>Add a list</a>  </div></span>");
                GetRecentBoards();
                MyBoardsList();
                $('#seachcard1').val("");
            },

        });

    }
    else {

        $("[id$=seachcard1]").autocomplete({
            source: function (request, response) {

                $.ajax({
                    type: "GET",
                    url: "/DataTracker.svc/GetBoardsCardSearch?CartTitle=" + subtitle + "&BoardId=" + BoardID + "",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {
                        response($.map(data.d, function (item) {
                            return {
                                label: item.Title,
                                val: item.Title
                            }
                        }))
                    },
                    error: function (response) {
                        alert(response.responseText);
                    },
                    failure: function (response) {
                        alert(response.responseText);
                    }
                });
            },
            select: function (e, i) {
                $("[id$=hfCustomerId]").val(i.item.val);
            },
            minLength: 1
        });
    }
}

function GetListofcart(ProjectID, ProjectType, Titlename) {
    var composer = "composer" + ProjectID;
    var dfdfdf = "cardaddid" + ProjectID;
    var abc = "#cardlist" + ProjectID;
    $("#" + dfdfdf).remove();
    $(abc).empty();
    $.ajax({
        type: "GET",
        url: "/DataTracker.svc/GetListofcart?Project_ID=" + ProjectID + "&Titlename=" + Titlename + "",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            $.each(data.d, function (index, item) {
                var due_date = "";
                var Task_Description = "";
                var texts = "";
                var bg_color = "";
                var today = new Date();
                var MoveHours = "";
                if (item.MoveDate.length > 0) {
                    MoveHours = Math.round((today - new Date(item.MoveDate)) / 3600000);
                    if (MoveHours > 24) {
                        MoveHours = "This has not been moved from last " + Math.round(MoveHours / 24) + " Days";
                    }
                    else {
                        MoveHours = "This has not been moved from last " + MoveHours + " Hours";
                    }
                }
                if (item.DueDate.length > 0) {
                    due_date = formatDate(item.DueDate);
                    if (due_date == '01 Jan 1990') {
                        due_date = "";
                    }
                    else {
                        due_date = "&#x1f550; " + due_date;
                        someday = new Date(item.DueDate);
                        if (someday > today) {
                            var hours = Math.round((someday - today) / 3600000);
                            if (hours < 24) {
                                texts = "This card is due in less than " + hours + "hours.";
                                bg_color = 'e6b800';
                            }
                            else {
                                texts = "This card is due later.";
                                bg_color = '8cd98c';
                            }
                        } else {
                            texts = "This card has been overdue.";
                            bg_color = 'ff8080';
                        }
                    }
                    if (!(item.Description == "" || item.Description == null)) {
                        Task_Description = "&#8801;";
                    }
                }

                $(abc).append("<li id=" + item.TaskID + " class='ui-state-default movecard' title='" + MoveHours + "'><input type='hidden' id='MoveDate' value='" + item.MoveDate + "'/><a class='cards'   onclick='javascript:ShowModal(" + item.TaskID + ");'><div class='cRdNmE'>" + item.Title + "</div><div class='icon'></div style='padding:5px 0 0 0;'><p id='comment" + item.TaskID + "' title='Comments' class='badge-text2'></p><p class='badge-text3' title='This card has description.'>" + Task_Description + "</p><p id='attachment" + item.TaskID + "' title='Attachments' ></p><div class='showhim' id='showhim" + item.TaskID + "'><img src='Images/icons-cardmen.png' alt='per'/><div class='showme'><ul><li id='mem_task" + item.TaskID + "'></li></ul></div></div><div class='clr'></div></a>");

                NoOfComments(item.TaskID);
                GetMembersInCard(item.TaskID);
                sortable_add_newList();
            });
            $(abc).after("<div id=" + composer + " class='card-composer hide'><div style='color:red; font-size:80%;clear:both;' id='span" + ProjectID + "'></div><textarea maxlength='200'  id='textaera" + ProjectID + "' onkeyup='javascript:AddCardToList(this, event," + ProjectID + "," + BoardID + ",\"" + ProjectType + "\")' class='js-search-boards' placeholder=''></textarea><a id='addNewcard1' class='fl create_button' onclick='javascript:AddCardToListOnAddButtonClick(" + ProjectID + "," + BoardID + ",\"" + ProjectType + "\")'  href='#'>Add</a><div id='cancelcard' onclick='mycross(" + ProjectID + ");' class='cross'>Cancel</div><p id='loadstage' class='displayloadstage'>Adding...</p><a href='#' class='list_action show-pop btn btn-default bottom-right' data-placement='auto-top' ></a>   </div>    <a id=" + dfdfdf + "  onclick='myFunction(" + ProjectID + ");' draggable='false' class='add_card'>Add a card...</a> ");
            $(abc).val('');
        }
    });
}
/******************************************************************End Search card from Board******************************************************/


/******************************************************************Start Export Data to Excel******************************************************/
function exportsExcel(projecttypeid, boardid,sprintid) {
    
    var cards = new Array();
    var projectidd = '#' + projecttypeid;
    var id = projectidd + ' ' + '#cardlist' + projecttypeid+'_'+sprintid;
    $(id).children("li").each(function (index, ele) {
        var cardid = "#" + ele.id + ' ' + '.cRdNmE';
        var cardname = $(cardid).text();
        cards.push(cardname);
    });
    var projectid = '#' + projecttypeid + ' ' + '.tile__name';
    var projectname = $(projectid).text();
    var arr = JSON.stringify({ cards: cards });
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "/DataTracker.svc/exportdata",
        data: "{\"projectname\": \"" + projectname + "\",\"boardid\": \"" + boardid + "\"," + arr + "}",
        dataType: "json",
        success: function (data) {
            
            if (data.d.length > 0) {
                var Path = "/Downloadfiles.aspx?" + data.d;
                document.location.href = Path;
                var projectnameexport = listNameForArchiveList;
                UpdateActivityListItem(projectnameexport, 27);
            }
            else {
                alert("File not downloaded!" + ' ' + data.msg);
            }
        },
        error: function (data) {


        }
    })
}

/******************************************************************End Export Data to Excel******************************************************/
/********************Labels color code ****************************/



/*********************ShowModel Code***************************/
// Ameeq Changes for Swimlanes
function ShowModal1(card_id, BoardId, CardSprintId, priority, Description) {
    //

    if (Description == null || Description == "null") {
        Description = " ";
    }
    $("#divEditMultipleSelection").css("display", "none");
    $('.tile__list').find('input[type=checkbox]:checked').removeAttr('checked');
    $('.spndynamiccolor').css('background-color', '');
    var this_spncolor = "";
    var cardprioritycolor = priority;
    this_spncolor = $('#cardcolorset' + card_id).css('background-color');
    $('.spndynamiccolor').css('background-color', '');
    if (cardprioritycolor != 'undefined' && cardprioritycolor != "") {
        $('.spndynamiccolor').css('background-color', cardprioritycolor);
    }
    if (this_spncolor != cardprioritycolor) {
        $('.spndynamiccolor').css('background-color', this_spncolor);
    }

    $("#SendtoboardID").addClass('archivedisplay');
    $("#ArchiveID").removeClass('archivedisplay');
    $('#popup_list_comment').val("");
    var cardforid = "#" + card_id;
    CardIDForLabelColor = card_id;
    var card_name = $(cardforid).find(".cards .cRdNmE").text();
    cardNameOfComment = card_name;
    var list_name = $(cardforid).parents("span").find(".tile__name").text();

    $("#hide_cardid").val(card_id);
    $("#hide_sprintid").val(CardSprintId);
    $("#popup_cardname").html(card_name + "<img src='/Images/pencil5.png' alt='Edit' title='Click for edit.' />");
    $("#popup_cards_listname").html("'" + list_name + "'");
    IsSubscriber();
    SetDefaultCardColor();
    ShowPopupcolor();
    GetTaskDescription(card_id);
    Bindduedate(card_id);
    bindAttachmentonPop(card_id, cuid);
    showownermember(card_id);
    showboardcomment(card_id);
    var contentRequested = $(".cards").attr("name");   //for stopping model window when drag event is fired 
    if (contentRequested == "ankur") {
        $('.cards').attr('name', '');
    }
    else {
        $("#cardpop").modal('show');
    }
}

function IsSubscriber() {
    var currentuser = $('#cuserid').val();
    var cardid = $('#hide_cardid').val();;
    $.ajax({
        type: "POST",
        url: "/DataTracker.svc/IsSubscriber",
        data: "{\"cardid\": \"" + cardid + "\",\"currentuser\": \"" + currentuser + "\"}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            if (data.d == 1) {
                $(".Subscriber a").html("UnSubscribe");
            }
            else {
                $(".Subscriber a").html("Subscribe");
            }
        },
        error: function (data) {
        }
    });
}
function TaskSubscribed() {

    var Action = $(".Subscriber a").html();
    var BoardID = $('#boardID').val();
    var CardId = $('#hide_cardid').val();
    var ActivityLogID = "35";
    var ActivityLogMessage = "Task Subscribed";
    if (Action == "Subscribe") {
        $.ajax({
            type: "POST",
            url: "/DataTracker.svc/TaskSubscribed",
            data: "{\"BoardID\":\"" + BoardID + "\",\"CardId\":\"" + CardId + "\",\"ActivityLogID\":\"" + ActivityLogID + "\",\"ActivityLogMessage\":\"" + ActivityLogMessage + "\"}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                $(".Subscriber a").html("UnSubscribe");
                alert("Task Subscribed Successfully!");
            },
            error: function (data) {

            }
        });
    }
    else {
        $.ajax({
            type: "POST",
            url: "/DataTracker.svc/TaskUnSubscribed",
            data: "{\"CardId\":\"" + CardId + "\",\"ActivityLogID\":\"" + ActivityLogID + "\"}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                $(".Subscriber a").html("Subscribe");
                alert("Task UnSubscribed Successfully!");
            },
            error: function (data) {

            }
        });
    }

}

function GetBoardMessage() {

    var BoardId = $("#boardID").val();
    $.ajax({
        type: "POST",
        url: "/DataTracker.svc/GetBoardMessage",
        data: "{\"boardid\": \"" + BoardId + "\", \"logtype\":3}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {

            $("#ulshowcomments").empty();
            $.each(data.d, function (index, item) {


                $('#ulshowcomments').append(" <ul><li><div class='header_bar'><h3>" + item.username + "</h3><h5>" + item.activitydate + "</h5><div class='clr'></div></div><div class='message_area'><div id='message1" + item.ActivityLogId + "' class='Message_name'>" + item.message + "</div></div></li></ul>");

            });

            $('#ulshowcomments').scrollTop(0);
        },
        error: function (data) {
        }

    });


}


function sendmessage() {

    $.ajax({
        type: "GET",
        url: "/DataTracker.svc/SendMessage",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {

            var username = data.d;
            var message = $("#almessage").val();
            SendNotificationMailForMessageOnBoard(message.replace(/\n/g, '<br/>'));
            var datetime = 'just now';
            $('#ulshowcomments').prepend(" <ul><li><div class='header_bar'><h3>" + username + "</h3><h5>" + datetime + "</h5><div class='clr'></div></div><div class='message_area'><div class='Message_name' >" + message.replace(/\n/g, '<br/>') + "</div></div></li></ul>");
            $("#almessage").val('');
            $('#ulshowcomments').scrollTop(0);

            $('#btnalmessage').show();
            $("#lblsend").css('display', 'none');
        },
        error: function (data) {

            $('#btnalmessage').show();
            $("#lblsend").css('display', 'none');
        }

    });

}



function EditMessage(id) {

    var Id = "#message" + id;
    var values = $(Id).text();
    var IdtoPrepend = "#message1" + id;
    $(Id).hide();
    $(IdtoPrepend).prepend("<textarea maxlength='75' onblur='EditMessageonblurEvent(" + id + ");'  class='TextEditList1' >" + values + "</textarea>");
    $(IdtoPrepend).find('.TextEditList1').focus();

}

function EditMessageonblurEvent(id) {

    var ids = "#message1" + id;
    var editedlist = $.trim($(ids).find('.TextEditList1').val()).replace(/'/g, "").replace(/\n/g, "").replace(/"/g, "").replace(/\\/g, "").replace(/\//g, "");
    var values = $(ids).text();
    if ($.trim(editedlist).length > 0) {
        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: "/DataTracker.svc/EditMessageonblur",
            data: "{\"Message\": \"" + editedlist + "\",\"MessageId\": \"" + id + "\"}",
            dataType: "json",
            success: function (data) {

                var updateProjectName = values + " to " + editedlist;
                UpdateActivityListItem(updateProjectName, 31);
                $(ids).find('.TextEditList1').remove();
                $(ids).show();
                $(ids).html(data.d + "<img src='/Images/pencil5.png' title='Click for edit.' />");

            }
        })
    }
    else {
        alert("List cannot be empty");
        $(ids).find('.TextEditList1').val(values).focus();
    }
}


var specialKeys = new Array();
specialKeys.push(8);
specialKeys.push(9);
specialKeys.push(46);
specialKeys.push(36);
specialKeys.push(35);
specialKeys.push(37);
specialKeys.push(39);
specialKeys.push(44);
specialKeys.push(32);
function FunIsAlphaNumericallow(e) {

    var keyCode = e.keyCode == 0 ? e.charCode : e.keyCode;
    var ret = ((keyCode == 13) || (keyCode >= 32 && keyCode <= 32) || (keyCode >= 44 && keyCode <= 46) || (keyCode >= 48 && keyCode <= 57) || (keyCode >= 65 && keyCode <= 90) || (keyCode >= 97 && keyCode <= 122) || (specialKeys.indexOf(e.keyCode) != -1 && e.charCode != e.keyCode));
    if (ret == false) {
    }
    return ret;
}



function settings() {

    $("#divsettingsmenu").empty();


    $.ajax({
        type: "POST",
        url: "/DataTracker.svc/getsettingsdetails",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        data: "{\"boardid\":\"" + BoardID + "\"}",
        success: function (data) {

            getcolor();
            if (data.d.length > 0) {


                $("#txtboardname1").val(data.d);


            }
            else {
            }
        },
        error: function () {

        }
    });


}

$('.lblprioritysave').live('blur', function () {

    txtmsgval = $(this).val();
    var spnColorId = $(this).attr('id');
    var mmg = "#" + spnColorId;
    var backcolor = $(mmg).css('background-color');
    hexc(backcolor);

    if (lblhelptext == "Enter") {

        lblhelptext = "";

        return false;

    }
    else {
        if (lbltextval != txtmsgval) {

            $.ajax({
                type: "POST",
                url: "/DataTracker.svc/AddMessageonlabelpriority1",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                data: "{\"color\": \"" + spnColorId + "\",\"Message\": \"" + txtmsgval + "\",\"boardid\": \"" + BoardID + "\"}",

                success: function (data) {

                    ShowPopupcolor();
                    lbltextval = txtmsgval;
                    alert("Label color text added successfully. ");
                },
                error: function () {

                }
            });
        }

    }

});



$('.lblprioritysave').live('keydown', function (e) {

    var key = e.keyCode || e.charCode;

    if (key == 13) {
        txtmsgval = $(this).val();
        var spnColorId = $(this).attr('id');
        var mmg = "#" + spnColorId;
        var backcolor = $(mmg).css('background-color');
        hexc(backcolor);

        $.ajax({
            type: "POST",
            url: "/DataTracker.svc/AddMessageonlabelpriority1",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            data: "{\"color\": \"" + spnColorId + "\",\"Message\": \"" + txtmsgval + "\",\"boardid\": \"" + BoardID + "\"}",

            success: function (data) {

                ShowPopupcolor();
                lblhelptext = "Enter";
                lbltextval = txtmsgval;
                alert("Label color text added successfully. ");
            },
            error: function () {

            }
        });
    }

});


function hexc(colorval) {
    var parts = colorval.match(/^rgb\((\d+),\s*(\d+),\s*(\d+)\)$/);
    delete (parts[0]);
    for (var i = 1; i <= 3; ++i) {
        parts[i] = parseInt(parts[i]).toString(16);
        if (parts[i].length == 1) parts[i] = '0' + parts[i];
    }
    color = '#' + parts.join('');
    GetTitleofCard(color);

}
function hexc1(colorval, id) {
    var parts = colorval.match(/^rgb\((\d+),\s*(\d+),\s*(\d+)\)$/);
    delete (parts[0]);
    for (var i = 1; i <= 3; ++i) {
        parts[i] = parseInt(parts[i]).toString(16);
        if (parts[i].length == 1) parts[i] = '0' + parts[i];
    }
    color = '#' + parts.join('');
    GetTitleofCard1(color, id);

}
function GetTitleofCard(colorcode) {
    $.ajax({
        type: "POST",
        url: "/DataTracker.svc/GetTitleofcardlabel",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        data: "{\"colorcode\": \"" + colorcode + "\",\"boardid\": \"" + BoardID + "\"}",
        success: function (data) {

            for (var i = 0; i < data.d.length; i++) {

                var mmg = "#set" + data.d[i];
                $(mmg).prop("title", txtmsgval);

            }

        },
        error: function (error) {

        }
    });
}

function GetTitleofCard1(colorcode, taskid) {
    $.ajax({
        type: "POST",
        url: "/DataTracker.svc/GetTitleofcardlabel1",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        data: "{\"colorcode\": \"" + colorcode + "\",\"boardid\": \"" + BoardID + "\"}",
        success: function (data) {

            for (var i = 0; i < taskid.length; i++) {

                var taskid1 = taskid[i];

                var mmg = "#set" + taskid1;
                $(mmg).prop("title", data.d);

            }

        },
        error: function (error) {

        }
    });
}

function addcolor1() {

    var colorcollection = ["#a20080", "#780992", "#8742a6", "#1061e0", "#129eb1", "#008e55", "#84ab3d", "#dcc609"];
    var flag = 0;
    var count = $("#divsettingsmenu > div").length;

    if (count == 13) {
        alert("Label color can not be added as maximum limit(12) of label is reached.");
    }
    else {
        for (var i = 0; i < colorcollection.length; i++) {
            var color = colorcollection[i].toString();
            var response = $.ajax({
                type: "POST",
                url: "/DataTracker.svc/CheckDynamicColor",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                data: "{\"color\": \"" + color + "\",\"boardid\": \"" + BoardID + "\"}",
                async: false,
                success: function (data) {

                    if (data.d != 1) {
                        flag = 1;
                        $(".ffg").empty();
                        $('#divsettingsmenu ').append("<div class='a'  style='margin-top: 8px;'><span  id='" + data.d + "' style='width: 9%;float: left;margin: 3px 0 0 0px;margin-left: 45px;height: 19px;display: block;padding: 0px;background-color:" + color + ";'></span> <input type='text' id='" + data.d + "' maxlength=100 onkeypress='return FunIsAlphaNumericallow(event);'  class='lblprioritysave'    style='width: 130px;margin: 3px -80px 0px -84px;'/></div><input type='button' id='" + data.d + "' class='deletelabel1' OnClick='javascript:deletelabel1(" + data.d + ");' value='-' style='float: right;border: none;background-color: #fff;font-size: 29px;margin: -32px 20px 0 0px;cursor: pointer;'>");
                        ShowPopupcolor();
                    }

                },
                error: function () {
                }
            })

            if (flag == 1) {

                return false;
            }
        }
    }

}




function deletelabel1(colorid) {


    var value = confirm("Are you sure you want to delete label color ?");
    if (value) {
        $.ajax({
            type: "POST",
            url: "/DataTracker.svc/DeleteDynamicCardColor",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            data: "{\"color\": \"" + colorid + "\",\"boardid\": \"" + BoardID + "\"}",

            success: function (data) {
                if (data.d == 1) {
                    confirm("Label can not be deleted as it is associated with the card.");
                }
                spnpopupcolor = "";
                getcolor();
                ShowPopupcolor();

            },
            error: function () {
            }
        });
    }

}



function editboardnamefromsettings() {
    var boarnamewidth = $("#hboardname").width();
    var board__name = $('#hboardname').text();
    oldboardname = board__name;
    $('#hboardname').hide();
    $('#hboardname').after("<textarea id='txtboardname' maxlength='75' onblur='javascript:editboardnamefromsettingsonblur();'  onkeyup='javascript:editboardnamefromsettingsonenter(event);'>" + board__name + "</textarea>");
    $("#txtboardname").keydown(function () {
        if ($("#txtboardname").val().length > 1) {
            $("#txtboardname")[0].cols = $("#txtboardname").val().length + 8;
        }
    });

    document.getElementById('txtboardname').focus();
}


function editboardnamefromsettingsonenter(e) {
    var enterkey = 13;
    if (e.which == enterkey) {
        var BoardId = $("#boardID").val();
        var editedboardname = $.trim($('#txtboardname').val()).replace(/'/g, "").replace(/\n/g, "").replace(/"/g, "").replace(/\\/g, "").replace(/\//g, "");

        if ($.trim(editedboardname).length > 0) {
            $.ajax({
                type: "POST",
                url: "/DataTracker.svc/updateboarddetailsforsettings",
                dataType: "json",
                contentType: "application/json;charset=utf-8",
                data: "{\"boardid\":\"" + BoardId + "\",\"boardname\":\"" + editedboardname + "\"}",
                success: function (data) {
                    if (data.d.length > 0) {
                        if (data.d != "exist") {
                            alert("Board name added/updated successfully.");
                            var updatedboardname = oldboardname + " to " + editedboardname;
                            boardnameenterFromsetting = "Enter";
                            UpdateActivityListItem(updatedboardname, 20);
                            $("#txtboardname").remove();
                            $("#hboardname").show().html(data.d + "<img src='/Images/pencil5.png' alt='Edit' title='Click for edit.' />");
                            $("#BoardName").show().html(data.d + "<img src='/Images/pencil5.png' alt='Edit' title='Click for edit.' />");
                            boardName = data.d;
                            GetRecentBoards();
                            MyBoardsList();
                        }
                        else {
                            boardnameenterFromsetting = "Enter";
                            alert("Board name added/updated successfully.");
                            $("#txtboardname").remove();
                            $("#hboardname").show();

                        }

                    }
                    else {
                        alert('failed');
                    }
                },
                error: function (data) {

                }

            });

        }
        else {
            boardnameenterFromsetting = "Enter";
            var board__name = $('#BoardName').text();
            alert("board cannot be empty");
            $('#txtboardname').val(board__name).focus();

        }

    }

}

function editboardnamefromsettingsonblur() {

    var BoardId = $("#boardID").val();
    var editedboardname = $.trim($('#txtboardname').val()).replace(/'/g, "").replace(/\n/g, "").replace(/"/g, "").replace(/\\/g, "").replace(/\//g, "");
    if (boardnameenterFromsetting == "Enter") {
        boardnameenterFromsetting = "";
        return false;
    }
    else {

        if ($.trim(editedboardname).length > 0) {
            $.ajax({
                type: "POST",
                url: "/DataTracker.svc/updateboarddetailsforsettings",
                dataType: "json",
                contentType: "application/json;charset=utf-8",
                data: "{\"boardid\":\"" + BoardId + "\",\"boardname\":\"" + editedboardname + "\"}",
                success: function (data) {
                    if (data.d.length > 0) {
                        if (data.d != "exist") {
                            alert("Board name added/updated successfully.");
                            var updatedboardname = oldboardname + " to " + editedboardname;
                            UpdateActivityListItem(updatedboardname, 20);
                            $("#txtboardname").remove();
                            $("#hboardname").show().html(data.d + "<img src='/Images/pencil5.png' alt='Edit' title='Click for edit.' />");
                            $("#BoardName").show().html(data.d + "<img src='/Images/pencil5.png' alt='Edit' title='Click for edit.' />");
                            var favoriteboardlistid = "#Favorite" + ' ' + "#list" + BoardId;
                            var favoriteboardlistidd = favoriteboardlistid + ' ' + 'a' + "#" + BoardId;
                            $(favoriteboardlistidd).text(editedboardname);
                            boardName = data.d;
                            GetRecentBoards();
                            MyBoardsList();
                        }
                        else {
                            alert("Board name added/updated successfully.");
                            $("#txtboardname").remove();
                            $("#hboardname").show();

                        }

                    }
                    else {
                        alert('failed');
                    }
                },
                error: function (data) {

                }

            });
        }
        else {
            var board__name = $('#BoardName').text();
            alert("board cannot be empty");
            $('#txtboardname').val(board__name).focus();
        }

    }

}
function ShowPopupcolor() {

    $("#lblPopupdivcolor").empty();
    var str = "";
    $.ajax({
        type: "POST",
        url: "/DataTracker.svc/GetCardlabelcolor",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        data: "{\"boardid\":\"" + BoardID + "\"}",
        success: function (data) {
            c = data.d.length;

            if (c != 0) {
                $.each(data.d, function (index, item) {
                    str = "<div><a href='#'><span class='card-label spnSelectcolor' id='" + item.labelcolorid + "'; style=' margin: 10px;margin-left: 2px;height: 25px;display: block;background-color:" + item.color + ";'></span></a><label style='float:left'>" + item.message + "</label><div class='clr'></div></div>";
                    $("#lblPopupdivcolor").append(str);

                });
            }
        },

        error: function () {
        }
    });
}

function ShowPopupcolormulti() {

    $("#Div2").empty();
    var str = "";
    $.ajax({
        type: "POST",
        url: "/DataTracker.svc/GetCardlabelcolor",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        data: "{\"boardid\":\"" + BoardID + "\"}",
        success: function (data) {
            c = data.d.length;

            if (c != 0) {
                $.each(data.d, function (index, item) {
                    str = "<div><a><span class='card-label spnSelectcolor' id='" + item.labelcolorid + "'; style=' margin: 10px;margin-left: 2px;height: 25px;display: block;background-color:" + item.color + ";'></span></a><label style='float:left'>" + item.message + "</label><div class='clr'></div></div>";
                    $("#Div2").append(str);

                });
            }
        },

        error: function () {
        }
    });
}

function SetDefaultCardColor() {

    $.ajax({
        type: "POST",
        url: "/DataTracker.svc/SetDefaultCardColor",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        data: "{\"boardid\": \"" + BoardID + "\"}",

        success: function (data) {

        },
        error: function () {
        }
    });
}


function getcolor() {
    $("#divsettingsmenu").empty();
    var c = 0;
    var fordelete = 0;
    var str = "";
    $.ajax({
        type: "POST",
        url: "/DataTracker.svc/GetCardlabelcolor",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        data: "{\"boardid\":\"" + BoardID + "\"}",
        success: function (data) {
            $("#divsettingsmenu").empty();
            c = data.d.length;
            if (c != 0) {

                $.each(data.d, function (index, item) {
                    if (c >= 4) {
                        if (fordelete <= 3) {
                            str = '<div style="margin-top: 8px;"><div class="a"><span id="' + item.labelcolorid + '"  style="width: 9%;float: left;margin: 3px 0 0 0px;margin-left: 45px;height: 19px;display: block;padding: 0px;background-color:' + item.color + ';" ></span> <input id="' + item.labelcolorid + '" type="text" value="' + item.message + '" maxlength=100   onkeypress="return FunIsAlphaNumericallow(event);"  class="lblprioritysave"  style="width: 130px;margin: 3px -80px 0px -84px;"/></div></div>';
                            $("#divsettingsmenu").append(str);
                            fordelete++;
                        }
                        else {
                            str = '<div style="margin-top: 8px;"><div class="a"><span id="' + item.labelcolorid + '" style="width: 9%;float: left;margin: 3px 0 0 0px;margin-left: 45px;height: 19px;display: block;padding: 0px;background-color:' + item.color + ';" ></span> <input id="' + item.labelcolorid + '" type="text" value="' + item.message + '"  onkeypress="return FunIsAlphaNumericallow(event);"  maxlength=100      class="lblprioritysave"  style="width: 130px;margin: 3px -80px 0px -84px;"/></div><input type="button" OnClick="javascript:deletelabel1(' + item.labelcolorid + ');" class="deletelabel1" id="' + item.labelcolorid + '" value="-" style="float: right;border: none;background-color: #fff;font-size: 29px;margin: -32px 20px 0 0px;cursor: pointer;"></div>';
                            $("#divsettingsmenu").append(str);
                        }

                    }


                });
            }
        },
        error: function () {
        }
    });
}




function exportboardexcel() {

    var arrayofprojectnames = new Array();

    var arrayoftotalcards = new Array();
    var projectlistids = "#multi";
    $(projectlistids).children("span").each(function (index, element) {

        var projectid = "#" + element.id;
        var projectlistid = projectid + ' ' + '.project_header' + ' ' + '.tile__name';
        var projectname = $(projectlistid).text();
        arrayofprojectnames.push(projectname);

        var cards = "#cardlist" + element.id;
        var arrayofcards = new Array();
        $(cards).children("li").each(function (index, el) {

            var a = cards;
            var cardid = "#" + el.id;
            var cardidd = cardid + ' ' + ".cRdNmE";
            var cardname = $(cardidd).text();

            arrayofcards.push(cardname);

        });
        arrayoftotalcards.push(arrayofcards);

    });


    var pcarray = JSON.stringify({ arrayofprojectnames: arrayofprojectnames, arrayoftotalcards: arrayoftotalcards });



    var boardid = $("#boardID").val();
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "/DataTracker.svc/exportdata1",
        data: "{\"boardid\": \"" + boardid + "\"," + pcarray + "}",
        dataType: "json",
        success: function (data) {

            if (data.d.length > 0) {
                if (data.d == "cant export") {
                    alert('cant export...board is empty');
                }
                else {
                    var Path = "/Downloadfiles.aspx?" + data.d;
                    document.location.href = Path;
                    var projectnameexport = listNameForArchiveList;
                    UpdateActivityListItem(projectnameexport, 27);
                }

            }
            else {
                alert("File not downloaded!");
            }
        },
        error: function (data) {


        }
    })
}



function uploadprofile(UID) {


    $.ajax({
        type: "GET",
        url: "/DataTracker.svc/getfile",

        contentType: "application/json; charset=utf-8",
        dataType: "json",

        success: function (data) {
            if (data.d != 0) {

                var filename = data.d;
                var filenamewithsrc = "userimage/" + filename;

                $(".profileimage" + UID).empty();
                $(".profileimage" + UID).attr('src', filenamewithsrc);

            }
            else {
                alert('we are sorry for getting problem in uploading, please try again later...');
            }

        },
        error: function (data) {
        }


    });
}

function ArchiveBoard() {

    var txtArchiveBoardval = $('#txtArchiveBoard').val();

    if (txtArchiveBoardval != "") {
        var p = confirm("Are you sure you want to archive this Board?");
        if (p) {
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "/DataTracker.svc/ArchiveBoard",
                data: "{\"BoardId\": \"" + BoardID + "\",\"reasonarchiveboard\": \"" + txtArchiveBoardval + "\"}",
                dataType: "json",
                success: function (data) {

                    $('#spnArchiveBoard').html("");
                    $("#multi").remove();
                    window.location.href = "AdminDashboard.aspx";
                    SendNotificationMailForArchiveBoard(txtArchiveBoardval);
                },
                error: function (error) {

                }
            });
        }
    }
    else {
        $('#spnArchiveBoard').html("Please mention a reason.");
        $('#spnArchiveBoard').css("color", "red");
    }
}


function ArchiveBoardComplete() {


    $('#spnArchiveBoard').html("");

    var p = confirm("Are you sure  want to archive this Board complete?");
    if (p) {
        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: "/DataTracker.svc/ArchiveBoardComplete",
            data: "{\"BoardId\": \"" + BoardID + "\"}",
            dataType: "json",
            success: function (data) {
                SendNotificationMailForCompleteBoard();
                $("#multi").remove();
                $("#archiveboardcomplete").css("display", "block");
                window.location.href = "AdminDashboard.aspx";

            },
            error: function (error) {

            }
        });
    }

}

function CalculateBoardCompleteWithSprint(Board_id, Sprint_id) {


    $.ajax({
        type: "GET",
        url: "/DataTracker.svc/CalculateBoardCompleteWithSprint?boardid=" + Board_id + "&sprintid=" + Sprint_id + "",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: false,
        success: function (data) {
            BoardComplePer = data.d;
        },
        error: function (error) {

        }
    });

}

function SetProgressBarPer(spid) {
    //var progressbarwithSprint = "#progressbar_" + spid + "";
    //var fullvalue = parseInt(BoardComplePer);
    //$(progressbarwithSprint).text(fullvalue + "% Complete");
    //var progressbar = $(progressbarwithSprint).progressbar({
    //    value: fullvalue,
    //    change: function (event, ui) {
    //        var newVal = $(this).progressbar('option', 'value');
    //        var label = $('.pblabel_' + spid, this).text(newVal + '% Complete');
    //    }
    //});
    //var label = progressbar.find('.pblabel_' + spid).clone().width(progressbar.width());
    //progressbar.find('.ui-progressbar-value').append(label);
}

function SetProgressBarPer(spid) {
    var fullvalue = parseInt(BoardComplePer);
    $('.pblabel_' + spid).text(fullvalue + "% Complete");
    var progressbarwithSprint = "#progressbar_" + spid + "";
    var progressbar = $(progressbarwithSprint).progressbar({
        value: fullvalue,
        change: function (event, ui) {
            var newVal = $(this).progressbar('option', 'value');
            var label = $('.pblabel_' + spid, this).text(newVal + '% Complete');
        }
    });
    var label = progressbar.find('.pblabel_' + spid).clone().width(progressbar.width());
    $('#progressbar_' + spid + ' span').css('display', 'none');
    progressbar.find('.ui-progressbar-value').append(label);
    progressbar.find('.ui-progressbar-value').addClass('ui-progressbar progressbarValue');
    progressbar.find('.ui-progressbar-value').removeClass('ui-progressbar-value ui-widget-header ui-corner-left');
}

//function SetProgressBarPer() {

//    var fullvalue = parseInt(BoardComplePer);
//    $('.pblabel').text(fullvalue + "% Complete");
//    var progressbar = $('#progressbar').progressbar({
//        value: fullvalue,
//        change: function (event, ui) {
//            var newVal = $(this).progressbar('option', 'value');
//            var label = $('.pblabel', this).text(newVal + '% Complete');
//        }
//    });
//    var label = progressbar.find('.pblabel').clone().width(progressbar.width());
//    progressbar.find('.ui-progressbar-value').append(label);

//}

//function GetTotalTimeEstimate() {

//    $("#barforestimate").empty();
//    $.ajax({
//        type: "GET",
//        url: "/DataTracker.svc/GetTotalTimeEstimate?BoardId=" + BoardID + "",
//        contentType: "application/json; charset=utf-8",
//        dataType: "json",
//        async: false,
//        success: function (data) {
//            var TotalHours = data.d[0].TotalHours;
//            var TotalEstimedHours = data.d[0].TotalEstimedHours;
//            var colorOfProgress = data.d[0].colorEstimate;
//            var values1 = TotalHours.split(":");

//            var thv = parseFloat(values1[0]);
//            var thv1 = parseFloat(values1[1]);
//            var totalhv1 = thv + "." + thv1;
//            var TotalHours1 = parseFloat(totalhv1);
//            var values = TotalEstimedHours.split(":")
//            var tehv1 = parseFloat(values[0]);
//            var tehv2 = parseFloat(values[1]);

//            var totaleshv2 = tehv1 + "." + tehv2;
//            var TotalEstimedHours1 = parseFloat(totaleshv2);
//            if (TotalEstimedHours1 >= TotalHours1) {
//                $("#barforestimate_").removeAttr('title');
//                $("#barforestimate_").css('background-color', '');

//                $("#barforestimate_").append("<div class='hourr'><P>" + TotalHours + "</p><div class='hourcomplete'>" + TotalEstimedHours + "</div></div>");
//            }
//            else if (TotalEstimedHours1 < TotalHours1) {
//                $("#barforestimate_").attr('title', 'Consumed hours is greater than estimated hours');
//                $("#barforestimate_").append("<div class='hourr7'><P>" + TotalHours + "</p><div class='hourcomplete7'>" + TotalEstimedHours + "</div></div>");
//            }
//        },
//        error: function (error) {
//        }
//    });


//}

function GetTotalTimeEstimate(TotalTime, TotalEstimedHours, spid) {
    var barforestimatewithSprint = "#barforestimate_" + spid + "";
    $(barforestimatewithSprint).empty();
    var hours = 0;
    var minutes = 0;
    if (TotalTime != null) {
        var milliseconds = Math.abs(TotalTime);
        hours = Math.floor(milliseconds / 60);
        var hr = hours;
        minutes = Math.floor(TotalTime - (hours * 60));
        var min = minutes;
        if (hr < 9) {
            hr = '0' + hr;
        }
        if (min < 9) {
            min = '0' + min;
        }
        if (hours > 0 && minutes > 0) {
            TotalHours = hr + ":" + min;

        }
        else if (hours <= 0 && minutes > 0) {
            TotalHours = "00:" + min;
        }
        else if (hours > 0 && minutes <= 0) {
            TotalHours = hr + ":" + " 00";
        }
        else {
            TotalHours = "00:" + "00";
        }
    }
    else {
        TotalHours = "00:" + "00";
    }
    if (TotalEstimedHours != null) {
        if (TotalEstimedHours != "00" && TotalEstimedHours != "0") {
            TotalEstimedHours = TotalEstimedHours + ":" + "00";
        }
        else {
            TotalEstimedHours = "00" + ":00";
        }
    }
    else {
        TotalEstimedHours = "00" + ":00";
    }
    var values1 = TotalHours.split(":");
    var thv = parseFloat(values1[0]);
    var thv1 = parseFloat(values1[1]);
    var totalhv1 = thv + "." + thv1;
    var TotalHours1 = parseFloat(totalhv1);
    var values = TotalEstimedHours.split(":");
    var tehv1 = parseFloat(values[0]);
    var tehv2 = parseFloat(values[1]);

    var totaleshv2 = tehv1 + "." + tehv2;
    var TotalEstimedHours1 = parseFloat(totaleshv2);
    if (TotalEstimedHours1 >= TotalHours1) {
        $(barforestimatewithSprint).removeAttr('title');
        $(barforestimatewithSprint).css('background-color', '');

        $(barforestimatewithSprint).append("<div class='hourr'><P class='hourr_" + spid + "'>" + TotalHours + "</p><div class='hourcomplete hourcomplete_" + spid + "'>" + TotalEstimedHours + "</div></div>");
    }
    else if (TotalEstimedHours1 < TotalHours1) {
        $(barforestimatewithSprint).attr('title', 'Consumed hours is greater than estimated hours');
        $(barforestimatewithSprint).css({ 'background-color': 'red', 'color': '#ffff' });
        $(barforestimatewithSprint).append("<div class='hourr'><P class='hourr_" + spid + "'>" + TotalHours + "</p><div class='hourcomplete hourcomplete_" + spid + "'>" + TotalEstimedHours + "</div></div>");
    }

    //$.ajax({
    //    type: "GET",
    //    url: "/DataTracker.svc/GetTotalTimeEstimateWithSprint?BoardId=" + BoardID + "&SprintId=" + spid + "",
    //    contentType: "application/json; charset=utf-8",
    //    dataType: "json",
    //    async: false,
    //    success: function (data) {
    //        var TotalHours = data.d[0].TotalHours;
    //        var TotalEstimedHours = data.d[0].TotalEstimedHours;
    //        var colorOfProgress = data.d[0].colorEstimate;
    //        var values1 = TotalHours.split(":");

    //        var thv = parseFloat(values1[0]);
    //        var thv1 = parseFloat(values1[1]);
    //        var totalhv1 = thv + "." + thv1;
    //        var TotalHours1 = parseFloat(totalhv1);
    //        var values = TotalEstimedHours.split(":");
    //        var tehv1 = parseFloat(values[0]);
    //        var tehv2 = parseFloat(values[1]);

    //        var totaleshv2 = tehv1 + "." + tehv2;
    //        var TotalEstimedHours1 = parseFloat(totaleshv2);
    //        if (TotalEstimedHours1 >= TotalHours1) {
    //            $(barforestimatewithSprint).removeAttr('title');
    //            $(barforestimatewithSprint).css('background-color', '');

    //            $(barforestimatewithSprint).append("<div class='hourr'><P>" + TotalHours + "</p><div class='hourcomplete hourcomplete_" + spid + "'>" + TotalEstimedHours + "</div></div>");
    //        }
    //        else if (TotalEstimedHours1 < TotalHours1) {
    //            $(barforestimatewithSprint).attr('title', 'Consumed hours is greater than estimated hours');
    //            $(barforestimatewithSprint).append("<div class='hourr7'><P>" + TotalHours + "</p><div class='hourcomplete7'>" + TotalEstimedHours + "</div></div>");
    //        }
    //    },
    //    error: function (error) {
    //    }
    //});


}

function CalculateBoardComplete(Board_id) {


    $.ajax({
        type: "GET",
        url: "/DataTracker.svc/CalculateBoardComplete?boardid=" + Board_id + "",

        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: false,
        success: function (data) {

            BoardComplePer = data.d;


        },
        error: function (error) {

        }
    });

}







function second() {

    var fullvalue = parseInt(BoardComplePer);
    var newVal = parseInt(fullvalue + " Complete", 10);
    $('#progressbar').progressbar('value', newVal);

}

function EditMultipleSelection() {
    SetDefaultCardColor();
    ShowPopupcolormulti();

    $("#cardpop1").modal('show');

}

function UncheckAllSelectedid() {
    $('.tile__list').find('input[type=checkbox]:checked').removeAttr('checked');
    $('.tile__list').find('input[type=checkbox]').css("display", "none");
    $("#divEditMultipleSelection").css("display", "none");

}

function SetCardTImeColor() {
    var totalminute = 0;
    var avgtime = parseInt($("#txtCardEstimatedTime").val());
    var avgtime1 = parseInt($("#txtCardEstimatedminute").val());
    if (isNaN(avgtime)) {
        avgtime = 0;
    }
    if (isNaN(avgtime1)) {
        avgtime1 = 0;
    }
    var id = 0;
    var timeinminute = 0;
    var minute = parseInt((avgtime * 60) + avgtime1);
    totalminute = parseInt((minute * 0.25) + minute);

    for (var i = 0; i < carididArray.length; i++) {
        var splt = carididArray[i].split(",");
        timeinminute = splt[0];
        id = splt[1];
        if (minute <= timeinminute && timeinminute <= totalminute) {
            $("#settimecolor" + id).css('background', '');
            $("#settimecolor" + id).css('background', 'orange');
        }

        else {
            if (timeinminute < minute) {
                $("#settimecolor" + id).css('background', '');
                $("#settimecolor" + id).css('background', 'rgb(239,239,239)');
            }
            else {
                $("#settimecolor" + id).css('background', '');
                $("#settimecolor" + id).css('background', 'rgb(255, 0, 0)');
            }

        }
    }

}



$('#txtCardEstimatedTime').live('blur', function () {

    var Estimatedvalminute = $('#txtCardEstimatedminute').val().trim();
    var Estimatedval = $('#txtCardEstimatedTime').val().trim();
    if (text == "Enter") {
        text = "";
        return false;
    }
    else {
        if (estboardcardtime1 != Estimatedval) {
            if (Estimatedval > 100) {

                swal("That’s a lot of hours for each card! We recommend that you further divide the cards and keep the average time needed to complete each card lesser, that makes projects much more measurable and easy to track.");

                return false;
            }
            if (Estimatedval == 100) {
                if (Estimatedvalminute.length > 0) {


                    swal("That’s a lot of hours for each card! We recommend that you further divide the cards and keep the average time needed to complete each card lesser, that makes projects much more measurable and easy to track.");
                    $('#txtCardEstimatedminute').focus();
                    $('#txtCardEstimatedTime').val(estboardcardtime1);
                    return false;
                }
            }

            if (Estimatedval != "" && Estimatedval != undefined && Estimatedval != null) {
                var Estimatedval1 = Estimatedval + "." + Estimatedvalminute;
                $.ajax({
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    url: "/DataTracker.svc/CardEstimatedTime",
                    data: "{\"boardid\":\"" + BoardID + "\",\"CardEstimatedTime\":\"" + Estimatedval1 + "\"}",
                    dataType: "json",
                    success: function (data) {
                        estboardcardtime1 = $('#txtCardEstimatedTime').val();
                        alert("Card Estimated hours save successfully.");
                        SetCardTImeColor();

                    },
                    error: function (error) {
                    }
                })
            }
            else {
                alert("Please enter estimated hours for card.");
                $('#txtCardEstimatedTime').val(estboardcardtime1);
            }
        }
    }
});

$('#txtCardEstimatedminute').live('blur', function () {

    var Estimatedvalminute = $('#txtCardEstimatedminute').val().trim();
    var Estimatedval = $('#txtCardEstimatedTime').val().trim();

    if (Estimatedvalminute > 59) {
        return false;
    }
    if (text == "Enter") {
        text = "";
        return false;
    }
    else {
        if (estboardcardtime2 != Estimatedvalminute) {
            if (Estimatedval > 100) {

                swal("That’s a lot of hours for each card! We recommend that you further divide the cards and keep the average time needed to complete each card lesser, that makes projects much more measurable and easy to track.");

                return false;
            }
            if (Estimatedval == 100) {
                if (Estimatedvalminute.length > 0) {
                    swal("That’s a lot of hours for each card! We recommend that you further divide the cards and keep the average time needed to complete each card lesser, that makes projects much more measurable and easy to track.");

                    $('#txtCardEstimatedminute').focus();
                    return false;
                }
            }

            if (Estimatedvalminute != "") {
                var Estimatedval1 = Estimatedval + "." + Estimatedvalminute;
                $.ajax({
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    url: "/DataTracker.svc/CardEstimatedTime",
                    data: "{\"boardid\":\"" + BoardID + "\",\"CardEstimatedTime\":\"" + Estimatedval1 + "\"}",
                    dataType: "json",
                    success: function (data) {

                        estboardcardtime2 = $('#txtCardEstimatedminute').val();
                        alert("Card Estimated time save successfully.");
                        SetCardTImeColor();
                    },
                    error: function (error) {
                    }
                })
            }
            else {
                alert("Please enter estimated time for card.");
                $('#txtCardEstimatedminute').val(estboardcardtime2);
            }
        }
    }
});

$(function () {
    $('#txtCardEstimatedTime').keypress(function (event) {

        var Estimatedvalminute = $('#txtCardEstimatedminute').val().trim();
        var Estimatedval = $('#txtCardEstimatedTime').val().trim();
        if (event.keyCode == 13) {
            if (estboardcardtime1 != Estimatedval) {
                if (Estimatedval > 100) {
                    swal("That’s a lot of hours for each card! We recommend that you further divide the cards and keep the average time needed to complete each card lesser, that makes projects much more measurable and easy to track.");

                    return false;
                }
                if (Estimatedval == 100) {
                    if (Estimatedvalminute.length > 0) {
                        swal("That’s a lot of hours for each card! We recommend that you further divide the cards and keep the average time needed to complete each card lesser, that makes projects much more measurable and easy to track.");
                        $('#txtCardEstimatedminute').focus();
                        return false;
                    }
                }

                if (Estimatedval != "" && Estimatedval != undefined && Estimatedval != null) {
                    var Estimatedval1 = Estimatedval + "." + Estimatedvalminute;
                    $.ajax({
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        url: "/DataTracker.svc/CardEstimatedTime",
                        data: "{\"boardid\":\"" + BoardID + "\",\"CardEstimatedTime\":\"" + Estimatedval1 + "\"}",
                        dataType: "json",
                        success: function (data) {

                            estboardcardtime1 = $('#txtCardEstimatedTime').val();
                            alert("Card Estimated hours save successfully.");
                            SetCardTImeColor();
                        },
                        error: function (error) {
                        }
                    })
                }
                else {
                    text = "Enter";
                    alert("Please enter estimated hours for card.");
                    $('#txtCardEstimatedTime').val(estboardcardtime1);
                }
            }
        }
    })
})


$(function () {
    $('#txtCardEstimatedminute').keypress(function (event) {
        var Estimatedvalminute = $('#txtCardEstimatedminute').val().trim();
        var Estimatedval = $('#txtCardEstimatedTime').val().trim();
        if (Estimatedvalminute > 59) {
            return false;
        }
        if (event.keyCode == 13) {
            if (estboardcardtime2 != Estimatedvalminute) {
                if (Estimatedval > 100) {
                    swal("That’s a lot of hours for each card! We recommend that you further divide the cards and keep the average time needed to complete each card lesser, that makes projects much more measurable and easy to track.");

                    return false;
                }
                if (Estimatedval == 100) {
                    if (Estimatedvalminute.length > 0) {

                        swal("That’s a lot of hours for each card! We recommend that you further divide the cards and keep the average time needed to complete each card lesser, that makes projects much more measurable and easy to track.");
                        $('#txtCardEstimatedminute').focus();
                        return false;
                    }
                }

                if (Estimatedvalminute != "") {
                    var Estimatedval1 = Estimatedval + "." + Estimatedvalminute;
                    $.ajax({
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        url: "/DataTracker.svc/CardEstimatedTime",
                        data: "{\"boardid\":\"" + BoardID + "\",\"CardEstimatedTime\":\"" + Estimatedval1 + "\"}",
                        dataType: "json",
                        success: function (data) {

                            estboardcardtime2 = $('#txtCardEstimatedminute').val();
                            alert("Card Estimated time save successfully.");
                            SetCardTImeColor();
                        },
                        error: function (error) {
                        }
                    });
                }
                else {
                    text = "Enter";
                    alert("Please enter estimated time for card.");
                    $('#txtCardEstimatedminute').val(estboardcardtime2);
                }
            }
        }
    })
})

//$('#txtEstimated').live('blur', function () {

//    var Estimatedval = $('#txtEstimated').val().trim();
//    if (text == "Enter") {
//        text = "";
//        return false;
//    }
//    else {
//        if (estboardtime1 != Estimatedval) {
//            if (Estimatedval != "") {
//                $.ajax({
//                    type: "POST",
//                    contentType: "application/json; charset=utf-8",
//                    url: "/DataTracker.svc/BoardEstimated",
//                    data: "{\"boardid\":\"" + BoardID + "\",\"EstimatedHours\":\"" + Estimatedval + "\"}",
//                    dataType: "json",
//                    success: function (data) {

//                        GetTotalTimeEstimate();
//                        estboardtime1 = $('#txtEstimated').val();
//                        alert("Board estimated hours saved successfully.");
//                    },
//                    error: function (error) {
//                    }
//                })
//            }
//            else {

//                alert("Please enter estimated hours for board.");
//                $('#txtEstimated').val(estboardtime1);
//            }
//        }
//    }
//});

//$(function () {
//    $('#txtEstimated').keypress(function (event) {

//        var Estimatedval = $('#txtEstimated').val().trim();
//        if (event.keyCode == 13) {

//            if (estboardtime1 != Estimatedval) {
//                if (Estimatedval != "") {
//                    $.ajax({
//                        type: "POST",
//                        contentType: "application/json; charset=utf-8",
//                        url: "/DataTracker.svc/BoardEstimated",
//                        data: "{\"boardid\":\"" + BoardID + "\",\"EstimatedHours\":\"" + Estimatedval + "\"}",
//                        dataType: "json",
//                        success: function (data) {

//                            GetTotalTimeEstimate();
//                            estboardtime1 = $('#txtEstimated').val();

//                            alert("Board estimated hours save successfully.");
//                        },
//                        error: function (error) {
//                        }
//                    })
//                }
//                else {
//                    text = "Enter";
//                    alert("Please enter estimated hours for board.");
//                    $('#txtEstimated').val(estboardtime1);
//                }
//            }
//        }
//    })

//})

$('#txtestimatetime').live('blur', function () {

    var Estimatedval = $('#txtestimatetime').val().trim();
    var spid = $('#hdsprintestimatetime').val()
    if (text == "Enter") {
        text = "";
        return false;
    }
    else {
        if (estboardtime1 != Estimatedval) {
            if (Estimatedval != "") {
                $.ajax({
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    url: "/DataTracker.svc/BoardEstimated",
                    data: "{\"boardid\":\"" + BoardID + "\",\"EstimatedHours\":\"" + Estimatedval + "\",\"sprintid\":\"" + spid + "\"}",
                    dataType: "json",
                    success: function (data) {

                        GetTotalTimeEstimate();
                        estboardtime1 = $('#txtestimatetime').val();
                        $('.hourcomplete_' + spid).html(Estimatedval + ':00');
                        var TotallogTime = $('.hourr_' + spid).html() + 1;
                        TotallogTime = TotallogTime * 60;
                        Estimatedval = Estimatedval * 60;
                        if (TotallogTime > Estimatedval) {
                            $('#barforestimate_' + spid).css({ 'background-color': 'red', 'color': '#ffff' });
                        }
                        else {
                            $('#barforestimate_' + spid).css('background-color', '#ffff')
                        }
                        alert("Sprint estimated hours saved successfully.");
                        $('.blocker').css('display', 'none');
                    },
                    error: function (error) {
                    }
                })
            }
            else {

                alert("Please enter estimated hours for sprint.");
                $('#txtestimatetime').val('');
            }
        }
    }
});

$(function () {
    $("#txtestimatetime").keydown(function (e) {
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
            // Allow: Ctrl+A, Command+A
            (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) ||
            // Allow: home, end, left, right, down, up
            (e.keyCode >= 35 && e.keyCode <= 40)) {
            // let it happen, don't do anything
            return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });
    $('#txtestimatetime').on('click', function () {
        $('#txtestimatetime').val('');
    });
    $('#txtestimatetime').keypress(function (event) {
        var Estimatedval = $('#txtestimatetime').val().trim();
        var spid = $('#hdsprintestimatetime').val()
        if (event.keyCode == 13) {
            if (estboardtime1 != Estimatedval) {
                if (Estimatedval != "") {
                    $.ajax({
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        url: "/DataTracker.svc/BoardEstimated",
                        data: "{\"boardid\":\"" + BoardID + "\",\"EstimatedHours\":\"" + Estimatedval + "\",\"sprintid\":\"" + spid + "\"}",
                        dataType: "json",
                        success: function (data) {
                            GetTotalTimeEstimate();
                            estboardtime1 = $('#txtestimatetime').val();
                            $('.hourcomplete_' + spid).html(Estimatedval + ':00');
                            var TotallogTime = $('.hourr_' + spid).html().split(':');
                            TotallogTime = TotallogTime[0] + 1;
                            TotallogTime = TotallogTime * 60;
                            Estimatedval = Estimatedval * 60;
                            if (TotallogTime > Estimatedval) {
                                $('#barforestimate_' + spid).css('background-color', 'red');
                            }
                            else {
                                $('#barforestimate_' + spid).css('background-color', '#fff')
                            }
                            alert("Sprint estimated hours save successfully.");
                            $('.blocker').css('display', 'none');
                        },
                        error: function (error) {
                        }
                    })
                }
                else {
                    text = "Enter";
                    alert("Please enter estimated hours for sprint.");
                    $('#txtestimatetime').val('');
                }
            }
        }
    })

});

function RenameSprint() {
    var _SwimlanesName = $('#rename_Swimlanes').val().trim();
    var _spid = $('#hdSwimlanesid').val()
         
        if (_SwimlanesName != "") {
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "/DataTracker.svc/RenameSwimlanes",
                data: JSON.stringify({ boardid: BoardID, spid: _spid, SwimlanesName: _SwimlanesName }),
                //data: "{\"spid\":\"" + spid + "\",\"SwimlanceName\":\"" + SwimlanceName + "\"}",
                dataType: "json",
                success: function (data) {
                    if (data.d == "1") {
                        $('#ShowSprint_' + _spid + ' span').html(_SwimlanesName);
                        $('#HideSprint_' + _spid + ' span').html(_SwimlanesName);
                        alert('Swimlanes update successfully!!');
                    }
                    else if (data.d == "2") {
                        alert('Swimlanes already exist!!');
                    }
                    else if (data.d == "3") {
                        alert('Swimlanes already exist in arrchived list!!');
                    }
                    $('.blocker').css('display', 'none');
                },
                error: function (error) {
                }
            })
        }
        else {
            text = "Enter";
            alert("Please enter Sprint Name.");
            $('#rename_Swimlanes').val('');
        }
}

function ArchiveSwimlanes() {
    var _spid = $('#hdSwimlanesid').val()
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "/DataTracker.svc/ArchiveSwimlanes",
        data: JSON.stringify({ spid: _spid }),
        dataType: "json",
        success: function (data) {
            if (data.d == "1") {
                $('#ShowSprint_' + _spid).css('display', 'none');
                $('.multi_' + _spid).css('display', 'none');
                $('#HideSprint_' + _spid).css('display', 'none');
                $('.Sprint_' + _spid).css('display', 'none');
                $('#hr0_' + _spid).css('display', 'none');
                alert('Swimlanes Archived!!');
            }
            $('.blocker').css('display', 'none');
        },
        error: function (error) {
        }
    })
}

function DeleteSwimlanes() {
    if (confirm("Are you sure!"))
    {
        var _spid = $('#hdSwimlanesid').val()
        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: "/DataTracker.svc/DeleteSwimlanes",
            data: JSON.stringify({ spid: _spid }),
            dataType: "json",
            success: function (data) {
                if (data.d == "1") {
                    $('#ShowSprint_' + _spid).css('display', 'none');
                    $('.multi_' + _spid).css('display', 'none');
                    $('#HideSprint_' + _spid).css('display', 'none');
                    $('.Sprint_' + _spid).css('display', 'none');
                    $('#hr0_' + _spid).css('display', 'none');
                    alert('Swimlanes Deleted!!');
                }
                $('.blocker').css('display', 'none');
            },
            error: function (error) {
            }
        })
    }
}

function show_TeamMembers(ownername) {
    var usertype = 0;
    var tempUserId = 0;
    var flag = 0;
    var CUserId = 0;
    var OnerUserId = 0;

    document.getElementById("ShowMember").innerHTML = "";
    $.ajax({
        type: "POST",
        url: "/DataTracker.svc/BindMembers",
        data: "{\"teamid\": \"" + teamIdForOwner + "\"}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            if (data.d[0].userId != -2) {
                $.each(data.d, function (index, item) {
                    if (item.userName == ownername) {
                        flag = 1;
                    }
                });
                if (flag == 0) {
                    $('#ShowMember').append("<option value='0'>" + ownername + "</option>");
                }
                $.each(data.d, function (index, item) {
                    tempUserId = item.tempUserId;
                    usertype = item.useType;
                    $('#ShowMember').append("<option value=" + item.userId + ">" + item.userName + "</option>"); // Added by Ravi to bind dropdown on create Board PopUp.
                });
                $('#currentboardteammember').val(users);
                CUserId = data.d[0].tempUserId;
                OnerUserId = $('#ownerID').val();

                if (usertype == 12 || usertype == 14 || CUserId == OnerUserId) {
                    $("#ShowMember").removeAttr("disabled");
                    $("#boardduedate").removeAttr("disabled");
                    $("#ckCompleteBoardId").removeAttr("disabled");
                    $("#Clients").removeAttr("disabled");
                    $("#AccountManager").removeAttr("disabled");

                }

                $("#ShowMember option:contains(" + ownername + ")").attr('selected', 'selected');
            }
            else {
                $('#ShowMember').append("<option value='0'>" + ownername + "</option>");
                tempUserId = data.d[0].tempUserId;
                OnerUserId = $('#ownerID').val();
                usertype = data.d[0].useType;
                if (usertype == 12 || usertype == 14 || CUserId == OnerUserId) {
                    $("#ShowMember").removeAttr("disabled");
                    $("#boardduedate").removeAttr("disabled");
                    $("#ckCompleteBoardId").removeAttr("disabled");
                    $("#Clients").removeAttr("disabled");
                    $("#AccountManager").removeAttr("disabled");

                }
            }

        }


    });


}

function GetOwnerName() {
    $.ajax({
        type: "POST",
        url: "/DataTracker.svc/GetOwnerName",
        data: "{\"boardid\":\"" + BoardID + "\"}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            if (data.d != "" && data.d != undefined) {
                show_TeamMembers(data.d);
            }
            else {
                show_TeamMembers("None");
            }

        }
    });
}

function OnSelectMember() {

    var userid = $("#ShowMember").val();
    var teamid = $("#teamID").val();
    $.ajax({
        type: "POST",
        url: "/DataTracker.svc/UpdateBoardOwner",
        data: "{\"boardid\":\"" + BoardID + "\",\"userid\":\"" + userid + "\",\"teamid\":\"" + teamid + "\"}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {

        }
    });

}

function CheckBoardCompletionFlag() {

    $.ajax({
        type: "POST",
        url: "/DataTracker.svc/CheckBoardCompletionFlag",
        data: "{\"boradid\":\"" + BoardID + "\"}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {

            if (data.d[0].Flag == "1") {
                $('#progressbar').css('display', 'block');
                $("#ckCompleteBoardId").attr("checked", true);
            } else {
                $('#progressbar').css('display', 'none');
                $("#ckCompleteBoardId").attr("checked", false);
                $("#barforestimate").css('right', '100px');
            }
        }
    });
}

function ChaneBoardCompletion() {
    var check = confirm("Are you sure you want to Hide/Unhide Board Percentage?");

    var isChecked = $("#ckCompleteBoardId").is(":checked");

    if (check) {
        if (isChecked) {
            $.ajax({
                type: "POST",
                url: "/DataTracker.svc/UpdateBoardCompletioncheck",
                data: "{\"boradid\":\"" + BoardID + "\"}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    $('#progressbar').css('display', 'block');
                    $("#ckCompleteBoardId").attr("checked", true);

                    $("#barforestimate").css('right', '305px');
                }
            });
        }
        else {
            $.ajax({
                type: "POST",
                url: "/DataTracker.svc/UpdateBoardCompletion",
                data: "{\"boradid\":\"" + BoardID + "\"}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    $('#progressbar').css('display', 'none');
                    $("#ckCompleteBoardId").attr("checked", false);
                    $("#barforestimate").css('right', '100px');
                }
            });
        }
    }
    else {
        if (isChecked) {
            $("#ckCompleteBoardId").attr("checked", false);
        }
        else {
            $("#ckCompleteBoardId").attr("checked", true);
        }
    }
}

function GetBoardOwnerID(BID) {

    $.ajax({
        type: "GET",
        url: "/DataTracker.svc/CheckOwnerId?boardid=" + BID + "",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: false,
        success: function (data) {
            if (data) {
                var returnsingleval = data.d;
                $('#ownerID').val(returnsingleval);
            }
        },
        error: function (error) {
        }
    });
}


$(function () {
    $('.curentindex').click(function () {
        $('#TotalEmployee').empty();
        $('#txtdate').val('');
        flagexport = $(this).attr('id');
        var tab_id = $(this).attr('data-tab');

        $.ajax({
            type: "POST",
            url: "/DataTracker.svc/TotalEmployeeSheet",
            data: "{\"boardid\":\"" + BoardID + "\"}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                $('#TotalEmployee').empty();
                if (data.d.length > 0) {
                    var TotalEmployee = $('#TotalEmployee');
                    $(TotalEmployee).append("<table class='effort_table'><tr class='effort_tr'><th class='effort_th'>Name</th><th class='effort_th'>Total Time</th></tr>");

                    $.each(data.d, function (index, item) {

                        var totaltime = item.TotalTime.split(':');

                        $(TotalEmployee).append("<tr class='effort_tr'><td class='effort_td'>" + item.Fname + " " + item.Lname + "</td><td class='effort_td'>" + totaltime[0] + " h " + totaltime[1] + " m" + "</td></tr>");
                    });
                }
                else {
                    $('#TotalEmployee').append("No Record");
                }
            }
        });

    });


    $('#liTotalEmployeedatewise').click(function () {

        flagexport = $(this).attr('id');
        var txtdateval = $('#txtdate').val();

        if (txtdateval != "") {

            $('#TotalEmployeebydate').empty();
            $.ajax({
                type: "POST",
                url: "/DataTracker.svc/TotalEmployeeSheetByDate",
                data: "{\"boardid\":\"" + BoardID + "\",\"datewise\":\"" + txtdateval + "\"}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    $('#TotalEmployee').empty();
                    if (data.d.length > 0) {
                        var TotalEmployee = $('#TotalEmployee');

                        $(TotalEmployee).append("<table class='effort_table'><tr class='effort_tr'><th class='effort_th'>Name</th><th class='effort_th'>Total Time</th></tr>");

                        $.each(data.d, function (index, item) {

                            var totaltime = item.TotalTime.split(':');

                            $(TotalEmployee).append("<tr class='effort_tr'><td class='effort_td'>" + item.Fname + " " + item.Lname + "</td><td class='effort_td'>" + totaltime[0] + " h " + totaltime[1] + " m" + "</td></tr>");
                        });
                    }
                    else {
                        $('#TotalEmployee').append("No Record");
                    }
                }
            });
        }
        else {
            $('#TotalEmployee').empty();
            alert("Please select a date.");
        }

    });

    $('#ExportAllEmployeeData').click(function () {
        $.ajax({
            type: "POST",
            url: "/DataTracker.svc/ExportBoardDateWise",
            data: "{\"boardid\":\"" + BoardID + "\"}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                if (data.d.length > 0) {
                    var Path = "/Downloadfiles.aspx?" + data.d;
                    document.location.href = Path;
                }
            }

        });
    });



    $('#tblExportData').click(function () {

        if (flagexport == "liTotalEmployeedatewise") {
            ExportAllDataForBoardBydate();
        }
        else {
            ExportAllDataForBoard();
        }

    });

});

function GetMonthName(pdate) {
    var objDate = new Date(pdate),
    locale = "en-us",
    month = objDate.toLocaleString(locale, { month: "long" });

    return month;

}

function ExportAllDataForBoard() {
    $.ajax({
        type: "POST",
        url: "/DataTracker.svc/exportTotalEmployee",
        data: "{\"boardid\":\"" + BoardID + "\"}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            if (data.d.length > 0) {
                var Path = "/Downloadfiles.aspx?" + data.d;
                document.location.href = Path;
            }
        }

    });
}

function ExportAllDataForBoardBydate() {
    var txtdateval = $('#txtdate').val();
    $.ajax({
        type: "POST",
        url: "/DataTracker.svc/exportTotalEmployeeBydate",
        data: "{\"boardid\":\"" + BoardID + "\",\"datewise\":\"" + txtdateval + "\"}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            if (data.d.length > 0) {
                var Path = "/Downloadfiles.aspx?" + data.d;
                document.location.href = Path;
            }
        }

    });
}

//Board.aspx page

function MakeTemplates() {

    var Board_id = $('#boardID').val();
    var TemplatType = $('input[name=ArchiveBoardfortemplates]:checked').attr('id');
    var teamID = $('#teamID').val();
    var AccessModifier = $('#AccessModifier').text().substring(1);

    var templatename1 = $('#templatename').val().trim();
    var templatename = templatename1;
    var pattern = new RegExp('^[0-9a-zA-Z.-\\s]+$');
    var templatenamecheck = pattern.test(templatename);
    var IsSaveList = 0;
    var IsSaveListWithCard = 0;
    var IsPublic = 2;
    if (TemplatType == "1") {
        IsSaveList = 1;
        IsSaveListWithCard = 0;
    }
    else if (TemplatType == "2") {
        IsSaveList = 0;
        IsSaveListWithCard = 1;
    }
    if (AccessModifier == "Public") {
        IsPublic = 2;

    }
    else if (AccessModifier == "Private") {
        IsPublic = 1;
    }



    if (templatename != "" && templatename != null) {
        if (templatenamecheck) {
            $.ajax({
                type: "POST",
                url: "/DataTracker.svc/MakeTemplates",
                data: "{\"TamplateName\":\"" + templatename + "\",\"BoardId\":\"" + Board_id + "\",\"TeamId\":\"" + teamID + "\",\"IsPublic\":\"" + IsPublic + "\",\"IsSaveList\":\"" + IsSaveList + "\",\"IsSaveListWithCard\":\"" + IsSaveListWithCard + "\"}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                async: false,
                success: function (data) {
                    if (data.d == 0) {
                        alert("Template Name already exists.");
                    }
                    else {
                        $("#templatename").val('');
                        alert("Template created successfully.");
                        $(".webui-popover").hide();
                    }


                },
                error: function (error) {

                }
            });
        }
        else {
            alert('Letters, Numbers, (.) and (-) are Allowed');
        }
    }
    else {
        alert('Template Name Required');
    }



}



function openTab(evt, TabName) {

    var i, tabcontent, tablinks;
    tabcontent = document.getElementsByClassName("tabcontent");
    for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.display = "none";
    }
    tablinks = document.getElementsByClassName("tablinks");
    for (i = 0; i < tablinks.length; i++) {
        tablinks[i].className = tablinks[i].className.replace(" active", "");
    }
    document.getElementById(TabName).style.display = "block";
    evt.currentTarget.className += " active";

    if (TabName == "Activity") {
        $("#Datecontainer > div > .dropdown-container1").css("display", "none");
        $("#Datecontainer > div:first-child > .dropdown-container1").show();
        $("#Datecontainer > div a").removeClass('minus').addClass('plus');
        $("#Datecontainer > div:first-child a").removeClass('plus').addClass('minus');
        $("#Datecontainer").scrollTop(0);
    }
    else if (TabName == "Attachment") {
        showAttachmentList();
        $("#attachlist").scrollTop(0);


    }
    else if (TabName == "Message") {
        GetBoardMessage();
        $("#ulshowcomments").scrollTop(0);
    }
    else if (TabName == "Settings") {

        $("#divsettingsmenu").empty();
        settings();
        GetManagerClientOwner();
        $(".setting_cont").scrollTop(0);
    }
}

$(".movecard").live("mouseenter", function (e) {

    var today = new Date();
    var cardId = this.id;
    var dateMove = $("#" + cardId).find("#MoveDate").val();
    if (dateMove != "" && dateMove != undefined) {
        MoveHours = Math.round((today - new Date(dateMove)) / 3600000);
        if (MoveHours > 24) {
            MoveHours = "This has not been moved from last " + Math.round(MoveHours / 24) + " Days";
        }
        else {
            MoveHours = "This has not been moved from last " + MoveHours + " Hours";
        }
    } else { MoveHours = "This has not been moved from last 0 Hours."; }
    $("#" + cardId).attr("title", MoveHours);

    $('[name="reply_comment"]').keyup(function (e) {
        if (e.which === 13) {
            var comment = $(e.currentTarget).val(),
    commentDiv = $('.main_comment_DIV_id_no');
            //you have the comment here, so you can call an ajax request to update it in the database.
            $(e.currentTarget).val("");
            //if the blank div where you want to add the comment is already present, then write
            $(commentDiv[commentDiv.length - 1]).text(comment);
            // Else no need to add the blank div initially, just create a new div and add the comment there.
            $(commentDiv[commentDiv.length - 1]).after('<div class="main_comment_DIV_id_no">' + comment + '</div>');
        }
    })
    $("#ancrclose").click(function () {
        $("#txtmembers").val("");
    });
});


$('#txtCardEstimatedminute').live('keyup keydown', function (e) {

    var txtvalue1 = $('#txtCardEstimatedTime').val();

    if (txtvalue1 == 100) {
        $('#txtCardEstimatedminute').val("");
        swal("That’s a lot of hours for each card! We recommend that you further divide the cards and keep the average time needed to complete each card lesser, that makes projects much more measurable and easy to track.");
        return false;
    }

    if ($(this).val() > 59
        && e.keyCode != 46
        && e.keyCode != 8
       ) {
        e.preventDefault();
        swal("That’s a lot of hours for each card! We recommend that you further divide the cards and keep the average time needed to complete each card lesser, that makes projects much more measurable and easy to track!");
        $('#txtCardEstimatedminute').val("");

    }
});


$("#txtCardEstimatedminute").live('keypress', function (evt) {

    var txtlength = $("#txtCardEstimatedminute").val().length;

    if (txtlength > 2) {
        swal("That’s a lot of hours for each card! We recommend that you further divide the cards and keep the average time needed to complete each card lesser, that makes projects much more measurable and easy to track!");
        return false;
    }
    var charCode = (evt.which) ? evt.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {

        return false;
    }
    else {
        return true;
    }
});

//Ameeq changes in Swimlanes for passing SprintId with BoardId
var TotalsprintIdCheck = "";
$(document).ready(function () {

    $("#txtInviteMembers").keyup(function () {
        $("#appendInviteMembers").empty();
        var userlist = $("#txtInviteMembers").val();
        $(".org-members-page-layout-list").empty();
        if (userlist.length > 0) {
            $.ajax({
                type: "GET",
                url: "/DataTracker.svc/GetmembernameonTeam?membersname=" + userlist + "",
                dataType: "json",
                async: false,
                success: function (data) {
                    if (data.d.length > 0) {
                        $.each(data.d, function (index, item) {
                            var str = item.Email;
                            var firstletter = str.charAt(0);
                            var username = item.username;
                            $("#appendInviteMembers").append("<span onclick='InviteMemberOnBoard(" + item.Email + ");'> <a href='#'><h2>" + firstletter + "</h2><div class='member_id'><p title='" + item.Email + "'>" + item.Email + "</p></div><div class='clr'></div></a></span>");
                            $("#appendInviteMembers").show();
                        });
                    }
                },
                error: function (error) { alert('Error has occurred!'); alert(JSON.stringify(error)) }
            });
        }
    });

    $("#HideCardsFromOtherUsers").click(function () {
        $.ajax({
            type: "POST",
            url: "/DataTracker.svc/HidePublicBoardCardsFromOtherUsers",
            data: "{\"boardID\":\"" + $('#boardID').val() + "\"}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: false,
            success: function (data) {
                if (data.d == "1") {
                    $('#HideCardsFromOtherUsers').css('display', 'none');
                    $('#ShowCardsToOtherUsers').css('display', 'block');
                }
            },
            error: function (error) {

            }
        });
    });
    $("#ShowCardsToOtherUsers").click(function () {
        $.ajax({
            type: "POST",
            url: "/DataTracker.svc/ShowPublicBoardCardsToOtherUsers",
            data: "{\"boardID\":\"" + $('#boardID').val() + "\"}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: false,
            success: function (data) {
                if (data.d == "1") {
                    $('#ShowCardsToOtherUsers').css('display', 'none');
                    $('#HideCardsFromOtherUsers').css('display', 'block');
                }
            },
            error: function (error) {

            }
        });
    });

    //$('#txtCardSearch').keyup(function () {
    //    SearchByCards();
    //});

    $("#ArchivedSwimlanesList").click(function () {
        window.location.href = "DisplayArchivedAllSwimlanes.aspx?bid=" + $('#boardID').val();
    });

    $(".cd-primary-nav > li > a").mouseenter(function () {
        $('#cd-nav-gallerysec').addClass('is-hidden');
    });
    //$("#imgLightHouse").mouseenter(function () {
    //    $('#cd-nav-gallerysec').addClass('is-hidden');
    //});
    $('#cd-nav-gallerysec').mouseleave(function () {
        //$('#cd-nav-gallerysec').addClass('is-hidden');
        $('a#emptxt').css('background', '#0000');
        $('a#emptxt').css('color', '#fffff');
    });
    $('#cd-nav-gallerysec').mouseenter(function () {
        $('a#emptxt').css('background', '#ffffff');
        $('a#emptxt').css('color', '#0000');
    });

    $('.fa-bars').click(function () {
        if ($(this).hasClass("sh")) {
            $(this).removeClass('sh')
            $('.toggaldiv').css({ 'visibility': 'hidden' });
        } else {
            $('.fa-bars').addClass('sh')
            $('.toggaldiv').css({ 'visibility': 'visible' });
        }
    });
    $('.fa-bars').mouseenter(function () {
        $('.toggaldiv').css({ 'visibility': 'visible' });
    });
    $('.boardname1').mouseenter(function () {
        $('.toggaldiv').css({ 'visibility': 'hidden' });
    });
    //$('.toggaldiv').mouseleave(function () {
    //    $('.toggaldiv').css({ 'visibility': 'hidden' });
    //});
    //$('.board_sec').mouseenter(function () {
    //    $('#cd-nav-gallerysec').addClass('is-hidden');
    //    $('.toggaldiv').css({ 'visibility': 'hidden' });
    //});
    var defaultSpId = "1";
    var a = location.href;
    var k = a.indexOf('/board.aspx');

    if (a.indexOf('/board.aspx') > -1) {
        $(".infodiv").hover(function () {
            $(".h111").show();

        },
                 function () {
                     $(".h111").hide();
                 });

        $("#popup4").mouseenter(function () {
            $(".h111").css("display", "block");
            $("#webuiPopover1").css("display", "block");
            $(".h111").show();
        })
        $("#popup4").mouseleave(function () {
            $(".h111").css("display", "none");
            $("#webuiPopover1").css("display", "none");
        })


        $('#webuiPopover5').click(function (evt) {
            if (!$(evt.target).is('#webuiPopover5')) {
                $('.tabcontent').hide();
            }
        });
        $('#webuiPopover4').click(function (evt) {
            if (!$(evt.target).is('#webuiPopover4')) {
                $('.tabcontent').hide();
            }
        });

        $('.cd-main-content').click(function () {
            $('.tabcontent').hide();
        });
        $('.tabcontent').click(function (event) {
            event.stopPropagation();
        });


        $('#activitymenuslideout').click(function (event) {

            event.stopPropagation();
            $(".tab_content_button>ul>li>a").removeClass('active');
        });

        $('.CleanAcceModifier').click(function () {
            $('#BoardAccessModifier').html("Public");
        });

        var boardidCheck = a.substring(a.indexOf("?") + 1).replace("#", "");
        BoardID = boardidCheck;
        GetBoardDetails(boardidCheck);
        //$.ajax({
        //    type: "POST",
        //    url: "/DataTracker.svc/CheckSprints",
        //    data: "{\"CheckSprintId\":\"" + boardidCheck + "\"}",
        //    contentType: "application/json; charset=utf-8",
        //    dataType: "json",
        //    async: false,
        //    success: function (data) {
        //        if (data.d != 0) {

        //            for (var i = 0; i < (data.d).length; i++) {
        //                if (a.indexOf("?") != -1) {
        //                    //GetCardAndList(a.substring(a.indexOf("?") + 1).replace("#", ""), data.d[i].Sprintorder, data.d[i].SprintId);
        //                } else {
        //                   HomeBoard();
        //                }
        //            }
        //        }
        //    },
        //    error: function (error) {

        //    }
        //});

        $("#ArchiveBoard1").click(function () {
            $("#templatename").val('');
            $('input[name = "ArchiveBoardfortemplates"]').filter('[value = "template"]').attr('checked', true);

        });

        $('.spnSelectcolor').live("click", function () {

            var backcolor = $(this).css('background-color');

            if ($('.spndynamiccolor').css('background-color') == backcolor) {
                $('.spndynamiccolor').css('background-color', '');
                var lblid_delete = 0;
                Deletetaskprioritycolor(lblid_delete);
            }
            else {
                $('.spndynamiccolor').css('background-color', backcolor);
                $('.spndynamiccolor').css('display', 'block');
                var id = $(this).attr('id');
                Savecolor(id);
            }
        });


        $("#txtEstimated").keypress(function (evt) {

            var txtlength = $("#txtEstimated").val().length;
            if (txtlength > 4) {
                swal("Hmm … a board consuming more than 10,000 hours? Its a good idea to keep boards smaller and more manageable!");
                return false;
            }
            var charCode = (evt.which) ? evt.which : event.keyCode;
            if (charCode > 31 && (charCode < 48 || charCode > 57)) {

                return false;
            }
            else {
                return true;
            }
        });

        $('#txtEstimated').on('keyup keydown', function (e) {

            if ($(this).val() > 9999
                && e.keyCode != 46
                && e.keyCode != 8
               ) {
                e.preventDefault();
                $('#txtEstimated').val("");
                swal("Hmm … a board consuming more than 10,000 hours? Its a good idea to keep boards smaller and more manageable!");

            }
        });


        $("#txtCardEstimatedTime").keypress(function (evt) {

            var txtlength = $("#txtCardEstimatedTime").val().length;

            if (txtlength > 3) {
                swal("That’s a lot of hours for each card! We recommend that you further divide the cards and keep the average time needed to complete each card lesser, that makes projects much more measurable and easy to track!");
                return false;
            }
            var charCode = (evt.which) ? evt.which : event.keyCode;
            if (charCode > 31 && (charCode < 48 || charCode > 57)) {

                return false;
            }
            else {
                return true;
            }
        });

        $('#txtCardEstimatedTime').on('keyup keydown', function (e) {

            var val = $("#txtCardEstimatedTime").val();


            if ($(this).val() > 100
                && e.keyCode != 46
                && e.keyCode != 8
               ) {
                e.preventDefault();

                swal("That’s a lot of hours for each card! We recommend that you further divide the cards and keep the average time needed to complete each card lesser, that makes projects much more measurable and easy to track!");
                $('#txtCardEstimatedTime').val("");

            }
        });


        $(function () {
            $('#txteditdescription').on('keyup paste focus', function () {

                var $el = $(this),
                    offset = $el.innerHeight() - $el.height();

                if ($el.innerHeight < this.scrollHeight) {
                    //Grow the field if scroll height is smaller
                    $el.height(this.scrollHeight - offset);
                } else {
                    //Shrink the field and then re-set it to the scroll height in case it needs to shrink
                    $el.height(1);
                    $el.height(this.scrollHeight - offset);
                }
            });
        });

        $('#btninvsend').on('click', function () {

            if ($('#txtinvemail').val() != "") {
                if (validateInvEmail($('#txtinvemail').val())) {

                    SendInvitation($('#txtinvemail').val(), $('#boardID').val(), 4);
                    $('#errinvemail').text("");
                }
                else {
                    $('#errinvemail').text("Oops! It's an invalid email.");

                }
            }
            else {
                $('#errinvemail').text("Opps ! Please enter email.");
                return false;
            }
        });

        $("#txtinvemail").keyup(function () {

            $('#errinvemail').text("");
        });

        function UpdateAttachmentCount(count) {
            $('#attcount').html("(<a id='attacmentlist' href='javascript:showAttachmentList();'>" + count + "</a>)");

        }

        function validateInvEmail(email) {

            var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
            var isEmail = regex.test(email);
            if (isEmail) {
                return true;
            }
            else {
                $('#errinvemail').text("Opps ! it's a invalid email");
                return false;
            }
        }


        $('#btnalmessage').on('click', function () {

            text_max = 500;
            $('#textarea_feedback').html("");
            $('#textarea_feedback').html('(' + text_max + ' characters remaining)');


            $('#btnalmessage').css('display', 'none');
            $("#lblsend").show();

            var megval = $('#almessage').val().trim();
            if (megval != "") {

                if ($('#almessage').val().length > 500) {
                    $('#almessage').val('');


                    $('#btnalmessage').show();
                    $("#lblsend").css('display', 'none');

                    alert("Maximum word limit reached (500 Characters).");
                }
                else {

                    closeMessageBox('pop');
                    UpdateActivityListItem($('#almessage').val(), 3);
                }

            }
            else {
                closeMessageBox('pop');
                alert("Message box can not be empty.");
                $('#almessage').val("");
                $('#btnalmessage').show();
                $("#lblsend").css('display', 'none');
            }
        });


        $("#BoardName").click(function () {

            var boarnamewidth = $(this).width();
            $("#txtareaboardname").css('min-width', boarnamewidth);
        });

        $("#ddlteamtoboard").click(function () {

            $(".webui-popover").removeClass('out');
        });

        var text_max = 500;
        $('#textarea_feedback').html('(' + text_max + ' characters remaining)');

        $('#almessage').keyup(function () {

            var text_length = $('#almessage').val().length;
            var text_remaining = text_max - text_length;

            $('#textarea_feedback').html('(' + text_remaining + ' characters remaining)');
        });


        var max = 500;
        $('#almessage').keypress(function (e) {

            if (e.which < 0x20) {
                return;
            }
            if (this.value.length == max) {
                e.preventDefault();
            } else if (this.value.length > max) {
                // Maximum exceeded
                alert("Maximum word limit reached (500 Characters)");
                this.value = this.value.substring(0, max);

            }
        });

        $(document).mouseup(function (e) {
            var container = $("#webuiPopover4");
            if (!container.is(e.target) // if the target of the click isn't the container...
                && container.has(e.target).length === 0) // ... nor a descendant of the container
            {
                container.hide();
            }
        });
        $(document).mouseup(function (e) {
            var container = $("#webuiPopover10");
            if (!container.is(e.target) // if the target of the click isn't the container...
                && container.has(e.target).length === 0) // ... nor a descendant of the container
            {
                container.hide();
            }
        });
        $(document).mouseup(function (e) {
            var container = $("#webuiPopover5");
            if (!container.is(e.target) // if the target of the click isn't the container...
                && container.has(e.target).length === 0) // ... nor a descendant of the container
            {
                container.hide();
            }
        });

        $(document).mouseup(function (e) {
            var container = $("#webuiPopover0");
            if (!container.is(e.target) // if the target of the click isn't the container...
                && container.has(e.target).length === 0) // ... nor a descendant of the container
            {
                container.hide();
            }
        });

        $("#TeamP1").click(function () {

            $("#change2").show();
            $(".webui-popover").hide();
        });

        $(".leftarrow").click(function () {

            $("#change1").hide();
            $(".webui-popover").show();
        });

        $("#larrow").click(function () {
            $("#change2").hide();
            $(".webui-popover").show();
        });



        $(".pop-close").click(function () {
            $("#change1").hide();
            $("#webuiPopover2").hide();
        });



        $("#pclose").click(function () {
            $("#change2").hide();
            $("#webuiPopover3").hide();
        });

        $(document).mouseup(function (e) {
            var container = $("#webuiPopover2");
            if (!container.is(e.target) // if the target of the click isn't the container...
                && container.has(e.target).length === 0) // ... nor a descendant of the container
            {
                container.hide();
            }
        });
        $(document).mouseup(function (e) {
            var container = $("#webuiPopover9");
            if (!container.is(e.target) // if the target of the click isn't the container...
                && container.has(e.target).length === 0) // ... nor a descendant of the container
            {
                container.hide();
            }
        });
        $(document).mouseup(function (e) {
            var container = $("#webuiPopover13");
            if (!container.is(e.target) // if the target of the click isn't the container...
                && container.has(e.target).length === 0) // ... nor a descendant of the container
            {
                container.hide();
            }
        });




        $(document).click(function (e) {

            if (!$(e.target).hasClass("pickmeup")
                && $(e.target).parents("#webuiPopover14").length === 0) {
                $("#webuiPopover14").hide();
            }
        });

        $(document).mouseup(function (e) {
            var container = $("#webuiPopover6");
            if (!container.is(e.target) // if the target of the click isn't the container...
                && container.has(e.target).length === 0) // ... nor a descendant of the container
            {
                container.hide();
            }
        });
        $('.tesxer').live('click', function () {

            var selected = [];
            $(".tile__list  :checked").each(function () {

                selected.push(this.id);

            });

            if (selected.length > 0) {
                $("#divEditMultipleSelection").css("display", "block");

                var cardidtogetspncolor = selected[0];
                var getcolortosetspndynamic = $("#cardcolorset" + cardidtogetspncolor).css('background-color');
                $('.spndynamiccolor').css('background-color', getcolortosetspndynamic);
                $("#hide_cardid").val(cardidtogetspncolor);


            }
            else { $("#divEditMultipleSelection").css("display", "none"); }

        });

        $(document).mouseup(function (e) {
            var container = $("#webuiPopover3");
            if (!container.is(e.target) // if the target of the click isn't the container...
                && container.has(e.target).length === 0) // ... nor a descendant of the container
            {
                container.hide();
            }
        });
        $(document).mouseup(function (e) {
            var container = $("#webuiPopover1");
            if (!container.is(e.target) // if the target of the click isn't the container...
                && container.has(e.target).length === 0) // ... nor a descendant of the container
            {
                container.hide();
            }
        });
        $(document).mouseup(function (e) {
            var container = $("#change1");
            if (!container.is(e.target) // if the target of the click isn't the container...
                && container.has(e.target).length === 0) // ... nor a descendant of the container
            {
                container.hide();
            }
        });

        $(document).mouseup(function (e) {
            var container = $(".webui-popoverab");

            if (!container.is(e.target) // if the target of the click isn't the container...
                 && container.has(e.target).length === 0) // ... nor a descendant of the container
            {
                container.hide();
            }
        });
        $(document).mouseup(function (e) {
            var container = $("#webuiPopover14");

            if (!container.is(e.target) // if the target of the click isn't the container...
                 && container.has(e.target).length === 0) // ... nor a descendant of the container
            {
                container.hide();
            }
        });

        $(document).mouseup(function (e) {
            var container = $("#webuiPopover17");
            if (!container.is(e.target) // if the target of the click isn't the container...
                && container.has(e.target).length === 0) // ... nor a descendant of the container
            {
                container.hide();
            }
        });

        $(document).mouseup(function (e) {
            var container = $("#webuiPopover18");
            if (!container.is(e.target) // if the target of the click isn't the container...
                && container.has(e.target).length === 0) // ... nor a descendant of the container
            {
                container.hide();
            }
        });

        $(document).mouseup(function (e) {
            var container = $("#webuiPopover19");

            if (!container.is(e.target) // if the target of the click isn't the container...
                 && container.has(e.target).length === 0) // ... nor a descendant of the container
            {
                container.hide();
            }
        });
        $(document).mouseup(function (e) {
            var container = $("#webuiPopover16");

            if (!container.is(e.target) // if the target of the click isn't the container...
                 && container.has(e.target).length === 0) // ... nor a descendant of the container
            {
                container.hide();
            }
        });

        $(document).mouseup(function (e) {
            var container = $("#webuiPopover15,.pickmeup");
            var dtpicker = $(".pickmeup");
            if (!container.is(e.target) // if the target of the click isn't the container...
                && container.has(e.target).length === 0) // ... nor a descendant of the container
            {
                container.hide();


            }
        });

        $("#divsettingsmenu").empty();

        $('.messagepop_content').hide().before(' <a href="javascript:ShowMessageBox()" id="contact"></a>');
        $('#contact').click(function () {
            $('.messagepop_content').slideToggle(1000);
            return false;
        });
        $('#cardpop').on('click', function () {
            $('#webuiPopover12').hide();
        });


        function ShowMessageBox() {
            if ($(this).hasClass('selected')) {
                closeMessageBox('pop');
            } else {
                $(this).addClass('selected');
                $('.pop').slideFadeToggle();
            }
        }

        function closeMessageBox(elm) {
            $('.' + elm).slideFadeToggle(function () {
            });
        }

        $('#btnmsgclose').on('click', function () {
            $('.messagepop_content').slideToggle(1000);
            return false;
        });


        $(function () {

            var rightVal = -380;

            $("#activitymenu").click(function () {
                rightVal = (rightVal * -1) - 380;
                $("#activitymenuslideout").animate({ right: rightVal + 'px' }, { queue: false, duration: 500 });
                if (rightVal == 0) {
                    $("#activitymenu").text('Hide Menu');
                }
                else {
                    $("#activitymenu").text('Show Menu');
                }
            });



        });

        $("#txtCardDuration").keypress(function (evt) {
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            if (charCode < 48 || charCode > 57) {

                return false;
            }
            else {
                return true;
            }
        });


        $(".js-change-org").click(function () {
            $("#change").show();
        });


        $("#changeteam").click(function () {
            $("#change3").show();
            $(".webui-popover").hide();
        });

        $("#larrow1").click(function () {
            $("#change3").hide();
            $(".webui-popover").show();
        });

        $("#pclose1").click(function () {
            $("#change3").hide();
            $("#webuiPopover4").hide();

        });





        $(".leftarrow").click(function () {

            $("#change").hide();
        });
        $(".pop-close").click(function () {
            $("#change").hide();
            $("#webuiPopover1").hide();
        });
        $(".js-view-org").click(function () {
            var teamid = $("#teamID").val();
            team_landing(teamid);

        });



        $("#TeamP").click(function () {
            $("#change1").show();
            $(".webui-popover").hide();
        });


        $("#teamlabel").click(function () {
            $("#team-name1").show();
        });

        $('ul.tabs li').click(function () {
            var tab_id = $(this).attr('data-tab');

            $('ul.tabs li').removeClass('current');
            $('.tab-content').removeClass('current');

            $(this).addClass('current');
            $("#" + tab_id).addClass('current');

        });
    }

    $('#txtdate').keydown(function (e) {
        if (e.shiftKey || e.altKey) {
            e.preventDefault();
        }
        else {

            var key = e.keyCode || e.charCode;
            if (!((key == 8) || (key == 46) || (key == 17) || (key == 189) || (key == 173) || (key == 109) || e.ctrlKey && (e.keyCode == 88 || e.keyCode == 67 || e.keyCode == 86 || e.keyCode == 65) || (key >= 35 && key <= 40) || (key >= 48 && key <= 57) || (key >= 96 && key <= 105))) {
                e.preventDefault();
            }

        }
    });

    $(function () {
        var Task_Id = "";
        var FName = "";
        $('body').on(".card_bottom_sec").filedrop({
            fallback_id: 'btnUpload',
            fallback_dropzoneClick: true,
            url: 'Handler.ashx',
            allowedfiletypes: [],
            allowedfileextensions: [],
            paramname: 'fileData',
            maxfiles: 20, //Maximum Number of Files allowed at a time.
            maxfilesize: 30, //Maximum File Size in MB.
            dragOver: function () {
            },
            dragLeave: function () {
            },
            drop: function () {
                $('body').on('drop', '.card_bottom_sec', function (e) {
                    
                    Task_Id = $(this).attr('id').replace(/^\D+/g, '');
                })
            },
            uploadFinished: function (i, file, response, time) {
                $('#uploadedFiles').append(file.name + '<br />')
                FName = file.name;
            },
            afterAll: function (e) {
                //To do some task after all uploads done.
                UploadDraggableImage(FName, Task_Id);
                GetTotalAttachmentCount(Task_Id);
            }
        });
        
    })
});



function GetTotalAttachmentCount(Task_Id) {
    $.ajax({
        type: 'POST',
        url: '/Datatracker.svc/GetTotalCountAttachment',
        data: "{\"taskid\":\"" + Task_Id + "\"}",
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        success: function (data) {
            var response = JSON.parse(data.d);
            $.each(response, function (index, item) {
                AttachmentCount = item.AttachmentCount;
                
                ShowAddAttachment(AttachmentCount, Task_Id);
            });
        },
        error: function (error) {

        }
    });
}
function dragStart(event) {
    
    event.dataTransfer.setData("Text", event.target.id);
    document.getElementById("demo").innerHTML = "Started to drag the p element";
}

function allowDrop(event) {
    
    event.preventDefault();
}

function drop(event) {
    
    event.preventDefault();
    var data = event.dataTransfer.getData("Text");
    event.target.appendChild(document.getElementById(data));
    document.getElementById("demo").innerHTML = "The p element was dropped";
}

function ShowSprint(SprintId) {
    //Hide other
    //$('.panel-heading').css('display', 'block');
    //$('.estimatedprogress').css('display', 'none');
    //$('.accordion-sprint-header').css('display', 'none');
    //$('.tile').css('display', 'none');
    //$('.add_list').css('display', 'none');



    //Show 
    $('.multi_' + SprintId).css('display', 'block');
    $('.SprintProgress_' + SprintId).css('display', 'block');
    $('.Sprint_' + SprintId).css('display', 'block');
    $('#HideSprint_' + SprintId).css('display', 'block');
    $('#ShowSprint_' + SprintId).css('display', 'none');
    //Dynamic Height
    var spheight = $('.Spheight_' + SprintId).val();
    $('#HideSprint_' + SprintId).css('height', spheight);

    if (spheight == undefined) {
        $('#HideSprint_' + SprintId).css('margin-top', '5px');
    }
    //Dynamic width
    var spwidth = $('.Spwidth').val();
    spwidth = spwidth + 'px';
    $('#multi').css('width', spwidth);
    CalculateBoardCompleteWithSprint($('#boardID').val(), SprintId);
    //Sprintprogress(item.NoOfCardsInList, item.NoOfCardsInSprint, temp);
    if (flagWhenBoardCreate == 0) {
        SetProgressBarPer(SprintId);
    }
}
function HideSprint(SprintId) {
    //Hide
    $('.multi_' + SprintId).css('display', 'none');
    $('.SprintProgress_' + SprintId).css('display', 'none');
    $('.Sprint_' + SprintId).css('display', 'none');
    $('#HideSprint_' + SprintId).css('display', 'none');
    $('#ShowSprint_' + SprintId).css('display', 'block');
}

function AttachmentFileUpload() {

    var UID = $('#cuserid').val();
    var val = $("#fileToUploadFromAL").val();
    var ext = val.split('.').pop().toLowerCase();
    if (ext == "jpg" || ext == "jpeg" || ext == "gif" || ext == "png" || ext == "bmp" || ext == "doc" || ext == "docx" || ext == "pdf" || ext == "txt" || ext == "xml" || ext == "sql" || ext == "ods" || ext == "odt" || ext == "xls" || ext == "xlsx" || ext == "ppt" || ext == "pptx" || ext == "rtf" || ext == "csv") {

        ajaxFileUploadforActivityLog(null, UID);
        $("#fileToUploadFromAL").val('');

    }
    else {
        alert("This file format is not supported.");
        $("#fileToUploadFromAL").val('');

    }
    return false;
}

function Upload() {
    var UID = $('#cuserid').val();
    var val = $("#fileToUpload").val();
    var ext = val.split('.').pop().toLowerCase();
        var taskid = $("#hide_cardid").val();
        SavedMessagePopUp(5);
        boardUploadAttach(UID, taskid);
        $("#fileToUpload").val('');
}

function UploadDraggableImage(imgpath, taskid) {
    if(taskid!=""){
    var UID = $('#cuserid').val();
    var val = imgpath;
    var ext = val.split('.').pop().toLowerCase();
    var imgname = imgpath.split('.')[0];
        SavedMessagePopUp(5);
        MycardajaxFileUpload(taskid, UID, val, imgname);
  }
  else {
        alert('Please Drag and Drop file Again!');
    }
}

function MycardajaxFileUpload(C_taskid, UID, imgPath, imgName) {
    if (C_taskid != "") {
        $.ajax({
            type: 'POST',
            url: '/Datatracker.svc/SaveAttachmentonCard',
            data: JSON.stringify({ taskid: C_taskid, userid: UID, filename: imgName, filepath: imgPath }),
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            success: function (data) {
                
                bindAttachmentonPop(C_taskid, UID);
            },
            error: function (error) {

            }

        });
    }
    else
    {
        alert('Please Drag and Drop file Again!');
    }
}

function ShowInviteBox() {
    $('#Inviteonboard').css('display', 'block');
    $('#Updateestimatedtime').css('display', 'none');
    $('#myModal').modal();

}

var callback = function () {

    var h = $(window).height();
    var k = 80;
    $('.board_sec').height(h - k);
};
$(document).ready(callback);
$(window).resize(callback);


function setVisibility(id, visibility) {

    document.getElementById(id).style.display = visibility;
}


$.fn.slideFadeToggle = function (easing, callback) {

    return this.animate({ opacity: 'toggle', height: 'toggle' }, 'fast', easing, callback);
};



function SendNotificationMailForAttachmentOnBoard(fileName) {

    $.ajax({
        type: 'POST',
        url: '/Datatracker.svc/SendNotificationMailForAttachmentOnBoard',
        data: "{\"boardname\":\"" + globalboardname + "\",\"userid\":\"" + globlaUserid + "\",\"filename\":\"" + fileName + "\",\"boardid\":\"" + BoardID + "\"}",
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        success: function (data) {

        },
        error: function (error) {

        }

    });
}


function SendNotificationMailForMessageOnBoard(msg) {

    $.ajax({
        type: 'POST',
        url: '/Datatracker.svc/SendNotificationMailForMessageOnBoard',
        data: "{\"boardname\":\"" + globalboardname + "\",\"userid\":\"" + globlaUserid + "\",\"message\":\"" + msg + "\",\"boardid\":\"" + BoardID + "\"}",
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        success: function (data) {

        },
        error: function (error) {

        }

    });
}

function SendNotificationMailForAttachmentOnCard(fileName, cardname, TID) {

    $.ajax({
        type: 'POST',
        url: '/Datatracker.svc/SendNotificationMailForAttachmentOnCard',
        data: "{\"boardname\":\"" + globalboardname + "\",\"userid\":\"" + globlaUserid + "\",\"filename\":\"" + fileName + "\",\"boardid\":\"" + BoardID + "\",\"cardname\":\"" + cardname + "\",\"taskid\":\"" + TID + "\"}",
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        success: function (data) {

        },
        error: function (error) {

        }

    });
}


function SendNotificationMailForMessageOnCard(cardname, cardid, msg) {

    $.ajax({
        type: 'POST',
        url: '/Datatracker.svc/SendNotificationMailForMessageOnCard',
        data: "{\"boardname\":\"" + globalboardname + "\",\"userid\":\"" + globlaUserid + "\",\"cardname\":\"" + cardname + "\",\"cardid\":\"" + cardid + "\",\"message\":\"" + msg + "\"}",
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        success: function (data) {

        },
        error: function (error) {

        }

    });
}

function SendNotificationMailForMessageOnCardListOwner(ListId, cardname, cardid, msg) {
    $.ajax({
        type: 'POST',
        url: '/Datatracker.svc/SendNotificationMailForMessageOnCardListOwner',
        data: "{\"ListId\":\"" + ListId + "\",\"boardname\":\"" + globalboardname + "\",\"userid\":\"" + globlaUserid + "\",\"cardname\":\"" + cardname + "\",\"cardid\":\"" + cardid + "\",\"message\":\"" + msg + "\"}",
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        success: function (data) {
        }
    });
}

function SendNotificationMailForAddMemberOnCard(cardname, cardid, addedmember) {

    $.ajax({
        type: 'POST',
        url: '/Datatracker.svc/SendNotificationMailForAddMemberOnCard',
        data: "{\"boardname\":\"" + globalboardname + "\",\"userid\":\"" + globlaUserid + "\",\"cardname\":\"" + cardname + "\",\"cardid\":\"" + cardid + "\",\"member\":\"" + addedmember + "\"}",
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        success: function (data) {

        },
        error: function (error) {

        }

    });
}

function SendNotificationMailForRemoveMemberOnCard(cardname, cardid, email) {

    $.ajax({
        type: 'POST',
        url: '/Datatracker.svc/SendNotificationMailForRemoveMemberOnCard',
        data: "{\"boardname\":\"" + globalboardname + "\",\"userid\":\"" + globlaUserid + "\",\"cardname\":\"" + cardname + "\",\"cardid\":\"" + cardid + "\",\"email\":\"" + email + "\"}",
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        success: function (data) {

        },
        error: function (error) {

        }

    });
}

function SendNotificationMailForArchiveCard(cardname, cardid) {

    $.ajax({
        type: 'POST',
        url: '/Datatracker.svc/SendNotificationMailForArchiveCard',
        data: "{\"boardname\":\"" + globalboardname + "\",\"userid\":\"" + globlaUserid + "\",\"cardname\":\"" + cardname + "\",\"cardid\":\"" + cardid + "\",\"boardid\":\"" + BoardID + "\"}",
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        success: function (data) {

        },
        error: function (error) {

        }

    });
}

function SendNotificationMailForArchiveBoard(reason) {

    $.ajax({
        type: 'POST',
        url: '/Datatracker.svc/SendNotificationMailForArchiveBoard',
        data: "{\"boardname\":\"" + globalboardname + "\",\"userid\":\"" + globlaUserid + "\",\"boardid\":\"" + BoardID + "\",\"reason\":\"" + reason + "\"}",
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        async: false,
        success: function (data) {

        },
        error: function (error) {

        }

    });
}


function SendNotificationMailForCompleteBoard() {

    $.ajax({
        type: 'POST',
        url: '/Datatracker.svc/SendNotificationMailForCompleteBoard',
        data: "{\"boardname\":\"" + globalboardname + "\",\"userid\":\"" + globlaUserid + "\",\"boardid\":\"" + BoardID + "\"}",
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        async: false,
        success: function (data) {

        },
        error: function (error) {

        }

    });
}


function SendEmailToChangeTeam(teamid, teamname) {

    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "/DataTracker.svc/SendEmailToChangeTeam",
        data: "{\"teamid\": \"" + teamid + "\",\"boardname\": \"" + globalboardname + "\",\"teamname\": \"" + teamname + "\"}",
        dataType: "json",
        success: function (data) {
        },
        error: function (error) {
        }
    });
}

function BindClient(clientName) {
    var flag = 0;
    document.getElementById("Clients").innerHTML = "";
    $.ajax({
        type: "GET",
        url: "/DataTracker.svc/BindClients",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            if (clientName != 'none') {
                $.each(data.d, function (index, item) {
                    if (item.clientName == clientName) {
                        flag = 1;
                    }
                });
                if (flag == 0) {
                    $('#Clients').append("<option value='0'>" + clientName + "</option>");
                }
                $.each(data.d, function (index, item) {
                    $('#Clients').append("<option value=" + item.clientid + ">" + item.clientName + "</option>"); // Added by Ravi to bind dropdown on create Board PopUp.
                    if (item.clientName == clientName) {
                        $("#Clients option:contains(" + clientName + ")").attr('selected', 'selected');
                    }
                });
            }
            else {
                $('#Clients').append("<option value='0'>" + clientName + "</option>");
            }
        }
    });

}

function BindAccountManager(managername) {
    var flag = 0;
    document.getElementById("AccountManager").innerHTML = "";
    $.ajax({
        type: "GET",
        url: "/DataTracker.svc/BindManager",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            if (managername != 'none') {
                $.each(data.d, function (index, item) {
                    if (item.managerName == managername) {
                        flag = 1;
                    }
                });
                if (flag == 0) {
                    $('#AccountManager').append("<option value='0'>" + managername + "</option>");
                }
                $.each(data.d, function (index, item) {
                    $('#AccountManager').append("<option value=" + item.managerid + ">" + item.managerName + "</option>"); // Added by Ravi to bind dropdown on create Board PopUp.
                    if (item.managerName == managername) {
                        $("#AccountManager option:contains(" + managername + ")").attr('selected', 'selected');
                    }
                });
            }
            else {
                $('#AccountManager').append("<option value='0'>" + managername + "</option>");
            }
        }
    });
}


function OnSelectClient() {
    var userid = $("#Clients").val();
    $.ajax({
        type: "POST",
        url: "/DataTracker.svc/UpdateBoardClient",
        data: "{\"boardid\":\"" + BoardID + "\",\"userid\":\"" + userid + "\"}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
        }
    });
}

function OnSelectManager() {
    var userid = $("#AccountManager").val();
    $.ajax({
        type: "POST",
        url: "/DataTracker.svc/UpdateBoardManager",
        data: "{\"boardid\":\"" + BoardID + "\",\"userid\":\"" + userid + "\"}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
        }
    });
}


function GetManagerName() {
    $.ajax({
        type: "POST",
        url: "/DataTracker.svc/GetManagerName",
        data: "{\"boardid\":\"" + BoardID + "\"}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            if (data.d != "" && data.d != undefined) {

                BindAccountManager(data.d);
            }
            else {
                BindAccountManager("None");
            }
        }
    });
}


function GetClientName() {
    $.ajax({
        type: "POST",
        url: "/DataTracker.svc/GetClientName",
        data: "{\"boardid\":\"" + BoardID + "\"}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            if (data.d != "" && data.d != undefined) {
                BindClient(data.d);
            }
            else {
                BindClient("None");
            }
        }
    });
}

function GetManagerClientOwner() {
    $.ajax({
        type: "POST",
        url: "/DataTracker.svc/GetManagerClientOwners",
        data: "{\"BoardID\":\"" + BoardID + "\",\"TeamID\":\"" + teamIdForOwner + "\"}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            
            var xmlDoc = $.parseXML(data.d);
            var xml = $(xmlDoc);
            var tblaccountmanager = xml.find("accountmanager");
            var tblclient = xml.find("client");
            var tblassignedmembertoteam = xml.find("assignedmembertoteam");

            var accountmanagerName = xml.find("accountmanagerName").find('name').text();
            if (accountmanagerName == "" || accountmanagerName == undefined) {
                accountmanagerName = "None";
            }
            var clientName = xml.find("clientName").find('name').text();
            if (clientName == "" || clientName == undefined) {
                clientName = "None";
            }
            var ownername = xml.find("OwnerName").find('fname').text();
            if (ownername == "" || ownername == undefined) {
                ownername = "None";
            }
            if (tblaccountmanager.length > 0) {
                $('#AccountManager').empty();
                $.each(tblaccountmanager, function () {
                    if ($(this).find("name").text() == accountmanagerName) {
                        flag = 1;
                    }
                });
                if (flag == 0) {
                    $('#AccountManager').append("<option value='0'>" + accountmanagerName + "</option>");
                }
                $.each(tblaccountmanager, function () {
                    $('#AccountManager').append("<option value=" + $(this).find("Id").text() + ">" + $(this).find("name").text() + "</option>");
                    if ($(this).find("name").text() == accountmanagerName) {
                        $("#AccountManager option:contains(" + accountmanagerName + ")").attr('selected', 'selected');
                    }
                });
            }
            else {
                $('#AccountManager').append("<option value='0'>" + accountmanagerName + "</option>");
            }
            if (tblclient.length > 0) {
                $('#Clients').empty();
                var flag = 0;
                document.getElementById("Clients").innerHTML = "";
                $.each(tblclient, function () {
                    if ($(this).find("name").text() == clientName) {
                        flag = 1;
                    }
                });
                if (flag == 0) {
                    $('#Clients').append("<option value='0'>" + clientName + "</option>");
                }
                $.each(tblclient, function () {
                    $('#Clients').append("<option value=" + $(this).find("ClientId").text() + ">" + $(this).find("name").text() + "</option>");
                    if ($(this).find("name").text() == clientName) {
                        $("#Clients option:contains(" + clientName + ")").attr('selected', 'selected');
                    }
                });
            }
            else {
                $('#Clients').append("<option value='0'>" + clientName + "</option>");
            }
            if (tblassignedmembertoteam.length > 0) {
                $('#ShowMember').empty();
                var usertype = 0;
                var tempUserId = 0;
                var flag = 0;
                var CUserId = 0;
                var OnerUserId = 0;
                var cuserid = $('#cuserid').val();
                var usertypeid = $('#usertypeid').val();
                $.each(tblassignedmembertoteam, function () {
                    if ($(this).find("fname").text() == ownername) {
                        flag = 1;
                    }
                });
                if (flag == 0) {
                    $('#ShowMember').append("<option value='0'>" + ownername + "</option>");
                }
                $.each(tblassignedmembertoteam, function () {
                    tempUserId = cuserid;
                    usertype = usertypeid;
                    $('#ShowMember').append("<option value=" + $(this).find("UserID").text() + ">" + $(this).find("fname").text() + "</option>");
                });
                CUserId = cuserid;
                OnerUserId = $('#ownerID').val();
                if (usertype == 12 || usertype == 14 || CUserId == OnerUserId) {
                    $("#ShowMember").removeAttr("disabled");
                    $("#boardduedate").removeAttr("disabled");
                    $("#ckCompleteBoardId").removeAttr("disabled");
                    $("#Clients").removeAttr("disabled");
                    $("#AccountManager").removeAttr("disabled");
                }
                $("#ShowMember option:contains(" + ownername + ")").attr('selected', 'selected');
            }
            else {
                $('#ShowMember').append("<option value='0'>" + ownername + "</option>");
                tempUserId = cuserid;
                OnerUserId = $('#ownerID').val();
                usertype = usertypeid;
                if (usertype == 12 || usertype == 14 || CUserId == OnerUserId) {
                    $("#ShowMember").removeAttr("disabled");
                    $("#boardduedate").removeAttr("disabled");
                    $("#ckCompleteBoardId").removeAttr("disabled");
                    $("#Clients").removeAttr("disabled");
                    $("#AccountManager").removeAttr("disabled");
                }
            }
        }
    });
}

function InviteMemberOnBoard(email) {
    if (email != "") {
        if (validateInvEmail(email)) {

            SendInvitation(email, $('#boardID').val(), 4);
        }
        else {
            $('#errinvemail').text("Oops! It's an invalid email.");
        }
    }
    else {
        $('#errinvemail').text("Opps ! Please enter email.");
        return false;
    }
}