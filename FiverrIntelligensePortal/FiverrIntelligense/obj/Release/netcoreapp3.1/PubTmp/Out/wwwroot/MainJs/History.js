﻿
var CountriesList = [];
var lstLatLagData = [];
var isMapPloted = false;
$(document).ready(function () {
    $('#ddlBussinessGroup').multiselect({
        buttonWidth: '200px',
        includeSelectAllOption: true,
        multiple: true
    });
    $('#ddlBussinessGroup').multiselect('selectAll', false);
    $('#ddlBussinessGroup').multiselect('updateButtonText');

    $('#ddlBussinessGroup').on('change', function () {
        var startDate = $('#reportrange').data('daterangepicker').startDate.format('YYYY-MM-DD');
        var endDate = $('#reportrange').data('daterangepicker').endDate.format('YYYY-MM-DD');
        var groupId = $('#ddlBussinessGroup').val().join(',');
        GetLastUpdatedDate(groupId);

        $.get("/History/GetAllDataForGrid", { groupId: groupId, startDate: startDate, endDate: endDate }, function (data) {
            $('#divMain').html(data);
        });
    });
    $('#ddlBussinessGroup').trigger('change');
});
function GetLastUpdatedDate(groupId) {
    $.get("/Dashboard/GetLastUpdatedDate", { groupId: groupId }, function (data) {
        data = data === "" ? " NA" : moment(new Date(data.split(' ')[0])).format('MMMM DD, YYYY');
        $('#txtDwnldDate').html(data);
    });
}