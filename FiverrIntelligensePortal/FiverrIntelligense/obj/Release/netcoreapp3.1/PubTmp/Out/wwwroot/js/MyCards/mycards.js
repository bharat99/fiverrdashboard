﻿/// <reference path="../Board-JS/jquery-1.4.2.min.js" />
var TotalBoardList = "";
var flag = 19;
var listIndex = 0;
var ddlIdIndex = 0;
var i = 0;
var addvaluetomultiplylist = 1;
var DomainURL = "";
$(document).ready(function () {
    DomainURL = window.location.origin;
    // BindMyCards();

    var currentPage = 1;
    BindMyCards(currentPage);
    $(window).scroll(function () {
        if ($(window).scrollTop() == $(document).height() - $(window).height()) {
            currentPage += 1;
            BindMyCards(currentPage);
        }
    });

    $('#loadboard').click(function () {
       
        var arrindex = 0;
        addvaluetomultiplylist = addvaluetomultiplylist + 1;

        flag = flag + 1;
        var multiplyboardlist = flag + 20;
        var fullvalue = "";
        var table = $('#mycards').DataTable();
        table.destroy();
        for (var i = listIndex; i < multiplyboardlist; i++) {


            if (i < TotalBoardList.length) {
                if (TotalBoardList[i].taskDeskcription == "") {
                    TotalBoardList[i].taskDeskcription = "No Description";
                }
                if (TotalBoardList[i].hoursandminute != "") {
                    var cardhoursandminute = TotalBoardList[i].hoursandminute.split(':');
                }
                $("#mycards tbody").append('<tr id=c' + TotalBoardList[i].cardid + '><td>' + TotalBoardList[i].cardTitle + '<span class="triangle-up"><img src="images/tool-tip.png"></span><label class="tool-tip">' + TotalBoardList[i].taskDeskcription + '</label></td><td>' + cardhoursandminute[0] + ' hr ' + cardhoursandminute[1] + '  m</td><td>' + TotalBoardList[i].AssignedDate + '</td><td>' + TotalBoardList[i].DueDate + '</td><td>' + TotalBoardList[i].assignby + '</td><td>' + TotalBoardList[i].assignedOn + '</td><td>' + TotalBoardList[i].assignedOnint + '</td><td><a target="_blank" href=' + DomainURL + "/board.aspx?" + TotalBoardList[i].boardid + '>' + TotalBoardList[i].BoardName + '</a></td><td class="listname">' + TotalBoardList[i].listName + '</td><td><select id=' + i + ' class="list"><option value="0">Move To list </option>');
                for (var j = 0; j < TotalBoardList[i].listInfo.length; j++) {
                    $("#" + i).append('<option value=' + TotalBoardList[i].listInfo[j].listId + '>' + TotalBoardList[i].listInfo[j].listName + '</option>')
                }

                $("#" + i).append('</select></td>');
                $('#loadboard').css('display', 'block');
            }
            else {
                $('#loadboard').css('display', 'none');
                BindGrid();
                return false;
            }


            listIndex = i;
            flag = i;

        }
        BindGrid();

        listIndex = listIndex + 1;
        $("#mycards_info").css("display", "none");
    });

    $('.txtSearch').live('keydown keyup', function () {
        var searchval = $(this).val();
        // alert(searchval);

        //if (searchval == "" || searchval == undefined)
        //{
        //    $('#loadboard').css('display', 'block');
        //}
        //else
        //{
        //    $('#loadboard').css('display', 'none');
        //}

        //var rowCount = $('#mycards >tbody >tr>td').length;
        //if (rowCount > 1) {
        //    $('#loadboard').css('display', 'block');
        //}
        //else {
        //    $('#loadboard').css('display', 'none');
        //}

    });

});

function BindGrid() {
   
    $("#mycards").dataTable({
        autoWidth: false,
        "aoColumns": [
        { "bSortable": true },
        { "bSortable": true },
        { "bSortable": true, "sType": "date" },
        { "bSortable": true, "sType": "date" },
        { "bSortable": true },
         { "bSortable": true, "iDataSort": 6, "aTargets": [6]},
        {
            "bSortable": true,

            "visible": false

        },
        { "bSortable": true },
        { "bSortable": true },
        { "bSortable": false }//invisible column numeric
        ],
        "order": [],
        "destroy": true,
        "bPaginate": false,
        "bInfo": true,
        "bFilter": true,
        "bLengthChange": false,
        "bSort": true,
        "iDisplayLength": 20,
        "oSearch": { "bSmart": false, "bRegex": true },
        "columnDefs": [
          { "width": "76px", "targets": 0 },
          { "width": "76px", "targets": 1 },
          { "width": "82px", "targets": 2 },
          { "width": "80px", "targets": 3 },
          { "width": "104px", "targets":4},
          { "width": "95px", "targets": 5 },
          { "width": "87px", "targets": 6 },
          { "width": "87px", "targets": 7 },
          { "width": "50px", "targets": 8 },
          { "width": "90px", "targets": 9 },
        ]
    });
   
    $("#mycards_filter").find('input').eq(0).css({ "border": "1px solid #ccc", "border-radius": "4px" });
    $("#mycards_filter").find('input').eq(0).addClass('txtSearch');
    $("#mycards_info").css("display", "none");
   
}



function BindMyCards(currentPageNumber) {
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "/DataTracker.svc/BindMyCards",
        data: "{\"pageNumber\": \"" + currentPageNumber + "\",\"pageSize\": \"" + 20 + "\"}",
        dataType: "json",
        success: function (data) {

            if (i == 0) {
                $("#mycards").append('<thead><tr class="head" ><th class="reportheard"><img src="images/Card1.png">Card Title</th><th>Hours & Minute</th><th class="reportheard"><img src="images/start.png">Start Date</th><th class="reportheard"><img src="images/start.png">Due Date</th><th class="reportheard" ><img src="images/assigned_by.png">Assigned By</th><th class="reportheard"><img src="images/assigned_on.png">Assigned on</th><th>Assigned</th><th class="reportheard"><img src="images/board.png">Board</th><th class="reportheard"><img src="images/current.png">Current List</th><th class="reportheard"><img src="images/move_card.png">Move Card</th></tr></thead><tbody>');
                i++;
            }


            if (data.d.length > 0) {

                var table = $('#mycards').DataTable();
                table.destroy();
                $.each(data.d, function (index, item) {



                    if (item.taskDeskcription == "" || item.taskDeskcription === undefined) {
                        item.taskDeskcription = "No Description";
                    }

                    var DueDate = "";
                    if (item.DueDate == "01 January 1990" || item.DueDate === undefined || item.DueDate == "") {
                        DueDate = "";
                    }
                    else {
                        DueDate = item.DueDate;
                    }

                    if (item.hoursandminute != "") {
                        var cardhoursandminute = item.hoursandminute.split(':');
                    }

                    var title = "";
                    if (item.cardTitle != "" || item.cardTitle!=undefined) {
                        title = item.cardTitle;
                    }
                 

                    $("#mycards tbody").append('<tr id=c' + item.cardid + '><td>' + item.cardTitle + '<span class="triangle-up"><img src="images/tool-tip.png"></span><label class="tool-tip">' + item.taskDeskcription + '</label></td><td>' + cardhoursandminute[0] + ' hr ' + cardhoursandminute[1] + '  m</td><td>' + item.AssignedDate + '</td><td>' + DueDate + '</td><td>' + item.assignby + '</td><td>' + item.assignedOn + '</td><td>' + item.assignedOnint + '</td><td><a target="_blank" href=' + DomainURL + "/board.aspx?" + item.boardid + '>' + item.BoardName + '</a></td><td class="listname">' + item.listName + '</td><td><select id=' + ddlIdIndex + ' class="list"><option value="0" >Move To list </option>');
                    $("#c" + item.cardid).find('td').eq(0).attr('title', title);

                    for (var j = 0; j < item.listInfo.length; j++) {
                        $("#" + ddlIdIndex).append('<option value=' + item.listInfo[j].listId + '>' + item.listInfo[j].listName + '</option>')
                    }

                    ddlIdIndex = ddlIdIndex + 1;

                });

                BindGrid();
            }



        },
        error: function () {

        }
    });

}



$(document).on('change', 'select', function () {
    //var check = $(this).parent().parent().children('td:eq(6)').text();
    var taskid = $(this).closest('tr').attr('id').replace('c', '');
    var ProjectTypeId = $(this).val();
    var CardNameDescription = $(this).parent().parent().children('td:eq(0)').find('.tool-tip').text();
    var CardName = $(this).parent().parent().children('td:eq(0)').text().replace(CardNameDescription, '');
    var OldListName = $('#c' + taskid + ' .listname').text();
    var ListName = $(".list option[value='" + ProjectTypeId + "']").html();
    var itemName = "<b>" + CardName + "</b> swaped <b> " + OldListName + "</b> To <b>" + ListName + "</b>";
    var ActivityLogType = 2;
    var cardid = taskid;
    var BoardUrl = $(this).parent().parent().children('td:eq(6)').children().attr('href').split('?');
    var BoardId = BoardUrl[1];
    $.ajax({
        type: "POST",
        url: "DataTracker.svc/MoveMyCards",
        data: "{\"taskid\":" + taskid + ",\"ProjectTypeId\":" + ProjectTypeId + "}",
        contentType: "application/json;charset=utf-8",
        dataType: "json",
        success: function (data) {
            $('#c' + taskid + ' .listname').text(ListName);
            UpdateActivityListItem(itemName, ActivityLogType, cardid, BoardId);
        },
        error: function (data) {
        }
    });
});

function UpdateActivityListItem(itemName, ActivityLogType, cardid, BoardId) {

    var Ui_Id = 1;
    itemName = itemName.replace(/\n/g, "<br />");
    var cardidd = 0;
    if (cardid != null && cardid != "" && cardid != 'undefined') {
        cardidd = cardid;
    }
    $.ajax({
        type: "POST",
        url: "/DataTracker.svc/AddActivityLog",
        data: "{\"Message\": \"" + itemName + "\",\"ActivityLogType\": \"" + ActivityLogType + "\",\"boardid\": \"" + BoardId + "\", \"cardid\": \"" + cardidd + "\"}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            if (ActivityLogType == 3) {
                sendmessage();
            }
            specificactivitylog(ActivityLogType);
        },
        error: function (error) {

        }
    });
}