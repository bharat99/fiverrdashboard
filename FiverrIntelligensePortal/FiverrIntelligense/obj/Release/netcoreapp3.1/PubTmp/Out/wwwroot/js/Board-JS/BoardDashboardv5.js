﻿$(document).ready(function () {
    uploadprofile();
    GetTotalActive();
    RecentMessage();
    FavouriteBoardlist();
    ArchieveBoardlist()
    RecentAttachment();
    TopFiveActiveBoard();
    //getdaysofactivity1(30, 'container1');
    GetMylist();
   
});

function FavouriteBoardlist() {
    //document.getElementById("Favoriteboard").innerHTML = "";
    $("#Favorite").empty();
    $myList = $('#Favorite');
    if ($myList.children().length === 0) {
        $.ajax({
            type: "GET",
            url: "/DataTracker.svc/GetFavoriteBoards",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                //document.getElementById("Favoriteboard").innerHTML = "";
                $("#Favorite").empty();
                $.each(data.d, function (index, item) {
                    $('#Favorite').append("<li id='list" + item.BoardID + "' style='margin:0; padding:0 0 5px 0;'><a style='float:left;' href='javascript:myBoardurl(" + item.BoardID + ");'  id='" + item.BoardID + "' title='" + item.BoardName + "'>" + item.BoardName + "</a><a  class='testclass'  href='javascript:removeFavouriteboard(" + item.BoardID + ");'>x</a><div class='clr'></div><div id='progress_bar" + item.BoardID + "'></div></li>");
                    $('#progress_bar' + item.BoardID + '').goalProgress({
                        goalAmount: 100,
                        currentAmount: item.TaskCompletePercent,
                        textBefore: '',
                        textAfter: ''
                    });
                });
            }
        });
    }
}


function uploadprofile() {
    $.ajax({
        type: "GET",
        url: "/DataTracker.svc/getUser",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            document.getElementById("UserProfile").innerHTML = "";
            $.each(data.d, function (index, item) {
                var filename;
                var filenamewithsrc;
                if (item.filename.length > 0) {
                    filename = item.filename;

                }
                else {
                    filename = "DefaultImage.jpg";
                }
                $("#txtFname").val(item.Fname);
                $("#TxtLname").val(item.Lname);
                $("#TxtAddress").val(item.Address);
                $('#UserProfile').append("<div class='display_pic'><img id='ProfileImage' width='130' height='130' src='userimage/" + filename + "' /><div class='image-upload'><label for='file2' ><img src='Images/camera.png' alt='' /></label><input id='file2' type='file' name='file2' data-type='image' onchange='javascript:ProfilePictureupload()'/></div></div><div class='mem_detail'><h4>" + item.Fname + " " + item.Lname + "<a href='#' onclick='javascript:ShowModalForProfile()'><img  src='Images/setting.png' alt=''/></a></h4><a class='change_pass' href='#' onclick='javascript:ShowChangePassBox()'>Change Password</a></div>");
            });

        },
        error: function (data) {
        }

    });
}



function ShowChangePassBox() {
    $('#myModal').modal();

}

function OpenToggel(id) {
    $("#" + id).slideToggle();
    $(".reply_icon_pop").not("#" + id).slideUp('fast');
    return false;
}

function ShowModalForProfile() {
    $('#myModalForProfile').modal();

}

function ChangePassword() {
    var oldpassword = $("#txtCupass");
    var Newpass = $("#TxtNpass");
    var confirmPass = $("#TxtCrpass");
    if (oldpassword.length > 0) {
        if ($("#txtCupass")[0].value != "") {
            $("#txtCupass").addClass("ChangePassBox");
        }
        else {
            $("#ChangeMsg")[0].style.color = "red";
            $("#ChangeMsg")[0].innerHTML = "Please enter password.";
            return false;
        }
    }
    else {
    }
    if (Newpass.length > 0) {
        if (Newpass[0].value != "") {
            $("#TxtNpass").addClass("ChangePassBox");
        }
        else {
            $("#ChangeMsg")[0].style.color = "red";
            $("#ChangeMsg")[0].innerHTML = "Please enter new password.";
            return false;
        }
    }
    else {
    }
    if (confirmPass.length > 0) {
        if (confirmPass[0].value != "") {
            $("#TxtCrpass").addClass("ChangePassBox");
        }
        else {
            $("#ChangeMsg")[0].style.color = "red";
            $("#ChangeMsg")[0].innerHTML = "Please enter confirm password.";
            return false;
        }
    }
    if (Newpass[0].value != confirmPass[0].value) {
        $("#ChangeMsg")[0].style.color = "red";
        $("#ChangeMsg")[0].innerHTML = "New password and confirm password not match.";
        return false;
    }
    $.ajax({
        type: "POST",
        url: "/DataTracker.svc/ChangePass",
        data: "{\"Opass\": \"" + escape(oldpassword[0].value) + "\",\"Npass\": \"" + escape(Newpass[0].value) + "\"",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            var res = data.d;
            if (res != null) {
                if (res == "1") {
                    $("#ChangeMsg")[0].style.color = "green";
                    $("#ChangeMsg")[0].innerHTML = "Your Password has been changed.";
                    closewithtime();
                }
                else if (res == "0") {
                    $("#ChangeMsg")[0].style.color = "red";
                    $("#ChangeMsg")[0].innerHTML = "Your current password does not exist";

                }
                else if (res == "-1") {
                    $("#ChangeMsg")[0].style.color = "red";
                    $("#ChangeMsg")[0].innerHTML = "Some technical problem occur, please retry!";

                }

            }
            else {
                $("#ChangeMsg")[0].style.color = "red";
                $("#ChangeMsg")[0].innerHTML = "Some technical problem occur, please retry!";
                return false;
            }
        }
    });

}

function UpdateProfile() {

    var FName = $("#txtFname").val().replace(/ /g, "");
    var Lname = $("#TxtLname").val().replace(/ /g, "");
    var Address = $("#TxtAddress").val().replace(/ /g, "");

    if (FName == "") {
        $("#ErrorMsg")[0].style.color = "red";
        $("#ErrorMsg")[0].innerHTML = "Please enter first name.";
        return false;
    }
    if (Lname == "") {
        $("#ErrorMsg")[0].style.color = "red";
        $("#ErrorMsg")[0].innerHTML = "Please enter last name.";
        return false;
    }

    $.ajax({
        type: "POST",
        url: "/DataTracker.svc/Updateprofile",
        data: "{\"FName\": \"" + FName + "\",\"Lname\": \"" + Lname + "\",\"Address\": \"" + Address + "\"",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            var res = data.d;
            if (res != null) {
                if (res == "1") {

                    $("#ErrorMsg")[0].style.color = "green";
                    $("#ErrorMsg")[0].innerHTML = "Profile updated successfully.";
                    uploadprofile();
                }
                else if (res == "0") {
                    $("#ErrorMsg")[0].style.color = "green";
                    $("#ErrorMsg")[0].innerHTML = "Profile not updated successfully.";

                }
                else if (res == "-1") {
                    $("#ErrorMsg")[0].style.color = "red";
                    $("#ErrorMsg")[0].innerHTML = "Some technical problem occur, please retry!";

                }
            }
            else {
                $("#ErrorMsg")[0].style.color = "red";
                $("#ErrorMsg")[0].innerHTML = "Some technical problem occur, please retry!";
                return false;
            }
        },
        error: function (data) {
        }
    });
}

function GetTotalActive() {
    $.ajax({
        type: "GET",
        url: "/DataTracker.svc/GetTotalActive",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            document.getElementById("ActiveBoardCount").innerHTML = "";
            $('#ActiveBoardCount').append("<h4>Active Boards <span>(" + data.d[0].ActiveBoard + "/" + data.d[0].TotalBoards + ")</span></h4>");
            $(".demo-4").attr('data-percent', data.d[0].AverageBoard);
            $('.demo-4').percentcircle({
                animate: true,
                coverBg: '#2791d3',
                bgColor: '#2791d3',
                fillColor: '#fff',
                percentWeight: 'normal'
            });
            $('#sample_goal').goalProgress({
                goalAmount: data.d[0].TotalBoards,
                currentAmount: data.d[0].ActiveBoard,
                textBefore: '',
                textAfter: ' / ' + data.d[0].TotalBoards + ''
            });
        },
        error: function (data) {
        }
    });
}

function RecentMessage() {
    $.ajax({
        type: "GET",
        url: "/DataTracker.svc/GetRecentMessage",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {

            document.getElementById("RecentMessage").innerHTML = "";
            $.each(data.d, function (index, item) {
                var filename = item.filename;

                if (item.cardname != "") {
                    $('#RecentMessage').append("<li><div class='user_det'> <img src='userimage/" + filename + "'/><h3>" + item.BoardName + "</h3><p style='text-align: left;'>" + item.cardname + "</p> <div class='clr'></div></div><div class='message_para'><p>" + item.Message + "</p></div> <div class='reply_icon'><a class='toggle_msg_cont' onclick='javascript:OpenToggel(" + item.ActivityID + ");'><img src='Images/reply.png'/></a><div id=" + item.ActivityID + " class='reply_icon_pop'><textarea id='Message" + item.ActivityID + "'></textarea><input type='button' class='submit_button' value='Submit' onclick='javascript:CardReplyMessage(" + item.Taskid + "," + item.ActivityID + ");' /></div></div> <div class='msgdate'>" + item.ActivityDateTime + "</div> <div class='clr'></div> </li>");
                }
                else {
                    $('#RecentMessage').append("<li><div class='user_det'> <img src='userimage/" + filename + "'/><h3>" + item.BoardName + "</h3> <div class='clr'></div></div><div class='message_para'><p>" + item.Message + "</p></div> <div class='reply_icon'><a class='toggle_msg_cont' onclick='javascript:OpenToggel(" + item.ActivityID + ");'><img src='Images/reply.png'/></a><div id=" + item.ActivityID + " class='reply_icon_pop'><textarea id='Message" + item.ActivityID + "'></textarea><input type='button' class='submit_button' value='Submit' onclick='javascript:ReplyRecentMessage(" + item.BoardId + "," + item.ActivityID + ");' /></div></div> <div class='msgdate'>" + item.ActivityDateTime + "</div> <div class='clr'></div> </li>");
                }
            });

        },
        error: function (data) {
        }
    });
}

function ReplyRecentMessage(BoardId, ActivityLogId) {
  
    var Message = $('#Message' + ActivityLogId).val().trim();
  
   // Message = Message.trim();
    Message = Message.replace(/\n/g, "<br />");
    if (Message == "" || Message == undefined || Message == null) {
        alert("Please enter message.");
        return false;
    }

    if (Message.length > 500) {
        alert("Maximum word limit reached (500 Characters)");
        return false;
    }
    $.ajax({
        type: "POST",
        url: "/DataTracker.svc/AddActivityLog",
        data: "{\"Message\": \"" + Message + "\",\"ActivityLogType\": \"3\",\"boardid\": \"" + BoardId + "\"}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            $("#" + ActivityLogId).slideToggle();
            alert('Message Send successfully.');
            RecentMessage();
            TopFiveActiveBoard();
        },
        error: function (error) {
        }
    });
}

function RecentAttachment() {

    $.ajax({
        type: "GET",
        url: "/DataTracker.svc/GetRecentAttachment",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            document.getElementById("RecentAttachments").innerHTML = "";
            $.each(data.d, function (index, item) {

                if (item.cardname != "") {
                    $('#RecentAttachments').append("<li><div class='user_det'><img src='userimage/" + item.ProfilePicture + "' /></div><div class='boardname'><h3>" + item.BoardName + "</h3><p style='text-align: left;'>" + item.cardname + "</p></div><div class='file_name'>" + item.FileName + "</div><div class='dwnld_icon'><a href='" + item.imgurlpath + "' download><img src='Images/dwnld.png'/></a></div><div class='clr'></div></li>");
                }
                else {
                    $('#RecentAttachments').append("<li><div class='user_det'><img src='userimage/" + item.ProfilePicture + "' /></div><div class='boardname'><h3>" + item.BoardName + "</h3></div><div class='file_name'>" + item.FileName + "</div><div class='dwnld_icon'><a href='/TaskTempDoc/" + item.FileName + "' download><img src='Images/dwnld.png'/></a></div><div class='clr'></div></li>");
                }

            });
        },
        error: function (data) {
        }
    });
}


function TopFiveActiveBoard() {
    var Color;
    var star;
    $.ajax({
        type: "GET",
        url: "/DataTracker.svc/GetTopFiveActiveBoards",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {

            document.getElementById("FiveActiveBoard").innerHTML = "";
            $.each(data.d, function (index, item) {
                if (item.IsFavourite == 1) {
                    Color = '#32CD32';
                    star = '&#9733;';
                }
                else {
                    Color = '#ccc';
                    star = '&#9734';
                }
                $('#FiveActiveBoard').append("<li><div class='boardname'><h3>" + item.BoardName + "</h3></div><div class='iconlist'><div class='iconset star_icn' style='color:" + Color + "'>" + star + "</div> <div class='iconset'><img src='Images/attach.png'/><p>" + item.TotalAttachment + "</p></div><div class='iconset'><img src='Images/msg.png'/><p>" + item.TotalMessage + "</p></div><div class='iconset'><img src='Images/read.png' /><p>" + item.TotalCard + "</p></div><div class='clr'></div></div><div class='clr'></div></li>");

            });
        },
        error: function (data) {
        }
    });
}

function ajaxFileUploadProfilePicture(UID, flag) {

    var fileName = "";
    $.ajaxFileUpload
            (
            {
                url: 'AjaxFileUploader.ashx?flag=' + flag + "&Cuid=" + UID,
                fileElementId: 'file2',
                dataType: 'json',
                success: function (data, status) {
                    if (data.d != 0) {
                        uploadprofile();
                        RecentMessage();
                        RecentAttachment();
                        alert('uploaded successfully');
                    }
                    else {
                        alert('failed');
                    }

                },
                error: function (data, status, e) {

                    alert("error in service (kendo script)");
                }
            }
            )

    return fileName;


}

function getdaysofactivity1(day, id) {
    var numofday = 30;
    if (day == 90 || day == 180) {
        numofday = day;
    }
    var a = [];
    var b = [];
    var c = [];
    var d = [];
    $.ajax({
        type: "GET",
        contentType: "application/json; charset=utf-8",
        url: "/DataTracker.svc/GetMyActivityGraph?days=" + numofday + "",
        dataType: "json",
        success: function (data) {
            for (var i = 1; i <= 12; i++) {
                if (i == data.d[0].monthcount) {
                    a.push(data.d[0].TotalArchiveBoards);
                }
                else {
                    a.push(0);
                }
            }
            for (var j = 1; j <= 12; j++) {

                if (j == data.d[0].monthcount) {
                    b.push(data.d[0].TotalTask);
                }
                else {
                    b.push(0);
                }

            }
            for (var k = 1; k <= 12; k++) {
                if (k == data.d[0].monthcount) {
                    c.push(data.d[0].TotalBoard);
                }
                else {
                    c.push(0);
                }
            }
            for (var l = 1; l <= 12; l++) {
                if (l == data.d[0].monthcount) {
                    d.push(data.d[0].activityprocess);
                }
                else {
                    d.push(0);
                }
            }

            $('#' + id).highcharts({

                chart: {
                },

                xAxis: {
                    categories: ["jan", "feb", "mar", "apr", "may", "jun", "jul", "aug", "sep", "oct", "nov", "dec"],
                    tickInterval: 1,
                    labels: {
                        enabled: true
                    }
                },


                series: [{
                    name: "Task Done(" + data.d[0].TotalArchiveBoards + ")",
                    data: a
                }, {
                    name: "New Card(" + data.d[0].TotalTask + ")",
                    data: b
                },

                {
                    name: "New Boards(" + data.d[0].TotalBoard + ")",
                    data: c
                },
                {
                    name: "Task Progress(" + data.d[0].activityprocess + ")",
                    data: d
                }

                ]
            });


        },
        error: function (error) {
        }
    })


}
function GetMylist() {
    $.ajax({
        type: "GET",
        contentType: "application/json; charset=utf-8",
        url: "/DataTracker.svc/GetMyToDoList",
        dataType: "json",
        success: function (data) {
            $.each(data.d, function (index, item) {

                if (item.DueDate != "") {
                    $('#mylistappend').append("<li><div class='board_detail'><h5>" + item.BoardName + "</h5><p>" + item.TitleOfCard + "</p></div><div class='bar'><div id='sample_goal" + item.TaskIDOfCard + "'></div></div><div class='date'>" + item.DueDate + "</div><div class='play'><a href='board.aspx?" + item.BoardID + "'><img src='Images/play.png' alt='play' /></a></div><div class='clr'></div></li>");
                    $('#sample_goal' + item.TaskIDOfCard).goalProgress({
                        goalAmount: item.totaldays,
                        currentAmount: item.remainsdays,
                        textBefore: '',
                        textAfter: '/' + item.totaldays + 'days'
                    });
                    $('#sample_goal' + item.TaskIDOfCard + '  .progressBar').css('background-color', item.colorbar);
                }
                else {
                    $('#mylistappend').append("<li><div class='board_detail'><h5>" + item.BoardName + "</h5><p>" + item.TitleOfCard + "</p></div><div class='bar'><div id='sample_goal" + item.TaskIDOfCard + "'></div></div><div class='date'>N/A</div><div class='play'><a href='board.aspx?" + item.BoardID + "'><img src='Images/play.png' alt='play' /></a></div><div class='clr'></div></li>");
                    $('#sample_goal' + item.TaskIDOfCard).goalProgress({
                        goalAmount: '',
                        currentAmount: '',
                        textBefore: '',
                        textAfter: ''
                    });
                    $('#sample_goal' + item.TaskIDOfCard + '  .progressBar').css('background-color', '#ddd');
                }

            });

        },
        error: function (error) {
        }
    })
}

function CardReplyMessage(cardid, ActivityLogId) {

    var Message = $('#Message' + ActivityLogId).val().trim();
    Message = Message.replace(/\n/g, "<br />");
    if (Message.length <= 0) {
        alert("Please enter message.");
        return false;
    }
    if (Message.length > 500) {
        alert("Maximum word limit reached (500 Characters)");
        return false;
    }
    $.ajax({
        type: "POST",
        url: "/DataTracker.svc/AddCommentsByTaskID",
        data: "{\"Comment\": \"" + Message + "\",\"Task_ID\": \"" + cardid + "\"}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            // $("#" + ActivityLogId).slideToggle();
            alert('Message Send successfully.');
            RecentMessage();
            TopFiveActiveBoard();
        },
        error: function (error) {
        }
    });
}

