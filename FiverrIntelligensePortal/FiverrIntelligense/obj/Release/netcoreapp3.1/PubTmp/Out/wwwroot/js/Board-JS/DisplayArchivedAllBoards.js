﻿

function showArchiveboards()
{
    $('#searchboard').val('');
    $("#searchboard").show();
    $("#allboardretrieve").show();
    $(".selectall_check1").show();
    var operation = "show";
    $.ajax({
        type: "POST",
        url: "/DataTracker.svc/ShowAllArchivedboards",
        data: "{\"OperationStatus\": \"" + operation + "\"}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
          
            if (data.d != 0) {
                $("#archiveboard").empty();
                $.each(data.d, function (index, item) {
                    var str = item.BName;
                    var firstletter = str.charAt(0);
                    $("#archiveboard").append("<div class='member-list-item-detail'><div class='member member-no-menu' ><span title='" + item.BName + "' class='member-initials'>" + firstletter + "</span></div><div class='details' ><p class='name-line' ><input type='hidden' id='bid' value=" + item.BoardID + " /><span class='full-name' id=" + item.BoardID + " >" + item.BName + "</span></p></div><div class='options' ><input type='checkbox' name='chkforboard' id='chkboard' ></div><div class='clr'></div></div>");
                });

            } else {
               
                $("#archiveboard").hide();
                $("#searchboard").hide();
                $("#allboardretrieve").hide();
                $(".selectall_check1").hide();
                $("#archiveboardsnorecod").show();
                $("#archiveboardsnorecod").append("No Record Found In Archived Boards");


            }
        }
    });
}



function searchboard_from_grid1() {
   
    if ($("#searchboard").val() != "") {
        var boardname = $('#searchboard').val();
        var OperationStatus = "search";
        // var Boardid = get_Projectid_from_qstring_onload('bid');
        // var projid = get_Projectid_from_qstring_onload('Pid');
        if (boardname.length > 0) {
            $.ajax({
                type: "POST",
                url: "/DataTracker.svc/ShowAllArchivedboards",
                data: "{\"txtsearch\": \"" + boardname + "\",\"OperationStatus\": \"" + OperationStatus + "\"}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                  
                    $("#archiveboard").empty();
                    $.each(data.d, function (index, item) {
                        var str = item.BName;
                        var firstletter = str.charAt(0);
                        $("#archiveboard").append("<div class='member-list-item-detail'><div class='member member-no-menu' ><span title='" + item.BName + "' class='member-initials'>" + firstletter + "</span></div><div class='details' ><p class='name-line' ><input type='hidden' id='bid' value=" + item.BoardID + " /><span class='full-name' id=" + item.BoardID + " >" + item.BName + "</span></p></div><div class='options' ><input type='checkbox' name='chkforboard' id='chkboard' ></div><div class='clr'></div></div>");
                    });

                }
            });
        }
    } else {
        showArchiveboards();
    }
}


function selectallarchive() {
    if ($("#selectallboards").is(":checked")) {
        $("#archiveboard :checkbox").attr('checked', true);
        return;
    }
    $("#archiveboard :checkbox").attr('checked', false);
    return;
}



function retrieveboard() {
  
    var Boardid = "";
    var Boardid1 = "";
    var BoardidForName = "";
    var allBoardRemovedArchiveName = "";
    var allBoardRemovedArchiveName1 = "";
    $('#archiveboard input:checkbox').each(function () {
      
        Boardid = Boardid + (this.checked ? $(this).parent().siblings().first().next().find('input[type="hidden"]').val() : "0") + ",";
        Boardid2 = Boardid1 + (this.checked ? $(this).parent().siblings().first().next().find('input[type="hidden"]').val() : "0");
        if (Boardid2 != 0) {
            allBoardRemovedArchiveName = allBoardRemovedArchiveName + $('#' + Boardid2 + '').text() + ",";
            allBoardRemovedArchiveName1 = allBoardRemovedArchiveName.split(',');
            var allBoardRemovedArchiveName2 = allBoardRemovedArchiveName1.pop();
            allBoardRemovedArchiveName1 = allBoardRemovedArchiveName1.join(',');

        }

    });
    $.ajax({

        type: "POST",
        data: "{\"boardid\": \"" + Boardid + "\"}",
        url: "/DataTracker.svc/UpdateArchiveboardstatus",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            showArchiveboards();
            UpdateActivityListItem(allBoardRemovedArchiveName1, 29);

        }
    });

}