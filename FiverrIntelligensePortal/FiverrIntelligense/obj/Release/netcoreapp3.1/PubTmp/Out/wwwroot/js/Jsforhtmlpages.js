﻿var randomId = 0;
timezonevaluefromcombo = '';
permissionsfromcombo = '';
//Invite User


function AddInviteUserfromWorkitemPopup(usersemail) {
   
    if ($("#DivInviteUser").data("kendoWindow") != null) {
        var dialogtest = $("#DivInviteUser").data("kendoWindow");
        dialogtest.close();
    }

    var windowElement = $("#DivInviteUser").kendoWindow({
        iframe: true,
        content: "AddInviteUser.htm?sid=" + usersemail,
        modal: true,
        width: "460px",
        title: "Invite User",
        height: "380px",
        position: {
            top: 5,
            left: 2,
            bottom: 5
        },
        resizable: false,
        actions: ["Close"],
        draggable: false

    });
    var dialog = $("#DivInviteUser").data("kendoWindow");
    dialog.open();
    dialog.center();
    return false;
    // }

}
var idd = 1;
function displayallnonexistuser(emails) {

    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "/DataTracker.svc/AddNewUnExistUsersfirst",
        data: "{\"emails\": \"" + emails + "\"",
        dataType: "json",
        success: function (data) {
            $.each(data.d, function (index, item) {
                $("#addnonexistinguser").append("<table style='width:872px;' cellpadding='0' cellspacing='1'><tr> <td align='left' style='width:315px; padding-right:4px ' > <input style='width:100%' type='text' value=" + item.NewUserEmail + " id='emailbox" + idd + "' readonly='true' disabled /> </td> <td align='left' style='width:174px; padding-left:4px;padding-right:4px'> <input style='width:100%' type='text' id='namebox" + idd + "' placeholder='Enter name' /> </td> <td align='left' style='width:182px; padding-left:4px'> <select style='width:100%' id='timezoneselect" + idd + "'> <option  value='+05:30'>Indian Standard Time (IST)</option> <option value='-05:00'>Eastern Standard Time (EST)</option> <option value='−04:00'>Eastern Daylight Time (EDT)</option> </select> </td> <td align='left' style='width:84px; padding-left:4px'> <select style='width:100%' id='permissionselect" + idd + "'> <option value='0~0'>WorkItem</option> <option value='0~1'>Webscrapes</option> <option value='1~1'>All</option> </select> </td> </tr></table>");
                idd = idd + 1;
               
            });
        },
        error: function (result) {

        }
    });
}

function addalluserss() {
    for (var i = 1; i < idd; i++) {
        var email = $('#emailbox' + i + '').val();
        var usernamenew = $('#namebox' + i + '').val();
        if (usernamenew == "") {
            alert("Please enter name!!");
            return false;
        }
        else {
            var uname = usernamenew;
            var tzone = $('#timezoneselect' + i + '').val();
            var permission = $('#permissionselect' + i + '').val();
            var permis = new Array();
            permis = permission.split("~");
            adss = permis[0];
            wscrap = permis[1];
            AddUserFromGrid(email, uname, tzone, adss, wscrap);
        }
    }
    SaveTask();
}


function AddUserFromGrid(email, name, timezone, ads, webscrap) {


    $.ajax({
        type: "POST",
        url: "/DataTracker.svc/AddNewInviteUser",
        data: "{\"email\": \"" + email + "\",\"name\": \"" + name + "\",\"timezone\": \"" + timezone + "\",\"ads\": \"" + ads + "\",\"webscrap\": \"" + webscrap + "\"",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            var res = data.d;
            if (res != null) {

                $("#btnSavePer").attr('value', 'Save');
                //Cancelpermission();
                if (res == "1") {
                    //$("#addnonexistinguser").dataItem($(e.currentTarget).closest("tr").hide());


                }
                else if (res == "0") {

                }
                else if (res == "-3") {

                }


            }
            else {

                return false;
            }
        },
        failure: function (response) {

        },
        error: function (xhr, status, error) {

        }
    });

}


function showbacktaskdiv() {
    // $("#btnSaveSce")[0].attr("value", "Save");
    $("#btnSaveSce").val('Save');
    $("#contioner")[0].style.display = "block";

    $("#divforaddnewuser")[0].style.display = "none";
    var trtt = window.parent.$("#AddTaskWindow").data("kendoWindow");
    trtt.setOptions({
        title: "Add New Work Item"
    });
    $("#addnonexistinguser").html("");
    idd = 1;
}

function SavePermission() {
    $("#btnSavePer").attr('value', 'Saving...');
    if ($("#TxtEmail").length > 0) {
        if ($("#TxtEmail")[0].value.trim() != "") {
            var validEmail = validateMultipleEmailsCommaSeparated($('#TxtEmail')[0].value);
            if (validEmail == false) {
                $("#btnSavePer").attr('value', 'Save');
                return false;
            }
            else {
            }

        }
        else {
            alert("Please enter email");
            $("#btnSavePer").attr('value', 'Save');
            return false;
        }

    }
    else {


    }
    if ($("#Txtname").length > 0) {
        if ($("#Txtname")[0].value.trim() != "") {


        }
        else {
            alert("Please enter name");
            $("#btnSavePer").attr('value', 'Save');
            return false;
        }

    }
    else {
    }
    var e_mail = $("#TxtEmail").val();
    var nameuser = $("#Txtname").val();
    var tzone = $("#HtmComcoTimezone").val();
    var teamid = $('#HtmComcoteamlist').val();
    var add = false;
    var webscrap = false;
    if ($("#secondpermission").is(':checked'))
        webscrap = true;
    else
        webscrap = false;
    if ($("#thirdpermission").is(':checked')) {
        webscrap = true;
        add = true;
    }
    else {
    }
    InserInviteUser(e_mail, nameuser, tzone, teamid, add, webscrap);
}



function Cancelpermission() {
    window.parent.$("#DivInviteUser").data("kendoWindow").close();
}



function checkallper() {
    if ($("#thirdpermission").is(':checked')) {
        $('#secondpermission')[0].checked = true;
    }
    else {
        $('#secondpermission')[0].checked = false;
    }
}

$(document).ready(function () {
    $("#Txtname").live('keydown', function (e) {
        if (e.altKey) {
            e.preventDefault();
        }
        else {
            var key = e.keyCode || e.charCode;
            if (!((key == 8) || (key == 32) || (key == 17) || (key == 46) || (key >= 35 && key <= 40) || (key >= 65 && key <= 90))) {

                e.preventDefault();
            }
        }
    });
});