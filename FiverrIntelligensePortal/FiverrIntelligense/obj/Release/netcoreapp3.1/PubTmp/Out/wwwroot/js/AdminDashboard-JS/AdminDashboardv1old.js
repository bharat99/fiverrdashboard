﻿var Dropdownval = "";
var Dropdownvalactivities = "";
//var TodayComments;
//var WeekComments;
//var TodayActivities;
//var WeekActivities;
//var TodayAttachments;
//var WeekAttachments;
//var allDataOfBoardWeek;
//var allDataOfBoardToday;
var totalCommentsactivity = "";
var totalActivites = "";
var totalAttachment = "";
var allDataofBoard = "";
var DomainURL = "";
$(document).ready(function () {
    DomainURL = window.location.origin;
  
    $('.icon-first-sep').live('click', function () {
        $('.asigned-by-me-cntnr').slideDown();
        $('.asigned-to-me-cntnr').slideUp();
    });
    $('.icon-second-sep').live('click', function () {       
        $('.asigned-to-me-cntnr').slideDown();
        $('.asigned-by-me-cntnr').slideUp();

    });
  
    $("#accordion").accordion();
    (function () {
        var settings = {
            trigger: 'click',
            multi: false,
            closeable: true,
            style: '',
            delay: 300,
            padding: true,
            backdrop: false
        };
        function initPopover() {
            $('a.show-pop').webuiPopover('destroy').webuiPopover(settings);
            var tableContent = $('#tableContent').html(),
            tableSettings = {
                content: tableContent,
                width: 500
            };
            $('a.show-pop-table').webuiPopover('destroy').webuiPopover($.extend({}, settings, tableSettings));
            var listContent = $('#listContent').html(),
            listSettings = {
                content: listContent,
                title: '',
                padding: false
            };
            $('a.show-pop-list').webuiPopover('destroy').webuiPopover($.extend({}, settings, listSettings));
            var largeContent = $('#largeContent').html(),
            largeSettings = {
                content: largeContent,
                width: 400,
                height: 350,
                delay: { show: 300, hide: 1000 },
                closeable: true
            };

        }
        initPopover();
    })();
    $("#tab21").css('display', 'block');
    $("#tab22").css('display', 'none');
    $("#tab23").css('display', 'none');
    $("#tab24").css('display', 'none');
    GetAllProjects();
    BoardCount();
    UserEfforts();
    GetActualTimeVsProductiveTime();

    GetUserTeamWise();
    BoardsForcastChart();
    UserSpecificFavoriteBoard();
    GetAllUsersCard();
    //   AllData();

    //$("#notifytab").click(function () {
    //    Notifications(); $('#tab21').show();
    //    $('#tab22,#tab23,#tab24').hide();
    //});
    //$("#commenttab").click(function () {
    //    ShowComments();
    //    $('#tab22').show();
    //    $('#tab21,#tab23,#tab24').hide();
    //});
    //$("#swapactivitytab").click(function () {
    //    Showswappingactivities();
    //    $('#tab23').show();
    //    $('#tab22,#tab21,#tab24').hide();
    //});
    //$("#attachmenttab").click(function () {
    //    ShowAttachments();
    //    $('#tab24').show();
    //    $('#tab22,#tab23,#tab21').hide();
    //});

    //$('#time').change(function () {
    //    var selectedvalue = $("#time option:selected").text();
    //    Dropdownvalactivities = selectedvalue;
    //    AllData();
    //});

});

//function ShowComments() {
//    $("#ShowCommentTabInfo").empty();
//    var profileimage = "";
//    var countcomments = 0;
//    var timetype = $("#time option:selected").text();
//    if (timetype == 'Today') {
//        $.each(totalCommentsactivity, function (index, item) {
//            countcomments = item.commentcount;
//            var filename = item.profile_pic;
//            if (filename == "" || filename == undefined) { profileimage = "DefaultImage.jpg"; }
//            else { profileimage = filename; }
//            $("#ShowCommentTabInfo").append("<div class='adminDashBoardTab'><div class='adminDashBoardTab_cont' style='position:relative;'><img src='AdminImage/iconsComments.png' alt='ic'  style='position: absolute; width: 20px; top: 38%; left: 3%;'/><p>" + item.comments + "</p><h3>" + item.boardname + "</h3></div><div class='adminDashBoardTab_img'><div class='adminDashBoardTab_img_img1'><img src='userimage/" + profileimage + "' alt='img' /></div><p>" + item.date + "</p></div>  <div class='clr'></div></div>");
//        });
//    }
//    else {
//        $.each(totalCommentsactivity, function (index, item) {
//            countcomments = item.commentcount;
//            var filename = item.profile_pic;
//            if (filename == "" || filename == undefined) { profileimage = "DefaultImage.jpg"; }
//            else { profileimage = filename; }
//            $("#ShowCommentTabInfo").append("<div class='adminDashBoardTab'><div class='adminDashBoardTab_cont' style='position:relative;'><img src='AdminImage/iconsComments.png' alt='ic'  style='position: absolute; width: 20px; top: 38%; left: 3%;'/><p>" + item.comments + "</p><h3>" + item.boardname + "</h3></div><div class='adminDashBoardTab_img'><div class='adminDashBoardTab_img_img1'><img src='userimage/" + profileimage + "' alt='img' /></div><p>" + item.date + "</p></div>  <div class='clr'></div></div>");
//        });
//    }
//    $("#countcomments").text(countcomments);
//}
//function Showswappingactivities() {
//    $("#ShowActivityTabInfo").empty();
//    var profileimage = "";
//    var countactivities = 0;
//    var timetype = $("#time option:selected").text(); if (timetype == 'Today') {
//        $.each(totalActivites, function (index, item) {
//            countactivities = item.swapactivitycount;
//            var filename = item.filename;
//            if (filename == "" || filename == undefined) { profileimage = "DefaultImage.jpg"; }
//            else { profileimage = filename; }
//            $("#ShowActivityTabInfo").append("<div class='adminDashBoardTab'><div class='adminDashBoardTab_cont' style='position:relative;'><img src='AdminImage/iconsProgress.png' alt='ic'  style='position: absolute; width: 20px; top: 38%; left: 3%;'/><p>" + item.Message + "</p><h3>" + item.boardname + "</h3></div><div class='adminDashBoardTab_img'><div class='adminDashBoardTab_img_img1'><img src='userimage/" + profileimage + "' alt='img' /></div><p>" + item.date + "</p></div>  <div class='clr'></div></div>");
//        });
//    }
//    else {
//        $.each(totalActivites, function (index, item) {
//            countactivities = item.swapactivitycount;
//            var filename = item.filename;
//            if (filename == "" || filename == undefined) {
//                profileimage = "DefaultImage.jpg";
//            }
//            else {
//                profileimage = filename;
//            }
//            $("#ShowActivityTabInfo").append("<div class='adminDashBoardTab'><div class='adminDashBoardTab_cont' style='position:relative;'><img src='AdminImage/iconsProgress.png' alt='ic'  style='position: absolute; width: 20px; top: 38%; left: 3%;'/><p>" + item.Message + "</p><h3>" + item.boardname + "</h3></div><div class='adminDashBoardTab_img'><div class='adminDashBoardTab_img_img1'><img src='userimage/" + profileimage + "' alt='img' /></div><p>" + item.date + "</p></div>  <div class='clr'></div></div>");
//        });
//    }
//    $("#countactivities").text(countactivities);
//}
//function ShowAttachments() {
//    var profileimage = "";
//    $("#ShowAttachmentTabInfo").empty();
//    var countattachments = 0;
//    var timetype = $("#time option:selected").text();
//    if (timetype == 'Today') {
//        $.each(totalAttachment, function (index, item) {
//            countattachments = item.countattachment;
//            var filename = item.profile_pic;
//            if (filename == "" || filename == undefined) {
//                profileimage = "DefaultImage.jpg";
//            }
//            else {
//                profileimage = filename;
//            }
//            $("#ShowAttachmentTabInfo").append("<div class='adminDashBoardTab'><div class='adminDashBoardTab_cont' style='position:relative;'><img src='AdminImage/icons-2_0001_Selective-Color-6.png' alt='ic'  style='position: absolute; width: 20px; top: 38%; left: 3%;'/><p>" + item.filename + "</p><h3>" + item.boardname + "</h3></div><div class='adminDashBoardTab_img'><div class='adminDashBoardTab_img_img1'><img src='userimage/" + profileimage + "' alt='img' /></div><p>" + item.date + "</p></div>  <div class='clr'></div></div>");
//        });
//    }
//    else {
//        $.each(totalAttachment, function (index, item) {
//            countattachments = item.countattachment;
//            var filename = item.profile_pic;
//            if (filename == "" || filename == undefined) {
//                profileimage = "DefaultImage.jpg";
//            }
//            else {
//                profileimage = filename;
//            }
//            $("#ShowAttachmentTabInfo").append("<div class='adminDashBoardTab'><div class='adminDashBoardTab_cont' style='position:relative;'><img src='AdminImage/icons-2_0001_Selective-Color-6.png' alt='ic'  style='position: absolute; width: 20px; top: 38%; left: 3%;'/><p>" + item.filename + "</p><h3>" + item.boardname + "</h3></div><div class='adminDashBoardTab_img'><div class='adminDashBoardTab_img_img1'><img src='userimage/" + profileimage + "' alt='img' /></div><p>" + item.date + "</p></div>  <div class='clr'></div></div>");
//        });
//    }
//    $("#countattachments").text(countattachments);
//}
//function Notifications1() {
//    $('#tab21').show(); $('#tab22,#tab23,#tab24').hide();
//    Notifications();
//}
//function AllData() {
//    var ddlActivitySelectval = "";
//    if (Dropdownvalactivities != "") {
//        ddlActivitySelectval = Dropdownvalactivities;
//    }
//    else {
//        ddlActivitySelectval = $("#time option:selected").text();
//    }
//    if (ddlActivitySelectval != "" || ddlActivitySelectval != undefined) {

//        $.ajax({
//            type: 'GET',
//            url: "/DataTracker.svc/AllData?activity=" + ddlActivitySelectval + "",
//            contentType: "application/json; charset=utf-8",
//            datatype: 'json',
//            async: true,
//            success: function (data) {

//                totalCommentsactivity = data.d[0].Combinelist1;
//                totalActivites = data.d[0].Combinelist2;
//                totalAttachment = data.d[0].Combinelist3;
//                allDataofBoard = data.d[0].Combinelist4;

//                //TodayComments = data.d[0].Combinelist1;
//                //WeekComments = data.d[1].Combinelist1;
//                //TodayActivities = data.d[0].Combinelist2;
//                //WeekActivities = data.d[1].Combinelist2;
//                //TodayAttachments = data.d[0].Combinelist3;
//                //WeekAttachments = data.d[1].Combinelist3;
//                //allDataOfBoardToday = data.d[0].Combinelist4;
//                //allDataOfBoardWeek = data.d[1].Combinelist4;

//                ShowComments();
//                Showswappingactivities();
//                ShowAttachments();
//                Notifications();
//                $('#tab21').show();
//                $('#tab22,#tab23,#tab24').hide();
//            },
//            error: function (data)
//            { }
//        });
//    }
//}

//function Notifications() {
//    var profileimage = "";
//    var profileimage1 = "";
//    $("#NotificationsTabInfo").empty();
//    var countcomments = 0;
//    var countactivities = 0;
//    var countattachments = 0;
//    var timetype = $("#time option:selected").text();
//    if (timetype == 'Today') {
//        $('ul.tabs>li').removeClass('active');
//        $('#notifytab').parent().addClass('active');
//        if (allDataofBoard.length > 0) {
//            $.each(allDataofBoard, function (index, item) {
//                var filename = item.profile_pic;
//                filename1 = item.filename;
//                if (filename == "" || filename == undefined) {
//                    profileimage = "DefaultImage.jpg";
//                }
//                else {
//                    profileimage = filename;
//                }
//                if (filename1 == "" || filename1 == undefined) {
//                    profileimage1 = "DefaultImage.jpg";
//                }
//                else {
//                    profileimage1 = filename1;
//                }
//                if (item.uniqueid == 0) {
//                    countcomments = item.commentcount;
//                    $("#NotificationsTabInfo").append("<div class='adminDashBoardTab'><div class='adminDashBoardTab_cont' style='position:relative;'><img src='../AdminImage/iconsComments.png' alt='ic'  style='position: absolute; width: 20px; top: 38%; left: 3%;'/><p>" + item.comments + "</p><h3>" + item.boardname + "</h3></div><div class='adminDashBoardTab_img'><div class='adminDashBoardTab_img_img1'><img src='userimage/" + profileimage + "' alt='img' /></div><p>" + item.date + "</p></div>  <div class='clr'></div></div>");
//                }
//                else if (item.uniqueid == 1) {
//                    countactivities = item.swapactivitycount; $("#NotificationsTabInfo").append("<div class='adminDashBoardTab'><div class='adminDashBoardTab_cont' style='position:relative;'><img src='../AdminImage/iconsProgress.png' alt='ic'  style='position: absolute; width: 20px; top: 38%; left: 3%;'/><p>" + item.Message + "</p><h3>" + item.boardname + "</h3></div><div class='adminDashBoardTab_img'><div class='adminDashBoardTab_img_img1'><img src='userimage/" + profileimage1 + "' alt='img' /></div><p>" + item.date + "</p></div>  <div class='clr'></div></div>");
//                }
//                else if (item.uniqueid == 2) {
//                    countattachments = item.countattachment; $("#NotificationsTabInfo").append("<div class='adminDashBoardTab'><div class='adminDashBoardTab_cont' style='position:relative;'><img src='AdminImage/icons-2_0001_Selective-Color-6.png' alt='ic'  style='position: absolute; width: 20px; top: 38%; left: 3%;'/><p>" + item.filename + "</p><h3>" + item.boardname + "</h3></div><div class='adminDashBoardTab_img'><div class='adminDashBoardTab_img_img1'><img src='userimage/" + profileimage + "' alt='img' /></div><p>" + item.date + "</p></div>  <div class='clr'></div></div>");
//                }
//            });
//        }
//        else {
//            countcomments = 0; countactivities = 0; countattachments = 0;
//        }
//    }
//    else {
//        if (allDataofBoard.length > 0) {
//            $('ul.tabs>li').removeClass('active');
//            $('#notifytab').parent().addClass('active');
//            $.each(allDataofBoard, function (index, item) {
//                var filename = item.profile_pic;
//                filename1 = item.filename;
//                if (filename == "" || filename == undefined) {
//                    profileimage = "DefaultImage.jpg";
//                }
//                else {
//                    profileimage = filename;
//                }
//                if (filename1 == "" || filename1 == undefined) {
//                    profileimage1 = "DefaultImage.jpg";
//                }
//                else {
//                    profileimage1 = filename1;
//                }
//                if (item.uniqueid == 0) {
//                    countcomments = item.commentcount; $("#NotificationsTabInfo").append("<div class='adminDashBoardTab'><div class='adminDashBoardTab_cont' style='position:relative;'><img src='../AdminImage/iconsComments.png' alt='ic'  style='position: absolute; width: 20px; top: 38%; left: 3%;'/><p>" + item.comments + "</p><h3>" + item.boardname + "</h3></div><div class='adminDashBoardTab_img'><div class='adminDashBoardTab_img_img1'><img src='userimage/" + profileimage + "' alt='img' /></div><p>" + item.date + "</p></div>  <div class='clr'></div></div>");
//                }
//                else if (item.uniqueid == 1) {
//                    countactivities = item.swapactivitycount; $("#NotificationsTabInfo").append("<div class='adminDashBoardTab'><div class='adminDashBoardTab_cont' style='position:relative;'><img src='../AdminImage/iconsProgress.png' alt='ic'  style='position: absolute; width: 20px; top: 38%; left: 3%;'/><p>" + item.Message + "</p><h3>" + item.boardname + "</h3></div><div class='adminDashBoardTab_img'><div class='adminDashBoardTab_img_img1'><img src='userimage/" + profileimage1 + "' alt='img' /></div><p>" + item.date + "</p></div>  <div class='clr'></div></div>");
//                }
//                else if (item.uniqueid == 2) {
//                    countattachments = item.countattachment; $("#NotificationsTabInfo").append("<div class='adminDashBoardTab'><div class='adminDashBoardTab_cont' style='position:relative;'><img src='AdminImage/icons-2_0001_Selective-Color-6.png' alt='ic'  style='position: absolute; width: 20px; top: 38%; left: 3%;'/><p>" + item.filename + "</p><h3>" + item.boardname + "</h3></div><div class='adminDashBoardTab_img'><div class='adminDashBoardTab_img_img1'><img src='userimage/" + profileimage + "' alt='img' /></div><p>" + item.date + "</p></div>  <div class='clr'></div></div>");
//                }
//            });
//        }
//        else {
//            countcomments = 0; countactivities = 0; countattachments = 0;
//        }
//    }
//    $("#countcomments").text(countcomments);
//    $("#countactivities").text(countactivities);
//    $("#countattachments").text(countattachments);
//}
function BoardCount() {
    var BoardCountExceedingTime = 0;
    var BoardCountPerformingTime = 0;
    $.ajax({
        type: 'GET',
        url: '/DataTracker.svc/BoardCount',
        contentType: "application/json; charset=utf-8",
        datatype: 'json',
        async: true,
        success: function (data) {

            var len = data.d.length; if (len == 2) {
                BoardCountExceedingTime = data.d[0].Boards;
                BoardCountPerformingTime = data.d[1].Boards;
                BindBoardPerExceedingTime(BoardCountPerformingTime, BoardCountExceedingTime);
            }
            else if (len == 1) {
                if (data.d[0].BoardTimeCheck = "Boards Performing on Time") {
                    BoardCountPerformingTime = data.d[0].Boards;
                    BoardCountExceedingTime = 0;
                    BindBoardPerExceedingTime(BoardCountPerformingTime, BoardCountExceedingTime);
                }
                else {
                    BoardCountExceedingTime = data.d[0].Boards;
                    BoardCountPerformingTime = 0;
                    BindBoardPerExceedingTime(BoardCountPerformingTime, BoardCountExceedingTime);
                }
            }
            else {
                BoardCountExceedingTime = 0;
                BoardCountPerformingTime = 0;
                BindBoardPerExceedingTime(BoardCountPerformingTime, BoardCountExceedingTime);
            }
        }, error: function (data) {
        }
    });
}
function BindBoardPerExceedingTime(BoardPerTime, BoardExceTime) {
    AmCharts.makeChart("chartdiv", { "type": "pie", "balloonText": "[[title]]<br><span style='font-size:10px'>([[percents]]%)</span>", "labelRadius": 3, "outlineColor": "", "outlineThickness": "0", "startDuration": 0, "startAngle": 360, "alpha": 0.99, "colors": ["#ffc875", "#ed622b", "#FF9E01"], "hoverAlpha": 0.65, "marginBottom": 0, "marginTop": 0, "maxLabelWidth": 205, "tabIndex": -2, "titleField": "category", "valueField": "column-1", "fontSize": 10, "percentPrecision": 0, "processCount": 1001, "theme": "light", "allLabels": [], "balloon": {}, "legend": { "enabled": false, "align": "center", "gradientRotation": 7, "left": 0, "marginLeft": 0, "marginRight": 0, "markerBorderThickness": 5, "markerLabelGap": 3, "markerSize": 10, "markerType": "circle", "verticalGap": 7 }, "titles": [], "dataProvider": [{ "category": "Boards Performing<br>On Time", "column-1": BoardPerTime }, { "category": "Boards Exceeding<br>Time Limit", "column-1": BoardExceTime }] });
}
function UserEfforts() {
    var timetype = $("#EffortTime option:selected").text();
    $.ajax({
        type: 'POST',
        url: 'DataTracker.svc/UserEfforts',
        data: "{\"timetype\":\"" + timetype + "\"}",
        contentType: "application/json; charset=utf-8",
        datatype: 'json',
        async: true,
        success: function (data) {
            var chart = AmCharts.makeChart("chartdiv3", { "type": "pie", "startDuration": 0, "outlineColor": "", "outlineThickness": "0", "theme": "light", "radius": 30 + "%", "addClassNames": true, "legend": { "divId": "legend", "horizontalGap": 0, "position": "left", "valueAlign": "left", "markerSize": 10, "autoMargins": false, "verticalGap": 5 }, "innerRadius": "0%", "defs": { "filter": [{ "id": "shadow", "width": "200%", "height": "200%", "feOffset": { "result": "offOut", "in": "SourceAlpha", "dx": 0, "dy": 0 }, "feGaussianBlur": { "result": "blurOut", "in": "offOut", "stdDeviation": 5 }, "feBlend": { "in": "SourceGraphic", "in2": "blurOut", "mode": "normal" } }] }, "dataProvider": JSON.parse(data.d), "titleField": "category", "valueField": "column1", "export": { "enabled": true } }); chart.addListener("init", handleInit); chart.addListener("rollOverSlice", function (e) { handleRollOver(e); }); function handleInit() { chart.legend.addListener("rollOverItem", handleRollOver); }
            function handleRollOver(e) { var wedge = e.dataItem.wedge.node; wedge.parentNode.appendChild(wedge); }
        }, error: function (data) { }
    });
}

//function TotalComments() {
//    var timetype = $("#time option:selected").text();
//    $.ajax({
//        type: 'POST',
//        url: '/DataTracker.svc/TotalComments',
//        data: "{\"timetype\":\"" + timetype + "\"}",
//        contentType: "application/json; charset=utf-8",
//        datatype: 'json',
//        success: function (data) {
//            var countcomments = data.d[0]; $("#countcomments").text(data.d[0]); ShowComments();
//        },
//        error: function (data) {
//        }
//    });
//}
//function TotalCommentsTemp(info) {
//    $("#ShowCommentTabInfo").empty();
//    var countcomments = 0;
//    $.each(info, function (index, item) {
//        countcomments = item.commentcount;
//        $("#ShowCommentTabInfo").append("<div class='adminDashBoardTab'><div class='adminDashBoardTab_cont' style='position:relative;'><img src='AdminImage/icons-2_0001_Selective-Color-6.png' alt='ic'  style='position: absolute; width: 20px; top: 38%; left: 3%;'/><p>" + item.comments + "</p><h3>" + item.boardname + "</h3></div><div class='adminDashBoardTab_img'><div class='adminDashBoardTab_img_img1'><img src='userimage/" + item.profile_pic + "' alt='img' /></div><p>" + item.commenteddate + "</p></div>  <div class='clr'></div></div>");
//    });
//    $("#countcomments").text(countcomments);
//}
//function TotalSwapedActivites() {
//    var timetype = $("#time option:selected").text();
//    $.ajax({
//        type: 'POST',
//        url: '/DataTracker.svc/TotalSwapedActivites',
//        data: "{\"timetype\":\"" + timetype + "\"}",
//        contentType: "application/json; charset=utf-8",
//        datatype: 'json',
//        success: function (data) {
//            var countcomments = data.d[0];
//            $("#countactivities").text(data.d[0]);
//            Showswappingactivities();
//        },
//        error: function (data) { }
//    });
//}
//function TotalSwapedActivitesTemp(info) {
//    var countcomments = 0;
//    $("#ShowActivityTabInfo").empty();
//    $.each(info, function (index, item) {
//        countcomments = item.swapactivitycount;
//        $("#ShowActivityTabInfo").append("<div class='adminDashBoardTab'><div class='adminDashBoardTab_cont' style='position:relative;'><img src='AdminImage/icons-2_0001_Selective-Color-6.png' alt='ic'  style='position: absolute; width: 20px; top: 38%; left: 3%;'/><p>" + item.Message + "</p><h3>" + item.BoardName + "</h3></div><div class='adminDashBoardTab_img'><div class='adminDashBoardTab_img_img1'><img src='userimage/" + item.filename + "' alt='img' /></div><p>" + item.ActivityDate + "</p></div>  <div class='clr'></div></div>");
//    });

//    $("#countactivities").text(countcomments);
//}
//function TotalAttachments() {
//    var timetype = $("#time option:selected").text();
//    $.ajax({
//        type: 'POST',
//        url: '/DataTracker.svc/TotalAttachments',
//        data: "{\"timetype\":\"" + timetype + "\"}",
//        contentType: "application/json; charset=utf-8",
//        datatype: 'json',
//        success: function (data) {
//            var countattachments = data.d[0];
//            $("#countattachments").text(data.d[0]);
//            ShowAttachments();
//        },
//        error: function (data) { }
//    });
//}
//function TotalAttachmentsTemp(info) {
//    $("#ShowAttachmentTabInfo").empty();
//    var countattachments = 0;
//    $.each(info, function (index, item) {
//        countattachments = item.countattchment;
//        $("#ShowAttachmentTabInfo").append("<div class='adminDashBoardTab'><div class='adminDashBoardTab_cont' style='position:relative;'><img src='AdminImage/icons-2_0001_Selective-Color-6.png' alt='ic'  style='position: absolute; width: 20px; top: 38%; left: 3%;'/><p>" + item.filename + "</p><h3>" + item.boardname + "</h3></div><div class='adminDashBoardTab_img'><div class='adminDashBoardTab_img_img1'><img src='userimage/" + item.profile_pic + "' alt='img' /></div><p>" + item.uploadeddate + "</p></div>  <div class='clr'></div></div>");
//    });
//    $("#countattachments").text(countattachments);
//}
$(function () {
    $('#ddlTeamWiseTime').change(function () {
        var firstDropVal = $('#ddlTeamWiseTime').val();
        Dropdownval = firstDropVal;
        GetActualTimeVsProductiveTime();
    })
})
function BindChart(info) {
    AmCharts.makeChart("chartdiv2", {
        "type": "serial", "svgIcons": false, "mouseWheelScrollEnabled": true, "chartScrollbar": { "color": "transparent", "hideResizeGrips": true, "backgroundAlpha": 1, "backgroundColor": "#cccccc", "enabled": true, "autoGridCount": true, "scrollbarHeight": 15, "dragIconWidth": 20, "dragIconHeight": 20 }, "chartCursor": { "cursorPosition": "mouse" }, "categoryField": "category", "angle": 25, "autoMarginOffset": 1, "depth3D": 33, "marginBottom": 1, "marginLeft": 1, "marginRight": 15, "marginTop": 1, "minMarginBottom": 1, "minMarginLeft": 1, "minMarginRight": 1, "minMarginTop": 1, "zoomOutButtonTabIndex": 1, "startDuration": 0, "fontSize": 9, "hideBalloonTime": 149, "percentPrecision": 1, "theme": "light", "categoryAxis": {
            "autoRotateAngle": 0, "gridPosition": "start", "minVerticalGap": 20, "title": "", "labelFunction": function (valueText, serialDataItem, categoryAxis) {
                if (valueText.length > 15)
                    return valueText.substring(0, 15) + '...'; else
                    return valueText;
            }
        }, "trendLines": [], "graphs": [{ "balloonText": "[[title]] of [[category]]:[[value]]", "fillAlphas": 1, "id": "AmGraph-1", "title": "Time consumed", "type": "column", "valueField": "column1" }, { "balloonText": "[[title]] of [[category]]:[[value]]", "fillAlphas": 1, "id": "AmGraph-2", "title": "Time estimated", "type": "column", "valueField": "column2" }, { "id": "AmGraph-3", "legendPeriodValueText": "", "tabIndex": -1, "title": "Time forecast", "valueField": "column3" }], "guides": [], "valueAxes": [{ "id": "ValueAxis-1", "stackType": "3d", "title": "Hours" }], "allLabels": [], "balloon": { "animationDuration": 0, "fadeOutDuration": 0, "fontSize": 10, "horizontalPadding": 12, "offsetY": 2, "pointerWidth": 7, "shadowAlpha": 0 }, "legend": { "enabled": true, "combineLegend": true, "labelWidth": 53, "marginBottom": -20, "marginLeft": 1, "marginTop": 5, "markerSize": 5, "position": "left", "right": 1, "spacing": 8, "top": 2, "useGraphSettings": true, "valueWidth": 25, "verticalGap": 21, "markerSize": 8 }, "titles": [{ "id": "Title-1", "size": 12, "text": "Boards Exceeding Deadline" }], "dataProvider": JSON.parse(info)
    });
}
function GetActualTimeVsProductiveTime() {
    var usertypeid = $('#usertypeid').val();
    if (usertypeid == 12 || usertypeid == 14) {
        if (Dropdownval != "") {
            var val = Dropdownval;
        }
        else {
            var val = $('#ddlTeamWiseTime').val();
        }

        if (val != "" && val != undefined) {
            $.ajax({
                type: 'GET',
                url: "/DataTracker.svc/GetActualTimeVsProductiveTime?totalTeam=" + val + "",
                contentType: "application/json; charset=utf-8",
                datatype: 'json',
                async: true,

                success: function (data) {
                    var res = data.d.length;
                    $('#TeamDetailsId ').empty();
                    if (res != "" && res != undefined) {
                        $.each(data.d, function (index, item) {
                            var workTotalTime = (item.workTotalTime * 100);
                            var IdleTotalTime = (item.IdleTotalTime * 100);
                            var BreakTotalTime = (item.BreakTotalTime * 100);
                            var TotalSum = item.workTotalTime + item.IdleTotalTime + item.BreakTotalTime;
                            var ratioWorks = Math.floor(workTotalTime / TotalSum);
                            var ratioIdle = Math.floor(IdleTotalTime / TotalSum);
                            var ratioBreak = Math.floor(BreakTotalTime / TotalSum);
                            $('.actualTime').css('display', 'block');
                            $('#TeamDetailsId ').append("<li> <div class='actualTime_depart'> <img src='AdminImage/admin-dashboard-Redesign-08-Dec-2016-02.jpg' alt='icon' /> <h4>" + item.TeamName + "</h4><div id='imgg'>  <div id='plain" + item.TeamID + "'></div> </div> <div class='clr'></div></div></li>"); $('#plain' + item.TeamID).multiprogressbar({ parts: [{ value: ratioWorks, barClass: "blue" }, { value: ratioIdle, barClass: "yellow" }, { value: ratioBreak, barClass: "orange" }] });
                        });
                    }
                    else {
                        $('.actualTime').css('display', 'block');
                        $('#TeamDetailsId ').append("No Record available");
                    }
                },
                error: function (err) {
                }
            });
        }
    }
    else {
        $(".actualTime").css("display", "none !important");
        $(".effort_sec").css({ "top": "0", "border-top": "none", "bottom": "initial" });
        $(".admin_right_left_1").css("height", "100%");
    }
}
function UserSpecificFavoriteBoard() {
    $('#flexiselDemo1').empty();
    $.ajax({
        type: 'GET',
        url: 'DataTracker.svc/UsersAllFavoriteBoard',
        contentType: "application/json; charset=utf-8",
        datatype: 'json',
        async: false,
        success: function (data) {
            var res = data.d.length;
            if (res != "" && res != undefined) {
                $.each(data.d, function (index, item) {
                    var totalhoursAndminutes = item.TotalMinute;
                    var estimatedhour = item.EstimatedHours;
                    var values = item.TotalBoardPercent;
                    var totalboardpercent = Math.floor(values);
                    var integerval = parseInt(totalhoursAndminutes);
                    var values1 = integerval * 100;
                    if (isNaN(values1) || values1 == undefined || values1 == "") {
                        values1 = 0;
                    }
                    var valueConsumeboard = values1 / estimatedhour;
                    if (isNaN(valueConsumeboard) || valueConsumeboard == undefined || valueConsumeboard == "") {
                        valueConsumeboard = 0;
                    }
                    var orange = valueConsumeboard - totalboardpercent; if (isNaN(orange) || orange == undefined || orange == "") { orange = 0; }
                    if (totalboardpercent >= valueConsumeboard && valueConsumeboard >= 0) {
                        $('#flexiselDemo1').append("<li><a target='_blank' href='/board.aspx?" + item.BoardID + "'><div class='main_div_text'><div class='progress-bar position' data-percent='" + totalboardpercent + "' data-duration='1000' data-color='#ccc,#7ed6be'></div><h4>" + item.BoardName + "<br></h4></div></a></li>");
                    }
                    else if (orange <= 10 && orange >= 0) {
                        $('#flexiselDemo1').append("<li><a target='_blank' href='/board.aspx?" + item.BoardID + "'><div class='main_div_text'><div class='progress-bar position' data-percent='" + totalboardpercent + "' data-duration='1000' data-color='#ccc,#FFA500'></div><h4>" + item.BoardName + "<br></h4></div></a></li>");
                    }
                    else {
                        $('#flexiselDemo1').append("<li><a target='_blank' href='/board.aspx?" + item.BoardID + "'><div class='main_div_text'><div class='progress-bar position' data-percent='" + totalboardpercent + "' data-duration='1000' data-color='#ccc,#f55758'></div><h4>" + item.BoardName + "<br></h4></div></a></li>");
                    }
                }); $(".progress-bar").loading(); $('input').on('click', function () { $(".progress-bar").loading(); });
            }
        }, error: function (err) { }
    });
}
function GetAllProjects() {
    $.ajax({
        type: 'GET',
        url: 'DataTracker.svc/GetAllProjectUserWise',
        contentType: "application/json; charset=utf-8",
        datatype: 'json',
        async: false,
        success: function (data) {
            document.getElementById("Allprojects").innerHTML = "";
            if (data.d.length > 0) {
                $.each(data.d, function (index, item) {
                    $('#Allprojects').append("<h2>" + item.TodayDate + "</h2><table><tr><td>Total Project</td><td>" + item.TotalProjects + "</td></tr><tr><td>Open Project</td><td>" + item.TotalOpenProjects + "</td></tr><tr><td>Overdue Project</td><td>" + item.TotalOverDueProjects + "</td></tr><tr><td>Task Due This Week</td><td>" + item.TotalTaskDueThisWeek + "</td></tr></table>");
                });
            }
            else {
                var today = new Date();
                var dd = today.getDate();
                var mm = today.getMonth() + 1;
                var yyyy = today.getFullYear();
                if (dd < 10) {
                    dd = '0' + dd
                }
                if (mm < 10) {
                    mm = '0' + mm
                }
                today = mm + '/' + dd + '/' + yyyy; $('#Allprojects').append("<h2>" + today + "</h2><table><tr><td>Total Project</td><td>" + 0 + "</td></tr><tr><td>Open Project</td><td>" + 0 + "</td></tr><tr><td>Overdue Project</td><td>" + 0 + "</td></tr><tr><td>Task Due This Week</td><td>" + 0 + "</td></tr></table>");
            }
        },
        error: function (err) {
        }
    });
}
function BoardsForcastChart() {
    $.ajax({
        type: 'GET',
        url: 'DataTracker.svc/BoardsForcastChart',
        contentType: "application/json; charset=utf-8",
        datatype: 'json',
        async: true,
        success: function (data) {
            BindChart(data.d);
        },
        error: function (err) {
        }
    });
}
function GetUserTeamWise() {
    var usertypeid = $('#usertypeid').val();
    $('#totalUser').css('display', 'none');
  
    if (usertypeid == 12 || usertypeid == 14) {
        $('#totalUser').css('display', 'block');
        $.ajax({
            type: "GET", url: "/DataTracker.svc/GetUserTotalTimeTeamWise", contentType: "application/json; charset=utf-8", dataType: "json", async: false, success: function (data) {
                var res = data.d.length; if (res != "" && res != undefined) {
                    var minUser = data.d[0]; var maxUser = data.d[1]; var totalWork = data.d[2]; var totauser = data.d[3]; var totalusers = totauser.totauser; if (minUser.username.length > 0) { var FirstName = minUser.Fname; var LastName = minUser.Lname; var TotalSecond = minUser.TotalTime; var hours = Math.floor(TotalSecond / 3600); var minutes = Math.floor((TotalSecond - (hours * 3600)) / 60); }
                    else { FirstName = "No user"; LastName = "No user"; TotalSecond = 0; hours = 0; minutes = 0; }
                    if (maxUser.username.length > 0) { var FirstName1 = maxUser.Fname; var LastName1 = maxUser.Lname; var TotalSecond1 = maxUser.TotalTime; var hours1 = Math.floor(TotalSecond1 / 3600); var minutes1 = Math.floor((TotalSecond1 - (hours1 * 3600)) / 60); }
                    else { FirstName1 = "No user"; LastName1 = "No user"; TotalSecond1 = 0; hours1 = 0; minutes1 = 0; }
                    var TotalTeamSecond = totalWork.TotalTime; var TotalTeamsecondavg = Math.floor(TotalTeamSecond / totalusers); var TotalTeamhours = Math.floor(TotalTeamsecondavg / 3600); if (isNaN(TotalTeamhours) || TotalTeamhours == undefined || TotalTeamhours == "") { TotalTeamhours = 0; }
                    var TotalTeamMinutes = Math.floor((TotalTeamsecondavg - (TotalTeamhours * 3600)) / 60); if (isNaN(TotalTeamMinutes) || TotalTeamMinutes == undefined || TotalTeamMinutes == "") { TotalTeamMinutes = 0; }
                    var h = totalWork.TotalTime / 3600; var avgval = h / totalusers; var avg = Math.floor(avgval); if (isNaN(avg) || avg == undefined || avg == "") { var avg = 0; }
                    $("#totalUser").append("<div class='admin_person_time'><div class='lft-text'>" + hours + "<sup>h</sup> " + minutes + "<sup>m</sup><br><p>" + FirstName + " " + LastName + "</p></div><div class='Center-text'> " + TotalTeamhours + "<sup>h</sup> " + TotalTeamMinutes + "<sup>m</sup><br></div><div class='rgt-text'>" + hours1 + "<sup>h</sup> " + minutes1 + "<sup>m</sup><br><p>" + FirstName1 + " " + LastName1 + "</p></div><div class='clr'></div></div><div class='admin_person_progress'><div class='semi'><div class='progress_person'><div class='barOverflow'><div class='progress_person_bar'></div></div><div class='time_details'><span>" + avg + "</span><p>Avg Time Reported</p></div></div></div></div>"); $(".progress_person").each(function () { var $bar = $(this).find(".progress_person_bar"); var $val = $(this).find("span"); var perc = parseInt($val.text(), 10); $({ p: 0 }).animate({ p: perc }, { duration: 3000, easing: "swing", step: function (p) { $bar.css({ transform: "rotate(" + (45 + (p * 1.8)) + "deg)", }); $val.text(p | 0); } }); });
                }
                else {
                    $('#totalUser').css('display', 'block');
                  
                    $("#totalUser").append("<div class='admin_person_time'><div class='lft-text'>" + 0 + "<sup>h</sup> " + 0 + "<sup>m</sup><br><p> No User  </p></div><div class='Center-text'> " + 0 + "<sup>h</sup> " + 0 + "<sup>m</sup><br></div><div class='rgt-text'>" + 0 + "<sup>h</sup> " + 0 + "<sup>m</sup><br><p> No User </p></div><div class='clr'></div></div><div class='admin_person_progress'><div class='semi'><div class='progress_person'><div class='barOverflow'><div class='progress_person_bar'></div></div><div class='time_details'><span>" + 0 + "</span><p>Avg Time Reported</p></div></div></div></div>"); $(".progress_person").each(function () {
                        var $bar = $(this).find(".progress_person_bar"); var $val = $(this).find("span"); var perc = parseInt($val.text(), 10);
                        $({ p: 0 }).animate({ p: perc }, { duration: 3000, easing: "swing", step: function (p) { $bar.css({ transform: "rotate(" + (45 + (p * 1.8)) + "deg)", }); $val.text(p | 0); } });
                    });
                }
            }, error: function (error) { }
        });
    }
   
}

function GetAllUsersCard() {
    $.ajax({
        type: 'GET',
        url: '/DataTracker.svc/GetAllCardUsers',
        contentType: "application/json; charset=utf-8",
        datatype: 'json',
        success: function (data) {
            if (data.d.length > 0) {
                $('.asigned-by-me-cntnr').empty();
                $('.asigned-to-me-cntnr').empty();
                $.each(data.d, function (index, item) {
                    var DueDate = "";
                    if (item.DueDate == "01/Jan/1990" || item.DueDate === undefined || item.DueDate == "") {
                        DueDate = "";
                    }
                    else {
                        DueDate = item.DueDate;
                    }
                    $('.asigned-by-me-cntnr').append("<div class='not-grey-full-strip'><a target='_blank' href='" + DomainURL + "/board.aspx?" + item.boardid + "'>" + item.BoardName + "</a></div><ul><li><span>Card:</span>" + item.Title + "</li><li><span>List:</span>" + item.projecttype + " </li><li><span>Start Date:</span><label>" + item.AssignedDate + "</label><span>Due Date:</span><label>" + DueDate + "</label> </li><li><span>Assigned To:</span>" + item.assignedToFname + " " + item.assignedToLname + " </li></ul>");
                    $('.asigned-to-me-cntnr').append("<div class='not-grey-full-strip'><a target='_blank' href='" + DomainURL + "/board.aspx?" + item.boardid + "'>" + item.BoardName + "</a></div><ul><li><span>Card:</span>" + item.Title + "</li><li><span>List:</span>" + item.projecttype + " </li><li><span>Start Date:</span><label>" + item.AssignedDate + "</label><span>Due Date:</span><label>" + DueDate + "</label> </li><li><span>Assigned By:</span>" + item.assignby + "</li></ul>");
                });
            }
            else {
                $('.asigned-by-me-cntnr').empty();
                $('.asigned-to-me-cntnr').empty();
                $('.asigned-by-me-cntnr').append("No record.");
                $('.asigned-to-me-cntnr').append("No record.");
            }

        },
        error: function () {

        }
    });
}

//function ArchieveBoardlist() { $("#Archive").empty(); $myList = $('#Archive'); var operation = "show"; if ($myList.children().length === 0) { $.ajax({ type: "POST", url: "/DataTracker.svc/ShowAllArchivedboards", data: "{\"OperationStatus\": \"" + operation + "\"}", contentType: "application/json; charset=utf-8", dataType: "json", success: function (data) { $("#Archive").empty(); $.each(data.d, function (index, item) { $('#Archive').append("<li id='list" + item.BoardID + "' style='margin:0; padding:0 0 5px 0;'><a style='float:left;' href='javascript:myBoardurl(" + item.BoardID + ");'  id='" + item.BoardID + "' title='" + item.BName + "'>" + item.BName + "</a><a  class='testclass'  href='javascript:removeFavouriteboard(" + item.BoardID + ");'>x</a><div class='clr'></div><div id='progress_bar" + item.BoardID + "'></div></li>"); }); } }); } }

//function ArchieveBoardlist() {

//    $("#Archive").empty();
//    $myList = $('#Archive');
//    var operation = "show";
//    if ($myList.children().length === 0) {
//        $.ajax({
//            type: "POST",
//            url: "/DataTracker.svc/ShowAllArchivedboards",
//            data: "{\"OperationStatus\": \"" + operation + "\"}",
//            contentType: "application/json; charset=utf-8",
//            dataType: "json",
//            success: function (data) {
//                $("#Archive").empty();
//                $.each(data.d, function (index, item) {
//                    $('#Archive').append("<li id='list" + item.BoardID + "' style='margin:0; padding:0 0 5px 0;'><a style='float:left;' href='javascript:myBoardurl(" + item.BoardID + ");'  id='" + item.BoardID + "' title='" + item.BName + "'>" + item.BName + "</a><a  class='testclass'  href='javascript:removeFavouriteboard(" + item.BoardID + ");'>x</a><div class='clr'></div><div id='progress_bar" + item.BoardID + "'></div></li>");
//                });
//            }
//        });




//    }
//}