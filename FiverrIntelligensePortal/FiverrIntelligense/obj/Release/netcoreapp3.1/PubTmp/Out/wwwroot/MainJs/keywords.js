﻿var CurretnGroupId = null;
$(document).ready(function () {

    $('#ddlBussinessGroup').multiselect({
        buttonWidth: '200px',
        includeSelectAllOption: true,
        nonSelectedText: 'Select an Option'
    });
    $.get("/Keyword/GetAllGroupsAndKeywords", {}, function (data) {
        data = JSON.parse(data);
        AllGroups = data.Groups;
        AllKeywords = data.Keywords;
        autocomplete(document.getElementById("txtGroupName"), $.map(AllGroups, function (item, index) {
            return item.GroupName;
        }));
        //autocomplete(document.getElementById("txtKeywordGroup"), $.map(AllGroups, function (item, index) {
        //    return item.GroupName;
        //}));
        $('#dllKeywordGroup').html('');
        $('#dllKeywordGroup').append('<option value="-1">Select Group</option>');
        for (var i = 0; i < AllGroups.length; i++) {
            $('#dllKeywordGroup').append('<option value="' + AllGroups[i].GroupId + '">' + AllGroups[i].GroupName + '</option>');
        }
    });

    /// load all groups with keywords

    AllGroupsWithKeywords();

    $('#dllKeywordGroup').on('change', function () {
        GetAllKeywordsOfGroup($('#dllKeywordGroup').val());
    })

    $('#frmAddGroup input[type="button"]').on('click', function () {
        var GroupName = $('#txtGroupName').val();
        if (GroupName.trim() === "") {
            toastr.error('Fill group Name.');
            return false;
        }
        $.post("/group/AddGroup", { GroupName: GroupName }, function (data) {
            if (data !== 'success') {
                toastr.error(data);
            }
            else {
                toastr.success('Group added successfully.');
                $('#addGroupModel').modal('hide');
                AllGroupsWithKeywords();
            }
        });
    });

    $('#btnEditKeyword').on('click', function () {
        var KeywordID = $('#txtEditKeywordId').val();
        var KeyName = $('#txtEditKeywordName').val();
        $.post("/Keyword/EditKeyword", { KeywordID: KeywordID, keywordName: KeyName }, function (data) {
            if (data !== 'success') {
                toastr.error(data);
            }
            else {
                toastr.success('Keyword updated successfully.');
                $('#editKeywordModel').modal('hide');
                //$('#ddlBussinessGroup').trigger('change');
                GetAllKeywordDetailsOCurrentfGroup();
            }
        });
    });
    $('#frmAddKeyword input[type="button"]').on('click', function () {
        var keywordName = $('#txtKeywordName').val();
        //var groupid = $('#dllKeywordGroup').val();
        //if (groupid.trim() === "") {
        //    toastr.error('Please select group from dropdown.');
        //    return false;
        //}
        if (keywordName.trim() === "") {
            toastr.error('Fill Keyword Name.');
            return false;
        }
        $.post("/Keyword/AddKeyword", { keywordName: keywordName, groupId: CurretnGroupId }, function (data) {
            if (data !== 'success') {
                toastr.error(data);
            }
            else {
                toastr.success('Keyword added successfully.');

                $('#addKeywordModel').modal('hide');
                //$('#ddlBussinessGroup').trigger('change');
                GetAllKeywordDetailsOCurrentfGroup();
            }
        });
    });

    $('#ddlBussinessGroup').on('change', function () {
        if ($('#ddlBussinessGroup').val().join(',') === "") {
            toastr.error('Select business group.');
        }
        $.get("/Keyword/GetAllKeywordsDetails", { GroupId: $('#ddlBussinessGroup').val().join(',') }, function (data) {
            $('#dtKeywords tbody').html('');
            if (data === "") {
                $('#dtKeywords tbody').append('<tr><td colspan="6" class="text-center">No Data Found.</td></tr>');
            }
            else {
                data = JSON.parse(data);

                if (data.length === 0) {
                    $('#dtKeywords tbody').append('<tr><td colspan="6" class="text-center">No Keyword Found.</td></tr>');
                    //$('#btnShowAddKeyword').prop('disabled', '');
                }
                else {
                    //$('#btnShowAddKeyword').prop('disabled', '');
                    for (var i = 0; i < data.length; i++) {
                        var item = data[i];
                        $('#dtKeywords tbody').append('<tr> <td>' + (i + 1) + '</td> <td>' + item.GroupName + '</td><td>' + item.KeywordName + '</td><td>' + item.ServicesAvailable + '</td> <td>' + item.CreatedOn + '</td> <td></td> <td><a onclick="RemoveKeywordFromUser(' + item.KeywordID + ')" class="" title="Remove" style="cursor:pointer;"><i class="ti-trash"></i></a>   <a onclick=EditKeyword("' + item.KeywordID + '","' + item.KeywordName + '")  class="" title="Edit" style="cursor:pointer;"><i class="ti-credit-card"></i></a></td> </tr>');
                    }
                }

                //GetAllKeywordsOfGroup($('#ddlBussinessGroup').val());
            }
        });
    })
});

function GetAllKeywordDetailsOCurrentfGroup() {
    $.get("/Keyword/GetAllKeywordsDetails", { GroupId: CurretnGroupId }, function (data) {
        $('#tbody_' + CurretnGroupId).html('');
        if (data === "") {
            $('#tbody_' + CurretnGroupId).append('<tr><td colspan="6" class="text-center">No Data Found.</td></tr>');
        }
        else {
            data = JSON.parse(data);

            if (data.length === 0) {
                $('#tbody_' + CurretnGroupId).append('<tr><td colspan="6" class="text-center">No Keyword Found.</td></tr>');
                //$('#btnShowAddKeyword').prop('disabled', '');
            }
            else {
                //$('#btnShowAddKeyword').prop('disabled', '');
                for (var i = 0; i < data.length; i++) {
                    var item = data[i];
                    $('#tbody_' + CurretnGroupId).append('<tr> <td>' + (i + 1) + '</td><td>' + item.KeywordName + '</td><td>' + (item.ServicesAvailable !== null ? item.ServicesAvailable : "NA") + '</td> <td>' + moment(item.CreatedOn).format('MMMM Do YYYY') + '</td> <td></td> <td><a onclick="RemoveKeywordFromUser(' + item.KeywordID + ')" class="" title="Remove" style="cursor:pointer;"><i class="ti-trash"></i></a>   <a onclick="EditKeyword(' + item.KeywordID + ',\'' + item.KeywordName + '\')"  class="" title="Edit" style="cursor:pointer;"><i class="ti-pencil"></i></a></td> </tr>');
                }
            }

            //GetAllKeywordsOfGroup($('#ddlBussinessGroup').val());
        }
    });
}
function AllGroupsWithKeywords() {
    $.get("/Keyword/GetAllKeywordsDetailsOfAllGroups", {}, function (data) {
        if (data === "") {
            $('#accordion').html('<center>No Group found.</center>')
            toastr.error('No Groups found.');
        }
        else {
            $('#accordion').html('');
            data = JSON.parse(data);
            for (var i = 0; i < data.length; i++) {
                var grp = data[i];
                var cls = i === 0 ? 'show' : '';
                var clsPM = i === 0 ? 'minus' : 'plus';
                if (i === 0) {
                    CurretnGroupId = grp.GroupId;
                }
                var grpHtml = '<div class="card mB-10"> <div class="card-header" id="heading_' + grp.GroupId + '"> <h5 class="mb-0"> <button id="' + grp.GroupId + '" class="btn btn-link btnToggleCollapse" data-toggle="collapse" data-target="#collapse_' + grp.GroupId + '" aria-expanded="true" aria-controls="collapse_' + grp.GroupId + '" style=" width: 100%; text-align: left; "> Business Group : ' + grp.GroupName + '  <i class="fa fa-' + clsPM + ' mT-5" style=" float: right; "></i></button></h5> </div>';
                grpHtml += '<div id="collapse_' + grp.GroupId + '" class="collapse ' + cls + '" aria-labelledby="heading_' + grp.GroupId + '" data-parent="#accordion">';
                grpHtml += '<div class="card-body">';
                grpHtml += '<div class="col-md-12 form-row">';
                grpHtml += '<div class="col-md-12" style="float:right;">';
                //grpHtml += '<input type="button" class="btn cur-p btn-primary mL-10" style="float:right;" data-toggle="modal" data-target="#addGroupModel" value="Add Business Group" />';
                grpHtml += '<input id="btnShowAddKeyword" type="button" class="btn cur-p btn-primary" style="float:right;" data-toggle="modal" data-target="#addKeywordModel" value="Add Keyword" />';
                grpHtml += '</div></div><div class="col-md-12 mT-10">';
                grpHtml += '<table id="dtKeywords" class="table table-striped table-bordered" cellspacing="0" width="100%">';
                grpHtml += '<thead><tr><th>S.No.</th><th>Keyword</th><th>Services Available</th><th>Added On</th><th>Last Check</th><th>Action</th></tr></thead><tbody id="tbody_' + grp.GroupId + '">';

                if (grp.lstKeyword.length > 0) {
                    for (var j = 0; j < grp.lstKeyword.length; j++) {
                        var item = grp.lstKeyword[j];
                        grpHtml += '<tr> <td>' + (j + 1) + '</td><td>' + item.KeywordName + '</td><td>' + (item.ServicesAvailable !== null ? item.ServicesAvailable : "NA") + '</td> <td>' + moment(item.CreatedOn).format('MMMM DD, YYYY') + '</td> <td></td> <td><a onclick="RemoveKeywordFromUser(' + item.KeywordID + ')" class="" title="Remove" style="cursor:pointer;"><i class="ti-trash"></i></a>   <a onclick="EditKeyword(' + item.KeywordID + ',\'' + item.KeywordName + '\')"  class="" title="Edit" style="cursor:pointer;"><i class="ti-pencil"></i></a></td> </tr>';
                    }
                }
                else {
                    grpHtml += '<tr> <td colspan="6" class="text-center"> No Keyword Found. </td> </tr>';
                }
                grpHtml += '</tbody></table></div></div></div>';
                $('#accordion').append($(grpHtml)[0]);
            }

            $(".collapse").on('show.bs.collapse', function () {
                $(this).prev(".card-header").find(".fa").removeClass("fa-plus").addClass("fa-minus");
            }).on('hide.bs.collapse', function () {
                $(this).prev(".card-header").find(".fa").removeClass("fa-minus").addClass("fa-plus");
            });
            $('.btnToggleCollapse').on('click', function () {
                CurretnGroupId = this.id;
            });
        }
    });
}
function GetAllKeywordsOfGroup(groupId) {
    if (groupId) {
        $.get("/Keyword/GetAllKeywordsOfGroup", { GroupId: groupId }, function (data) {
            if (data === "") {
                AllKeywords = [];
            }
            else {
                AllKeywords = JSON.parse(data);
            }
            autocomplete(document.getElementById("txtKeywordName"), $.map(AllKeywords, function (item, index) {
                return item.KeywordName;
            }));
        });
    }
}

function RemoveKeywordFromUser(KeywordID) {
    $.confirm({
        title: 'Remove Keyword!',
        content: 'Do you want to remove?',
        buttons: {
            confirm: function () {
                $.post("/Keyword/RemoveKeywordFromGroup", { keywordId: KeywordID }, function (data) {
                    if (data === 'success') {
                        toastr.success("Removed Successfully.");
                        //$('#ddlBussinessGroup').trigger('change');
                        GetAllKeywordDetailsOCurrentfGroup();
                    }
                    else {
                        toastr.error(data);
                    }
                });
            },
            cancel: function () {
                //$.alert('Canceled!');
            }
        }
    });
    
}

function EditKeyword(KeywordID, KeywordName) {

    $('#txtEditKeywordId').val(KeywordID);
    $('#txtEditKeywordName').val(KeywordName);
    $('#editKeywordModel').modal('show');
}