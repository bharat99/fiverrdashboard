﻿/// <reference path="../ClientDetails.aspx" />
/// <reference path="../ClientDetails.aspx" />
/// <reference path="../ClientDetails.aspx" />
childSID = 0;
var ListStoreName = "";
var ListStartDate = "";
var ListDueDate = "";
var StatusIds = "";
var ListWebSite = "";
var text = "";
var CompletedStatus = "";
var childOverDue = false;
var ClientTypeID = 16;
var SubBulkDetail = "";
IsDeveloper = false;
var OtherTaskTab = false;
var RegularTaskTab = false;
var ScheduleTab = false;
var WorkItemID = false;
var OffcyleAds1 = false;
var OffcyleAds2 = false;
var OffcyleAds3 = false;
var OffcyleAds4 = false;
var slideright = 65;
var emailTaskID = 0;
var statusidofemailtask = "";
var slideleft = 0;
var arrStatus = new Array;
var Offoptions = new Array();
var CurrentDomainName = "";

$(document).ready(function () {
    CDomainname = window.location.href;
    CurrentDomain = CDomainname.split('/');
    CurrentDomainName = CurrentDomain[2];

    if ($("#Dashdialog").data("kendoWindow") != null) {
        var dialogschedule = $("#Dashdialog").data("kendoWindow");
        dialogschedule.close();
    }
    if ($("#Bulkstatusdialog").data("kendoWindow") != null) {
        var dialogbulk = $("#Bulkstatusdialog").data("kendoWindow");
        dialogbulk.close();
    }
    if ($("#AddTaskWindow").data("kendoWindow") != null) {
        var dialogTask = $("#AddTaskWindow").data("kendoWindow");
        dialogTask.close();
    }
    $("#TxtLog").kendoNumericTextBox();
    // strat date grid
    if ($("#StartGrid").length > 0) {
        $("#StartGrid")[0].innerHTML = "";
    }

    $("#DivReportMenu").kendoMenu({
        select: onReportSelect

    });

    $('#OnAssignedClick').click(function () {
        GetBoardData();
    });
    $('#OnManualClick').click(function () {
        $('#BNameId').empty(); $('#LNameId').empty(); $('#SNameId').empty(); $('#CNameId').empty();
        $("#BId").hide(); $("#SId").hide(); $("#LId").hide(); $("#CId").hide();
        $("#TId").show();
        $("#webuiPopover23").css("top", "202px");
        $('#OnAssignedClick').css("background", "darkgray");
        $('#OnManualClick').css("background", "black");
        $('#taskbarid').css("background-color", "black");
    });

    $("#BNameId").on('change', function () {

        var BID = $('#BNameId').find("option:selected").val();
        if (BID == "0") {
            $('#SNameId').empty();
            $('#LNameId').empty();
            $('#CNameId').empty();
        } else {
            $.ajax({
                type: "POST",
                url: "/DataTracker.svc/GetSprintDataOnBoardClick",
                data: "{\"BoardID\":\"" + BID + "\"}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    var response = JSON.parse(data.d);
                    $('#SNameId').empty();
                    $("#SNameId").append("<option value='0'>Select Sprints Name</option>");
                    $.each(response, function (index, item) {
                        $("#SNameId").append($("<option></option>").val(item.SprintBoardRelationId).html(item.Sprint_Name));
                    });
                }
            });
        }
    });

    $("#SNameId").on('change', function () {

        var SID = $('#SNameId').find("option:selected").val();
        if (SID == "0") {
            $('#LNameId').empty();
            $('#CNameId').empty();
        } else {
            $.ajax({
                type: "POST",
                url: "/DataTracker.svc/GetListDataOnBoardClick",
                data: "{\"SprintID\":\"" + SID + "\"}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    var response = JSON.parse(data.d);
                    $('#LNameId').empty();
                    $("#LNameId").append("<option value='0'>Select Lists Name</option>");
                    $.each(response, function (index, item) {
                        $("#LNameId").append($("<option></option>").val(item.ProjectID).html(item.projecttype));
                    });
                }
            });
        }
    });

    $("#LNameId").change(function () {
        var ListID = $('#LNameId').find("option:selected").val();
        if (ListID == "0") {
            $('#CNameId').empty();
        } else {
            $.ajax({
                type: "POST",
                url: "/DataTracker.svc/GetCardDataOnListClick",
                data: "{\"List_ID\":\"" + ListID + "\"}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    var response = JSON.parse(data.d);
                    $('#CNameId').empty();
                    $("#CNameId").append("<option value='0'>Select Cards Name</option>");
                    $.each(response, function (index, item) {
                        $("#CNameId").append($("<option></option>").val(item.TaskID).html(item.Title));
                        var card_id = $('#CNameId').find("option:selected").val();
                    });
                }
            });
        }
    });

    $("#CNameId").change(function () {
        var CardID = $('#CNameId').find("option:selected").val();
        if (CardID == "0") {
            $('#CNameId').empty();
        } else {
            debugger;
            showownermember(CardID);
        }
    });

    $('#assignedTextArea').click(function () {
        $('#BNameId').empty();
        $('#SNameId').empty();
        $('#LNameId').empty();
        $('#CNameId').empty();
    });

    WebscrapesMenu(-1, -1);
    AADMenu(-1, -1);
    AdsMenu(-1, -1);
    ActionsMenu(-1, -1);
    ReportsMenu(-1, -1);
    AssignedTaskMenu(-1, -1);
    ClientMenu(-1, -1);
    HRMReport(-1, -1);
    HRMMenu(-1, -1);
    CommonExpectationMenu(-1, -1);
    IncidentReportingMenu(-1, -1);
});



function SettingMenu(UserName) {
    $("#SettingMenu").kendoMenu({
        dataSource: [
                        {
                            text: "Hi, " + UserName + "", imageUrl: "/Images/profilepic.png",
                            items: [
                                { text: "Change Password", imageUrl: "/Images/Icon/NewIcons/password-lock.png", width: "180px" },
                                { text: "Logout", imageUrl: "/Images/Icon/NewIcons/logout.png", width: "180px" },
                                //{ text: "Email Preference", imageUrl: "/Images/Icon/NewIcons/password-lock.png", width: "180px" }
                            ]
                        }
        ],
        select: onReportSelect

    });
}
function ActionsMenu(Ads, WebScrape) {
    $("#ActionsMenu").kendoMenu({
        dataSource: ShowMenuList(Ads, WebScrape),
        select: onReportSelect

    });

    var menu = $("#ActionsMenu").data("kendoMenu");
}

function showeditmembers(sender, arg) {
    var teamid = 0;
    $('.member_cont').empty();
    var oneditmembers = $("#txteditmembers").val();
    if (oneditmembers.length > 0) {
        $.ajax({
            type: "GET",
            url: "/DataTracker.svc/Getmembername?membersname=" + oneditmembers + "&TeamID=" + teamid + "",
            dataType: "json",
            async: false,
            success: function (data) {
                if (data.d.length > 0) {
                    $.each(data.d, function (index, item) {
                        var str = item.Email;
                        var firstletter = str.charAt(0);
                        var username = item.username;
                        $("#appendmembers").append("<span onclick='subscribetouser(\"" + firstletter + "\",\"" + str + "\"  ," + item.userid + ",\"" + username + "\");'> <a href='#'><h2>" + firstletter + "</h2><div class='member_id'><p title='" + item.Email + "'>" + item.Email + "</p></div><div class='clr'></div></a></span>");
                        $("#appendmembers").show();
                    });
                }
                else {
                    $("#appendmembers").append("<span><p id='noResult' style='width:246px;padding: 24px 6px;'>No Result</p><div class='member_id'></div><div class='clr'></div></span>");
                }
            },
            error: function (error) { alert('Error has occurred!'); alert(JSON.stringify(error)) }
        });
    }
}

function SubmitPopup() {
    var TaskId = "";
    var TextAreaVlue = $('#assignedTextArea').val();
    if (TextAreaVlue != "") {
        $.ajax({
            type: "POST",
            url: "/DataTracker.svc/GetInsertedTaskId",
            data: "{\"Card_Name\": \"" + TextAreaVlue + "\"}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                var response = JSON.parse(data.d);
                $.each(response, function (index, item) {
                    TaskId = item.TCount;
                });
                if (TaskId != 0) {
                    if (TextAreaVlue != "") {
                        alert("Task Successfully Assigned");
                    } else {
                        alert("Please Create Task.");
                    }
                }
                else {
                    alert("Please assigned Member on Task")
                }
            }
        });
    }
    else {
        if ($('#CNameId').val() != null) {
            var card_id = $('#CNameId').find("option:selected").val();
            $.ajax({
                type: "POST",
                url: "/DataTracker.svc/GetAssignedTaskIdOnly",
                data: "{\"Card_Id\": \"" + card_id + "\"}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    var response = JSON.parse(data.d);
                    $.each(response, function (index, item) {
                        TaskId = item.TCount;
                    });
                    if (TaskId != 0) {
                        alert("Task Successfully Assigned");
                    }
                    else {
                        alert("Please assigned Member on Task")
                    }
                }
            });
        }
        else {
            alert("please select the Card first");
        }
    }
}

function assignmembersToCardsOnClick(cardid, assignedto, firstletter, email, username) {
    $.ajax({
        type: "POST",
        url: "/DataTracker.svc/assigneduserfortaskonnewcard",
        data: "{\"assignedto\": \"" + assignedto + "\",\"cardid\":" + cardid + "}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            var newMember = "in " + cardNameOfComment;
            if (data.d != 0) {
                SendNotificationMailForAddMemberOnCard(cardNameOfComment, cardid, email);
                username = username != (null) ? username : '';
                $("#showsubsuser").append("<div class='subuser-pop-cont'><a id=userfirstletter href='#' >" + firstletter + "</a><div class='subuser-pop-data'><div class='cardmember_cont'><div class='cardmember_cont_upper'><h2>" + firstletter + "</h2><div class='member_id'><h5>" + username + "</h5><p>" + email + "</p>     </div><div class='clr'></div></div><a onclick='remove_members_from_board(" + cardid + ",\"" + email + "\"," + assignedto + ");'>Remove from Card</a></div></div></div>");
                var showmembeid = cardid;
                GetMembersInCard(showmembeid);
                AddMemeberToTask(showmembeid);
                UpdateActivityListItem(newMember, 15, showmembeid);
                // $('#Text1').val("");
            }
            else {
                alert("User already assigned for this task.");
            }
        }
    });
}

function subscribetouser(firstletter, email, assignedto, username) {
    var TextAreaVlue = $('#assignedTextArea').val();
    var boaridforcard = $('#BNameId').find("option:selected").val();
    var AddsprintId = $('#SNameId').find("option:selected").val();
    var ProjectID = $('#LNameId').find("option:selected").val();
    if (TextAreaVlue != "" && $('#BNameId').val() == null && $('#SNameId').val() == null && $('#LNameId').val() == null && $('#CNameId').val() == null) {
        var membersname = email;
        firstletter = firstletter.toUpperCase();
        $.ajax({
            type: "POST",
            url: "/DataTracker.svc/SaveNewTaskWithoutBoardName",
            data: "{\"Card_Name\": \"" + TextAreaVlue + "\"}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                var cardid = data.d;
                assignmembersToCardsOnClick(cardid, assignedto, firstletter, email, username);
            }
        });
    }
    else if (TextAreaVlue != "") {
        var membersname = email;
        firstletter = firstletter.toUpperCase();
        $.ajax({
            type: "POST",
            url: "/DataTracker.svc/SaveNewCardWithSprintId",
            data: "{\"Card_Name\": \"" + TextAreaVlue + "\",\"Project_ID\": \"" + ProjectID + "\",\"InSprintId\": \"" + AddsprintId + "\",\"boardid\": \"" + boaridforcard + "\"}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                var cardid = data.d;
                assignmembersToCardsOnClick(cardid, assignedto, firstletter, email, username);
            }
        });
    }
    else {
        var selected = [];
        var membersname = email;
        var cardid = $('#CNameId').find("option:selected").val();
        if (membersname.length > 0) {
            selected.push(cardid);
            if (selected.length > 0) {
                var arr2 = JSON.stringify({ cardid: selected });
            }
        }
        else {
            selected.push(cardid);
            var arr2 = JSON.stringify({ cardid: selected });
        }
        firstletter = firstletter.toUpperCase();
        $.ajax({
            type: "POST",
            url: "/DataTracker.svc/assigneduserfortask",
            data: "{\"assignedto\": \"" + assignedto + "\"," + arr2 + "}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                var newMember = "in " + cardNameOfComment;
                if (data.d != 0) {
                    SendNotificationMailForAddMemberOnCard(cardNameOfComment, cardid, email);
                    username = username != (null) ? username : '';
                    $("#showsubsuser").append("<div class='subuser-pop-cont'><a id=userfirstletter href='#' >" + firstletter + "</a><div class='subuser-pop-data'><div class='cardmember_cont'><div class='cardmember_cont_upper'><h2>" + firstletter + "</h2><div class='member_id'><h5>" + username + "</h5><p>" + email + "</p>     </div><div class='clr'></div></div><a onclick='remove_members_from_board(" + cardid + ",\"" + email + "\"," + assignedto + ");'>Remove from Card</a></div></div></div>");
                    for (var i = 0; i < selected.length; i++) {
                        var showmembeid = selected[i];
                        GetMembersInCard(showmembeid);
                        AddMemeberToTask(showmembeid);
                        UpdateActivityListItem(newMember, 15, showmembeid);
                    }
                    //$('#Text1').val("");
                }
                else {
                    alert("User already assigned for this task.");
                }
            }
        });
    }
}

function AddMemeberToTask(CardID) {
    var boardid = $('#BNameId').find("option:selected").val();
    var TeamID = 0;
    var ProjectID = $('#LNameId').find("option:selected").val();
    $.ajax({
        type: "POST",
        url: "/DataTracker.svc/AddMemberInCard",
        data: "{\"boardidd\": \"" + boardid + "\",\"listid\": \"" + ProjectID + "\",\"TaskID\": \"" + CardID + "\",\"TeamID\": \"" + TeamID + "\"}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
        },
        error: function (error) {
        }
    });
}

function UpdateActivityListItem(itemName, ActivityLogType, cardid) {
    var Ui_Id = 1;
    itemName = itemName.replace(/\n/g, "<br />");
    var cardidd = 0;
    if (cardid != null && cardid != "" && cardid != 'undefined') {
        cardidd = cardid;
    }
    var BoardId = $('#BNameId').find("option:selected").val();
    $.ajax({
        type: "POST",
        url: "/DataTracker.svc/AddActivityLog",
        data: "{\"Message\": \"" + itemName + "\",\"ActivityLogType\": \"" + ActivityLogType + "\",\"boardid\": \"" + BoardId + "\", \"cardid\": \"" + cardidd + "\"}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            if (ActivityLogType == 3) {
                sendmessage();
            }
            specificactivitylog(ActivityLogType);
        },
        error: function (error) {
        }
    });
}

function specificactivitylog(ActivityLogType) {
    var BoardId = $('#BNameId').find("option:selected").val();
    $.ajax({
        type: "POST",
        url: "/DataTracker.svc/specificactivitylogs",
        data: "{\"boardid\": \"" + BoardId + "\",\"ActivityLogType\": \"" + ActivityLogType + "\"}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            var errorstatus = data.d[0].errorstatus;
            if (errorstatus == 0) {
                var item = data.d[0];
                var taskid = data.d[0].taskid;
                var ActivityDate = data.d[0].ActivityDate.replace("-", "").replace("-", "");
                var ID = '#activitylist_' + ActivityDate;
                var dropdownid = "dropdown-" + ActivityDate;
                var UserId = data.d[0].UserId;
                var UserName = data.d[0].UserName;
                var filename = data.d[0].profileimage;
                var activitylogtypemessage = data.d[0].activitylogtypemessage;
                var activitylogmessage = data.d[0].activitylogmessage;
                var datetime = 'just now';
                var profileimage;
                if (filename.length > 0) {
                    profileimage = filename;
                }
                else {
                    profileimage = "DefaultImage.jpg";
                }

                var match = $("#Datecontainer").children().first().attr('id');
                if (ActivityLogType == 2 || ActivityLogType == 7 || ActivityLogType == 10 || ActivityLogType == 12 || ActivityLogType == 13 || ActivityLogType == 15 || ActivityLogType == 16 || ActivityLogType == 17) {
                    if (dropdownid == match) {
                        $("#Datecontainer > div > .dropdown-container1").css("display", "none");
                        $("#Datecontainer > div:first-child > .dropdown-container1").show();
                        $("#Datecontainer > div a").removeClass('minus').addClass('plus');
                        $("#Datecontainer > div:first-child a").removeClass('plus').addClass('minus');
                        $(ID).prepend("<li id = activity_" + item.Activitylogid + " onmouseenter='javascript:hoverinactivity(" + item.Activitylogid + "," + taskid + ")' onmouseleave='javascript:hoveroutactivity(" + item.Activitylogid + ")'><div class='activitysectionleft'><div class='activityimg'><img class=profileimage" + UserId + " src='userimage/" + profileimage + "' /></div><p> " + UserName + " </p></div><div class='activitysectionright'><div class='activitysectionright_cont'><h6>" + activitylogtypemessage + "</h6><p>" + activitylogmessage + "</p><div class='right_bottom_time'>" + datetime + "</div></div></div><div class='clr'></div></li>");
                        var totals = ID + ' ' + 'li';
                        var total = $(totals).length;
                        var countid = "#p-" + ActivityDate;
                        $(countid).html("(" + total + ")");
                    }
                    else {
                        $('#Datecontainer').prepend("<div id='dropdown-" + ActivityDate + "' class='dropdown dropdown-processed'><a onclick='javascript:ShowRecentActivityMsg(" + ActivityDate + " )' id='content" + ActivityDate + "' class='dropdown-link plus' href='#' >" + data.d[0].ActivityDate + "</a><p class='pboardno' id='p-" + ActivityDate + "' style='font-weight:bold; margin-left:4px; font-size:13px; display:inline;'></p><p class='icon-cntnr'><p title='No of new card created' id='t-" + ActivityDate + "' class='pboardno sqicn1' style='font-weight:bold; margin-left:4px; font-size:13px; display:inline;'><span></span></p><p title='No of Progress in task or list' id='c-" + ActivityDate + "' class='pboardno sqicn2' style='font-weight:bold; margin-left:4px; font-size:13px; display:inline;'></p><p title='Hours report daily' id='e-" + ActivityDate + "' class='pboardno sqicn3' style='font-weight:bold; margin-left:4px; font-size:13px; display:inline;'></i>0</p></p><div id='dropdowncontainer" + ActivityDate + "' class='dropdown-container1' style='display: none;'><ul id='activitylist_" + ActivityDate + "'></ul></div>");
                        $(ID).prepend("<li id = activity_" + item.Activitylogid + " onmouseenter='javascript:hoverinactivity(" + item.Activitylogid + "," + taskid + ")' onmouseleave='javascript:hoveroutactivity(" + item.Activitylogid + ")'><div class='activitysectionleft'><div class='activityimg'><img class=profileimage" + UserId + " src='userimage/" + profileimage + "' /></div><p> " + UserName + " </p></div><div class='activitysectionright'><div class='activitysectionright_cont'><h6>" + activitylogtypemessage + "</h6><p>" + activitylogmessage + "</p><div class='right_bottom_time'>" + datetime + "</div></div></div><div class='clr'></div></li>");
                        var totals = ID + ' ' + 'li';
                        var total = $(totals).length;
                        var countid = "#p-" + ActivityDate;
                        $(countid).html("(" + total + ")");
                        $("#Datecontainer > div:first-child a").removeClass('plus').addClass('minus');
                    }
                }
                else {
                    if (dropdownid == match) {
                        $("#Datecontainer > div:first-child a").removeClass('plus').addClass('minus');
                        $(ID).prepend("<li id = activity_" + item.Activitylogid + " onmouseenter='javascript:hoverinactivity(" + item.Activitylogid + "," + taskid + ")' onmouseleave='javascript:hoveroutactivity(" + item.Activitylogid + ")'><div class='activitysectionleft'><div class='activityimg'><img class=profileimage" + UserId + " src='userimage/" + profileimage + "' /></div><p> " + UserName + " </p></div><div class='activitysectionright'><div class='activitysectionright_cont'><h6>" + activitylogtypemessage + "</h6><p>" + activitylogmessage + "</p><div class='right_bottom_time'>" + datetime + "</div></div></div><div class='clr'></div></li>");
                        var totals = ID + ' ' + 'li';
                        var total = $(totals).length;
                        var countid = "#p-" + ActivityDate;
                        $(countid).html("(" + total + ")");
                    }
                    else {
                        $('#Datecontainer').prepend("<div id='dropdown-" + ActivityDate + "' class='dropdown dropdown-processed'><a onclick='javascript:ShowRecentActivityMsg(" + ActivityDate + " )' id='content" + ActivityDate + "' class='dropdown-link plus' href='#' >" + data.d[0].ActivityDate + "</a><p class='pboardno' id='p-" + ActivityDate + "' style='font-weight:bold; margin-left:4px; font-size:13px; display:inline;'></p><p class='icon-cntnr'><p title='No of new card created' id='t-" + ActivityDate + "' class='pboardno sqicn1' style='font-weight:bold; margin-left:4px; font-size:13px; display:inline;'><span></span></p><p title='No of Progress in task or list' id='c-" + ActivityDate + "' class='pboardno sqicn2' style='font-weight:bold; margin-left:4px; font-size:13px; display:inline;'></p><p title='Hours report daily' id='e-" + ActivityDate + "' class='pboardno sqicn3' style='font-weight:bold; margin-left:4px; font-size:13px; display:inline;'></i>0</p></p><div id='dropdowncontainer" + ActivityDate + "' class='dropdown-container1' style='display: none;'><ul id='activitylist_" + ActivityDate + "'></ul></div>");
                        $(ID).prepend("<li id = activity_" + item.Activitylogid + " onmouseenter='javascript:hoverinactivity(" + item.Activitylogid + "," + taskid + ")' onmouseleave='javascript:hoveroutactivity(" + item.Activitylogid + ")'><div class='activitysectionleft'><div class='activityimg'><img class=profileimage" + UserId + " src='userimage/" + profileimage + "'/></div><p> " + UserName + " </p></div><div class='activitysectionright'><div class='activitysectionright_cont'><h6>" + activitylogtypemessage + "</h6><p>" + activitylogmessage + "</p><div class='right_bottom_time'>" + datetime + "</div></div></div><div class='clr'></div></li>");
                        var totals = ID + ' ' + 'li';
                        var total = $(totals).length;
                        var countid = "#p-" + ActivityDate;
                        $(countid).html("(" + total + ")");
                        $("#Datecontainer > div:first-child a").removeClass('plus').addClass('minus');
                    }
                }
            }
            else {

            }

            if (ActivityLogType == 7) {
                var newcardpid = "#t-" + ActivityDate;
                var newcardpdata = $(newcardpid).text();
                if (newcardpdata != "") {
                    var newcardpcount = parseInt($(newcardpid).text().replace('(', '').replace(')', '')) + 1;
                    $(newcardpid).empty();
                    $(newcardpid).text("" + newcardpcount + "");
                }
                else {
                    $(newcardpid).text("" + 1 + "");
                }

            }
            if (ActivityLogType == 2 || ActivityLogType == 34) {
                var swapcardpid = "#c-" + ActivityDate;
                var swapcardpdata = $(swapcardpid).text();
                if (swapcardpdata != "") {
                    var swapcardpcount = parseInt($(swapcardpid).text().replace('(', '').replace(')', '')) + 1;
                    $(swapcardpid).empty();
                    $(swapcardpid).html("" + swapcardpcount + "");
                }
                else {
                    $(swapcardpid).text("" + 1 + "");
                }

            }

            var newcardpid = "#t-" + ActivityDate;
            var newcardpcount = $(newcardpid).text();

            if (newcardpcount == "") {
                $(newcardpid).text("" + 0 + "");
            }

            var swapcardpid = "#c-" + ActivityDate;
            var swapcardpdata = $(swapcardpid).text();
            if (swapcardpdata == "") {
                $(swapcardpid).text("" + 0 + "");
            }

        },
        error: function (data) {

        }
    });
}

function ReportsMenu(IsAds, UserTypeID) {
    // alert("ankur2");
    $("#ReportMenu").kendoMenu({
        dataSource: ReportMenuList(IsAds, UserTypeID),
        select: onReportSelect

    });
}
// Anaam Code
function ClientMenu(IsAds, UserTypeID) {
    $("#ClientMenu").kendoMenu({
        //dataSource: [{ text: "Client", url: "/Dashbord.aspx?2"}],
        dataSource: ClientMenuList(IsAds, UserTypeID),
        select: onReportSelect
    });
}

// Ravi Code Start
function AdsMenu(IsAds, UserTypeID) {
    $("#AdsMenu").kendoMenu({
        dataSource: [{ text: "Ads", url: "/Dashbord.aspx?1" }],
        // select: onReportSelect
    });
}
function WebscrapesMenu(IsAds, UserTypeID) {
    $("#WebscrapesMenu").kendoMenu({
        dataSource: [{ text: "Webscrapes", url: "/Dashbord.aspx?2" }],
        //select: onReportSelect
    });
}
function AADMenu(IsAds, UserTypeID) {
    $("#AADMenu").kendoMenu({
        dataSource: [{ text: "All Ads Detected", url: "/Dashbord.aspx?3" }],
        // select: onReportSelect
    });
}
function HRMMenu(IsAds, UserTypeID) {
    $("#HRMMenu").kendoMenu({
        //dataSource: [{ text: "Client", url: "/Dashbord.aspx?2"}],
        dataSource: HRMMenuList(IsAds, UserTypeID),
        //select: onReportSelect
    });
}
function AssignedTaskMenu(IsAds, UserTypeID) {
    $("#AssignedTaskMenu").kendoMenu({
        //dataSource: [{ text: "Client", url: "/Dashbord.aspx?2"}],
        dataSource: AssignedTaskMenuList(IsAds, UserTypeID),
        select: onReportSelect
    });
}
function HRMReport(IsAds, UserTypeID) {
    $("#HRMReport").kendoMenu({
        //dataSource: [{ text: "Client", url: "/Dashbord.aspx?2"}],
        dataSource: HRMReportList(IsAds, UserTypeID),
        //select: onReportSelect
    });
}



function IncidentReportingMenu(IsAds, UserTypeID) {
    if (UserTypeID != 16) {
        $("#IncidentReportingMenu").kendoMenu({
            dataSource: [{ text: "Lighthouse", url: "/HRM/AddIncidentGroupMaster.aspx" }],
            //select: onReportSelect
        });
    }
}
function CommonExpectationMenu(IsAds, UserTypeID) {
    if (UserTypeID != 16) {
        $("#CommonExpectationMenu").kendoMenu({
            dataSource: [{ text: "Manage Common Expectations", url: "/HRM/ExpectationMaster.aspx" }],
            //select: onReportSelect
        });
    }
}


// Ravi Code End

function ShowMenuList(AdsTabs, WebScrape) {



    var Data = "";

    if (AdsTabs == 0 && WebScrape == 1) {
        Data = [
                        {
                            text: "Actions", imageUrl: "",
                            items: [

                            //{ text: "Add work item", imageUrl: "/Images/Icon/NewIcons/addwork.png", width: "180px" },
                                { text: "Upload Schedule", url: "/UploadSheet.aspx", imageUrl: "/Images/Icon/NewIcons/upload-status.png", width: "180px" }


                            ]
                        }
        ]

    }
    else if (AdsTabs == 0 && WebScrape == 0) {
        $("#ActionsMenu").hide();
        // Data = [
        //   {
        // text: "Actions", imageUrl: "",
        // items: [
        //{ text: "Add work item", imageUrl: "/Images/Icon/NewIcons/addwork.png", width: "180px" }


        // ]
        //  }
        //  ]
        //
    }
    else {

        if (IsClient) {
            Data = [
                        {
                            text: "Actions", imageUrl: "",
                            items: [
                            // { text: "Add work item", imageUrl: "/Images/Icon/NewIcons/addwork.png", width: "180px" },
                                { text: "Mapping Address", url: "/MappingAddress.aspx", imageUrl: "/Images/Icon/NewIcons/mapping-address.png", width: "180px" },
                                { text: "Upload Schedule", url: "/UploadSheet.aspx", imageUrl: "/Images/Icon/NewIcons/upload-status.png", width: "180px" }
                            ]
                        }
            ]
        }
        else if (usertypeid == 12) {

            Data = [
                        {
                            text: "Actions", imageUrl: "",
                            items: [
                            //{ text: "Add work item", imageUrl: "/Images/Icon/NewIcons/addwork.png", width: "180px" },
                                 { text: "Mapping Address", url: "/MappingAddress.aspx", imageUrl: "/Images/Icon/NewIcons/mapping-address.png", width: "180px" },
                                 { text: "Upload Schedule", url: "/UploadSheet.aspx", imageUrl: "/Images/Icon/NewIcons/upload-status.png", width: "180px" },
                            { text: "Team/Project Management", imageUrl: "/Images/Icon/NewIcons/projectmgmt.jpg", width: "180px" }
                            ]
                        }
            ]
        }
        else if (IsDeveloper) {
            Data = [
                        {
                            text: "Actions", imageUrl: "",
                            items: [
                            //{ text: "Add work item", imageUrl: "/Images/Icon/NewIcons/addwork.png", width: "180px" },
                                 { text: "Mapping Address", url: "/MappingAddress.aspx", imageUrl: "/Images/Icon/NewIcons/mapping-address.png", width: "180px" },
                                 { text: "Upload Schedule", url: "/UploadSheet.aspx", imageUrl: "/Images/Icon/NewIcons/upload-status.png", width: "180px" }
                            ]
                        }
            ]

        }
        else {
            Data = [
                        {
                            text: "Actions", imageUrl: "",
                            items: [
                            //{ text: "Add work item", imageUrl: "/Images/Icon/NewIcons/addwork.png", width: "180px" },
                                { text: "Mapping Address", url: "/MappingAddress.aspx", imageUrl: "/Images/Icon/NewIcons/mapping-address.png", width: "180px" },
                                { text: "Upload Schedule", url: "/UploadSheet.aspx", imageUrl: "/Images/Icon/NewIcons/upload-status.png", width: "180px" }
                            ]
                        }
            ]
        }
    }
    return Data

}

//Anaam Code
function ClientMenuList(IsAds, UserTypeID) {
    var Data = "";
    if (UserTypeID == 12) {
        $("#ClientMenu").show();
        Data = [
                {
                    text: "Manager", imageUrl: "",
                    items: [
                             { text: "Add Client", imageUrl: "/Images/Icon/NewIcons/AddClient1.png", width: "180px" },
                             { text: "Clients", imageUrl: "/Images/Icon/NewIcons/ManageClient1.png", width: "180px" },
                             { text: "Add Account Manager", imageUrl: "/Images/Icon/NewIcons/AddAccount.png", width: "180px" },
                             { text: "Account Managers", imageUrl: "/Images/Icon/NewIcons/ManageAccount.png", width: "180px" },
                             //{ text: "Add Client", imageUrl: "/Images/Icon/NewIcons/AddClient.png", width: "180px" },
                             //{ text: "Manage Client", imageUrl: "/Images/Icon/NewIcons/ManageClient.png", width: "180px" },
                    ]
                }
        ]
    }
    else if (UserTypeID == 19) {
        $("#ClientMenu").show();
        Data = [
                {
                    text: "Manager", imageUrl: "",
                    items: [
                             { text: "Add Client", imageUrl: "/Images/Icon/NewIcons/AddClient1.png", width: "180px" },
                             { text: "Clients", imageUrl: "/Images/Icon/NewIcons/ManageClient1.png", width: "180px" },

                    ]
                }
        ]
    }
    else {
        $("#ClientMenu").hide();
    }
    return Data
}

function HRMMenuList(IsAds, UserTypeID) {
    var Data = "";

    //if (IsManager = 1 && ManagerID != "0") {

    $("#HRMMenu").show();
    Data = [
            {
                text: "HRM", imageUrl: "",
                items: [
                         { text: "HR", url: "/HRM/ViewAllEmployee.aspx", imageUrl: "/Images/Icon/NewIcons/AddClient1.png", width: "180px" },



                ]
            }
    ]
    // }


    return Data
}

function AssignedTaskMenuList(IsAds, UserTypeID) {
    var Data = "";
    $("#AssignedTaskMenu").show();
    Data = [
            {
                text: "Assigned Task", imageUrl: "",
                items: [
                         { text: "Assigned New Task", id: "ANTask", imageUrl: "/Images/Icon/NewIcons/AddClient1.png", width: "180px" },
                ]
            }
    ]
    return Data
}

function HRMReportList(IsAds, UserTypeID) {
    var Data = "";

    //if (IsManager = 1 && ManagerID != "0") {

    $("#HRMReport").show();
    Data = [
            {
                text: "Report", imageUrl: "",
                items: [
                         { text: "Department wise", url: "/HRM/Departmentwise.aspx", imageUrl: "/Images/Icon/NewIcons/AddClient1.png", width: "180px" },
                         { text: "Designation wise", url: "/HRM/Designationwise.aspx", imageUrl: "/Images/Icon/NewIcons/AddClient1.png", width: "180px" },
                         { text: "Duration wise", url: "/HRM/Durationwise.aspx", imageUrl: "/Images/Icon/NewIcons/AddClient1.png", width: "180px" },
                         { text: "Department cost wise", url: "/HRM/CostDepartmentWise.aspx", imageUrl: "/Images/Icon/NewIcons/AddClient1.png", width: "180px" },
                         { text: "Attrition wise", url: "/HRM/AttritionWise.aspx", imageUrl: "/Images/Icon/NewIcons/AddClient1.png", width: "180px" },
                ]
            }
    ]
    // }


    return Data
}

function ReportMenuList(IsAds, UserTypeID) {
    var Data = "";
    if (IsAds == 1) {

        if (IsClient) {
            //alert("2");
            Data = [
                        {
                            text: "Reports", imageUrl: "",
                            items: [
                                { text: "Monthly Jobs Summary", imageUrl: "/Images/Icon/NewIcons/summary icon.png", width: "180px" },
                                { text: "Download Reports", imageUrl: "/Images/Icon/NewIcons/download-report.png", width: "180px" },
                                { text: "Missing Ads", url: "/DataReport.aspx", imageUrl: "/Images/Icon/NewIcons/missing-ads.png", width: "180px" },
                                { text: "Compare Schedule", url: "/CompareSchedule.aspx", imageUrl: "/Images/Icon/NewIcons/compare-schedule.png", width: "180px" },
                                { text: "Analysis Script", url: "/ScriptanalysisReport.aspx", imageUrl: "/Images/Icon/NewIcons/compare-schedule.png", width: "180px" },


                            ]
                        }
            ]
        }
        else {
            //alert("3");
            if (ArrMaricoClients.indexOf(cuid) > -1) {
                Data = [
                        {
                            text: "Reports", imageUrl: "",
                            items: [
                                 { text: "Marico Price Data", url: "/MaricoPriceReport.aspx", imageUrl: "/Images/Icon/NewIcons/team-task-summary.png", width: "180px" }
                            ]
                        }
                ]
            }
            else if (UserTypeID == 12 || UserTypeID == 14) {
                //alert("4");
                if (UserTypeID == 12) {
                    // alert("5");
                    if (cuid == 13 || cuid == 10 || cuid == 455) {
                        //alert("7");
                        Data = [
                        {
                            text: "Reports", imageUrl: "",
                            items: [
                                { text: "Monthly Jobs Summary", imageUrl: "/Images/Icon/NewIcons/summary icon.png", width: "180px" },
                                { text: "Download Reports", imageUrl: "/Images/Icon/NewIcons/download-report.png", width: "180px" },
                                { text: "Missing Ads", url: "/DataReport.aspx", imageUrl: "/Images/Icon/NewIcons/missing-ads.png", width: "180px" },
                                { text: "Compare Schedule", url: "/CompareSchedule.aspx", imageUrl: "/Images/Icon/NewIcons/compare-schedule.png", width: "180px" },
                                { text: "Slipage Report", imageUrl: "/Images/Icon/NewIcons/slipage.png", width: "180px" },
                                { text: "Tasks Report", url: "/Chart.aspx", imageUrl: "/Images/Icon/NewIcons/taskreport.png", width: "180px" },
                                { text: "Team Tasks Summary", url: "/TaskTeam.aspx", imageUrl: "/Images/Icon/NewIcons/team-task-summary.png", width: "180px" },
                                { text: "Teams Time Report", url: "/DepartmentsTaskTime.aspx", imageUrl: "/Images/Icon/NewIcons/team-time-reports.png", width: "180px" },
                                { text: "Employee Time Sheet", url: "/EmployeeTimesheetReports.aspx", imageUrl: "/Images/Icon/NewIcons/emptimesheet.jpg", width: "180px" },
                                { text: "Marico Price Data", url: "/MaricoPriceReport.aspx", imageUrl: "/Images/Icon/NewIcons/team-task-summary.png", width: "180px" },
                                { text: "Analysis Script", url: "/ScriptanalysisReport.aspx", imageUrl: "/Images/Icon/NewIcons/compare-schedule.png", width: "180px" },
                               { text: "Mobile Apps Report", url: "/MobileAppsReport.aspx", imageUrl: "/Images/Icon/NewIcons/mobile.png", width: "180px" },
                               { text: "Board Report", url: "/BoardReport.aspx", imageUrl: "/Images/Icon/NewIcons/board.png", width: "180px" },
                               { text: "My Cards Report", url: "/MyCardsReport.aspx", imageUrl: "/Images/Icon/NewIcons/card.png", width: "180px" },
                               { text: "BoardWise TimeSheet Report", url: "/BoardWiseTeamReport.aspx", imageUrl: "/Images/Icon/NewIcons/card.png", width: "180px" },
                               { text: "BoardWise Cards count Report", url: "/BoardWiseCardsCount.aspx", imageUrl: "/Images/Icon/NewIcons/card.png", width: "180px" },
                               { text: "User Detail", url: "/UserDetails.aspx", imageUrl: "/Images/Icon/NewIcons/card.png", width: "180px" }
                            ]
                        }
                        ]
                    }
                    else {
                        // alert("8");
                        Data = [
                        {
                            text: "Reports", imageUrl: "",
                            items: [
                                { text: "Monthly Jobs Summary", imageUrl: "/Images/Icon/NewIcons/summary icon.png", width: "180px" },
                                { text: "Download Reports", imageUrl: "/Images/Icon/NewIcons/download-report.png", width: "180px" },
                                { text: "Missing Ads", url: "/DataReport.aspx", imageUrl: "/Images/Icon/NewIcons/missing-ads.png", width: "180px" },
                                { text: "Compare Schedule", url: "/CompareSchedule.aspx", imageUrl: "/Images/Icon/NewIcons/compare-schedule.png", width: "180px" },
                                { text: "Slipage Report", imageUrl: "/Images/Icon/NewIcons/slipage.png", width: "180px" },
                                { text: "Tasks Report", url: "/Chart.aspx", imageUrl: "/Images/Icon/NewIcons/taskreport.png", width: "180px" },
                                { text: "Team Tasks Summary", url: "/TaskTeam.aspx", imageUrl: "/Images/Icon/NewIcons/team-task-summary.png", width: "180px" },
                                { text: "Teams Time Report", url: "/DepartmentsTaskTime.aspx", imageUrl: "/Images/Icon/NewIcons/team-time-reports.png", width: "180px" },
                                { text: "Employee Time Sheet", url: "/EmployeeTimesheetReports.aspx", imageUrl: "/Images/Icon/NewIcons/emptimesheet.jpg", width: "180px" },
                                { text: "Marico Price Data", url: "/MaricoPriceReport.aspx", imageUrl: "/Images/Icon/NewIcons/team-task-summary.png", width: "180px" },
                                { text: "Analysis Script", url: "/ScriptanalysisReport.aspx", imageUrl: "/Images/Icon/NewIcons/compare-schedule.png", width: "180px" },
                                { text: "My Cards Report", url: "/MyCardsReport.aspx", imageUrl: "/Images/Icon/NewIcons/card.png", width: "180px" },
                                { text: "BoardWise TimeSheet Report", url: "/BoardWiseTeamReport.aspx", imageUrl: "/Images/Icon/NewIcons/card.png", width: "180px" },
                               { text: "BoardWise Cards count Report", url: "/BoardWiseCardsCount.aspx", imageUrl: "/Images/Icon/NewIcons/card.png", width: "180px" },
                               { text: "User Detail", url: "/UserDetails.aspx", imageUrl: "/Images/Icon/NewIcons/card.png", width: "180px" }

                            ]
                        }
                        ]

                    }

                }
                else {
                    //alert("9");
                    Data = [
                        {
                            text: "Reports", imageUrl: "",
                            items: [
                                { text: "Monthly Jobs Summary", imageUrl: "/Images/Icon/NewIcons/summary icon.png", width: "180px" },
                                { text: "Download Reports", imageUrl: "/Images/Icon/NewIcons/download-report.png", width: "180px" },
                                { text: "Missing Ads", url: "/DataReport.aspx", imageUrl: "/Images/Icon/NewIcons/missing-ads.png", width: "180px" },
                                { text: "Compare Schedule", url: "/CompareSchedule.aspx", imageUrl: "/Images/Icon/NewIcons/compare-schedule.png", width: "180px" },
                                { text: "Slipage Report", imageUrl: "/Images/Icon/NewIcons/slipage.png", width: "180px" },
                                { text: "Tasks Report", url: "/Chart.aspx", imageUrl: "/Images/Icon/NewIcons/taskreport.png", width: "180px" },
                                { text: "Team Tasks Summary", url: "/TaskTeam.aspx", imageUrl: "/Images/Icon/NewIcons/team-task-summary.png", width: "180px" },
                                { text: "Teams Time Report", url: "/DepartmentsTaskTime.aspx", imageUrl: "/Images/Icon/NewIcons/team-time-reports.png", width: "180px" },
                                { text: "Employee Time Sheet", url: "/EmployeeTimesheetReports.aspx", imageUrl: "/Images/Icon/NewIcons/emptimesheet.jpg", width: "180px" },
                                { text: "Marico Price Data", url: "/MaricoPriceReport.aspx", imageUrl: "/Images/Icon/NewIcons/team-task-summary.png", width: "180px" },
                                { text: "Analysis Script", url: "/ScriptanalysisReport.aspx", imageUrl: "/Images/Icon/NewIcons/compare-schedule.png", width: "180px" },
                                 { text: "My Cards Report", url: "/MyCardsReport.aspx", imageUrl: "/Images/Icon/NewIcons/card.png", width: "180px" },
                                 { text: "BoardWise Cards count Report", url: "/BoardWiseCardsCount.aspx", imageUrl: "/Images/Icon/NewIcons/card.png", width: "180px" },
                                { text: "Working Hours By Card", url: "/WorkingHoursByCard.aspx", imageUrl: "/Images/Icon/NewIcons/card.png", width: "180px" }

                            ]
                        }
                    ]
                }
            }
            else {
                //  alert("10");
                Data = [
                       {
                           text: "Reports", imageUrl: "",
                           items: [
                                { text: "Monthly Jobs Summary", imageUrl: "/Images/Icon/NewIcons/summary icon.png", width: "180px" },
                                { text: "Download Reports", imageUrl: "/Images/Icon/NewIcons/download-report.png", width: "180px" },
                               { text: "Missing Ads", url: "/DataReport.aspx", imageUrl: "/Images/Icon/NewIcons/missing-ads.png", width: "180px" },
                               { text: "Compare Schedule", url: "/CompareSchedule.aspx", imageUrl: "/Images/Icon/NewIcons/compare-schedule.png", width: "180px" },
                               { text: "Slipage Report", imageUrl: "/Images/Icon/NewIcons/slipage.png", width: "180px" },
                               { text: "Tasks Report", url: "/Chart.aspx", imageUrl: "/Images/Icon/NewIcons/taskreport.png", width: "180px" }, { text: "Employee Time Sheet", url: "/EmployeeTimesheetReports.aspx", imageUrl: "/Images/Icon/NewIcons/team-time-reports.png", width: "180px" },
                               { text: "Analysis Script", url: "/ScriptanalysisReport.aspx", imageUrl: "/Images/Icon/NewIcons/compare-schedule.png", width: "180px" },
                               { text: "Mobile Apps Report", url: "/MobileAppsReport.aspx", imageUrl: "/Images/Icon/NewIcons/mobile.png", width: "180px" },
                                { text: "My Cards Report", url: "/MyCardsReport.aspx", imageUrl: "/Images/Icon/NewIcons/card.png", width: "180px" },
                           ]
                       }
                ]


            }

        }

    }
    else {
        // alert("11");
        if (IsClient) {
            Data = [
                        {
                            text: "Reports", imageUrl: "",
                            items: [
                                { text: "Monthly Jobs Summary", imageUrl: "/Images/Icon/NewIcons/summary icon.png", width: "180px" },
                                { text: "Download Reports", imageUrl: "/Images/Icon/NewIcons/download-report.png", width: "180px" },
                                { text: "Missing Ads", url: "/DataReport.aspx", imageUrl: "/Images/Icon/NewIcons/missing-ads.png", width: "180px" },
                                { text: "Compare Schedule", url: "/CompareSchedule.aspx", imageUrl: "/Images/Icon/NewIcons/compare-schedule.png", width: "180px" },
                                { text: "Analysis Script", url: "/ScriptanalysisReport.aspx", imageUrl: "/Images/Icon/NewIcons/compare-schedule.png", width: "180px" }


                            ]
                        }
            ]
        }
        else {
            //   alert("12");
            //if (ArrMaricoClients.indexOf(cuid) > -1) {
            //    Data = [
            //            {
            //                text: "Reports", imageUrl: "",
            //                items: [
            //                     { text: "Marico Price Data", url: "/MaricoPriceReport.aspx", imageUrl: "/Images/Icon/NewIcons/team-task-summary.png", width: "180px" }


            //                ]
            //            }
            //            ]


            //}

            // else if (UserTypeID == 12 || UserTypeID == 14) {

            if (UserTypeID == 12 || UserTypeID == 14) {

                //alert("13");
                Data = [
                        {
                            text: "Reports", imageUrl: "",
                            items: [
                              { text: "Slipage Report", imageUrl: "/Images/Icon/NewIcons/slipage.png", width: "180px" },
                              { text: "Tasks Report", url: "/Chart.aspx", imageUrl: "/Images/Icon/NewIcons/taskreport.png", width: "180px" },
                             { text: "Team task summary", url: "/TaskTeam.aspx", imageUrl: "/Images/Icon/NewIcons/team-task-summary.png", width: "180px" },
                             { text: "Teams Time Report", url: "/DepartmentsTaskTime.aspx", imageUrl: "/Images/Icon/NewIcons/team-time-reports.png", width: "180px" },
                             { text: "Employee Time Sheet", url: "/EmployeeTimesheetReports.aspx", imageUrl: "/Images/Icon/NewIcons/emptimesheet.jpg", width: "180px" },
                             { text: "Analysis Script", url: "/ScriptanalysisReport.aspx", imageUrl: "/Images/Icon/NewIcons/compare-schedule.png", width: "180px" },
                            { text: "Board Report", url: "/BoardReport.aspx", imageUrl: "/Images/Icon/NewIcons/board.png", width: "180px" },
                             { text: "My Cards Report", url: "/MyCardsReport.aspx", imageUrl: "/Images/Icon/NewIcons/card.png", width: "180px" },
                              { text: "BoardWise TimeSheet Report", url: "/BoardWiseTeamReport.aspx", imageUrl: "/Images/Icon/NewIcons/card.png", width: "180px" },
                                { text: "Working Hours By Card", url: "/WorkingHoursByCard.aspx", imageUrl: "/Images/Icon/NewIcons/card.png", width: "180px" }
                            ]
                        }
                ]

            }
            else {
                //   alert("14");
                if (ArrRestaurentReportViewer.indexOf(cuid) > -1) {
                    Data = [
                       {
                           text: "Reports", imageUrl: "",
                           items: [
                             { text: "Slipage Report", imageUrl: "/Images/Icon/NewIcons/slipage.png", width: "180px" },
                            { text: "Tasks Report", url: "/Chart.aspx", imageUrl: "/Images/Icon/NewIcons/taskreport.png", width: "180px" }, { text: "Employee Time Sheet", url: "/EmployeeTimesheetReports.aspx", imageUrl: "/Images/Icon/NewIcons/team-time-reports.png", width: "180px" },
                            { text: "Analysis Script", url: "/ScriptanalysisReport.aspx", imageUrl: "/Images/Icon/NewIcons/compare-schedule.png", width: "180px" },
                             { text: "Mobile Apps Report", url: "/MobileAppsReport.aspx", imageUrl: "/Images/Icon/NewIcons/mobile.png", width: "180px" }

                           ]
                       }
                    ]
                }
                else {
                    Data = [
                       {
                           text: "Reports", imageUrl: "",
                           items: [
                             { text: "Slipage Report", imageUrl: "/Images/Icon/NewIcons/slipage.png", width: "180px" },
                            { text: "Tasks Report", url: "/Chart.aspx", imageUrl: "/Images/Icon/NewIcons/taskreport.png", width: "180px" }, { text: "Employee Time Sheet", url: "/EmployeeTimesheetReports.aspx", imageUrl: "/Images/Icon/NewIcons/team-time-reports.png", width: "180px" },
                            { text: "Analysis Script", url: "/ScriptanalysisReport.aspx", imageUrl: "/Images/Icon/NewIcons/compare-schedule.png", width: "180px" },
                            { text: "Board Report", url: "/BoardReport.aspx", imageUrl: "/Images/Icon/NewIcons/board.png", width: "180px" },
                             { text: "My Cards Report", url: "/MyCardsReport.aspx", imageUrl: "/Images/Icon/NewIcons/card.png", width: "180px" },
                              //{ text: "BoardWise TimeSheet Report", url: "/BoardWiseTeamReport.aspx", imageUrl: "/Images/Icon/NewIcons/card.png", width: "180px" }
                               //{ text: "BoardWise Cards count Report", url: "/BoardWiseCardsCount.aspx", imageUrl: "/Images/Icon/NewIcons/card.png", width: "180px" }
                           ]
                       }
                    ]

                }
            }
        }
    }

    return Data

}

function onReportSelect(e) {

    if ($(e.item).children(".k-link").text() == "Address not match") {
        window.location.href = "/DataReport.aspx";
    }
    else if ($(e.item).children(".k-link").text() == "Download schedule") {
        window.location.href = "/CompareSchedule.aspx";
    }
    else if ($(e.item).children(".k-link").text() == "Monthly Jobs Summary") {
        window.location.href = "/JobReport.aspx";
    }
    else if ($(e.item).children(".k-link").text() == "Change Password") {
        ChangePassPopUp();
    }
    else if ($(e.item).children(".k-link").text() == "Logout") {
        deleteAllCookiesAtLogOut();
        window.location.href = "/Dashbord.aspx?Logout=1";
    }
    else if ($(e.item).children(".k-link").text() == "Add Task") {
        AddSchedule('0', 0);
    }
    else if ($(e.item).children(".k-link").text() == "Add work item") {
        AddOtherTask('0', 0);
    }

    else if ($(e.item).children(".k-link").text() == "Download Reports") {
        MonthlyOfficeCycle("0");
    }
    else if ($(e.item).children(".k-link").text() == "Slipage Report") {
        GetSlipageReport(cuid, usertypeid);
    }
    else if ($(e.item).children(".k-link").text() == "Team/Project Management") {
        Openteammanage();
    }
    else if ($(e.item).children(".k-link").text() == "Email Preference") {
        EmailPreferencePopUp();
    }
    else if ($(e.item).children(".k-link").text() == "Add Client") {
        AddClientPopUp();
    }
    else if ($(e.item).children(".k-link").text() == "Assigned New Task") {
        $(".BNameClass").css({ "max-height": "300px", "margin-bottom": "14px", "width": "100%" });
        $("#BId").show(); $("#SId").show(); $("#LId").show(); $("#CId").show();
        $("#TId").hide();
        $("#webuiPopover23").css("top", "310px");
        $('#OnManualClick').css("background", "darkgray");
        $('#OnAssignedClick').css("background", "black");
        $('#taskbarid').css("background-color", "darkgray");
        AssignedTaskPopUp();
        GetBoardData();
    }
    else if ($(e.item).children(".k-link").text() == "Clients") {
        window.location.href = "/ClientDetails.aspx";
    }
    else if ($(e.item).children(".k-link").text() == "Add Account Manager") {
        AddAccountPopUp();
    }
    else if ($(e.item).children(".k-link").text() == "Account Managers") {
        window.location.href = "/AccountDetails.aspx";
        //window.location.href = "/ClientDetails.aspx";
    }

}


function GetBoardData() {
    $.ajax({
        type: "POST",
        url: "/DataTracker.svc/GetBoardsDataOnClick",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            $(".BNameClass").css({ "max-height": "300px", "margin-bottom": "14px", "width": "100%" });
            $("#BId").show(); $("#SId").show(); $("#LId").show(); $("#CId").show();
            $("#TId").hide();
            $("#webuiPopover23").css("top", "310px");
            $('#OnManualClick').css("background", "darkgray");
            $('#OnAssignedClick').css("background", "black");
            $('#taskbarid').css("background-color", "darkgray");

            var response = JSON.parse(data.d);
            $("#BNameId").append("<option value='0'>Select Boards Name</option>");
            $.each(response, function (index, item) {
                $("#BNameId").append($("<option></option>").val(item.BoardID).html(item.BoardName));
            });
        }
    });
}

function AssignedTaskPopUp() {
    var windowElement = $("#AssignedTaskToMembers").kendoWindow({
        actions: {},
        content: "/AssignedTaskToMembers.aspx",
        modal: true,
        //width: "500px",
        //height: "400px",
        title: "Assigned Task",
        position: {
            top: 10,
            left: 2
        },
        resizable: false,
        actions: ["Close"],
        draggable: false,

    });
    var dialog = $("#AssignedTaskToMembers").data("kendoWindow");
    dialog.center();
    dialog.open();
    // return false;
}



//function ShowRegularGrid(FromDate, ToDate, SearchStoreName, Jobnumber, Description, StDate, TrDate, Stname, ActID, ComboStatus, ComboMonth, Ismonth) {//
//    // regular grid    //
//    StatusIds = ComboStatus;
//    // alert(ComboStatus);
//    if (ComboMonth != null) {
//        GetRegularSchedule(FromDate, ToDate, SearchStoreName, Jobnumber, Description, StDate, TrDate, Stname, ActID, ComboMonth, StatusIds);
//    }
//    else {
//        GetRegularSchedule(FromDate, ToDate, SearchStoreName, Jobnumber, Description, StDate, TrDate, Stname, ActID, "", StatusIds);
//    }
//}

function ShowRegularGrid(FromDate, ToDate, SearchStoreName, DescriptionName, Jobnumber, Description, StDate, TrDate, Stname, ActID, ComboStatus, ComboMonth, Ismonth) {//
    // regular grid    //
    StatusIds = ComboStatus;
    // alert(ComboStatus);
    if (ComboMonth != null) {
        GetRegularSchedule(FromDate, ToDate, SearchStoreName, DescriptionName, Jobnumber, Description, StDate, TrDate, Stname, ActID, ComboMonth, StatusIds);
    }
    else {
        GetRegularSchedule(FromDate, ToDate, SearchStoreName, DescriptionName, Jobnumber, Description, StDate, TrDate, Stname, ActID, "", StatusIds);
    }
}

function ShowLastUpdatedTime() {
    for (var i = 0; i < $(".timeago").length; i++) {
        if (($.timeago($(".timeago")[i]).indexOf("NaN")) == "-1") {
            var stime = $(".timeago")[i].title;
            var StID = $(".timeago")[i].id;
            var mindiff = GetMindiffrence(stime);
            if (mindiff > 40 && (ComValue != StID && Cacelvaue != StID && Onhold != StID && CancelPdf != StID && scnotready != StID && Noaddeted != StID && AddressnotMatch != StID)) {
                $(".timeago")[i].innerHTML = "<span style='color:red'>" + $.timeago($(".timeago")[i]) + "</span>";
            }
            else {
                $(".timeago")[i].innerHTML = $.timeago($(".timeago")[i]);

            }

        }

    }

}

function GetMindiffrence(CreatedTime) {
    var TS = CreatedTime.replace(':', '-').replace(':', '-');
    var TS = TS.split('-');
    var ts2 = TS[2].split(' ');
    var months = TS[1];
    // var month = months < 10 ? '0' + months : months;
    var days = ts2[0];
    //var day = (days < 10) ? '0' + days : days;
    var hours = ts2[1]
    // var hr = hours < 10 ? '0' + hours : hours;
    var minutes = TS[3]; //date.getMinutes();
    //var min = (minutes < 10) ? '0' + minutes : minutes;
    var seconds = TS[4];
    // var sec = (seconds < 10) ? '0' + seconds : seconds; 
    var UpdateDate = new Date(TS[0], months, days, hours, minutes, seconds);
    var test = months + "/" + days + "/" + TS[0] + " " + hours + ": " + minutes + ": " + seconds;
    //var tt=CreatedTime.replace('-','/').repalce('-','/').replace('-','/');
    var date1 = new Date(test);
    var date2 = new Date();
    var timeDiff = date2.getTime() - date1.getTime();
    var Tmins = DateMeasure(timeDiff);
    return Tmins;

}

function DateMeasure(ms) {
    var d, h, m, s;
    s = Math.floor(ms / 1000);
    m = Math.floor(s / 60);
    s = s % 60;
    h = Math.floor(m / 60);
    m = m % 60;
    d = Math.floor(h / 24);
    h = h % 24;
    var totalmin = h * 60 + m;
    return totalmin;

}

function HideColumnsForClient(GridName) {
    if (IsClient) {
        var grid = $(GridName).data("kendoGrid");
        if (grid != null) {
            grid.columns[0].width = "10.5%";
            grid.columns[1].width = "21%";
            grid.columns[11].width = "14%";
            grid.columns[13].width = "18%";
            grid.hideColumn(6);
            grid.hideColumn(7);
            grid.hideColumn(12);
            grid.hideColumn(14);
            grid.hideColumn(15);


        }
    }
}

function HideColumnsStartForClient(GridName) {
    if (IsClient) {
        var grid = $(GridName).data("kendoGrid");
        if (grid != null) {
            grid.columns[0].width = "39%";
            grid.columns[7].width = "30%";
            grid.hideColumn(6);
            grid.hideColumn(8);
            grid.hideColumn(9);
            grid.hideColumn(2);
            grid.hideColumn(3);
            grid.hideColumn(4);

        }
    }
}

function HideColumnsDueForClient(GridName) {
    if (IsClient) {
        var grid = $(GridName).data("kendoGrid");
        if (grid != null) {
            grid.columns[0].width = "39%";
            grid.columns[6].width = "30%";
            grid.columns[8].width = "30%"
            grid.columns[1].width = "13%";
            grid.hideColumn(2);
            grid.hideColumn(3);
            grid.hideColumn(4);
            grid.hideColumn(5);

            grid.hideColumn(7);
            grid.hideColumn(9);
            grid.hideColumn(10);

        }
    }
}

function SetDownloadCount(GroupName, StatusID, Sdate) {
    if (StatusID == "11") {
        tat = "";
        var tat = GetDownlodCount(GroupName, Sdate);
        return "<a   title='" + tat + "'   >" + tat + "</a>";
    }
    else {
        return "";
    }

}
// complete img
function SetCompletedImg(StatusName, Stmsg) {
    //ShowLastUpdatedTime(); 
    if (StatusName == "QA Returned") {
        var tootltip = StatusName;
        if (Stmsg != "")
            tootltip = Stmsg;
        else
            tootltip = StatusName;
        if (!IsClient) {
            return "<a style='cursor:pointer;' title='" + tootltip + "'   >" + StatusName + "</a>&nbsp;<img class='tooltip-block' title='" + tootltip + "'   src='/Images/Icon/Qareturn.png' />";
        }
        else {
            return "<a >" + StatusName + "</a>";
        }
    }
    else
        return StatusName;
}
//all child grid
function StartdetailInit(e) {

    $("<div/>").appendTo(e.detailCell).kendoGrid({
        dataSource: {
            transport: {
                read: {
                    url: "/DataTracker.svc/GetSdateHierarchyByStoreName",
                    data: {

                        StoreName: escape(e.data.StoreName.split('(')[0].trim()),
                        StartDate: RetrunCurrentDate()

                    }
                }
            },
            serverPaging: true,
            serverSorting: true,
            serverFiltering: true,
            pageSize: 5,
            schema: {
                data: "d",
                total: function (d) {
                    if (d.d.length > 0)
                        return d.d[0].TotalCount
                },
                model: {
                    id: "ID",
                    fields: {
                        ID: { editable: false, type: "number" },
                        Job_Number: { editable: false, type: "number" },
                        User: { editable: false, type: "string" },
                        StatusName: { editable: true, type: "string" }

                    }
                }

            }
        },

        pageable: true,
        dataBound: function () {
            // this.expandRow(this.tbody.find("tr.k-master-row").first());
        },
        columns: [
                            { field: "ID", width: "0%" },
                             { field: "Job_Number", title: "Job number", width: "16%" },
                            { field: "User", title: "Last updated by", width: "30%" },
                             { field: "StatusName", title: "Current Status", width: "35%", editor: ChildDropDownEditorhire, template: "#=StatusName#" },
                            { command: { text: "Edit", click: EditDetails }, title: "", width: "20%" }
        ],
        editable: !IsClient,
        selectable: true

    });
}
// Due Date
function DuedetailInit(e) {
    $("<div/>").appendTo(e.detailCell).kendoGrid({
        dataSource: {
            transport: {
                read: {
                    //using jsfiddle echo service to simulate JSON endpoint
                    url: "/DataTracker.svc/GetDueHierarchyByStoreName",
                    data: {

                        StoreName: escape(e.data.StoreName.split('(')[0].trim()),
                        DueDate: RetrunCurrentDate()

                    }
                }
            },
            serverPaging: true,
            serverSorting: true,
            serverFiltering: true,
            pageSize: 5,
            schema: {
                data: "d",
                total: function (d) {
                    if (d.d.length > 0)
                        return d.d[0].TotalCount
                },
                model: {
                    id: "ID",
                    fields: {
                        ID: { editable: false, type: "number" },
                        Job_Number: { editable: false, type: "number" },
                        User: { editable: false, type: "string" },
                        StatusName: { editable: true, type: "string" }
                    }
                }

            }

        },
        pageable: true,
        dataBound: function () {
            // this.expandRow(this.tbody.find("tr.k-master-row").first());
        },
        columns: [
                            { field: "ID", width: "0%" },
                             { field: "Job_Number", title: "Job number", width: "16%" },
                            { field: "User", title: "Last updated by", width: "30%" },
                             { field: "StatusName", title: "Current Status", width: "35%", editor: ChildDropDownEditorhire, template: "#=StatusName#" },
                            { command: { text: "Edit", click: EditDetails }, title: "", width: "20%" }
        ],
        editable: !IsClient,
        selectable: true

    });
}
//
function InProdetailInit(e) {
    $("<div/>").appendTo(e.detailCell).kendoGrid({
        dataSource: {
            transport: {
                read: {
                    url: "/DataTracker.svc/GetInProHierarchyByStoreName",
                    data: {

                        StoreName: escape(e.data.StoreName.split('(')[0].trim())


                    }
                }
            },
            serverPaging: true,
            serverSorting: true,
            serverFiltering: true,
            pageSize: 5,
            schema: {
                data: "d",
                total: function (d) {
                    if (d.d.length > 0)
                        return d.d[0].TotalCount
                },
                model: {
                    id: "ID",
                    fields: {
                        ID: { editable: false, type: "number" },
                        Job_Number: { editable: false, type: "number" },
                        User: { editable: false, type: "string" },
                        StatusName: { editable: true, type: "string" }
                    }
                }

            }

        },
        pageable: true,
        dataBound: function () {
            // this.expandRow(this.tbody.find("tr.k-master-row").first());
        },
        columns: [
                            { field: "ID", width: "0%" },
                            { field: "Job_Number", title: "Job number", width: "16%" },
                            { field: "User", title: "Last updated by", width: "30%" },
                             { field: "StatusName", title: "Current Status", width: "35%", editor: ChildDropDownEditorhire, template: "#=StatusName#" },
                            { command: { text: "Edit", click: EditDetails }, title: "", width: "20%" }
        ],
        editable: !IsClient,
        selectable: true

    });
}
//dropdown editor
function SDateParentDropDownEditor(container, options) {
    if (options.model.StatusName != 'Completed') {
        SdateParentComboList(options.field, container);
    }
    else {
        SdateParentComboList(options.field, container);

    }
}

function SdateParentComboList(optionsfield, container) {
    $('<input ID="Stdropparent" required data-text-field="StatusName" data-value-field="StatusName" data-bind="value:' + optionsfield + '"/>')
                        .appendTo(container)
                        .kendoDropDownList({
                            autoBind: false,
                            dataSource: {
                                transport: {
                                    read: {
                                        url: "/DataTracker.svc/GetStatusList"
                                    }
                                },
                                schema: {
                                    data: "d"
                                }
                            },
                            change: SDateParentDroponChange

                        });
}

function DueParentDropDownEditor(container, options) {

    if (options.model.StatusName != 'Completed') {

        dueParentComboList(options.field, container)
    }
    else {
        dueParentComboList(options.field, container)

    }

}

function dueParentComboList(optionsfield, container) {
    $('<input ID="Duedropparent" required data-text-field="StatusName" data-value-field="StatusName" data-bind="value:' + optionsfield + '"/>')
                        .appendTo(container)
                        .kendoDropDownList({
                            autoBind: false,
                            dataSource: {
                                transport: {
                                    read: {
                                        url: "/DataTracker.svc/GetStatusList"
                                    }
                                },
                                schema: {
                                    data: "d"
                                }
                            },
                            change: DueParentDroponChange

                        });

}

function RegularParentDropDownEditor(container, options) {
    childSID = options.model.ID;
    if (options.model.StatusName != 'Completed') {

        RegularComboList(options.field, container)
    }
    else {
        RegularComboList(options.field, container)

    }

}

function RegularComboList(optionsfield, container) {
    $('<input ID="Inpdropparent" required data-text-field="StatusName" data-value-field="StatusName" data-bind="value:' + optionsfield + '"/>')
                        .appendTo(container)
                        .kendoDropDownList({
                            autoBind: false,
                            dataSource: {
                                transport: {
                                    read: {
                                        url: "/DataTracker.svc/GetRegularStatusList"
                                    }
                                },
                                schema: {
                                    data: "d"
                                }
                            },
                            change: RegularParentDroponChange

                        });
}

// child drop
function ChildDropDownEditorhire(container, options) {
    childSID = options.model.ID;
    $('<input ID="Stdrop" required data-text-field="StatusName" data-value-field="StatusName" data-bind="value:' + options.field + '"/>')
                        .appendTo(container)
                        .kendoDropDownList({
                            autoBind: false,
                            dataSource: {
                                transport: {
                                    read: {
                                        url: "/DataTracker.svc/GetStatusList"
                                    }
                                },
                                schema: {
                                    data: "d"
                                }
                            },
                            change: onChangeChild

                        });
}

function SDateParentDroponChange() {
    var Statusvalue = $("#Stdropparent").val();
    var grid = $("#StartGrid").data("kendoGrid");
    var selectedrow = $("#StartGrid").find("tbody tr.k-state-selected");
    var goods = $('#StartGrid').data("kendoGrid").dataItem(selectedrow);
    var goodsjson = goods.toJSON();
    var StoreName = goodsjson.StoreName;
    var startDate = goodsjson.StartDate;
    var Confrm = "";
    if (Statusvalue == "Completed") {
        Confrm = confirm("Are you sure its completed?");
    }
    else if (Statusvalue == "Change Detected") {
        Confrm = confirm("Are you sure change detected?");
    }

    else if (Statusvalue == "Analysis")
    { Confrm = confirm("Are you sure its analysis?"); }
    else if (Statusvalue == "Primary Completed")
    { Confrm = confirm("Are you sure its primary completed?"); }
    if (Statusvalue == "Change Detected" || Statusvalue == "Completed" || Statusvalue == "Analysis" || Statusvalue == "Primary Completed") {
        if (Confrm) {
            ChangeBulkstatusByStoreName(startDate, StoreName, Statusvalue, "", "", "1");
        }
        else {
            return false;

        }
    }
    else {

        ChangeBulkstatusByStoreName(startDate, StoreName, Statusvalue, "", "", "1");

    }


}

function RegularParentDroponChange() {
    debugger;
    var Statusvalue = $("#Inpdropparent").val();
    var grid = $("#RegularTaskGrid").data("kendoGrid");
    var selectedrow = $("#RegularTaskGrid").find("tbody tr.k-state-selected");
    var goods = $('#RegularTaskGrid').data("kendoGrid").dataItem(selectedrow);
    var goodsjson = goods.toJSON();
    var StoreName = goodsjson.StoreName;
    var SID = goodsjson.StatusID;
    var StartDate = goodsjson.DbStartDate;
    //  DbStartDate
    var Confrm = "";
    if (Statusvalue == "Completed") {
        Confrm = confirm("Are you sure its completed?");
    }
    else if (Statusvalue == "Change Detected") {
        Confrm = confirm("Are you sure change detected?");

    }
    else if (Statusvalue == "Analysis")
    { Confrm = confirm("Are you sure its analysis?"); }
    else if (Statusvalue == "Primary Completed")
    { Confrm = confirm("Are you sure its primary completed?"); }

    if (Statusvalue == "QA Returned") {
        OpenStatusComment(StartDate, StoreName, Statusvalue, StartDate, SID, "2", childSID)
        //     ChangeBulkstatusByStoreName(startDate, StoreName, value, DueDate, StatusId,"1");

    }
    else {

        if (Statusvalue == "Change Detected" || Statusvalue == "Completed" || Statusvalue == "Analysis") {
            if (Confrm) {

                ChangeRegularStatus(childSID, Statusvalue);
            }
            else {
                return false;
            }
        }
        else {
            ChangeRegularStatus(childSID, Statusvalue);
        }
    }


}
// anupam
function onChangeChild() {
    var value = $("#Stdrop").val();
    var text = $("#Stdrop").text();
    var tt = childSID;
    CallchangeStatus(childSID, value);

}
//StartDateGrid
function EditDetails(e) {
    e.preventDefault();
    if ($("#DashManageSche").data("kendoWindow") != null) {
        var dialogtest = $("#DashManageSche").data("kendoWindow");
        dialogtest.close();
    }
    var dataItem = this.dataItem($(e.currentTarget).closest("tr"));
    if (dataItem != null) {
        var Sid = dataItem.ID;
        $.ajax({
            type: "GET",
            url: "/DataTracker.svc/CkeckTaskType?SceduleId=" + Sid + "",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                alert("hj");
                var res = data.d;
                if (res != null) {
                    if (res == "s") {
                        AddManageSchedule(Sid);
                    }
                    else if (res == "o") {
                        AddOtherTask(Sid, 0)
                    }

                }
                else {
                    return false;
                }
            }
        });

    }

}
// anupam
function CallchangeStatus(ID, Statusval) {
    debugger;

    $.ajax({
        type: "GET",
        url: "/DataTracker.svc/Changestatus?SceduleId=" + ID + "&StatusID=" + Statusval + "",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            var res = data.d;
            if (res != null) {
                if (res == "1") {
                    SavedMessagePopUp("1")
                }
                else if (res == "-2") {
                    window.location.href = "/Login.aspx";

                }
                else if (res == "-1") {
                    SavedMessagePopUp("0")

                }
            }
            else {
                SavedMessagePopUp("0");
                return false;
            }
        }
    });
}

function GetDownlodCount(GroupName, Sdate) {
    var ResCount = "0";
    $.ajax({
        type: "GET",
        url: "/DataTracker.svc/GetDownloadStatusCount?GroupName=" + escape(GroupName) + "&SDate=" + Sdate + "",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            if (data.d != null) {
                ResCount = "0";
                ResCount = data.d;

            }
        },
        Error: function (data) {


        }
    });
    return ResCount;

}

function ViewAllDetail(ViewType) {
    if (ViewType == "d") {
        window.location.href = '/Details.aspx?d=1';

    }
    else if (ViewType == "s") {
        window.location.href = '/Details.aspx?s=1';
    }
    else if (ViewType == "i") {
        window.location.href = '/Details.aspx?i=1';

    }
    else if (ViewType == "o") {
        window.location.href = '/Details.aspx?o=1';

    }

}

function OnClientDropDownClosed(sender, args) {
    if (args.get_domEvent().stopPropagation)
        args.get_domEvent().stopPropagation();
}
function removeLastComma(str) {
    return str.replace(/,(?=[^,]*$)/, '');
}
function FilterByStatus(id, InProgress, ComboStoreName, ComboStatus, ComboStratDate, ComboDueDate) {

    if (ComboStatus != null) {
        for (var i = 0; i < ComboStatus.get_items().get_count() ; i++) {
            if (ComboStatus.get_items().getItem(i).get_value() == InProgress) {
                ComboStatus.get_items().getItem(i).set_checked(true);
                break;
            }
        }

        BindDetailsGrid(ComboStoreName, ComboStatus, ComboStratDate, "", "0", false);
    }


}
function FilterByStartDate(ComboStoreName, ComboStatus, ComboStratDate, ComboDueDate) {

    if (ComboStratDate != null) {

        BindDetailsGrid(ComboStoreName, ComboStatus, ComboStratDate, "", "0", false);
    }


}
function FilterBydueDate(ComboStoreName, ComboStatus, ComboStratDate, ComboDueDate) {

    var ds = new Date();
    var curr_day1 = ds.getDate();
    if (curr_day1 < 10) {
        curr_day1 = '0' + curr_day1;
    }
    var curr_month1 = ('0' + (ds.getMonth() + 1)).slice(-2)
    var curr_year1 = ds.getFullYear();
    var Due_date = curr_day1 + "-" + curr_month1 + "-" + curr_year1;

}

function FilterByOverDueDate(ComboStoreName, ComboStatus, ComboStratDate, ComboDueDate, IsOverDue) {

    var ds = new Date();
    var curr_day1 = ds.getDate();
    if (curr_day1 < 10) {
        curr_day1 = '0' + curr_day1;
    }
    var curr_month1 = ('0' + (ds.getMonth() + 1)).slice(-2)
    var curr_year1 = ds.getFullYear();
    var Due_date = curr_day1 + "-" + curr_month1 + "-" + curr_year1;
    BindDetailsGrid(ComboStoreName, ComboStatus, ComboStratDate, "", "0", IsOverDue);

}

function BindDetailsGrid(ComboStoreName, ComboStatus, ComboStratDate, ComboDueDate, ChkAllCombo, IsOverDue) {

    ListStoreName = "";
    ListStartDate = "";
    ListDueDate = "";
    StatusIds = "";
    text = "";
    // if (ChkAllCombo != "ctl00_ContentMain_RadComboStoreName") {
    if (ComboStoreName != null) {
        var svalue = ComboStoreName.split(',');
        if (svalue.length > 75)
            ListStoreName = "";
        else
            ListStoreName = ComboStoreName;


    }
    //}
    // if (ChkAllCombo != "ctl00_ContentMain_RadComboStatus") {
    if (ComboStatus != null) {

        StatusIds = ComboStatus;
    }
    // }
    ListStartDate = ComboStratDate;
    //ShowDetailData(ListStoreName, StatusIds, ListStartDate, "", IsOverDue);
    BindDetailGridforboth(ListStoreName, StatusIds, ListStartDate, "", IsOverDue);
}
function BindDetailGridforboth(ListStoreName, StatusIds, ListStartDate, ListDueDate, IsOverDue) {
    if (IsClient)
        ShowDetailDataClient(ListStoreName, StatusIds, ListStartDate, ListDueDate, IsOverDue);
    else
        ShowDetailData(ListStoreName, StatusIds, ListStartDate, ListDueDate, IsOverDue);
}
//var dataSourceDetail = "";
function ShowDetailData(ListStoreName, StatusIds, ListStartDate, ListDueDate, IsOverDue) {

    debugger;

    var Storenamecnt = ListStoreName.split(',');
    if (Storenamecnt.length > 78)
        ListStoreName = "";
    else
    { }

    abortRequests();
    childOverDue = IsOverDue;
    $("#DetailGrid")[0].innerHTML = "";
    if ($("#DetailGrid").data("kendoWindow") != null) {
        $('#DetailGrid').data().kendoGrid.destroy();
        $('#DetailGrid').empty();
    }
    var dataSourceDetail = "";
    dataSourceDetail = new kendo.data.DataSource({
        pageSize: 100,
        serverPaging: true,
        schema: {
            data: "d",
            total: function (d) {
                if (d.d.length > 0)
                    return d.d[0].TotalCount
            },
            model: {
                id: "ID",
                fields: {
                    ID: { editable: false, type: "number" },
                    Research_Job: { editable: false, type: "string" },
                    TotalJob_Number: { editable: false, type: "string" },
                    StoreName: { editable: false, type: "string" },
                    User: { editable: false, type: "string" },
                    StatusName: { editable: true, type: "string" },
                    StartDate: { editable: false, type: "string" },
                    LastUpdatedTime: { editable: false, type: "string" },
                    StMsg: { editable: false, type: "string" },
                    DownLoadOrCheckList: { editable: false, type: "string" }

                }
            }

        },

        transport: {
            read: function (options) {
                arrStatus[0] = $.ajax({
                    type: "GET",
                    url: "/DataTracker.svc/GetDetailList",
                    dataType: "json",
                    data: {

                        Page: options.data.page,
                        PageSize: options.data.pageSize,
                        skip: options.data.skip,
                        take: options.data.take,
                        StatusIDs: StatusIds,
                        StoreName: escape(ListStoreName),
                        StartDate: ListStartDate,
                        DueDate: ListDueDate,
                        IsChild: false,
                        IsOverDue: IsOverDue,
                        CurrentDate: RetrunCurrentDate()
                    },

                    success: function (result) {
                        options.success(result);
                        ScheduleTab = true;
                        //                        if ($.browser.safari)
                        //                            setTimeout(function () { GetadsDatas(); }, 1500);
                    }
                });
            }
        }

    });

    if ($("#DetailGrid").length > 0)
    { $("#DetailGrid")[0].innerHTML = ""; }
    var element = $("#DetailGrid").kendoGrid({
        dataSource: dataSourceDetail,
        detailInit: DetailStoreListByGroup,
        dataBound: function () {
            ShowLastUpdatedTime();
        },
        pageable: {
            batch: true,
            pageSize: 100,
            refresh: true,
            pageSizes: true,
            input: true
        },
        columns: [
                            {
                                field: "StoreName",
                                title: "Group Name/Task",
                                resize: true
                            },
                            {
                                field: "Research_Job",
                               hidden:true
                            },
                            {
                                field: "TotalJob_Number",
                                title: "Total jobs",
                                template: "<div>#=TotalJob_Number#</div>",
                                width: 90
                            },
                            {
                                field: "ID",
                                title: "",
                                hidden: true
                            },
                           { field: "StartDate", hidden: true },
                            { field: "DueDate", hidden: true },
                             { field: "StatusID", hidden: true },
                            {
                                field: "User",
                                title: "Last updated by",
                                width: 135
                            },
                            {
                                field: "LastUpdatedTime",
                                title: "Last Updated Time",
                                width: 140,
                                template: "<abbr class='timeago' title='#=LastUpdatedTime#' ID='#=StatusID#' >#=LastUpdatedTime#</abbr>"
                            },
                            { field: "StatusName", title: "Current Status", width: 200, editor: DetailParentDropDownEditor, template: "#=SetCompletedImg(StatusName,StMsg)# #=DownLoadOrCheckList#" },
                            { command: { text: "Edit", click: EditDetails }, title: "", hidden: true },
                            { command: { text: "Log", click: StatusDetails }, title: "", width: 90 },
                            {
                                field: "",
                                title: "",
                                width: 105,
                                template: "<div>#=DownloadData#</div>"

                            },
        ],
        editable: !IsClient,
        sortable: true,
        selectable: true

    });
    // HideColumnsDetailForClient("#DetailGrid")
    // GetadsDatas();
}

function ShowDetailDataClient(ListStoreName, StatusIds, ListStartDate, ListDueDate, IsOverDue) {

    var Storenamecnt = ListStoreName.split(',');
    if (Storenamecnt.length > 78)
        ListStoreName = "";
    else
    { }

    abortRequests();
    childOverDue = IsOverDue;
    $("#DetailGridClient")[0].innerHTML = "";
    if ($("#DetailGridClient").data("kendoWindow") != null) {
        $('#DetailGridClient').data().kendoGrid.destroy();
        $('#DetailGridClient').empty();
    }
    var dataSourceDetail = "";
    dataSourceDetail = new kendo.data.DataSource({
        pageSize: 100,
        serverPaging: true,
        schema: {
            data: "d",
            total: function (d) {
                if (d.d.length > 0)
                    return d.d[0].TotalCount
            },
            model: {
                id: "ID",
                fields: {
                    ID: { editable: false, type: "number" },
                    TotalJob_Number: { editable: false, type: "string" },
                    StoreName: { editable: false, type: "string" },
                    User: { editable: false, type: "string" },
                    StatusName: { editable: true, type: "string" },
                    StartDate: { editable: false, type: "string" },
                    LastUpdatedTime: { editable: false, type: "string" },
                    StMsg: { editable: false, type: "string" },
                    DownLoadOrCheckList: { editable: false, type: "string" }

                }
            }

        },

        transport: {
            read: function (options) {
                arrStatus[0] = $.ajax({
                    type: "GET",
                    url: "/DataTracker.svc/GetDetailList",
                    dataType: "json",
                    data: {

                        Page: options.data.page,
                        PageSize: options.data.pageSize,
                        skip: options.data.skip,
                        take: options.data.take,
                        StatusIDs: StatusIds,
                        StoreName: escape(ListStoreName),
                        StartDate: ListStartDate,
                        DueDate: ListDueDate,
                        IsChild: false,
                        IsOverDue: IsOverDue,
                        CurrentDate: RetrunCurrentDate()
                    },

                    success: function (result) {
                        options.success(result);
                        ScheduleTab = true;
                        //                        if ($.browser.safari)
                        //                            setTimeout(function () { GetadsDatas(); }, 1500);
                    }
                });
            }
        }

    });

    if ($("#DetailGridClient").length > 0)
    { $("#DetailGridClient")[0].innerHTML = ""; }
    var element = $("#DetailGridClient").kendoGrid({
        dataSource: dataSourceDetail,
        detailInit: DetailStoreListByGroup,
        dataBound: function () {
            ShowLastUpdatedTime();
        },
        pageable: {
            batch: true,
            pageSize: 100,
            refresh: true,
            pageSizes: true,
            input: true
        },
        columns: [
                            {
                                field: "StoreName",
                                title: "Group Name/Task",
                                resize: true
                            },
                            {
                                field: "TotalJob_Number",
                                title: "Total jobs",
                                template: "<div>#=TotalJob_Number#</div>",
                                width: 90
                            },
                            {
                                field: "User",
                                title: "Last updated by",
                                width: 160
                            },
                            { field: "StatusName", title: "Current Status", width: 200, editor: DetailParentDropDownEditor, template: "#=SetCompletedImg(StatusName,StMsg)# #=DownLoadOrCheckList#" },
                            {
                                field: "",
                                title: "",
                                width: 90,
                                template: "<div>#=DownloadData#</div>"

                            },
        ],
        editable: !IsClient,
        sortable: true,
        selectable: true

    });
    // HideColumnsDetailForClient("#DetailGrid")


}

function HideColumnsDetailForClient(GridName) {

    if (IsClient) {
        var grid = $(GridName).data("kendoGrid");
        if (grid != null) {
            grid.columns[1].width = "45%";
            grid.hideColumn(2);
            grid.hideColumn(3);
            grid.hideColumn(4);
            grid.hideColumn(5);
            grid.hideColumn(7);
            grid.hideColumn(9);
            grid.hideColumn(10);
        }
        $("#DetailGrid").addClass("detailGridJobSummry");

    }
    else {
        $("#DetailGrid").removeClass("detailGridJobSummry");
    }

}

//all child grid  

function DetailParentDroponChange() {
    var value = $("#DetailStdropparent").val();
    var grid = $("#DetailGrid").data("kendoGrid");
    var selectedrow = $("#DetailGrid").find("tbody tr.k-state-selected");
    var goods = $('#DetailGrid').data("kendoGrid").dataItem(selectedrow);
    var goodsjson = goods.toJSON();
    var StoreName = goodsjson.StoreName;
    var startDate = goodsjson.StartDate;
    var DueDate = goodsjson.StartDate;
    var StatusId = goodsjson.StatusID;
    var Confrm = "";
    if (value == "Completed") {
        Confrm = confirm("Are you sure its completed?");
    }
    else if (value == "Change Detected") {
        Confrm = confirm("Are you sure change detected?");

    }
    else if (value == "Analysis")
    { Confrm = confirm("Are you sure its analysis?"); }

    if (value == "QA Returned") {
        OpenStatusComment(startDate, StoreName, value, DueDate, StatusId, "1", 0)


    }
    else {

        if (value == "Change Detected" || value == "Completed" || value == "Analysis") {
            if (Confrm) {
                ChangeBulkstatusByStoreName(startDate, StoreName, value, DueDate, StatusId, "1", "");
            }
            else {
                return false;

            }
        }
        else {

            ChangeBulkstatusByStoreName(startDate, StoreName, value, DueDate, StatusId, "1", "");
        }
    }



}
function DetailonChangeChild() {
    var value = $("#DetailStdrop").val();
    var text = $("#DetailStdrop").text();
    var tt = childSID;
    CallchangeStatus(childSID, value);


}
//dropdown editor
function DetailParentDropDownEditor(container, options) {
    debugger;
    if (options.model.StatusName != 'Completed') {
        $('<input ID="DetailStdropparent" required data-text-field="StatusName" data-value-field="StatusName" data-bind="value:' + options.field + '"/>')
                        .appendTo(container)
                        .kendoDropDownList({
                            autoBind: false,
                            dataSource: {
                                transport: {
                                    read: {
                                        url: "/DataTracker.svc/GetStatusList"
                                    }
                                },
                                schema: {
                                    data: "d"
                                }
                            },
                            change: DetailParentDroponChange

                        });
    }
    else {
        //  alert("hell:"+ options.field);
        $('<input ID="DetailStdropparent" required data-text-field="StatusName" data-value-field="StatusName" data-bind="value:' + options.field + '"/>')
                        .appendTo(container)
                        .kendoDropDownList({
                            autoBind: false,
                            dataSource: {
                                transport: {
                                    read: {
                                        url: "/DataTracker.svc/GetStatusList"
                                    }
                                },
                                schema: {
                                    data: "d"
                                }
                            },
                            change: DetailParentDroponChange

                        });

    }
}

// child drop
function DetailChildDropDownEditorhire(container, options) {

    childSID = options.model.ID;
    $('<input ID="DetailStdrop" required data-text-field="StatusName" data-value-field="StatusName" data-bind="value:' + options.field + '"/>')
                        .appendTo(container)
                        .kendoDropDownList({
                            autoBind: false,
                            dataSource: {
                                transport: {
                                    read: {
                                        url: "/DataTracker.svc/GetStatusList"
                                    }
                                },
                                schema: {
                                    data: "d"
                                }
                            },
                            change: DetailonChangeChild

                        });

}

function AlldetailInit(e) {
    //$(e.detailCell).addClass("grid_sub2");
    $("<div/>").appendTo(e.detailCell).kendoGrid({
        dataSource: {
            transport: {
                read: {

                    url: "/DataTracker.svc/GetDetailChildList",
                    data: {
                        StatusIDs: StatusIds,
                        StoreName: escape(e.data.StoreName.split('(')[0].trim()),
                        GroupName: e.data.GroupName,
                        StartDate: ListStartDate,
                        DueDate: ListDueDate,
                        IsChild: true,
                        IsOver: childOverDue,
                        CurrentDate: RetrunCurrentDate()
                    }


                }
            },
            serverPaging: true,
            serverSorting: true,
            serverFiltering: true,
            pageSize: 6,
            schema: {
                data: "d",
                total: function (d) {
                    if (d != null) {
                        if (d.d.length > 0)
                            return d.d[0].TotalCount
                    }
                },
                model: {
                    id: "ID",
                    fields: {
                        ID: { editable: false, type: "number" },
                        StoreID: { editable: false, type: "string" },
                        ZipCode: { editable: false, type: "string" },
                        Address: { editable: false, type: "string" },
                        User: { editable: false, type: "string" },
                        StatusName: { editable: true, type: "string" }

                    }
                }

            }

        },
        pageable: true,
        dataBound: function () {

        },
        columns: [
                            { field: "ID", width: "0px", hidden: true },
                            { field: "StoreID", title: "StoreID", width: "100px" },
                            { field: "ZipCode", title: "Zip code", width: "100px" },
                            { field: "Address", title: "Address", width: "150px" },
                            { field: "User", title: "Last updated by", width: "200px" },
                             { field: "StatusName", title: "Current Status", width: "250px", editor: ChildDropDownEditorhire, template: "#=StatusName#" },
                            { command: { text: "Edit", click: EditDetails }, title: "", width: "150px" }
        ],
        editable: !IsClient,
        selectable: true

    });
}

function OverDueTaskCount() {
    $.ajax({
        type: "GET",
        url: "/DataTracker.svc/OverDueTask?CDate=" + RetrunCurrentDate() + "",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            var res = data.d;
            if (res != null) {
                if (res != "0") {
                    $("#DivOverDue")[0].innerHTML = "";
                    $("#DivOverDue")[0].innerHTML = "<a id=\"AnchorOverDue\"  class=\"AnchorOver\" title=\"Over due tasks\"  onclick=\"javascript:ViewAllDetail('o');\">Over Due Tasks - " + res + "</a>";
                }
                else {
                    $("#DivOverDue")[0].style.display = "none";
                }
            }
            else {
                return false;
            }
        }
    });

}

function RetrunCurrentDate() {

    var ds = new Date();
    var curr_day1 = ds.getDate();
    if (curr_day1 < 10) {
        curr_day1 = '0' + curr_day1;
    }
    var curr_month1 = ('0' + (ds.getMonth() + 1)).slice(-2)
    var curr_year1 = ds.getFullYear();
    var Due_date = curr_day1 + "-" + curr_month1 + "-" + curr_year1;
    return Due_date;


}

function BulkStatus() {

    if ($("#Bulkstatusdialog").data("kendoWindow") != null) {
        var dialogtest = $("#Bulkstatusdialog").data("kendoWindow");
        dialogtest.close();
    }
    var windowElement = $("#Bulkstatusdialog").kendoWindow({
        iframe: true,
        content: "ManageBulkStatus.aspx",
        modal: true,
        width: "620px",
        title: "Manage Bulk Status",
        height: "270px",
        position: {
            top: 10,
            left: 2
        },
        resizable: false,
        actions: ["Close"],
        draggable: false

    });
    var dialog = $("#Bulkstatusdialog").data("kendoWindow");
    dialog.open();
    dialog.center();
    return false;

}
//add schedule
function AddSchedule(sid) {
    if ($("#Dashdialog").data("kendoWindow") != null) {
        var dialogtest = $("#Dashdialog").data("kendoWindow");
        dialogtest.close();
    }
    var windowElement = $("#Dashdialog").kendoWindow({
        iframe: true,
        content: "ManageSchedule.aspx?sid=" + sid,
        modal: true,
        width: "69%",
        title: "Manage schedule",
        height: "540px",
        position: {
            top: 10,
            left: 2
        },
        resizable: false,
        actions: ["Close"],
        draggable: false

    });
    var dialog = $("#Dashdialog").data("kendoWindow");
    dialog.open();
    dialog.center();
    return false;

}
//
// Manage AddManageSchedule
function AddManageSchedule(sid) {
    if ($("#DashManageSche").data("kendoWindow") != null) {
        var dialogtest = $("#DashManageSche").data("kendoWindow");
        dialogtest.close();
    }

    var windowElement = $("#DashManageSche").kendoWindow({
        iframe: true,
        content: "ManageSchedule.htm?sid=" + sid,
        modal: true,
        width: "670px",
        title: "Add Schedule",
        height: "490px",
        position: {
            top: 5,
            left: 2,
            bottom: 5
        },
        resizable: true,
        actions: ["Close"],
        draggable: false

    });
    var dialog = $("#DashManageSche").data("kendoWindow");
    dialog.open();
    dialog.center();
    return false;
    // }

}

// Manage ManageScheduleStatus (anupam)
function ManageScheduleStatus(sid) {
    var arr = [];
    i = 0;
    $('input:checkbox[name=jobnumber]:checked').each(function () {
        debugger;
        arr[i++] = $(this).val();

    });



    if ($("#DashManageSche").data("kendoWindow") != null) {
        var dialogManageStatus = $("#DashManageSche").data("kendoWindow");
        dialogManageStatus.close();
    }

    var windowElement = $("#DashManageSche").kendoWindow({
        iframe: true,
        content: "ManageScheduleStatus.htm?sid=" + arr,
        modal: true,
        width: "420px",
        title: "Manage Schedule Status",
        height: "140px",
        position: {
            top: 5,
            left: 2,
            bottom: 5
        },
        resizable: true,
        actions: ["Close"],
        draggable: false

    });
    windowElement.parent().find(".k-window-action").css("visibility", "hidden");
    var dialog = $("#DashManageSche").data("kendoWindow");
    dialog.open();
    dialog.center();

    return false;

    // }

}



//Add other task
function AddOtherTask(sid, IsCreator) {
    if ($("#AddTaskWindow").data("kendoWindow") != null) {
        var dialogtest = $("#AddTaskWindow").data("kendoWindow");
        dialogtest.close();
    }

    var windowElement = $("#AddTaskWindow").kendoWindow({
        iframe: true,
        content: "ManageWorkItem.htm?sid=" + sid + "&IsCre=" + IsCreator + "",
        modal: true,
        width: "940px",
        title: "Add New Work Item",
        height: "350px",
        position: {
            top: 5,
            left: 2,
            bottom: 5
        },
        resizable: false,
        actions: ["Close"],
        draggable: false

    });
    var dialog = $("#AddTaskWindow").data("kendoWindow");
    dialog.open();
    dialog.center();
    return false;
    // }

}

function ChangeBulkstatusByStoreName(StartDate, StoreName, Status, DueDate, CurrentStatus, IsGroup, StMsg) {
    debugger;
    var groupname1 = $("#DetailSubStdropparent").parent().parent('td').parent().children('td:eq(1)').text();
    $.ajax({
        type: "POST",
        url: "/DataTracker.svc/UpdateBulkstatus",
        data: "{\"StoreName\": \"" + escape(StoreName) + "\",\"StartDate\": \"" + StartDate + "\",\"StatusID\": \"" + Status + "\",\"DueDate\": \"" + DueDate + "\",\"CurrentStatusID\": \"" + CurrentStatus + "\",\"IsGroup\": \"" + IsGroup + "\",\"IsBulk\": \"" + "0" + "\",\"StMsg\": \"" + StMsg + "\",\"groupname\": \"" + groupname1 + "\"",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            var res = data.d;
            if (res != null) {
                if (res == "1") {
                    SavedMessagePopUp("1");

                }
                else if (res == "-6") {


                }
                else if (res == "-2") {
                    window.location.href = "/Login.aspx";

                }
                else {
                    SavedMessagePopUp("0");
                }
            }
            else {

                window.location.href = "/Login.aspx";
            }
        }
    });

}
function ShowAdddressPopup(StoreOrGroupName) {
    if ($("#DivPartialdialog").data("kendoWindow") != null) {
        var dialogtest = $("#DivPartialdialog").data("kendoWindow");
        dialogtest.close();
    }
    var windowElement = $("#DivPartialdialog").kendoWindow({
        iframe: true,
        content: "/ChangeNonAddressstatus.aspx?st=" + StoreOrGroupName,
        modal: true,
        width: "600px",
        title: "Change not matching status",
        height: "375px",
        position: {
            top: 10,
            left: 2
        },
        resizable: false,
        actions: ["Close"],
        draggable: false

    });
    var dialog = $("#DivPartialdialog").data("kendoWindow");
    dialog.open();
    dialog.center();
    return false;


}

//status detail

function StatusDetails(e) {
    //  ;
    e.preventDefault();
    if ($("#StatusReportwindow").data("kendoWindow") != null) {
        var StatusReportwindow = $("#StatusReportwindow").data("kendoWindow");
        // StatusReportwindow.close();
    }
    var stname = $(e.currentTarget).closest("tr").find("td").eq(1).text();
    var stDate = $(e.currentTarget).closest("tr").find("td").eq(5).text();
    console.log(stname + "....." + stDate);
    if (stname != null) {
        OpenStatusPopup(stname, stDate, "0", 1);
    }

    // var dataItem = $("#DivUnprocessAdsGrid").dataItem($(e.currentTarget).closest("tr"));
    //  var dataItem = this.dataItem($(e.currentTarget).closest("tr"));


    //alert(dataItem);
    //    if (dataItem != null) {
    //        var stname = dataItem.StoreName;
    //        var stDate = dataItem.StartDate;

    //        OpenStatusPopup(stname, stDate, "0", 1);

    //    }

}

function OpenStatusPopup(StoreName, Sdate, Job, TaskType) {
    //  ;
    if ($("#StatusReportwindow").data("kendoWindow") != null) {
        var StatusReport = $("#StatusReportwindow").data("kendoWindow");
        // StatusReport.close();
    }
    var Title = "Status Report (" + StoreName + ")";
    var windowElement = $("#StatusReportwindow").kendoWindow({
        iframe: true,
        content: "StatusReport.aspx?stn=" + escape(StoreName) + "&sdate=" + Sdate + "&t=" + TaskType + "&j=" + Job,
        modal: true,
        width: "50%",
        title: "Status Report",
        height: "450px",
        position: {
            top: 10,
            left: 2
        },
        resizable: false,
        actions: ["Close"],
        draggable: false

    });
    var dialog = $("#StatusReportwindow").data("kendoWindow");
    dialog.setOptions({
        title: Title
    });
    dialog.open();
    dialog.center();
}

function StatusOtherDetails(e) {

    e.preventDefault();
    if ($("#StatusReportwindow").data("kendoWindow") != null) {
        var WorkItemwindow = $("#StatusReportwindow").data("kendoWindow");
        WorkItemwindow.close();
    }
    var dataItem = this.dataItem($(e.currentTarget).closest("tr"));
    if (dataItem != null) {

        var stDate = dataItem.CustomTargetDate;
        var TID = dataItem.TaskID;
        OpenWorkItemDetailPopup(TID);
    }

}
function gridrowselect() {
    var entityGrid = $("#DataGrid").data("kendoGrid");
    var data = entityGrid.dataSource.data();
    var totalNumber = data.length;

    for (var i = 0; i < totalNumber; i++) {
        var currentDataItem = data[i];
        VersionIdArray[i] = currentDataItem.VersionId;
    }
}
//open status detail
function OpenWorkItemDetailPopup(Tid) {
    window.location.href = "/WorkItemDetails.aspx?tid=" + Tid;


}


function ImportHistoryDetail() {

    $("#DivUploadHistoryGrid")[0].innerHTML = "";
    if ($("#DivUploadHistoryGrid").data("kendoGrid") != null) {
        $('#DivUploadHistoryGrid').data().kendoGrid.destroy();
        $('#DivUploadHistoryGrid').empty();
    }
    var dataSourceDetail = "";
    dataSourceDetail = new kendo.data.DataSource({
        transport: {
            read: {
                url: "/DataTracker.svc/GetImportList",
                data: {


                }
            }
        },
        schema: {
            data: "d",
            total: function (d) {
                if (d.d.length > 0)
                    return d.d[0].TotalCount
            },
            model: {
                id: "ID",
                fields: {
                    ID: { editable: false, type: "number" },
                    FileName: { editable: false, type: "string" },
                    User: { editable: false, type: "string" },
                    UploadedDate: { editable: false, type: "string" },
                    SheetName: { editable: false, type: "string" },
                    ScheduleType: { editable: false, type: "string" }



                }
            }
        },
        pageSize: 100,
        serverPaging: true

    });

    if ($("#DivUploadHistoryGrid").length > 0)
    { $("#DivUploadHistoryGrid")[0].innerHTML = ""; }
    var element = $("#DivUploadHistoryGrid").kendoGrid({
        dataSource: dataSourceDetail,
        dataBound: function () {

        },
        pageable: {
            batch: true,
            pageSize: 5,
            refresh: true,
            pageSizes: true,
            input: true
        },
        columns: [
                            {
                                field: "FileName",
                                title: "FileName",
                                width: "30%"
                            },
                            {
                                field: "User",
                                title: "Uploaded By",
                                width: "20%"

                            },
                            {
                                field: "ScheduleType",
                                title: "Schedule Type",
                                width: "10%"

                            },
                            {
                                field: "ID",
                                title: "",
                                width: "0%"
                            },
                            {
                                field: "UploadedDate",
                                title: "Uploaded Date",
                                width: "15%"
                            },
                            {
                                field: "SheetName",
                                title: "Sheet Name",
                                width: "15%"
                            },

                            { command: { text: "Download", click: DwonloadFiles }, title: "", width: "10%" }



        ],
        editable: true,
        sortable: true,
        selectable: true

    });

}
//status detail
function DwonloadFiles(e) {
    debugger;

    e.preventDefault();
    var dataItem = this.dataItem($(e.currentTarget).closest("tr"));
    if (dataItem != null) {
        var ID = dataItem.ID;
        window.open("/Downloadfiles.aspx?i=" + ID, '_blank');

    }

}
function SavedMessagePopUp(Saved) {
    $("#WinSavedMessage").innerHTML = "";
    if (Saved == "1")
        OpenAndCloseNotify("Saved successfully!")
    else if (Saved == "3") {
        OpenAndCloseNotify("Title already exist !")
    }
    else if (Saved == '4') {
        OpenAndCloseNotify("Attachment Saved!")
    }
    else if (Saved == '5') {
        OpenAndCloseNotify("Processing!")
    }
    else if (Saved == '7') {
        OpenAndCloseNotify("Attachment Deleted!")
    }
    else if (Saved == '-3') {
        OpenAndCloseNotify("Only creator can Hold or Cancel a task")
    }
    else if (Saved == '-4') {
        OpenAndCloseNotify("This task has expired and can't be reopened. Please create a new task");
    }
    else if (Saved == '10') {
        OpenAndCloseNotify("You can only reopen task");
    }
    else if (Saved == '9') {
        OpenAndCloseNotify("Completed or cancelled task only reopen");
    }
    else

        OpenAndCloseNotify("Change fail")

}

//function forgetPopUp() {
//    $("#WinChangePass").html("<div id='DivforgetPass'  align='center'><div style='width:100%;margin:10px 5px 10px 10px;'><div style='float:left'>Enter email*:</div><input type='text' id='TxtEmail'  class='forgetBox' /></div><br /><div style='float: right; width: 100%;margin-top:25px;; margin-bottom:15px;height:28px'><input type='button' id='btnforget' value='Submit' onclick='javascript:forgetPass();' class='forgetfont' />&nbsp&nbsp&nbsp;<input type='button' value='Cancel' name='cancelpop' id='btncancelf' onclick='javascript:CanelCPopup();' class='Cancelbtnfont' /></div><br/><div id='forgetMsg'></div></div>");
//    var windowElement = $("#WinChangePass").kendoWindow(

//      {
//          modal: true,
//          width: "450px",
//          height: "220px",
//          title: "Forget password",
//          position: {

//              top: 10,
//              left: 2
//          },
//          resizable: false,
//          actions: ["Close"],
//          draggable: false

//      });
//    var dialog = $("#WinChangePass").data("kendoWindow");
//    dialog.open();
//    dialog.center();
//}
//due date group store 

function DueStoreListInit(e) {
    $("<div/>").appendTo(e.detailCell).kendoGrid({
        dataSource: {
            transport: {
                read: {

                    url: "/DataTracker.svc/GetdueTaskByGroupName",
                    data: {

                        GroupName: escape(e.data.StoreName.trim()),
                        CurrentDate: RetrunCurrentDate()

                    }
                }
            },
            serverPaging: true,
            serverSorting: true,
            serverFiltering: true,
            pageSize: 5,

            schema: {
                data: "d",
                total: function (d) {
                    if (d.d.length > 0)
                        return d.d[0].TotalCount
                },
                model: {
                    id: "ID",
                    fields: {
                        ID: { editable: false, type: "number" },
                        TotalJob_Number: { editable: false, type: "number" },
                        StoreName: { editable: false, type: "string" },
                        User: { editable: false, type: "string" },
                        StatusName: { editable: true, type: "string" },
                        StartDate: { editable: false, type: "string" },
                        LastUpdatedTime: { editable: false, type: "string" }

                    }
                }

            }

        },
        pageable: true,
        detailInit: DuedetailInit,
        dataBound: function () {

        },
        columns: [
                            {
                                field: "StoreName",
                                title: "Store Name",
                                width: "33%"
                            },
                            {
                                field: "TotalJob_Number",
                                title: "Total jobs",
                                width: "15%"
                            },
                            {
                                field: "ID",
                                title: "",
                                width: "0%"
                            },
                            { field: "DueDate", width: "0%" },
                            { field: "StartDate", width: "0%" },
                            { field: "StatusID", width: "0%" },
                            {
                                field: "User",
                                title: "Last updated by",
                                width: "30%"
                            },
                            {
                                field: "LastUpdatedTime",
                                title: "Last Updated Time",
                                width: "0%",
                                template: "<abbr class='timeago' title='#=LastUpdatedTime#' ID='#=StatusID#'>#=LastUpdatedTime#</abbr>"
                            },
                            { field: "StatusName", title: "Current Status", width: "23%", editor: DueStoreListDropDownEditor, template: "#=SetCompletedImg(StatusName,'')#" }

        ],
        editable: !IsClient,
        selectable: true

    });
}
//start date
function StartStoredetailInit(e) {
    $("<div/>").appendTo(e.detailCell).kendoGrid({
        dataSource: {
            transport: {
                read: {
                    url: "/DataTracker.svc/GetTodayStartTaskByGroupName",
                    data: {
                        GroupName: escape(e.data.StoreName.trim()),
                        CurrentSDate: RetrunCurrentDate()

                    }
                }
            },
            serverPaging: true,
            serverSorting: true,
            serverFiltering: true,
            pageSize: 5,
            schema: {
                data: "d",
                total: function (d) {
                    if (d.d.length > 0)
                        return d.d[0].TotalCount
                },
                model: {
                    id: "ID",
                    fields: {
                        ID: { editable: false, type: "number" },
                        TotalJob_Number: { editable: false, type: "number" },
                        StoreName: { editable: false, type: "string" },
                        User: { editable: false, type: "string" },
                        StatusName: { editable: true, type: "string" },
                        StartDate: { editable: false, type: "string" },
                        LastUpdatedTime: { editable: false, type: "string" },
                        StMsg: { editable: false, type: "string" }
                    }
                }
            }


        },

        pageable: true,
        detailInit: StartdetailInit,
        dataBound: function () {

        },
        columns: [
                            {
                                field: "StoreName",
                                title: "Store Name",
                                width: "33%"
                            },
                            {
                                field: "TotalJob_Number",
                                title: "Total jobs",
                                width: "15%"
                            },
                            {
                                field: "ID",
                                title: "",
                                width: "0%"
                            },
                            { field: "StartDate", width: "0%" },
                            { field: "StatusID", width: "0%" },
                            {
                                field: "User",
                                title: "Last updated by",
                                width: "30%"
                            },
                            {
                                field: "LastUpdatedTime",
                                title: "Last Updated Time",
                                width: "0%",
                                template: "<abbr class='timeago' title='#=LastUpdatedTime#' ID='#=StatusID#'>#=LastUpdatedTime#</abbr>"
                            },

                            { field: "StatusName", title: "Current Status", width: "23%", editor: SDateStoreListDropDownEditor, template: "#=SetCompletedImg(StatusName,StMsg)#" }


        ],
        editable: !IsClient,
        selectable: true

    });
}

function DetailStoreListByGroup(e) {
    var AllListStoreName = "";
    if (ListStoreName != "") {
        var asname = ListStoreName.split(',');
        if (asname.length > 75)
            AllListStoreName = "";
        else
            AllListStoreName = ListStoreName;
    }
    $("<div/>").appendTo(e.detailCell).kendoGrid({
        dataSource: {
            transport: {
                read: {

                    url: "/DataTracker.svc/GetStoreListByGroupName",
                    data: {
                        StatusIDs: StatusIds,
                        StoreName: AllListStoreName,
                        StartDate: ListStartDate,
                        DueDate: ListDueDate,
                        IsChild: true,
                        IsOver: childOverDue,
                        CurrentDate: RetrunCurrentDate(),
                        GName: escape(e.data.StoreName.trim())
                    }


                }
            },
            serverPaging: true,
            serverSorting: true,
            serverFiltering: true,
            pageSize: 6,
            schema: {
                data: "d",
                total: function (d) {
                    if (d != null) {
                        if (d.d.length > 0)
                            return d.d[0].TotalCount
                    }
                },
                model: {
                    id: "ID",
                    fields: {
                        ID: { editable: false, type: "number" },
                        StoreName: { editable: false, type: "string" },
                        TotalJob_Number: { editable: false, type: "number" },
                        User: { editable: false, type: "string" },
                        StatusName: { editable: true, type: "string" },
                        User: { editable: false, type: "string" },
                        LastUpdatedTime: { editable: false, type: "string" },
                        StMsg: { editable: false, type: "string" }

                    }
                }

            }

        },
        detailInit: AlldetailInit,
        dataBound: function () {
            ShowLastUpdatedTime();

        },
        columns: [
                            {
                                field: "GroupName",
                                title: "Group Name",
                                hidden: true
                            },
                            {
                                field: "StoreName",
                                title: "Store Name",
                                width: TotalStoreWidth
                            },
                            {
                                field: "TotalJob_Number",
                                title: "Total jobs",
                                width: "12%"

                            },
        // {
        //    field: "ID",
        //   title: "",
        //   width: "0%"
        //   },
        //{ field: "StartDate", width: "0%" },
        //{ field: "DueDate", width: "0%" },
        // { field: "StatusID", width: "0%" },
                            {
                                field: "User",
                                title: "Last updated by",
                                width: UserWidth
                            },
                            {
                                field: "LastUpdatedTime",
                                title: "Last Updated Time",
                                width: LastUpWidth,
                                template: "<abbr class='timeago' title='#=LastUpdatedTime#' ID='#=StatusID#' >#=LastUpdatedTime#</abbr>"
                            },
                            { field: "StatusName", title: "Current Status", width: DropdListwidth, editor: DetailStoreListDropDownEditor, template: "#=SetCompletedImg(StatusName,StMsg)#" }
        //                            { command: { text: "Status Detail", click: SubStatusDetails }, title: "", width: statusReWidth }

        ],
        editable: !IsClient,
        sortable: true,
        selectable: true

    });

}

// 
// due dropdown editor
function DueStoreListDropDownEditor(container, options) {
    SubStore = options.model.StoreName + "," + options.model.StartDate + "," + options.model.DueDate + "," + options.model.StatusID;
    if (options.model.StatusName != 'Completed') {
        $('<input ID="DetailSubStdropparent" required data-text-field="StatusName" data-value-field="StatusName" data-bind="value:' + options.field + '"/>')
                        .appendTo(container)
                        .kendoDropDownList({
                            autoBind: false,
                            dataSource: {
                                transport: {
                                    read: {
                                        url: "/DataTracker.svc/GetStatusList"
                                    }
                                },
                                schema: {
                                    data: "d"
                                }
                            },
                            change: DetailSubchildDroponChange

                        });
    }
    else {

        $('<input ID="DetailSubStdropparent" required data-text-field="StatusName" data-value-field="StatusName" data-bind="value:' + options.field + '"/>')
                        .appendTo(container)
                        .kendoDropDownList({
                            autoBind: false,
                            dataSource: {
                                transport: {
                                    read: {
                                        url: "/DataTracker.svc/GetStatusList"
                                    }
                                },
                                schema: {
                                    data: "d"
                                }
                            },
                            change: DueSubchildDroponChange

                        });


    }
}
function DueSubchildDroponChange() {
    var value = $("#DetailSubStdropparent").val();
    var detaiSub = SubStore.split(',');
    var Confrm = "";
    if (value == "Completed") {
        Confrm = confirm("Are you sure its completed?");
    }
    else if (value == "Change Detected") {
        Confrm = confirm("Are you sure change detected?");

    }
    else if (value == "Analysis")
    { Confrm = confirm("Are you sure its analysis?"); }
    if (value == "Change Detected" || value == "Completed" || value == "Analysis") {
        if (Confrm) {
            ChangeBulkstatusByStoreName(detaiSub[1], detaiSub[0], value, detaiSub[2], detaiSub[3], "0", "");
        }
        else {
            return false;

        }
    }
    else {

        ChangeBulkstatusByStoreName(detaiSub[1], detaiSub[0], value, detaiSub[2], detaiSub[3], "0", "");

    }

}
//
// ---------------
// Sdate dropdown editor
function SDateStoreListDropDownEditor(container, options) {
    SubStore = options.model.StoreName + "," + options.model.StartDate + "," + options.model.DueDate + "," + options.model.StatusID;
    if (options.model.StatusName != 'Completed') {
        $('<input ID="SDateSubStdropparent" required data-text-field="StatusName" data-value-field="StatusName" data-bind="value:' + options.field + '"/>')
                        .appendTo(container)
                        .kendoDropDownList({
                            autoBind: false,
                            dataSource: {
                                transport: {
                                    read: {
                                        url: "/DataTracker.svc/GetStatusList"
                                    }
                                },
                                schema: {
                                    data: "d"
                                }
                            },
                            change: SdateSubchildDroponChange

                        });
    }
    else {

        $('<input ID="SDateSubStdropparent" required data-text-field="StatusName" data-value-field="StatusName" data-bind="value:' + options.field + '"/>')
                        .appendTo(container)
                        .kendoDropDownList({
                            autoBind: false,
                            dataSource: {
                                transport: {
                                    read: {
                                        url: "/DataTracker.svc/GetStatusList"
                                    }
                                },
                                schema: {
                                    data: "d"
                                }
                            },
                            change: SdateSubchildDroponChange

                        });


    }
}
function SdateSubchildDroponChange() {
    var value = $("#SDateSubStdropparent").val();
    var detaiSub = SubStore.split(',');
    var Confrm = "";
    if (value == "Completed") {
        Confrm = confirm("Are you sure its completed?");
    }
    else if (value == "Change Detected") {
        Confrm = confirm("Are you sure change detected?");

    }
    else if (value == "Analysis")
    { Confrm = confirm("Are you sure its analysis?"); }
    if (value == "Change Detected" || value == "Completed" || value == "Analysis") {
        if (Confrm) {
            ChangeBulkstatusByStoreName(detaiSub[1], detaiSub[0], value, detaiSub[2], detaiSub[3], "0", "");
        }
        else {
            return false;

        }
    }
    else {

        ChangeBulkstatusByStoreName(detaiSub[1], detaiSub[0], value, detaiSub[2], detaiSub[3], "0", "");

    }


}

// detail dropdown editor
function DetailStoreListDropDownEditor(container, options) {
    SubStore = options.model.StoreName + "," + options.model.StartDate + "," + options.model.DueDate + "," + options.model.StatusID;
    if (options.model.StatusName != 'Completed') {
        $('<input ID="DetailSubStdropparent" required data-text-field="StatusName" data-value-field="StatusName" data-bind="value:' + options.field + '"/>')
                        .appendTo(container)
                        .kendoDropDownList({
                            autoBind: false,
                            dataSource: {
                                transport: {
                                    read: {
                                        url: "/DataTracker.svc/GetStatusList"
                                    }
                                },
                                schema: {
                                    data: "d"
                                }
                            },
                            change: DetailSubchildDroponChange

                        });
    }
    else {

        $('<input ID="DetailSubStdropparent" required data-text-field="StatusName" data-value-field="StatusName" data-bind="value:' + options.field + '"/>')
                        .appendTo(container)
                        .kendoDropDownList({
                            autoBind: false,
                            dataSource: {
                                transport: {
                                    read: {
                                        url: "/DataTracker.svc/GetStatusList"
                                    }
                                },
                                schema: {
                                    data: "d"
                                }
                            },
                            change: DetailSubchildDroponChange

                        });


    }
}
function DetailSubchildDroponChange() {
    var text = $(this).parent().parent().text();
    var value = $("#DetailSubStdropparent").val();
    var detaiSub = SubStore.split(',');
    var Confrm = "";
    if (value == "Completed") {
        Confrm = confirm("Are you sure its completed?");
    }
    else if (value == "Change Detected") {
        Confrm = confirm("Are you sure change detected?");

    }
    else if (value == "Analysis")
    { Confrm = confirm("Are you sure its analysis?"); }
    if (value == "Change Detected" || value == "Completed" || value == "Analysis") {
        if (Confrm) {
            ChangeBulkstatusByStoreName(detaiSub[1], detaiSub[0], value, detaiSub[2], detaiSub[3], "0", "");
        }
        else {
            return false;

        }
    }
    else {

        ChangeBulkstatusByStoreName(detaiSub[1], detaiSub[0], value, detaiSub[2], detaiSub[3], "0", "");
    }


}
function SubStatusDetails(e) {

    if ($("#DivNewStreport").data("kendoWindow") != null) {
        var StatusReportwindow = $("#DivNewStreport").data("kendoWindow");
        StatusReportwindow.close();
    }
    var dataItem = this.dataItem($(e.currentTarget).closest("tr"));
    if (dataItem != null) {
        var stname = dataItem.StoreName;
        var stDate = dataItem.StartDate;
        OpenSubRePopup(stname, stDate, "0", "1");
        return true;
    }

}
//open status detail
function OpenSubRePopup(StoreName, Sdate, Job, TaskType) {
    if ($("#DivNewStreport").data("kendoWindow") != null) {
        var StatusReport = $("#DivNewStreport").data("kendoWindow");
        StatusReport.close();
    }

    var windowElement = $("#DivNewStreport").kendoWindow({
        iframe: true,
        content: "StatusReport.aspx?stn=" + escape(StoreName) + "&sdate=" + Sdate + "&t=" + TaskType + "&j=" + Job,
        modal: true,
        width: "50%",
        title: "Status Report",
        height: "450px",
        position: {
            top: 10,
            left: 2
        },
        resizable: false,
        actions: ["Close"],
        draggable: false

    });
    var dialog = $("#DivNewStreport").data("kendoWindow");
    dialog.open();
    dialog.center();
    return false;
}


function StatusRegularDe(e) {
    e.preventDefault();
    if ($("#DivRegularReport").data("kendoWindow") != null) {
        var StatusReportwindow = $("#DivRegularReport").data("kendoWindow");
        StatusReportwindow.close();
    }

    var dataItem = this.dataItem($(e.currentTarget).closest("tr"));
    //alert(dataItem);
    if (dataItem != null) {
        var stname = dataItem.StoreName;
        var stDate = dataItem.DbStartDate;
        var stjob = dataItem.Job_Number;
        OpenRegulartPopup(stname, stDate, stjob, "2");
        //return true;
    }

}

//open status detail
function OpenRegulartPopup(StoreName, Sdate, stjob, TaskType) {
    if ($("#DivRegularReport").data("kendoWindow") != null) {
        var StatusReport = $("#DivRegularReport").data("kendoWindow");
        StatusReport.close();
    }

    var Title = "Status Report (" + StoreName + ")";

    var windowElement = $("#DivRegularReport").kendoWindow({
        iframe: true,
        content: "StatusReport.aspx?stn=" + escape(StoreName) + "&sdate=" + Sdate + "&t=" + TaskType + "&j=" + stjob,
        modal: true,
        width: "50%",
        // title: "Status Report",
        height: "450px",
        position: {
            top: 10,
            left: 2
        },
        resizable: false,
        actions: ["Close"],
        draggable: false

    });
    var dialog = $("#DivRegularReport").data("kendoWindow");
    dialog.setOptions({
        title: Title
    });
    dialog.open();
    dialog.center();
    return false;
}
function ChangeRegularStatus(ID, Statusval) {
    $.ajax({
        type: "GET",
        url: "/DataTracker.svc/ChangeRegularstatus?SceduleId=" + ID + "&StatusID=" + Statusval + "",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            var res = data.d;
            if (res != null) {
                if (res == "1") {
                    SavedMessagePopUp("1")
                }
                else if (res == "-2") {
                    window.location.href = "/Login.aspx";
                }
                else if (res == "-1") {
                    SavedMessagePopUp("0")
                }
            }
            else {
                SavedMessagePopUp("0");
                return false;
            }
        }
    });
}
var monthdate = "";
//GetRegularSchedule(FromDate, ToDate, SearchStoreName, DescriptionName, Jobnumber, Description, StDate, TrDate, Stname, ActID, ComboMonth, StatusIds);
function GetRegularSchedule(StartDaterange, TargetDaterange, SearchStoreName, DescriptionName, Jobnumber, Description, StartDate, TargetDate, Storename, AccountID, itmonth, StatusList) {

    if (IsClient) {
        GetRegularScheduleGridClient(StartDaterange, TargetDaterange, SearchStoreName, DescriptionName, Jobnumber, Description, StartDate, TargetDate, Storename, AccountID, itmonth, StatusList);
    }
    else {

        GetRegularScheduleGrid(StartDaterange, TargetDaterange, SearchStoreName, DescriptionName, Jobnumber, Description, StartDate, TargetDate, Storename, AccountID, itmonth, StatusList);
    }


}



function GetRegularScheduleGrid(StartDaterange, TargetDaterange, SearchStoreName, DescriptionName, Jobnumber, Description, StartDate, TargetDate, StName, ActID, itmonth, StatusList) {
    debugger;
    var FilterOption = "";
    if ($("#FilterOption").val() == "ByFilter") {
        FilterOption = $("#FilterOption").val();
    }
    $("#RegularTaskGrid")[0].innerHTML = "";
    if ($("#RegularTaskGrid").data("kendoWindow") != null) {
        $('#RegularTaskGrid').data().kendoGrid.destroy();
        $('#RegularTaskGrid').empty();
    }
    var dataSourceRegular = "";
    dataSourceRegular = new kendo.data.DataSource({
        pageSize: 100,
        serverPaging: true,
        schema: {
            data: "d",
            total: function (d) {
                if (d.d.length > 0)
                    return d.d[0].TotalCount
            },
            model: {
                id: "ID",
                fields: {
                    ID: { editable: false, type: "number" },
                    StoreName: { editable: false, type: "string" },
                    Job_Number: { editable: false, type: "number" },
                    User: { editable: false, type: "string" },
                    StatusName: { editable: true, type: "string" },
                    StartDate: { editable: false, type: "string" },
                    DueDate: { editable: false, type: "string" },
                    TargetDate: { editable: false, type: "string" },
                    LastUpdatedTime: { editable: false, type: "string" },
                    DbStartDate: { editable: false, type: "string" },
                    Address: { editable: false, type: "string" },
                    City: { editable: false, type: "string" },
                    ST: { editable: false, type: "string" },
                    Zip: { editable: false, type: "string" },
                    StMsg: { editable: false, type: "string" },
                    Account_ID: { editable: false, type: "string" },
                    WebUrl: { editable: false, type: "string" },
                    FileType: { editable: false, type: "string" },
            
                }
            }
        },
        transport: {
            read: function (options) {
                arrStatus[2] = $.ajax({
                    type: "GET",
                    url: "/DataTracker.svc/GetRegularTaskList",
                    dataType: "json",
                    data: {
                        Page: options.data.page,
                        PageSize: options.data.pageSize,
                        skip: options.data.skip,
                        take: options.data.take,
                        // CurrentMonth: Month,
                        // Year: CYear,
                        StatusIDs: StatusList,
                        yyr: itmonth,
                        StName: StName,
                        DstName: DescriptionName,
                        ActID: ActID,
                        StDate: StartDate,
                        TrDate: TargetDate,
                        Jobnumber: Jobnumber,
                        Description: Description,
                        SearchStoreName: SearchStoreName,
                        StartDaterange: StartDaterange,
                        TargetDaterange: TargetDaterange,
                        FilterOption: FilterOption
                    },
                    success: function (result) {
                      
                        options.success(result);
                        RegularTaskTab = true;
                    }

                });

            }
        }


    });
    //
    // Regular grid  ( anupam) 


    if ($("#RegularTaskGrid").length > 0)
    { $("#RegularTaskGrid")[0].innerHTML = ""; }
    var element = $("#RegularTaskGrid").kendoGrid({
        dataSource: dataSourceRegular,
        dataBound: onDataBound,
        pageable: {
            batch: true,
            pageSize: 100,
            refresh: true,
            pageSizes: true,
            input: true
        },

        //change: function (e) {
        //    $('tr').find('[type=checkbox]').prop('checked', false);
        //    $('tr.k-state-selected').find('[type=checkbox]').prop('checked', true);
        //},
        columns: [
                            {
                                field: "",
                                title: "",
                                width: 30,
                                template: "<input type='checkbox' name='jobnumber' onclick='boxDisable()' class='cb-element' value='#=ID#'>"
                            },
                            {
                                field: "Job_Number",
                                title: "Job Number",
                                width: 87,
                                template: "<div title='#=Account_ID#'>#=Job_Number#</div>"
                            },

                            {
                                field: "StoreName",
                                title: "Store Name",
                                resize: true,
                                template: "<div class='hover'>#=StoreName#<div class='tooltip_webscrape'>#=WebUrl#</div></div>"
                            },
                            {
                                field: "Address",
                                title: "Address",
                                width: 120
                            },
                            {
                                field: "City",
                                title: "City",
                                width: 70
                            },
                            {
                                field: "ST",
                                title: "ST",
                                width: 60
                            },
                            {
                                field: "Zip",
                                title: "Zip",
                                width: 60
                            },
                            {
                                field: "ID",
                                title: "",
                                hidden: true
                            },
                            {
                                field: "StatusID", hidden: true
                            },
                            {
                                field: "StartDate", width: 80, title: "Start Date"
                            },
                            {
                                field: "DbStartDate", hidden: true, title: ""
                            },
                            {
                                field: "TargetDate", width: 95, title: "Target Date"
                            },

                            {
                                field: "User",
                                title: "Last updated by",
                                width: 120
                            },
                             {
                                 field: "LastUpdatedTime",
                                 title: "Updated Time",
                                 width: 100,
                                 template: "<abbr class='timeago' title='#=LastUpdatedTime#' ID='#=StatusID#' >#=LastUpdatedTime#</abbr>"
                             },

                            {
                                field: "StatusName", title: "Current Status", width: 110, editor: RegularParentDropDownEditor, template: "#=SetCompletedImg(StatusName,StMsg)#"
                            },
                            {
                                //template: "#if(ID%2 == 0) {#<div>.XLS</div>#} else{#<div>.PSV</div>#}#", width: 70, title: "File Type"
                                field: "FileType", width: 70, title: "File Type"
                            },

                            { command: { text: "Edit", click: EditDetails }, title: "", hidden: true },
                             { command: [{ text: "Log", click: StatusRegularDe }], title: "", width: 85 },
                             {
                                 field: "",
                                 title: "",
                                 width: 94,
                                 template: "<div>#=DownloadRegData#</div>"

                             },
        ],
        editable: !IsClient,
        sortable: true,
        //selectable: "multiple, row",
        selectable: true,
    });
}




function onDataBound(e) {
    var grid = $("#RegularTaskGrid").data("kendoGrid");
    var gridData = grid.dataSource.view();
    var columns = e.sender.columns;
    //  var StartDatecolumnIndex = this.wrapper.find(".k-grid-header [data-field=" + "StartDate" + "]").index();
    // var TargetDatecolumnIndex = this.wrapper.find(".k-grid-header [data-field=" + "TargetDate" + "]").index();
    ShowLastUpdatedTime();
    var rows = e.sender.tbody.children();
    if (rows.length > 0) {
        $('#NoRecordFound').css('display', 'none');
    }
    else {
        $('#NoRecordFound').css('display', 'block');
    }
    for (var j = 0; j < rows.length; j++) {
        var row = $(rows[j]);
        var dataItem = e.sender.dataItem(row);
        var Sdate = dataItem.get("StartDate");
        var Tdate = dataItem.get("TargetDate");
        var d = new Date();
        var month = d.getMonth() + 1;
        var day = d.getDate();
        var currentdate = (day < 10 ? '0' : '') + day + "-"
                    + (month < 10 ? '0' : '') + month + '-'
                    + d.getFullYear();


        var Current_DateCheck = currentdate.split('-');
        var Target_DateCheck = Tdate.split('-');
        var Current_Dateday = Current_DateCheck[0];
        var Target_DateDay = Target_DateCheck[0];
        var Targetdate_Month = Target_DateCheck[1];
        var Current_Datemonth = Current_DateCheck[1];
        var Target_Dateyear = Target_DateCheck[2];
        var Current_DateYear = Current_DateCheck[2];

        if (Sdate == currentdate && Tdate == currentdate) {
            $(row).css("color", "#2d870a");
            //var StartDate_cell = row.children().eq(StartDatecolumnIndex);
            //$(StartDate_cell).css("color", "#2d870a");
            //var TargetDate_cell = row.children().eq(TargetDatecolumnIndex);
            //$(TargetDate_cell).css("color", "#2d870a");
        }
        else if (Tdate == currentdate) {
            $(row).css("color", "#e89e00");
            //var StartDate_cell = row.children().eq(StartDatecolumnIndex);
            //$(StartDate_cell).css("color", "#ef1c1c");
            //var TargetDate_cell = row.children().eq(TargetDatecolumnIndex);
            //$(TargetDate_cell).css("color", "#ef1c1c");
        }

        if (Targetdate_Month <= Current_Datemonth && Target_Dateyear <= Current_DateYear) {
            if (Target_DateDay < Current_Dateday) {
                $(row).css("color", "#ef1c1c");
            }

        }
        else {
            if (Target_Dateyear <= Current_DateYear) {
                if (Target_DateDay < Current_Dateday && Targetdate_Month <= Current_Datemonth) {
                    $(row).css("color", "#ef1c1c");
                }
                if (Targetdate_Month < Current_Datemonth) {
                    $(row).css("color", "#ef1c1c");
                }
                if (Target_DateDay > Current_Dateday && Targetdate_Month < Current_Datemonth && Target_Dateyear <= Current_DateYear) {
                    $(row).css("color", "#ef1c1c");
                }
                if (Target_DateDay < Current_Dateday && Targetdate_Month > Current_Datemonth && Target_Dateyear < Current_DateYear) {
                    $(row).css("color", "#ef1c1c");
                }
                if (Targetdate_Month > Current_Datemonth && Target_Dateyear < Current_DateYear) {
                    $(row).css("color", "#ef1c1c");
                }

            }
        }
        //else if (Target_DateDay < Current_Dateday) {
        //    $(row).css("color", "#ef1c1c");
        //}
    }

    //for (var i = 0; i < gridData.length; i++) {
    //    var currentUid = gridData[i].uid;
    //    if (gridData[i].StartDate == gridData[i].TargetDate) {
    //        var currenRow = grid.table.find("tr[data-uid='" + currentUid + "']");
    //        //  $(currenRow).addClass("green");
    //        $(currenRow).css("background-color", "green");
    //    }
    //    else {
    //        var currenRow = grid.table.find("tr[data-uid='" + currentUid + "']");
    //        $(currenRow).css("background-color", "red");
    //    }
    //}
}

function GetRegularScheduleGridClient(StartDaterange, TargetDaterange, SearchStoreName, DescriptionName, Jobnumber, Description, StartDate, TargetDate, Storename, AccountID, itmonth, StatusList) {
    var FilterOption = "";
    if ($("#FilterOption").val() == "ByFilter") {
        FilterOption = $("#FilterOption").val();
    }
    $("#RegularTaskGrid")[0].innerHTML = "";
    if ($("#RegularTaskGrid").data("kendoWindow") != null) {
        $('#RegularTaskGrid').data().kendoGrid.destroy();
        $('#RegularTaskGrid').empty();
    }

    var dataSourceRegular = "";
    dataSourceRegular = new kendo.data.DataSource({
        pageSize: 100,
        serverPaging: true,
        schema: {
            data: "d",
            total: function (d) {
                if (d.d.length > 0)
                    return d.d[0].TotalCount
            },
            model: {
                id: "ID",
                fields: {
                    ID: { editable: false, type: "number" },
                    StoreName: { editable: false, type: "string" },
                    Job_Number: { editable: false, type: "number" },
                    User: { editable: false, type: "string" },
                    StatusName: { editable: true, type: "string" },
                    StartDate: { editable: false, type: "string" },
                    DueDate: { editable: false, type: "string" },
                    TargetDate: { editable: false, type: "string" },
                    LastUpdatedTime: { editable: false, type: "string" },
                    DbStartDate: { editable: false, type: "string" },
                    Address: { editable: false, type: "string" },
                    City: { editable: false, type: "string" },
                    ST: { editable: false, type: "string" },
                    Zip: { editable: false, type: "string" },
                    StMsg: { editable: false, type: "string" },
                    Account_ID: { editable: false, type: "string" }
                }
            }
        },
        transport: {
            read: function (options) {
                arrStatus[2] = $.ajax({
                    type: "GET",
                    url: "/DataTracker.svc/GetRegularTaskList",
                    dataType: "json",
                    data: {
                        Page: options.data.page,
                        PageSize: options.data.pageSize,
                        skip: options.data.skip,
                        take: options.data.take,
                        // CurrentMonth: Month,
                        //                    Year: CYear,
                        StatusIDs: StatusList,
                        yyr: itmonth,
                        StName: Storename,
                        DstName: DescriptionName,
                        ActID: AccountID,
                        StDate: StartDate,
                        TrDate: TargetDate,
                        Jobnumber: Jobnumber,
                        Description: Description,
                        SearchStoreName: SearchStoreName,
                        StartDaterange: StartDaterange,
                        TargetDaterange: TargetDaterange,
                        FilterOption: FilterOption
                    },
                    success: function (result) {
                        options.success(result);
                        RegularTaskTab = true;

                    }

                });

            }
        }


    });
    //
    // Regular grid   

    if ($("#RegularTaskGrid").length > 0)
    { $("#RegularTaskGrid")[0].innerHTML = ""; }
    var element = $("#RegularTaskGrid").kendoGrid({
        dataSource: dataSourceRegular,
        dataBound: onDataBound,
        pageable: {
            batch: true,
            pageSize: 100,
            refresh: true,
            pageSizes: true,
            input: true
        },
        columns: [
                            {
                                field: "Job_Number",
                                title: "Job Number",
                                width: 87,
                                template: "<div title='#=Account_ID#'>#=Job_Number#</div>"
                            },

                            {
                                field: "StoreName",
                                title: "Store Name",
                                resize: true,
                                template: "<div class='hover'>#=StoreName#<div class='tooltip_webscrape'>#=WebUrl#</div></div>"
                                // width: "14.5%"
                            },
                            {
                                field: "Address",
                                title: "Address",
                                width: 170
                            },
                            {
                                field: "City",
                                title: "City",
                                width: 95
                            },
                            {
                                field: "ST",
                                title: "ST",
                                width: 105
                            },
                            {
                                field: "Zip",
                                title: "Zip",
                                width: 85
                            },
        //                            {
        //                                field: "ID",
        //                                title: "",
        //                                hidden: true
        //                            },
        //                            { field: "StatusID", hidden: true },
                            { field: "StartDate", width: 80, title: "Start Date" },
                            { field: "DbStartDate", hidden: true, title: "" },
                            { field: "TargetDate", width: 95, title: "Target Date" },

                            {
                                field: "User",
                                title: "Last updated by",
                                width: 120
                            },
        //                             {
        //                                 field: "LastUpdatedTime",
        //                                 title: "Updated Time",
        //                                 width: 120,
        //                                 template: "<abbr class='timeago' title='#=LastUpdatedTime#' ID='#=StatusID#' >#=LastUpdatedTime#</abbr>"
        //                             },

                            { field: "StatusName", title: "Current Status", width: 110, editor: RegularParentDropDownEditor, template: "#=SetCompletedImg(StatusName,StMsg)#" },

        //                            { command: { text: "Edit", click: EditDetails }, title: "", hidden: true },
        //                             { command: [{ text: "Log", click: StatusRegularDe}], title: "", width: 85 },
                             {
                                 field: "",
                                 title: "",
                                 width: 94,
                                 template: "<div>#=DownloadRegData#</div>"

                             },
        // { command: [{ text: "Log", click: StatusAdDetectedDetails}], title: "", width: 90 }
        ],
        editable: !IsClient,
        sortable: true,
        selectable: true

    });
    // HideColumnsForClient("#RegularTaskGrid");


}

function GetMonthlyRegShDetail(MonthType, ComboStatus) {
    GetRegularall(MonthType, ComboStatus);
}
function GetRegularall(MonthType, ComboStatus) {
    //  ;
    alert(MonthType);
    var StatusIds = "";
    if (ComboStatus != null) {
        var MonthYr = "";
        if (MonthType != null) {
            MonthYr = MonthType;
        }

        GetRegularSchedule(MonthYr, StatusIds);
    }

}
function DeveloperMenu() {
    IsDeveloper = true;
}
function CloseCommentPopup() {
    window.parent.$("#StatusCommentwindow").data("kendoWindow").close();
}

function OpenStatusComment(startDate, StoreName, value, DueDate, StatusId, Is, Scid) {
    if ($("#StatusCommentwindow").data("kendoWindow") != null) {
        var StatusReport = $("#StatusCommentwindow").data("kendoWindow");
        StatusReport.close();
    }

    var windowElement = $("#StatusCommentwindow").kendoWindow({
        iframe: true,
        content: "StatusComment.aspx?gname=" + escape(StoreName) + "&sdate=" + startDate + "&st=" + value + "&SId=" + StatusId + "&Ddate=" + DueDate + "&type=" + Is + "&scid=" + Scid + "",
        modal: true,
        width: "400px",
        title: "Comment",
        height: "280px",
        position: {
            top: 10,
            left: 2
        },
        resizable: false,
        actions: ["Close"],
        draggable: false

    });
    var dialog = $("#StatusCommentwindow").data("kendoWindow");
    dialog.open();
    dialog.center();
    return false;
}
//add status comment
function AddStatusComment(GroupName, StName, Sdate, DDate, StID, type, scid) {

    $.ajax({
        type: "POST",
        url: "/DataTracker.svc/AddstatusComment",
        data: "{\"GroupName\": \"" + GroupName + "\",\"StName\": \"" + StName + "\",\"Msg\": \"" + escape($("#TextComment")[0].value) + "\",\"Sdate\": \"" + Sdate + "\",\"StId\": \"" + StID + "\",\"Type\": \"" + type + "\"",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            if (data.d != null) {
                if (data.d == "1") {
                    $("#StatusMessage")[0].style.color = "green";
                    $("#StatusMessage")[0].innerHTML = "Saved successfully";
                    if (type == "2") {

                        ChangeRegularStatus(scid, StName);
                    }
                    else {
                        ChangeBulkstatusByStoreName(Sdate, GroupName, StName, DDate, StID, "1", escape($("#TextComment")[0].value));
                    }
                    CloseCommentPopup();
                }
                else if (data.d == "0") {
                    $("#StatusMessage")[0].style.color = "red";
                    $("#StatusMessage")[0].innerHTML = "Already logged same message, try again";
                }

            }
            else {

                $("#StatusMessage")[0].style.color = "red";
                $("#StatusMessage")[0].innerHTML = "Some technical problem occur, please retry!";
                CloseCommentPopup();
            }
        },
        Error: function (data) {

            CloseCommentPopup();
        }
    });
}

function GetLatestStatusMsg(StatusName, GroupName, Sdate) {
    var msg = "";
    $.ajax({
        type: "POST",
        url: "/DataTracker.svc/GetLastestStatusMsg",
        data: "{\"GroupName\": \"" + GroupName + "\",\"StName\": \"" + StatusName + "\",\"Sdate\": \"" + Sdate + "\"",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            if (data.d != null) {
                if (data.d != null) {
                    msg = data.d;
                }
            }

        },
        Error: function (data) {


        }
    });

    return msg;
}

var permanotice = [];
permanotice[0] = null;
permanotice[1] = null;
permanotice.length = 2

function Notification(text, icon) {
    var stack_bottomrightDown = { "dir1": "up", "dir2": "left", "firstpos1": 15, "firstpos2": 15 };
    if (icon == "no") {
        if (permanotice[1] == null) {
            permanotice[0] = $.pnotify({
                pnotify_addclass: "stack-bottomright",
                pnotify_stack: stack_bottomrightDown, pnotify_text: text,
                pnotify_nonblock: true, pnotify_hide: false,
                pnotify_closer: false, pnotify_history: false, pnotify_mouse_reset: false, pnotify_opacity: .8, pnotify_notice_icon: ""
            });
        }
    }
    else {

        CloseNotification(); //Specifically to remove sending email notification      
        permanotice[1] = $.pnotify({ pnotify_addclass: "stack-bottomright", pnotify_stack: stack_bottomrightDown, pnotify_text: text, pnotify_nonblock: true, pnotify_hide: false, pnotify_closer: false, pnotify_history: false, pnotify_mouse_reset: false, pnotify_opacity: .8 });
    }
}
function CloseNotification() {
    if (permanotice[1] != null) {
        permanotice[1].pnotify_remove();
        permanotice[1] = null;
    }
}
function OpenAndCloseNotify(Msg) {

    Notification(Msg, "yes");
    if (Msg == "Processing!") {
        setTimeout("CloseNotification()", 3000);
    }
    else
        setTimeout("CloseNotification()", 5000);
}
function onChange() {
    StoreItemChecked();
}

function onOffCyChange() {
    var CDate = $("#DivOffCal").val();
    OffcycleNoAdsDetected($("#DivOffCal").val());
    $("#DivOffcycleNewAdsGrid")[0].innerHTML = "";
    $("#DivUnprocessAdsGrid")[0].innerHTML = "";
    $("#DivShowAlreadyGrid")[0].style.display = "none";
    $("#Button3Alreadyads")[0].style.display = "none";
    $("#DivOffcycleOldDeAdsGrid")[0].innerHTML = "";
    abortRequests();
    // GetOffCycleNewAdsFilters();
    // BindOffcycleFilterSt();
    GetOffcycleNewStAds(CDate, FormatedItemCombo(Offoptions));
    GetOffcycleUnprocessedAds($("#DivOffCal").val());


}
function ShowAlreadyExist() {
    $("#DivShowAlreadyGrid")[0].style.display = "block";
    $("#BtnShowAlreadyads").val("Loading ...")
    if (OffcyleAds2 == false) {
        $("#DivOffcycleOldDeAdsGrid")[0].innerHTML = "";
        GetOffcycleOldStAds($("#DivOffCal").val());
    }

}


var s;
var IsCreator = 0;

function ShowWorkItemDetails() {

    var entityGrid = $("#DivOtherTask").data("kendoGrid");
    var data = entityGrid.dataSource.data();
    var totalNumber = data.length;
    if (data[0] != null) {
        var currentDataItem = data[0];
        s = currentDataItem.TaskID;
        IsCreator = currentDataItem.IsCreator;
    }
    if (emailTaskID == 0) {

    }
    $("#filtertxt").animate({ margin: '0px 0px 0px 40px' }, 300);
    $("#tabstripDashboard").animate({ width: "56%" }, 300);
    $("#addworkitemdiv").animate({ margin: '0px 0px 0px 0px' }, 300);
    $("#asana")[0].style.display = "block";
    // $("#lblerror")[0].style.display = "none";
    var row = $("#DivOtherTask").data("kendoGrid").table.find('tr[TaskID=' + s + ']');
    $(row).addClass('k-state-selected');
}
function MonthlyOfficeCycle(OffCycle) {

    if ($("#DownloadAdsReportsdialog").data("kendoWindow") != null) {
        var dialogtest = $("#DownloadAdsReportsdialog").data("kendoWindow");
        dialogtest.close();
    }
    var Title = "";
    Title = "Download Reports";
    var windowElement = $("#DownloadAdsReportsdialog").kendoWindow({
        iframe: true,
        content: "SecondaryAds.aspx",
        modal: true,
        width: "550px",
        title: Title,
        height: "380px",
        position: {
            top: 10,
            left: 2
        },
        resizable: true,
        actions: ["Close"],
        draggable: false

    });
    var dialog = $("#DownloadAdsReportsdialog").data("kendoWindow");
    dialog.setOptions({
        title: Title
    });
    dialog.open();
    dialog.center();
    return false;

}
var l = 0;
var t = 0;
var k = 0;
var taid = 0;
function onTabChange(arg) {
    // alert("in onTabChange");
    var selected = this.select()[0],
            item = this.dataItem(selected);
    if (taid != item.TaskID) {
        k = 0;
    }
    taid = item.TaskID;
    IsCreator = item.IsCreator;
    if (item.TaskID == emailTaskID) {
        InsertTimlog1(item.TaskID, 0, IsCreator);
    }
    else {
        InsertTimlog1(item.TaskID, 1, IsCreator);

    }
}
var WorkStatusList = "";

function GetClearItems(CombStatusList, isrespd) {
    $("#DivOtherTask").innerHTML = "";
    $("#DivOtherTask").html("");
    var isresp = $("#Checkforresponsible").val();
    abortRequests();
    // WorkStatusList = "";
    WorkStatusList = CombStatusList;
    var tasktextvalue = $("#filtext").val();
    var dataSourceStore = "";
    dataSourceStore = new kendo.data.DataSource({
        serverPaging: true,
        pageSize: 30,
        sortable: {
            mode: "single",
            allowUnsort: false
        },
        schema: {
            data: "d",
            total: function (d) {
                if (d.d.length > 0)
                    return d.d[0].TotalCount
            },
            model: {
                id: "ID",
                fields: {
                    Sno: { editable: false, type: "string" },
                    TaskID: { editable: false, type: "number" },
                    Title: { editable: false, type: "string" },
                    WebSite: { editable: false, type: "string" },
                    // Description: { editable: false, type: "string" },
                    Description: { editable: false, type: "string" },
                    StatusName: { editable: true, type: "string" },
                    TargetDate: { editable: false, type: "string" },
                    CreatedDate: { editable: false, type: "string" },
                    Priority: { editable: false, type: "string" },
                    Feasibility: { editable: false, type: "string" },
                    AssinedTo: { editable: false, type: "string" },
                    AssinedBy: { editable: false, type: "string" },
                    Comments: { editable: false, type: "string" },
                    StMsg: { editable: false, type: "string" },
                    CustomTargetDate: { editable: false, type: "string" },
                    IsCreator: { editable: false, type: "number" },
                    isresponsible: { editable: false, type: "string" },
                    enddate: { editable: false, type: "string" },
                    ProjectType: { editable: false, type: "string" }


                }
            }
        },
        transport: {
            read: function (options) {
                arrStatus[12] = $.ajax({
                    type: "GET",
                    url: "/DataTracker.svc/GetAllOtherTasklist",
                    dataType: "json",
                    data: {
                        Page: options.data.page,
                        PageSize: options.data.pageSize,
                        skip: options.data.skip,
                        take: options.data.take,
                        StatusIDs: WorkStatusList,
                        Titlval: tasktextvalue,
                        IsResp: isresp
                    },
                    success: function (result) {
                        options.success(result);
                        OtherTaskTab = true;

                    }

                });

            }
        }



    });

    var element = $("#DivOtherTask").kendoGrid({
        dataSource: dataSourceStore,
        change: onTabChange,
        dataBound: function (e) {
            ShowLastUpdatedTime();
            ShowWorkItemDetails();
            ShowAdDetectedLastUpdatedTime();
            ShowLastaaaaa();
            var grid = e.sender;
            $.each(grid.tbody.find('tr'), function () {
                var model = grid.dataItem(this);
                if (emailTaskID != 0) {
                    if (model.TaskID == emailTaskID) {
                        grid.select(this);
                    }
                }
                else {
                    if (model.TaskID == s) {
                        grid.select(this);
                    }
                }
            });

        },
        pageable: {

            pageSize: 30,
            refresh: true,
            // resizable: true,
            pageSizes: true,
            input: true
        },

        columns: [

                           {
                               field: "Sno",
                               title: "Work Item ID",
                               width: 90,
                               hidden: true

                           },
                            {
                                field: "TaskID",
                                title: "Sno",
                                width: 0,
                                hidden: true
                            },
                            {
                                field: " IsCreator",
                                width: 0,
                                hidden: true
                            },
                             {
                                 field: "StatusID",
                                 title: "",
                                 width: 0,
                                 hidden: true
                             },

                              {
                                  field: "Title",
                                  title: "Title",
                                  resizable: true,
                                  template: "<span>#=Title#</span>"

                              },

                               { field: "CustomTargetDate", width: "0px", title: "", hidden: true },
                               {
                                   field: "Priority",
                                   title: "Priority",
                                   width: 0,
                                   template: "#=SetPriorityColor(Priority,enddate,StatusID,isresponsible)#",
                                   resizable: false,
                                   hidden: true
                               },
                              {
                                  field: "ProjectType",
                                  title: "Project",
                                  width: 120,
                                  resizable: false
                              },
                               {
                                   field: "CreatedDate",
                                   title: "Logged Date",
                                   width: 0,
                                   hidden: true

                               },
                                                           {
                                                               field: "enddate",
                                                               title: "Due Date",
                                                               width: 100,
                                                               template: "<abbr class='timeagotask' title='#=enddate#' ID='#=StatusID#'>#=enddate#</abbr>"

                                                           },
                               {
                                   field: "TargetDate",
                                   title: "Completed Date",
                                   width: 90,
                                   hidden: true
                               },
                               {
                                   field: "AssinedBy",
                                   title: "Created By",
                                   width: 0,
                                   hidden: true,
                                   template: "<span>#=AssinedBy#</span>"
                               },

                           { field: "StatusName", title: "Status", width: 125, editor: OtherTaskParentDropDownEditor, template: "#=SetCompletedImg(StatusName,StMsg)#" }
        ],

        editable: !IsClient,
        sortable: true,
        selectable: true

    });

    // CombStatusList.set_emptyMessage("4 items checked");
    $("#DivOtherTask .k-grid-content").css("max-height", $(window).height() - 245);
    $('#divstatuscheck').css('display', 'none');
}
//}

var itemsiscompleted = 0;
function ShowStatusDetails() {

    if (itemsiscompleted == 0 && emailTaskID != 0) {
        $('#divstatuscheck').css('display', 'none');
    }
}

var IsAllCheck = 0;

function GetAllOtherTasks(CombStatusList, isload, Clearfilter, Responsibled) {
    //alert(CombStatusList);
    $("#DivOtherTask").innerHTML = "";
    $("#DivOtherTask").html("");
    var isResponsibled = $("#Checkforresponsible").val();

    var WorkStatusList = CombStatusList;
    var tasktextvalue = $("#filtext").val();
    var dataSourceStore = "";
    dataSourceStore = new kendo.data.DataSource({
        serverPaging: true,
        pageSize: 30,
        sortable: {
            mode: "single",
            allowUnsort: false
        },
        schema: {
            data: "d",
            total: function (d) {
                if (d.d.length > 0)
                    return d.d[0].TotalCount
            },
            model: {
                id: "ID",
                fields: {
                    Sno: { editable: false, type: "string" },
                    TaskID: { editable: false, type: "number" },
                    Title: { editable: false, type: "string" },
                    WebSite: { editable: false, type: "string" },
                    // Description: { editable: false, type: "string" },
                    Description: { editable: false, type: "string" },
                    StatusName: { editable: true, type: "string" },
                    TargetDate: { editable: false, type: "string" },
                    CreatedDate: { editable: false, type: "string" },
                    Priority: { editable: false, type: "string" },
                    Feasibility: { editable: false, type: "string" },
                    AssinedTo: { editable: false, type: "string" },
                    AssinedBy: { editable: false, type: "string" },
                    Comments: { editable: false, type: "string" },
                    StMsg: { editable: false, type: "string" },
                    CustomTargetDate: { editable: false, type: "string" },
                    IsCreator: { editable: false, type: "number" },
                    isresponsible: { editable: false, type: "string" },
                    enddate: { editable: false, type: "string" },
                    ProjectType: { editable: false, type: "string" }


                }
            }
        },
        transport: {
            read: function (options) {
                arrStatus[9] = $.ajax({
                    type: "GET",
                    url: "/DataTracker.svc/GetAllOtherTasklist",
                    dataType: "json",
                    data: {
                        Page: options.data.page,
                        PageSize: options.data.pageSize,
                        skip: options.data.skip,
                        take: options.data.take,
                        StatusIDs: WorkStatusList,
                        Titlval: tasktextvalue,
                        IsResp: isResponsibled
                    },
                    success: function (result) {
                        options.success(result);
                        OtherTaskTab = true;

                    }

                });


            }
        }



    });

    // other task date grid

    var element = $("#DivOtherTask").kendoGrid({
        dataSource: dataSourceStore,
        change: onTabChange,
        dataBound: function (e) {


            ShowLastUpdatedTime();
            ShowWorkItemDetails();
            ShowAdDetectedLastUpdatedTime();
            // SetPriorityColor(Priority, enddate, StatusID, isresponsible);
            ShowLastaaaaa();
            var grid = e.sender;
            $.each(grid.tbody.find('tr'), function () {
                var model = grid.dataItem(this);
                if (emailTaskID != 0) {
                    if (model.TaskID == emailTaskID) {
                        grid.select(this);
                        itemsiscompleted = 1;

                    }
                }
                else {
                    if (model.TaskID == s) {
                        grid.select(this);
                    }
                }
            });
            ShowStatusDetails();
        },
        pageable: {

            pageSize: 30,
            refresh: true,
            resizable: true,
            pageSizes: true,
            input: true
        },

        columns: [

                           {
                               field: "Sno",
                               title: "Work Item ID",
                               width: 90,
                               hidden: true

                           },
                            {
                                field: "TaskID",
                                title: "Sno",
                                width: 0,
                                hidden: true
                            },
                            {
                                field: " IsCreator",
                                width: 0,
                                hidden: true
                            },
                             {
                                 field: "StatusID",
                                 title: "",
                                 width: 0,
                                 hidden: true
                             },

                              {
                                  field: "Title",
                                  title: "Title",
                                  resizable: true,
                                  template: "<span>#=Title#</span>"

                              },

                               { field: "CustomTargetDate", width: "0px", title: "", hidden: true },
                               {
                                   field: "Priority",
                                   title: "Priority",
                                   width: 0,
                                   template: "#=SetPriorityColor(Priority,enddate,StatusID,isresponsible)#",
                                   resizable: false,
                                   hidden: true
                               },
                              {
                                  field: "ProjectType",
                                  title: "Project",
                                  width: 120,
                                  resizable: false
                              },
                               {
                                   field: "CreatedDate",
                                   title: "Logged Date",
                                   width: 0,
                                   hidden: true

                               },
                                                           {
                                                               field: "enddate",
                                                               title: "Due Date",
                                                               width: 100,
                                                               template: "<abbr class='timeagotask' title='#=enddate#' ID='#=StatusID#'>#=enddate#</abbr>"

                                                           },
                               {
                                   field: "TargetDate",
                                   title: "Completed Date",
                                   width: 90,
                                   hidden: true
                               },
                               {
                                   field: "AssinedBy",
                                   title: "Created By",
                                   width: 0,
                                   hidden: true,
                                   template: "<span>#=AssinedBy#</span>"
                               },

                           { field: "StatusName", title: "Status", width: 125, editor: OtherTaskParentDropDownEditor, template: "#=SetCompletedImg(StatusName,StMsg)#" },

        ],

        editable: !IsClient,
        sortable: true,
        selectable: true

    });
    $("#DivOtherTask .k-grid-content").css("max-height", $(window).height() - 245);
    $('#divstatuscheck').css('display', 'none');


}




var ok = 0;
var k = 0;

function showtaskdata(TaskWorkID, display, IsCrea) {
    //  ;
    if (display == 4) {
        emailTaskID = TaskWorkID;
        s = TaskWorkID;
        IsCreator = IsCrea;

    }
    else {

        if (TaskWorkID != null) {
            var Sid = TaskWorkID;
            s = Sid;
            IsCreator = IsCrea;
            // $('#divmain')[0].innerHTML = "";
            showEditForm(Sid, IsCreator)
        }
    }
}

function InsertTimlog1(TaskWorkID, display, IsCrea) {
    //  ;
    if (display == 4) {
        emailTaskID = TaskWorkID;
    }
    $.ajax({
        type: "GET",
        url: "/RetailDataService.svc/GetOtherTaskData?ID='" + TaskWorkID + "'&sel='" + "0" + "'&p='" + "" + "'",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {

            statusidofemailtask = data.d.GetOtherTaskData.StatusID;
            showtaskdata(TaskWorkID, display, IsCrea);
        },
        failure: function (response) {

        },
        error: function (xhr, status, error) {

        }
    });
}


function Close() {
    $("#tabstripDashboard").animate({ width: '100%' }, 300);
    slideright = 65;
    $("#filtertxt").animate({ margin: '0px 0px 0px 40px' }, 300);
    $("#addworkitemdiv").animate({ margin: '0px 0px 0px 820px' }, 300);
    $("#asana")[0].style.display = "none";
    if ($("#asana").data("kendoWindow") != null) {
        var dialogtest = $("#asana").data("kendoWindow");
        dialogtest.close();
    }
    s = null;
    IsCreator = 0;
}
var allmoreowners = "";
function showEditForm(ID, IsCreator) {
    //   ;
    var sel = 0;
    abortRequests();
    arrStatus[10] = $.ajax({
        type: "GET",
        url: "/RetailDataService.svc/GetOtherTaskData?ID='" + ID + "'&sel='" + sel + "'&p='" + "" + "'",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {

            setWindowData(data, ID, IsCreator);
            GetallSubtasks(ID);
            GetTaskcomment(ID, 0)
        },
        failure: function (response) {

        },
        error: function (xhr, status, error) {

        }
    });
}
var focusedCelldataforsel = "";
function setWindowData(data, SchID, IsCreator) {
    focusedCelldataforsel = data.d;
    TaskID = SchID;
    var filecount = data.d.GetOtherTaskData.FileCount;
    var taskstatusid = data.d.GetOtherTaskData.StatusID;
    var tasktitle = data.d.GetOtherTaskData.Title;
    var descs = data.d.GetOtherTaskData.Description;
    var cre = data.d.GetOtherTaskData.AssinedBy;
    $("#details_property_sheet_title")[0].value = "";
    $("#descriptionoftask")[0].innerHTML = "";
    $("#fulldescriptionoftask")[0].innerHTML = "";

    $("#details_property_sheet_title")[0].value = tasktitle;
    $("#CreaterOfTasks")[0].innerHTML = "";
    $("#CreaterOfTasks")[0].innerHTML = "<span title='Created By: " + cre + "'>" + cre + "</span>"

    $("#descriptionoftask")[0].innerHTML = descs;
    $("#fulldescriptionoftask")[0].innerHTML = descs;

    if ($("#fulldescriptionoftask").css('display') == 'block') {
        $("#descriptionoftask")[0].style.display = 'block';
        $("#fulldescriptionoftask")[0].style.display = 'none';
    }

    var showChar = 150;
    var ellipsetext = "...";
    var moretext = "more";
    var c = "";
    var totlines = descs.split('\n').length;
    var height = $("#descriptionoftask").height();

    if (descs.length < showChar && descs.length != 0 && totlines > 5) {
        for (i = 0; i < 5; i++) {
            c += descs.split('\n')[i] + "\n";
        }
        c = c.slice(0, c.lastIndexOf("\n"));
        var html = "" + c + "<span class='moreellipses'>" + ellipsetext + "&nbsp;</span><span class='morecontent' onclick='javascript:fulldescription();'><span class='morelink' style='cursor:pointer; color:#1f8dd6;'>" + moretext + "</span></span>";
    }
    else {
        if (totlines >= 5) {
            for (i = 0; i < 4; i++) {
                c += descs.split('\n')[i] + "\n";
            }
            c = c.slice(0, c.lastIndexOf("\n"));
            var html = "" + c + "<span class='moreellipses'>" + ellipsetext + "&nbsp;</span><span class='morecontent' onclick='javascript:fulldescription();'><span class='morelink' style='cursor:pointer; color:#1f8dd6;'>" + moretext + "</span></span>";
        }
        else {

            var html = descs;
        }
    }

    if (descs.length >= showChar) {
        var c = descs.substr(0, showChar);
        var h = descs.substr(showChar - 1, descs.length - showChar);

        var html = "" + c + "<span class='moreellipses'>" + ellipsetext + "&nbsp;</span><span class='morecontent' onclick='javascript:fulldescription();'><span class='morelink' style='cursor:pointer; color:#1f8dd6;'>" + moretext + "</span></span>";
    }


    $("#descriptionoftask")[0].innerHTML = html;
    //$("#descriptionoftask")[0].innerHTML = descs;
    $("#allattachmentlist")[0].innerHTML = "";
    $("#divforduedate")[0].innerHTML = "";
    $("#DivforOwners")[0].innerHTML = '';
    $("#DashdivforTaskTime")[0].innerHTML = '';

    if ($("#allattachmentlist").length > 0) {
        if (data.d.GetOtherTaskData.Attachments != "")
            $("#allattachmentlist")[0].innerHTML = data.d.GetOtherTaskData.Attachments;
    }

    var dudate = data.d.GetOtherTaskData.TargetDate;
    var fdudate = MDFormat(dudate);
    var ttimr = data.d.GetOtherTaskData.TotalWTime;
    if (ttimr == "0" || ttimr == "") { $("#DivFortotalWorkTimes")[0].style.display = "none"; }
    else { $("#DivFortotalWorkTimes")[0].style.display = "block"; }
    $("#divforduedate").append("<span data-icon='calendar' class='calendar glyph toolbar-icon prod'></span><span class='token-wrapper' title='Due Date'><span class='token toolbar-due-date' alt='Tomorrow'><a tabindex='-1' class='token_name'><span class='grid_due_date due_soon'>" + fdudate + "</span></a></span> </span>");
    $("#DashdivforTaskTime").append("<span data-icon='calendar' class='calendar glyph toolbar-icon prod'></span><span class='token-wrapper' title='Task Time Sheet'><span class='token toolbar-due-date' alt='Tomorrow'><a tabindex='-1' class='token_name'><span class='grid_due_date due_soon'>" + ttimr + "</span></a></span> </span>")
    var issubscriberr = data.d.GetOtherTaskData.issubscribeerr;

    var bothassigandsubs = data.d.GetOtherTaskData.AssinedTo + "" + data.d.GetOtherTaskData.SubscribTo;
    var arrcheck = data.d.GetOtherTaskData.AssinedTo.split(',');
    var arrow = bothassigandsubs.split(',');
    var moreow = '';
    var co;
    if (data.d.GetOtherTaskData.SubscribTo != "" && data.d.GetOtherTaskData.SubscribTo != null)
        co = 1;
    else
        co = 2;
    for (var i = 0; i <= arrow.length - co; i++) {
        if (i <= 2) {
            if ($("#DivforOwners").length > 0) {
                $("#DivforOwners").append(arrow[i]);
            }

        }
        else {
            var bb = arrow[i];
            if (arrcheck.length - 1 <= i)
                moreow += $(bb).attr('title') + " (Subscriber)" + ",";
            else
                moreow += $(bb).attr('title') + " (Owner)" + ",";
        }

    }
    if (moreow != '' && moreow != null) {
        moreow = removeLastComma(moreow);
        allmoreowners = moreow;
        $("#DivforOwners").append("<input type='button' value='More...' title='" + moreow + "' onclick='javascript:alertshowallowners();' style='border-width: 3px 1px 1px; border-style: solid; height: 28px; border-radius: 0px; background-color: white; color: rgb(67, 67, 67); margin-left: 5px; border-color: rgb(8, 121, 192) gray gray;' />");
    }
    else { }

    if (issubscriberr == true) {
        $('#edit-dis').attr('onclick', 'javascript:alertforsubscriber();');
        $('#edit-dis').css({ 'cursor': "default" });
        $("#edit-dis").disabled = 'disabled';
    }
    else if (taskstatusid == 12 || taskstatusid == 13 || taskstatusid == 14) {

        $('#edit-dis').attr('onclick', null);
        $('#edit-dis').css({ 'cursor': "default" });
        $("#edit-dis").disabled = 'disabled';
        $("#file-dis")[0].style.display = "none";

    }
    else {
        $('#edit-dis').attr('onclick', 'javascript:EditOtherDetails();');
        $('#edit-dis').css({ 'cursor': "pointer" });
        $("#edit-dis").enabled = 'enabled';
        $("#file-dis")[0].style.display = "block";

    }



}
function validateEmail(field) {
    var regex = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,5}$/;
    return (regex.test(field)) ? true : false;
}
function validateMultipleEmailsCommaSeparated(txtvalue) {
    var text = txtvalue;
    if (text != '') {
        var result = text.split(",");
        for (var i = 0; i < result.length; i++) {
            var trimmed = result[i].trim();
            if (trimmed != '') {
                if (!validateEmail(trimmed)) {
                    alert('Invalid Email Id entered.');
                    return false;
                }
            }
        }
    }
    return true;
}


function inserts() {
    var TaskID = s;
    var Message = $("#TxtMessageasana").val();
    if (Message != "") {
        $("#spanforblankposting")[0].style.display = "none";
        $("#spanforposting")[0].style.display = "block";
        $.ajax({
            type: "POST",
            url: "/DataTracker.svc/InsertMessages",
            data: "{\"TaskID\": \"" + TaskID + "\",\"Message\": \"" + encodeURIComponent(escape(Message)) + "\"",
            contentType: "application/json; charset=utf-16",
            dataType: "json",
            success: function (data) {
                var res = data.d.split('~');
                if (res != null) {
                    if (res[0] == "1") {

                        GetTaskcomment(TaskID, 0);
                        $("#TxtMessageasana").attr('value', '');
                        $("#spanforposting")[0].style.display = "none";
                    }
                    else if (res[0] == "0") {
                        $("#spanforposting")[0].style.display = "none";
                    }
                    else if (res[0] == "-1") {
                        $("#spanforposting")[0].style.display = "none";
                    }
                    else if (res[0] == "-3") {
                        $("#spanforposting")[0].style.display = "none";
                        alert("Insert Valid Comment");
                        return false;
                    }
                }
                else {
                    $("#spanforposting")[0].style.display = "none";
                    return false;
                }
            },
            failure: function (response) {
                $("#spanforposting")[0].style.display = "none";
            },
            error: function (xhr, status, error) {
                $("#spanforposting")[0].style.display = "none";
            }
        });
    }
    else {

        $("#spanforblankposting")[0].style.display = "block";
    }
}

function GetTaskcommentfolall(Tid, LastID) {
    $("#showallcomments")[0].innerHTML = "";
    var counterr = 0;
    arrStatus[11] = $.ajax({
        type: "GET",
        url: "/DataTracker.svc/GetAllMessagesList?TaskID=" + Tid + "&LastCommentID=" + LastID + "",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            // var res = data.d;
            if (data.d != null) {
                //  ;
                // $('#tabstripcom').css('display', 'block');
                $.each(data.d, function (data, t) {
                    //  ;
                    counterr++;
                    var arr = t.CommentedDate.split(' ');
                    var tcc = t.totalcmntcount;
                    if (tcc != 0) {
                        var ntcc = tcc - 3;
                        if (counterr == 1) {
                            $("#showallcomments")[0].innerHTML = "";
                            $("#showallcomments").append("<a class='section-name' onclick='javascript:GetTaskcomment(s, 0);'> Hide earlier comments</a>");
                            $("#showallcomments").append("<div class='feed-story big-feed-story feed-story-comment multiline-feed-story'><div class='photo-view photo-view-remix inbox-size photo-view-rounded-corners '><div class='comment-icon'></div></div><div class='comment-content'><span class='feed-story-creator'>" + t.CommentedBy + " :</span><span class='comment-text '><span> <span style='white-space: pre-wrap;'>" + t.Comments.replace(/\n/g, "<br />") + "</span></span></span><div><span class='feed-story-footer'><span class='loading-boundary hidden'><span class='story-timestamp-view'>'" + arr[0] + " " + arr[1] + " " + arr[2] + "'</span></span><span class='story-pin-view'><span class='footer-separator'> • </span><span class='story-pin-icon'><span data-icon='pin' class='pin glyph  prod'></span></span></span></span></div></div></div>");
                        }
                        else
                            $("#showallcomments").append("<div class='feed-story big-feed-story feed-story-comment multiline-feed-story'><div class='photo-view photo-view-remix inbox-size photo-view-rounded-corners '><div class='comment-icon'></div></div><div class='comment-content'><span class='feed-story-creator'>" + t.CommentedBy + " :</span><span class='comment-text '><span> <span style='white-space: pre-wrap;'>" + t.Comments.replace(/\n/g, "<br />") + "</span></span></span><div><span class='feed-story-footer'><span class='loading-boundary hidden'><span class='story-timestamp-view'>'" + arr[0] + " " + arr[1] + " " + arr[2] + "'</span></span><span class='story-pin-view'><span class='footer-separator'> • </span><span class='story-pin-icon'><span data-icon='pin' class='pin glyph  prod'></span></span></span></span></div></div></div>");
                    }



                    $("#TxtMessageasana").attr('value', '');


                });

            }
            else {

            }
        }
    });
}

function GetTaskcomment(Tid, LastID) {
    $("#showallcomments")[0].innerHTML = "";
    var counterr = 0;
    arrStatus[11] = $.ajax({
        type: "GET",
        url: "/DataTracker.svc/GetAllMessagesList?TaskID=" + Tid + "&LastCommentID=" + LastID + "",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            // var res = data.d;
            if (data.d != null) {
                //  ;
                // $('#tabstripcom').css('display', 'block');
                $.each(data.d, function (data, t) {
                    //  ;
                    counterr++;
                    var arr = t.CommentedDate.split(' ');
                    var tcc = t.totalcmntcount;
                    if (tcc != 0) {
                        var ntcc = tcc - 3;
                        if (ntcc >= counterr) {
                            $("#showallcomments")[0].innerHTML = "";
                            $("#showallcomments").append("<a class='section-name' onclick='javascript:GetTaskcommentfolall(s, 0);'>" + ntcc + " more comments</a>");
                        }     // $("#showallcomments").append("<div class='feed-story big-feed-story feed-story-comment multiline-feed-story'><div class='photo-view photo-view-remix inbox-size photo-view-rounded-corners '><div class='comment-icon'></div></div><div class='comment-content'><div class='delete click-target' tabindex='-1'></div><span class='feed-story-creator'><a href='https://app.asana.com/0/39847031540290/39847031540290' class=''>" + t.CommentedBy + "</a></span><span class='comment-text '><span> <span style='white-space: pre-wrap;'>" + t.Comments.replace(/\n/g, "<br />") + "</span></span></span><div><span class='feed-story-footer'><span class='loading-boundary hidden'><span class='story-timestamp-view'>'" + arr[0] + " " + arr[1] + " " + arr[2] + "'</span></span><span class='story-pin-view'><span class='footer-separator'> • </span><span class='story-pin-icon'><span data-icon='pin' class='pin glyph  prod'></span></span></span></span></div></div></div>");
                        else
                            $("#showallcomments").append("<div class='feed-story big-feed-story feed-story-comment multiline-feed-story'><div class='photo-view photo-view-remix inbox-size photo-view-rounded-corners '><div class='comment-icon'></div></div><div class='comment-content'><span class='feed-story-creator'>" + t.CommentedBy + " :</span><span class='comment-text '><span> <span style='white-space: pre-wrap;'>" + t.Comments.replace(/\n/g, "<br />") + "</span></span></span><div><span class='feed-story-footer'><span class='loading-boundary hidden'><span class='story-timestamp-view'>'" + arr[0] + " " + arr[1] + " " + arr[2] + "'</span></span><span class='story-pin-view'><span class='footer-separator'> • </span><span class='story-pin-icon'><span data-icon='pin' class='pin glyph  prod'></span></span></span></span></div></div></div>");
                    }



                    $("#TxtMessageasana").attr('value', '');


                });
                // if (data.d == '')
                //GetTaskcommentSheet(Tid, 'no');
                // else
                // GetTaskcommentSheet(Tid, 'yes');
            }
            else {
                //$("#btnsubmitmsg").attr("value", "Post Comment")
                //  $('#divmain div').empty();
                //GetTaskcommentSheet(Tid, 'no');
            }
        }
    });
}
function ClearAttch() {
    $('#fileToUpload').val('');

}
function FileCount() {
    $("#DivLComents")[0].style.height = "200px";
    $("#divmain")[0].style.height = "150px";
}

function onchanges() {

    // var desc = $("#TxtDesc").val();
    var desc = $("#TxtDesc")[0].innerHTML;
    //var fea = $("#TxtFeas").val();
    //var title = $("#lblTitle").val();
    // var prip = $("#priority");

    var id = s;

    //insert(title, user[0].value, desc, prip[0].value, id)
    insert(desc, id)

}
function insert(desc, taskid) {
    $.ajax({
        type: "POST",
        url: "/DataTracker.svc/UpdateTitle",
        data: "{\"TaskID\": \"" + taskid + "\",\"Desc\": \"" + escape(encodeURIComponent(desc)) + "\"",
        // data: "{\"TaskID\": \"" + taskid + "\",\"TTitle\": \"" + escape(Txttitle) + "\",\"Desp\": \"" + escape(Detail) + "\",\"TDate\": \"" + TargetDate_date + "\",\"StatusID\": \"" + sid + "\",\"CID\": \"" + Cid + "\",\"Owner\": \"" + UserList + "\",\"GroupName\": \"" + escape(GroupName) + "\",\"pri\": \"" + Prit + "\",\"feab\": \"" + escape(feab) + "\",\"commts\": \"" + escape(commts) + "\",\"ViToclient\": \"" + ViToclient + "\",\"pid\": \"" + pid + "\",\"Filelist\": \"" + escape(fname) + "\"",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            var res = data.d;
            if (res != null) {
                SavedMessagePopUp("1")
            }
            else {
                // alert("ff");
                return false;
            }
        },
        failure: function (response) {
            //alert("error");
        },
        error: function (xhr, status, error) {
            // alert("error1");
        }
    });

}

function DeleteImage(taskID, fileID) {
    $("#allattachmentlist")[0].innerHTML = "";
    $.ajax({
        type: "POST",
        url: "/DataTracker.svc/DeleteAttachment",
        data: "{\"TaskID\": \"" + taskID + "\",\"FileID\": \"" + fileID + "\"",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            SavedMessagePopUp(7)
            if ($("#allattachmentlist").length > 0) {

                if (data.d.Attachments != "") {
                    $("#allattachmentlist")[0].innerHTML = data.d.Attachments;
                }
                else {

                    $("#allattachmentlist")[0].innerHTML = "<div style='font-size:13px;margin-left:150px; margin-top:15px'>&nbsp;</div>";
                }
            }


        },
        error: function (data, status, e) {
            alert("Some technical problem occur, please retry!");
        }
    });
}
function ajaxFileUpload(TID, UID) {
    var fileName = "";
    $.ajaxFileUpload
            (
            {
                url: 'AjaxFileUploader.ashx?tid=' + TID + "&Cuid=" + UID,
                secureuri: false,
                fileElementId: 'fileToUpload',
                dataType: 'json',
                data: { name: 'logan', id: 'id' },
                success: function (data, status) {
                    //alert("in success");
                    if (typeof (data.error) != 'undefined') {
                        if (data.error != '') {
                            alert("Some technical problem occur, please retry!");

                        } else {
                            fileName = data.msg;
                            if (fileName != "") {
                                var attch = $("#allattachmentlist")[0].innerHTML;
                                // if (attch.indexOf("No attachments") != -1) {
                                if (attch.indexOf(" ") != -1) {
                                    if (fileName == '0') {
                                        alert("File already exist with the same name");
                                        return false;
                                    }
                                    else {

                                        var dd = data.msg;
                                        dd = dd.replace('@', '');
                                        var fdd = dd.split(',');
                                        if (fdd[6] == ".jpeg" || fdd[6] == ".png" || fdd[6] == ".jpg" || fdd[6] == ".gif" || fdd[6] == ".jif" || fdd[6] == ".jfif" || fdd[6] == ".bmp" || fdd[6] == ".psd" || fdd[6] == ".tif") {
                                            $("#allattachmentlist").append("<div class='attachment'><a style='font-family:Tahoma, Helvetica, sans-serif; float:left;font-weight:normal;font-size:12px;text-decoration:none;margin-left:10px;color:#6D6E6E;padding-top:4px' onclick='SetUrlOfPpImage(\"" + fdd[7] + "\"  );' title=" + fdd[3] + "><label for='luna3' class='property-name'><span class='property-icon-name attach-icon'> <img src='" + fdd[5] + "/" + fdd[4] + "'/>&nbsp;</span></label>&nbsp;" + fdd[2] + "&nbsp;&nbsp;</a><img 0'='' style='display:block;vertical-align:bottom;cursor:pointer;float:left; height:15px;padding-top:4px; border=' title='Delete' onclick='javascript:DeleteImage(" + fdd[0] + "," + fdd[1] + ");' src='/Images/close.png'></div>");
                                        }
                                        else {
                                            if (fdd[4] != "0" && fdd[5] != "0")
                                                $("#allattachmentlist").append("<div class='attachment'><a style='font-family:Tahoma, Helvetica, sans-serif; float:left;font-weight:normal;font-size:12px;text-decoration:none;margin-left:10px;color:#6D6E6E;padding-top:4px' href='/Downloadfiles.aspx?tid=" + fdd[0] + "&fid=" + fdd[1] + "' title=" + fdd[3] + "><label for='luna3' class='property-name'><span class='property-icon-name attach-icon'> <img src='" + fdd[5] + "/" + fdd[4] + "'/>&nbsp;</span></label>&nbsp;" + fdd[2] + "&nbsp;&nbsp;</a><img 0'='' style='display:block;vertical-align:bottom;cursor:pointer;float:left; height:15px;padding-top:4px; border=' title='Delete' onclick='javascript:DeleteImage(" + fdd[0] + "," + fdd[1] + ");' src='/Images/close.png'></div>");
                                            else
                                                $("#allattachmentlist").append("<div class='attachment'><a style='font-family:Tahoma, Helvetica, sans-serif; float:left;font-weight:normal;font-size:12px;text-decoration:none;margin-left:10px;color:#6D6E6E;padding-top:4px' href='/Downloadfiles.aspx?tid=" + fdd[0] + "&fid=" + fdd[1] + "' title=" + fdd[3] + "><label for='luna3' class='property-name'><span class='property-icon-name attach-icon'> <img src='/Images/Icon/NewIcons/Defaulti.png'/>&nbsp;</span></label>&nbsp;" + fdd[2] + "&nbsp;&nbsp;</a><img 0'='' style='display:block;vertical-align:bottom;cursor:pointer;float:left; height:15px;padding-top:4px; border=' title='Delete' onclick='javascript:DeleteImage(" + fdd[0] + "," + fdd[1] + ");' src='/Images/close.png'></div>");
                                            // $("#allattachmentlist").append(fileName);
                                        }

                                    }

                                }
                                else {

                                    var dd = data.msg;
                                    dd = dd.replace('@', '');
                                    var fdd = dd.split(',');
                                    if (fdd[6] == ".jpeg" || fdd[6] == ".png" || fdd[6] == ".jpg" || fdd[6] == ".gif" || fdd[6] == ".jif" || fdd[6] == ".jfif" || fdd[6] == ".bmp" || fdd[6] == ".psd" || fdd[6] == ".tif") {
                                        $("#allattachmentlist").append("<div class='attachment'><a style='font-family:Tahoma, Helvetica, sans-serif; float:left;font-weight:normal;font-size:12px;text-decoration:none;margin-left:10px;color:#6D6E6E;padding-top:4px' onclick='SetUrlOfPpImage(\"" + fdd[7] + "\"  );' title=" + fdd[3] + "><label for='luna3' class='property-name'><span class='property-icon-name attach-icon'> <img src='" + fdd[5] + "/" + fdd[4] + "'/>&nbsp;</span></label>&nbsp;" + fdd[2] + "&nbsp;&nbsp;</a><img 0'='' style='display:block;vertical-align:bottom;cursor:pointer;float:left; height:15px;padding-top:4px; border=' title='Delete' onclick='javascript:DeleteImage(" + fdd[0] + "," + fdd[1] + ");' src='/Images/close.png'></div>");
                                    }
                                    else {
                                        if (fdd[4] != "0" && fdd[5] != "0")
                                            $("#allattachmentlist").append("<div class='attachment'><a style='font-family:Tahoma, Helvetica, sans-serif; float:left;font-weight:normal;font-size:12px;text-decoration:none;margin-left:10px;color:#6D6E6E;padding-top:4px' href='/Downloadfiles.aspx?tid=" + fdd[0] + "&fid=" + fdd[1] + "' title=" + fdd[3] + "><label for='luna3' class='property-name'><span class='property-icon-name attach-icon'> <img src='" + fdd[5] + "/" + fdd[4] + "'/>&nbsp;</span></label>&nbsp;" + fdd[2] + "&nbsp;&nbsp;</a><img 0'='' style='display:block;vertical-align:bottom;cursor:pointer;float:left; height:15px;padding-top:4px; border=' title='Delete' onclick='javascript:DeleteImage(" + fdd[0] + "," + fdd[1] + ");' src='/Images/close.png'></div>");
                                        else
                                            $("#allattachmentlist").append("<div class='attachment'><a style='font-family:Tahoma, Helvetica, sans-serif; float:left;font-weight:normal;font-size:12px;text-decoration:none;margin-left:10px;color:#6D6E6E;padding-top:4px' href='/Downloadfiles.aspx?tid=" + fdd[0] + "&fid=" + fdd[1] + "' title=" + fdd[3] + "><label for='luna3' class='property-name'><span class='property-icon-name attach-icon'> <img src='/Images/Icon/NewIcons/Defaulti.png'/>&nbsp;</span></label>&nbsp;" + fdd[2] + "&nbsp;&nbsp;</a><img 0'='' style='display:block;vertical-align:bottom;cursor:pointer;float:left; height:15px;padding-top:4px; border=' title='Delete' onclick='javascript:DeleteImage(" + fdd[0] + "," + fdd[1] + ");' src='/Images/close.png'></div>");
                                        // $("#allattachmentlist").append(fileName);
                                    }
                                    SavedMessagePopUp(4)
                                    ClearAttch();
                                }
                                $('#fileToUpload').val('');


                            }
                        }
                    }
                },
                error: function (data, status, e) {

                    alert("Some technical problem occur, please retry!");
                }
            }
            )

    return fileName;

}


function ajaxFileUploadforActivityLog(TID, UID) {

    var fileName = "";
    $.ajaxFileUpload
            (
            {
                url: 'AjaxFileUploader.ashx?tid=' + TID + "&Cuid=" + UID,
                secureuri: false,
                fileElementId: 'fileToUploadFromAL',
                dataType: 'json',
                data: { name: 'logan', id: 'id' },
                success: function (data, status) {

                    if (typeof (data.error) != 'undefined') {
                        if (data.error != '') {
                            alert("Some technical problem occur, please retry!");

                        } else {
                            fileName = data.msg;
                            if (fileName != "") {
                                if (fileName == "formaterror") {

                                    $('#fileToUpload').val('');
                                    alert('This File Format is not supported');

                                }
                                else if (fileName == '0') {

                                    $('#fileToUpload').val('');
                                    alert("File already exist with the same name");
                                    return false;



                                }
                                else {

                                    $('#fileToUpload').val('');
                                    SaveActivityLogAttachment(fileName);

                                }


                            }
                        }
                    }
                },
                error: function (data, status, e) {

                    alert("Some technical problem occur, please retry!");
                }
            }
            )

    return fileName;

}



//function ajaxFileUploadforActivityLog1(UID, flag) {

//    var fileName = "";
//    $.ajaxFileUpload
//            (
//            {
//                url: 'AjaxFileUploader.ashx?flag=' + flag + "&Cuid=" + UID,
//                fileElementId: 'file2',
//                dataType: 'json',
//                success: function (data, status) {
//                    if (data.d != 0) {
//                        if (data.msg == 0) {
//                            uploadprofile(UID);
//                            alert('uploaded successfully');
//                        }
//                        else {
//                            alert('This File Format is not supported');
//                        }
//                    }
//                    else {
//                        alert('failed');
//                    }

//                },
//                error: function (data, status, e) {

//                    alert("error in service (kendo script)");
//                }
//            }
//            )

//    return fileName;


//}

function UploadAttach(UID) {
    var TID = s;
    if (TID != null) {
        ajaxFileUpload(TID, UID);
        setTimeout("ClearAttch()", 5000);
        //        if ($("#fileToUpload").length > 0) {
        //            if ($("#fileToUpload").val() != "") {
        //                ajaxFileUpload(TID, UID);
        //                setTimeout("ClearAttch()", 5000);
        //            }
        //            else {
        //                alert("Please select file");
        //            }
        //        }
    }


}
function ClearAttch() {
    $('#fileToUpload').val('');

}

function AutoAllStoreList() {
    //  $("#Droppriority").kendoDropDownList();
    var dictionary = new Array();
    $.ajax({
        type: "GET",
        url: "/datatracker.svc/GetAllStoreLists",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            var res = data.d;
            if (res != null) {
                for (var n = 0; n < data.d.length; n++) {
                    var value = data.d[n].StoreName;
                    dictionary.push(value);
                }
                $("#StoreName").kendoAutoComplete({
                    dataSource: dictionary,
                    filter: "startswith",
                    placeholder: "Select or insert storename...",
                    separator: ","
                });

            }
            else {

            }
        }

    });

}
//function AutoAllUserList() {

//    var dictionary = new Array();
//    $.ajax({
//        type: "GET",
//        url: "/datatracker.svc/getallUserName",
//        contentType: "application/json; charset=utf-8",
//        dataType: "json",
//        success: function (data) {
//            var res = data.d;
//            if (res != null) {
//                for (var n = 0; n < data.d.length; n++) {
//                    var value = data.d[n].UserName;
//                    dictionary.push(value);
//                }
//                $("#AllUserName").kendoAutoComplete({
//                    dataSource: dictionary,
//                    filter: "startswith",
//                    placeholder: "Select or insert user name...",
//                   animation: true,
//                    separator: ","
//                });
//              

//            }
//            else {

//            }
//        }

//    });



//}
var o = 0;
function OtherTaskParentDropDownEditor(container, options) {
    k = 1;
    var isress = options.model.isresponsible;
    if (isress == 'true') {
        alert("You are not able to change the status for subscribed task")
        return false;
    }
    childSID = options.model.TaskID;
    if (options.model.StatusName != 'Completed') {

        OtherTaskComboList(options.field, container)
    }
    else {
        OtherTaskComboList(options.field, container)

    }

}
function slideclose() {
    //k=0;
    l = 0;
}

function OtherTaskComboList(optionsfield, container) {
    $('<input ID="Otherdropparent" required data-text-field="StatusName" data-value-field="StatusName" data-bind="value:' + optionsfield + '"/>')
                        .appendTo(container)
                        .kendoDropDownList({
                            autoBind: false,
                            dataSource: {
                                transport: {
                                    read: {
                                        url: "/DataTracker.svc/GetOtherStatusList"
                                    }
                                },
                                schema: {
                                    data: "d"
                                }
                            },
                            change: OtherTaskParentDroponChange,
                            close: slideclose

                        });

}
function OtherTaskParentDroponChange() {
    l = 0;
    var Statusvalue = $("#Otherdropparent").val();
    var grid = $("#DivOtherTask").data("kendoGrid");
    var selectedrow = $("#DivOtherTask").find("tbody tr.k-state-selected");
    var goods = $('#DivOtherTask').data("kendoGrid").dataItem(selectedrow);
    var goodsjson = goods.toJSON();
    var StoreName = goodsjson.Title;
    var SID = goodsjson.StatusID;
    var Confrm = "";
    if (Statusvalue == "Completed") {
        Confrm = confirm("Are you sure its completed?");
    }
    else if (Statusvalue == "Change Detected") {
        Confrm = confirm("Are you sure change detected?");

    }
    else if (Statusvalue == "Analysis")
    { Confrm = confirm("Are you sure its analysis?"); }
    else if (Statusvalue == "Cancel")
    { Confrm = confirm("Are you sure its Cancel?"); }
    else if (Statusvalue == "Primary Completed")
    { Confrm = confirm("Are you sure its primary completed?"); }
    if (Statusvalue == "Change Detected" || Statusvalue == "Completed" || Statusvalue == "Analysis" || Statusvalue == "Cancel") {
        if (Confrm) {
            if (Statusvalue == "Cancel" || Statusvalue == "Completed")
                OpenWorkItemStatusComment(childSID, Statusvalue);
            else
                ChangeOtherStatus(childSID, Statusvalue);
        }
        else {
            return false;

        }
    }
    else {
        //CommentsOnCompleted(childSID, Statusvalue);
        ChangeOtherStatus(childSID, Statusvalue);

    }


}

function ChangeOtherStatus(ID, Statusval) {
    $.ajax({
        type: "GET",
        url: "/DataTracker.svc/ChangeOtherstatus?TaskID=" + ID + "&StatusID=" + Statusval + "",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            var res = data.d;
            if (res != null) {
                if (res == "1") {
                    SavedMessagePopUp("1")
                }
                else if (res == "-3") {
                    SavedMessagePopUp("-3")
                }
                else if (res == "-4") {
                    SavedMessagePopUp("-4")
                }
                else if (res == "-2") {
                    window.location.href = "/Login.aspx";

                }
                else if (res == "-1") {
                    SavedMessagePopUp("0")

                }
                else if (res == "9") {
                    SavedMessagePopUp("9")

                }
                else if (res == "10") {
                    SavedMessagePopUp("10")

                }
            }
            else {
                SavedMessagePopUp("0");
                return false;
            }
        }
    });
}

//
function EditOtherDetails() {
    AddOtherTask(s, IsCreator)
}



//function ShowOtherStatusReport(title, TargetDate, TType) {

//    $("#StatusReportGrid")[0].innerHTML = "";
//    if ($("#StatusReportGrid").data("kendoGrid") != null) {
//        $('#StatusReportGrid').data().kendoGrid.destroy();
//        $('#StatusReportGrid').empty();
//    }
//    var dataSourceDetail = "";
//    dataSourceDetail = new kendo.data.DataSource({
//        transport: {
//            read: {
//                url: "/DataTracker.svc/GetStatusLogList",
//                data: {

//                    StoreName: escape(title),
//                    StartDate: TargetDate,
//                    CurrentDate: RetrunCurrentDate(),
//                    TaskType: TType

//                }
//            }
//        },
//        schema: {
//            data: "d",
//            total: function (d) {
//                if (d.d.length > 0)
//                    return d.d[0].TotalCount
//            },
//            model: {
//                id: "ID",
//                fields: {
//                    ID: { editable: false, type: "number" },
//                    StMsg: { editable: false, type: "string" },
//                    StausName: { editable: false, type: "string" },
//                    UpdatedBy: { editable: false, type: "string" },
//                    LogTime: { editable: false, type: "string" }


//                }
//            }
//        },
//        pageSize: 100,
//        serverPaging: true

//    });

//    if ($("#StatusReportGrid").length > 0)
//    { $("#StatusReportGrid")[0].innerHTML = ""; }
//    var element = $("#StatusReportGrid").kendoGrid({
//        dataSource: dataSourceDetail,
//        dataBound: function () {

//        },
//        pageable: {
//            batch: true,
//            pageSize: 5,
//            refresh: true,
//            pageSizes: true,
//            input: true
//        },
//        columns: [

//                            { field: "StatusName", title: "Status Name", width: "25%", template: "#=SetCompletedImg(StatusName,StMsg)#" },
//                            {
//                                field: "UpdatedBy",
//                                title: "Updated By",
//                                width: "50%"

//                            },
//                            {
//                                field: "ID",
//                                title: "",
//                                width: "0%"
//                            },
//                            {
//                                field: "LogTime",
//                                title: "Logged Time",
//                                width: "25%"
//                            }

//                        ],
//        editable: false,
//        sortable: true,
//        selectable: true

//    });

//}

function ShowAllOtherTaskDesc(tid) {
    $(document).ready(function () {
        $("#Tooltipwindow")[0].innerHTML = "hell this demo dfgfdfgfdgdfgfddfgdgdgdfgdgdfgdfgdgdgd";
        if ($("#Tooltipwindow").data("kendoWindow") != null) {
            var Tooltipwind = $("#Tooltipwindow").data("kendoWindow");
            Tooltipwind.close();
        }
        var hhhh = "ghjhjhhjjhjhjhjdgdfgg";
        var windowElement = $("#Tooltipwindow").kendoWindow({
            modal: true,
            title: "",
            position: {
                top: 10,
                left: 2
            },
            resizable: false,
            actions: ["Close"],
            draggable: false

        });
        var dialog = $("#Tooltipwindow").data("kendoWindow");
        dialog.open();
        dialog.center();
        return false;
    });

}

function WorkItemStatus(TaskID) {
    //alert(StartDate);
    $("#DivStLogs")[0].innerHTML = "";
    if ($("#DivStLogs").data("kendoGrid") != null) {
        $('#DivStLogs').data().kendoGrid.destroy();
        $('#DivStLogs').empty();
    }
    var dataSourceDetail = "";
    dataSourceDetail = new kendo.data.DataSource({
        transport: {
            read: {
                url: "/DataTracker.svc/GetActivitylistByTaskID",
                data: {

                    //                                        StoreName: escape(StoreName),
                    //                                        StartDate: StartDate,
                    //                                        CurrentDate: RetrunCurrentDate(),
                    //    TaskType: TType
                    TaskID: TaskID


                }
            }
        },
        schema: {
            data: "d",
            total: function (d) {
                if (d.d.length > 0)
                    return d.d[0].TotalCount
            },
            model: {
                id: "ID",
                fields: {
                    ActivityID: { editable: false, type: "number" },
                    ActivityDateTime: { editable: false, type: "string" },
                    Message: { editable: false, type: "string" },
                    UserName: { editable: false, type: "string" }


                }
            }
        },
        pageSize: 100,
        serverPaging: true

    });

    if ($("#DivStLogs").length > 0)
    { $("#DivStLogs")[0].innerHTML = ""; }
    var element = $("#DivStLogs").kendoGrid({
        dataSource: dataSourceDetail,
        // detailInit: AlldetailInit,
        dataBound: function () {
            // this.expandRow(this.tbody.find("tr.k-master-row").first());
        },
        pageable: {
            batch: true,
            pageSize: 5,
            refresh: true,
            pageSizes: true,
            input: true
        },
        columns: [


                            { field: "ActivityID", title: "ActivityID", width: "8%" },
                            {
                                field: "ActivityDateTime",
                                title: "Activity Time",
                                width: "15%"

                            },
                            {
                                field: "Message",
                                title: "Message"

                            },
                            {
                                field: "UserName",
                                title: "UserName",
                                width: "25%"
                            }

        ],
        editable: false,
        sortable: true,
        selectable: true

    });

}

function WorkItemTimeLogs(tid) {
    //alert(StartDate);
    $("#divTimeLog")[0].innerHTML = "";
    if ($("#divTimeLog").data("kendoGrid") != null) {
        $('#divTimeLog').data().kendoGrid.destroy();
        $('#divTimeLog').empty();
    }
    var dataSourceDetail = "";
    dataSourceDetail = new kendo.data.DataSource({
        transport: {
            read: {
                url: "/DataTracker.svc/GetTimeLoggedList",
                data: {
                    tid: tid
                }
            }
        },
        schema: {
            data: "d",
            total: function (d) {
                if (d.d.length > 0)
                    return d.d[0].TotalCount
            },
            model: {
                id: "ID",
                fields: {
                    ID: { editable: false, type: "number" },
                    TaskID: { editable: false, type: "number" },
                    Hour: { editable: false, type: "string" },
                    Comments: { editable: false, type: "string" },
                    LoggedBy: { editable: false, type: "string" },
                    LoggedDate: { editable: false, type: "string" }

                }
            }
        },
        pageSize: 100,
        serverPaging: true

    });

    if ($("#divTimeLog").length > 0)
    { $("#divTimeLog")[0].innerHTML = ""; }
    var element = $("#divTimeLog").kendoGrid({
        dataSource: dataSourceDetail,
        dataBound: function () {

        },
        pageable: {
            batch: true,
            pageSize: 5,
            refresh: true,
            pageSizes: true,
            input: true
        },
        columns: [


                            { field: "Hour", title: "Hour", width: 80 },
                            {
                                field: "Comments",
                                title: "Comments"


                            },
                            {
                                field: "LoggedBy",
                                title: "Logged By",
                                width: 200
                            },
                            {
                                field: "LoggedDate",
                                title: "Logged Time",
                                width: 200
                            }

        ],
        editable: false,
        sortable: true,
        selectable: true

    });

}






function InsertTimeSheet(TaskID, Hour, Comment, Uid) {
    $.ajax({
        type: "POST",
        url: "/DataTracker.svc/InsertTaskTime",
        data: "{\"TaskID\": \"" + TaskID + "\",\"Hour\": \"" + escape(Hour) + "\",\"Comment\": \"" + escape(Comment) + "\",\"Uid\": \"" + Uid + "\"",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            var res = data.d;
            if (res != null) {
                if (res != "0" || res != "-1") {
                    SavedMessagePopUp("1");
                    closeAddlogTime();
                }
                else if (res == "-2") {
                    window.location.href = "/Login.aspx";
                    closeAddlogTime();

                }
                else if (res == "-1") {
                    SavedMessagePopUp("0")
                    closeAddlogTime();

                }
            }
            else {
                SavedMessagePopUp("0");
                closeAddlogTime();
                return false;
            }
        },
        failure: function (response) {

        },
        error: function (xhr, status, error) {

        }
    });
}

function InsertTimlog() {
    //  e.preventDefault();
    $("#DivLogTime")[0].style.display = "block";
    if ($("#DivLogTime").data("kendoWindow") != null) {
        var dialogtest = $("#DivLogTime").data("kendoWindow");
        dialogtest.close();
    }
    if (!$("#DivLogTime").data("kendoWindow")) {
        $("#DivLogTime").kendoWindow({
            modal: true,
            width: "450px",
            title: "Add Time",
            height: "250px",
            position: {
                top: 10,
                left: 2
            },
            resizable: false,
            actions: ["Close"],
            draggable: true
        });
    }
    var dataItem = s;
    if (dataItem != null) {

        $("#TxtLogcomts").val("");
        var numerictextbox = $("#TxtLog").data("kendoNumericTextBox");
        numerictextbox.value("0.5");
        WorkItemID = dataItem.TaskID;
        // alert(WorkItemID);
        $("#DivLogTime").data("kendoWindow").center()
        $("#DivLogTime").data("kendoWindow").open();
        //  AddOtherTask(Sid)

    }

}
function closeAddlogTime() {
    if ($("#DivLogTime").data("kendoWindow") != null) {
        var dialogtest = $("#DivLogTime").data("kendoWindow");
        dialogtest.close();
    }
}
function AddWITimeLog() {
    //alert("in AddWITimeLog");
    var TaskID = s;
    var numerictextbox = $("#TxtLog").data("kendoNumericTextBox");
    var hour = numerictextbox.value();
    var comts = $("#TxtLogcomts").val();
    // alert("TaslID " + TaskID + "Hour :" + hour + " commsts" + comts + "UsersId " + cuid);
    if (hour != "0.00") {
        InsertTimeSheet(TaskID, hour, comts, cuid);
        InsertTimlog1(TaskID, 1);
    }
    else {
        alert("Please enter hour");
        return false;
    }
}

function InsertWITimeLog(sender, args) {
    if (args.keyCode == 13) {
        AddWITimeLog();
    }
}

function AddProjectPopUp() {
    $("#DivProject")[0].style.display = "block";
    if ($("#DivProject").data("kendoWindow") != null) {
        var dialogproject = $("#DivProject").data("kendoWindow");
        dialogproject.close();
    }
    if (!$("#DivProject").data("kendoWindow")) {
        $("#DivProject").kendoWindow({
            modal: true,
            width: "425px",
            title: "Add Project",
            height: "150px",
            position: {
                top: 10,
                left: 2
            },
            resizable: false,
            actions: ["Close"],
            draggable: true
        });
    }
    $("#DivProject").data("kendoWindow").center()
    $("#DivProject").data("kendoWindow").open();



}

function closeAddProjects() {
    if ($("#DivProject").data("kendoWindow") != null) {
        var dialogtest = $("#DivProject").data("kendoWindow");
        dialogtest.close();
    }
}

function AddNewProject(sender, args) {
    if (args.keyCode == 13) {
        InsertProject();
    }
}
function AddNewProjects() {
    InsertProject();
}
function InsertProject() {
    if ($("#TxtProjectName")[0].value == "") {
        alert("Please enter project name");
        return false;
    }
    else {

    }

    $.ajax({
        type: "POST",
        url: "/DataTracker.svc/AddNewProject",
        data: "{\"Pname\": \"" + escape($("#TxtProjectName")[0].value) + "\",\"Uid\": \"" + Uid + "\"",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            var res = data.d;
            if (res != null) {
                if (res != "0" || res != "-1") {
                    SavedMessagePopUp("1");
                    closeAddProjects();
                }
                else if (res == "-2") {
                    window.location.href = "/Login.aspx";
                    closeAddProjects();

                }
                else if (res == "-1") {
                    SavedMessagePopUp("0")
                    closeAddProjects();

                }
            }
            else {
                SavedMessagePopUp("0");
                closeAddProjects();
                return false;
            }
        },
        failure: function (response) {

        },
        error: function (xhr, status, error) {

        }
    });
}

function GetOffcycleNewStAds(date, OffCycleCombo) {
    $("#DivOffcycleNewAdsGrid")[0].innerHTML = "";
    var StatusIds = "";
    if (OffCycleCombo != null) {
        StatusIds = OffCycleCombo;
    }

    var dataSourceStore = "";
    dataSourceStore = new kendo.data.DataSource({
        pageSize: 100,
        serverPaging: true,
        schema: {
            data: "d",
            total: function (d) {
                if (d.d.length > 0)
                    return d.d[0].TotalCount
            },
            model: {
                id: "ID",
                fields: {
                    // GroupName: { editable: false, type: "string" },
                    WebSite: { editable: false, type: "string" },
                    Url: { editable: false, type: "string" },
                    Title: { editable: false, type: "string" },
                    AdDate: { editable: false, type: "string" },
                    StatusID: { editable: false, type: "string" },
                    StatusName: { editable: true, type: "string" },
                    DateDetected: { editable: false, type: "string" },
                    UserName: { editable: false, type: "string" },
                    LastUpdatedTime: { editable: false, type: "string" },
                    TotalZipCode: { editable: false, type: "string" },
                    TotalZipCodeforexport: { editable: false, type: "string" },
                    AdGridType: { editable: false, type: "number" }


                }
            }
        },
        transport: {
            // read: {
            read: function (options) {
                arrStatus[3] = $.ajax({
                    type: "GET",
                    url: "/DataTracker.svc/GetOffCycleAdsVerifyList",
                    dataType: "json",
                    data: {
                        Page: options.data.page,
                        PageSize: options.data.pageSize,
                        skip: options.data.skip,
                        take: options.data.take,
                        CDate: date,
                        IsNew: true,
                        SID: StatusIds
                    },
                    success: function (result) {
                        options.success(result);
                        OffcyleAds1 = true;

                    }

                });

            }
        },

        sortable: {
            mode: "single",
            allowUnsort: false
        }
    });

    // other task date grid

    var element = $("#DivOffcycleNewAdsGrid").kendoGrid({
        dataSource: dataSourceStore,
        dataBound: function () {
            ShowAdDetectedLastUpdatedTime();
        },
        pageable: {
            //  batch: true,
            pageSize: 50,
            refresh: true,
            // resizable: true,
            pageSizes: true,
            input: true
        },

        columns: [



                            {
                                field: "StatusID",
                                title: "",
                                hidden: true,
                                width: 0
                            },
                        {
                            field: "AdGridType",
                            title: "",
                            hidden: true,
                            width: 0
                        },

                            {
                                field: "CurrentDate",
                                title: "",
                                hidden: true,
                                width: 0
                            },
                            {
                                field: "WebSite",
                                title: "WebSite",
                                width: 170
                            },

                              {
                                  field: "Title",
                                  title: "Title",
                                  width: 170


                              },
                               {
                                   field: "AdDate",
                                   title: "AdDate",
                                   resizable: true


                               },
                              {
                                  field: "TotalZipCode",
                                  title: "Count",
                                  width: 95,
                                  template: "<div id='offnew'>#=TotalZipCode#</div>",
                                  hidden: IsClient


                              },
                               {
                                   field: "DateDetected",
                                   title: "Date found",
                                   hidden: true

                               },
                                {
                                    field: "UserName",
                                    title: "Last updated by",
                                    width: 150
                                },
                              {
                                  field: "LastUpdatedTime",
                                  title: "Last Updated Time",
                                  width: 150,
                                  template: "<abbr class='timeago' title='#=LastUpdatedTime#' ID='#=StatusID#'>#=LastUpdatedTime#</abbr>"
                              },
                          { field: "StatusName", title: "Status", width: 140, editor: OffcycleNewParentDropDownEditor, template: "#=SetCompletedImg(StatusName,'')#" },

                          { command: { text: "Log", click: StatusAdDetectedDetails }, title: "", width: 90 },
                           {
                               field: "",
                               title: "",
                               width: 94,
                               template: "<div>#=DownloadData#</div>"

                           },
        ],


        editable: !IsClient,
        sortable: true,
        selectable: true

    });

}
function OffcycleNewParentDropDownEditor(container, options) {
    if (options.model.StatusName != 'Completed') {

        OffcyleNewComboList(options.field, container)
    }
    else {
        OffcyleNewComboList(options.field, container)

    }

}

function OffcyleNewComboList(optionsfield, container) {
    $('<input ID="Offcyledropparent" required data-text-field="StatusName" data-value-field="StatusName" data-bind="value:' + optionsfield + '"/>')
                        .appendTo(container)
                        .kendoDropDownList({
                            autoBind: false,
                            dataSource: {
                                transport: {
                                    read: {
                                        url: "/DataTracker.svc/GetAdDetectedStatusList"
                                    }
                                },
                                schema: {
                                    data: "d"
                                }
                            },
                            change: OffcyleNewDroponChange

                        });

}
function OffcyleNewDroponChange() {

    var Statusvalue = $("#Offcyledropparent").val();
    var grid = $("#DivOffcycleNewAdsGrid").data("kendoGrid");
    var selectedrow = $("#DivOffcycleNewAdsGrid").find("tbody tr.k-state-selected");
    var goods = $('#DivOffcycleNewAdsGrid').data("kendoGrid").dataItem(selectedrow);
    var goodsjson = goods.toJSON();
    var WebSite = goodsjson.WebSite;
    var SID = goodsjson.StatusID;
    var title = goodsjson.Title;
    var AdDate = goodsjson.AdDate;
    var AdGridType = goodsjson.AdGridType;
    var Confrm = "";
    if (Statusvalue == "Completed") {
        Confrm = confirm("Are you sure its completed?");
    }
    else if (Statusvalue == "Change Detected") {
        Confrm = confirm("Are you sure change detected?");

    }
    else if (Statusvalue == "Analysis")
    { Confrm = confirm("Are you sure its analysis?"); }
    else if (Statusvalue == "Primary Completed")
    { Confrm = confirm("Are you sure its primary completed?"); }
    if (Statusvalue == "Change Detected" || Statusvalue == "Completed" || Statusvalue == "Analysis") {
        if (Confrm) {

            ChangeOffCycleStatus(Statusvalue, WebSite, title, AdDate, false, AdGridType);
        }
        else {
            return false;

        }
    }
    else {

        ChangeOffCycleStatus(Statusvalue, WebSite, title, AdDate, false, AdGridType);

    }


}
// old offcycle ads
function GetOffcycleOldStAds(date) {
    $("#DivOffcycleOldDeAdsGrid")[0].innerHTML = "";
    var dataSourceStore = "";
    dataSourceStore = new kendo.data.DataSource({
        pageSize: 100,
        serverPaging: true,
        sortable: {
            mode: "single",
            allowUnsort: false
        },

        schema: {
            data: "d",
            total: function (d) {
                if (d.d.length > 0)
                    return d.d[0].TotalCount
            },
            model: {
                id: "ID",
                fields: {

                    WebSite: { editable: false, type: "string" },
                    Url: { editable: false, type: "string" },
                    Title: { editable: false, type: "string" },
                    AdDate: { editable: false, type: "string" },
                    StatusID: { editable: false, type: "string" },
                    StatusName: { editable: true, type: "string" },
                    DateDetected: { editable: false, type: "string" },
                    UserName: { editable: false, type: "string" },
                    LastUpdatedTime: { editable: false, type: "string" },
                    TotalZipCode: { editable: false, type: "string" },
                    AdGridType: { editable: false, type: "number" },
                    TotalZipCodeforexport: { editable: false, type: "string" }
                }
            }
        },
        transport: {
            read: function (options) {
                arrStatus[7] = $.ajax({
                    type: "GET",
                    url: "/DataTracker.svc/GetOffCycleAdsVerifyList",
                    dataType: "json",
                    data: {
                        Page: options.data.page,
                        PageSize: options.data.pageSize,
                        skip: options.data.skip,
                        take: options.data.take,
                        CDate: date,
                        IsNew: false,
                        SID: "0"
                    },
                    success: function (result) {
                        options.success(result);
                        OffcyleAds2 = true;
                        $("#BtnShowAlreadyads").val("Show Ads");
                        $("#Button3Alreadyads")[0].style.display = "block";

                    }

                });
            }
        }

    });

    var element = $("#DivOffcycleOldDeAdsGrid").kendoGrid({
        dataSource: dataSourceStore,
        dataBound: function () {
            ShowAdDetectedLastUpdatedTime();
        },
        pageable: {

            pageSize: 50,
            refresh: true,
            pageSizes: true,
            input: true
        },

        columns: [



                            {
                                field: "StatusID",
                                title: "",
                                hidden: true,
                                width: 0
                            },
                            {
                                field: "AdGridType",
                                title: "",
                                hidden: true,
                                width: 0
                            },

                            {
                                field: "CurrentDate",
                                title: "",
                                hidden: true,
                                width: 0
                            },

                            {
                                field: "WebSite",
                                title: "WebSite",
                                width: 160
                            },

                              {
                                  field: "Title",
                                  title: "Title",
                                  width: 160

                              },
                               {
                                   field: "AdDate",
                                   title: "AdDate",
                                   resizable: true

                               },
                              {
                                  field: "TotalZipCode",
                                  title: "Count",
                                  width: 95,
                                  template: "<div Id='offalready'>#=TotalZipCode#</div>",
                                  hidden: IsClient


                              },
                               {
                                   field: "DateDetected",
                                   title: "Date found",
                                   width: 80

                               },
                               {
                                   field: "UserName",
                                   title: "Last updated by",
                                   width: 130
                               },
                              {
                                  field: "LastUpdatedTime",
                                  title: "Last Updated Time",
                                  width: 130,
                                  template: "<abbr class='timeago' title='#=LastUpdatedTime#' ID='#=StatusID#'>#=LastUpdatedTime#</abbr>"
                              },
                          { field: "StatusName", title: "Status", width: 130, editor: OffcycleOldParentDropDownEditor, template: "#=SetCompletedImg(StatusName,'')#" },
        // { command: { text: "Status Detail", click: StatusAdDetectedDetails }, title: "", width: 110 }

                        {
                            field: "",
                            title: "",
                            width: 94,
                            template: "<div>#=DownloadData#</div>"

                        },
                        { command: { text: "Log", click: StatusAdDetectedDetails }, title: "", width: 90 }
        ],


        editable: !IsClient,
        sortable: true,
        selectable: true

    });

}
function OffcycleOldParentDropDownEditor(container, options) {
    if (options.model.StatusName != 'Completed') {

        OffcyleOldComboList(options.field, container)
    }
    else {
        OffcyleOldComboList(options.field, container)

    }

}

function OffcyleOldComboList(optionsfield, container) {
    $('<input ID="OffcyleOlddropparent" required data-text-field="StatusName" data-value-field="StatusName" data-bind="value:' + optionsfield + '"/>')
                        .appendTo(container)
                        .kendoDropDownList({
                            autoBind: false,
                            dataSource: {
                                transport: {
                                    read: {
                                        url: "/DataTracker.svc/GetAdDetectedStatusList"
                                    }
                                },
                                schema: {
                                    data: "d"
                                }
                            },
                            change: OffcyleOldDroponChange

                        });

}
function OffcyleOldDroponChange() {

    var Statusvalue = $("#OffcyleOlddropparent").val();
    var grid = $("#DivOffcycleOldDeAdsGrid").data("kendoGrid");
    var selectedrow = $("#DivOffcycleOldDeAdsGrid").find("tbody tr.k-state-selected");
    var goods = $('#DivOffcycleOldDeAdsGrid').data("kendoGrid").dataItem(selectedrow);
    var goodsjson = goods.toJSON();
    var WebSite = goodsjson.WebSite;
    var SID = goodsjson.StatusID;
    var title = goodsjson.Title;
    var AdDate = goodsjson.AdDate;
    var AdGridType = goodsjson.AdGridType;
    var Confrm = "";
    if (Statusvalue == "Completed") {
        Confrm = confirm("Are you sure its completed?");
    }
    else if (Statusvalue == "Change Detected") {
        Confrm = confirm("Are you sure change detected?");

    }
    else if (Statusvalue == "Analysis")
    { Confrm = confirm("Are you sure its analysis?"); }
    else if (Statusvalue == "Primary Completed")
    { Confrm = confirm("Are you sure its primary completed?"); }
    if (Statusvalue == "Change Detected" || Statusvalue == "Completed" || Statusvalue == "Analysis") {
        if (Confrm) {

            ChangeOffCycleStatus(Statusvalue, WebSite, title, AdDate, false, AdGridType);
        }
        else {
            return false;

        }
    }
    else {

        ChangeOffCycleStatus(Statusvalue, WebSite, title, AdDate, false, AdGridType);

    }


}
function ChangeOffCycleStatus(Statusvalue, WebSite, Title, AdDate, UnProcessed, AdGridType) {
    var CDate = $("#DivOffCal").val();
    $.ajax({
        type: "POST",
        url: "/DataTracker.svc/ChangeOffcylestatus",
        data: "{\"StatusName\": \"" + escape(Statusvalue) + "\",\"WebSite\": \"" + WebSite + "\",\"Title\": \"" + escape(Title) + "\",\"AdDate\": \"" + AdDate + "\",\"CDate\": \"" + CDate + "\",\"UnProcessed\": \"" + UnProcessed + "\",\"AdGridType\": \"" + AdGridType + "\"",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            var res = data.d;
            if (res != null) {
                if (res == "1") {
                    SavedMessagePopUp("1")
                }
                else if (res == "-2") {
                    window.location.href = "/Login.aspx";

                }
                else if (res == "-1") {
                    SavedMessagePopUp("0")

                }
            }
            else {
                SavedMessagePopUp("0");
                return false;
            }
        }
    });
}
//un processed ads
function GetOffcycleUnprocessedAds(date) {
    debugger;
    $("#DivUnprocessAdsGrid")[0].innerHTML = "";
    var dataSourceStore = "";
    dataSourceStore = new kendo.data.DataSource({
        serverPaging: true,
        sortable: {
            mode: "single",
            allowUnsort: false
        },
        pageSize: 100,
        schema: {
            data: "d",
            total: function (d) {
                if (d.d.length > 0)
                    return d.d[0].TotalCount
            },
            model: {
                id: "ID",
                fields: {

                    WebSite: { editable: false, type: "string" },
                    Title: { editable: false, type: "string" },
                    AdDate: { editable: false, type: "string" },
                    StatusID: { editable: false, type: "string" },
                    StatusName: { editable: true, type: "string" },
                    DateDetected: { editable: false, type: "string" },
                    UserName: { editable: false, type: "string" },
                    LastUpdatedTime: { editable: false, type: "string" },
                    TotalZipCode: { editable: false, type: "string" },
                    TotalZipCodeforexport: { editable: false, type: "string" },
                    AdGridType: { editable: false, type: "number" }

                }
            }
        },
        transport: {
            read: function (options) {
                //arrStatus[4] = $.ajax({
                $.ajax({                        //by me
                    type: "GET",
                    url: "/DataTracker.svc/GetOffcycleUnProcessedAdsList",
                    dataType: "json",
                    data: {
                        Page: options.data.page,
                        PageSize: options.data.pageSize,
                        skip: options.data.skip,
                        take: options.data.take,
                        CDate: date
                    },
                    success: function (result) {
                        options.success(result);
                        OffcyleAds3 = true;

                    }

                });

            }
        }
    });

    var element = $("#DivUnprocessAdsGrid").kendoGrid({
        dataSource: dataSourceStore,
        dataBound: function () {
            ShowAdDetectedLastUpdatedTime();
        },
        pageable: {

            pageSize: 50,
            refresh: true,
            pageSizes: true,
            input: true
        },

        columns: [



                            {
                                field: "StatusID",
                                title: "",
                                hidden: true,
                                width: 0
                            },
                            {
                                field: "AdGridType",
                                title: "",
                                hidden: true,
                                width: 0
                            },

                            {
                                field: "CurrentDate",
                                title: "",
                                hidden: true,
                                width: 0
                            },

                            {
                                field: "WebSite",
                                title: "WebSite",
                                width: 160
                            },

                              {
                                  field: "Title",
                                  title: "Title",
                                  width: 160

                              },
                               {
                                   field: "AdDate",
                                   title: "AdDate",
                                   resizable: true

                               },
                               {
                                   field: "TotalZipCode",
                                   title: "Count",
                                   width: 95,
                                   template: "<div>#=TotalZipCode#</div>",
                                   hidden: IsClient


                               },
                               {
                                   field: "DateDetected",
                                   title: "Date found",
                                   width: 80

                               },

                               {
                                   field: "UserName",
                                   title: "Last updated by",
                                   width: 130
                               },
                              {
                                  field: "LastUpdatedTime",
                                  title: "Last Updated Time",
                                  width: 130,
                                  template: "<abbr class='timeago' title='#=LastUpdatedTime#' ID='#=StatusID#'>#=LastUpdatedTime#</abbr>"
                              },
                          { field: "StatusName", title: "Status", width: 130, editor: OffcycleUnProParentDropDownEditor, template: "#=SetCompletedImg(StatusName,'')#" },
        //                          {
        //                              field: "LastUpdatedTime",
        //                              title: "Last Updated Time",
        //                              width: 130,
        //                              template: "<abbr class='timeago' title='#=LastUpdatedTime#' ID='#=StatusID#'>#=LastUpdatedTime#</abbr>"
        //                          },
        // { command: { text: "Status Detail", click: StatusAdDetectedDetails }, title: "", width: 110 }
        // {command: [{ text: "Log", click: StatusAdDetectedDetails}, { text: "Download", click: StatusAdDetectedDetails }], title: "", width: 175 }
                                 { command: [{ text: "Log", click: StatusAdDetectedDetails }], title: "", width: 90 }

        ],


        editable: !IsClient,
        sortable: true,
        selectable: true

    });

}

function OffcycleUnProParentDropDownEditor(container, options) {
    if (options.model.StatusName != 'Completed') {

        OffcyleUnProComboList(options.field, container)
    }
    else {
        OffcyleUnProComboList(options.field, container)

    }

}

function OffcyleUnProComboList(optionsfield, container) {
    $('<input ID="OffcyleUnProdropparent" required data-text-field="StatusName" data-value-field="StatusName" data-bind="value:' + optionsfield + '"/>')
                        .appendTo(container)
                        .kendoDropDownList({
                            autoBind: false,
                            dataSource: {
                                transport: {
                                    read: {
                                        url: "/DataTracker.svc/GetAdDetectedStatusList"
                                    }
                                },
                                schema: {
                                    data: "d"
                                }
                            },
                            change: OffcyleUpDroponChange

                        });

}
function OffcyleUpDroponChange() {

    var Statusvalue = $("#OffcyleUnProdropparent").val();
    var grid = $("#DivUnprocessAdsGrid").data("kendoGrid");
    var selectedrow = $("#DivUnprocessAdsGrid").find("tbody tr.k-state-selected");
    var goods = $('#DivUnprocessAdsGrid').data("kendoGrid").dataItem(selectedrow);
    var goodsjson = goods.toJSON();
    var WebSite = goodsjson.WebSite;
    var SID = goodsjson.StatusID;
    var title = goodsjson.Title;
    var AdDate = goodsjson.AdDate;
    var AdGridType = goodsjson.AdGridType;
    var Confrm = "";
    if (Statusvalue == "Completed") {
        Confrm = confirm("Are you sure its completed?");
    }
    else if (Statusvalue == "Change Detected") {
        Confrm = confirm("Are you sure change detected?");

    }
    else if (Statusvalue == "Analysis")
    { Confrm = confirm("Are you sure its analysis?"); }
    else if (Statusvalue == "Primary Completed")
    { Confrm = confirm("Are you sure its primary completed?"); }
    if (Statusvalue == "Change Detected" || Statusvalue == "Completed" || Statusvalue == "Analysis") {
        if (Confrm) {

            ChangeOffCycleStatus(Statusvalue, WebSite, title, AdDate, true, AdGridType);
        }
        else {
            return false;

        }
    }
    else {

        ChangeOffCycleStatus(Statusvalue, WebSite, title, AdDate, true, AdGridType);

    }


}

function OffcycleNoAdsDetected(Date) {
    //  ;
    var dataSourceStore = "";
    dataSourceStore = new kendo.data.DataSource({
        serverPaging: true,
        pageSize: 100,
        sortable: {
            mode: "single",
            allowUnsort: false
        },

        schema: {
            data: "d",
            total: function (d) {
                if (d.d.length > 0)
                    return d.d[0].TotalCount

            },
            model: {
                id: "ID",
                fields: {

                    WebSite: { editable: false, type: "string" }

                }
            }
        },
        transport: {
            read: function (options) {
                //arrStatus[5] = $.ajax({
                $.ajax({                          //by me
                    type: "GET",
                    url: "/DataTracker.svc/GetOffcyleNoAdsWebSiteList",
                    dataType: "json",
                    data: {
                        Page: options.data.page,
                        PageSize: options.data.pageSize,
                        skip: options.data.skip,
                        take: options.data.take,
                        CDate: Date
                    },
                    success: function (result) {
                        options.success(result);
                        OffcyleAds4 = true;

                    }

                });

            }
        }

    });


    var element = $("#DivOffNoAdsDe").kendoGrid({
        dataSource: dataSourceStore,

        dataBound: function () {
            TotalNoAdsWebsiteCount();
        },

        pageable: {
            pageSize: 30,
            refresh: true,
            pageSizes: true,
            input: true
        },

        columns: [

                            {
                                field: "WebSite",
                                title: "WebSite",
                                width: "70%"
                            }

        ],


        editable: !IsClient,
        sortable: true,
        selectable: true

    });

}
function ShowNoAdsOffcycle() {

    $("#DivTotalnoAdsoff")[0].style.display = "block";
    if ($("#DivTotalnoAdsoff").data("kendoWindow") != null) {
        var dialogtest = $("#DivTotalnoAdsoff").data("kendoWindow");
        dialogtest.close();
    }
    if (!$("#DivTotalnoAdsoff").data("kendoWindow")) {
        $("#DivTotalnoAdsoff").kendoWindow({
            modal: true,
            width: "530px",
            title: "No ads found web site list",
            height: "450px",
            position: {
                top: 10,
                left: 2
            },
            resizable: false,
            actions: ["Close"],
            draggable: true
        });
    }

    $("#DivTotalnoAdsoff").data("kendoWindow").center()
    $("#DivTotalnoAdsoff").data("kendoWindow").open();


}
function TotalNoAdsWebsiteCount() {
    var grid = $("#DivOffNoAdsDe").data("kendoGrid");
    var count = grid.dataSource.total();
    $("#DivNoAdsFound")[0].innerHTML = "<a  onclick='javascript:ShowNoAdsOffcycle();' style='font-weight:bold; font-size:18px; color:#218ED2; text-decoration:underline;cursor:pointer'>No ads found for " + count + " websites</a>";
    $("#DivScreenShot")[0].innerHTML = "<a  onclick='javascript:ShowScreenShot();' style='font-weight:bold; font-size:18px; color:#218ED2; text-decoration:underline;cursor:pointer'>ScreenShots</a>";


    // $("#DivNoAdsFound")[0].innerHTML ="<h5>hello</h5>"; //"<a style='font-weight:bold; font-size:18px; color:#218ED2; text-decoration:underline;cursor:pointer'>No ads found for websites</a>";
    // $("#DivScreenShot")[0].innerHTML = "<h5>hello Hi</h5>"; //"<a style='font-weight:bold; font-size:18px; color:#218ED2; text-decoration:underline;cursor:pointer'>ScreenShots</a>";
}
function ShowScreenShot() {

    ClearScreenShotDrop();
    $("#BtnPrescreen")[0].style.display = "none";
    $("#btnNextscreen")[0].style.display = "none";
    $("#DivScreenshotPopup")[0].style.display = "block";
    var datepicker = $("#TxtScreentcal").data("kendoDatePicker");
    datepicker.value($("#DivOffCal").val());

    if ($("#DivScreenshotPopup").data("kendoWindow") != null) {
        var dialogtest = $("#DivScreenshotPopup").data("kendoWindow");
        dialogtest.close();
    }
    if (!$("#DivScreenshotPopup").data("kendoWindow")) {
        $("#DivScreenshotPopup").kendoWindow({
            modal: true,
            width: "95%",
            title: "ScreenShots",
            height: "80%",
            position: {
                top: 10,
                left: 2
            },
            resizable: false,
            actions: ["Close"],
            draggable: true
        });
    }

    $("#DivScreenshotPopup").data("kendoWindow").center()
    $("#DivScreenshotPopup").data("kendoWindow").open();

}
function OnClientItemsRequesting(sender, eventArgs) {
    var context = eventArgs.get_context();
    context["FilterString"] = eventArgs.get_text();
    context["Cdate"] = $("#DivOffCal").val();
}

function ShowAdDetectedLastUpdatedTime() {

    for (var i = 0; i < $(".timeago").length; i++) {
        if (($.timeago($(".timeago")[i]).indexOf("NaN")) == "-1") {
            var stime = $(".timeago")[i].title;
            var StID = $(".timeago")[i].id;
            $(".timeago")[i].innerHTML = $.timeago($(".timeago")[i]);

        }

    }

}

function StatusAdDetectedDetails(e) {
    e.preventDefault();
    if ($("#StatusReportwindow").data("kendoWindow") != null) {
        var StatusReportwindow = $("#StatusReportwindow").data("kendoWindow");
        StatusReportwindow.close();
    }
    var dataItem = this.dataItem($(e.currentTarget).closest("tr"));
    if (dataItem != null) {
        var siteName = dataItem.WebSite;
        var title = dataItem.Title;
        var Addate = dataItem.AdDate;
        var CurrentDate = dataItem.CurrentDate;
        OpenAdDetectedStatusPopup(siteName, title, Addate, CurrentDate);

    }

}

function OpenAdDetectedStatusPopup(siteName, title, Addate, CurrentDate) {
    if ($("#DivAdDestatusLog").data("DivAdDestatusLog") != null) {
        var StatusReport = $("#DivAdDestatusLog").data("kendoWindow");
        StatusReport.close();
    }
    if (Addate == "" || Addate == null) {
        var Title = "Status Report (" + siteName + ")";
    }
    else {
        var Title = "Status Report (" + siteName + ", " + Addate + ")";
    }
    var windowElement = $("#DivAdDestatusLog").kendoWindow({
        iframe: true,
        modal: true,
        width: "50%",
        height: "450px",
        position: {
            top: 10,
            left: 2
        },
        resizable: false,
        actions: ["Close"],
        draggable: true

    });
    var dialog = $("#DivAdDestatusLog").data("kendoWindow");
    dialog.setOptions({
        title: Title
    });
    dialog.open();
    dialog.center();
    ShowAdsDetectedsReport(siteName, title, Addate, CurrentDate);

}
function ShowAdsDetectedsReport(siteName, title, Addate, CurrentDate) {

    $("#AdsdeStatusReportGrid")[0].innerHTML = "";
    if ($("#AdsdeStatusReportGrid").data("kendoGrid") != null) {
        $('#AdsdeStatusReportGrid').data().kendoGrid.destroy();
        $('#AdsdeStatusReportGrid').empty();
    }
    var dataSourceDetail = "";
    dataSourceDetail = new kendo.data.DataSource({

        pageSize: 100,
        serverPaging: true,
        schema: {
            data: "d",
            total: function (d) {
                if (d.d.length > 0)
                    return d.d[0].TotalCount
            },
            model: {
                id: "ID",
                fields: {
                    ID: { editable: false, type: "number" },
                    StMsg: { editable: false, type: "string" },
                    StausName: { editable: false, type: "string" },
                    UpdatedBy: { editable: false, type: "string" },
                    LogTime: { editable: false, type: "string" }


                }
            }
        },
        transport: {
            read: function (options) {
                arrStatus[6] = $.ajax({
                    type: "GET",
                    url: "/DataTracker.svc/GetAdsDetectedStatusLogLists",
                    dataType: "json",
                    data: {
                        Page: options.data.page,
                        PageSize: options.data.pageSize,
                        skip: options.data.skip,
                        take: options.data.take,
                        Website: siteName,
                        Title: title,
                        AdDates: Addate,
                        Cdate: CurrentDate
                    },
                    success: function (result) {
                        options.success(result);
                        // OffcyleAds = true;

                    }

                });

            }
        }


    });

    if ($("#AdsdeStatusReportGrid").length > 0)
    { $("#AdsdeStatusReportGrid")[0].innerHTML = ""; }
    var element = $("#AdsdeStatusReportGrid").kendoGrid({
        dataSource: dataSourceDetail,
        dataBound: function () {

        },
        pageable: {
            batch: true,
            pageSize: 5,
            refresh: true,
            pageSizes: true,
            input: true
        },
        columns: [

                            { field: "StatusName", title: "Status Name", width: "40%", template: "#=SetCompletedImg(StatusName,'')#" },
                            {
                                field: "UpdatedBy",
                                title: "Updated By",
                                width: "35%"

                            },
                            {
                                field: "ID",
                                title: "",
                                width: "0%"
                            },
                            {
                                field: "LogTime",
                                title: "Logged Time",
                                width: "25%"
                            }

        ],
        editable: false,
        sortable: true,
        selectable: true

    });
}

function ShowScreenShotByWebSite(RadComBoWebSite, WebSite, StoreIDZipAddress, Cdate, IsNext) {

    if ($("#Btnshoe").length > 0) {
        $("#Btnshoe").val("Loading ...");
    }
    var ds = new Date($("#TxtScreentcal").val());
    var curr_day1 = ds.getDate();
    if (curr_day1 < 10) {
        curr_day1 = '0' + curr_day1;
    }
    var curr_month1 = ('0' + (ds.getMonth() + 1)).slice(-2)
    var curr_year1 = ds.getFullYear();
    var Due_date = curr_day1 + "/" + curr_month1 + "/" + curr_year1;

    $.ajax({
        type: "POST",
        url: "/DataTracker.svc/GetScreenShot",
        data: "{\"Website\": \"" + escape(WebSite) + "\",\"StoreIDZipAddress\": \"" + escape(StoreIDZipAddress) + "\",\"Cdate\": \"" + Due_date + "\",\"IsNext\": \"" + IsNext + "\"",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            var res = data.d;
            if (res != null) {
                if (res != "") {
                    var splitval = res.split('~');
                    $("#DivScreeImglist")[0].style.display = "block";
                    // $("#ImgscreenShot")[0].style.display = "block";
                    $("#BtnPrescreen")[0].style.display = "block";
                    $("#btnNextscreen")[0].style.display = "block";
                    var SplitWebsite = splitval[0].split('_');
                    if (SplitWebsite[1] != "No") {
                        $("#DivScreenShotTime")[0].style.display = "block";
                        $("#DivScreenShotTime")[0].innerHTML = "Capture Time: " + SplitWebsite[1];
                    }
                    else
                        $("#DivScreenShotTime")[0].style.display = "none";
                    if (SplitWebsite[0] != "0" && SplitWebsite[0] != "") {

                        RadComBoWebSite.trackChanges();
                        RadComBoWebSite.set_value(SplitWebsite[0]);
                        RadComBoWebSite.set_text(SplitWebsite[0]);
                        RadComBoWebSite.commitChanges();

                    }
                    if (splitval[1] != "-1") {
                        $("#DivInvalidUrl")[0].style.display = "none";
                        $("#ImgscreenShot")[0].style.display = "block";
                        $("#ImgscreenShot")[0].src = splitval[1];
                        //  $("#DivScreenShotTime")[0].innerHTML = splitval[2]; 

                    }
                    else {
                        $("#ImgscreenShot")[0].style.display = "none";
                        $("#DivInvalidUrl")[0].style.display = "block";
                        $("#DivInvalidUrl")[0].innerHTML = "Screenshot not found";
                        // $("#DivScreenShotTime")[0].style.display = "none";

                    }
                    $("#Btnshoe").val("Show Screenshot");
                }
                else {
                    // $("#ImgscreenShot")[0].src = "";
                    $("#DivInvalidUrl")[0].style.display = "block";
                    $("#DivInvalidUrl")[0].innerHTML = "No ScreenShot";
                    $("#DivScreeImglist")[0].style.display = "none";
                    $("#Btnshoe").val("Show Screenshot");
                }

            }
            else {
                $("#Btnshoe").val("Show Screenshot");
                $("#DivScreeImglist")[0].style.display = "none";
                $("#ImgscreenShot")[0].src = "";
            }
        }
    });
}

function OnWebsiteSelectedIndexchanged(sender, eventArgs) {
    // $("#ImgscreenShot")[0].src = "";
    $("#ImgscreenShot")[0].style.display = "none";
    $("#DivScreeImglist")[0].style.display = "none";
    $("#DivScreenShotTime")[0].innerHTML = "";
    $("#BtnPrescreen")[0].style.display = "none";
    $("#btnNextscreen")[0].style.display = "none";
    LoadWebsiteZipStoreID(sender);


}
function ShowNextPreScreenShot(RadComBoWebSite, WebSite, StoreIDZipAddress, Cdate, IsNext) {

    ShowScreenShotByWebSite(RadComBoWebSite, WebSite, StoreIDZipAddress, Cdate, IsNext);
}

function LoadStoreIdZipAddress(storecombo, item, Cdate) {

    $.ajax({
        type: "GET",
        url: "/DataTracker.svc/GetZipStoreIsByWebsite?WebSite=" + item.get_value() + "&Cdate=" + Cdate + "",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            var res = data.d;
            if (res != null) {

                storecombo.clearItems();
                storecombo.trackChanges();
                for (var i = 0; i < data.d.length; i++) {
                    var comboItem = new Telerik.Web.UI.RadComboBoxItem();
                    comboItem.set_text(data.d[i].Address);
                    comboItem.set_value(data.d[i].Address);
                    storecombo.get_items().add(comboItem);
                }
                storecombo.commitChanges();
                storecombo.set_emptyMessage("---Search storeid or address or zip---");
            }
            else {

            }
        }
    });

}

var curentdate
var endddate
function SetPriorityColor(prio, edate, stid, isress) {
    //  ;
    // alert(isress);
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth() + 1;
    var yyyy = today.getFullYear();

    var split = edate.split('/');
    var edatemm = split[0];
    var edatedd = split[1];
    var edateyy = split[2];
    if (edate != "") {
        var yearrr = edateyy.split(' ');
        var yyyyyy = yearrr[0];
        var endate = edatemm + '/' + edatedd + '/' + yyyyyy;

        var today = mm + '/' + dd + '/' + yyyy + '';
        curentdate = new Date(today);
        endddate = new Date(endate);

        //        if (isress == 'true') {
        //            return prio;
        //        }
        if (curentdate.getTime() == endddate.getTime()) {

            return "<span style='color:blue'>" + prio + "</span>";
        }
        else if ((stid == ComValue) || (stid == Onhold) || (stid == Cacelvaue)) {

            return prio;
        }
        else if (curentdate > endddate) {

            return "<span style='color:red'>" + prio + "</span>";
        }
        else {
            return prio;
        }
    }

    else {
        return prio;
    }

}
//function statuscombind(statuscomid) {
//    abortRequests();
//    if (statuscomid != null) {
//        arrStatus[8] = $.ajax({
//            type: "GET",
//            url: "/DataTracker.svc/GetOtherStatusListcom",
//            contentType: "application/json; charset=utf-8",
//            dataType: "json",
//            success: function (data) {
//                var res = data.d;
//                if (res != null) {

//                    statuscomid.clearItems();
//                    statuscomid.trackChanges();
//                    for (var i = 0; i < data.d.length; i++) {
//                        var comboItem = new Telerik.Web.UI.RadComboBoxItem();
//                        comboItem.set_text(data.d[i].StatusName);
//                        comboItem.set_value(data.d[i].StatusID);
//                        statuscomid.get_items().add(comboItem);
//                    }
//                    statuscomid.commitChanges();
//                    for (var i = 0; i < statuscomid.get_items().get_count(); i++) {
//                        if (statuscomid.get_items().getItem(i).get_value() != ComValue && statuscomid.get_items().getItem(i).get_value() != Onhold && statuscomid.get_items().getItem(i).get_value() != Cacelvaue) {

//                            statuscomid.get_items().getItem(i).set_checked(true);
//                            //break;
//                        }
//                    }
//                }
//                else {


//                }

//                GetAllWorkItem();

//            }
//        });
//    }
//}
//
function storecombind(storecomid) {

    if (storecomid != null) {
        $.ajax({
            type: "GET",
            url: "/DataTracker.svc/GetAllStoreNameList",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                var res = data.d;
                if (res != null) {

                    storecomid.clearItems();
                    storecomid.trackChanges();
                    for (var i = 0; i < data.d.length; i++) {
                        var comboItem = new Telerik.Web.UI.RadComboBoxItem();
                        comboItem.set_text(data.d[i].StoreName);
                        comboItem.set_value(data.d[i].StoreName);
                        storecomid.get_items().add(comboItem);
                    }
                    storecomid.set_emptyMessage("Filter by store name ...");
                }
                else {
                    storecomid.Enabled = false;

                }
            }
        });
    }
}
//
function monthcombind(monthcomid) {
    var today = new Date();
    var mm = today.getMonth() + 1;
    var yyyy = today.getFullYear();
    var today = mm + '~' + yyyy;
    abortRequests();
    //        var curentdate = new Date(today);
    if (monthcomid != null) {
        arrStatus[1] = $.ajax({
            type: "GET",
            url: "/DataTracker.svc/BindMonthScrapCom",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                var res = data.d;
                if (res != null) {

                    monthcomid.clearItems();
                    monthcomid.trackChanges();
                    for (var i = 0; i < data.d.length; i++) {
                        var comboItem = new Telerik.Web.UI.RadComboBoxItem();
                        comboItem.set_text(data.d[i].monthnamee);
                        comboItem.set_value(data.d[i].monthvalue);
                        monthcomid.get_items().add(comboItem);

                    }
                    // calbindstatusscrapcom();byme
                    statusscrapcombindBoot();

                }
                else {
                    //                    monthcomid.Enabled = false;

                }
                var item = monthcomid.findItemByValue(today);
                if (item != null) {
                    item.select();
                }
            }
        });
    }
}
//
function statusscrapcombind(statusscarpcomid) {

    if (statusscarpcomid != null) {
        $.ajax({
            type: "GET",
            url: "/DataTracker.svc/BindStatusScrapCom",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                var res = data.d;
                if (res != null) {

                    statusscarpcomid.clearItems();
                    statusscarpcomid.trackChanges();
                    for (var i = 0; i < data.d.length; i++) {
                        var comboItem = new Telerik.Web.UI.RadComboBoxItem();
                        comboItem.set_text(data.d[i].Status);
                        comboItem.set_value(data.d[i].StatusID);
                        statusscarpcomid.get_items().add(comboItem);
                    }
                    statusscarpcomid.commitChanges();
                    for (var i = 0; i < statusscarpcomid.get_items().get_count() ; i++) {
                        if (statusscarpcomid.get_items().getItem(i).get_value() != ComValue && statusscarpcomid.get_items().getItem(i).get_value() != Onhold && statusscarpcomid.get_items().getItem(i).get_value() != Cacelvaue) {

                            statusscarpcomid.get_items().getItem(i).set_checked(true);
                            //break;
                        }
                    }

                    ShowReg();

                }
                else {


                }
            }
        });
    }
}


function ShowDetectedStoreDetails(website, title, addate, Cdate, oIsDetected, oStatusID) {
    $("#DivOffdeStoreDetails")[0].style.display = "block";
    if ($("#DivOffdeStoreDetails").data("DivOffdeStoreDetails") != null) {
        var StatusReport = $("#DivOffdeStoreDetails").data("kendoWindow");
        StatusReport.close();
    }
    if (addate == "" || addate == null) {
        var Title = "Store details (" + website + ")";
    }
    else {
        var Title = "Store details (" + website + ", " + addate + ")";
    }

    var windowElement = $("#DivOffdeStoreDetails").kendoWindow({
        iframe: true,
        modal: true,
        width: "50%",
        // title: "Store details",
        height: "450px",
        position: {
            top: 10,
            left: 2
        },
        resizable: false,
        actions: ["Close"],
        draggable: true

    });
    var dialog = $("#DivOffdeStoreDetails").data("kendoWindow");
    dialog.setOptions({
        title: Title
    });

    dialog.open();
    dialog.center();
    ShowoffCycleStoreDetectedDetails(website, title, addate, Cdate, oIsDetected, oStatusID);

}
function ShowoffCycleStoreDetectedDetails(website, title, addate, Cdate, oIsDetected, oStatusID) {

    $("#DivOffdetectedStGrid")[0].innerHTML = "";
    if ($("#DivOffdetectedStGrid").data("kendoGrid") != null) {
        $('#DivOffdetectedStGrid').data().kendoGrid.destroy();
        $('#DivOffdetectedStGrid').empty();
    }
    var dataSourceDetail = "";
    dataSourceDetail = new kendo.data.DataSource({
        transport: {
            read: {
                url: "/DataTracker.svc/GetAdsDetectedStoreLists",
                data: {

                    Website: website,
                    Title: title,
                    AdDates: addate,
                    Cdate: Cdate,
                    IsDetected: oIsDetected,
                    StatusID: oStatusID

                }
            }
        },
        schema: {
            data: "d",
            total: function (d) {
                if (d.d.length > 0)
                    return d.d[0].TotalCount
            },
            model: {
                id: "ID",
                fields: {

                    StoreID: { editable: false, type: "string" },
                    ZipCode: { editable: false, type: "string" },
                    Address: { editable: false, type: "string" }



                }
            }
        },
        pageSize: 100,
        serverPaging: true

    });

    if ($("#DivOffdetectedStGrid").length > 0)
    { $("#DivOffdetectedStGrid")[0].innerHTML = ""; }
    var element = $("#DivOffdetectedStGrid").kendoGrid({
        dataSource: dataSourceDetail,
        dataBound: function () {

        },
        pageable: {
            batch: true,
            pageSize: 5,
            refresh: true,
            pageSizes: true,
            input: true
        },
        columns: [

                            {
                                field: "StoreID",
                                title: "Store ID",
                                width: 100
                            },
                            {
                                field: "ZipCode",
                                title: "ZipCode",
                                width: 100

                            },

                            {
                                field: "Address",
                                title: "Address",
                                resizable: true

                            }

        ],
        editable: false,
        sortable: true,
        selectable: true

    });
}

function Titlein() {
    document.getElementById("TxtTitle").style.border = "1px solid grey";
}
function Crein() {
    document.getElementById("Txtcreated").style.border = "1px solid grey";
}
function Creout() {
    document.getElementById("TxtTitle").style.border = "none";
}
function Titleout() {
    document.getElementById("TxtTitle").style.border = "none";
}
function Descin() {
    document.getElementById("TxtDesc").style.border = "1px solid grey";
}
function Descout() {
    document.getElementById("TxtDesc").style.border = "none";
}
function Ownin() {
    document.getElementById("TxtOwner").style.border = "1px solid grey";
}
function Ownout() {
    document.getElementById("TxtOwner").style.border = "none";
}
function Feain() {
    document.getElementById("lblTitle").style.border = "1px solid grey";
}
function Feaout() {
    document.getElementById("lblTitle").style.border = "none";
}
function StoreNameIn() {
    document.getElementById("StoreName").style.border = "1px solid grey";
}
function StoreNameOut() {
    document.getElementById("StoreName").style.border = "none";
}
var counter;
counter = 0;

function KeepSessionAlive() {

    counter++;

    var img = document.getElementById("imgSessionAlive");
    if (CurrentDomainName != "")
        img.src = "http://" + CurrentDomainName + "/Dashbord.aspx?c=" + counter;
    else
        img.src = "http://light.loginworks.com/Dashbord.aspx?c=" + counter;

    setTimeout(KeepSessionAlive, 1140000);
}

function PermissionTabs(add, webscrapers, UserType) {
    //  ;
    setTimeout("AdsMenu('" + add + "', " + UserType + ")", 500);
    setTimeout("WebscrapesMenu('" + add + "', " + UserType + ")", 500);
    setTimeout("AADMenu('" + add + "', " + UserType + ")", 500);
    setTimeout("ActionsMenu('" + add + "', " + webscrapers + ")", 500);
    setTimeout("ReportsMenu('" + add + "'," + UserType + ")", 500);
    setTimeout("ClientMenu('" + add + "'," + UserType + ")", 500);
    //New one
    if (add == 0 && webscrapers == 1) {
        $('#WebscrapesMenu').css("display", "block");
        $('#AdsMenu').css("display", "none");
        $('#AADMenu').css("display", "none");
    } else if (add == 0 && webscrapers == 0) {
        $('#WebscrapesMenu').css("display", "none");
        $('#AdsMenu').css("display", "none");
        $('#AADMenu').css("display", "none");
    }
    else {
        $('#WebscrapesMenu').css("display", "inline");
        $('#AdsMenu').css("display", "inline");
        $('#AADMenu').css("display", "inline");
    }



    //till here

}
function UserPreferenceTab(add, webscrapers, IsRedir, UserTypeID) {
    debugger;
    if (add == 0 && webscrapers == 1) {

        ///////// Ravi Code ///////////////
        $('#WebscrapesMenu').css("display", "block");
        $('#AdsMenu').css("display", "none");
        $('#AADMenu').css("display", "none");
        $('#WebscrapesMenu').addClass('active_new');
        $('#AdsMenu').removeClass('active_new');
        $('#AADMenu').removeClass('active_new');
        onTabSelect("Webscrapes");
        ////////////////////////////////////
        if (IsRedir != 1) {
            $("#DivAdsRegular")[0].style.display = "block";
            $("#filtertxt").animate({ margin: '0px 0px 0px 40px' }, 300);
            $("#tabstripDashboard").animate({ width: '100%' }, 300);
            $("#tabWebscrapers").attr("class", "k-state-active");
            $("#tabWebscrapers").toggleClass('k-state-active');
            $("#asana")[0].style.display = "none";
        }
        $("#DivAds")[0].style.display = "none";
        $("#asana")[0].style.display = "none";
        var tabIndx = -1;
        pref = 1;
        var tab = $("#tabWebscrapers");
        if (tab != null) { //safety check
            tabIndx = tab.index();
        }

        $("#tabstripDashboard").kendoTabStrip({
            animation: {
                open: {
                    effects: "fadeIn"
                }
            },
            select: onTabSelectDivInviteUser
        });
        if (IsRedir != 1) {
            var tabStrip1 = $('#tabstripDashboard').kendoTabStrip().data("kendoTabStrip");
            tabStrip1.select(1);
            tabStrip1.select(1);
            if (RegularTaskTab == false) {
                setTimeout('monthcombindHTML()', 500);
            }
        }
    }
    else if (add == 0 && webscrapers == 0) {
        ///////// Ravi Code ///////////////
        $('#WebscrapesMenu').css("display", "none");
        $('#AdsMenu').css("display", "none");
        $('#AADMenu').css("display", "none");
        Home();
        ////////////////////////////////////
        $("#DivAdsRegular")[0].style.display = "none";
        $("#tabwork").attr("class", "k-state-active");
        $("#tabwork").toggleClass('k-state-active');
        var tabIndx = -1;
        var tab = $("#tabwork");
        if (tab != null) { //safety check
            tabIndx = tab.index();
        }
        onTabSelect1(tabIndx)
        $("#tabstripDashboard").kendoTabStrip({
            animation: {
                open: {
                    effects: "fadeIn"
                }
            },
            select: onTabSelect
        });
    }
    else {

        //alert("ankur11");
        ///////// Ravi Code ///////////////
        $('#WebscrapesMenu').css("display", "inline");
        $('#AdsMenu').css("display", "inline");
        $('#AADMenu').css("display", "inline");
        ////////////////////////////////////
        var url = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
        for (var i = 0; i < url.length; i++) {
            var urlparam = url[i].split('=');
        }
        if (urlparam[0] == '1') {
            $("#AdsMenu").addClass('active_new');
            $('#WebscrapesMenu').removeClass('active_new');
            $('#AADMenu').removeClass('active_new');
            $('#DivAds').css("display", "block");
            $('#DivAdsRegular').css("display", "none");
            $('#DivOffcycle').css("display", "none");
            onTabSelect("Ads");

        }
        else if (urlparam[0] == '2') {
            $('#WebscrapesMenu').addClass('active_new');
            $('#AdsMenu').removeClass('active_new');
            $('#AADMenu').removeClass('active_new');
            $('#DivAdsRegular').css("display", "block");
            $('#DivAds').css("display", "none");
            $('#DivOffcycle').css("display", "none");
            onTabSelect("Webscrapes");
        }
        else if (urlparam[0] == '3') {
            $('#AADMenu').addClass('active_new');
            $('#WebscrapesMenu').removeClass('active_new');
            $('#AdsMenu').removeClass('active_new');
            $('#DivOffcycle').css("display", "block");
            $('#DivAdsRegular').css("display", "none");
            $('#DivAds').css("display", "none");
            onTabSelect("All Ads Detected");
        }
        else {
            $("#AdsMenu").addClass('active_new');
            $('#WebscrapesMenu').removeClass('active_new');
            $('#AADMenu').removeClass('active_new');
            $('#DivAds').css("display", "block");
            $('#DivAdsRegular').css("display", "none");
            $('#DivOffcycle').css("display", "none");
            onTabSelect("Ads");
        }


    }

    setTimeout("ActionsMenu('" + add + "', " + webscrapers + ")", 500);
    window.setTimeout("ReportsMenu('" + add + "'," + UserTypeID + ")", 1000);
    //setTimeout("ReportsMenu('" + add + "'," + UserTypeID + ")", 500);

    window.setTimeout("ReportsMenu('" + add + "'," + UserTypeID + ")", 3000);
    window.setTimeout("ClientMenu('" + add + "'," + UserTypeID + ")", 3000);

    if (IsRedir == 1) {
        alert("ttryryre");
        if (add == 0 && webscrapers == 1) {
            // $("#Divcounter")[0].style.display = "none";
            var tabStrip1 = $('#tabstripDashboard').kendoTabStrip().data("kendoTabStrip");
            tabStrip1.select(3);
            $("#filtertxt").animate({ margin: '0px 0px 0px 40px' }, 300);
            $("#tabstripDashboard").animate({ width: '56%' }, 300);
            //$("#DivAdsRegular")[0].style.display = "none";
            // $("#tabwork").attr("class", "k-state-active");
            // $("#tabwork").toggleClass('k-state-active');
            $("#addworkitemdiv").animate({ margin: '0px 0px 0px 0px' }, 300);
            $("#asana")[0].style.display = "block";
            if (OtherTaskTab == false) {
                // OtherTaskTab = true;
                // calbindstatuscom();
                // setTimeout('GetAllWorkItem()', 500);
                // abortRequests();
                // setTimeout('calbindstatuscom()', 200);
                fff();
            }

        }
        else if (add == 0 && webscrapers == 0) {
            // $("#Divcounter")[0].style.display = "none";
            var tabStrip1 = $('#tabstripDashboard').kendoTabStrip().data("kendoTabStrip");
            tabStrip1.select(3);
            $("#filtertxt").animate({ margin: '0px 0px 0px 40px' }, 300);
            $("#tabstripDashboard").animate({ width: '56%' }, 300);
            $("#addworkitemdiv").animate({ margin: '0px 0px 0px 0px' }, 300);
            $("#asana")[0].style.display = "block";
            if (OtherTaskTab == false) {
                // OtherTaskTab = true;
                // calbindstatuscom();
                // setTimeout('GetAllWorkItem()', 500);
                // abortRequests();
                // setTimeout('calbindstatuscom()', 200);
                fff();
            }
        }
        else {
            //$("#Divcounter")[0].style.display = "none";
            var tabStrip1 = $('#tabstripDashboard').kendoTabStrip().data("kendoTabStrip");
            tabStrip1.select(3);
            $("#filtertxt").animate({ margin: '0px 0px 0px 40px' }, 300);
            $("#tabstripDashboard").animate({ width: '56%' }, 300);
            $("#addworkitemdiv").animate({ margin: '0px 0px 0px 0px' }, 300);
            $("#asana")[0].style.display = "block";
            if (OtherTaskTab == false) {
                // OtherTaskTab = true;
                // calbindstatuscom();
                // setTimeout('GetAllWorkItem()', 500);
                // abortRequests();
                // setTimeout('calbindstatuscom()', 200);
                fff();
            }
        }

    }

    if (IsRedir == 1) {
        IsRedir = 0;
    }

}
var s;
function onTabSelect1(e) {
    if (e == 3) {
        var tabStrip1 = $('#tabstripDashboard').kendoTabStrip().data("kendoTabStrip");
        tabStrip1.select(3);
        $("#filtertxt").animate({ margin: '0px 0px 0px 40px' }, 300);
        $("#tabstripDashboard").animate({ width: '56%' }, 300);
        $("#addworkitemdiv").animate({ margin: '0px 0px 0px 0px' }, 300);
        $("#asana")[0].style.display = "block";
        if (OtherTaskTab == false) {
            //OtherTaskTab = true;
            // calbindstatuscom();
            // setTimeout('GetAllWorkItem()', 500);
            // abortRequests();
            // setTimeout('calbindstatuscom()', 200);
            fff();
        }
    }
    else if (e == 1) {

        $("#filtertxt").animate({ margin: '0px 0px 0px 40px' }, 300);
        $("#tabstripDashboard").animate({ width: '100%' }, 300);
        $("#asana")[0].style.display = "none";
        var tabStrip1 = $('#tabstripDashboard').kendoTabStrip().data("kendoTabStrip");
        tabStrip1.select(1);
        if (RegularTaskTab == false) {
            // RegularTaskTab = true;
            // setTimeout('ShowReg()', 500);
            // setTimeout('GetAllWorkItem()', 500);
            //abortRequests();
            //setTimeout('calbindmonthcom()', 200);ByMe
            setTimeout('monthcombindHTML()', 200);
            //setTimeout('calbindstatusscrapcom()', 500);
            // calbindmonthcom();
            // calbindstatusscrapcom();
        }
    }
    else if (e == "Ads") {

        $("#filtertxt").animate({ margin: '0px 0px 0px 40px' }, 300);
        $("#tabstripDashboard").animate({ width: '100%' }, 300);
        $("#asana")[0].style.display = "none";
        var tabStrip1 = $('#tabstripDashboard').kendoTabStrip().data("kendoTabStrip");
        tabStrip1.select(0);
        if (ScheduleTab == false) {
            // abortRequests();
            // ScheduleTab = true;

            FilterGridByStartDate();
        }
    }

    else if (e == "All Ads Detected") {

        $("#filtertxt").animate({ margin: '0px 0px 0px 40px' }, 300);
        $("#tabstripDashboard").animate({ width: '100%' }, 300);
        $("#asana")[0].style.display = "none";
        var tabStrip1 = $('#tabstripDashboard').kendoTabStrip().data("kendoTabStrip");
        tabStrip1.select(2);
        var ds = new Date();
        var curr_day1 = ds.getDate();
        if (curr_day1 < 10) {
            curr_day1 = '0' + curr_day1;
        }
        var curr_month1 = ('0' + (ds.getMonth() + 1)).slice(-2)
        var curr_year1 = ds.getFullYear();
        var Start_date = curr_month1 + "/" + curr_day1 + "/" + curr_year1;
        var datepicker = $("#DivOffCal").data("kendoDatePicker");
        datepicker.value(Start_date);

        if (OffcyleAds1 == false || OffcyleAds2 == false || OffcyleAds3 == false || OffcyleAds4 == false) {
            $("#DivShowAlreadyGrid")[0].style.display = "none";
            $("#Button3Alreadyads")[0].style.display = "none";
            // OffcyleAds = true;
            var CDate = $("#DivOffCal").val();
            abortRequests();
            if (OffcyleAds4 == false)
                OffcycleNoAdsDetected(CDate);
            if (OffcyleAds3 == false)
                GetOffcycleUnprocessedAds(CDate);
            if (OffcyleAds1 == false)
                BindOffcycleFilterSt();
            //   GetOffcycleNewStAds(CDate);
            // if (OffcyleAds2 == false)
            //    GetOffcycleOldStAds(CDate);
        }


    }

}
function onTabSelect(value) {

    var tabValue = value;
    if (tabValue.trim() == "Work items") {
        var tabStrip1 = $('#tabstripDashboard').kendoTabStrip().data("kendoTabStrip");
        tabStrip1.select(3);
        $("#filtertxt").animate({ margin: '0px 0px 0px 40px' }, 300);
        $("#tabstripDashboard").animate({ width: '56%' }, 300);
        $("#addworkitemdiv").animate({ margin: '0px 0px 0px 0px' }, 300);
        $("#asana")[0].style.display = "block";
        if (OtherTaskTab === false) {
            //OtherTaskTab = true;
            //  setTimeout('calbindstatuscom()', 200);
            fff();
            // GetAllWorkItem();
        }
    }
    else if (tabValue.trim() == "Webscrapes") {

        $("#filtertxt").animate({ margin: '0px 0px 0px 40px' }, 300);
        $("#tabstripDashboard").animate({ width: '100%' }, 300);
        $("#asana")[0].style.display = "none";
        var tabStrip1 = $('#tabstripDashboard').kendoTabStrip().data("kendoTabStrip");
        tabStrip1.select(1);
        if (RegularTaskTab == false) {
            setTimeout('monthcombindHTML()', 200);
        }

    }
    else if (tabValue.trim() == "Ads") {

        $("#filtertxt").animate({ margin: '0px 0px 0px 40px' }, 300);
        $("#tabstripDashboard").animate({ width: '100%' }, 300);
        $("#asana")[0].style.display = "none";
        var tabStrip1 = $('#tabstripDashboard').kendoTabStrip().data("kendoTabStrip");
        tabStrip1.select(0);
        if (ScheduleTab == false) {

            setTimeout('FilterGridByStartDate()', 200);

        }

    }
    else if (tabValue.trim() == "All Ads Detected") {
        //  ;
        $("#filtertxt").animate({ margin: '0px 0px 0px 40px' }, 300);
        $("#tabstripDashboard").animate({ width: '100%' }, 300);
        $("#asana")[0].style.display = "none";
        var tabStrip1 = $('#tabstripDashboard').kendoTabStrip().data("kendoTabStrip");
        tabStrip1.select(2);
        var ds = new Date();
        var curr_day1 = ds.getDate();

        if (curr_day1 < 10) {
            curr_day1 = '0' + curr_day1;
        }
        var curr_month1 = ('0' + (ds.getMonth() + 1)).slice(-2)
        var curr_year1 = ds.getFullYear();
        var start_date = curr_month1 + "/" + curr_day1 + "/" + curr_year1;

        var datepicker = $("#DivOffCal").kendoDatePicker();
        //  var datepicker = $("#DivOffCal").data("kendoDatePicker");

        datepicker.val(start_date);
        if (OffcyleAds1 == false || OffcyleAds2 == false || OffcyleAds3 == false || OffcyleAds4 == false) {

            $("#DivShowAlreadyGrid")[0].style.display = "none";
            $("#Button3Alreadyads")[0].style.display = "none";
            var cdate = $("#DivOffCal").val();
            abortRequests();
            if (OffcyleAds4 == false)
                OffcycleNoAdsDetected(cdate);
            if (OffcyleAds3 == false)
                GetOffcycleUnprocessedAds(cdate);
            if (OffcyleAds1 == false) {
                BindOffcycleFilterSt();
            }
        }
    }
}

//slip time popup
function GetSlipageReport(userid, typeid) {

    if ($("#SlipageWindow").data("kendoWindow") != null) {
        var dialogtest = $("#SlipageWindow").data("kendoWindow");
        dialogtest.close();
    }

    var windowElement = $("#SlipageWindow").kendoWindow({
        iframe: true,
        content: "ElapsedTime.htm?userid=" + userid + "&typeid=" + typeid + "",
        modal: true,
        width: "1000px",
        title: "Slipage Time Report",
        height: "450px",
        position: {
            top: 5,
            left: 2,
            bottom: 5
        },
        resizable: false,
        actions: ["Close"],
        draggable: false

    });
    var dialog = $("#SlipageWindow").data("kendoWindow");
    dialog.open();
    dialog.center();
    return false;

}

//Invite User
function AddInviteUser(userid, typeid) {

    if ($("#DivInviteUser").data("kendoWindow") != null) {
        var dialogtest = $("#DivInviteUser").data("kendoWindow");
        dialogtest.close();
    }
    var heightt = 0;
    $("#DivInviteUser").append("<div id='AllcontentforInviteUser' style='display:none;' > <div style='background-color: #1f8dd6;border-radius: 4px 4px 0px 0;'><div style='padding: 5px 0;'><span style='font-size: 25px; color: white;margin-left: 10px; font-family: arial;'>Invite Your Team</span> </div> <div style='float: right;margin-top: -32px;'> <input type='button' id='Button1ClassForCross' onclick='javascript:Cancelpermission();' style='padding: 0 6px;' class='ClassForCross cursor-pointer' />         </div>     </div> <div style='background-color:#004e7f; height:6px;'></div><table style='background-color: white;' cellspacing='15px' cellpadding='5px' class='width-100'><tr><td><div class='invite_input_row'><input type='text' id='TxtEmail' class='WirthHeightforinput' placeholder='Email Id:*'/></div><div id='diverrormsg' class='display-none' style='background-color: white;'><label id='errormsg' class='font-family-Arial' style='color: red;'>Email already exists!</label></div></td></tr><tr><td><div class='invite_input_row'><input type='text' placeholder='Name:*' id='Txtname' class='WirthHeightforinput'/></div></td></tr><tr><td><div class='invite_input_row'><select id='HtmComcoTimezone' class='WAndHfor'></select></div></td></tr><tr id='teamlistid'><td><div id='teamlistbind2' class='invite_input_row display-none'><select id='HtmComcoteamlist' class='WAndHfor'></select></div></td></tr><tr><td><div id='divlabelpermission' class='display-none'> <label id='Label3' class='classforcheck1'>Permissions:</label> </div></td> </tr> <tr><td><div class='checkbox_cont'><div class='permission_checkboxes'><div id='divsecondpermission' class='display-none checkbox_row'><input type='checkbox' id='secondpermission' /><label id='Label5' class='classforcheck'>Webscrapes</label></div><div id='divonlyworkitem' class='display-none checkbox_row'><input type='checkbox' checked='checked' disabled='disabled' id='workitempermission' /><label id='Label7' class='classforcheck'> WorkItem  </label></div> <div id='divthirdpermission' class='display-none checkbox_row'><input type='checkbox' id='thirdpermission' onchange='javascript:checkallper();' /><label id='Label6' class='classforcheck'> All </label></div><div class='clr'></div></div> <div class='cancel_invite_buttons'> <div id='DivCancel' class='hWforsavecancle'><input type='button' id='BtnCancel' class='new-primary-button2' style='font-size: 14px;font-family: arial;color: white;font-weight: 100;' value='Cancel' onclick='javascript:Cancelpermission();' class='hWforCancleimg cursor-pointer' /></div> <div id='DivSave' class='float-left hWforsavecancle'><input type='button' id='btnSavePer'class='new-primary-button' style='font-size: 14px;font-family: arial;color: white;font-weight: 100;' value='Invite' onclick='javascript:SavePermission();'class='cursor-pointer' /></div></div></div> </td></tr></table><center><div class='div-aiu-1 float-right'></div></center></div>");
    $('.WirthHeightforinput').each(function () {
        $(this).val("");
    });
    //alert($('#secondpermission')[0]);
    $('#secondpermission')[0].checked = false;

    $('#thirdpermission')[0].checked = false;
    $("#diverrormsg").css("display", "none");
    $("#btnSavePer").attr('value', 'Save');
    if (typeid == "14" || typeid == "12") {
        $('#teamlistbind2')[0].style.display = "block";
        bindhtmteamlist();
        heightt = 380;
    }
    else {
        bindhtmteamlistwithoutdropdown(userid);
        $("#teamlistid")[0].style.display = "none";
        heightt = 320;
    }
    Checkpermissiononaddinvitepage();
    bindhtmdropdown();
    $("#AllcontentforInviteUser").css("display", "block");

    var windowElement = $("#DivInviteUser").kendoWindow({
        iframe: true,
        //content: "AddInviteUser.htm?userid=" + userid + "&typeid=" + typeid + "",
        modal: true,

        //title: "Invite User",
        title: false,
        height: heightt + "px",
        position: {
            top: 5,
            left: 2,
            bottom: 5
        },
        resizable: false,
        // actions: ["Close"],
        draggable: false

    });
    var dialog = $("#DivInviteUser").data("kendoWindow");
    dialog.open();
    dialog.center();
    return false;

}

//function PermissionTabs(add, webscrapers, UserTypeID) {
//     ;
//    setTimeout("AdsMenu('" + add + "', " + UserTypeID + ")", 500);
//    setTimeout("WebscrapesMenu('" + add + "', " + UserTypeID + ")", 500);
//    setTimeout("AADMenu('" + add + "', " + UserTypeID + ")", 500);
//    setTimeout("ActionsMenu('" + add + "', " + webscrapers + ")", 500);
//    setTimeout("ReportsMenu('" + add + "', " + UserTypeID + ")", 500);
//// Adding New code
//    if (add == 0 && webscrapers == 1) {
//        $('#WebscrapesMenu').css("display", "block");
//        $('#AdsMenu').css("display", "none");
//        $('#AADMenu').css("display", "none");
//    } else if (add == 0 && webscrapers == 0) {
//        $('#WebscrapesMenu').css("display", "none");
//        $('#AdsMenu').css("display", "none");
//        $('#AADMenu').css("display", "none");
//    }
//    else {
//        $('#WebscrapesMenu').css("display", "inline");
//        $('#AdsMenu').css("display", "inline");
//        $('#AADMenu').css("display", "inline");

//    }
//}

function abortRequests() {

    for (var e = 0; e < 13; e++) {
        if (arrStatus[e] != null) {
            arrStatus[e].abort()
        }
    }
}
//function deleteAllCookiesAtLogOut() {
//    var uuname = getCookie("username");
//    if (uuname != null && uuname != "") {

//        var arrr = new Array();
//        arrr = uuname.split("~");
//        usernm = arrr[0];
//        passwd = arrr[1];
//        document.cookie = 'username' + '=; Path=/; Expires=Thu, 01 Jan 1970 00:00:01 GMT;';
//    }
//}
/////////
function insertsfromprompt(Message, TaskID, Statusvalue) {
    if (Message != "") {
        $.ajax({
            type: "POST",
            url: "/DataTracker.svc/InsertCompletedMessages",
            data: "{\"TaskID\": \"" + TaskID + "\",\"Message\": \"" + encodeURIComponent(escape(Message)) + "\"",
            contentType: "application/json; charset=utf-16",
            dataType: "json",
            success: function (data) {
                var res = data.d.split('~');
                if (res != null) {
                    if (res[0] == "1") {
                        //$("#StatusMessagework")[0].style.color = "green";
                        // $("#StatusMessagework")[0].innerHTML = "Save successfully";
                        ChangeOtherStatus(TaskID, Statusvalue);
                        CloseworkCommentPopup();
                        GetTaskcomment(TaskID, 0);
                        // GetTaskcomment(TaskID, res[1]);

                    }
                    else if (res[0] == "0") {
                        ChangeOtherStatus(TaskID, Statusvalue);
                    }
                    else if (res[0] == "-1") {
                    }
                }
                else {
                    return false;
                }
            },
            failure: function (response) {
            },
            error: function (xhr, status, error) {
            }
        });
    }
    else {
        // $("#lblerror")[0].style.display = "block";
    }
}
/////
function OpenWorkItemStatusComment(childSID, Statusvalue) {
    if ($("#WorkItemStatusCommentwindow").data("kendoWindow") != null) {
        var StatusReport = $("#WorkItemStatusCommentwindow").data("kendoWindow");
        StatusReport.close();
    }
    var windowElement = $("#WorkItemStatusCommentwindow").kendoWindow({
        iframe: true,
        content: "WorkItemStatusComment.aspx?childSID=" + childSID + "&Statusvalue=" + Statusvalue + "",
        modal: true,
        width: "400px",
        title: "Comment",
        height: "280px",
        position: {
            top: 10,
            left: 2
        },
        resizable: false,
        actions: ["Close"],
        draggable: false

    });
    var dialog = $("#WorkItemStatusCommentwindow").data("kendoWindow");
    dialog.open();
    dialog.center();
    return false;
}
//////////////
function CloseworkCommentPopup() {
    window.parent.$("#WorkItemStatusCommentwindow").data("kendoWindow").close();
}


function alertforsubscriber() {
    alert("You can not edit this task");
}

function printAdsGrid(sv) {
    var priname = "";
    if (sv == '1') {
        priname = "AdsData";
    }
    else if (sv == '2')
        priname = "WebscrapsData";
    else if (sv == '3')
        priname = "JobSummary";

    if (sv == '1') {
        var gridElement = $('#DetailGrid'),
        printableContent = '',
        win = window.open('', '', 'width=800, height=500'),
        doc = win.document.open();
    }
    else if (sv == '2') {
        var gridElement = $('#RegularTaskGrid'),
        printableContent = '',
        win = window.open('', '', 'width=800, height=500'),
        doc = win.document.open();
    }
    else if (sv == '3') {
        var gridElement = $('#reportjDategrid'),
        printableContent = '',
        win = window.open('', '', 'width=800, height=500'),
        doc = win.document.open();
    }

    var htmlStart =
            '<!DOCTYPE html>' +
            '<html>' +
            '<head>' +
            '<meta charset="utf-8" />' +
            '<title>' + priname + '</title>' +
            '<link href="/Styles/kendo.common.min.css" rel="stylesheet" type="text/css" /> ' +
            '<style>' +
            'html { font: 11pt sans-serif; }' +
            '.k-grid { border-top-width: 0; }' +
            '.k-grid, .k-grid-content { height: auto !important; }' +
            '.k-grid-content { overflow: visible !important; }' +
            'div.k-grid table { table-layout: auto; width: 100% !important; }' +
            '.k-grid .k-grid-header th { border-top: 1px solid; }' +
            '.k-grid-toolbar, .k-grid-pager > .k-link { display: none; }' +
            '</style>' +
            '</head>' +
            '<body>';

    var htmlEnd =
            '</body>' +
            '</html>';

    var gridHeader = gridElement.children('.k-grid-header');
    if (gridHeader[0]) {
        var thead = gridHeader.find('thead').clone().addClass('k-grid-header');
        printableContent = gridElement
            .clone()
                .children('.k-grid-header').remove()
            .end()
                .children('.k-grid-content')
                    .find('table')
                        .first()
                            .children('tbody').before(thead)
                        .end()
                    .end()
                .end()
            .end()[0].outerHTML;
    } else {
        printableContent = gridElement.clone()[0].outerHTML;
    }

    doc.write(htmlStart + printableContent + htmlEnd);
    doc.close();
    win.print();
}

function OffcycleNodataAlert() {
    alert("This is weekly or early ad to download please visit ads tab");
    return false;
}
//offcycle
function OffcycleAdsStbind(OffCycleCombo) {
    if (OffCycleCombo != null) {
        $.ajax({
            type: "GET",
            url: "/DataTracker.svc/GetAdDetectedStatusList",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                var res = data.d;
                if (res != null) {
                    OffCycleCombo.clearItems();
                    OffCycleCombo.trackChanges();
                    for (var i = 0; i < data.d.length; i++) {
                        var comboItem = new Telerik.Web.UI.RadComboBoxItem();
                        comboItem.set_text(data.d[i].StatusName);
                        comboItem.set_value(data.d[i].StatusID);
                        OffCycleCombo.get_items().add(comboItem);
                    }
                    OffCycleCombo.commitChanges();
                    for (var i = 0; i < OffCycleCombo.get_items().get_count() ; i++) {
                        if (OffCycleCombo.get_items().getItem(i).get_value() != ComValue && OffCycleCombo.get_items().getItem(i).get_value() != Onhold && OffCycleCombo.get_items().getItem(i).get_value() != Cacelvaue && OffCycleCombo.get_items().getItem(i).get_value() != "25" && OffCycleCombo.get_items().getItem(i).get_value() != "29" && OffCycleCombo.get_items().getItem(i).get_value() != "31" && OffCycleCombo.get_items().getItem(i).get_value() != "32") {

                            OffCycleCombo.get_items().getItem(i).set_checked(true);
                        }
                    }

                    var CDate = $("#DivOffCal").val();
                    GetOffcycleNewStAds(CDate, OffCycleCombo);

                }
                else {


                }
            }
        });
    }
}

function ShowLastaaaaa() {
    for (var i = 0; i < $(".timeagotask").length; i++) {
        if (($.timeago($(".timeagotask")[i]).indexOf("NaN")) == "-1") {
            var stime = $(".timeagotask")[i].title;
            var StID = $(".timeagotask")[i].id;
            var today = new Date();
            var dd = today.getDate();
            var mm = today.getMonth() + 1;
            var yyyy = today.getFullYear();
            var split = stime.split('/');
            var edatemm = split[0];
            var edatedd = split[1];
            var edateyy = split[2];

            if (split != "") {

                var yearrr = edateyy.split(' ');
                var yyyyyy = yearrr[0];
                var endate = edatemm + '/' + edatedd + '/' + yyyyyy;

                var today = mm + '/' + dd + '/' + yyyy + '';
                curentdate = new Date(today);
                endddate = new Date(endate);
                if (curentdate.getTime() == endddate.getTime()) {

                    $(".timeagotask")[i].innerHTML = "<span style='color:blue'>" + $.timeago($(".timeagotask")[i]) + "</span>";
                }
                else if ((StID == '12') || (StID == '13') || (StID == '14')) {

                    $(".timeagotask")[i].innerHTML = $.timeago($(".timeagotask")[i]);
                }
                else if (curentdate > endddate) {

                    $(".timeagotask")[i].innerHTML = "<span style='color:#ff0000'>" + $.timeago($(".timeagotask")[i]) + "</span>";
                }
                else {

                    $(".timeagotask")[i].innerHTML = $.timeago($(".timeagotask")[i]);
                }
            }

            else {

                $(".timeagotask")[i].innerHTML = $.timeago($(".timeagotask")[i]);
            }

        }

    }

}


function MDFormat(MMDD) {
    MMDD = new Date(MMDD);

    var months = ["Jan", "Feb", "Mar", "Apr", "May", "June", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
    var strDate = "";
    var day = MMDD.getDate();
    var month = months[MMDD.getMonth()];

    var today = new Date();
    today.setHours(0, 0, 0, 0);

    var yesterday = new Date();
    yesterday.setHours(0, 0, 0, 0);
    yesterday.setDate(yesterday.getDate() - 1);

    var tomorrow = new Date();
    tomorrow.setHours(0, 0, 0, 0);
    tomorrow.setDate(tomorrow.getDate() + 1);
    // console.log(MMDD.getTime(), today.getTime(), MMDD.getTime() == today.getTime());

    if (today.getTime() == MMDD.getTime()) {
        strDate = "Today";
    } else if (yesterday.getTime() == MMDD.getTime()) {
        strDate = "Yesterday";
    } else if (tomorrow.getTime() == MMDD.getTime()) {
        strDate = "Tomorrow";
    } else {
        strDate = months[MMDD.getMonth()] + "-" + MMDD.getDate();
    }

    return strDate;
}

function UpdateTitleandDescription() {
    var title = $("#details_property_sheet_title").val();
    if (title != "" && title != null) {
        var description = document.getElementById('descriptionoftask').innerHTML;
        if (titlebefore.trim() == title.trim() && descriptionbefore.trim() == description.trim()) {
            return false;
        }
        else {

            $.ajax({

                type: "POST",
                url: "/DataTracker.svc/UpdateTitleandDesc",
                data: "{\"TaskID\": \"" + s + "\",\"Title\": \"" + encodeURIComponent(escape(title)) + "\",\"desc\": \"" + encodeURIComponent(escape(description)) + "\"",
                contentType: "application/json; charset=utf-16",
                dataType: "json",
                success: function (data) {
                    var res = data.d;
                    if (res != null) {
                        Settitleanddescriptionforwindow(title, description);

                    }
                    else {
                        return false;
                    }
                },
                failure: function (response) {
                    // alert(response);
                },
                error: function (xhr, status, error) {
                    // alert(error);
                }
            });
        }
    }
    else {
        alert("Title should not be blank!");
        return false;
    }
}
var titlebefore = "";
var descriptionbefore = "";
function Mousedowntitle() {
    titlebefore = $("#details_property_sheet_title").val();
    descriptionbefore = document.getElementById('descriptionoftask').innerHTML

}
function Settitleanddescriptionforwindow(title, desc) {
    $("#details_property_sheet_title")[0].value = "";
    $("#descriptionoftask")[0].innerHTML = "";
    $("#details_property_sheet_title")[0].value = title;
    $("#descriptionoftask")[0].innerHTML = desc;
}
function alertshowallowners() {
    if (allmoreowners != "")
        alert(allmoreowners);
    else
        return false;
}

function kendoFastRedrawRow(grid, row) {
    var dataItem = grid.dataItem(row);

    var rowChildren = $(row).children('td[role="gridcell"]');

    for (var i = 0; i < grid.columns.length; i++) {

        var column = grid.columns[i];
        var template = column.template;
        var cell = rowChildren.eq(i);

        if (template !== undefined) {
            var kendoTemplate = kendo.template(template);

            // Render using template
            cell.html(kendoTemplate(dataItem));
        } else {
            var fieldValue = dataItem[column.field];

            var format = column.format;
            var values = column.values;

            if (values !== undefined && values != null) {
                // use the text value mappings (for enums)
                for (var j = 0; j < values.length; j++) {
                    var value = values[j];
                    if (value.value == fieldValue) {
                        cell.html(value.text);
                        break;
                    }
                }
            } else if (format !== undefined) {
                // use the format
                cell.html(kendo.format(format, fieldValue));
            } else {
                // Just dump the plain old value
                cell.html(fieldValue);
            }
        }
    }
}


function SetUrlOfPpImage(imgurl) {
    $("#AttachedImage")[0].innerHTML = "";
    var imgName = "";
    imgName = imgurl.substring(imgurl.lastIndexOf("/") + 1, imgurl.lastIndexOf("."));
    $("#AttachedImage").append("<div id='imageDiv'> <img id='imgup'  src='" + imgurl + "'></img></div>");
    $("#AttachedImage").kendoWindow({
        actions: ["Close"],
        draggable: false,
        height: "auto",
        modal: true,
        resizable: false,
        title: imgName,
        width: "auto",
        border: '1px solid red',
        visible: false /*don't show it yet*/
    });
    $("#AttachedImage").data("kendoWindow").open();
    $("#AttachedImage").parent().addClass("setimage");
    //css("left", "50% !important");
    // $("#AttachedImage").parent().css("Transform", "Translate(-50%,-50%) !important");
    //$("#AttachedImage").parent().css("top", "50% !important");
    //$("#AttachedImage").css({ "border-color": "#C1E0FF", "border-width": "2px", "border-style": "solid" });
    var setimg = window.parent.$("#AttachedImage").data("kendoWindow");
    setimg.setOptions({
        title: imgName
    });

    return false;
}

function funclose() {
    window.parent.$("#AttachedImage").data("kendoWindow").close();

}
var Current_usertypeid = "";
function Setcuser(tid) {
    Current_usertypeid = tid;
}
function ShowPopTaskTimeSheet() {
    ShowPopTaskTimeSheetT(s, Current_usertypeid);
}


function Openteammanage() {
    if ($("#DivTeamManage").data("kendoWindow") != null) {
        var StatusReport = $("#DivTeamManage").data("kendoWindow");
        StatusReport.close();
    }

    var windowElement = $("#DivTeamManage").kendoWindow({
        iframe: true,
        content: "TeamsManagement.htm",
        modal: true,
        width: "920px",
        title: "Team/Project Management",
        height: "550px",
        position: {
            top: 8,
            left: 2
        },
        resizable: false,
        actions: ["Close"],
        draggable: false

    });
    var dialog = $("#DivTeamManage").data("kendoWindow");
    dialog.open();
    dialog.center();
    return false;
}

function fulldescription() {

    if ($("#descriptionoftask").css('display') == 'block') {
        $("#descriptionoftask")[0].style.display = 'none';
        $("#fulldescriptionoftask")[0].style.display = 'block';
        $("#fulldescriptionoftask")[0].focus();
    }
    else {
        $("#descriptionoftask")[0].style.display = 'block';
        $("#fulldescriptionoftask")[0].style.display = 'none';
    }
}

function InsertSubTask(sender, args) {
    if (args.keyCode == 13) {
        var subtitle = $("#txtadd-subtask").val();
        $("#txtadd-subtask")[0].value = "";
        if (subtitle != "") {
            $.ajax({
                type: "GET",
                contentType: "application/json; charset=utf-8",
                url: "/DataTracker.svc/InsertSubTask?taskid=" + s + "&SubTitle=" + subtitle + "",
                dataType: "json",
                success: function (data) {
                    if (data.d == 0) {
                        GetallSubtasks(s);
                    }


                },
                error: function (result) {

                }
            });
        }
        else {
            alert("please enter title !");
            return false;
        }
    }
}


function GetallSubtasks(tid) {

    $("#AllSubTasksList")[0].innerHTML = "";
    $.ajax({
        type: "GET",
        contentType: "application/json; charset=utf-8",
        url: "/DataTracker.svc/GetAllSubtasks?taskId=" + tid + "",
        dataType: "json",
        success: function (data) {
            $.each(data.d, function (index, item) {

                if (cuid == item.userid) {

                    $("#AllSubTasksList").append("<li  id='list" + item.sid + "' ondrop='javascript:drop(event);'  ondragover='javascript:allowDrop(event);'  ondragstart='javascript:drag(event);'   draggable='true'   class='lisubtasks'     style='position: relative;'><i class='icon-th handler'></i><span class='checkmark'><input type='checkbox' id='check" + item.sid + "'   onchange='javascript:CompleteSubTask(" + item.sid + ");' /></span><span class='subtitle'    id='sub_" + item.sid + "' onclick='javascript:LisubEdittask(" + item.sid + ");' title='CreatedBy: " + item.createdBy + "'>" + item.title + "</span><input class='subedit' id='editsub_" + item.sid + "'     onkeydown='javascript:UpdateSubTask(this, event," + item.sid + ");'  onblur='javascript:editSubtask(" + item.sid + ")' style='display:none' type='text' value='" + item.title + "'><a class='subclose' href='#' onclick='javascript:deleteSubTask(" + item.sid + ");'>×</a></li>");
                    if (item.statusID == 12)
                        $("#check" + item.sid)[0].checked = true;
                } else {

                    $("#AllSubTasksList").append("<li  id='list" + item.sid + "'   class='lisubtasks'     style='position: relative;'><i class='icon-th handler'></i><span class='checkmark'><input type='checkbox' id='check" + item.sid + "'   onchange='javascript:CompleteSubTask(" + item.sid + ");' /></span><span class='subtitle'    id='sub_" + item.sid + "' onclick='javascript:LisubEdittask(" + item.sid + ");' title='CreatedBy: " + item.createdBy + "'>" + item.title + "</span><input class='subedit' id='editsub_" + item.sid + "'     onkeydown='javascript:UpdateSubTask(this, event," + item.sid + ");'  onblur='javascript:editSubtask(" + item.sid + ")' style='display:none' type='text' value='" + item.title + "'><a class='subclose' href='#' onclick='javascript:deleteSubTask(" + item.sid + ");'>×</a></li>");
                    if (item.statusID == 12)
                        $("#check" + item.sid)[0].checked = true;
                }
            });

        },
        error: function (result) {

        }
    });
}


function GetchartData() {
    $("#main").show();
    $("#chartRevenue")[0].innerHTML = " ";
    $("#chartOrder")[0].innerHTML = " ";
    $("#chartRevenue").html("");
    $("#chartOrder").html("");
    MobileReportChartRevenueLast_30();
    MobileReportChartOrderLast_30();
}

function MobileReportChartRevenueLast_30() {
    //  ;
    var cdate = new Date();
    var dd = cdate.getDate();
    var mm = cdate.getMonth() + 1;
    var yyyy = cdate.getFullYear();
    today_Date = yyyy + '/' + mm + '/' + dd;
    if ($("#Lastsevendays").is(':checked')) {
        $("#AllDateDIv").hide();
        $("#chartRevenue").empty();
        var msecPerDay = 7 * 24 * 60 * 60 * 1000;
        var Seven_days_ago = new Date(cdate.getTime() - msecPerDay);
        var sdd = Seven_days_ago.getDate();
        var smm = Seven_days_ago.getMonth() + 1;
        var syyyy = Seven_days_ago.getFullYear();
        var Pre_Date = syyyy + '/' + smm + '/' + sdd;
        late_Date = Pre_Date;
        CreateRevenueChart(late_Date, today_Date);
    }
    else if ($("#Lastthirty").is(':checked')) {
        $("#AllDateDIv").hide();
        $("#chartRevenue").html("");
        var msecPerDay = 30 * 24 * 60 * 60 * 1000;
        var Seven_days_ago = new Date(cdate.getTime() - msecPerDay);
        var sdd = Seven_days_ago.getDate();
        var smm = Seven_days_ago.getMonth() + 1;
        var syyyy = Seven_days_ago.getFullYear();
        var Pre_Date = syyyy + '/' + smm + '/' + sdd;
        late_Date = Pre_Date;
        excelfilename = "Last_30_days_TimeReport";
        CreateRevenueChart(late_Date, today_Date);
    }
    else if ($("#CustomRadiobt").is(':checked')) {

        Pre_Date = $("#dtpStartDate").val();
        today_Date = $("#dtpEndDate").val();
        late_Date = Pre_Date;
        excelfilename = "From" + Pre_Date + "-" + today_Date + "Timereport";
        CreateRevenueChart(Pre_Date, today_Date);
    }
    else {
        //alert("hh");
        return false;
    }
}
function MobileReportChartOrderLast_30() {
    var cdate = new Date();
    var dd = cdate.getDate();
    var mm = cdate.getMonth() + 1;
    var yyyy = cdate.getFullYear();
    today_Date = yyyy + '/' + mm + '/' + dd;
    if ($("#Lastsevendays").is(':checked')) {
        $("#AllDateDIv").hide();
        $("#chartOrder").empty();
        var msecPerDay = 7 * 24 * 60 * 60 * 1000;
        var Seven_days_ago = new Date(cdate.getTime() - msecPerDay);
        var sdd = Seven_days_ago.getDate();
        var smm = Seven_days_ago.getMonth() + 1;
        var syyyy = Seven_days_ago.getFullYear();
        var Pre_Date = syyyy + '/' + smm + '/' + sdd;
        late_Date = Pre_Date;
        createOrderChart(late_Date, today_Date);
    }
    else if ($("#Lastthirty").is(':checked')) {
        $("#AllDateDIv").hide();
        $("#chartOrder").html("");
        var msecPerDay = 30 * 24 * 60 * 60 * 1000;
        var Seven_days_ago = new Date(cdate.getTime() - msecPerDay);
        var sdd = Seven_days_ago.getDate();
        var smm = Seven_days_ago.getMonth() + 1;
        var syyyy = Seven_days_ago.getFullYear();
        var Pre_Date = syyyy + '/' + smm + '/' + sdd;
        late_Date = Pre_Date;
        excelfilename = "Last_30_days_TimeReport";
        createOrderChart(late_Date, today_Date);
    }
    else if ($("#CustomRadiobt").is(':checked')) {
        Pre_Date = $("#dtpStartDate").val();
        today_Date = $("#dtpEndDate").val();
        late_Date = Pre_Date;
        excelfilename = "From" + Pre_Date + "-" + today_Date + "Timereport";
        createOrderChart(late_Date, today_Date);
    }
    else {

        return false;
    }
}

function customDiv() {
    $("#AllDateDIv").show();
    $("#main").hide();
}

function CreateRevenueChart(date1, date2) {
    $.ajax({
        type: "GET",
        url: "/DataTracker.svc/GetAppTotalRevenueReport?startdate=" + date1 + "&enddate=" + date2 + "&Arrappidnotshowinmobilapp=" + Arrappidnotshowinmobilapp + "",
        dataType: "Json",
        success: function (data) {
            RestaurentName = new Array();
            TotalRevenue = new Array();
            var res = data.d;
            if (data.d.length < 10) {
                document.getElementById('chartRevenue').setAttribute("style", "height:350px;font-size: 15px;margin-left: 21px; width:48%; border:1px solid Gray; float:left");
            } else if (data.d.length < 15) {
                document.getElementById('chartRevenue').setAttribute("style", "height:400px;font-size: 15px;margin-left: 21px; width:48%; border:1px solid Gray; float:left");
            } else if (data.d.length < 20) {
                document.getElementById('chartRevenue').setAttribute("style", "height:450px;font-size: 15px;margin-left: 21px; width:48%; border:1px solid Gray; float:left");
            } else if (data.d.length < 25) {
                document.getElementById('chartRevenue').setAttribute("style", "height:500px;font-size: 15px;margin-left: 21px; width:48%; border:1px solid Gray; float:left");
            } else if (data.d.length < 30) {
                document.getElementById('chartRevenue').setAttribute("style", "height:550px;font-size: 15px;margin-left: 21px; width:48%; border:1px solid Gray; float:left");
            } else if (data.d.length < 35) {
                document.getElementById('chartRevenue').setAttribute("style", "height:600px;font-size: 15px;margin-left: 21px; width:48%; border:1px solid Gray; float:left");
            } else if (data.d.length < 40) {
                document.getElementById('chartRevenue').setAttribute("style", "height:650px;font-size: 15px;margin-left: 21px; width:48%; border:1px solid Gray; float:left");
            } else if (data.d.length < 45) {
                document.getElementById('chartRevenue').setAttribute("style", "height:700px;font-size: 15px;margin-left: 21px; width:48%; border:1px solid Gray; float:left");
            } else if (data.d.length < 50) {
                document.getElementById('chartRevenue').setAttribute("style", "height:750px;font-size: 15px;margin-left: 21px; width:48%; border:1px solid Gray; float:left");
            }

            for (var i = 0; i < res.length; i++) {
                RestaurentName[i] = res[i].UserName;
                TotalRevenue[i] = (res[i].TotalRevenue / 1000).toFixed(2);
            }
            $(document).bind("kendo:skinChange", createRevenue(RestaurentName, TotalRevenue));
        },
        error: function (result) {

        }
    });
}


function createRevenue(RestaurentName, TotalRevenue) {
    $("#chartRevenue").kendoChart({
        title: {

            text: "Restaurants(Total Revenue)"
        },
        legend: {
            position: "bottom"
        },
        seriesDefaults: {
            type: "bar",
            labels: {
                visible: true,
                background: "transparent"
            },
            color: '#0879bf'
        },
        series: [{
            name: "Total Revenue(k)",
            data: TotalRevenue
        }],
        valueAxiss: {
            max: 1000000,
            line: {
                visible: true
            },
            minorGridLines: {
                visible: false
            },
            labels: {
                rotation: "auto"
            }
        },
        categoryAxis: {
            categories: RestaurentName,
            majorGridLines: {
                visible: false
            }
        }

    });

}


function createOrderChart(date1, date2) {
    $.ajax({
        type: "Get",
        url: "/DataTracker.svc/GetTotalOrderNoReports?startdate=" + date1 + "&enddate=" + date2 + "&Arrappidnotshowinmobilapp=" + Arrappidnotshowinmobilapp + "",
        dataType: "Json",
        success: function (data) {
            var Username = new Array();
            var TotalOrderNo = new Array();
            var TotalDeliverdNo = new Array();
            var TotalUnDeliverdNo = new Array();
            var res = data.d;
            if (data.d.length < 10) {
                document.getElementById('chartOrder').setAttribute("style", "height:350px;font-size: 15px; width:48%; border:1px solid Gray;");
            } else if (data.d.length < 15) {
                document.getElementById('chartOrder').setAttribute("style", "height:400px;font-size: 15px; width:48%; border:1px solid Gray;");
            } else if (data.d.length < 20) {
                document.getElementById('chartOrder').setAttribute("style", "height:450px;font-size: 15px; width:48%; border:1px solid Gray;");
            }
            else if (data.d.length < 25) {
                document.getElementById('chartOrder').setAttribute("style", "height:500px;font-size: 15px; width:48%; border:1px solid Gray;");
            } else if (data.d.length < 30) {
                document.getElementById('chartOrder').setAttribute("style", "height:550px;font-size: 15px; width:48%; border:1px solid Gray;");
            } else if (data.d.length < 35) {
                document.getElementById('chartOrder').setAttribute("style", "height:600px;font-size: 15px; width:48%; border:1px solid Gray;");
            } else if (data.d.length < 40) {
                document.getElementById('chartOrder').setAttribute("style", "height:650px;font-size: 15px; width:48%; border:1px solid Gray;");
            }
            else if (data.d.length < 45) {
                document.getElementById('chartOrder').setAttribute("style", "height:700px;font-size: 15px; width:48%; border:1px solid Gray;");
            }
            else if (data.d.length < 50) {
                document.getElementById('chartOrder').setAttribute("style", "height:750px;font-size: 15px; width:48%; border:1px solid Gray;");
            }


            for (var i = 0; i < res.length; i++) {
                Username[i] = res[i].UserName;
                TotalOrderNo[i] = res[i].TotalOrder;
                TotalDeliverdNo[i] = res[i].TotalDeliverd;
                TotalUnDeliverdNo[i] = res[i].TotalUnDeliverd;
            }

            $(document).bind("kendo:skinChange", CreateOrderChart(Username, TotalOrderNo, TotalDeliverdNo, TotalUnDeliverdNo));


        },
        error: function (result) {

        }
    });

}

function CreateOrderChart(Username, TotalOrderNo, TotalDeliverdNo, TotalUnDeliverdNo) {
    $("#chartOrder").kendoChart({
        title: {

            text: "Restaurants (Total Order )"
        },
        legend: {
            position: "bottom"
        },
        seriesDefaults: {
            type: "bar",
            //labels: {
            //    visible: true,
            //    background: "transparent"
            //},
            //color: '#0879bf'
            stack: true
        },
        series: [{
            name: "Total Deliverd",
            data: TotalDeliverdNo,
            //labels: {
            //    visible: true,
            //    position: "center",
            //    background: "transparent"
            //},
            color: '#46b54d'
        }, {
            name: "Total UnDeliverd",
            data: TotalUnDeliverdNo,
            //labels: {
            //    visible: true,
            //    //position: "center",
            //    background: "transparent"
            //},
            color: '#aaaaaa'
        }],
        valueAxiss: {
            max: 700000,
            minorGridLines: {
                visible: true
            },
            //labels: {
            //    rotation: "auto"
            //}
        },
        categoryAxis: {
            categories: Username,
            majorGridLines: {
                visible: false
            }
        },
        tooltip: {
            visible: true,
            template: "#= series.name #: #= value #"
        }
    });
}

var dragId; var DropId; var myString; var my;
function allowDrop(ev) {
    ev.preventDefault();
}
function drag(ev) {
    dragId = ev.target.id;
    var avoid = "sub_";
    myString = dragId.replace(avoid, '');
    console.log("Ev.target=" + ev.target.id);
    //console.log(ev.target.id);
    ev.dataTransfer.setData('Text/html', ev.target.id);
}

function drop(ev) {

    ev.preventDefault();
    DropId = ev.target.id;
    var avoid = "sub_";
    my = DropId.replace(avoid, '');
    var avoidl1 = "list";
    var b = myString.replace(avoidl1, '');
    var a = my.replace(avoidl1, '');
    console.log("DropId=" + a + " ," + "dragId=" + b);
    ev.preventDefault();
    SwapSubTask(b, a);
    dragId = "";
    DropId = "";
    myString = "";
    my = "";
    b = "";
    a = "";
    console.log("dragId=" + dragId + "," + "DropId=" + DropId + "," + "myString=" + myString + "," + "my=" + my + "," + "b=" + b + "," + "a=" + a);
    // alert(data)
}
function SwapSubTask(oldsubtaskid, newSubtaskid) {
    $.ajax({
        type: "GET",
        contentType: "application/json; charset=utf-8",
        url: "/DataTracker.svc/GetSwapPriortySubtasks?oldLi=" + oldsubtaskid + "&NewLi=" + newSubtaskid + "",
        dataType: "json",
        success: function (data) {
            setTimeout(GetallSubtasks(s), 1000);
        }
    })
}


/////// Ravi Code Start ////
function Home() {
    //    var Board_home = [];
    //    //document.getElementById("BoardList").innerHTML = "";
    //    $.ajax({
    //        type: "GET",
    //        url: "/DataTracker.svc/GetRecentBoards",
    //        contentType: "application/json; charset=utf-8",
    //        dataType: "json",
    //        success: function (data) {
    //            $.each(data.d, function (index, item) {
    //                Board_home = Board_home.concat(item.BoardID);
    //            });

    var urll = "AdminDashboard.aspx";
    //             + Board_home[0];
    window.location.href = urll;

    //        }
    //    });
}
////////Ravi Code Ends ////


function EmailPreference() {
    var emailPreferenceValue = $("input[name='rdbcommon']:checked").val();

    $.ajax({
        type: "POST",
        url: "/DataTracker.svc/SaveEmailOreference",
        data: "{\"EmailPreference\":\"" + emailPreferenceValue + "\"}",
        contentType: "application/json; charset=utf-16",
        dataType: "json",
        success: function (data) {
            alert("Saved successfully.");
        },
        error: function (xhr, status, error) {

        }
    });
}
function Exit() {
    var dialog = $("#WinChangePass").data("kendoWindow");
    dialog.close();

}

function ExportRegularTaskList() {
    if ($.browser.safari) {
        alert("Safari will not properly Export as excel");
        return false;
    }
    else {
        $('#ExportWebscrapData').val("Please wait....");
        $.ajax({
            type: "POST",
            url: "/DataTracker.svc/ExportRegularTaskList",
            data: "{}",
            contentType: "application/json; charset=utf-16",
            dataType: "json",
            success: function (data) {

                var response = JSON.parse(data.d);
                fadname = 'WebscrapReport.xlsx';
                var rows = [{
                    cells: [
                { value: "Job_Number" },
                { value: "StoreName" },
                { value: "Address" },
                { value: "City" },
                { value: "ST" },
                { value: "Zip" },
                { value: "StartDate" },
                { value: "TargetDate" },
                { value: "User" },
                { value: "LastUpdatedTime" },
                { value: "StatusName" }
                    ]
                }];

                $.each(response, function (index, item) {

                    var dataItem = response[index];

                    rows.push({
                        cells: [
                    { value: dataItem.Job_Number },
                    { value: dataItem.StoreName },
                    { value: dataItem.Address },
                    { value: dataItem.City },
                    { value: dataItem.ST },
                    { value: dataItem.Zip },
                    { value: dataItem.StartDate },
                    { value: dataItem.TargetDate },
                    { value: dataItem.User },
                    { value: dataItem.LastUpdatedTime },
                    { value: dataItem.StatusName }
                        ]
                    })

                });
                if (rows != undefined && rows != '') {
                    $('#ExportWebscrapData').val("Export to excel");
                    excelExport(rows);
                    e.preventDefault();
                }
                else {
                    alert('Please wait and try again');
                    return false;
                }
            },
            error: function (xhr, status, error) {

            }
        });
    }
}

function excelExport(rows) {
    var workbook = new kendo.ooxml.Workbook({
        sheets: [
        {
            columns: [
            { autoWidth: true },
            { autoWidth: true }
            ],
            title: "WebscrapReport",
            rows: rows
        }
        ]
    });
    kendo.saveAs({ dataURI: workbook.toDataURL(), fileName: fadname });

}