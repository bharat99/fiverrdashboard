﻿
alldatafun = {
    BindGridData: function () {
        
        var d1 = $("#datepicfrom").val();
        var d2 = $("#datepicto").val();
        
        $("#Gridforscriptdata")[0].innerHTML = "";
        if ($("#Gridforscriptdata").data("kendoWindow") != null) {
            $('#Gridforscriptdata').data().kendoGrid.destroy();
            $('#Gridforscriptdata').empty();
        }
        var dataSourceDetail = "";
        dataSourceDetail = new kendo.data.DataSource({
            pageSize: 15,
            serverPaging: true,
            schema: {
                data: "d",
                total: function (d) {
                    if (d.d.length > 0)
                        return d.d[0].TotalCount
                },
                model: {
                    id: "ID",
                    fields: {
                        ID: { editable: false, type: "number" },
                        WebsiteName: { editable: false, type: "string" },
                        reportedBy: { editable: false, type: "string" },
                        Adstype: { editable: false, type: "string" },
                        reportedDate: { editable: false, type: "string" }


                    }
                }

            },

            transport: {
                read: function (options) {
                    $.ajax({
                        type: "GET",
                        url: "/DataTracker.svc/GetAnalysisData",
                        dataType: "json",
                        data: {

                            Page: options.data.page,
                            PageSize: options.data.pageSize,
                            skip: options.data.skip,
                            take: options.data.take,
                            sdate: d1,
                            ldate: d2
                        },

                        success: function (result) {
                            options.success(result);

                        }
                    });
                }
            }
             
        });
        if ($("#Gridforscriptdata").length > 0)
        { $("#Gridforscriptdata")[0].innerHTML = ""; }
        var element = $("#Gridforscriptdata").kendoGrid({
            dataSource: dataSourceDetail,
            dataBound: function () {

            },
            pageable: {
                // batch: true,
                pageSize: 15,
                refresh: true,
                pageSizes: true,
                input: true
            },
            columns: [
                            {
                                field: "WebsiteName",
                                title: "Website Name",
                                width: "40%"
                            },
                                                        {
                                                            field: "reportedBy",
                                                            title: "ReportedBy",
                                                            width: "20%"
                                                        },
                                                        {
                                                            field: "Adstype",
                                                            title: "Ads type",
                                                            width: "20%"
                                                        },
                                                        {
                                                            field: "reportedDate",
                                                            title: "Reported Date",
                                                            width: "20%"
                                                        }


                        ],
            editable: false,
            sortable: true,
            selectable: true

        });

    }
};





