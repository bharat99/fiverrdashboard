﻿var tID = '';
function GetTimesheetList(TaskIdd,C_TypeId) {
    tID = TaskIdd;
    if ($("#DivTimesheetGrid").data("kendoWindow") != null) {
        $('#DivTimesheetGrid').data().kendoGrid.destroy();
        $('#DivTimesheetGrid').empty();
    }
    var dataSourceDetail = "";
    dataSourceDetail = new kendo.data.DataSource({

        schema: {
            data: "d",
            total: function (d) {
                if (d.d.length > 0)
                    return d.d[0].TotalCount
            },
            model: {
                id: "ID",
                fields: {
                    ID: { editable: false, type: "number" },
                    Tdate: { editable: false, type: "string" },
                    WUser: { editable: false, type: "string" },
                    worktime: { editable: false, type: "string" },
                    worktimeinformate: { editable: false, type: "string" },
                    dateforexport: { editable: false, type: "string" },
                    FormateInfo: { editable: false, type: "string" },
                    UserId: { editable: false, type: "number" }
                }
            }

        },

        transport: {
            read: function (options) {
                $.ajax({
                    type: "GET",
                    url: "/DataTracker.svc/GetAllTaskTimeSheet",
                    dataType: "json",
                    data: {

                        Page: options.data.page,
                        PageSize: options.data.pageSize,
                        skip: options.data.skip,
                        take: options.data.take,
                        TaskId: TaskIdd
                    },

                    success: function (result) {
                        options.success(result);

                    }
                });
            }
        }

    });
    if ($("#DivTimesheetGrid").length > 0)
    { $("#DivTimesheetGrid")[0].innerHTML = ""; }
    if(C_TypeId==14 || C_TypeId==12){
    var element = $("#DivTimesheetGrid").kendoGrid({
        dataSource: dataSourceDetail,
        dataBound: function () {

        },

        columns: [

                            {
                                field: "WUser",
                                title: "Name",
                                width: "20%",
                                template: "#=SetCompletedName(WUser)#"
                            },
                                                        {
                                                            field: "Tdate",
                                                            title: "Date",
                                                            width: "18%"
                                                        },
                            {
                                field: "worktime",
                                title: "Time",
                                width: "12%"


                            },
                                                       {
                                                           field: "FormateInfo",
                                                           title: "Comments",
                                                           width: "40%",
                                                           template: "#=SetCompletedText(FormateInfo)#"
                                                       },

                                                        {
                                                            field: "worktimeinformate",
                                                            title: "WorkTime",
                                                            width: "0%",
                                                            hidden: true


                                                        },
                                                          {
                                                              field: "dateforexport",
                                                              title: "date",
                                                              width: "0%",
                                                              hidden: true
                                                          },
                                                           { command: { text: "Screenshot", click: OpenScreenshotsWindow }, title: "", width: "10%" } //OpenScreenshotWindow
                        ],
        editable: false,
        sortable: true,
        selectable: true,
         change: checkline,

    });
    }
    else
    {
     var element = $("#DivTimesheetGrid").kendoGrid({
        dataSource: dataSourceDetail,
        dataBound: function () {

        },

        columns: [

                            {
                                field: "WUser",
                                title: "Name",
                                width: "20%",
                                template: "#=SetCompletedName(WUser)#"
                            },
                                                        {
                                                            field: "Tdate",
                                                            title: "Date",
                                                            width: "18%"
                                                        },
                            {
                                field: "worktime",
                                title: "Time",
                                width: "12%"


                            },
                                                       {
                                                           field: "FormateInfo",
                                                           title: "Comments",
                                                           width: "50%",
                                                           template: "#=SetCompletedText(FormateInfo)#"
                                                       },

                                                        {
                                                            field: "worktimeinformate",
                                                            title: "WorkTime",
                                                            width: "0%",
                                                            hidden: true


                                                        },
                                                          {
                                                              field: "dateforexport",
                                                              title: "date",
                                                              width: "0%",
                                                              hidden: true
                                                          }
                                                          // { command: { text: "Screenshot", click: OpenScreenshotsWindow }, title: "", width: "10%" } //OpenScreenshotWindow
                        ],
        editable: false,
        sortable: true,
        selectable: true,
         change: checkline,

    });
    }
}

function SetCompletedText(Tdate) {
    return Tdate;
//    if (Tdate == null) {
//        return "No Comment";
//        else{
//        var Allcmnt=Tdate.split(',');
//        if(Allcmnt.length==1)
//        return ''
//        else{

//        }
//        }
//    }
}

//function ShowAllCmnt() { }


function ShowPopTaskTimeSheetT(TaskId,tidds) {
//C_TypeId=tidds;
    if ($("#DivTimeSheetCom").data("kendoWindow") != null) {
        var StatusReport = $("#DivTimeSheetCom").data("kendoWindow");
        StatusReport.close();
    }

    var windowElement = $("#DivTimeSheetCom").kendoWindow({
        iframe: true,
        // content: "WorkItemStatusComment.aspx?childSID=" + childSID + "&Statusvalue=" + Statusvalue + "",
        content: "AllComment_TimeSheet.htm?TaskId=" + TaskId + "&tid="+tidds+"",
        modal: true,
        width: "960px",
        title: "Task Time Sheet",
        height: "560px",
        position: {
            top: 10,
            left: 2
        },
        resizable: false,
        actions: ["Close"],
        draggable: false

    });
    var dialog = $("#DivTimeSheetCom").data("kendoWindow");
    dialog.open();
    dialog.center();
    return false;
}

function ShowAllCmnt(CurrentDate) {
    var datar = '';
    var cnt = 0;
    $.ajax({
        type: "GET",
        contentType: "application/json; charset=utf-8",
        url: "/DataTracker.svc/GetAllTaskTimeSheetComments",
        dataType: "json",
        data: {
            TaskId: tID,
            CoDate: CurrentDate
        },
        success: function (data) {
            $.each(data.d, function (index, item) {

                if (item.counter > 1) {
                    var predata = '';
                    if (cnt > 0) {
                        predata = ('<p><span>' + item.comments + '</span><span>' + item.CBy + '</span><span>' + item.Cdate + '</span></p>');
                        cnt++;
                    }
                    else {
                        datar = ('<p><span>' + item.comments + '</span><span>' + item.CBy + '</span><span>' + item.Cdate + '</span><span style="color:blue;" title=' + predata + '>more...<span></p>');
                        cnt++;
                    }
                }
                else
                    datar = ('<p><span>' + item.comments + '</span><span>' + item.CBy + '</span><span>' + item.Cdate + '</span></p>');
                return datar;

            });
        },
        error: function (result) {

        }
    });

}
function OpenAllCommentPOPLI() {
    if ($("#AllComenntListK").data("kendoWindow") != null) {
        var StatusReport = $("#AllComenntListK").data("kendoWindow");
        StatusReport.close();
    }

    var windowElement = $("#AllComenntListK").kendoWindow({
        iframe: true,
        // content: "WorkItemStatusComment.aspx?childSID=" + childSID + "&Statusvalue=" + Statusvalue + "",
        // content: "AllComment_TimeSheet.htm",
        modal: true,
        width: "600px",
        title: "Comments",
        height: "280px",
        position: {
            top: 10,
            left: 2
        },
        resizable: false,
        actions: ["Close"],
        draggable: false

    });
    var dialog = $("#AllComenntListK").data("kendoWindow");
    dialog.open();
    dialog.center();
    return false;
}

function AddHeader() {

    var table = document.getElementById("Tableforcomment");
    var header = table.createTHead();
    var row = header.insertRow(0);
    var cell = row.insertCell(0);
    cell.innerHTML = "<b style='width: 321px;height: 30px;'>Comments</b>";
    var cell1 = row.insertCell(1);
    cell1.innerHTML = "<b style='border-left: thin solid gray;width: 321px;height: 30px;'>Commented By</b>";
    var cell2 = row.insertCell(2);
    cell2.innerHTML = "<b style='border-left: thin solid gray;width: 321px;height: 30px;'>Date</b>";
    OpenAllCommentPOPLI();
}
function SetCompletedName(name) {
    return name;
}

function DisplayMoreCmnt(idd) {
//DivAllCmntsshow6
    if ($('#DivAllCmntsshow'+idd+'').css('display') == 'none'){
        $('#DivAllCmntsshow'+idd+'')[0].style.display = "block";
        $('#Spanup'+idd+'')[0].style.display = "inline-block";
        $('#SpanDown'+idd+'')[0].style.display = "none";
        }
    else
    {
        $('#DivAllCmntsshow'+idd+'')[0].style.display = "none";
                $('#Spanup'+idd+'')[0].style.display = "none";
        $('#SpanDown'+idd+'')[0].style.display = "inline-block";
        }
}

function checkline(){
 $('.k-state-selected').removeClass('k-state-selected k-state-selecting'); 
}

function OpenScreenshotsWindow(e){
    e.preventDefault();

    var dataItem = this.dataItem($(e.currentTarget).closest("tr"));
    if(dataItem !=null){
            var ID = dataItem.UserId;
        var sdate = dataItem.dateforexport;
        var ldate = dataItem.dateforexport;
        OpenScreenshotWindowfromgrid(sdate + "~" + ldate + "~" + ID + "~" + "00", "");
        
    }
}