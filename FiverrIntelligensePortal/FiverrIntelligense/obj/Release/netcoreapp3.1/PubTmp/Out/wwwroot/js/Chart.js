﻿$(document).ready(function () {

    GetTaskByDate();
});

function SetText(id) {
    
    if (id.value == "Search") {
        id.value = "Searching...";
    }
    else
        id.value = "Search"; //return to the previous value.
    return false;
}

function validate(){
    
    var datePicker = document.getElementById('ctl00_ContentMain_date_pick_dateInput');
    if (datePicker.value == "") {
        alert('Please Select Date');
        return false;
    }
}
function sorting2(b) {
    if (b != 1) {
        document.getElementById('BreakUp').style.display = "block";
    }
    else {
        document.getElementById('BreakDown').style.display = "block";
    }
}
function sorting3(b) {
    if (b != 1) {
        document.getElementById('IdleUp').style.display = "block";
    }
    else {
        document.getElementById('IdleDown').style.display = "block";
    }
}

function sorting(a, b) {
    if (a.innerText == "Names") {
        if (b != 1) {
            document.getElementById('NameUp').style.display = "block";
        }
        else {
            document.getElementById('NameDown').style.display = "block";
        }
    }

    if (a.innerText == "Time Worked") {
        if (b != 1) {
            document.getElementById('TimeUp').style.display = "block";
        }
        else {
            document.getElementById('TimeDown').style.display = "block";
        }
    }

}

function GetTaskByDate() {
    
    $('.loader').css("display", "block");
    var result = [];
    var dataPush = '';
    var Date = $("#ctl00_ContentMain_date_pick_dateInput").val();
    $.ajax({
        type: "POST",
        url: "/DataTracker.svc/NewTaskDetails",
        data: "{\"date\": \"" + Date + "\"}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: false,
        success: function (data) {
            
            document.getElementById('example').innerHTML = "";
            if (data.d == null) {
                $('.loader').css("display", "none");
                return;
            }
            if (data.d.length > 2) {
                $('.loader').css("display", "block");
                var chart = visavailChart();
                function draw_visavail() {
                    chart.width($('#visavail_container').width() - 150);
                    d3.select("#example")
                            .datum(JSON.parse(data.d))
                            .call(chart);
                }
                $('#btnSave').val("Search");
                draw_visavail();
                $('.loader').css("display", "none");
            }
            else {
                $('.loader').css("display", "none");
                $('#btnSave').val("Search");
            }
            
         
        }

    });
    $('#btnSave').val("Search");
}

function GetTaskTeamWise() {
    
    $('.loader').css("display", "block");
    var CheckBoxvalue = "False";
    var Date = $("#ctl00_ContentMain_date_pick_dateInput").val();
    if ($('#ContentMain_CheckBox1').is(":checked")) {
        CheckBoxvalue = "True";
    }
    else {
        CheckBoxvalue = "False";
    }
    var value = $('#ctl00_ContentMain_combo_user_Input').val();
    $.ajax({
        type: "POST",
        url: "/DataTracker.svc/task",
        data: "{\"drpvalue\": \"" + value + "\",\"date\": \"" + Date + "\",\"CheckBoxvalue\": \"" + CheckBoxvalue + "\"}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: false,
        success: function (data) {
            
           document.getElementById('example').innerHTML = "";
            if (data.d == null)
            {
                $('.loader').css("display", "none");
                return;
            }
            if (data.d.length > 2 ) {
                $('.loader').css("display", "block");
                if (CheckBoxvalue == "False")
                {
                    var chart = visavailChart();
                    function draw_visavail() {
                        chart.width($('#visavail_container').width() - 150);
                        d3.select("#example")
                                .datum(JSON.parse(data.d))
                                .call(chart);
                    }
                    $('#btnSave').val("Search");
                    draw_visavail();
                    $('.loader').css("display", "none");
                }
                else if (CheckBoxvalue == "True") {

                    $.each(JSON.parse(data.d), function (index, item) {
                        if (item.workingtime == "00:00:00") {
                            $('#example').append("<tbody><tr><td style='font-weight:bold; width:134px;'>" + item.fname + "</td><td>" + item.workingtime + "</td><td>" + item.idletime + "</td><td>" + item.breaktimeinsec + "</td> </tr></tbody>");
                            $('.loader').css("display", "none");
                        }
                    });
                    $('#btnSave').val("Search");
                   

            }
             
         }
         else {
                $('.loader').css("display", "none");
             $('#btnSave').val("Search");
         }
        }
    });
    $('#btnSave').val("Search");
}