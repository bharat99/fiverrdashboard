﻿var cdate = "";
var Titleforpop = "";
var SiteNameforpop = "";
var CidForpop = "";
var CycleName = "";
var arrStatusm = new Array();
function GetAllPriceData(CycleId, cdate, cname) {
    //cdate = cdate;
    abortRequestsm();
    document.getElementById("ContentMain_HiddenField1").value = CycleId + "~" + cdate;
    $("#AllDataMainTable tr").remove();
    $("#AllDataMainTable")[0].innerHTML = "";
    $("#SpanForDate")[0].innerHTML = "";
    if (cname != undefined)
        $("#SpanForDate").append(cdate + " " + cname);
    else
        $("#SpanForDate").append(cdate);
    // $("#SpanForDate").append(cdate + " " + CycleName);
    CycleName = "";
    // $("#content1")[0].style.display = "none";
    $("#content2")[0].style.display = "none";
    $("#HighlightDiv")[0].style.display = "block"
    $("#AllDataMainTable").append("<thead><tr><th>Product Name</th><th></th><th class='tc'>Amazon</th><th class='tc'>Snapdeal</th><th class='tc'>Big Basket</th></tr></thead>");
    arrStatusm[0] = $.ajax({
        type: "GET",
        contentType: "application/json; charset=utf-8",
        url: "/DataTracker.svc/GetSPForTitle?DDate=" + cdate + "&Dcid=" + CycleId + "",
        dataType: "json",
        success: function (data) {
            if (data.d != null && data.d != "") {
                var adata = "";
                var sdata = "";
                var bdata = "";
                $.each(data.d, function (index, item) {
                    $("#SpanForDate").append();
                    adata = item.APrice;
                    var aadata = adata.split('~');
                    sdata = item.SPrice;
                    var ssdata = sdata.split('~');
                    bdata = item.BPrice;
                    var bbdata = bdata.split('~');
                    var sitea = "Amazon";
                    var sites = "Snapdeal";
                    var siteb = "Bigbasket";

                    var ccdata = ColorFor(aadata[2], aadata[1]);
                    var sssdata = ColorFor(ssdata[2], ssdata[1]);
                    var bbaData = ColorFor(bbdata[2], bbdata[1]);
                    $("#AllDataMainTable").append("<tr><td class='product_name'>" + item.Title + "</td><td><p>MRP</p><h4>SP</h4></td><td><p><a href='#'>" + ForPriceRs(aadata[0]) + "</a></p><h4><a href='#' onclick='javascript:BeforePopup(\"" + item.Title + "\",\"" + CycleId + "\",\"" + aadata[1] + "\",\"" + sitea + "\");'>" + ccdata + "</a></h4></td><td><p><a href='#'>" + ForPriceRs(ssdata[0]) + "</a></p><h4><a href='#' onclick='javascript:BeforePopup(\"" + item.Title + "\",\"" + CycleId + "\",\"" + ssdata[1] + "\",\"" + sites + "\");'>" + sssdata + "</a></h4></td><td><p><a href='#'>" + ForPriceRs(bbdata[0]) + "</a></p><h4><a href='#'onclick='javascript:BeforePopup(\"" + item.Title + "\",\"" + CycleId + "\",\"" + bbdata[1] + "\",\"" + siteb + "\");'>" + bbaData + "</a></h4></td></tr>");
                });
                $("#HighlightDiv")[0].style.display = "none"
                $("#content2")[0].style.display = "block"
            }
            else if (data.d == "") {
                $("#AllDataMainTable")[0].innerHTML = "";
            }
            else {
                $("#AllDataMainTable")[0].innerHTML = "";
            }
        },
        error: function (result) {

        }
    });
    GetAllOStockData(CycleId);
    // CycleName = "";
}
function ForPriceRs(dval) {
    if (dval == "N/A") {
        return dval;
    }
    else {
        return "Rs. " + dval + "/-";
    }

}
function ColorFor(ddd, content) {
    if (content == "N/A")
        return content;
    else if (ddd == "True")
        return "<span style='color:red;'>Rs. " + content + "/-</span>";
    else
        return "Rs. " + content + "/-";


}

function BeforePopup(Title, Cid, Isneed, sitename) {
    if (Isneed == "Stock") {
        OpenAllsellerPopUp(Title, Cid, sitename, "S");
    }

    else if (Isneed != "N/A") {
        OpenAllsellerPopUp(Title, Cid, sitename, "P");

    }
    else {
        alert("No Record");
        return false;
    }

}
////
function GetAllOStockData(CycleId) {
   // abortRequestsm();
    $("#SEcondTableAllData")[0].style.display = "block";
    $("#AllDataOSTable")[0].innerHTML = "";
    $("#AllDataOSTable").append("<thead><tr><th>Product Name</th><th class='tc'>Amazon</th><th class='tc'>Snapdeal</th><th class='tc'>Big Basket</th></tr></thead>");
   arrStatusm[1]= $.ajax({
        type: "GET",
        contentType: "application/json; charset=utf-8",
        url: "/DataTracker.svc/GetOSForTitle?DDate=" + cdate + "&Dcid=" + CycleId + "",
        dataType: "json",
        success: function (data) {
            if (data.d != null && data.d != "") {
                var sitea = "Amazon";
                var sites = "Snapdeal";
                var siteb = "Bigbasket";
                $.each(data.d, function (index, item) {
                    var FOrwhat = "Stock";
                    $("#AllDataOSTable").append("<tr><td class='product_name'>" + item.Title + "</td> <td><p><a href='#' onclick='javascript:BeforePopup(\"" + item.Title + "\",\"" + CycleId + "\",\"" + FOrwhat + "\",\"" + sitea + "\");'>" + item.AOStock + "</a></p></td><td><p><a href='#' onclick='javascript:BeforePopup(\"" + item.Title + "\",\"" + CycleId + "\",\"" + FOrwhat + "\",\"" + sites + "\");'>" + item.SOStock + "</a></p></td><td><p><a href='#' onclick='javascript:BeforePopup(\"" + item.Title + "\",\"" + CycleId + "\",\"" + FOrwhat + "\",\"" + siteb + "\");'>" + item.BOStock + "</a></p></td></tr>");
                });
            }
            else if (data.d == "") {
                $("#AllDataOSTable")[0].innerHTML = "";
                $("#SEcondTableAllData")[0].style.display = "none";
            }
            else {
                $("#AllDataOSTable")[0].innerHTML = "";
                $("#SEcondTableAllData")[0].style.display = "none";
            }
        },
        error: function (result) {
            $("#SEcondTableAllData")[0].style.display = "none";

        }
    });
}



function OnMaricoDateChange() {
    BackToDataPoints();
    cdate = $("#SelectDateForMarico").val()
    AddDataPoints(cdate);
}

function AddDataPoints(CurrentDate) {
    FindIsAnyFlagIsTrue(CurrentDate);

}

function GetDataPointTime(cId) {
    if (cId == 1) {
        return "10:00 am";
    }
    else if (cId == 2) {
        return "01:00 pm";
    }
    else if (cId == 3) {
        return "04:00 pm";
    }
    else if (cId == 4) {
        return "07:00 pm";
    }
    else if (cId == 5) {
        return "10:00 pm";
    }
    else if (cId == 6) {
        return "12:00 am";
    }
}
function FindIsAnyFlagIsTrue(Time) {
    $("#AllDataPoints")[0].innerHTML = "";

    $.ajax({
        type: "GET",
        contentType: "application/json; charset=utf-8",
        url: "/DataTracker.svc/GetIsCyecleHaveFlagTrue?time=" + Time + "",
        dataType: "json",
        success: function (data) {

            if (data.d != null) {
                var cntt = 1;
                $.each(data.d, function (index, item) {

                    var sss = item.IsNeedToAdd.split('~');
                    sss[0]
                    if (sss[0] == "yes") {
                        var tmval = GetDataPointTime(cntt);
                        // CycleName = sss[1];
                        $("#AllDataPoints").append("<li><a href='#' class='alert' onclick='javascript:GetAllPriceData(\"" + cntt + "\",\"" + Time + "\",\"" + sss[1] + "\");'>" + sss[1] + "</a></li>");
                    }
                    else if (sss[0] == "no") {
                        $("#AllDataPoints").append("<li><a href='#' onclick='javascript:GetAllPriceData(\"" + cntt + "\",\"" + Time + "\",\"" + sss[1] + "\");'>" + sss[1] + "</a></li>");
                    }

                    cntt++;
                });
            }
            else {
                return null;
            }

        },
        error: function (result) {
            return null;
        }
    });
    BindAllHighLightPriceData(Time);
}

function BackToDataPoints() {
    $("#HighlightDiv")[0].style.display = "block";
    $("#content2")[0].style.display = "none";
}



function OpenAllsellerPopUp(Title, Cid, sitename, FW) {
    var fulltitle = "";
    fulltitle = "<span style='font-size: 16px;'>" + sitename + "</span>" + " - " + "<span style='color: #0878c0;font-size: 16px;'>" + Title + "</span>"
    if ($("#Divallseller").data("kendoWindow") != null) {
        var StatusReport = $("#Divallseller").data("kendoWindow");
        StatusReport.close();
    }
    var alldata = encodeURIComponent(Title.trim()) + "~" + sitename.trim() + "~" + Cid.trim() + "~" + cdate + "~" + FW;
    var windowElement = $("#Divallseller").kendoWindow({
        iframe: true,
        content: "AllSellerForMarico.htm?Titles=" + alldata + "",
        // content: "AllSellerForMarico.htm",
        modal: true,
        width: "400px",
        // title: sitename,
        height: "280px",
        position: {
            top: 10,
            left: 2
        },
        resizable: false,
        actions: ["Close"],
        draggable: false

    });
    var dialog = $("#Divallseller").data("kendoWindow");
    dialog.setOptions({
        title: fulltitle
    });
    dialog.open();
    dialog.center();
    return false;
}

function ShowAllSellers(Titleforpop, SiteNameforpop, CidForpop, ccdate) {
    $("#PopUpSellerTable")[0].innerHTML = "";
    $("#PopUpSellerTable").append("<thead><tr><th class='tc'>Sellers</th><th class='tc'>Price</th></tr></thead>");
    $.ajax({
        type: "GET",
        contentType: "application/json; charset=utf-8",
        url: "/DataTracker.svc/GetAllSellersList?date=" + ccdate + "&sitename=" + SiteNameforpop + "&title=" + encodeURIComponent(Titleforpop) + "&cid=" + CidForpop + "",
        dataType: "json",
        success: function (data) {
            if (data.d != null && data.d != "") {
                $.each(data.d, function (index, item) {
                    $("#PopUpSellerTable").append("<tr><td class='tc'>" + item.SellerName + "</td><td class='tc'>" + item.IsInStock + "</td></tr>");
                });
            }
            else {

            }
        },
        error: function (result) {


        }
    });
}

function ShowAllSellersforStock(Titleforpop, SiteNameforpop, CidForpop, ccdate) {
    $("#PopUpSellerTable")[0].innerHTML = "";
    $("#PopUpSellerTable").append("<thead><tr><th class='tc'>Sellers</th><th class='tc'>Stock Info</th></tr></thead>");
    $.ajax({
        type: "GET",
        contentType: "application/json; charset=utf-8",
        url: "/DataTracker.svc/GetAllSellersListForStock?date=" + ccdate + "&sitename=" + SiteNameforpop + "&title=" + encodeURIComponent(Titleforpop) + "&cid=" + CidForpop + "",
        dataType: "json",
        success: function (data) {
            if (data.d != null && data.d != "") {
                $.each(data.d, function (index, item) {
                    $("#PopUpSellerTable").append("<tr><td class='tc'>" + item.SellerName + "</td><td class='tc'>" + item.IsInStock + "</td></tr>");
                });
            }
            else {

            }
        },
        error: function (result) {


        }
    });
}

function BindAllHighLightPriceData(Time) {
    $("#Table1Highlight")[0].innerHTML = "";
    //  var dddd = getformateddate(Time);
    $("#Table1Highlight").append("<tr><td><h4>" + getformateddate(Time) + "</h4><h2>highlights for today</h2></td></tr>");
    $.ajax({
        type: "GET",
        contentType: "application/json; charset=utf-8",
        url: "/DataTracker.svc/GetHighLightData?date=" + Time + "",
        dataType: "json",
        success: function (data) {
            if (data.d != null && data.d != "") {
                $.each(data.d, function (index, item) {
                    $("#Table1Highlight").append("<tr><td style='float: left;'><h4 >" + item.AllPriceData + "</h4></td></tr>");
                });
            }
            else {

            }
        },
        error: function (result) {


        }
    });
    BindAllHighLightStockData(Time);
}

function BindAllHighLightStockData(Time) {
    
    $.ajax({
        type: "GET",
        contentType: "application/json; charset=utf-8",
        url: "/DataTracker.svc/GetHighLightDataStock?date=" + Time + "",
        dataType: "json",
        success: function (data) {
            if (data.d != null && data.d != "") {
                $.each(data.d, function (index, item) {
                    $("#Table1Highlight").append("<tr><td style='float: left;'><h4 >" + item.AllDataIsInStock + "</h4></td></tr>");
                });
            }
            else {

            }
        },
        error: function (result) {


        }
    });
}

function getformateddate(datee) {
    var d = new Date(datee);
    var dayofweek = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'][d.getDay()],
date = d.getDate(),
month = ['January', 'February', 'March', 'April', 'May', 'June',
'July', 'August', 'September', 'October', 'November', 'December'][d.getMonth()],
year = d.getFullYear();

    return (["<strong>" + dayofweek + "</strong>, ", date, month, year].join(' '));
}

function ReloadMaricopage() {
    window.location = "/MaricoPriceReport.aspx";
}

function abortRequestsm() {
    for (var e = 0; e < 13; e++) {
        if (arrStatusm[e] != null) {
            arrStatusm[e].abort()
        }
    }
}
