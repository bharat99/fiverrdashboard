﻿$(document).ready(function () {
    $('.bar').hide();
    var d = new Date();
    var addmonth = d.getMonth()+1;
    var adddays = d.getDate();
    if (addmonth < 10)
        addmonth = "0" + addmonth;
    if (adddays < 10)
        adddays = "0" + adddays;
    var CurrentDate = d.getFullYear() + "-" + addmonth + "-" + adddays;
    var FirstDateDate = d.getFullYear() + "-" + addmonth + "-" + "01";

    $('#SpStartDate').text(FirstDateDate);
    $('#SpEndDate').text(CurrentDate);
    BindTeam();
    $('#btnSearch').click(function () {
       
        $('#DivReport').html("");
      

        var TeamID = $('#ddlBindTeam').val();
        var StartDate = $('#SpStartDate').text();
        var EndDate = $('#SpEndDate').text();


        if (TeamID != "" && StartDate != "" && EndDate != "" && TeamID != undefined && StartDate != undefined && EndDate != undefined) {
            // var TID = JSON.stringify(TeamID);

            $.ajax({
                type: "POST",
                url: "/DataTracker.svc/GetBoardReportTeamWise",
                //data: "{\"TeamID\": \"" + TID + "\",\"startdate\": \"" + StartDate + "\",\"enddate\": \"" + EndDate + "\"}",
                data: JSON.stringify({ TeamID: TeamID, startdate: StartDate, enddate: EndDate }),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                 
                   
                    
                    $('#DivReport').html(data.d);
                   // $('#DivReport').html(data);
                   // $('#DivReport').InnerHtml(dw);
                },
               
                    beforeSend: function () {                       
                        $('.bar').show();                      
                    },
            complete: function () {

                $('.bar').hide();                       
            },
           
                error: function (err) {

                }
            });
        }
        //else
        //{
        //    alert("Start Date,End Date and Team Name cannot be blank !");
        //}
    });
});

function BindTeam() {
    $.ajax({
        url: "/DataTracker.svc/BindTeamForTeamWiseReport",
        type: "GET",
        dataType: "JSON",
        contentType: "application/json;charset=utf-8",
        async: false,
        success: function (data) {
            var xmlDoc = $.parseXML(data.d);
            var xml = $(xmlDoc);
            var table = xml.find("Table1");
            if (table.length > 0) {
                $(table).each(function () {
                    var teamID = $(this).find("id").text();
                    var TeamName = $(this).find("TeamName").text();
                    if (teamID == "62") {
                        $("#ddlBindTeam").append($("<option selected='selected'></option>").val(teamID).html(TeamName));
                    }
                    else { $("#ddlBindTeam").append($("<option></option>").val(teamID).html(TeamName)); }
                   
                });
            }
            else {

                // ddlBindTeam.append("<option selected='selected' value='0'>None</option>");
            }

        },
        error: function () {

        }
    });
}