﻿function ShowStatusReport(StoreName, StartDate,Job_Number, TType) {

    $("#StatusReportGrid")[0].innerHTML = "";
    if ($("#StatusReportGrid").data("kendoGrid") != null) {
        $('#StatusReportGrid').data().kendoGrid.destroy();
        $('#StatusReportGrid').empty();
    }
    var dataSourceDetail = "";
    dataSourceDetail = new kendo.data.DataSource({
        transport: {
            read: {
                url: "/DataTracker.svc/GetStatusLogList",
                data: {

                    StoreName: escape(StoreName),
                    StartDate: StartDate,
                    CurrentDate: RetrunCurrentDate(),
                    TaskType: TType,
                    JobNumber: Job_Number

                }
            }
        },
        schema: {
            data: "d",
            total: function (d) {
                if (d.d.length > 0)
                    return d.d[0].TotalCount
            },
            model: {
                id: "ID",
                fields: {
                    ID: { editable: false, type: "number" },
                    StMsg: { editable: false, type: "string" },
                    StausName: { editable: false, type: "string" },
                    UpdatedBy: { editable: false, type: "string" },
                    LogTime: { editable: false, type: "string" },
                    DownLoadOrCheckList: { editable: false, type: "string" }

                }
            }
        },
        pageSize: 100,
        serverPaging: true

    });

    if ($("#StatusReportGrid").length > 0)
    { $("#StatusReportGrid")[0].innerHTML = ""; }
    var element = $("#StatusReportGrid").kendoGrid({
        dataSource: dataSourceDetail,
        dataBound: function () {

        },
        pageable: {
            batch: true,
            pageSize: 5,
            refresh: true,
            pageSizes: true,
            input: true
        },
        columns: [

                            { field: "StatusName", title: "Status Name", width: "25%", template: "#=SetCompletedImg(StatusName,StMsg)# #=DownLoadOrCheckList#" },
                            {
                                field: "UpdatedBy",
                                title: "Updated By",
                                width: "50%"

                            },
                            {
                                field: "ID",
                                title: "",
                                width: "0%"
                            },
                            {
                                field: "LogTime",
                                title: "Logged Time",
                                width: "25%"
                            }

                        ],
        editable: false,
        sortable: true,
        selectable: true

    });

}

// complete img
function SetCompletedImg(StatusName, Stmsg) {
    //ShowLastUpdatedTime(); 
    if (StatusName == "QA Returned") {
        var tootltip = StatusName;
        if (Stmsg != "")
            tootltip = Stmsg;
        else
            tootltip = StatusName;
        if (!IsClient) {
            return "<a style='cursor:pointer;' title='" + tootltip + "'   >" + StatusName + "</a>&nbsp;<img class='tooltip-block' title='" + tootltip + "'   src='/Images/Icon/Qareturn.png' />";
        }
        else {
            return "<a >" + StatusName + "</a>";
        }
    }
    else
        return StatusName;
}

function ShowOtherStatusReport(title, TargetDate,Job_Number, TType) {

    $("#StatusReportGrid")[0].innerHTML = "";
    if ($("#StatusReportGrid").data("kendoGrid") != null) {
        $('#StatusReportGrid').data().kendoGrid.destroy();
        $('#StatusReportGrid').empty();
    }
    var dataSourceDetail = "";
    dataSourceDetail = new kendo.data.DataSource({
        transport: {
            read: {
                url: "/DataTracker.svc/GetStatusLogList",
                data: {

                    StoreName: escape(title),
                    StartDate: TargetDate,
                    CurrentDate: RetrunCurrentDate(),
                    TaskType: TType,
                    JobNumber: Job_Number

                }
            }
        },
        schema: {
            data: "d",
            total: function (d) {
                if (d.d.length > 0)
                    return d.d[0].TotalCount
            },
            model: {
                id: "ID",
                fields: {
                    ID: { editable: false, type: "number" },
                    StMsg: { editable: false, type: "string" },
                    StausName: { editable: false, type: "string" },
                    UpdatedBy: { editable: false, type: "string" },
                    LogTime: { editable: false, type: "string" }


                }
            }
        },
        pageSize: 100,
        serverPaging: true

    });

    if ($("#StatusReportGrid").length > 0)
    { $("#StatusReportGrid")[0].innerHTML = ""; }
    var element = $("#StatusReportGrid").kendoGrid({
        dataSource: dataSourceDetail,
        dataBound: function () {

        },
        pageable: {
            batch: true,
            pageSize: 5,
            refresh: true,
            pageSizes: true,
            input: true
        },
        columns: [

                            { field: "StatusName", title: "Status Name", width: "25%", template: "#=SetCompletedImg(StatusName,StMsg)#" },
                            {
                                field: "UpdatedBy",
                                title: "Updated By",
                                width: "50%"

                            },
                            {
                                field: "ID",
                                title: "",
                                width: "0%"
                            },
                            {
                                field: "LogTime",
                                title: "Logged Time",
                                width: "25%"
                            }

                        ],
        editable: false,
        sortable: true,
        selectable: true

    });

}

function RetrunCurrentDate() {

    var ds = new Date();
    var curr_day1 = ds.getDate();
    if (curr_day1 < 10) {
        curr_day1 = '0' + curr_day1;
    }
    var curr_month1 = ('0' + (ds.getMonth() + 1)).slice(-2)
    var curr_year1 = ds.getFullYear();
    var Due_date = curr_day1 + "-" + curr_month1 + "-" + curr_year1;
    return Due_date;


}