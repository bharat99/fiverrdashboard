﻿$(document).ready(function () {
     
    GetBoardbyUser();
    //Current date
    var d = new Date();
    var month = d.getMonth() + 1;
    var day = d.getDate();
    var CurrentDate = d.getFullYear() + '-' + (('' + month).length < 2 ? '0' : '') + month + '-' + (('' + day).length < 2 ? '0' : '') + day;
    var today = new Date();

    $('#Startdate').val(CurrentDate);
    $('#Startdate, #imgStartdate').pickmeup({
        position: 'bottom',
        hide_on_select: true,
        change: function (formatted) {
            $('#Startdate').val(formatted);
            //min: date1
        }
    });

    $('#Enddate').val(CurrentDate);
    $('#Enddate, #imgEnddate').pickmeup({
        position: 'bottom',
        hide_on_select: true,
        change: function (formatted) {
            $('#Enddate').val(formatted);
            //min: date1
        }
    });

    $('.Close_Modal').click(function () {
        $(this).parents('.CustomPopup').fadeOut();
    });
    $('#board').change(function () {
        GetBoardMember();
    });

});


function GetAllTeamWorkingHoursdetails() {
    var _StartDateList = $('#Startdate').val();
    var _Enddate = $('#Enddate').val();
    var _UserId = $('#cuid').val();
    $.ajax({
        type: "POST",
        url: "/DataTracker.svc/GetAlldatewiseDataByTeamMember",   //Ameeq Changes 15/02/18
        data: JSON.stringify({ UserId: _UserId, StartDateList: _StartDateList, Enddate: _Enddate }),
        dataType: 'json',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
             
            //var d = JSON.parse(data.d);
            $('#tblWorkinghours').empty();
            $('#tblWorkinghours').append("<thead><tr><th>Board Name</th><th>Task Name</th><th>Hour & Minute</th><th>Employee Name</th><th>Last working Date</th><th>Assigneddate</th><th>Action</th></tr></thead><tbody>");
            $.each(data.d, function (index, item) {
                var LastWorkingdate = item.LastWorkingDate.split(' ');
                var Assigneddate = item.Assigneddate.split(' ');
                if (item.worktimeinminutes != "") {
                    var cardhoursandminute = item.worktimeinminutes.split('.');
                }
                var items = JSON.stringify({ boardId: item.boardId, Boardname: item.Boardname, taskid: item.taskid, title: item.title, worktimeinminutes: item.worktimeinminutes, userid: item.userid, Username: item.Username, LastWorkingDate: item.LastWorkingDate, Assigneddate: item.Assigneddate });
                $('#tblWorkinghours').append("<tr id=" + item.boardId + "><td>" + item.Boardname + "</td><td id=" + item.taskid + ">" + item.title + "</td><td>" + cardhoursandminute[0] + " hr " + cardhoursandminute[1] + "  m</td><td id=" + item.userid + ">" + item.Username + "</td><td>" + LastWorkingdate[0] + "</td><td>" + Assigneddate[0] + "</td><td><i class='fa fa-plus-circle' aria-hidden='true' id='plus' title='Add minuts' onclick='AddMinutePapup(" + items + ");'></i><i class='fa fa-minus-circle' aria-hidden='true' id='minus' title='Remove minuts' onclick='RemoveMinutePopup(" + items + ");'></i></td></tr>");
            });
            $('#tblWorkinghours').append("</tbody>");
        },
        error: function (data) {
        }
    });
}

function GetBoardbyUser() {
    $.ajax({
        type: "POST",
        url: "/DataTracker.svc/GetBoardByUser",
        dataType: 'json',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            var d = JSON.parse(data.d);
            $('#board').empty();
            $('#board').append('<option value="0_0">All</option>')
            $.each(d, function (index, item) {
                $('#board').append('<option value=' + item.BoardID + '_' + item.TeamID + '>' + item.BoardName + '</option>')
            });
            GetBoardMember();
            GetAllTeamWorkingHoursdetails();
        },
        error: function (data) { }
    });
}

function GetBoardMember() {
    var Selectoptionid = $('#board').val();
    var Board_Team = Selectoptionid.split('_');
    if (Board_Team[0] == "0") {
        $.ajax({
            type: "POST",
            url: "/DataTracker.svc/BindTeamMembers",
            data: JSON.stringify({ teamid: "23" }),
            dataType: 'json',
            contentType: 'application/json; charset=utf-8',
            success: function (data) {
                $('#boarduser').empty();
                $('#boarduser').append('<option value="1_1">All</option>')     //Ameeq Changes 15/02/18
                $.each(data.d, function (index, item) {
                    $('#boarduser').append('<option value=' + item.userId + '>' + item.userName + '</option>')
                });
            },
            error: function (data) { }
        });
    }
    else {
        $.ajax({
            type: "POST",
            url: "/DataTracker.svc/BindMembers",
            data: JSON.stringify({ teamid: Board_Team[1] }),
            dataType: 'json',
            contentType: 'application/json; charset=utf-8',
            success: function (data) {
                $('#boarduser').empty();
                $('#boarduser').append('<option value="11_1">All</option>')        //Ameeq Changes 15/02/18
                $.each(data.d, function (index, item) {
                    $('#boarduser').append('<option value=' + item.userId + '>' + item.userName + '</option>')
                });
            },
            error: function (data) { }
        });
    }
}


function AddMinutePapup(items) {
     
    items = JSON.stringify({ items: items });
    $('#HourDetail').val(items);
    $('#addminutes').val('');
    $(".AddHour").fadeIn();
}

function RemoveMinutePopup(items) {
     
    items = JSON.stringify({ items: items });
    $('#HourDetail').val(items);
    $('#removeminutes').val('');
    $(".RemoveHour").fadeIn();
}

var amy = "";

function GetWorkingHouronCard() {
     
    var BoardId_Team = [];
    var _UserId = $('#boarduser').val();
    if (_UserId == "1_1") {
        _UserId = $('#cuid').val();
        var Selectoptionid = $('#boarduser').val();
    }
    else if (_UserId == "11_1") {
        _UserId = $('#cuid').val();
        var Selectoptionid = $('#boarduser').val();
        var Boardoptionid = $('#board').val();
        BoardId_Team = Boardoptionid.split('_');
        var _BoardIdTeam = BoardId_Team[0];
    } else
        Selectoptionid = $('#board').val();
    var Board_Team = [];
    var _BoardId = '';
    if (Selectoptionid != null) {
        Board_Team = Selectoptionid.split('_');
        _BoardId = Board_Team[0];
    }
    var _BoardId = Board_Team[0];
    var _StartDateList = $('#Startdate').val();
    var _Enddate = $('#Enddate').val();
    if (_BoardId == "0") {
        $.ajax({
            type: "POST",
            url: "/DataTracker.svc/GetAlldatewiseData",
            data: JSON.stringify({ UserId: _UserId, StartDateList: _StartDateList, Enddate: _Enddate }),
            dataType: 'json',
            contentType: 'application/json; charset=utf-8',
            success: function (data) {
                 
                //var d = JSON.parse(data.d);
                $('#tblWorkinghours').empty();
                $('#tblWorkinghours').append("<thead><tr><th>Board Name</th><th>Task Name</th><th>Hour & Minute</th><th>Employee Name</th><th>Last working Date</th><th>Assigneddate</th><th>Action</th></tr></thead><tbody>");
                $.each(data.d, function (index, item) {
                    var LastWorkingdate = item.LastWorkingDate.split(' ');
                    var Assigneddate = item.Assigneddate.split(' ');
                    if (item.worktimeinminutes != "") {
                        var cardhoursandminute = item.worktimeinminutes.split('.');
                    }
                    var items = JSON.stringify({ boardId: item.boardId, Boardname: item.Boardname, taskid: item.taskid, title: item.title, worktimeinminutes: item.worktimeinminutes, userid: item.userid, Username: item.Username, LastWorkingDate: item.LastWorkingDate, Assigneddate: item.Assigneddate });
                    $('#tblWorkinghours').append("<tr id=" + item.boardId + "><td>" + item.Boardname + "</td><td id=" + item.taskid + ">" + item.title + "</td><td>" + cardhoursandminute[0] + " hr " + cardhoursandminute[1] + "  m</td><td id=" + item.userid + ">" + item.Username + "</td><td>" + LastWorkingdate[0] + "</td><td>" + Assigneddate[0] + "</td><td><i class='fa fa-plus-circle' aria-hidden='true' id='plus' title='Add minuts' onclick='AddMinutePapup(" + items + ");'></i><i class='fa fa-minus-circle' aria-hidden='true' id='minus' title='Remove minuts' onclick='RemoveMinutePopup(" + items + ");'></i></td></tr>");
                });
                $('#tblWorkinghours').append("</tbody>");
            },
            error: function (data) {
            }
        });
    }
    else if (_BoardId == "1") {
        $.ajax({
            type: "POST",
            url: "/DataTracker.svc/GetAlldatewiseDataByTeamMember",   //Ameeq Changes 15/02/18
            data: JSON.stringify({ UserId: _UserId, StartDateList: _StartDateList, Enddate: _Enddate }),
            dataType: 'json',
            contentType: 'application/json; charset=utf-8',
            success: function (data) {
                 
                //var d = JSON.parse(data.d);
                $('#tblWorkinghours').empty();
                $('#tblWorkinghours').append("<thead><tr><th>Board Name</th><th>Task Name</th><th>Hour & Minute</th><th>Employee Name</th><th>Last working Date</th><th>Assigneddate</th><th>Action</th></tr></thead><tbody>");
                $.each(data.d, function (index, item) {
                    var LastWorkingdate = item.LastWorkingDate.split(' ');
                    var Assigneddate = item.Assigneddate.split(' ');
                    if (item.worktimeinminutes != "") {
                        var cardhoursandminute = item.worktimeinminutes.split('.');
                    }
                    var items = JSON.stringify({ boardId: item.boardId, Boardname: item.Boardname, taskid: item.taskid, title: item.title, worktimeinminutes: item.worktimeinminutes, userid: item.userid, Username: item.Username, LastWorkingDate: item.LastWorkingDate, Assigneddate: item.Assigneddate });
                    $('#tblWorkinghours').append("<tr id=" + item.boardId + "><td>" + item.Boardname + "</td><td id=" + item.taskid + ">" + item.title + "</td><td>" + cardhoursandminute[0] + " hr " + cardhoursandminute[1] + "  m</td><td id=" + item.userid + ">" + item.Username + "</td><td>" + LastWorkingdate[0] + "</td><td>" + Assigneddate[0] + "</td><td><i class='fa fa-plus-circle' aria-hidden='true' id='plus' title='Add minuts' onclick='AddMinutePapup(" + items + ");'></i><i class='fa fa-minus-circle' aria-hidden='true' id='minus' title='Remove minuts' onclick='RemoveMinutePopup(" + items + ");'></i></td></tr>");
                });
                $('#tblWorkinghours').append("</tbody>");
            },
            error: function (data) {
            }
        });
    }
    else if (_BoardId == "11") {
        $.ajax({
            type: "POST",
            url: "/DataTracker.svc/GetAlldatewiseDataByTeamMemberDetails",   //Ameeq Changes 30/03/18
            data: JSON.stringify({ UserId: _UserId, BoardIdTeam: _BoardIdTeam, StartDateList: _StartDateList, Enddate: _Enddate }),
            dataType: 'json',
            contentType: 'application/json; charset=utf-8',
            success: function (data) {
                 
                //var d = JSON.parse(data.d);
                $('#tblWorkinghours').empty();
                $('#tblWorkinghours').append("<thead><tr><th>Board Name</th><th>Task Name</th><th>Hour & Minute</th><th>Employee Name</th><th>Last working Date</th><th>Assigneddate</th><th>Action</th></tr></thead><tbody>");
                $.each(data.d, function (index, item) {
                    var LastWorkingdate = item.LastWorkingDate.split(' ');
                    var Assigneddate = item.Assigneddate.split(' ');
                    if (item.worktimeinminutes != "") {
                        var cardhoursandminute = item.worktimeinminutes.split('.');
                    }
                    var items = JSON.stringify({ boardId: item.boardId, Boardname: item.Boardname, taskid: item.taskid, title: item.title, worktimeinminutes: item.worktimeinminutes, userid: item.userid, Username: item.Username, LastWorkingDate: item.LastWorkingDate, Assigneddate: item.Assigneddate });
                    $('#tblWorkinghours').append("<tr id=" + item.boardId + "><td>" + item.Boardname + "</td><td id=" + item.taskid + ">" + item.title + "</td><td>" + cardhoursandminute[0] + " hr " + cardhoursandminute[1] + "  m</td><td id=" + item.userid + ">" + item.Username + "</td><td>" + LastWorkingdate[0] + "</td><td>" + Assigneddate[0] + "</td><td><i class='fa fa-plus-circle' aria-hidden='true' id='plus' title='Add minuts' onclick='AddMinutePapup(" + items + ");'></i><i class='fa fa-minus-circle' aria-hidden='true' id='minus' title='Remove minuts' onclick='RemoveMinutePopup(" + items + ");'></i></td></tr>");
                });
                $('#tblWorkinghours').append("</tbody>");
            },
            error: function (data) {
            }
        });
    }
    else {
        $.ajax({
            type: "POST",
            url: "/DataTracker.svc/GetWorkingHouronCard",
            data: JSON.stringify({ UserId: _UserId, BoardId: _BoardId, StartDateList: _StartDateList, Enddate: _Enddate }),
            dataType: 'json',
            contentType: 'application/json; charset=utf-8',
            success: function (data) {
                //var d = JSON.parse(data.d);
                $('#tblWorkinghours').empty();
                $('#tblWorkinghours').append("<thead><tr><th>Board Name</th><th>Task Name</th><th>Hour & Minute</th><th>Employee Name</th><th>Last working Date</th><th>Assigneddate</th><th>Action</th></tr></thead><tbody>");
                $.each(data.d, function (index, item) {
                    var LastWorkingdate = item.LastWorkingDate.split(' ');
                    var Assigneddate = item.Assigneddate.split(' ');
                    if (item.worktimeinminutes != "") {
                        var cardhoursandminute = item.worktimeinminutes.split(':');
                    }
                    var items = JSON.stringify({ boardId: item.boardId, Boardname: item.Boardname, taskid: item.taskid, title: item.title, worktimeinminutes: item.worktimeinminutes, userid: item.userid, Username: item.Username, LastWorkingDate: item.LastWorkingDate, Assigneddate: item.Assigneddate });
                    $('#tblWorkinghours').append("<tr id=" + item.boardId + "><td>" + item.Boardname + "</td><td id=" + item.taskid + ">" + item.title + "</td><td>" + cardhoursandminute[0] + " hr " + cardhoursandminute[1] + "  m</td><td id=" + item.userid + ">" + item.Username + "</td><td>" + LastWorkingdate[0] + "</td><td>" + Assigneddate[0] + "</td><td><i class='fa fa-plus-circle' aria-hidden='true' id='plus' title='Add minuts' onclick='AddMinutePapup(" + items + ");'></i><i class='fa fa-minus-circle' aria-hidden='true' id='minus' title='Remove minuts' onclick='RemoveMinutePopup(" + items + ");'></i></td></tr>");
                });
                $('#tblWorkinghours').append("</tbody>");
            },
            error: function (data) {
            }
        });
    }
}

function AddHours() {
     
    var items = $('#HourDetail').val();
    var arg = JSON.parse(items);
    var Addminuts = $('#addminutes').val();
    $.ajax({
        type: "POST",
        url: "/DataTracker.svc/AddHoursinTask",
        data: JSON.stringify({ boardId: arg.items.boardId, Boardname: arg.items.Boardname, taskid: arg.items.taskid, title: arg.items.title, worktimeinminutes: Addminuts, userid: arg.items.userid, Username: arg.items.Username, LastWorkingDate: arg.items.LastWorkingDate, Assigneddate: arg.items.Assigneddate }),
        dataType: 'json',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            if (data.d > 0) {
                alert('Successfully added.');
                GetWorkingHouronCard();
                $('.CustomPopup').fadeOut();
            }
        },
        error: function (data) {
        }
    });
}

function RemoveHours() {
     
    var items = $('#HourDetail').val();
    var arg = JSON.parse(items);
    var removeminutes = $('#removeminutes').val();
    $.ajax({
        type: "POST",
        url: "/DataTracker.svc/RemoveHoursinTask",
        data: JSON.stringify({ boardId: arg.items.boardId, Boardname: arg.items.Boardname, taskid: arg.items.taskid, title: arg.items.title, worktimeinminutes: removeminutes, userid: arg.items.userid, Username: arg.items.Username, LastWorkingDate: arg.items.LastWorkingDate, Assigneddate: arg.items.Assigneddate }),
        dataType: 'json',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            if (data.d > 0) {
                alert('Successfully removed.');
                GetWorkingHouronCard();
                $('.CustomPopup').fadeOut();
            }
        },
        error: function (data) {
        }
    });
}

//function ExportWorkingHoursDetails() {
//     
//    var _UserId = $('#boarduser').val();
//    if ($("#Startdate").val() != "" && $("#Enddate").val() != "") {
//        document.getElementById("downloaddata").innerHTML = "Please Wait..";
//        var selectedstartdate = $("#Startdate").val()
//        var selectedenddate = $("#Enddate").val()
//        $.ajax({
//            type: "POST",
//            url: "/DataTracker.svc/GetExcelData",
//            data: "{\"userid\": \"" + _UserId + "\",{\"startdate\": \"" + selectedstartdate + "\",{\"enddate\": \"" + selectedenddate + "\"",
//            contentType: "application/json; charset=utf-8",
//            dataType: "json",
//            success: function (data) {
//                if (data.d != null) {
//                    document.getElementById("downloaddata").innerHTML = "Download Employee Data";
//                    window.location.href = 'Downloadfiles.aspx?expoertschedule=' + data.d;
//                    //                            document.getElementById("schedule").innerHTML = "Download Schedule";
//                }
//                else {
//                    document.getElementById("schedule").innerHTML = "Download Schedule";
//                    alert("No Schedule Found")
//                }
//            },
//            Error: function (data) {
//                // alert("Data Cannot Be Exported")
//                document.getElementById("schedule").innerHTML = "Download Schedule";

//            }
//        });
//    }
//    else {
//        alert("Please Select Date");
//        return false;
//    }
//}

//Ameeq Changes 15/02/18
function ExportWorkingHoursDetails() {
    if ($.browser.safari) {
        alert("Safari will not properly Export as excel");
        return false;
    }
    else {
        $('#btnExport').val("Please wait....");
        $.ajax({
            type: "POST",
            url: "/DataTracker.svc/ExportWorkingHoursDetails",
            data: "{}",
            contentType: "application/json; charset=utf-16",
            dataType: "json",
            success: function (data) {
                 
                var response = JSON.parse(data.d);
                var U_Name = $("#boarduser option:selected").text();
                fadname = 'WorkingHoursReports of ' + U_Name + '.xlsx';
                var rows = [{
                    cells: [
                { value: "Board Name" },
                { value: "Task Name" },
                { value: "Hour & Minute" },
                { value: "Employee Name" },
                { value: "Date" },
                    ]
                }];

                $.each(response, function (index, item) {

                    var dataItem = response[index];

                    rows.push({
                        cells: [
                    { value: dataItem.Boardname },
                    { value: dataItem.title },
                    { value: dataItem.worktimeinminutes },
                    { value: dataItem.Username },
                    { value: dataItem.LastWorkingDate },
                        ]
                    })

                });
                if (rows != undefined && rows != '') {
                    $('#btnExport').val("Export to excel");
                    excelExport(rows);
                    e.preventDefault();
                }
                else {
                    alert('Please wait and try again');
                    return false;
                }
            },
            error: function (xhr, status, error) {

            }
        });
    }
}

//Ameeq Changes 15/02/18 for ExportExcelFunctionality
function excelExport(rows) {
     
    var workbook = new kendo.ooxml.Workbook({
        sheets: [
        {
            columns: [
            { autoWidth: true },
            { autoWidth: true }
            ],
            title: "WorkingHoursDetails",
            rows: rows
        }
        ]
    });
    kendo.saveAs({ dataURI: workbook.toDataURL(), fileName: fadname });

}