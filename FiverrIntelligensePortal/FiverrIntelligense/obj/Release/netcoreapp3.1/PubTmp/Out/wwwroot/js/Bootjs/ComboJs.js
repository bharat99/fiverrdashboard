﻿//WorkItem Combo
var flageS = false;
var options = new Array();
function fff() {
    removeOptions(document.getElementById("s9"));
    //abortRequests();
    options.length = 0;
    $.ajax({
        type: "GET",
        url: "/DataTracker.svc/GetOtherStatusListcom",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            if (data.d != null) {
                for (i = 0; i < data.d.length; i++) {
                    if (data.d[i].StatusID == 8 || data.d[i].StatusID == 9 || data.d[i].StatusID == 10 || data.d[i].StatusID == 33) {
                        options.push("" + data.d[i].StatusID + "");
                        $("#s9").append("<option selected='selected' value=' " + data.d[i].StatusID + "' text='" + data.d[i].StatusName + "'>" + data.d[i].StatusName + "</option>");
                    }
                    else
                        $("#s9").append("<option  value=' " + data.d[i].StatusID + "' text='" + data.d[i].StatusName + "'>" + data.d[i].StatusName + "</option>");
                }
            }
            createDropdown("#s9", 's9');
        }

    });
    //GetAllWorkItem();
}



function GetAllCheckItemBoot() {
    return options;
}

function OnclickBootStatusWork() {

    GetAllOtherTasks(FormatedItemCombo(options), false, false, $("#Checkforresponsible").val());

}

function FormatedItemCombo(options) {
    var data = "";
    for (var i = 0; i < options.length; i++) {
        if (i == 0)
            data = options[0];
        else
            data += "," + options[i];
    }
    return data
}
var descptoptions = new Array();
function createDropdown(id, coname) {
    if (coname == 's10') {
        options.length = 0;
        $(id).dropdownchecklist({
            icon: {},
            height: 250,
            firstItemChecksAll: true,
            emptyText: "Filter by store",
            textFormatFunction: function (data) {
                
                var selectedOptions = data.filter(":selected");
                var countOfSelected = selectedOptions.size();
                switch (countOfSelected) {
                    case 0: return "Filter by store..."
                    case 1: return selectedOptions.text();
                    case data.size(): return "All Selected";
                    default: return countOfSelected + " Item Checked";
                }
            }
                , onItemClick: function (checkbox, selector) {
                    var justChecked = checkbox.prop("checked");
                    if (justChecked && checkbox[0].value == "Select All") {
                        options.length = 0;
                        for (i = 1; i < selector.options.length; i++) {
                            options.push(selector.options[i].value.trim());
                        }
                    }
                    else if (!justChecked && checkbox[0].value == "Select All") {
                        options.length = 0;
                    }
                    else if (justChecked) {
                        options.push(checkbox[0].value.trim());
                    }
                    else {
                        options.splice($.inArray(checkbox[0].value.trim(), options), 1);
                    }
                    //OnclickBootStatusWork();
                    OnStoreNameSelectedIndexChanged();
                }

        });
        //GetAllWorkItem();
    }
    else if (coname == 's9') {
        $(id).dropdownchecklist({
            icon: {},
            height: 250,
            firstItemChecksAll: true,
            emptyText: "filter by status",
            textFormatFunction: function (data) {
                var selectedOptions = data.filter(":selected");
                var countOfSelected = selectedOptions.size();
                switch (countOfSelected) {
                    case 0: return "filter by status..."
                    case 1: return selectedOptions.text();
                    case data.size(): return "All Selected";
                    default: return countOfSelected + " Item Checked";
                }
            }
                , onItemClick: function (checkbox, selector) {
                    var justChecked = checkbox.prop("checked");
                    if (justChecked && checkbox[0].value == "Select All") {
                        options.length = 0;
                        for (i = 1; i < selector.options.length; i++) {
                            options.push(selector.options[i].value.trim());
                        }
                    }
                    else if (!justChecked && checkbox[0].value == "Select All") {
                        options.length = 0;
                    }
                    else if (justChecked) {
                        options.push(checkbox[0].value.trim());
                    }
                    else {
                        options.splice($.inArray(checkbox[0].value.trim(), options), 1);
                    }
                    OnclickBootStatusWork();
                }


        });
        GetAllWorkItem();
    }
    else if (coname == 's8') {
        $(id).dropdownchecklist({
            icon: {},
            height: 250,
            firstItemChecksAll: true,
            emptyText: "All status",
            textFormatFunction: function (data) {

                var RegselectedOptions = data.filter(":selected");
                var RegcountOfSelected = RegselectedOptions.size();
                switch (RegcountOfSelected) {
                    case 0: return "filter by status..."
                    case 1: return RegselectedOptions.text();
                    case data.size(): return "All Selected";
                    default: return RegcountOfSelected + " Item Checked";

                }
            }
                , onItemClick: function (checkbox, selector) {
                    var RegjustChecked = checkbox.prop("checked");
                    if (RegjustChecked && checkbox[0].value == "Select All") {
                        Regoptions.length = 0;
                        for (i = 1; i < selector.options.length; i++) {
                            Regoptions.push(selector.options[i].value.trim());
                        }
                    }
                    else if (!RegjustChecked && checkbox[0].value == "Select All") {
                        Regoptions.length = 0;
                    }
                    else if (RegjustChecked) {
                        Regoptions.push(checkbox[0].value.trim());
                    }
                    else {
                        Regoptions.splice($.inArray(checkbox[0].value.trim(), Regoptions), 1);
                    }
                    // OnclickBootStatusWork();
                    //ShowReg();
                }


        });
    }
    else if (coname == 's7') {
        $(id).dropdownchecklist({
            icon: {},
            height: 250,
            firstItemChecksAll: true,
            emptyText: "All status",
            textFormatFunction: function (data) {

                var OffselectedOptions = data.filter(":selected");
                var OffcountOfSelected = OffselectedOptions.size();
                switch (OffcountOfSelected) {
                    case 0: return "filter by status..."
                    case 1: return OffselectedOptions.text();
                    case data.size(): return "All Selected";
                    default: return OffcountOfSelected + " Item Checked";

                }
            }
                , onItemClick: function (checkbox, selector) {
                    var OffjustChecked = checkbox.prop("checked");
                    if (OffjustChecked && checkbox[0].value == "Select All") {
                        Offoptions.length = 0;
                        for (i = 1; i < selector.options.length; i++) {
                            Offoptions.push(selector.options[i].value.trim());
                        }
                    }
                    else if (!OffjustChecked && checkbox[0].value == "Select All") {
                        Offoptions.length = 0;
                    }
                    else if (OffjustChecked) {
                        Offoptions.push(checkbox[0].value.trim());
                    }
                    else {
                        Offoptions.splice($.inArray(checkbox[0].value.trim(), Offoptions), 1);
                    }
                    // OnclickBootStatusWork();
                    //ShowReg();
                    OffcycleStatusItemChecked();
                }


        });
    }
    else if (coname == 's6') {
        $(id).dropdownchecklist({
            icon: {},
            height: 250,
            firstItemChecksAll: true,
            emptyText: "All status",
            textFormatFunction: function (data) {

                var AdsStoselectedOptions = data.filter(":selected");
                var AdsStocountOfSelected = AdsStoselectedOptions.size();
                switch (AdsStocountOfSelected) {
                    case 0: return "filter by store name.."
                    case 1: return AdsStoselectedOptions.text();
                    case data.size(): return "All Selected";
                    default: return AdsStocountOfSelected + " Item Checked";

                }
            }
                , onItemClick: function (checkbox, selector) {
                    var AdsStojustChecked = checkbox.prop("checked");
                    if (AdsStojustChecked && checkbox[0].value == "Select All") {
                        AdsStoreoptions.length = 0;
                        for (i = 1; i < selector.options.length; i++) {
                            AdsStoreoptions.push(selector.options[i].value.trim());
                        }
                    }
                    else if (!AdsStojustChecked && checkbox[0].value == "Select All") {
                        AdsStoreoptions.length = 0;
                    }
                    else if (AdsStojustChecked) {
                        AdsStoreoptions.push(checkbox[0].value.trim());
                    }
                    else {
                        AdsStoreoptions.splice($.inArray(checkbox[0].value.trim(), AdsStoreoptions), 1);
                    }
                    // OnclickBootStatusWork();
                    //ShowReg();
                    //OffcycleStatusItemChecked();
                    StoreItemChecked();
                }


        });
    }
    else if (coname == 's5') {
        $(id).dropdownchecklist({
            icon: {},
            height: 250,
            firstItemChecksAll: true,
            emptyText: "All status",
            textFormatFunction: function (data) {

                var AdsStatuselectedOptions = data.filter(":selected");
                var AdsStatucountOfSelected = AdsStatuselectedOptions.size();
                switch (AdsStatucountOfSelected) {
                    case 0: return "filter by status.."
                    case 1: return AdsStatuselectedOptions.text();
                    case data.size(): return "All Selected";
                    default: return AdsStatucountOfSelected + " Item Checked";

                }
            }
                , onItemClick: function (checkbox, selector) {
                    var AdsStatujustChecked = checkbox.prop("checked");
                    if (AdsStatujustChecked && checkbox[0].value == "Select All") {
                        AdsStatujustChecked.length = 0;
                        for (i = 1; i < selector.options.length; i++) {
                            AdsStatusoptions.push(selector.options[i].value.trim());
                        }
                    }
                    else if (!AdsStatujustChecked && checkbox[0].value == "Select All") {
                        AdsStatusoptions.length = 0;
                    }
                    else if (AdsStatujustChecked) {
                        AdsStatusoptions.push(checkbox[0].value.trim());
                    }
                    else {
                        AdsStatusoptions.splice($.inArray(checkbox[0].value.trim(), AdsStatusoptions), 1);
                    }
                    // OnclickBootStatusWork();
                    //ShowReg();
                    //OffcycleStatusItemChecked();
                    StoreItemChecked();
                }


        });
    }
    else if (coname == 'ddllDescription') {
       // $(".ui-dropdownchecklist-dropcontainer ui-widget-content").resizable("enable");
             $('.ui-dropdownchecklist-dropcontainer ui-widget-content')
   .resizable({
       start: function (e, ui) {
           //  alert('resizing started');
       },
       resize: function (e, ui) {
           $(".ui-dropdownchecklist-dropcontainer ui-widget-content").resizable("enable");
       },
       stop: function (e, ui) {
           //  alert('resizing stopped');
       }
   });
        if ($("#ddllDescription").data("ui-dropdownchecklist")) {
            $("#ddllDescription").dropdownchecklist("destroy");
        }
       
        $(id).dropdownchecklist({
            icon: {},
            height: 250,
            width:400,
            firstItemChecksAll: true,
            emptyText: "All status",
            textFormatFunction: function (data) {
                
                //if (flageS == false) {
                    flageS = true;
                    var RegdescrpOptions = data.filter(":selected");
                    var RegdesOfSelected = RegdescrpOptions.size();
                    switch (RegdesOfSelected) {
                        case 0: return "filter by description..."
                        case 1: return RegdescrpOptions.text();
                        case data.size(): return "All Selected";
                        default: return RegdesOfSelected + " Item Checked";

                    }
                }
           // }
                , onItemClick: function (checkbox, selector) {
                    var RegjustCheckeddes = checkbox.prop("checked");
                    if (RegjustCheckeddes && checkbox[0].value == "Select All") {
                        descptoptions.length = 0;
                        for (i = 1; i < selector.options.length; i++) {
                            descptoptions.push(selector.options[i].value.trim());
                        }
                    }
                    else if (!RegjustCheckeddes && checkbox[0].value == "Select All") {
                        descptoptions.length = 0;
                    }
                    else if (RegjustCheckeddes) {
                        descptoptions.push(checkbox[0].value.trim());
                    }
                    else {
                        descptoptions.splice($.inArray(checkbox[0].value.trim(), descptoptions), 1);
                    }
                    OnDescriptionSelectedIndexChanged();
                    // OnclickBootStatusWork();
                    //ShowReg();
                }


        });
    }
}

function descriptionNamedet()
{
    //$("#ddllDescription").dropdownchecklist.val("");
    $("#ddllDescription").dropdownchecklist({
            icon: {},
            height: 250,
            firstItemChecksAll: true,
            emptyText: "All status",
            textFormatFunction: function (data) {

                var RegselectedOptions = data.filter(":selected");
                var RegcountOfSelected = RegselectedOptions.size();
                switch (RegcountOfSelected) {
                    case 0: return "filter by status..."
                    case 1: return RegselectedOptions.text();
                    case data.size(): return "All Selected";
                    default: return RegcountOfSelected + " Item Checked";

                }
            }
                , onItemClick: function (checkbox, selector) {
                    var RegjustChecked = checkbox.prop("checked");
                    if (RegjustChecked && checkbox[0].value == "Select All") {
                        descptoptions.length = 0;
                        for (i = 1; i < selector.options.length; i++) {
                            descptoptions.push(selector.options[i].value.trim());
                        }
                    }
                    else if (!RegjustChecked && checkbox[0].value == "Select All") {
                        descptoptions.length = 0;
                    }
                    else if (RegjustChecked) {
                        descptoptions.push(checkbox[0].value.trim());
                    }
                    else {
                        descptoptions.splice($.inArray(checkbox[0].value.trim(), descptoptions), 1);
                    }
                    OnDescriptionSelectedIndexChanged();
                    // OnclickBootStatusWork();
                    //ShowReg();
                }


        });
}

function resetOptionValue(id, allCheck) {
    var allCheckVal = allCheck.split(',');
    options.length = 0;
    $(id).dropdownchecklist('destroy');
    $(id + ' option').each(function () {
        $(this).attr('selected', false);
        if (allCheckVal != '' && allCheckVal != null) {
            for (i = 0; i <= allCheckVal.length - 1; i++) {
                if (this.value.trim() == allCheckVal[i].trim()) {
                    $(this).attr('selected', true);
                    options.push(this.value.trim());
                }

            }

        }
    });
    createDropdown(id, 's9');

}

function GetAllTaskLoad(UTYPEID) {
    if (UTYPEID == "12" || UTYPEID == "13") {
        $("#Checkforresponsible").val("12");
    }
    GetAllOtherTasks(FormatedItemCombo(options), true, false, $("#Checkforresponsible").val());
}
function ClearItems() {
    resetOptionValue('#s9', '8,9,10,33');

    document.getElementById("filtext").value = "";
    $("#Checkforresponsible").val("12");
    GetClearItems(FormatedItemCombo(options), $("#Checkforresponsible").val());
}

function GetAllWorkItemClear() {
    ForGetAllWorkItemClear(FormatedItemCombo(options));
}

function WorkItemStatusChecked() {
    var utidd = 0;
    GetAllOtherTasks(FormatedItemCombo(options), false, false, $("#Checkforresponsible").val());
}

function onisresponscheckchange() {
    Getonisresponscheckchange(FormatedItemCombo(options));
}

function Getsearchtitlebox(CombStatusList, sender, args) {
    var utidd = 0;
    if (args.keyCode == 13) {
        if (document.getElementById("filtext").value != "") {
            var TxtValue = document.getElementById("filtext").value;
            if (TxtValue != "") {

                resetOptionValue('#s9', CombStatusList);
                GetAllOtherTasks(CombStatusList, false, false, $("#Checkforresponsible").val());
            }
        }
        else if (document.getElementById("filtext").value == "") {

            var TxtVal = document.getElementById("filtext").value;
            if (TxtVal == "") {
                if ($.inArray(12, CombStatusList) && $.inArray(13, CombStatusList) && $.inArray(14, CombStatusList)) {
                    resetOptionValue('#s9', '8,9,10,33');
                }
                else
                    resetOptionValue('#s9', '8,9,10,33,14,13,12');

            }
            GetAllOtherTasks(CombStatusList, false, false, $("#Checkforresponsible").val());
        }
    }
}

function searchtitlebox(sender, args) {
    Getsearchtitlebox(FormatedItemCombo(options), sender, args);

}
//ForReg Combo
var Regoptions = new Array();
function statusscrapcombindBoot() {
    removeOptions(document.getElementById("s8"));
    $.ajax({
        type: "GET",
        url: "/DataTracker.svc/BindStatusScrapCom",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            if (data.d != null) {
                for (i = 0; i < data.d.length; i++) {
                    if (data.d[i].StatusID == 8 || data.d[i].StatusID == 9 || data.d[i].StatusID == 10 || data.d[i].StatusID == 11 || data.d[i].StatusID == 15 || data.d[i].StatusID == 16 || data.d[i].StatusID == 18 || data.d[i].StatusID == 20 || data.d[i].StatusID == 21 || data.d[i].StatusID == 23 || data.d[i].StatusID == 26 || data.d[i].StatusID == 28 || data.d[i].StatusID == 29 || data.d[i].StatusID == 30) {

                        Regoptions.push("" + data.d[i].StatusID + "");
                        $("#s8").append("<option selected='selected' value=' " + data.d[i].StatusID + "' text='" + data.d[i].Status + "'>" + data.d[i].Status + "</option>");

                    }
                    else
                        $("#s8").append("<option  value=' " + data.d[i].StatusID + "' text='" + data.d[i].Status + "'>" + data.d[i].Status + "</option>");
                }
            }
            createDropdown("#s8", 's8');
            ShowReg();

        }
    });

}
//REg Month Combo   
function monthcombindHTML() {
    var today = new Date();
    var mm = today.getMonth() + 1;
    var yyyy = today.getFullYear();
    var today = mm + '~' + yyyy;
    abortRequests();
    $.ajax({
        type: "GET",
        url: "/DataTracker.svc/BindMonthScrapCom",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            if (data.d != null) {
                $("#ComboForRegMonth").append('<option value = "">None</option>');
                $.each(data.d, function (index, item) {
                    $("#ComboForRegMonth").append('<option value = "' + item.monthvalue + '">' + item.monthnamee + '</option>');
                });
                $("#ComboForRegMonth").val(today);
                ddlRegularStoreName(today);
            }
            
            //ddlRegularAccountId(today);
            // calbindstatusscrapcom();byme
            statusscrapcombindBoot();
        }
    });

}

function ddlRegularStoreName(_Month) {
   // alert("Call");
    $.ajax({
        type: "POST",
        url: "/DataTracker.svc/ddlRegularStoreName",
        data: JSON.stringify({ Month: _Month }),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            var d = JSON.parse(data.d);
            if (d != null) {
                $("#s10").empty();
                $("#s10").append('<option value="Select All">Select All</option>');
                $.each(d, function (index, item) {
                    $("#s10").append('<option value = "' + item.StoreName + '">' + item.StoreName + '</option>');
                });
            }
            createDropdown("#s10", 's10');
           // createDropdown("#ddllDescription", 'ddllDescription');
            ddlRegularDescription(_Month, "");
          
            
        },
        error: function (data) { }
    });
}
function ddlRegularAccountId(_Month, _StName) {
    $.ajax({
        type: "POST",
        url: "/DataTracker.svc/ddlRegularAccountId",
        data: JSON.stringify({ Month: _Month, StName: _StName }),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            var d = JSON.parse(data.d);
            if (d != null) {
                $("#ddlAccountID").empty();
                $("#ddlAccountID").append('<option value = "">None</option>');
                $.each(d, function (index, item) {
                    if (item.AccountID == '')
                    {
                        $("#ddlAccountID").append('<option value = "">--Select--</option>');
                    }
                    else
                    {
                        $("#ddlAccountID").append('<option value = "' + item.AccountID + '">' + item.AccountID + '</option>');
                    }
                });
            }
        },
        error: function (data) { }
    });
}


function ddlRegularDescription(_Month, _StName) {
    $.ajax({
        type: "POST",
        url: "/DataTracker.svc/ddlRegularDescription",
        data: JSON.stringify({ Month: _Month, StName: _StName }),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            var d = JSON.parse(data.d);
            if (d != null) {
                $("#ddllDescription").empty();
                $("#ddllDescription").append('<option value="Select All">Select All</option>');
                $.each(d, function (index, item) {
                    $("#ddllDescription").append('<option value = "' + item.Description1 + '">' + item.Description + '</option>');
                });
                //$("#ddllDescription").empty();
                //$("#ddllDescription").append('<option value = "">Select</option>');
                //$.each(d, function (index, item) {
                //    if (item.Description == '') {
                //        $("#ddllDescription").append('<option value = "">--Select--</option>');
                //    }
                //    else {
                //        $("#ddllDescription").append('<option value = "' + item.Description + '">' + item.Description + '</option>');
                //    }
                //});
               
            }
            createDropdown("#ddllDescription", 'ddllDescription');
            //descriptionNamedet();
          ddlRegularAccountId(_Month, "");
        },
        error: function (data) { }
    });
}

function OnClientSelectedIndexChanged(sender, eventArgs) {
    ddlRegularStoreName($("#ComboForRegMonth").val());
    //ddlRegularDescription($("#ComboForRegMonth").val(), '');
    $('#ddllDescription').empty();
     //$('#ddlAccountID').empty();
     
     $('#TxtStartdate').val('');
     $('#TxtTargetdate').val('');
    //ShowRegularGrid("", "", "", "", "", "", "", "", "", FormatedItemCombo(Regoptions), $("#ComboForRegMonth").val(), false);
}
function OnStoreNameSelectedIndexChanged() {
    debugger;
    var StName = FormatedItemCombo(options);
    ddlRegularDescription($("#ComboForRegMonth").val(), StName);
   // ddlRegularAccountId($("#ComboForRegMonth").val(),StName);
    //ShowRegularGrid("", "", "", "", "", "", "", StName, "", FormatedItemCombo(Regoptions), $("#ComboForRegMonth").val(), false);
}
//description 
function OnDescriptionSelectedIndexChanged() {
    debugger;
    var StName = FormatedItemCombo(descptoptions);
   // ddlRegularDescription($("#ComboForRegMonth").val(), StName);
    ddlRegularAccountId($("#ComboForRegMonth").val(), StName);
    //ShowRegularGrid("", "", "", "", "", "", "", StName, "", FormatedItemCombo(Regoptions), $("#ComboForRegMonth").val(), false);
}

function OnAccountIDSelectedIndexChanged(sender, eventArgs) {
    var ActID = sender;
    ShowRegularGrid("", "", "", "", "", "", "", "", ActID, FormatedItemCombo(Regoptions), $("#ComboForRegMonth").val(), false);
}

function ShowReg() {
  //  ddlRegularStoreName($("#ComboForRegMonth").val());
    // $('#ddlAccountID').empty();
    descptoptions = [];
    options = [];
    $('#FilterByDescription').find('input[type=checkbox]:checked').removeAttr('checked');
    $('#FilterBystorenameandAccountID').find('input[type=checkbox]:checked').removeAttr('checked');
    $('#ddllDescription').empty();
    $('#TxtStartdate').val('');
    $('#TxtTargetdate').val('');
    ShowRegularGrid("", "", "", "", "", "", "", "", "", "", FormatedItemCombo(Regoptions), $("#ComboForRegMonth").val(), true);
    var StName = FormatedItemCombo(Regoptions);
    ddlRegularDescription($("#ComboForRegMonth").val(),"");
}
function ShowRegularTask() {
    debugger;
    $("#FilterOption").val("ByFilter");
    var status = FormatedItemCombo(Regoptions);
    var Month = $("#ComboForRegMonth").val();
    var StoreName = FormatedItemCombo(options);
    var DescriptionName = FormatedItemCombo(descptoptions);
    var AccountID = $('#ddlAccountID').val();
    var StartDate = $('#TxtStartdate').val();
    var TargetDate = $('#TxtTargetdate').val();
    ShowRegularGrid("", "", "", DescriptionName, "","", StartDate, TargetDate, StoreName, AccountID, FormatedItemCombo(Regoptions), Month, true);
    //ShowRegularGrid("", "", "", "", "", StartDate, TargetDate, StoreName, DescriptionName, AccountID, FormatedItemCombo(Regoptions), Month);
}
function SearchByJobnumber() {
    var Jobnumber = $('#TxtSearchByJobnumber').val();
    ShowRegularGrid("", "", "","", Jobnumber, "", "", "", "", "", FormatedItemCombo(Regoptions), $("#ComboForRegMonth").val());
}

function SearchByDescription() {
    debugger;
    var Description = $('#TxtSearchByDescription').val();
    ShowRegularGrid("", "", "", "", "", Description, "", "", "", "", FormatedItemCombo(Regoptions), $("#ComboForRegMonth").val());
}

function SearchByStoreName() {
    var SearchStoreName = $('#TxtSearchByStoreName').val();
    ShowRegularGrid("", "", SearchStoreName, "", "", "", "", "", "","", FormatedItemCombo(Regoptions), $("#ComboForRegMonth").val());
}
function FilterByStartdaterange() {
    var From = $('#TxtStartdateFrom').val();
    var To = $('#TxtStartdateTo').val();
    StartDaterange = From + "~" + To;
    ShowRegularGrid(StartDaterange,"","", "", "", "", "", "", "","", FormatedItemCombo(Regoptions), $("#ComboForRegMonth").val(), true);
}
function FilterByTargetdaterange() {
    var From = $('#TxtTargetdateFrom').val();
    var To = $('#TxtTargetdateTo').val();
    TargetDaterange = From + "~" + To;
    ShowRegularGrid("", TargetDaterange, "", "", "", "", "", "", "","", FormatedItemCombo(Regoptions), $("#ComboForRegMonth").val(), true);
}

function GetMonthlyRegSh(MonthType, ComboStatus) {
    var ComboStatus = FormatedItemCombo(Regoptions);
    if (ComboStatus != null) {
        GetMonthlyRegShDetail($("#ComboForRegMonth").val(), ComboStatus);
    }
}


//Offcycle

function OffcycleAdsStbindBoot() {
    removeOptions(document.getElementById("s7"));
    $.ajax({
        type: "GET",
        url: "/DataTracker.svc/GetAdDetectedStatusList",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            if (data.d != null) {
                for (i = 0; i < data.d.length; i++) {
                    if (data.d[i].StatusID == 12 || data.d[i].StatusID == 14 || data.d[i].StatusID == 25 || data.d[i].StatusID == 29 || data.d[i].StatusID == 31 || data.d[i].StatusID == 32) {


                        $("#s7").append("<option  value=' " + data.d[i].StatusID + "' text='" + data.d[i].StatusName + "'>" + data.d[i].StatusName + "</option>");
                    }
                    else {
                        $("#s7").append("<option selected='selected' value=' " + data.d[i].StatusID + "' text='" + data.d[i].StatusName + "'>" + data.d[i].StatusName + "</option>");
                        Offoptions.push("" + data.d[i].StatusID + "");
                    }
                }
            }
            createDropdown("#s7", 's7');
            var CDate = $("#DivOffCal").val();
            GetOffcycleNewStAds(CDate, FormatedItemCombo(Offoptions));

        }
    });
}

function OffcycleStatusItemChecked() {
    var ComboStatus = FormatedItemCombo(Offoptions);
    if (ComboStatus != null) {
        GetOffcycleNewStAds($("#DivOffCal").val(), ComboStatus);
    }
}
function BindOffcycleFilterSt() {

    OffcycleAdsStbindBoot();
}
function GetOffCycleNewAdsFilters() {
    ForGetOffCycleNewAdsFilters(FormatedItemCombo(Offoptions))
}

//Ads
var AdsStoreoptions = new Array();
function storecombindBoot() {

    $.ajax({
        type: "GET",
        url: "/DataTracker.svc/GetAllStoreNameList",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            if (data.d != null) {
                for (i = 0; i < data.d.length; i++) {
                    $("#s6").append("<option  value=' " + data.d[i].StoreName + "' text='" + data.d[i].StoreName + "'>" + data.d[i].StoreName + "</option>");
                }
                createDropdown("#s6", 's6');
                AdsStatuscombindBoot();
            }

        }
    });
}

var AdsStatusoptions = new Array();
function AdsStatuscombindBoot() {

    $.ajax({
        type: "GET",
        url: "/DataTracker.svc/BindStatusAdsComBoot",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            if (data.d != null) {
                for (i = 0; i < data.d.length; i++) {
                    $("#s5").append("<option  value=' " + data.d[i].StatusID + "' text='" + data.d[i].Status + "'>" + data.d[i].Status + "</option>");
                }
                createDropdown("#s5", 's5');
            }

        }
    });
}
function FilterGridByStartDate() {

    SetDefaultStartDate();
    FilterByStartDate(FormatedItemCombo(AdsStoreoptions), FormatedItemCombo(AdsStatusoptions), $("#datepickerStartDate").val(), "");
}
function FilterGridBydueDate() {
    FilterBydueDate(FormatedItemCombo(AdsStoreoptions), FormatedItemCombo(AdsStatusoptions), $("#datepickerStartDate").val(), "");
}
function FilterGridByOverDueDate() {
    FilterByOverDueDate(FormatedItemCombo(AdsStoreoptions), FormatedItemCombo(AdsStatusoptions), $("#datepickerStartDate").val(), "", true);
}

function StoreItemChecked() {
    BindDetailsGrid(FormatedItemCombo(AdsStoreoptions), FormatedItemCombo(AdsStatusoptions), $("#datepickerStartDate").val(), "", "0", false);
}

function removeOptions(selectbox) {
    var i;
    for (i = selectbox.options.length - 1; i >= 0; i--) {
        if (i == 0) { }
        else
            selectbox.remove(i);
    }
}