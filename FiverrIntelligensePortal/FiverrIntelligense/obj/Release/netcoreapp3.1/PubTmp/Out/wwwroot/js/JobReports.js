﻿        var monthdate = "";         
         $(document).ready(function () {
//             var months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December']; ;
//             var date = new Date();
//         var combo = $find("<%= RadComboBox1.ClientID %>");
//         var itmonth = combo.findItemByValue(myItemValue);
//         itmonth.select();
//             if ($("#DivReportPre").length > 0) {
//                 var CMonth = months[date.getMonth()];
//                 var PreMonth = months[date.getMonth() - 1];
//                 var NMonth = months[date.getMonth() + 1];  
//                 getjobdata("2");
//                 $("#DivReportPre")[0].innerHTML = String.format("<div style='width:100%' ><div  ID='DivMthPre' class='divMonthPre' onclick='javascript:getjobdata({1});'><a class='PrecurrNextFont' title='{0}' style='width:35%'   id='ReportAnPre' >{2}</a></div><div class='DivMonthNext'><div  class='DivMonthLast1' align='center' onclick='javascript:getjobdata({4});'><a class='SelectedNextFont' title='{3}' style='width:100%'   id='ReportAnCurr'>{5}</a></div><div align='center' onclick='javascript:getjobdata({7});'  class='DivMonthLast2'><a id='ReportAnNext' class='PrecurrNextFont' title='{6}'  style='width:100%'  >{8}</a></div></div>", PreMonth, "1", PreMonth, CMonth, "2", CMonth, NMonth, "3", NMonth);
//             }
         });

          function getjobdata(itmonth) { 
         // alert(itmonth);            
             monthdate = itmonth;                      

            var selecteddate = $("#txtdatepicker").val();
            var dataSourceStore = "";
            dataSourceStore = new kendo.data.DataSource({
                transport: {
                    read: {

                        url: "/DataTracker.svc/GetJobReport",
                        data: {
                            month: itmonth
                          
                        }
                    }
                },
                schema: {
                    data: "d",
                    total: function (d) {
                        if (d.d.length > 0)
                            return d.d[0].TotalCount
                    },
                    model: {

                        fields: {
                            Week: { editable: false, type: "int" },
                           // startdate: { editable: false, type: "string" },
                            CountStore: { editable: false, type: "int" },
                            ScheduleJobs: { editable: false, type: "int" },
                            DeliverJob: { editable: false, type: "int" },
                            AddressJob: { editable: false, type: "int" },
                            NoAdsAvail: { editable: false, type: "int" },
                            PDFJobs: { editable: false, type: "int" },
                            CountNew: { editable: false, type: "int" },
                            OffcycleJobs: { editable: false, type: "int" },
                            SecondaryJobs: { editable: false, type: "int" },
                            SecondaryWeek: { editable: false, type: "int" },
                            AnalysisCount: { editable: false, type: "int" }
                        }
                    }
                },
                pageSize: 5,
                serverPaging: true

            });

            if(dataSourceStore !="")
            {
                if($("#reportjDategrid").length>0)
                {
                    $("#reportjDategrid")[0].innerHTML="";
                }

            }
            var element = $("#reportjDategrid").kendoGrid({
                dataSource: dataSourceStore,
//            excel: {
//                fileName: "JobReport.xlsx",
//                filterable: true,
//                allPages: true
//                    },
                detailInit: GetJobsByGroupName,
                dataBound: function () {
                            
                },
                pageable: {
                   // batch: true,
                    pageSize: 5,
                    refresh: true,
                    pageSizes: true,
                   // input: true
                },
                columns: [
                            {
                                field: "Week",
                                title: "Week",
                                width: "5%"

                            },

                            {
                                field: "CountStore",
                                title: "Count of Stores",
                                width: "10%"
                            },
                            {
                                field: "ScheduleJobs",
                                title: "Scheduled Jobs",
                                width: "10%"
                            },

                            {
                                field: "AddressJob",
                                title: "Addresses Not Found",
                                width: "10%",
                                template: "#=ShowAllDataByGroup(AddressJob,Week,' ',' '," + AddressnotMatch + ")#"
                            },
                            {
                                field: "NoAdsAvail",
                                title: "Ads Not Found",
                                width: "8%",
                                template: "#=ShowAllDataByGroup(NoAdsAvail,Week,' ',' ',"  + Noaddeted + ")#"
                            },
                            {
                                field: "PDFJobs",
                                title: "PDF Jobs",
                                width: "5%",
                                template: "#=ShowAllDataByGroup(PDFJobs,Week,' ',' '," + CancelPdf + ")#"
                            }, 
                            {
                                field: "CountNew",
                                title: "Not yet completed",
                                width: "12%",
                                template: "#=ShowAllDataByGroup(CountNew,Week,' ',' ',"  + NewJobsSt + ")#"
                            },
                               {
                                   field: "OffcycleJobs",
                                   title: "Off cycle ads",
                                   template: "#=ShowAllDataByGroup(OffcycleJobs,Week,' ',' '," + OffcycleSt + ")#",
                                   width: "5%"                                  
                               },
                                                              {
                                   field: "SecondaryJobs",
                                   title: "Secondary Jobs",
                                   template: "#=ShowAllDataByGroup(SecondaryJobs,Week,' ',' ',SecondaryWeek)#",
                                   width: "5%"                                  
                               },
                               
                                                              {

                               field:"AnalysisCount",
                               title:"Analysis Count",
                               template: "#=ShowAllDataByGroup(AnalysisCount,Week,' ', ' ',"+ Analysis +")#",
                               width: "10%"

                               }                        
                            ],

                editable: false,
                sortable: true,
                selectable: true

            });

        ShowLastUpdatedTime(monthdate);


        }
        // 2nd grid       

        function GetJobsByGroupName(e) {
            $("<div/>").appendTo(e.detailCell).kendoGrid({
                dataSource: {
                    transport: {
                        read: {
                            //using jsfiddle echo service to simulate JSON endpoint
                            url: "/DataTracker.svc/GetJobDetailReport_GroupBy",
                            data: {
                               
                               // datetime: e.data.startdate,
                                date: monthdate, 
                                Week: e.data.Week
                                
                            }
                        }
                    },
                    serverPaging: true,
                    serverSorting: true,
                    serverFiltering: true,
                    pageSize: 20,
                    schema: {
                        data: "d",
                        total: function (d) {
                            if (d.d.length > 0)
                                return d.d[0].TotalCount
                        },
                        model: {
                            id: "ID",
                            fields: {

                                StoreName: { editable: false, type: "string" },
                                ScheduleJobs: { editable: false, type: "string" },
                                DeliverJob: { editable: false, type: "string" },
                                AddressJob: { editable: false, type: "string" },
                                NoAdsAvail: { editable: false, type: "string" },
                                PDFJobs: { editable: false, type: "string" },
                                NewJobs: { editable: false, type: "string" },
                                Week:  { editable: false, type: "string" },
                                OffcycleJobs: { editable: false, type: "string" }
                            }
                        }
                    }
                },
                pageable: true,
                detailInit: JobsWithStoreNameInit,
                dataBound: function () {
                    // this.expandRow(this.tbody.find("tr.k-master-row").first());             
                },
                columns: [
                            {
                                field: "StoreName",
                                title: "Group Name",
                                width: "22%"
                            },
                            {
                                field: "ScheduleJobs",
                                title: "Scheduled Jobs",
                                width: "14%"
                            },
//                            {
//                                field: "DeliverJob",
//                                title: "Jobs Delivered",
//                                width: "0%",
//                                template: "#=ShowAllDataByGroup(DeliverJob,Week,StoreName,' '," + ComValue + ")#"
//                            },
                            {
                                field: "AddressJob",
                                title: "Addresses Not Found",
                                width: "17%",
                                template: "#=ShowAllDataByGroup(AddressJob,Week,StoreName,' '," + AddressnotMatch + ")#"
                            },
                            {
                                field: "NoAdsAvail",
                                title: "Ads Not Found",
                                width: "10%",
                                template: "#=ShowAllDataByGroup(NoAdsAvail,Week,StoreName,' '," + Noaddeted + ")#"
                            },
                            {
                                field: "PDFJobs",
                                title: "PDF Jobs",
                                width: "10%",
                                template: "#=ShowAllDataByGroup(PDFJobs,Week,StoreName,' '," + CancelPdf + ")#"
                            },
                            {
                                field: "NewJobs",
                                title: "Not yet completed",
                                width: "15%",
                                template: "#=ShowAllDataByGroup(NewJobs,Week,StoreName,' '," + NewJobsSt + ")#"
                            },
//                            {
//                                field: "Week",
//                                title: "Week",
//                                width: "0%"

//                            },
                            {
                                field: "OffcycleJobs",
                                title: "Off cycle ads",
                                template: "#=ShowAllDataByGroup(OffcycleJobs,Week,StoreName,' '," + OffcycleSt + ")#",
                                width: "11%"

                            }
                            ],
                editable: false,
                selectable: true

            });
        }
        
        function JobsWithStoreNameInit(e) {           
            $("<div/>").appendTo(e.detailCell).kendoGrid({
                dataSource: {
                    transport: {
                        read: {
                            //using jsfiddle echo service to simulate JSON endpoint
                            url: "/DataTracker.svc/GetJobDetailReport_Page",
                            data: {
                                
                               // datetime: e.data.startdate,
                                date: monthdate,
                                Week: e.data.Week,
                                GroupName:e.data.StoreName
                            }
                        }
                    },
                    serverPaging: true,
                    serverSorting: true,
                    serverFiltering: true,
                    pageSize: 20,                
                    schema: {
                        data: "d",
                        total: function (d) {
                            if (d.d.length > 0)
                                return d.d[0].TotalCount
                        },
                        model: {
                            id: "ID",
                            fields: {

                                StoreName: { editable: false, type: "string" },
                                ScheduleJobs: { editable: false, type: "string" },
                                DeliverJob: { editable: false, type: "string" },
                                AddressJob: { editable: false, type: "string" },
                                NoAdsAvail: { editable: false, type: "string" },
                                PDFJobs: { editable: false, type: "string" },
                                NewJobs: { editable: false, type: "string" },
                                Week: { editable: false, type: "string" },
                                OffcycleJobs: { editable: false, type: "string" }
                            }
                        }
                    }
                },               
                pageable: true,
                dataBound: function () {
                    // this.expandRow(this.tbody.find("tr.k-master-row").first());             
                },
                columns: [
                            {
                                field: "StoreName",
                                title: "Store Name",
                                width: "24%"
                            },
                            {
                                field: "ScheduleJobs",
                                title: "Scheduled Jobs",
                                width: "12%"
                            },
//                            {
//                                field: "DeliverJob",
//                                title: "Jobs Delivered",
//                                width: "0%",
//                                template: "#=ShowAllDataByGroup(DeliverJob,Week,' ',StoreName," + ComValue + ")#"
//                            },
                            {
                                field: "AddressJob",
                                title: "Addresses Not Found",
                                width: "15%",
                                template: "#=ShowAllDataByGroup(AddressJob,Week,' ',StoreName," + AddressnotMatch + ")#"
                            },
                            {
                                field: "NoAdsAvail",
                                title: "Ads Not Found",
                                width: "12%",
                                template: "#=ShowAllDataByGroup(NoAdsAvail,Week,' ',StoreName," + Noaddeted + ")#"
                            },
                            {
                                field: "PDFJobs",
                                title: "PDF Jobs",
                                width: "10%",
                                template: "#=ShowAllDataByGroup(PDFJobs,Week,' ',StoreName," + NewJobsSt + ")#"
                            },
                            {
                                field: "NewJobs",
                                title: "Not yet completed",
                                width: "15%",
                                template: "#=ShowAllDataByGroup(NewJobs,Week,' ',StoreName," + NewJobsSt + ")#"
                            },
                            {
                                field: "OffcycleJobs",
                                title: "Off cycle ads",
                               // template: "#=ShowAllDataByGroup(OffcycleJobs,Week,startdate,' ',StoreName,' '," + OffcycleSt + ")#",
                                width: "12%"
                                
                            },                           
//                            {
//                                field: "Week",
//                                title: "",
//                                width: "0%"

//                            }
                            ],
                editable: false,
                selectable: true

            });
        }

        function ShowAllDataByGroup(StatusName, Week, Gname, Sname, Type) {            
            var fianlcontent = "";  
            var monthh = monthdate.split('~');          
            var mth =monthh[0];
            var yyr = monthh[1];                            
            if (Week == " ")
                Week = 0;
           
            if (StatusName == "0") {
                return "<div >" + StatusName + "</div>";
            }
            else if(StatusName == "NA")
            {
              return "<div >" + StatusName + "</div>";
            }
            else {               
                //var d = new Date(startdate); //.toLocaleDateString("en-US");             
             
                var listvar = "";
                if (Gname == " " && Sname == " ")
                    listvar = String.format("{0}~{1}~{2}~{3}~{4}~{5}~{6}", mth, Week, Type, "0", "0","a",yyr);
                else if (Gname != " " && Sname == " ") {
                    listvar = String.format("{0}~{1}~{2}~{3}~{4}~{5}~{6}", mth, Week, Type, escape(Gname), '0',"g",yyr);
                }
                else if (Gname == " " && Sname != " ") {
                    listvar = String.format("{0}~{1}~{2}~{3}~{4}~{5}~{6}", mth, Week, Type,  "0", escape(Sname),"s",yyr);
                }
                else {
                    listvar = String.format("{0}~{1}~{2}~{3}~{4}~{5}~{6}", mth, Week, Type, escape(Gname), escape(Sname),"a",yyr);
                }
                var ctype="0";
                if(Type=="31")
                {
                  ctype="1"
                }
              fianlcontent = "<div title='View all' style='cursor:pointer; cursor:hand; text-decoration:underline' href='#' onclick='javascript:OpenRePopup(\"" + listvar + "\",\"" + ctype + "\");'>" + StatusName + "</div>";
                 return fianlcontent;
            }
        }
        function parseDate(input) {
            var parts = input.split('/');          
            return new Date(parts[0], parts[1] - 1, parts[2]); 
        }
        function OpenRePopup(Qst, type) {           
            //month, week, type, dday, mmonth, yyear, Gname, Sname
            //var ddate = dday + '/' + mmonth + '/' + yyear;           
            var splitQst=Qst.split('~');
           //  alert(type);
            var Ptitle="Job details";
            if(splitQst[2]=="12")
              Ptitle="Delivered jobs";
              else if(splitQst[2] == "50")
              {Ptitle="Analysis Report";}
            else if(splitQst[2]=="19")
            { Ptitle="Ads not found jobs";}
            else if(splitQst[2]=="22")
            {Ptitle="Addresses not found jobs";}  
            else if(splitQst[2]=="25")
            {Ptitle="PDF jobs";}
            else if(splitQst[2]=="31")
            {Ptitle="Offcyle ads jobs";}
            else if(splitQst[2]=="8")
            {Ptitle="Not yet completed jobs";}   
             else {
             var ss=splitQst[2].split('^');
             if(ss[0]==11)
            {Ptitle="Secondary Ads";}    
            }
               
            var windowElement = $("#windows").kendoWindow({
                iframe: true,
               // content: "Detail_job.aspx?month=" + escape(month) + "&week=" + week + "&type=" + type + "&ddate=" + ddate + "&Gname=" + escape(Gname) + "&Sname=" + escape(Sname),
                content: "Detail_job.aspx?ddate=" + Qst + "&Cy=" + type,
                modal: true,
                width: "70%",
                title: Ptitle,
                height: "550px",
                position: {
                    top: 4,
                    left: 2
                },
                resizable: false,
                actions: ["Close"],
                draggable: false

            });
            var dialog = $("#windows").data("kendoWindow");
             dialog.setOptions({
        title: Ptitle
     });
            dialog.open();
            dialog.center();
            return false;
        }

        function JobsWithStoreNameInit(e) {
            $("<div/>").appendTo(e.detailCell).kendoGrid({
                dataSource: {
                    transport: {
                        read: {
                            //using jsfiddle echo service to simulate JSON endpoint
                            url: "/DataTracker.svc/GetJobDetailReport_Page",
                            data: {
                                week: e.data.Week,
                               // datetime: e.data.startdate,
                                date: monthdate,
                                GroupName: e.data.StoreName
                                
                            }
                        }
                    },
                    serverPaging: true,
                    serverSorting: true,
                    serverFiltering: true,
                    pageSize: 20,
                    schema: {
                        data: "d",
                        total: function (d) {
                            if (d.d.length > 0)
                                return d.d[0].TotalCount
                        },
                        model: {
                            id: "ID",
                            fields: {

                                StoreName: { editable: false, type: "string" },
                                ScheduleJobs: { editable: false, type: "string" },
                                DeliverJob: { editable: false, type: "string" },
                                AddressJob: { editable: false, type: "string" },
                                NoAdsAvail: { editable: false, type: "string" },
                                PDFJobs: { editable: false, type: "string" },
                                NewJobs: { editable: false, type: "string" },
                                Week: { editable: false, type: "string" },
                                OffcycleJobs: { editable: false, type: "string" }
                            }
                        }
                    }
                },
                pageable: true,
                dataBound: function () {
                    // this.expandRow(this.tbody.find("tr.k-master-row").first());             
                },
                columns: [
                            {
                                field: "StoreName",
                                title: "Store Name",
                                width: "25%"
                            },
                            {
                                field: "ScheduleJobs",
                                title: "Scheduled Jobs",
                                width: "15%"
                            },
//                            {
//                                field: "DeliverJob",
//                                title: "Jobs Delivered",
//                                width: "0%",
//                                template: "#=ShowAllDataByGroup(DeliverJob,Week,' ',StoreName," + ComValue + ")#"
//                            },
                            {
                                field: "AddressJob",
                                title: "Addresses Not Found",
                                width: "17%",
                                template: "#=ShowAllDataByGroup(AddressJob,Week,' ',StoreName," + AddressnotMatch + ")#"
                            },
                            {
                                field: "NoAdsAvail",
                                title: "Ads Not Found",
                                width: "12%",
                                template: "#=ShowAllDataByGroup(NoAdsAvail,Week,' ',StoreName," + Noaddeted + ")#"
                            },
                            {
                                field: "PDFJobs",
                                title: "PDF Jobs in Schedule",
                                width: "17%",
                                template: "#=ShowAllDataByGroup(PDFJobs,Week,' ',StoreName," + NewJobsSt + ")#"
                            },
                            {
                                field: "NewJobs",
                                title: "Not yet completed",
                                width: "16%",
                                template: "#=ShowAllDataByGroup(NewJobs,Week,' ',StoreName," + NewJobsSt + ")#"
                            },
                            {
                                field: "OffcycleJobs",
                                title: "Off cycle",
                                // template: "#=ShowAllDataByGroup(OffcycleJobs,Week,startdate,' ',StoreName,' '," + OffcycleSt + ")#",
                                width: "11%"

                            },
//                            {
//                                field: "Week",
//                                title: "",
//                                width: "0%"

//                            }
                            ],
                editable: false,
                selectable: true

            });
        }


function ShowLastUpdatedTime(Month)
{
    $.ajax({
        type: "GET",
        url: "/DataTracker.svc/GetUpdatedTime?Month="+Month,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            var res = data.d;
            if (res != null) {
                if (res != "") {
                    if (res != "") {
                       
                       $("#DivLastUpdate")[0].innerHTML="<b>Updated Time: "+res+"</b>";
                    }
                    else {
                    
                        $("#DivLastUpdate")[0].innerHTML="";
                       
                    }
                   
                }
            }
            else {
               
               $("#DivLastUpdate")[0].innerHTML="";
            }
        }
    });
   
}
