﻿
var late_Date = '';
var current_userId = '';
var current_userTypeid = '';
var ldatee = '';
var today_Daterr = '';
var today_Date = '';
var excelfilename = "";
var comboboxval = 0;

$(window).load(function () {
    OnB_Clock();
});

function Setusersids(userid, utypeid) {
    current_userId = userid;
    current_userTypeid = utypeid;
}

function lastseven() {
    $("#AllusersGrid").innerHTML = "";
    $("#AllusersGrid").html("");
    var cdate = new Date();
    var dd = cdate.getDate();
    var mm = cdate.getMonth() + 1;
    var yyyy = cdate.getFullYear();
    today_Date = yyyy + '/' + mm + '/' + dd;
    ShowDatediv();
    setdatevall();
    if ($("#Lastsevendays").is(':checked')) {
        //ShowDepartmentworkList(Pre_Date, today_Date);
        var msecPerDay = 7 * 24 * 60 * 60 * 1000;
        var Seven_days_ago = new Date(cdate.getTime() - msecPerDay);
        var sdd = Seven_days_ago.getDate();
        var smm = Seven_days_ago.getMonth() + 1;
        var syyyy = Seven_days_ago.getFullYear();
        var Pre_Date = syyyy + '/' + smm + '/' + sdd;
        late_Date = Pre_Date;
        excelfilename = "Last_7_days_TimeReport";
        if (current_userTypeid == '12' || current_userId == '173') {
            $("#Selfi").hide();
            $("#Myteam").hide();
            $("#allpermissions").val("1");
            if (comboboxval == 1) {
                //  ShowDepartmentworkList(Pre_Date, today_Date);

            }
            else if (comboboxval == 2) {
                $("#allpermissions").val("2");
                //  ShowTeamsworkList(Pre_Date, today_Date);
                // alert("altem");
                GetAllTeamsTime(Pre_Date, today_Date);

            }
            else {
                // ShowDepartmentworkList(Pre_Date, today_Date);
            }
        }
        else if (current_userTypeid == '14') {
            $("#allempl").hide();
            $("#allteamss").hide();
            $("#allpermissions").val("3");
            //            $("#AllusersGrid")[0].innerHTML = "";
            //            if ($("#AllusersGrid").data("kendoWindow") != null) {
            //                $('#AllusersGrid').data().kendoGrid.destroy();
            //                $('#AllusersGrid').empty();
            //            }
            if (comboboxval == 3) {
                // BindIndividualTaskGrid(current_userId, Pre_Date, today_Date, excelfilename);
                //alert("self");
            }
            else if (comboboxval == 4) {
                $("#allpermissions").val("4");
                //ShowDepartmentworkTL(current_userId, Pre_Date, today_Date);
            }
            else {
                // BindIndividualTaskGrid(current_userId, Pre_Date, today_Date, excelfilename);
            }
        }
        else {
            $("#allempl").hide();
            $("#allteamss").hide();
            $("#Myteam").hide();
            $("#allpermissions").val("3");
            // BindIndividualTaskGrid(current_userId, Pre_Date, today_Date, excelfilename);
        }
    }
    else if ($("#Lastthirty").is(':checked')) {
        var msecPerDay = 30 * 24 * 60 * 60 * 1000;
        var Seven_days_ago = new Date(cdate.getTime() - msecPerDay);
        var sdd = Seven_days_ago.getDate();
        var smm = Seven_days_ago.getMonth() + 1;
        var syyyy = Seven_days_ago.getFullYear();
        var Pre_Date = syyyy + '/' + smm + '/' + sdd;
        late_Date = Pre_Date;
        excelfilename = "Last_30_days_TimeReport";
        // ShowDepartmentworkList(Pre_Date, today_Date);
        if (current_userTypeid == '12' || current_userId == '173') {
            $("#Selfi").hide();
            $("#Myteam").hide();
            $("#allpermissions").val("1");
            if (comboboxval == 1) {
                // ShowDepartmentworkList(Pre_Date, today_Date);
                GetAllTeamsTime(Pre_Date, today_Date);
            }
            else if (comboboxval == 2) {
                $("#allpermissions").val("2");
                // ShowTeamsworkList(Pre_Date, today_Date);
                //alert("altem");
            }
            else {
                // ShowDepartmentworkList(Pre_Date, today_Date);
            }
        }
        else if (current_userTypeid == '14') {
            $("#allempl").hide();
            $("#allteamss").hide();
            $("#allpermissions").val("3");
            //            $("#AllusersGrid")[0].innerHTML = "";
            //            if ($("#AllusersGrid").data("kendoWindow") != null) {
            //                $('#AllusersGrid').data().kendoGrid.destroy();
            //                $('#AllusersGrid').empty();
            //            }
            if (comboboxval == 3) {
                //  BindIndividualTaskGrid(current_userId, Pre_Date, today_Date, excelfilename);
                //alert("self");
            }
            else if (comboboxval == 4) {
                $("#allpermissions").val("4");
                // ShowDepartmentworkTL(current_userId, Pre_Date, today_Date);
            }
            else {
                //BindIndividualTaskGrid(current_userId, Pre_Date, today_Date, excelfilename);
            }
        }
        else {
            $("#allempl").hide();
            $("#allteamss").hide();
            $("#Myteam").hide();
            $("#allpermissions").val("3");
            // BindIndividualTaskGrid(current_userId, Pre_Date, today_Date, excelfilename);
        }
    }
    else if ($("#CustomRadiobt").is(':checked')) {

        Pre_Date = $("#dtpStartDate").val();
        today_Date = $("#dtpEndDate").val();
        late_Date = Pre_Date;
        excelfilename = "From" + Pre_Date + "-" + today_Date + "Timereport";
        if (current_userTypeid == '12' || current_userId == '173') {
            $("#Selfi").hide();
            $("#Myteam").hide();
            $("#allpermissions").val("1");
            if (comboboxval == 1) {
                //ShowDepartmentworkList(Pre_Date, today_Date);
                GetAllTeamsTime(Pre_Date, today_Date);
            }
            else if (comboboxval == 2) {
                $("#allpermissions").val("2");
                // ShowTeamsworkList(Pre_Date, today_Date);
                // alert("altem");
            }
            else {
                // ShowDepartmentworkList(Pre_Date, today_Date);
            }
        }
        else if (current_userTypeid == '14') {

            $("#allempl").hide();
            $("#allteamss").hide();
            $("#allpermissions").val("3");
            // $("#AllusersGrid")[0].innerHTML = "";
            //            if ($("#AllusersGrid").data("kendoWindow") != null) {
            //                $('#AllusersGrid').data().kendoGrid.destroy();
            //                $('#AllusersGrid').empty();
            //            }
            if (comboboxval == 3) {
                // BindIndividualTaskGrid(current_userId, Pre_Date, today_Date, excelfilename);
                // alert("self");
            }
            else if (comboboxval == 4) {
                $("#allpermissions").val("4");
                //ShowDepartmentworkTL(current_userId, Pre_Date, today_Date);
            }
            else {
                // BindIndividualTaskGrid(current_userId, Pre_Date, today_Date, excelfilename);
            }
        }
        else {
            $("#allempl").hide();
            $("#allteamss").hide();
            $("#Myteam").hide();
            $("#allpermissions").val("3");
            //BindIndividualTaskGrid(current_userId, Pre_Date, today_Date, excelfilename);
        }
    }
    else {
        //alert("hh");
        return false;
    }
    GetAllDatevalueonpage();
}

function setdatevall() {
    var cdate = new Date();
    var dd = cdate.getDate();
    var mm = cdate.getMonth() + 1;
    var yyyy = cdate.getFullYear();
    today_Daterr = yyyy + '/' + mm + '/' + dd;
    if ($("#Lastsevendays").is(':checked')) {
        var msecPerDay = 7 * 24 * 60 * 60 * 1000;
        var Seven_days_ago = new Date(cdate.getTime() - msecPerDay);
        var sdd = Seven_days_ago.getDate();
        var smm = Seven_days_ago.getMonth() + 1;
        var syyyy = Seven_days_ago.getFullYear();
        var Pre_Date = syyyy + '/' + smm + '/' + sdd;
        ldatee = Pre_Date;
    }
    else if ($("#Lastthirty").is(':checked')) {
        var msecPerDay = 30 * 24 * 60 * 60 * 1000;
        var Seven_days_ago = new Date(cdate.getTime() - msecPerDay);
        var sdd = Seven_days_ago.getDate();
        var smm = Seven_days_ago.getMonth() + 1;
        var syyyy = Seven_days_ago.getFullYear();
        var Pre_Date = syyyy + '/' + smm + '/' + sdd;
        ldatee = Pre_Date;
    }
    else if ($("#CustomRadiobt").is(':checked')) {
        ldatee = $("#dtpStartDate").val();
        today_Daterr = $("#dtpEndDate").val();
    }
    else {
        return false;
    }
    // GetAllDatevalueonpage();
}


function ShowDatediv() {
    if ($("#CustomRadiobt").is(':checked')) {
        $('#AllDateDIv').css('display', 'block');
    }
    else {
        $('#AllDateDIv').css('display', 'none');
    }

}
function GetPieChartDisplay() {
    var dataforpi = new Array();
    if (allTeamlist != "" && allTeamlist != null) {
        for (var i = 0; i < allTeamlist.length; i++) {
            var itemss = allTeamlist[i].split('~')
            var fvall = '{' + "'country'" + ":" + "'" + itemss[0] + "'" + "," + "'value'" + ":" + itemss[1] + '}';
            dataforpi.push(fvall.replace('"', ''));

        }
    }
    else
        return false;
    var chart = AmCharts.makeChart("chartdiv", {
        "type": "pie",
        "theme": "light",
        "dataProvider": [dataforpi],
        //            "dataProvider": [{
        //                "country": "Break Time",
        //                "value": 10
        //            },
        //         {
        //             "country": "Work Time",
        //             "value": 15
        //         },
        //         {
        //             "country": "Idel Time",
        //             "value": 20
        //         }],
        "valueField": "value",
        "titleField": "country",
        "outlineAlpha": 0.4,
        "depth3D": 15,
        "balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
        "angle": 30,
        "export": {
            "enabled": true
        }
    });
    jQuery('.chart-input').off().on('input change', function () {
        var property = jQuery(this).data('property');
        var target = chart;
        var value = Number(this.value);
        chart.startDuration = 0;

        if (property == 'innerRadius') {
            value += "%";
        }

        target[property] = value;
        chart.validateNow();
    });
}

function GetAllDatevalue() {
    return ldatee + "~" + today_Daterr;
}
function GetAllDatevalueonpage() {
    var alldatevalues = GetAllDatevalue();
    document.getElementById("ContentMain_HiddenField1").value = alldatevalues;
}
var allTeamlist = new Array();
function GetAllTeamsTime(Pre_Date, today_Date) {
    $.ajax({
        type: "GET",
        url: "/DataTracker.svc/GetDepartmentsTasksTime?FDate='" + Pre_Date + "'&Ldate='" + today_Date + "'",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {

            if (data.d != null) {
                for (var i = 0; i < data.d.length; i++) {
                    // alert(data.d[i].DeName);
                    allTeamlist.push(data.d[i].DeName + "~" + data.d[i].TotalWorkTime + "~" + data.d[i].TotalBreakTime);
                }
            }
            GetPieChartDisplay();
        },
        failure: function (response) {
            // alert("f1");
        },
        error: function (xhr, status, error) {
            // alert("f2");
        }

    });

}
function GetReportAccordintoCombo() {
    comboboxval = $("#allpermissions").val();
    lastseven();
}

function OnB_Clock() {
    
    var Hv = document.getElementById("ContentMain_HiddenField1").value;
    var Hva = Hv.split('~');

    var date1 = new Date(Hva[0]);
    var date2 = new Date(Hva[1]);
    var timeDiff = Math.abs(date2.getTime() - date1.getTime());
    var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
    // alert(diffDays);
    if (diffDays > 30) {
        alert("Date diffrence should be less or equal to one month");
        return false;
    }
    $("#DateRangeofReport")[0].innerHTML = "";
    $("#DateRangeofReport").append("Timesheet from " + getformateddate(Hva[0]) + " To " + getformateddate(Hva[1]));
    var dval = $("#ContentMain_DropDownList1").val();

    $("#ttSpent")[0].innerHTML = "";
    $("#Reportfor")[0].innerHTML = "";
    $("#AllTables")[0].innerHTML = "";
    $("#fval")[0].innerHTML = "";
    $("#fval").append("&copy; " + getcfy() + " <a href='#'>Loginworks.com</a>");
    if (dval == 3) {
        GetAllTeamsTimeChartPie(Hva[0], Hva[1]);
    }
    else if (dval == 2) {
        GetteamwisedataD(Hva[0], Hva[1], 0);
    }
    else if (dval == 4) {
        GetteamwisedataD(Hva[0], Hva[1], $("#ContentMain_DropDownList2").val());
    }
    else if (dval == 1) {
        GetSingleuserdata(Hva[0], Hva[1]);
    }


}

function GetAllTeamsTimeChartPie(sd, ld) {
    var MyArray = new Array();
    var MyArrayAvg = new Array();

    // $("#AllAspControls")[0].style.display = "none";
    // $("#AllControls")[0].style.display = "none";
    $("#Ibd")[0].style.display = "none";
    $("#DivShowreport")[0].style.display = "block";

    $("#Reportfor").append("All Teams Task report");
    var totaltimespent = 0;
    $.ajax({
        type: "GET",
        contentType: "application/json; charset=utf-8",
        url: "/DataTracker.svc/GetTeamsWTT?FDate=" + sd + "&Ldate=" + ld + "",
        dataType: "json",
        success: function (data) {
            if (data.d != null) {
                // ;
                for (var i = 0; i < data.d.length; i++) {
                    totaltimespent = totaltimespent + data.d[i].TotalWTime;
                    MyArray.push({ "source": "" + data.d[i].TeamName + "", "percentage": "" + data.d[i].Wtime + "" });
                    MyArrayAvg.push({ "source": "" + data.d[i].TeamName + "", "percentage": "" + Number(data.d[i].AvgTotalWTime.toFixed(2)) + "" });
                    
                }
            }
            // $("#ttSpent").append(Number((totaltimespent).toFixed(2)) + " hours");
            $("#ttSpent").append(secondsToHms(totaltimespent));
            createChart(MyArray);
            createChartavg(MyArrayAvg);
        },
        error: function (result) {

        }
    });

}
function createChartavg(datass) {

    $("#Havg")[0].style.display = "block";
    $("#chartavg")[0].style.display = "block";
    $("#chartavg").kendoChart({
        //        title: {
        //            text: "Tasks report"
        //        },
        legend: {
            position: "top",
            padding: 1,
            margin: 1
        },
        dataSource: {
            data: datass
        },
        seriesDefaults: {
            labels: {
                visible: true,
                color: "#0878c0",
                template: "${ category } - ${ value } hr"
                //template: "#= kendo.format('{0:P}', percentage)#"
            },
            visible: true
        },
        series: [{
            type: "pie",
            field: "percentage",
            categoryField: "source",
            explodeField: "explode"
        }],
        seriesColors: [
                  "#4682B4", "#004990", "#da7633", "#7B68EE", "#008B8B", "#8a7967", "#676200", "#8b0f04", "#ead766", "78496a", "green", "#D2691E", "#FF69B4", "#808000", "#008080", "#6A5ACD"
             ],
        tooltip: {
            visible: true,
            color: "white",
            border: {
                color: "black",
                dashType: "dashDot",
                width: 1
            },
            template: "#= category # - #= kendo.format('{0:P}', percentage)#"
            // template: "${ category } - ${ value }%"
        }
    });
}

function createChart(datass) {

    $("#chart").kendoChart({
        //        title: {
        //            text: "Tasks report"
        //        },
        legend: {
            position: "top",
            padding: 1,
            margin: 1
        },
        dataSource: {
            data: datass
        },
        seriesDefaults: {
            labels: {
                visible: true,
                color: "#0878c0",
                template: "${ category } - ${ value } hr"
                //template: "#= kendo.format('{0:P}', percentage)#"
            },
            visible: true
        },
        series: [{
            type: "pie",
            field: "percentage",
            categoryField: "source",
            explodeField: "explode"
        }],
        seriesColors: [
                  "#4682B4", "#004990", "#da7633", "#7B68EE", "#008B8B", "#8a7967", "#676200", "#8b0f04", "#ead766", "78496a", "green", "#D2691E", "#FF69B4", "#808000", "#008080", "#6A5ACD"
             ],
        tooltip: {
            visible: true,
               color: "white",
            border: {
                color: "black",
                dashType: "dashDot",
                width: 1
            },
            template: "#= category # - #= kendo.format('{0:P}', percentage)#"
            
        }
    });
}

function BackToMainPage() {
    window.location = "/EmployeeTimesheetReports.aspx";
}


function GetteamwisedataD(std, ltd, forteam) {
    //  ;
    var MyArrayD = new Array();
    var totaltimespent = 0;
    var aui = new Array();
    // $("#AllAspControls")[0].style.display = "none";
    //  $("#AllControls")[0].style.display = "none";
    $("#DivShowreport")[0].style.display = "block";

    $.ajax({
        type: "GET",
        contentType: "application/json; charset=utf-8",
        url: "/DataTracker.svc/GetSTTime?FDate='" + std + "'&Ldate='" + ltd + "'&teamiidd=" + forteam + "",
        dataType: "json",
        success: function (data) {
            if (data.d != null) {
                for (var i = 0; i < data.d.length; i++) {
                    totaltimespent = totaltimespent + data.d[i].TotalWTime;
                    if (i == 1) {
                        $("#Reportfor").append(data.d[i].TeamName + " Team Task report");
                        MyArrayD.push({ "source": "" + data.d[i].Uname + "", "percentage": "" + data.d[i].Wtime + "" });
                    }
                    else
                        MyArrayD.push({ "source": "" + data.d[i].Uname + "", "percentage": "" + data.d[i].Wtime + "" });
                    aui.push(data.d[i].Uid);



                }
            }
            // $("#ttSpent").append(Number((totaltimespent).toFixed(2)) + " hours");secondsToHms
            $("#ttSpent").append(secondsToHms(totaltimespent));
            createChartD(MyArrayD);
            for (var i = 0; i < aui.length; i++) {
                GetdatainTable(std, ltd, aui[i]);
            }
        },
        error: function (result) {

        }
    });


}

function createChartD(datass) {

    $("#chart").kendoChart({
        //        title: {
        //            text: "Tasks report"
        //        },
        legend: {
            position: "top",
            padding: 1,
            margin: 1
        },
        dataSource: {
            data: datass
        },
        seriesDefaults: {
            labels: {
                visible: true,
                color: "#0878c0",
                template: "${ category } - ${ value } hr"
                //template: "#= kendo.format('{0:P}', percentage)#"
            },
            visible: true
        },
        series: [{
            type: "pie",
            field: "percentage",
            categoryField: "source",
            explodeField: "explode"
        }],
        seriesColors: [
                  "#004990", "#da7633", "#8a7967", "#676200", "#8b0f04", "#ead766", "78496a", "green"
             ],
        tooltip: {
            visible: true,
            color: "white",
            border: {
                color: "black",
                dashType: "dashDot",
                width: 1
            },
            template: "#= category # - #= kendo.format('{0:P}', percentage)#"
            // template: "${ category } - ${ value }%"
        }
    });
}
function GetSingleuserdata(std, ltd) {
    var MyArraySi = new Array();


    // $("#AllAspControls")[0].style.display = "none";
    // $("#AllControls")[0].style.display = "none";
    var totaltimespent = 0;
    $("#DivShowreport")[0].style.display = "block";
    $.ajax({
        type: "GET",
        contentType: "application/json; charset=utf-8",
        url: "/DataTracker.svc/GetIndividualsT?FDate='" + std + "'&Ldate='" + ltd + "'&userid=" + current_userId + "",
        dataType: "json",
        success: function (data) {
            if (data.d != null) {
                for (var i = 0; i < data.d.length; i++) {
                    totaltimespent = totaltimespent + data.d[i].TotalWTime;
                    if (i == 0) {
                        $("#Reportfor").append(data.d[i].Uname + "'s Task report");
                        MyArraySi.push({ "source": "" + data.d[i].TaskTitle + "", "percentage": "" + data.d[i].WWtime + "" });
                    }
                    else
                        MyArraySi.push({ "source": "" + data.d[i].TaskTitle + "", "percentage": "" + data.d[i].WWtime + "" });

                }
            }
            createChartS(MyArraySi);
            //$("#ttSpent").append(Number((totaltimespent).toFixed(2)) + " hours");
            $("#ttSpent").append(secondsToHms(totaltimespent));
            GetdatainTable(std, ltd, current_userId);
        },
        error: function (result) {

        }
    });
}

function createChartS(datass) {

    $("#chart").kendoChart({
        //        title: {
        //            text: "Tasks report"
        //        },
        legend: {
            position: "top",
            padding: 1,
            margin: 1
        },
        dataSource: {
            data: datass
        },
        seriesDefaults: {
            labels: {
                visible: true,
                color: "#0878c0",
                template: "${ category } - ${ value } hr"
                //template: "#= kendo.format('{0:P}', percentage)#"
            },
            visible: true
        },
        series: [{
            type: "pie",
            field: "percentage",
            categoryField: "source",
            explodeField: "explode"
        }],
        seriesColors: [
                  "#4682B4", "#004990", "#da7633", "#7B68EE", "#008B8B", "#8a7967", "#676200", "#8b0f04", "#ead766", "78496a", "green", "#D2691E", "#FF69B4", "#808000", "#008080", "#6A5ACD"
             ],
        tooltip: {
            visible: true,
            color: "white",
            border: {
                color: "black",
                dashType: "dashDot",
                width: 1
            },
            template: "#= category # - #= kendo.format('{0:P}', percentage)#"
            // template: "${ category } - ${ value }%"
        }
    });
}


function GetdatainTable(std, ltd, cu) {
    //alert(cu);
    var idt = "Dtable" + "" + cu;
    var idn = "PName" + "" + cu;
    var sdval = "derr" + "" + idt;
    var suval = "uerr" + "" + idt;
    $.ajax({
        type: "GET",
        contentType: "application/json; charset=utf-8",
        url: "/DataTracker.svc/GetIndividualsT?FDate='" + std + "'&Ldate='" + ltd + "'&userid=" + cu + "",
        dataType: "json",
        success: function (data) {

            // $("#AllTables").append(" <h4 id='PName'>Nitin Tyagi</h4>");#b8e4ff
            

            $("#AllTables").append("<h4 id='" + idn + "' style='background: #f3f3f3 none repeat scroll 0 0; color: #0878c0' ><span id='" + sdval + "' style='float:left;cursor: pointer; height: 20px;' onclick='javascript:ShowTaskTable(\"" + idt + "\");'><img src='/report/down.gif' style='padding-top: 8px;height: 13px;' /></span><span id='" + suval + "' style='float:left;cursor: pointer; display:none;height: 20px;' onclick='javascript:ShowTaskTable(\"" + idt + "\");'><img src='/report/up.gif' style='padding-top: 8px;height: 13px;' /></span></h4><table id='" + idt + "' class='timesheet_tbl' style='display:none;'><thead><tr><th>Board Name</th><th>Card Name</th><th>Date</th><th>Time</th></tr></thead></table>");

            if (data.d != null) {
                for (var i = 0; i < data.d.length; i++) {
                    if (i == 0) {
                        $("#" + "" + idn).append(data.d[i].Uname+"'s Tasks List");
                        $("#" + "" + idt).append("<tr><td class='first_col'>" + data.d[i].BoardName + "</td><td>" + data.d[i].TaskTitle + "</td><td>" + data.d[i].TDate + "</td><td>" + secondsToHms(data.d[i].Wtime) + "</td></tr>");
                    }
                    else
                        $("#" + "" + idt).append("<tr><td class='first_col'>" + data.d[i].BoardName + "</td><td>" + data.d[i].TaskTitle + "</td><td>" + data.d[i].TDate + "</td><td>" + secondsToHms(data.d[i].Wtime) + "</td></tr>");
                }
            }

        },
        error: function (result) {

        }
    });
}

function getformateddate(datee) {
    var d = new Date(datee);
    var dayofweek = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'][d.getDay()],
date = d.getDate(),
month = ['January', 'February', 'March', 'April', 'May', 'June',
'July', 'August', 'September', 'October', 'November', 'December'][d.getMonth()],
year = d.getFullYear();

    return (["<strong>" + date + "</strong>, ", month, year].join(' '));
}

function getcfy() {
    var d = new Date();
    year = d.getFullYear();
    return year;
}

function ShowTaskTable(idd) {
    if ($("#" + "" + idd).css('display') == 'none') {
        
        $(".timesheet_tbl").css("display", "none");
        $("#" + "" + idd)[0].style.display = "";
        $("#" + "derr" + idd)[0].style.display = "none";
        $("#" + "uerr" + idd)[0].style.display = "block";
        //$(window).scrollTop() + $(window).height();
    }
    else {
        $("#" + "" + idd)[0].style.display = "none";
        $("#" + "uerr" + idd)[0].style.display = "none";
        $("#" + "derr" + idd)[0].style.display = "block";
        //        $('html, body').scrollTop($(document).height());
       // $(document).height() - $(window).height() - $(window).scrollTop();
        
    }

}

function secondsToHms(d) {
    d = Number(d);
    var h = Math.floor(d / 3600);
    var m = Math.floor(d % 3600 / 60);
    var s = Math.floor(d % 3600 % 60);
     return ((h > 0 ? h + "hr " + (m < 10 ? "0" : "") : "") + m + "min " + (s < 10 ? "0" : "") + s+"sec");
   // return ((h > 0 ? h + "hr " + (m < 10 ? "0" : "") : "") + m + "min ");
}