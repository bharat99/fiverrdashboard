﻿
//ankur saxena
//To show Team on load of master page
//var for Team #region startMyTeam

function show_team() {   
    document.getElementById("MyTeam").innerHTML = "";
    document.getElementById("ddl_Team").innerHTML = "";
    $myList = $('#MyTeam');
    if ($myList.children().length === 0) {
        document.getElementById("MyTeam").innerHTML = "";
        var s = 's';
        $.ajax({
            type: "Post",
            url: "/DataTracker.svc/CreateSelectAddEditDeleteTeam",
            data: "{\"OperationStatus\": \"" + s + "\"}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {               
                $('#ddl_Team').empty();
                document.getElementById("MyTeam").innerHTML = "";
                $('#ddl_Team').append("<option value='0'>None</option>");
                $.each(data.d, function (index, item) {
                    $('#MyTeam').append("<li id='list" + item.Id + "' style='margin:0; padding:0;'><a style='float:left;' href='javascript:team_landing(" + item.Id + ");' title='" + item.TeamName + "'>" + item.TeamName + "</a><a class='testclass' href='javascript:removeteam(" + item.Id + ");'>x</a></li>");
                    $('#ddl_Team').append("<option value=" + item.Id + ">" + item.TeamName + "</option>"); // Added by Ravi to bind dropdown on create Board PopUp.
                    $("#ddlteamtoboard1").append($("<option></option>").val(item.Id).html(item.TeamName)); // ddl for  changeteam of board added by shobhana
                    $("#ddlteamtoboard2").append($("<option></option>").val(item.Id).html(item.TeamName));
                    $("#ddlteamtoboard3").append($("<option></option>").val(item.Id).html(item.TeamName));
                });
            }
        });
    }
}


function show_temolates() {

    document.getElementById("templates").innerHTML = "";

    $.ajax({
        type: "GET",
        url: "/DataTracker.svc/BindTemplates",

        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {

            $('#templates').append("<option value='0'>None</option>");
            $.each(data.d, function (index, item) {
                $('#templates').append("<option value=" + item.templateId + ">" + item.templateName + "</option>"); // Added by Ravi to bind dropdown on create Board PopUp.
            });
        }
    });

}





// Ravi Prasad 
function OnSelectTeam() {
    // This function is to change the Board Access Modifier to Private By Default if any of the team is selected by user.
    //  ;
    var Team = $("#ddl_Team").val();
    if (Team != 0) {
        $("#change_line").html("This board will be <span id='BoardAccessModifier'>Team Visible</span>.");
        $("#board_cont_team").append("&#10004;");
        $("#board_cont_public").text("Public");
        $("#board_cont_public").append("&#10004;");
        $("#board_cont_private").text("Private");
    }
    else {
        $("#change_line").html("This Board will be <span id='BoardAccessModifier'>Public</span>.");
        $("#board_cont_public").text("Public");
        $("#board_cont_public").append("&#10004;");
        $("#board_cont_private").text("Private");
        $("#board_cont_team").text("Team");

    }
}

// Ameeq
function OnSelectBoardMerge() {
    // This function is to merge two boards as selected by user.
    var Select_BoardId = $("#ddl_FromBoard").val();
    if (Select_BoardId != 0) {
        $.ajax({
            type: "POST",
            url: "/DataTracker.svc/BindSecondBoardforMerge",
            data: "{\"SelectedBoardId\": \"" + Select_BoardId + "\"}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                var response = JSON.parse(data.d);
                $("#ddl_ToBoard").empty();
                $.each(response, function (index, item) {
                    $("#ddl_ToBoard").append($("<option></option>").val(item.BoardId).html(item.BoardName));
                });
            }
        });
    }
    else
    {
        alert("Please select the Board for Merging");
    }
}

// Ravi Prasad
function ddl_Team_Bind(teamid) {
    ;
    var s = 's';
    $("#dropdownteamid").empty();


    $.ajax({
        type: "POST",
        url: "/DataTracker.svc/CreateSelectAddEditDeleteTeam",
        data: "{\"OperationStatus\": \"" + s + "\"}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            $.each(data.d, function (index, item) {
                //$("#dropdownteamid").append($("<option></option>").val(item.Id).html(decodeURI(item.TeamName)));
                $("#dropdownteamid").append($("<option></option>").val(item.Id).html(item.TeamName));
            });
            $("#dropdownteamid").val(teamid);
            var selectedText = $("#dropdownteamid").find("option:selected").text();
            //$("#txtteam").text(selectedText);
            $("#txtTeamName1").text(selectedText);
            showmembersonloadaccordingtoteam(teamid);
        }
    });
}

function removeteam(teamid) {
    var p = confirm("Are you sure you want to remove this item ?");
    //var selecteam = document.getElementById("dropdownteamid").length;
    var selecteam = $("#dropdownteamid").length;
    var s = "d";
    if (p) {
        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: "/DataTracker.svc/AddEditDeleteBoardTeam",
            data: "{\"Id\": \"" + teamid + "\",\"OperationStatus\": \"" + s + "\"}",
            dataType: "json",
            success: function (data) {
                if (data.d != 0) {
                    $('#MyTeam').empty();
                    show_team();
                    if (selecteam >= 1) {
                        $("#dropdownteamid option[value='" + teamid + "']").remove();
                        $("#dropdownteamid").val(teamid).trigger('change');
                    }
                    else {
                        //$("#dropdownteamid").html('<option>none</option>');
                        $("#dropdownteamid").val(teamid).trigger('change');
                    }
                }
                else {
                    alert("You can not remove this team because it already assigned to any board");
                }
            },
            error: function (error) {
                alert("Some error!");
            }
        })
    }
}

//for landing when select Team from Team Menue
function team_landing(Teamid) {
    window.location.href = "AssignedTeamToBoard.aspx?Teamid=" + Teamid;
}

//To get Teamid from q string onload 
function get_teamid_from_qstring_onload() {
    ;
    var url = window.location.href;
    var teamid = url.substring(url.lastIndexOf('=') + 1);
    AllBoardsInTeam(teamid);
    teamid = teamid.replace("#", "");
    return teamid;
}

//To change event of team dropdown 
function changeforfunction(selectedtext, selectedvalue) {
    $("#showuser").empty();
    $('#hiddenteamid').val('');
    //$("#txtteam").text(selectedtext);
    $('#txtTeamName1').text(selectedtext);
    $('#hiddenteamid').val(selectedvalue);
    var teamid = selectedvalue;
    showmembersonloadaccordingtoteam(teamid);
}

function showuserlistforsearch(sender, arg) {
    ;
    $("#appendmembers").empty();
    var userlist = $("#txteditmembers").val();
    $(".org-members-page-layout-list").empty();
    if (userlist.length > 0) {
        $.ajax({
            type: "GET",
            url: "/DataTracker.svc/GetmembernameonTeam?membersname=" + userlist + "",
            dataType: "json",
            async: false,
            success: function (data) {
                if (data.d.length > 0) {
                    $.each(data.d, function (index, item) {
                        var str = item.Email;
                        var firstletter = str.charAt(0);
                        var username = item.username;
                        $("#appendmembers").append("<span onclick='assignusertoboardonselection(" + item.userid + ");'> <a href='#'><h2>" + firstletter + "</h2><div class='member_id'><p title='" + item.Email + "'>" + item.Email + "</p></div><div class='clr'></div></a></span>");
                        $("#appendmembers").show();
                    });
                }
            },
            error: function (error) { alert('Error has occurred!'); alert(JSON.stringify(error)) }
        });
    }
}

//assign user to board on selection on behalf of team
function assignusertoboardonselection(userid) {
    var teamid = $('#hiddenteamid').val();
    var i = "i";
    $.ajax({
        type: "POST",
        url: "/DataTracker.svc/AddEditDeleteassignedmember",
        data: "{\"OperationStatus\": \"" + i + "\",\"userid\": \"" + userid + "\",\"teamid\": \"" + teamid + "\"}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            if (data.d != 0) {
                $("#showuser").empty();
                showmembersonloadaccordingtoteam(data.d);
            } else {
                alert("This User Is Already Assigned To This Team");
            }
        }
    });
}
//show members on load according to team
function showmembersonloadaccordingtoteam(teamid) {
    ;
    $("#showuser").empty();
    var s = "s";
    var txtsearch = "";
    var userid = 0;
    $.ajax({
        type: "POST",
        url: "/DataTracker.svc/ShowAssignUsersToTeamlist",
        data: "{\"OperationStatus\": \"" + s + "\",\"userid\": \"" + userid + "\",\"teamid\": \"" + teamid + "\",\"txtsearch\": \"" + txtsearch + "\"}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            if (data.d != 0) {
                $.each(data.d, function (index, item) {
                    var str = item.Email;
                    var firstletter = str.charAt(0).toUpperCase();
                    // $("#showuser").append("<tr><td>" + item.UserName + "</td></tr><tr><td>" + item.Email + "</td><td><span class='set_remove' style='cursor:pointer'  onclick='removememberfromteam(" + item.id + ","+teamid+")'>Remove</span></td></tr>");
                    $("#showuser").append(" <div id=" + index + " class='member-list-item-detail' data-reactid='.8.0.1.0.0'><div class='member member-no-menu' data-reactid='.8.0.1.0.0.0'><span title='" + item.UserName + " (" + item.Email + ")' class='member-initials' data-reactid='.8.0.1.0.0.0.0'>" + firstletter + "</span><span title='This member is an admin of this team.' class='member-type admin' data-reactid='.8.0.1.0.0.0.1'></span><span title='This member has Trello Gold.' class='member-gold-badge' data-reactid='.8.0.1.0.0.0.2'></span></div><div class='details' data-reactid='.8.0.1.0.0.1'><p class='name-line' data-reactid='.8.0.1.0.0.1.0'><span class='full-name' data-reactid='.8.0.1.0.0.1.0.0'>" + item.UserName + "</span></p><p class='u-bottom quiet' data-reactid='.8.0.1.0.0.1.1'><span class='quiet u-inline-block' data-reactid='.8.0.1.0.0.1.1.0'><span data-reactid='.8.0.1.0.0.1.1.0.0'>" + item.Email + "</span></span></p></div><div class='options' data-reactid='.8.0.1.0.0.2'><a data-reactid='.8.0.1.0.0.2.0' href='#' class='option quiet-button'><span id=" + index + " class='FromTeamRemoveMember'  onclick='removememberfromteam(" + item.id + "," + teamid + "," + index + ")' data-reactid='.8.0.1.0.0.2.0.0'>Remove</span></a></div><div class='clr'></div></div>");
                });
            } else {
                $("#showuser").append("No Member Assign To This  Team");
            }
            $('#searchmember').val("");
        }
    });
}

//show remove member from team
function removememberfromteam(id, teamid, spanid) {
    var p = confirm("Are you sure you want to delete this item ?");
    var x = "#" + spanid;
    var d = "d";
    var userid = 0;
    if (p) {
        $.ajax({
            type: "POST",
            url: "/DataTracker.svc/ShowAssignUsersToTeamlist",
            data: "{\"OperationStatus\": \"" + d + "\",\"userid\": \"" + userid + "\",\"teamid\": \"" + id + "\"}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                $(x).remove();
            }
        });
    }
}

function CreateTeamforBoard() {
    document.getElementById('Teamspanid').innerText = '';
    //var teamname = encodeURI($.trim($("#txtCreateTeam").val()).replace(/'/g, "squote").replace("\n", ""));
    //var teamdescription = encodeURI($.trim($("#teamdescription").val()).replace(/'/g, "squote").replace("\n", ""));
    var teamname = $.trim($("#txtCreateTeam").val()).replace(/'/g, "").replace("\n", "").replace(/"/g, "").replace(/\\/g, "").replace(/\//g, "");
    var teamdescription = $.trim($("#teamdescription").val()).replace(/'/g, "").replace("\n", "").replace(/"/g, "").replace(/\\/g, "").replace(/\//g, "");
    var TeamType = "p";
    var OperationStatus = "i";
    if (teamname.length == 0) {
        document.getElementById('txtCreateTeam').style.bordercolor = 'red';
        document.getElementById('Teamspanid').innerText = "* Please enter team name!";
        document.getElementById('Teamspanid').style.color = 'red';
    }
    else {
        document.getElementById('Teamspanid').style.display = 'none';
        $.ajax({
            type: "POST",
            url: "/DataTracker.svc/AddEditDeleteBoardTeam",
            data: "{\"TeamName\": \"" + teamname + "\",\"TeamType\": \"" + TeamType + "\",\"TeamDescription\": \"" + teamdescription + "\",\"CreatedBy\": \"" + cuid + "\",\"OperationStatus\": \"" + OperationStatus + "\"}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                if (data.d == 0) {
                    document.getElementById('Teamspanid').style.display = 'Block';
                    document.getElementById('Teamspanid').innerText = "* Team Name Already Exists!";
                    document.getElementById('Teamspanid').style.color = 'red';
                }
                else {
                    UpdateActivityListItem(teamname, 9);
                    $("#txtCreateTeam").val("");
                    $("#teamdescription").val("");
                    document.getElementById('Teamspanid').style.display = 'Block';
                    document.getElementById('Teamspanid').innerText = "Team Created Successfully";
                    document.getElementById('Teamspanid').style.color = 'green';
                    team_landing(data.d);
                    show_team();
                }
            },
            error: function (error) {
                alert("Some error!");
            }
        });
    }
}

function MergeBoard() {
    
    var FromBoard = $('#ddl_FromBoard option:selected').val();
    var ToBoard = $('#ddl_ToBoard option:selected').val();
    var MergeType = 1;
    if (FromBoard == 0 || ToBoard == 0) {
        document.getElementById('MergeBoardspanid').innerText = "* Please select Board names to Merge!";
        document.getElementById('MergeBoardspanid').style.color = 'red';
    }
    else {
        $.ajax({
            type: "POST",
            url: "/DataTracker.svc/MergeBoardsData",
            data: "{\"FromBoardName\": \"" + FromBoard + "\",\"ToBoardName\": \"" + ToBoard + "\",\"MergeBoardType\": \"" + MergeType + "\"}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                var response = JSON.parse(data.d);
                $.each(response, function (index, item) {
                    
                    if (item.DuplicateCount != 0) {
                        document.getElementById('MergeBoardspanid').style.display = 'Block';
                        document.getElementById('MergeBoardspanid').innerText = "* Sprints Name Same!.Please Change the Sprints Name of Selected Board";
                        document.getElementById('MergeBoardspanid').style.color = 'red';
                    }
                    else {
                        $("#ddl_FromBoard").empty();
                        $("#ddl_ToBoard").empty();
                        document.getElementById('MergeBoardspanid').style.display = 'Block';
                        document.getElementById('MergeBoardspanid').innerText = "Board Successfully Merge";
                        document.getElementById('MergeBoardspanid').style.color = 'green';
                        //$('#ex3').delay(5000).hide(0);
                        //document.getElementById('MergeBoardspanid').style.display = 'none';
                    }
                });
            },
            error: function (error) {
                alert("Some Technical error!");
            }
        });
    }
}

function UpdateTeamAndDescription() {
    var team_id = $('#dropdownteamid option:selected').val();
    var teamname = $.trim($("#txtUpdateTeam").val()).replace(/'/g, "").replace("\n", "").replace(/"/g, "").replace(/\\/g, "").replace(/\//g, "");//today
    var teamdescription = $.trim($("#txtUpdateDescription").val()).replace(/'/g, "").replace("\n", "").replace(/"/g, "").replace(/\\/g, "").replace(/\//g, "");//today
    var TeamType = "p";
    var OperationStatus = "u";
    if (teamname.length == 0) {
        document.getElementById('txtUpdateTeam').style.bordercolor = 'red';
        document.getElementById('UpdateTeamSpanId').innerText = "* Please enter team name!";
        document.getElementById('UpdateTeamSpanId').style.color = 'red';
        $('#txtUpdateTeam').focus();
    }
    else {
        document.getElementById('UpdateTeamSpanId').style.display = 'none';
        $.ajax({
            type: "POST",
            url: "/DataTracker.svc/AddEditDeleteBoardTeam",
            data: "{\"TeamName\": \"" + teamname + "\",\"TeamType\": \"" + TeamType + "\",\"TeamDescription\": \"" + teamdescription + "\",\"CreatedBy\": \"" + cuid + "\",\"OperationStatus\": \"" + OperationStatus + "\",\"Id\": \"" + team_id + "\"}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                if (data.d == 0) {
                    document.getElementById('UpdateTeamSpanId').style.display = 'Block';
                    document.getElementById('UpdateTeamSpanId').innerText = "* Team name already exists!";
                    document.getElementById('UpdateTeamSpanId').style.color = 'red';
                    $('#txtUpdateTeam').focus();
                }
                else {
                    document.getElementById('UpdateTeamSpanId').style.display = 'Block';
                    document.getElementById('UpdateTeamSpanId').innerText = "Record updated successfully";
                    document.getElementById('UpdateTeamSpanId').style.color = 'green';
                    show_team();
                    var teamname = $("#txtUpdateTeam").val();
                    $('#txtTeamName1').text(teamname);
                    $('#dropdownteamid option:selected').text(teamname);
                }
            },
            error: function (error) {
                alert("Some error!");
            }
        });
    }
}

// function for change team to board Shobhana
function change_Team_Board(id) {

    if (id == 1) {
        var Changedteamid = $("#ddlteamtoboard").find("option:selected").val();
        var boardid = $("#boardID").val();
        var changeteamname = $("#ddlteamtoboard").find("option:selected").text();
        var oldboardname = $('#BoardName').text();
        var changeteamnametooldboardname = oldboardname + " has changed it's team to " + changeteamname + ".";
        $("#teamID").val(Changedteamid);

        $.ajax({
            type: "POST",
            url: "/DataTracker.svc/ChangeTeamToBoard",
            data: "{\"changeteamid\": \"" + Changedteamid + "\",\"boardid\": \"" + boardid + "\"}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {

                if (Changedteamid == 0) {
                    $("#webuiPopover1").hide();
                    $("#TeamName").hide();
                    $("#TeamName1").hide();
                    $("#teamlabel").show();
                    bindddlforchangeteam(Changedteamid);
                    UpdateActivityListItem(changeteamnametooldboardname, 24);
                }
                else {
                    $("#TeamName").html("&#x2600;" + $("#ddlteamtoboard").find("option:selected").text());
                    $("#TeamName1").show();
                    $("#TeamName1").html("&#x2600;" + $("#ddlteamtoboard").find("option:selected").text());
                    $("#teamnameforheader").text($("#ddlteamtoboard").find("option:selected").text());
                    $("#teamnameforheader1").text($("#ddlteamtoboard").find("option:selected").text());
                    bindddlforchangeteam(Changedteamid);
                    UpdateActivityListItem(changeteamnametooldboardname, 24);
                    SendEmailToChangeTeam(Changedteamid, changeteamname);
                    GetOwnerName();
                }

                $('#change1, #change3').hide();
                $('#webuiPopover1').hide();
            }
        });
    }
    else if (id == 2) {

        var Changedteamid = $("#ddlteamtoboard1").find("option:selected").val();
        var boardid = $("#boardID").val();
        var changeteamname = $("#ddlteamtoboard1").find("option:selected").text();
        var oldboardname = $('#BoardName').text();
        var changeteamnametooldboardname = oldboardname + " has changed it's team to " + changeteamname + ".";
        $("#teamID").val(Changedteamid);

        $.ajax({
            type: "POST",
            url: "/DataTracker.svc/ChangeTeamToBoard",
            data: "{\"changeteamid\": \"" + Changedteamid + "\",\"boardid\": \"" + boardid + "\"}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {

                if (Changedteamid == 0) {
                    $("#webuiPopover2").hide();
                    $("#TeamName").hide();
                    $("#TeamName1").hide();
                    $("#teamlabel").show();
                    bindddlforchangeteam(Changedteamid)
                    UpdateActivityListItem(changeteamnametooldboardname, 24);
                }
                else {
                    $("#TeamName").show();
                    $("#TeamName").html("&#x2600;" + $("#ddlteamtoboard1").find("option:selected").text());
                    $("#TeamName1").show();
                    $("#TeamName1").html("&#x2600;" + $("#ddlteamtoboard1").find("option:selected").text());
                    $("#teamnameforheader").text($("#ddlteamtoboard1").find("option:selected").text());
                    $("#teamnameforheader1").text($("#ddlteamtoboard1").find("option:selected").text());
                    bindddlforchangeteam(Changedteamid);
                    UpdateActivityListItem(changeteamnametooldboardname, 24);
                    SendEmailToChangeTeam(Changedteamid, changeteamname);
                    GetOwnerName();
                }
                $('#change1, #change3').hide();
                $('#webuiPopover1').hide();
            }
        });
    }
    else if (id == 3) {

        var Changedteamid = $("#ddlteamtoboard2").find("option:selected").val();
        var boardid = $("#boardID").val();
        var changeteamname = $("#ddlteamtoboard2").find("option:selected").text();
        var oldboardname = $('#BoardName').text();
        var changeteamnametooldboardname = oldboardname + " has changed it's team to " + changeteamname + ".";
        $("#teamID").val(Changedteamid);
        $.ajax({
            type: "POST",
            url: "/DataTracker.svc/ChangeTeamToBoard",
            data: "{\"changeteamid\": \"" + Changedteamid + "\",\"boardid\": \"" + boardid + "\"}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {

                if (Changedteamid == 0) {
                    $("#webuiPopover3").hide();
                    $("#TeamName").hide();
                    bindddlforchangeteam(Changedteamid);
                    UpdateActivityListItem(changeteamnametooldboardname, 24);
                }
                else {

                    $("#TeamName").show();
                    $("#TeamName").html("&#x2600;" + $("#ddlteamtoboard2").find("option:selected").text());
                    $("#TeamName1").show();
                    $("#TeamName1").html("&#x2600;" + $("#ddlteamtoboard2").find("option:selected").text());
                    $("#teamnameforheader1").text($("#ddlteamtoboard2").find("option:selected").text());
                    $("#teamnameforheader").text($("#ddlteamtoboard2").find("option:selected").text());
                    $("#ddlteamtoboard2").val('0');
                    $("#teamlabel").hide();
                    $("#change2").hide();
                    bindddlforchangeteam(Changedteamid);
                    UpdateActivityListItem(changeteamnametooldboardname, 24);
                    SendEmailToChangeTeam(Changedteamid, changeteamname);
                    GetOwnerName();
                }
                $('#change1, #change3').hide();
                $('#webuiPopover1').hide();
            }
        });
    }
    else {

        var Changedteamid = $("#ddlteamtoboard3").find("option:selected").val();
        var boardid = $("#boardID").val();
        var changeteamname = $("#ddlteamtoboard3").find("option:selected").text();
        var oldboardname = $('#BoardName').text();
        var changeteamnametooldboardname = oldboardname + " has changed it's team to " + changeteamname + ".";
        $("#teamID").val(Changedteamid);
        $.ajax({
            type: "POST",
            url: "/DataTracker.svc/ChangeTeamToBoard",
            data: "{\"changeteamid\": \"" + Changedteamid + "\",\"boardid\": \"" + boardid + "\"}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {

                if (Changedteamid == 0) {
                    $("#webuiPopover4").hide();
                    $("#TeamName").hide();
                    $("#TeamName1").hide();
                    $("#change3").hide();
                    $("#teamlabel").show();
                    bindddlforchangeteam(Changedteamid);
                    UpdateActivityListItem(changeteamnametooldboardname, 24);
                }
                else {

                    $("#TeamName").show();
                    $("#TeamName").html("&#x2600;" + $("#ddlteamtoboard3").find("option:selected").text());
                    $("#TeamName1").html("&#x2600;" + $("#ddlteamtoboard3").find("option:selected").text());
                    $("#teamnameforheader1").text($("#ddlteamtoboard3").find("option:selected").text());
                    $("#teamnameforheader").text($("#ddlteamtoboard3").find("option:selected").text());
                    bindddlforchangeteam(Changedteamid);
                    UpdateActivityListItem(changeteamnametooldboardname, 24);
                    SendEmailToChangeTeam(Changedteamid, changeteamname);
                    GetOwnerName();

                }
                $('#change1, #change3').hide();
                $('#webuiPopover1').hide();
            }
        });
    }
}

//get the valuse Of Bord id from Querystring
function getUrlVars() {
    var a = window.location.href;
    if (a.indexOf("?") != -1) {
        var id = a.substring(a.indexOf("?") + 1);
        return id;
    }
}

function bindddlforchangeteam(teamid) {
    teamIdForOwner = teamid;
    $("#ddlteamtoboard").empty();
    $("#ddlteamtoboard1").empty();

    $("#ddlteamtoboard2").empty();
    $("#ddlteamtoboard3").empty();
    var s = 's';
    $.ajax({
        type: "Post",
        url: "/DataTracker.svc/bindteamforchange",
        data: "{\"OperationStatus\": \"" + s + "\"}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {

            $("#ddlteamtoboard").empty();
            $("#ddlteamtoboard1").empty();
            $("#ddlteamtoboard2").empty();
            $("#ddlteamtoboard3").empty();
            $('#ddlteamtoboard').append("<option value='0'>None</option>");
            $('#ddlteamtoboard1').append("<option value='0'>None</option>");
            $('#ddlteamtoboard2').append("<option value='0'>None</option>");
            $('#ddlteamtoboard3').append("<option value='0'>None</option>");

            $.each(data.d, function (index, item) {
                $("#ddlteamtoboard").append($("<option></option>").val(item.Id).html(item.TeamName));
                $("#ddlteamtoboard1").append($("<option></option>").val(item.Id).html(item.TeamName));
                $("#ddlteamtoboard2").append($("<option></option>").val(item.Id).html(item.TeamName));
                $("#ddlteamtoboard3").append($("<option></option>").val(item.Id).html(item.TeamName));
            });
            $('#ddlteamtoboard').val(teamid);
            $('#ddlteamtoboard1').val(teamid);
            $('#ddlteamtoboard2').val(teamid);
            $('#ddlteamtoboard3').val(teamid);
        }
    });
}

function searchmember_from_grid(sender, arg) {
    ;
    $("#showuser").empty();
    var teamid = $('#hiddenteamid').val();
    if ($("#searchmember").val() != "") {
        var membersname = $('#searchmember').val();
        var OperationStatus = "search";
        if (membersname.length > 0) {
            $.ajax({
                type: "POST",
                url: "/DataTracker.svc/ShowAssignUsersToTeamlist",
                data: "{\"txtsearch\": \"" + membersname + "\",\"OperationStatus\": \"" + OperationStatus + "\",\"teamid\": \"" + teamid + "\"}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                async: false,
                success: function (data) {
                    $.each(data.d, function (index, item) {
                        var str = item.Email;
                        var firstletter = str.charAt(0);
                        $("#showuser").append("<div class='member-list-item-detail' ><div class='member member-no-menu' ><span title='" + item.UserName + " (" + item.Email + ")' class='member-initials' >" + firstletter + "</span><span title='This member is an admin of this team.' class='member-type admin' ></span><span title='This member has Trello Gold.' class='member-gold-badge' ></span></div><div class='details' ><p class='name-line' ><span class='full-name' >" + item.UserName + "</span></p><p class='u-bottom quiet' ><span class='quiet u-inline-block' ><span data-reactid='.8.0.1.0.0.1.1.0.0'>" + item.Email + "</span></span></p></div><div class='options' ><a  href='#' class='option quiet-button'><span id=" + index + " class='FromTeamRemoveMember'  onclick='removememberfromteam(" + item.id + "," + teamid + "," + index + ")' >Remove</span></a></div><div class='clr'></div></div>");
                    });
                }
            });
        }
    } else {
        showmembersonloadaccordingtoteam(teamid);
    }
}

function AllBoardsInTeam(Team_ID) {
    $("#TeamsWithBoards").html("");
    ;
    $.ajax({
        type: "POST",
        url: "/DataTracker.svc/AllBoardsInTeam",
        data: "{\"TeamID\": \"" + Team_ID + "\"}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            $.each(data.d, function (index, item) {
                $("#TeamsWithBoards").append("<li><a href='javascript:GetCardAndList(" + item.BoardID + ")'  >" + (item.BoardName) + "</a></li>");
            });
        }
    });
}


$(document).ready(function () {
    //var $loading = $('#loading').hide();
    //$(document).ajaxStart(function () {
    //    $loading.show();

    //});
    //$(document).ajaxStop(function () {
    //    $loading.hide();

    //});
    $("#accordion").accordion();
    (function () {
        var settings = {
            trigger: 'click',
            multi: false,
            closeable: true,
            style: '',
            delay: 300,
            padding: true,
            backdrop: false
        };

        

        function initPopover() {
            $('a.show-pop').webuiPopover('destroy').webuiPopover(settings);

            var tableContent = $('#tableContent').html(),
            tableSettings = {
                content: tableContent,
                width: 500
            };
            $('a.show-pop-table').webuiPopover('destroy').webuiPopover($.extend({}, settings, tableSettings));

            var listContent = $('#listContent').html(),
            listSettings = {
                content: listContent,
                title: '',
                padding: false
            };
            $('a.show-pop-list').webuiPopover('destroy').webuiPopover($.extend({}, settings, listSettings));

            var largeContent = $('#largeContent').html(),
            largeSettings = {
                content: largeContent,
                width: 400,
                height: 350,
                delay: { show: 300, hide: 1000 },
                closeable: true
            };


            $('body').on('click', '.pop-click', function (e) {
                e.preventDefault();
                if (console) {
                    console.log('clicked');
                }
            });


        }

        initPopover();

    })();

    $("#emptxt").mouseover(function () {
        $(".cd-nav-gallery").removeClass("is-hidden");
    });
    $(document).mouseup(function (e) {
        
        var subject = $("#cd-nav-gallerysec");

        if (e.target.id != subject.attr('id') && !subject.has(e.target).length) {
            subject.addClass("is-hidden");
        }
    });
    $("#ActionsMenu, #ReportMenu, #SettingMenu ,#imgInviteUser, #AADMenu ,#WebscrapesMenu, #AdsMenu").mouseover(function () {
        $(".cd-nav-gallery").addClass("is-hidden");
    });

});
