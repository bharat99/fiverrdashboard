﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Mail;
using System.Threading.Tasks;

namespace FiverrIntelligense.Common
{
    public static class EmailHelper
    {
        public static void sendMail(string SenderMailId, string Password,string RecieverMailId,string Subject, string MailContent, bool IsHtml)
        {
			try
			{
                MailMessage mail = new MailMessage();
                SmtpClient SmtpServer = new SmtpClient("smtp.gmail.com");

                mail.From = new MailAddress(SenderMailId);
                mail.To.Add(RecieverMailId);
                mail.Subject = Subject;

                mail.IsBodyHtml = IsHtml;
                mail.Body = MailContent;

                SmtpServer.Port = 587;
                SmtpServer.Credentials = new System.Net.NetworkCredential(SenderMailId, Password);
                SmtpServer.EnableSsl = true;

                SmtpServer.Send(mail);
            }
			catch (Exception ex)
			{
				throw;
			}
        }
    }

    public static class CodeHelper
    {
        public static string GetRandomCode()
        {
            int len = 6;
            // A strong password has Cap_chars, Lower_chars, 
            // numeric value and symbols. So we are using all of 
            // them to generate our password 
            string Capital_chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            string Small_chars = "abcdefghijklmnopqrstuvwxyz";
            string numbers = "0123456789";
            string symbols = "!@#$%^&*_=+-/.?<>)";


            string values = Capital_chars + Small_chars +
                            numbers + symbols;

            // Using random method 
            Random rndm_method = new Random();

            string password = string.Empty;

            for (int i = 0; i < len; i++)
            {
                // Use of charAt() method : to get character value 
                // Use of nextInt() as it is scanning the value as int 
                password +=
                  values[rndm_method.Next(values.Length)];

            }
            return password;
        }
    }
}
