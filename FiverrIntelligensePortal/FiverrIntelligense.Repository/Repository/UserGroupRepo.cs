﻿using FiverrIntelligense.Data.Models;
using FiverrIntelligense.Repository.Common;
using FiverrIntelligense.Repository.IRepository;
using System;
using System.Collections.Generic;
using System.Text;

namespace FiverrIntelligense.Repository.Repository
{
    public class UsersGroupRepo : BaseRepo<TblUserGroupMap>, IUsersGroupRepo
    {
        public UsersGroupRepo(FiverrDevContext devContext):base(devContext)
        {

        }
    }
}
