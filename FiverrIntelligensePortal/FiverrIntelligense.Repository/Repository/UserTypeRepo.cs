﻿using FiverrIntelligense.Data.Models;
using FiverrIntelligense.Repository.Common;
using FiverrIntelligense.Repository.IRepository;
using System;
using System.Collections.Generic;
using System.Text;

namespace FiverrIntelligense.Repository.Repository
{
    public class UserTypeRepo : BaseRepo<TblUserType>, IUserTypeRepo
    {
        public UserTypeRepo(FiverrDevContext devContext):base(devContext)
        {

        }
    }
}
