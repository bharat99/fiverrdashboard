﻿using FiverrIntelligense.Data.Common;
using FiverrIntelligense.Data.Models;
using FiverrIntelligense.Repository.Common;
using FiverrIntelligense.Repository.IRepository;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FiverrIntelligense.Repository.Repository
{
    public class KeywordRepo : BaseRepo<TblFiverrKeyword>, IKeywordRepo
    {
        FiverrDevContext _context;
        public KeywordRepo(FiverrDevContext devContext) : base(devContext)
        {
            _context = devContext;
        }

        public IEnumerable<TblFiverrKeyword> GetAllKeywordOfUserGroup(Guid userId, string groupIds)
        {
            //select* from tbl_FiverrGroup g
            //inner join tbl_UserGroupMap ug on ug.GroupId = g.GroupID
            //inner Join tbl_Users u on u.UserId = ug.UserId
            //where u.UserId = '10D48B96-5F57-45C5-8FF9-80E12410D7C7'

            var t = ConvertDataTable<TblFiverrKeyword>(GetResultFromQuery("select fg.GroupName, g.* from tbl_FiverrKeyword g inner join tbl_FiverrGroup fg on fg.GroupID= g.GroupID inner join tbl_UserKeywordMap ug on ug.KeywordID = g.KeywordID inner Join tbl_Users u on u.UserId = ug.UserId where u.UserId = '" + userId + "' and g.GroupID in (" + groupIds + ") and ug.IsActive = 1"));

            //var result = _context.Set<TblFiverrKeyword>().FromSqlRaw("select fg.GroupName, g.* from tbl_FiverrKeyword g inner join tbl_FiverrGroup fg on fg.GroupID= g.GroupID inner join tbl_UserKeywordMap ug on ug.KeywordID = g.KeywordID inner Join tbl_Users u on u.UserId = ug.UserId where u.UserId = '" + userId + "' and g.GroupID in (" + groupIds + ") and ug.IsActive = 1");
            return t;
        }

        public List<KeywordRunningStatus> GetKeywodsRunningStatus(Guid userId, string groupId)
        {
            var t = ConvertDataTable<KeywordRunningStatus>(GetResultFromQuery("exec Fiverr_GetKeywodsRunningStatus '" + groupId + "','" + userId + "'"));

            //var result = _context.Set<TblFiverrKeyword>().FromSqlRaw("select fg.GroupName, g.* from tbl_FiverrKeyword g inner join tbl_FiverrGroup fg on fg.GroupID= g.GroupID inner join tbl_UserKeywordMap ug on ug.KeywordID = g.KeywordID inner Join tbl_Users u on u.UserId = ug.UserId where u.UserId = '" + userId + "' and g.GroupID in (" + groupIds + ") and ug.IsActive = 1");
            return t;
        }

        public List<KeywordProgressCount> GetKeywordProgressCount(Guid userId, string groupId)
        {
            var t = ConvertDataTable<KeywordProgressCount>(GetResultFromQuery("exec Fiverr_GetGigsCurrentStatus '" + groupId + "','" + userId + "'"));

            //var result = _context.Set<TblFiverrKeyword>().FromSqlRaw("select fg.GroupName, g.* from tbl_FiverrKeyword g inner join tbl_FiverrGroup fg on fg.GroupID= g.GroupID inner join tbl_UserKeywordMap ug on ug.KeywordID = g.KeywordID inner Join tbl_Users u on u.UserId = ug.UserId where u.UserId = '" + userId + "' and g.GroupID in (" + groupIds + ") and ug.IsActive = 1");
            return t;
        }

        #region Rolling Chart Shilpa
        public RollingData GetRollingData(Guid userId, int keywordId, int secDifference)
        {
            var result = ConvertDataTable<RollingData>(GetResultFromQuery("exec fiverr_GetKeywordStatus '" + keywordId + "', '" + userId + "'," + secDifference));
            return result.ToList().FirstOrDefault();
        }

        public List<Keyword> GetKeywordByGroup(Guid userId, int groupId)
        {
            var result = ConvertDataTable<Keyword>(GetResultFromQuery("exec fiverr_GetkeywordByGroup " + groupId + ", '" + userId + "'"));
            return result;
        }
        #endregion
    }
}
