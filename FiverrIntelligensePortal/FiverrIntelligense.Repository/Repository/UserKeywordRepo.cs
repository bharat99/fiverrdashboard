﻿using FiverrIntelligense.Data.Models;
using FiverrIntelligense.Repository.Common;
using FiverrIntelligense.Repository.IRepository;
using System;
using System.Collections.Generic;
using System.Text;

namespace FiverrIntelligense.Repository.Repository
{
    public class UserKeywordRepo : BaseRepo<TblUserKeywordMap>, IUserKeywordRepo
    {
        public UserKeywordRepo(FiverrDevContext devContext):base(devContext)
        {

        }
    }
}
