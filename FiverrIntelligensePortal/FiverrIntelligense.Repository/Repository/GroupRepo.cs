﻿using FiverrIntelligense.Data.Common;
using FiverrIntelligense.Data.Models;
using FiverrIntelligense.Repository.Common;
using FiverrIntelligense.Repository.IRepository;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FiverrIntelligense.Repository.Repository
{
    public class GroupRepo : BaseRepo<TblFiverrGroup>, IGroupRepo
    {
        FiverrDevContext _context;
        public GroupRepo(FiverrDevContext devContext) : base(devContext)
        {
            _context = devContext;
        }

        public List<TblFiverrGroup> GetAllGroupsOfUsr(Guid userId)
        {

            //select* from tbl_FiverrGroup g
            //inner join tbl_UserGroupMap ug on ug.GroupId = g.GroupID
            //inner Join tbl_Users u on u.UserId = ug.UserId
            //where u.UserId = '10D48B96-5F57-45C5-8FF9-80E12410D7C7'

            var result = _context.Set<TblFiverrGroup>().FromSqlRaw("select g.* from tbl_FiverrGroup g inner join tbl_UserGroupMap ug on ug.GroupId = g.GroupID inner Join tbl_Users u on u.UserId = ug.UserId where u.UserId = '" + userId + "'");
            return result.ToListAsync().GetAwaiter().GetResult();
        }

        public List<TblFiverrKeyword> GetAllKeywordsByGroups(Guid userId, string Groups)
        {
            var result = _context.Set<TblFiverrKeyword>().FromSqlRaw("select K.* from Tbl_fiverrKeyword K inner join tbl_UserKeywordMap uk on uk.KeywordId = K.KeywordId inner Join tbl_Users u on u.UserId = uk.UserId where u.UserId = '" + userId + "' and k.GroupId in (select *  from splitstring('" + Groups + "'))");
            return result.ToListAsync().GetAwaiter().GetResult();
        }

        public List<HistoryViewModel> GetAllHistory(Guid userId, string groupId, string startDate, string endDate)
        {
            var result = ConvertDataTable<HistoryViewModel>(GetResultFromQuery("exec Fiverr_GetAllHistory '" + groupId + "', '" + userId + "','" + startDate + "','" + endDate + "'"));
            return result;
        }

        public List<ChangeDetected> GetChangesDetected(Guid userId, string SelectedKeywordIds, string startDate, string endDate)
        {
            var result = ConvertDataTable<ChangeDetected>(GetResultFromQuery("exec Fiverr_GetDetectedChange '" + SelectedKeywordIds + "', '" + userId + "','" + startDate + "','" + endDate + "'"));
            return result;
        }

        public List<SellerAndCustomerCount> GetCountryWiseCustomerCount(Guid userId, string SelectedKeywordIds, string startDate, string endDate)
        {
            var result = ConvertDataTable<SellerAndCustomerCount>(GetResultFromQuery("exec Fiverr_GetCountryWiseCustomerCount '" + SelectedKeywordIds + "', '" + userId + "','" + startDate + "','" + endDate + "'"));
            return result;
        }

        public List<SellerAndCustomerCount> GetCountryWiseSellerCount(Guid userId, string SelectedKeywordIds, string startDate, string endDate)
        {
            var result = ConvertDataTable<SellerAndCustomerCount>(GetResultFromQuery("exec Fiverr_GetCountryWiseSellerCount '" + SelectedKeywordIds + "', '" + userId + "','" + startDate + "','" + endDate + "'"));
            return result;
        }

        public string GetLastUpdatedDate(Guid userId, string groupId)
        {
            var result = GetStringFromQuery("exec GetLastModiffiedDate '" + groupId + "', '" + userId + "'");

            return result == "" ? "" : DateTime.Parse(result).ToString("yyyy-MM-dd");
        }
        public string GetLastUpdatedDateByKeyword(Guid userId, string SelectedKeywordIds)
        {
            var result = GetStringFromQuery("exec GetLastUpdatedDateByKeyword '" + SelectedKeywordIds + "', '" + userId + "'");

            return result == "" ? "" : DateTime.Parse(result).ToString("yyyy-MM-dd");
        }

        public List<PlotData> GetPlotData(Guid userId, string SelectedKeywordIds, string startDate, string endDate, string type)
        {
            var result = ConvertDataTable<PlotData>(GetResultFromQuery("exec Fiverr_GetPlotData '" + SelectedKeywordIds + "', '" + userId + "','" + startDate + "','" + endDate + "','" + type + "'"));
            return result;
        }

        public List<ChangeDetectedDetails> GetChangesDetectedDetailsByLabel(Guid userId, string selectedKeywordIds, string startDate, string endDate, string label)
        {
            var result = ConvertDataTable<ChangeDetectedDetails>(GetResultFromQuery("exec Fiverr_GetDetectedChangeDetailsByLebal '" + selectedKeywordIds + "', '" + userId + "','" + startDate + "','" + endDate + "','" + label + "'"));
            return result;
        }

        

        #region Feedback Changes - Add View button for dahsboard chart and Display records on Popup
        public List<Seller> GetSellerByUserAssignedGroup(Guid userId, string KeywordIds, string startDate, string endDate)
        {
            var result = ConvertDataTable<Seller>(GetResultFromQuery("exec fiverr_GetSellerByUserAssignedGroup '" + KeywordIds + "', '" + userId + "','" + startDate + "','" + endDate + "', 'New Seller'"));
            return result;
        }
        public List<Gig> GetGigByUserAssignedGroup(Guid userId, string KeywordIds, string startDate, string endDate)
        {
            var result = ConvertDataTable<Gig>(GetResultFromQuery("exec fiverr_GetGigByUserAssignedGroup '" + KeywordIds + "', '" + userId + "','" + startDate + "','" + endDate + "', 'New Gig'"));
            return result;
        }

        public List<CustomerReview> GetNewOrderByUserAssignedGroup(Guid userId, string KeywordIds, string startDate, string endDate)
        {
            var result = ConvertDataTable<CustomerReview>(GetResultFromQuery("exec fiverr_GetNewOrderByUserAssignedGroup '" + KeywordIds + "', '" + userId + "','" + startDate + "','" + endDate + "'"));
            return result;
        }

        public List<Seller> GetRemovedSellerByUserAssignedGroup(Guid userId, string KeywordIds, string startDate, string endDate)
        {
            var result = ConvertDataTable<Seller>(GetResultFromQuery("exec fiverr_GetSellerByUserAssignedGroup '" + KeywordIds + "', '" + userId + "','" + startDate + "','" + endDate + "', 'Removed Seller'"));
            return result;
        }

        public List<Gig> GetRemovedGigByUserAssignedGroup(Guid userId, string KeywordIds, string startDate, string endDate)
        {
            var result = ConvertDataTable<Gig>(GetResultFromQuery("exec fiverr_GetGigByUserAssignedGroup '" + KeywordIds + "', '" + userId + "','" + startDate + "','" + endDate + "', 'Removed Gig'"));
            return result;
        }
        #endregion
    }
}
