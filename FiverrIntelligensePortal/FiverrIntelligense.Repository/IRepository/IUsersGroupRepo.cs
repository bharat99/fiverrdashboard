﻿using FiverrIntelligense.Data.Models;
using FiverrIntelligense.Repository.Common;
using System;
using System.Collections.Generic;
using System.Text;

namespace FiverrIntelligense.Repository.IRepository
{
    public interface IUsersGroupRepo : IBaseRepo<TblUserGroupMap>
    {

    }
}
