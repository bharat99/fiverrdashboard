﻿using FiverrIntelligense.Data.Common;
using FiverrIntelligense.Data.Models;
using FiverrIntelligense.Repository.Common;
using System;
using System.Collections.Generic;
using System.Text;

namespace FiverrIntelligense.Repository.IRepository
{
    public interface IKeywordRepo : IBaseRepo<TblFiverrKeyword>
    {
        IEnumerable<TblFiverrKeyword> GetAllKeywordOfUserGroup(Guid userId, string groupIds);
        List<KeywordRunningStatus> GetKeywodsRunningStatus(Guid userId, string groupId);
        List<KeywordProgressCount> GetKeywordProgressCount(Guid userId, string groupId);
        #region Rolling Chart Shilpa
        RollingData GetRollingData(Guid userId, int keywordId, int secDifference);
        List<Keyword> GetKeywordByGroup(Guid userId, int groupId);
        #endregion
    }
}
