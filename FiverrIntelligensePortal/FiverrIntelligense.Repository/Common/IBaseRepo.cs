﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FiverrIntelligense.Repository.Common
{
    public interface IBaseRepo<T>
    {
        IEnumerable<T> GetAll();
        T GetById(int id);
        void Insert(T obj);
        void Update(T obj);
        void Delete(int id);
        void Save();
    }
}
