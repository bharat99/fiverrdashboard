﻿using FiverrIntelligense.Data.Models;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Text;

namespace FiverrIntelligense.Repository.Common
{
    public class BaseRepo<T>:IBaseRepo<T> where T : class
    {
        private FiverrDevContext _context = null;
        public BaseRepo(FiverrDevContext fiverrDevContext)
        {
            this._context = fiverrDevContext;
        }
        public IEnumerable<T> GetAll()
        {
            return _context.Set<T>().ToList(); 
        }
        public T GetById(int id)
        {
            throw new NotImplementedException();
        }
        public void Insert(T obj)
        {
            _context.Add<T>(obj);
        }
        public void Update(T obj)
        {
            _context.Update<T>(obj);
        }
        public void Delete(int id)
        {
            T existing = _context.Find<T>(id);
            _context.Remove<T>(existing);
        }
        public void Save()
        {
            _context.SaveChanges();
        }

        //// ado

        public DataTable GetResultFromQuery(string query)
        {
            DataTable dt = new DataTable();
            try
            {
                DataSet ds = new DataSet();
                using (SqlConnection conn = new SqlConnection(_context.Database.GetDbConnection().ConnectionString))
                {
                    //SqlDataReader
                    conn.Open();

                    SqlDataAdapter adapter = new SqlDataAdapter();
                    adapter.SelectCommand = new SqlCommand(query, conn);
                    adapter.Fill(ds);
                    if (ds.Tables.Count > 0)
                    {
                        dt = ds.Tables[0];
                    }
                    conn.Close();
                }
            }
            catch (Exception ex)
            {

                throw;
            }
            return dt;
        }

        public string GetStringFromQuery(string query)
        {
            string result = "";
            DataTable dt = new DataTable();
            try
            {
                DataSet ds = new DataSet();
                using (SqlConnection conn = new SqlConnection(_context.Database.GetDbConnection().ConnectionString))
                {
                    //SqlDataReader
                    conn.Open();

                    SqlDataAdapter adapter = new SqlDataAdapter();
                    adapter.SelectCommand = new SqlCommand(query, conn);
                    adapter.Fill(ds);
                    if (ds.Tables.Count > 0)
                    {
                        dt = ds.Tables[0];
                    }
                    conn.Close();
                }
                if (dt.Rows.Count > 0)
                {
                    result = dt.Rows[0][0].ToString();
                }
            }
            catch (Exception ex)
            {

                throw;
            }
            return result;
        }

        public int InsertOrUpdate(string query)
        {
            int result = 0;
            try
            {
                DataSet ds = new DataSet();
                using (SqlConnection conn = new SqlConnection(_context.Database.GetDbConnection().ConnectionString))
                {
                    //SqlDataReader
                    conn.Open();

                    var cmd = new SqlCommand(query, conn);

                    result = cmd.ExecuteNonQuery();

                    conn.Close();
                }
            }
            catch (Exception ex)
            {

                throw;
            }
            return result; ;
        }

        public static List<T> ConvertDataTable<T>(DataTable dt)
        {
            List<T> data = new List<T>();
            try
            {
                foreach (DataRow row in dt.Rows)
                {
                    T item = GetItem<T>(row);
                    data.Add(item);
                }
            }
            catch (Exception ex)
            {
            }
            return data;
        }
        private static T GetItem<T>(DataRow dr)
        {
            Type temp = typeof(T);
            T obj = Activator.CreateInstance<T>();
            string lstCol = "";
            try
            {
                foreach (DataColumn column in dr.Table.Columns)
                {
                    lstCol += "," + column.ColumnName;
                    foreach (PropertyInfo pro in temp.GetProperties())
                    {
                        object value = dr[column.ColumnName];
                        if (value == DBNull.Value)
                            value = null;
                        if (pro.Name == column.ColumnName)
                            try
                            {
                                pro.SetValue(obj, value, null);
                            }
                            catch (Exception ex)
                            {
                                pro.SetValue(obj, value.ToString(), null);
                            }
                            
                        else
                            continue;
                    }
                }
            }
            catch (Exception ex)
            {
            }
            return obj;
        }
    }
}
